package kinetica.orch.action;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.PrintStream;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import kinetica.orch.YmlPipelineJob;
import kinetica.orch.YmlPipelineSchema.YmlCsvAction;
import kinetica.orch.process.ProcessLauncherBase;

public class CsvAction extends BaseAction<YmlCsvAction> {

    private static final Logger LOG = LoggerFactory.getLogger(CsvAction.class);
    
    private final Path _jarPath;
    private final ProcessLauncherBase _launcher;
    private final Path _dataRoot;
    
    private String _totRecInserted;
    private String _totRecUpdated;
    private String _totRecRejected;
    
    public CsvAction(YmlPipelineJob _job) throws Exception {
        super(_job);
        this._jarPath = _job.getPropertyPath("kiloader.jar", false);
        this._dataRoot = _job.getPropertyPath("kiloader.data.root.path", false);
        this._launcher = _job.getLauncher();
    }

    
    @Override
    public void executeType(YmlCsvAction _csvAction) throws Exception {
        Path _dataFile = getPath(_csvAction.dataFile);
        
        Path _dataPath = this._dataRoot.resolve(_dataFile);
        List<Path> _dataFiles = this._launcher.expandPath(_dataPath);
        
        Path _propertiesPath = getPath(_csvAction.properties);
        executeLoader(_propertiesPath, _dataFiles);
        LOG.info("Completed ingest: <{}>", _dataFile);
    }
    
    /**
     * Execute the CSV loader.
     * @param _properties
     * @param _dataFiles
     * @throws Exception
     */
    private void executeLoader(Path _properties, List<Path> _dataFiles) throws Exception {
        this._launcher.assertPath(_properties);
        
        List<String> _javaCommand = new ArrayList<>();
        _javaCommand.add("java");
        _javaCommand.add("-Xmx5g");
        _javaCommand.add("-jar");
        _javaCommand.add(this._jarPath.toString());
        _javaCommand.add(_properties.toString());
        
        for(Path _dataFile: _dataFiles) {
            this._launcher.assertPath(_dataFile);
            _javaCommand.add(_dataFile.toString());
        }
        
        String[] _commandArgs = _javaCommand.toArray(new String[0]);
        LOG.info("Ingest files: <{}>", _dataFiles);
        
        if(this._isDryRun) {
            LOG.info("DRY RUN LOADER: <{}>", String.join(" ", _commandArgs));
        }
        else {
        	String _flgFiles = _dataFiles.toString().substring(0,_dataFiles.toString().
        			indexOf(".")).replace("[", "") + ".flg";
           
            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            PrintStream ps = new PrintStream(baos);
            PrintStream old = System.out;
            System.setOut(ps);
        	
            this._launcher.launchProcess(_commandArgs);
            
            System.out.flush();
            System.setOut(old);
            System.out.println(baos.toString());
            
            int _indexStart = baos.toString().indexOf("Records Inserted......") + 23;
            int _indexEnd = baos.toString().indexOf("\n",_indexStart);
            _totRecInserted = baos.toString().substring(_indexStart, _indexEnd).
            		replace(",", "");
            
            _indexStart = baos.toString().indexOf("Records Updated.......") + 23;
            _indexEnd = baos.toString().indexOf("\n",_indexStart);
            _totRecUpdated = baos.toString().substring(_indexStart, _indexEnd).
            		replace(",", "");
            
            _indexStart = baos.toString().indexOf("Records Rejected......") + 23;
            _indexEnd = baos.toString().indexOf("\n",_indexStart);
            _totRecRejected = baos.toString().substring(_indexStart, _indexEnd).
            		replace(",", "");
            
            List<String> list = new ArrayList<String>();
            File file = new File(_flgFiles);
            list = Files.readAllLines(file.toPath(),Charset.defaultCharset());
            String _totRecInFlgTmp = list.toString().substring(list.toString().
            		indexOf("|")+1).replace("]", "").replace("[", "");
            
            String _totRecInFlg = "";
            
            if (_totRecInFlgTmp.indexOf("|") == -1)
            {
            	_totRecInFlg = _totRecInFlgTmp;
            }
            
            else
            {
            	_totRecInFlg = _totRecInFlgTmp.substring(0,_totRecInFlgTmp.indexOf("|"));
            }
            
            if (_totRecInserted.contentEquals(_totRecInFlg))
            {
            	LOG.info("Record inserted from " + _dataFiles.toString() + " (" + _totRecInserted + ") matches with total record in " + _flgFiles + " file (" + _totRecInFlg + ")");
            }
            
            else if (_totRecInFlg.isEmpty())
            {
            	LOG.info("No information in .flg file (" + _flgFiles + ")");
            }
            
            else 
            {
            	LOG.error("Record inserted from " + _dataFiles.toString() + " (" + _totRecInserted + ") does not match with total record in " + _flgFiles + " file (" + _totRecInFlg + ")");
            	throw new java.lang.Error("Record inserted from " + _dataFiles.toString() + " (" + _totRecInserted + ") does not match with total record in " + _flgFiles + " file (" + _totRecInFlg + ")");
            }
        }
    }
    
    public String getTotRecInserted()
    {
    	return this._totRecInserted;
    }
    
    public String getTotRecUpdated()
    {
    	return this._totRecUpdated;
    }
    
    public String getTotRecRejected()
    {
    	return this._totRecRejected;
    }
}
