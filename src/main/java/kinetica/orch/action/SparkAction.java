package kinetica.orch.action;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.io.PrintStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.kinetica.jdbc.Driver;

import kinetica.orch.YmlPipelineJob;
import kinetica.orch.YmlPipelineSchema.YmlSparkAction;
import kinetica.orch.process.ProcessLauncherBase;

public class SparkAction extends BaseAction<YmlSparkAction> {
    
    private static final Logger LOG = LoggerFactory.getLogger(SparkAction.class);
    
    private final ProcessLauncherBase _launcher;
    private final Map<String,String> _sparkConf;
    private final String _user;
    private final String _passwd;
    private final String _url;
    private final Path _sparkJar;
    
    private final Properties _props = new Properties();
    private static final Driver DRIVER = new Driver();
    private static final int JDBC_PORT = 9191;
    private final String _urlDb;
    
    private String _resultSourceTable;
    private String _resultTargetTable;
    
    public SparkAction(YmlPipelineJob _job) throws Exception {
        super(_job);
        this._launcher = _job.getLauncher();
        
        // HACK: This is hard-coded because the target environment is missing
        // an appropriate path link.
        this._launcher.setEnv("JAVA_HOME", "/usr/java/jdk1.8.0_121");
        
        this._sparkConf = _job.getPropertySet("spark.");
        this._user = _job.getProperty(CONF_KINETICA_USER);
        this._passwd = _job.getProperty(CONF_KINETICA_PWD);
        this._sparkJar = _job.getPropertyPath("kinetica.spark-jar", false);
        
        String _host = _job.getProperty(CONF_KINETICA_HOST);
        this._url = String.format("http://%s:%d", _host, 9191);
        
        this._urlDb = String.format("jdbc:kinetica://%s:%d", _host, JDBC_PORT);
        this._props.put("UID", _user);
        this._props.put("PWD", _passwd);
    }
    
    @Override
    public void executeType(YmlSparkAction _sparkAction) throws Exception {
        HashMap<String,String> _loaderArgs = getLoaderArgs(_sparkAction);

        _loaderArgs.entrySet().stream()
            .sorted((_e1, _e2) -> _e1.getKey().compareTo(_e2.getKey()))
            .forEach(_entry -> {
                LOG.info("SPARK-SUBMIT: {} = {}", _entry.getKey(), _entry.getValue());
         });
        
        String[] _commandArgs = getCommandArgs(_loaderArgs);
        
        if(this._isDryRun) {
            LOG.info("DRY RUN: <{}>", String.join(" ", _commandArgs));
        }
        else {
        	_resultSourceTable = "";
        	_resultTargetTable = "";
        	
        	ByteArrayOutputStream baos = new ByteArrayOutputStream();
            PrintStream ps = new PrintStream(baos);
            PrintStream old = System.out;
            System.setOut(ps);
            
            LOG.info("SPARK: <{}>", String.join(" ", _commandArgs));
            this._launcher.launchProcess(_commandArgs);
            
            System.out.flush();
            System.setOut(old);
            System.out.println(baos.toString());
            
            Path _sqlPath = this._pipelineRoot.resolve(_sparkAction.sqlSript);
            String _query = generateScript(_sqlPath);
            
            int _indexStart = baos.toString().indexOf("Original dataset has <") + 22;
            int _indexEnd = baos.toString().indexOf(">",_indexStart);
            _resultSourceTable = baos.toString().substring(_indexStart, _indexEnd);
            
            try (Connection _conn = newConnection()) {
                _resultTargetTable =  returnSql(_conn, "SELECT COUNT(*) FROM " + this._subst.replace(_sparkAction.destTable));
            
                if (_resultSourceTable.isEmpty() || _resultSourceTable.contentEquals("0"))
                {
                	LOG.info("No record in HIVE" + "\n" + "Query: " + "\n" + _query);
                }
                
                else if (_resultSourceTable.contentEquals(_resultTargetTable))
                {
                	LOG.info("Total record in HIVE (" + _resultSourceTable + ") matches with total record in Kinetica - " + this._subst.replace(_sparkAction.destTable) + " (" + _resultTargetTable + ")" + "\n" + "Query: " + "\n" + _query);
                }
                
                else 
                {
                	LOG.error("Total record in HIVE (" + _resultSourceTable + ") does not match with total record in Kinetica - " + this._subst.replace(_sparkAction.destTable) + " (" + _resultTargetTable + ")" + "\n" + "Query: " + "\n" + _query);
                	throw new java.lang.Error("Total record in HIVE (" + _resultSourceTable + ") does not match with total record in Kinetica - " + this._subst.replace(_sparkAction.destTable) + " (" + _resultTargetTable + ")" + "\n" + "Query: " + "\n" + _query);
                }
            }
        }
    }
    
    private HashMap<String,String> getLoaderArgs(YmlSparkAction _sparkAction) throws Exception {
        HashMap<String,String> _loaderArgs = new HashMap<>();
        _loaderArgs.put("database.url", this._url);
        _loaderArgs.put("database.username", this._user);
        _loaderArgs.put("database.password", this._passwd);
        _loaderArgs.put("table.name", this._subst.replace(_sparkAction.destTable));
        
        // create a new script with variable substitution
        Path _sqlPath = this._pipelineRoot.resolve(_sparkAction.sqlSript);
        String _tmpScript = generateScript(_sqlPath);
        _loaderArgs.put("source.sql_file", _tmpScript);
        
        return _loaderArgs;
    }
    
    private String[] getCommandArgs(HashMap<String,String> _loaderArgs) {
        List<String> _javaCommand = new ArrayList<>();
        
        _javaCommand.add("spark2-submit");
        _javaCommand.add("--class");
        _javaCommand.add("com.kinetica.spark.SparkKineticaDriver");
        
        this._sparkConf.forEach((_key, _val) -> {
            _javaCommand.add("--conf");
            _javaCommand.add(String.format("%s=%s", _key, _val));
        });

        _javaCommand.add(this._sparkJar.toString());
        _javaCommand.add("spark.properties");
        
        _loaderArgs.forEach((_key, _val) -> {
            _javaCommand.add("--" + _key);
            _javaCommand.add(_val);
        });
        
        String[] _commandArgs = _javaCommand.toArray(new String[0]);
        return _commandArgs;
    }
    
    private String generateScript(Path _scriptPath) throws Exception {
        List<String> _lines = Files.readAllLines(_scriptPath);
        
        StringBuilder _builder = new StringBuilder();
        
        _lines.forEach(_line -> {
            String _replace = this._subst.replace(_line);
            _builder.append(_replace);
            _builder.append(System.lineSeparator());
        });
        
        String _hiveSql = _builder.toString();
        LOG.info("Hive SQL: {}", _hiveSql);
        
        InputStream _reader = new ByteArrayInputStream(_hiveSql.getBytes());
        String _fileName = _scriptPath.getFileName().toString();
        String _tmpFile = this._launcher.copyTempFile(_reader, _fileName);
        return _tmpFile;
    }
    
    private String returnSql(Connection _conn, String _sqlText) throws Exception {

    	try (Statement _stmt = _conn.createStatement()) {
	    	String query = _sqlText;	
	    	ResultSet rs = _stmt.executeQuery(query);
	    	rs.next();
	    	String n = rs.getString(1);

	    	return n;
    	}
    	
    }
    
    public Connection newConnection() throws SQLException {
        return DRIVER.connect(this._urlDb, this._props);
    }
    
    public String getTotRecInserted()
    {
    	return this._resultSourceTable;
    }

}
