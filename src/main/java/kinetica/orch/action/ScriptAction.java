package kinetica.orch.action;


import java.nio.file.Path;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import kinetica.orch.YmlPipelineJob;
import kinetica.orch.YmlPipelineSchema.YmlScriptAction;
import kinetica.orch.process.ProcessLauncherBase;

public class ScriptAction  extends BaseAction<YmlScriptAction> {

    private static final Logger LOG = LoggerFactory.getLogger(ScriptAction.class);
    
    private final ProcessLauncherBase _launcher;
    
    public ScriptAction(YmlPipelineJob _job) {
        super(_job);
        this._launcher = _job.getLauncher();
    }

    @Override
    public void executeType(YmlScriptAction _scriptAction) throws Exception {
        String _substCommand = this._subst.replace(_scriptAction.command);
        
        LOG.info("Executing command: {}", _substCommand);
        
        String[] _commandArgs = _substCommand.split(" ");
        
        // check that program exists
        Path _scriptPath = getPath(_commandArgs[0]);
        this._launcher.assertPath(_scriptPath);
        
        if(this._isDryRun) {
            LOG.info("DRY RUN: <{}>", String.join(" ", _commandArgs));
        }
        else {
            this._launcher.launchProcess(_commandArgs);
        }
    }

}
