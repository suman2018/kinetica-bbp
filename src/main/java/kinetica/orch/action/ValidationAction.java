package kinetica.orch.action;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.kinetica.jdbc.Driver;

import kinetica.orch.Conductor;
import kinetica.orch.YmlPipelineJob;
import kinetica.orch.YmlPipelineSchema.YmlValidationAction;

public class ValidationAction extends BaseAction<YmlValidationAction> {

    private static final Logger LOG = LoggerFactory.getLogger(SqlAction.class);
    private static final Driver DRIVER = new Driver();
    private static final int JDBC_PORT = 9191;
    
    private final Properties _props = new Properties();
    private final String _url;
    private final String _pipelineName;
    private final String _runDate;
    private final String _utilsSchema;
    private final String _tablePrefix;

    private Path _workDir = null;

    public ValidationAction(YmlPipelineJob _job) throws Exception {
        super(_job);
        String _kineticaHost = _job.getProperty(CONF_KINETICA_HOST);
        this._url = String.format("jdbc:kinetica://%s:%d", _kineticaHost, JDBC_PORT);

        String _kineticaUser = _job.getProperty(CONF_KINETICA_USER);
        String _kineticaPassword = _job.getProperty(CONF_KINETICA_PWD);
        this._pipelineName = _job.getPipelineName();
        this._runDate = _job.getProperty(Conductor.CONFIG_RUN_DATE);
        this._utilsSchema = _job.getProperty(Conductor.CONFIG_UTILS_SCHEMA);
        this._tablePrefix = _job.getProperty(Conductor.CONFIG_TABLE_PREFIX);

        this._props.put("UID", _kineticaUser);
        this._props.put("PWD", _kineticaPassword);
        // props.setProperty("ParentSet",

        setWorkDir(_job.getPipelineRoot());
    }

    public void setWorkDir(Path _workDir) {
        this._workDir = Paths.get(_workDir.toString());
    }

    @Override
    public void executeType(YmlValidationAction _validationAction) throws Exception {
        Path _sqlPath = getPath(_validationAction.sqlSript);
        executeSql(_sqlPath, _validationAction.name);
    }
    
    public Connection newConnection() throws SQLException {
        return DRIVER.connect(this._url, this._props);
    }

    private void executeSql(Path sqlScriptPath, String _pipelineStep) throws Exception {
        Path _sqlFile = sqlScriptPath;
        
        if (this._workDir != null) {
            _sqlFile = this._workDir.resolve(sqlScriptPath);
        }

        String[] _statements = getStatements(_sqlFile);

        final String _hint = String.format(
            "/* KI_HINT_JOBID_PREFIX(pipeline=%s,step=%s,rundate=%s) */ \n",
            this._pipelineName, _pipelineStep, this._runDate);
        
        try (Connection _conn = newConnection()) {
        	
        	long _resultSourceTable = 0;
        	long _resultTargetTable = 0;
        	String _sourceTableName = "";
    		String _targetTableName = "";
    		String _srcColName ="";
    		String _tgtColName ="";
        	
        	for (int _idx = 0; _idx < _statements.length; _idx++) {
                
        		String _sqlText = _hint + _statements[_idx];
                LOG.info("SQLTEXT ({} {}/{}): \n{}", 
                        sqlScriptPath.getFileName(), _idx + 1, _statements.length, _sqlText);

                executeSql(_conn, _sqlText);
                
                if (_idx%2 == 0)
                {
                	String _statementsTmp = _statements[_idx].substring(_statements[_idx].toUpperCase().indexOf("FROM ")+5, _statements[_idx].length());
            		if (_statementsTmp.contains(" ")) {
            			_sourceTableName = _statementsTmp.substring(0,_statementsTmp.indexOf(" "));
            		}
            		else
            		{
            			_sourceTableName = _statementsTmp.replace(";", "");
            		}
                	
            		_srcColName = _statements[_idx].substring(_statements[_idx].indexOf("(")+1, _statements[_idx].indexOf(")"));
            		
                	_resultSourceTable =  returnSql(_conn, _statements[_idx]);
                }
                
                else
                {
                	
                	String _statementsTmp = _statements[_idx].substring(_statements[_idx].toUpperCase().indexOf("FROM ")+5, _statements[_idx].length());
            		if (_statementsTmp.contains(" ")) {
            			_targetTableName = _statementsTmp.substring(0,_statementsTmp.indexOf(" "));
            		}
            		else
            		{
            			_targetTableName = _statementsTmp.replace(";", "");
            		}
            		
                	_tgtColName = _statements[_idx].substring(_statements[_idx].indexOf("(")+1, _statements[_idx].indexOf(")"));
            		
                	String _aggregateTypeName = "sum";
                	if (_statements[_idx].toLowerCase().contains("count("))
                		{
                			_aggregateTypeName = "count";
                		}
                	
                	_resultTargetTable =  returnSql(_conn, _statements[_idx]);
                	
                	String deleteSql = "DELETE FROM " + _tablePrefix + _utilsSchema + ".data_validation WHERE "
                    		+ "file_dt = '" + _runDate + "' AND src_tbl = '" + _sourceTableName + "' AND tgt_tbl = '" + _targetTableName + "' AND "
                    		+ "src_col_nm = '" + _srcColName + "' AND tgt_col_nm = '" + _tgtColName + "' AND agg_tp = '" + _aggregateTypeName + "'";
                	executeSql(_conn, deleteSql);
                	
                	String insertSql = "INSERT INTO " + _tablePrefix + _utilsSchema + ".data_validation VALUES "
                    		+ "('" + _runDate + "', '" + _sourceTableName + "', '" + _targetTableName + "', '" + _srcColName + "', '" + _tgtColName + "', '"
                    		+ _aggregateTypeName + "'," + _resultSourceTable + "," + _resultTargetTable + ")";
                    
                    executeSql(_conn, insertSql);
                    
                	if (_resultSourceTable != _resultTargetTable)
                    {
                    	LOG.error("Data Validation - " + _aggregateTypeName + " - of " + _sourceTableName + " (" + _resultSourceTable + ")" + " and " + _targetTableName + " (" + _resultTargetTable + ")" + " failed");
                    	throw new java.lang.Error("Data Validation - " + _aggregateTypeName + " - of " + _sourceTableName + " (" + _resultSourceTable + ")" + " and " + _targetTableName + " (" + _resultTargetTable + ")" + " failed");
                    }
                    else 
                    {
                    	LOG.info("Data Validation - " + _aggregateTypeName + " - of " + _sourceTableName + " (" + _resultSourceTable + ")" + " and " + _targetTableName + " (" + _resultTargetTable + ")" + " succeeded");
                    }
                }
            }

        }
    }

    private void executeSql(Connection _conn, String _sqlText) throws Exception {

        if (this._isDryRun) {
            LOG.info("Skipping SQL because of dry run.");
            return;
        }

        try (Statement _stmt = _conn.createStatement()) {
            boolean _hasResults = _stmt.execute(_sqlText);
            if (_hasResults) {
                // read results to force any exceptions to be thrown
                try (ResultSet _results = _stmt.getResultSet()) {
                    _results.next();
                }
                LOG.info("SQL update count: {}", _stmt.getUpdateCount());
            }
            else {
                LOG.info("SQL returned no results: {}", _stmt.getUpdateCount());
            }
        }
    }
    
    private long returnSql(Connection _conn, String _sqlText) throws Exception {

    	try (Statement _stmt = _conn.createStatement()) {
	    	String query = _sqlText;
	    		
	    		ResultSet rs = _stmt.executeQuery(query);
	    		rs.next();
	    		long n = rs.getLong(1);

	    	return n;
    	}
    	
    }
    
    private String[] getStatements(Path _sqlFile) throws FileNotFoundException, IOException {

        StringBuilder _sb = new StringBuilder();
        final List<String> _statements = new ArrayList<>();

        try (BufferedReader _br = new BufferedReader(new FileReader(_sqlFile.toFile()))) {

            while (true) {
                String _line = _br.readLine();
                if (_line == null) {
                    break;
                }

                // strip comments
                _line = _line.replaceFirst("--.*", "");

                // strip trailing whitespace
                _line = StringUtils.stripEnd(_line, " \n");

                if (_line.isEmpty()) {
                    continue;
                }

                if (this._subst != null) {
                    _line = this._subst.replace(_line);
                }

                if (_line.contains(";")) {
                    _line = _line.replaceFirst(";.*", "");
                    _sb.append(_line);
                    // _sb.append('\n');

                    _statements.add(_sb.toString());
                    _sb = new StringBuilder();
                }
                else {
                    _sb.append(_line);
                    _sb.append('\n');
                }
            }
        }

        if (_sb.length() > 0) {
            _statements.add(_sb.toString());
        }

        return _statements.toArray(new String[0]);
    }
}
