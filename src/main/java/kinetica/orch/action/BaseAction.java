package kinetica.orch.action;

import java.nio.file.Path;
import java.nio.file.Paths;

import org.apache.commons.text.StringSubstitutor;

import kinetica.orch.YmlPipelineJob;
import kinetica.orch.YmlPipelineSchema.YmlAction;

public abstract class BaseAction<T extends YmlAction> {

    //private static final Logger LOG = LoggerFactory.getLogger(BaseAction.class);

    public final static String CONF_KINETICA_HOST = "kinetica.host";
    public final static String CONF_KINETICA_USER = "kinetica.host.user";
    public final static String CONF_KINETICA_PWD = "kinetica.host.password";
    
    protected final boolean _isDryRun;
    protected StringSubstitutor _subst;
    protected final Path _pipelineRoot;
    
    public BaseAction(YmlPipelineJob _job) {
        this._isDryRun = _job.getIsDryRun();
        this._subst = _job.getSubstitutor();
        this._pipelineRoot = _job.getPipelineRoot();
    }

    public void execute(YmlAction _job) throws Exception {
        @SuppressWarnings("unchecked")
        T _cast = (T)_job;
        executeType(_cast);
    }
    
    public abstract void executeType(T _csvAction) throws Exception;

    protected Path getPath(String _strPath) {
        // replace variables
        String _substStr = this._subst.replace(_strPath);
        return Paths.get(_substStr);
    }
    
    public void setSubst(StringSubstitutor _subst) {
    	this._subst = _subst;
    }
}
