package kinetica.orch;

import java.net.InetAddress;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Timestamp;
import java.time.Duration;
import java.time.Instant;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import kinetica.orch.action.BaseAction;
import kinetica.orch.action.SqlAction;

public class JobStatusUpdater implements AutoCloseable {

    private enum Columns {
    	job_id, 
        run_date,
        environment,
        step_num,
        pipeline_name, 
        pipeline_step, 
        status, 
        start_ts, 
        run_seconds,
        client_uid,
        client_host,
        kinetica_uid,
        error_msg,
        rec_inserted,
        rec_updated,
        rec_rejected
    }
        
    
    private static final Logger LOG = LoggerFactory.getLogger(JobStatusUpdater.class);
    private static final String LOG_TABLE = "pipeline_log";
    private static final String LOG_SCHEMA = "orch";
    private static final List<Columns> _enumList = Arrays.asList(Columns.values());

    private final Connection _conn;
    private final String _pipelineName;
    private final String _jobId;
    private final String _runDate;
    private final String _environment;
    private final PreparedStatement _pstmt;
    private final String _kineticaUid;
    private final String _clientHost;
    private final String _clientUid;

    public JobStatusUpdater(YmlPipelineJob _job, SqlAction _sqlAction) throws Exception {
        this._pipelineName = _job.getPipelineName();
        this._jobId = _job.getJobId();
        this._runDate = _job.getProperty(Conductor.CONFIG_RUN_DATE);
        this._environment = _job.getProperty(Conductor.CONFIG_TABLE_PREFIX).substring(0, 3);
        this._kineticaUid = _job.getProperty(BaseAction.CONF_KINETICA_USER);
        this._clientHost = InetAddress.getLocalHost().getHostName();
        this._clientUid = System.getProperty("user.name");
        this._conn = _sqlAction.newConnection();
        
        if(!hasTable()) {
            createTable();
        }
        
        String _sqlText = getSqlInsert();
        this._pstmt = this._conn.prepareStatement(_sqlText);
    }

    @Override
    public void close() throws Exception {
        this._pstmt.close();
        this._conn.close();
    }

    private static int colIdx(Columns _val) {
        return _enumList.indexOf(_val) + 1;
    }
    
    private void createTable() throws SQLException {
        
        Map<Columns, String> _colDefs = new HashMap<>();
        _colDefs.put(Columns.pipeline_name, "varchar(64, shard_key, dict) not null");
        _colDefs.put(Columns.pipeline_step, "varchar(64, shard_key, dict) not null");
        _colDefs.put(Columns.step_num, "int not null");
        _colDefs.put(Columns.job_id, "varchar(8) not null");
        _colDefs.put(Columns.run_date, "date(dict) not null");
        _colDefs.put(Columns.environment, "varchar(4) not null");
        _colDefs.put(Columns.status, "varchar(8, dict) not null");
        _colDefs.put(Columns.start_ts, "timestamp not null");
        _colDefs.put(Columns.run_seconds, "long not null");
        _colDefs.put(Columns.client_uid, "varchar(32, dict) not null");
        _colDefs.put(Columns.client_host, "varchar(32, dict) not null");
        _colDefs.put(Columns.kinetica_uid, "varchar(32, dict) not null");
        _colDefs.put(Columns.error_msg, "string");
        _colDefs.put(Columns.rec_inserted, "long");
        _colDefs.put(Columns.rec_updated, "long");
        _colDefs.put(Columns.rec_rejected, "long");
        
        String _colJoin = _enumList.stream()
            .map(_col -> { return String.format("%s %s", _col, _colDefs.get(_col)); })
            .collect(Collectors.joining(",\n"));

        StringBuilder _sqlText = new StringBuilder();
        _sqlText.append(String.format("create table %s.%s (\n", LOG_SCHEMA, LOG_TABLE));
        _sqlText.append(_colJoin);
        _sqlText.append(")");
        
        LOG.info("Creating log table: \n{}", _sqlText.toString());
        try (Statement _stmt = this._conn.createStatement()) {
            _stmt.execute(_sqlText.toString());
        }
    }
    
    private boolean hasTable() throws SQLException {
        // check that the table exists
        try (Statement _stmt = this._conn.createStatement()) {
            String _sqlText = String.format("select 1 from %s where 1 = 0", LOG_TABLE);
            _stmt.execute(_sqlText);
        }
        catch(SQLException _ex) {
            if(_ex.getSQLState() == "00E00") {
                return false;
            }
            else {
                throw _ex;
            }
        }
        return true;
    }

    public void logStep(String _stepName, int _stepNum, Instant _start, String _CsvRecInserted, String _CsvRecUpdated, String _CsvRecRejected, Exception _ex) throws SQLException {
        Timestamp _startTs = Timestamp.from(_start);
        Instant _end = Instant.now();
        long _runMs = Duration.between(_start, _end).toMillis();
        long _runSec = Math.round((double)_runMs/1000D);
        String _status = "SUCCESS";
        String _errorMsg = null;
        
        if(_ex != null) {
            _status = "FAIL";
            _errorMsg = _ex.toString();
        }
    
        this._pstmt.setString(colIdx(Columns.pipeline_name), this._pipelineName);
        this._pstmt.setString(colIdx(Columns.pipeline_step), _stepName);
        this._pstmt.setInt(colIdx(Columns.step_num), _stepNum);
        this._pstmt.setString(colIdx(Columns.job_id), this._jobId);
        this._pstmt.setString(colIdx(Columns.run_date), this._runDate);
        this._pstmt.setString(colIdx(Columns.environment), this._environment);
        this._pstmt.setTimestamp(colIdx(Columns.start_ts), _startTs);
        this._pstmt.setLong(colIdx(Columns.run_seconds), _runSec);
        this._pstmt.setString(colIdx(Columns.client_uid), this._clientUid);
        this._pstmt.setString(colIdx(Columns.client_host), this._clientHost);
        this._pstmt.setString(colIdx(Columns.kinetica_uid), this._kineticaUid);
        this._pstmt.setString(colIdx(Columns.status), _status);
        this._pstmt.setString(colIdx(Columns.error_msg), _errorMsg);
        this._pstmt.setString(colIdx(Columns.rec_inserted), _CsvRecInserted);
        this._pstmt.setString(colIdx(Columns.rec_updated), _CsvRecUpdated);
        this._pstmt.setString(colIdx(Columns.rec_rejected), _CsvRecRejected);
        
        this._pstmt.execute();
    }
    
    private static String getSqlInsert() {
        StringBuilder _sb = new StringBuilder();
        _sb.append(String.format("INSERT INTO %s\n ", LOG_TABLE));
        
        String _colStr = Arrays.asList(Columns.values()).stream()
            .map(Columns::toString)
            .collect(Collectors.joining(", "));
        _sb.append(String.format("(%s)\n ", _colStr));
        
        String _params =  _enumList.stream().map((x) -> "?")
                .collect(Collectors.joining(", "));
        _sb.append(String.format("VALUES(%s)\n ", _params));
        
        return _sb.toString();
    }
    
}
