package kinetica.orch;


import java.nio.file.Path;
import java.nio.file.Paths;
import java.time.Instant;
import java.util.Arrays;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.Map;
import java.util.stream.Collectors;

import org.apache.commons.text.RandomStringGenerator;
import org.apache.commons.text.StringSubstitutor;
import org.quartz.Job;
import org.quartz.JobDataMap;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.quartz.JobKey;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.slf4j.MDC;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.dataformat.yaml.YAMLFactory;

import kinetica.orch.Conductor.RunType;
import kinetica.orch.YmlPipelineSchema.YmlAction;
import kinetica.orch.YmlPipelineSchema.YmlCsvAction;
import kinetica.orch.YmlPipelineSchema.YmlScriptAction;
import kinetica.orch.YmlPipelineSchema.YmlSparkAction;
import kinetica.orch.YmlPipelineSchema.YmlSqlAction;
import kinetica.orch.YmlPipelineSchema.YmlValidationAction;
import kinetica.orch.action.CsvAction;
import kinetica.orch.action.ScriptAction;
import kinetica.orch.action.SparkAction;
import kinetica.orch.action.SqlAction;
import kinetica.orch.action.ValidationAction;
import kinetica.orch.process.ProcessLauncherBase;
import kinetica.orch.process.ProcessLauncherLocal;
import kinetica.orch.process.ProcessLauncherRemote;

public class YmlPipelineJob implements Job {

    private static final Logger LOG = LoggerFactory.getLogger(YmlPipelineJob.class);
    private static final RandomStringGenerator RANDOM;
    
    static {
        char[][] _filter = {{'A','Z'},{'0','9'}};
        RANDOM = new RandomStringGenerator.Builder().withinRange(_filter).build();
    }
    
    public enum CondFlags { INIT, REPROCESS, DELETE_DATA }
    
    private JobDataMap _jobProperties;
    private boolean _isDryRun;
    private Path _pipelineRoot;
    private String _pipelineName;
    private String _pipelineStep;
    private StringSubstitutor _subst;
    private EnumSet<CondFlags> _condFlags = EnumSet.noneOf(CondFlags.class);
    
    private String _csvRecInserted;
    private String _csvRecUpdated;
    private String _csvRecRejected;
    private String _sparkRecInserted;
    
    // action executors
    private ProcessLauncherBase _launcher;
    private CsvAction _csvAction;
    private SqlAction _sqlAction;
    private SparkAction _sparkAction;
    private ScriptAction _scriptAction;
    private ValidationAction _validationAction;
    private String _jobId;
    private RunType _runType;

    /** 
     * Get a property from the job configuration and throw exception if missing.
     * @param _key
     * @return
     * @throws Exception
     */
    public String getProperty(String _key) throws Exception {
        String _val = this._jobProperties.getString(_key);
        if(_val == null) {
            throw new Exception("Config key not found: " + _key);
        }
        return _val;
    }
    
    public boolean getPropertyBool(String _key) {
        return this._jobProperties.getBoolean(_key);
    }
    
    public Path getPropertyPath(String _key, boolean _validate) throws Exception {
        String _valStr = getProperty(_key);
        Path _path = Paths.get(_valStr);
        
        if(_validate && !_path.toFile().exists()) {
            String _msg = String.format("Param path <%s> does not exist: %s", _key, _path);
            throw new Exception(_msg);
        }
        return _path;
    }
    
    public Map<String,String> getPropertySet(String _setKey) {
        Map<String,String> _props = new HashMap<String,String>();

        this._jobProperties.forEach((_key, _val) -> {
        	if(_key.startsWith(_setKey)) {
                if(_val != null) {
                    _props.put(_key, _val.toString());                    
                }
            }
        });
        return _props;
    }

    public StringSubstitutor getSubstitutor() {
        return this._subst;
    }
    
    public boolean getIsDryRun() {
        return this._isDryRun;
    }
    
    public ProcessLauncherBase getLauncher() {
        return this._launcher;
    }
    
    public Path getPipelineRoot() {
        return this._pipelineRoot;
    }
    
    public String getPipelineName() {
        return this._pipelineName;
    }
    
    public String getLogName() {
        return String.format("%s_%s", this._pipelineName, this._jobId);
    }
    
    public String getJobId() { 
        return this._jobId;
    }

    private boolean isCondition(String _condFlag) {
        CondFlags _val = CondFlags.valueOf(_condFlag.toUpperCase());
        return this._condFlags.contains(_val);
    }

    @Override
    public void execute(JobExecutionContext _context) throws JobExecutionException {
        try {
            JobKey _jobKey = _context.getJobDetail().getKey();
            this._pipelineName = _jobKey.getName();
            this._jobId = RANDOM.generate(8);

            // add job name to logging MDC context
            MDC.put("job", getLogName());
            
            LOG.info("Starting job: <{}>", getLogName());
            this._jobProperties = _context.getMergedJobDataMap();
            
            setupJob();

            Path _ymlFile = this.getPropertyPath(Conductor.CONFIG_YML_FILE, true);
            
            LOG.info("Executing YML: {}", _ymlFile);
            ObjectMapper mapper = new ObjectMapper(new YAMLFactory());
            YmlPipelineSchema _pipeline = mapper.readValue(_ymlFile.toFile(), YmlPipelineSchema.class);
            
            if(!this._pipelineName.equals(_pipeline.name)) {
                throw new Exception(
                        String.format("Name in YML script <%s> does not match pipeline name <%s>.",
                                _pipeline.name, this._pipelineName));
            }
            
            LOG.info("Pipeline Name: {} ({})", _pipeline.name, _pipeline.description);
            try(JobStatusUpdater _status = new JobStatusUpdater(this, this._sqlAction)) {
                boolean _wasExecuted = false;
                int _stepsExecuted = 0;
                
                for(YmlAction _action : _pipeline.workflow) {
                	this._subst = ParamSubstitutor.createSubstitutor(this._jobProperties, this._jobId, _action.name);
                	this._sqlAction.setSubst(this._subst);
                    Instant _startTime = Instant.now();
                    try {
                        if(executeStep(_action, _wasExecuted)) {
                            _wasExecuted = true;
                            _stepsExecuted += 1;
                            _sparkRecInserted = _sparkAction.getTotRecInserted();
                            _csvRecInserted = _csvAction.getTotRecInserted();
                            _csvRecUpdated = _csvAction.getTotRecUpdated();
                            _csvRecRejected = _csvAction.getTotRecRejected();
                            
                            String _actionType = _action.getClass().toString();
                            
                            if (_actionType.contains("YmlCsvAction"))
                            {
                            	_status.logStep(_action.name, _stepsExecuted, _startTime, _csvRecInserted, _csvRecUpdated, _csvRecRejected, null);
                            }
                            
                            else if (_actionType.contains("YmlSparkAction"))
                            {
                            	_status.logStep(_action.name, _stepsExecuted, _startTime, _sparkRecInserted, null, null, null);
                            }
                            
                            else
                            	
                            {
                            	_status.logStep(_action.name, _stepsExecuted, _startTime, null, null, null, null);
                            }
                        }
                    }
                    catch(Exception _ex) {
                        _stepsExecuted += 1;
                        _status.logStep(_action.name, _stepsExecuted, _startTime, null, null, null, _ex);
                        throw _ex;
                    }
                }
                
                if(!_wasExecuted) {
                    throw new Exception("No Steps were executed!");
                }
            }
            
            LOG.info("Completed job: <{}>", _jobKey.getName());
        }
        catch(Exception ex) {
            LOG.error("Job failed", ex);
            throw new JobExecutionException(ex.getMessage(), ex);
        }
        finally {
            if(this._launcher != null) { 
                try {
                    this._launcher.close();
                }
                catch (Exception e) {
                    e.printStackTrace();
                } 
                
            }
        }
    }

    private void setupJob() throws Exception {
        this._jobProperties.entrySet().stream()
            .sorted((_e1, _e2) -> _e1.getKey().compareTo(_e2.getKey()))
            .forEach(_entry -> {
                LOG.info("PARAM: {} = {}", _entry.getKey(), _entry.getValue());
         });

        this._pipelineStep = this._jobProperties.getString(Conductor.CONFIG_PIPELINE_STEP);
        this._subst = ParamSubstitutor.createSubstitutor(this._jobProperties, this._jobId, this._pipelineStep);
        this._isDryRun = this._jobProperties.getBoolean(Conductor.CONFIG_DRY_RUN);
        this._pipelineRoot = this.getPropertyPath(Conductor.CONFIG_PIPELINE_ROOT, true);
        this._runType = RunType.valueOf(this._jobProperties.getString(Conductor.CONFIG_RUN_TYPE));
        
        String _condFlagStr = this._jobProperties.getString(Conductor.CONFIG_COND_FLAGS);
        if(_condFlagStr != null) {
            // convert conditional flags to EnumSet
            this._condFlags = EnumSet.copyOf(Arrays.asList(_condFlagStr.split(","))
                    .stream().map(CondFlags::valueOf).collect(Collectors.toList()));
        }

        boolean _useSSH = this._jobProperties.getBoolean("ssh.execute.enabled");
        if(_useSSH) {
            LOG.info("Configuring SSH process launcher.");
            this._launcher = new ProcessLauncherRemote(this);
        }
        else {
            this._launcher = new ProcessLauncherLocal(this);
        }
        
        this._csvAction = new CsvAction(this);
        this._sqlAction = new SqlAction(this);
        this._sparkAction = new SparkAction(this);
        this._scriptAction = new ScriptAction(this);
        this._validationAction = new ValidationAction(this);
    }
    

    private boolean executeStep(YmlAction _action, boolean _wasExecuted) throws Exception {
        
        if(this._pipelineStep != null) {
            boolean _matchStep = this._pipelineStep.equalsIgnoreCase(_action.name);
            
            if(this._runType == RunType.RESUME && _matchStep) {
                LOG.info("Resuming workflow at: {}", _action);
            }
            else if(this._runType == RunType.RESUME && _wasExecuted) {
                LOG.info("Continuing workflow at: {}", _action);
            }
            else if(this._runType == RunType.IMMEDIATE && _matchStep) {
                LOG.info("Matched workflow: {} = {}", _action, this._pipelineStep);
            }
            else {
                LOG.info("Skipping workflow action: {} != {}", _action);
                return false;
            }
        }
        else {
            LOG.info("Executing workflow action: {}", _action);
        }
        
        if(_action.executeIf != null && !isCondition(_action.executeIf)) {
            LOG.info("Skipping: execute-if != {}", _action.executeIf);
            return false;
        }
        
        if(_action.executeIfNot != null && isCondition(_action.executeIfNot)) {
            LOG.info("Skipping: execute-if-not = {}", _action.executeIfNot);
            return false;
        }
        
        if(_action instanceof YmlSqlAction) {
            this._sqlAction.execute(_action);
        }
        else if(_action instanceof YmlCsvAction) {
            this._csvAction.execute(_action);
        }
        else if(_action instanceof YmlSparkAction) {
            this._sparkAction.execute(_action);
        }
        else if(_action instanceof YmlScriptAction) {
            this._scriptAction.execute(_action);
        }
        else if(_action instanceof YmlValidationAction) {
        	this._validationAction.execute(_action);
        }
        else {
            throw new Exception("Unidentified action.");
        }
        
        return true;
    }
}
