package kinetica.orch;

import java.io.File;
import java.time.LocalDate;
import java.util.EnumSet;

import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.CommandLineParser;
import org.apache.commons.cli.DefaultParser;
import org.apache.commons.cli.HelpFormatter;
import org.apache.commons.cli.Option;
import org.apache.commons.cli.Options;
import org.apache.commons.cli.ParseException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import kinetica.orch.Conductor.RunType;
import kinetica.orch.YmlPipelineJob.CondFlags;

public class OrchCmdlineParser {

    private static final Logger LOG = LoggerFactory.getLogger(OrchCmdlineParser.class);

    public String _pipelineName = null; 
    public String _pipelineStep = null;
    public final EnumSet<CondFlags> _condFlags = EnumSet.noneOf(CondFlags.class);
    public File _propertiesFile;
    public boolean _isDryRun = false;
    public LocalDate _runDate = LocalDate.now();
    public LocalDate _startDate = null;
    public RunType _runType = null;
    
    //public boolean _isResume = false;
    //public boolean _isImmediate = true;
    
    public OrchCmdlineParser() { }

    private static Options getCmdlineOptions() {
        Options _cmdOptions = new Options();
        
        _cmdOptions.addOption(Option.builder("h")
                .longOpt("help")
                .desc("Show help options")
                .build());
        
        _cmdOptions.addOption(Option.builder("s")
                .longOpt("schedule")
                .desc("Trigger immediately vs. use cron expression from properties file.")
                .build());
        
        _cmdOptions.addOption(Option.builder()
                .longOpt("reprocess")
                .desc("Enable reprocess flag.")
                .build());
        
        _cmdOptions.addOption(Option.builder()
                .longOpt("init")
                .desc("Enable initialization flag.")
                .build());
        
        _cmdOptions.addOption(Option.builder()
                .longOpt("delete_data")
                .desc("Enable delete_data flag.")
                .build());
        
        _cmdOptions.addOption(Option.builder("d")
                .longOpt("dry-run")
                .desc("Do not execute loader or SQL programs.")
                .build());
        
        _cmdOptions.addOption(Option.builder()
                .longOpt("run-date")
                .desc("The date used for substitution of run-date. (optional)")
                .hasArg().argName(Conductor.DATE_FORMAT)
                .build());
        
        _cmdOptions.addOption(Option.builder()
                .longOpt("run-date-minus-days")
                .desc("Number of days to subtract from the run-date. (optional)")
                .hasArg().argName("integer days")
                .type(Number.class)
                .build());
        
        _cmdOptions.addOption(Option.builder()
                .longOpt("start-date")
                .desc("The date used for substitution of start-date. (optional)")
                .hasArg().argName(Conductor.DATE_FORMAT)
                .build());
        
        _cmdOptions.addOption(Option.builder("f")
                .longOpt("prop-file")
                .desc("Relative path to properties file from where jar file is executed.")
                .hasArg().argName("file")
                .type(File.class)
                .build());

        _cmdOptions.addOption(Option.builder("p")
                .longOpt("pipeline")
                .desc("Indicates the data pipeline with optional task.")
                .hasArg().argName("[pipeline].[task]")
                .build());
        
        _cmdOptions.addOption(Option.builder()
                .longOpt("resume")
                .desc("Resume pipeline from a given task.")
                .hasArg().argName("[pipeline].[task]")
                .build());
        
        return _cmdOptions;
    }
    

    /**
     * Use the apache command line parser. You can get a summary with the --help option.
     * @param args
     * @throws ParseException
     * @throws java.text.ParseException 
     */
    public void readCommandLineParameters(String[] args) throws ParseException {
        Options _cmdOptions = getCmdlineOptions();
        CommandLine _cmd;
        
        try {
            CommandLineParser _cmdParser = new DefaultParser();
            _cmd = _cmdParser.parse(_cmdOptions, args);
            
            if(_cmd.hasOption("help")) {
                throw new ParseException("Help requested.");
            }
            
            
            if(_cmd.hasOption("reprocess")) {
                this._condFlags.add(CondFlags.REPROCESS);
            }
            
            if(_cmd.hasOption("init")) {
                this._condFlags.add(CondFlags.INIT);
            }
            
            if(_cmd.hasOption("delete_data")) {
                this._condFlags.add(CondFlags.DELETE_DATA);
            }
            
            if(_cmd.hasOption("run-date")) {
                String _runDateStr = _cmd.getOptionValue("run-date");
                this._runDate = LocalDate.parse(_runDateStr, Conductor.DATE_FORMATTER);
            }
            
            if(_cmd.hasOption("start-date")) {
                String _startDateStr = _cmd.getOptionValue("start-date");
                this._startDate = LocalDate.parse(_startDateStr, Conductor.DATE_FORMATTER);
            }
            
            if(_cmd.hasOption("run-date-minus-days")) {
                Number _runDay = (Number)_cmd.getParsedOptionValue("run-date-minus-days");
                LocalDate _newDate = this._runDate.minusDays(_runDay.intValue());
                
                LOG.info("Calculating relative run-date: {} = {} - ({} days)", 
                        Conductor.DATE_FORMATTER.format(_newDate), 
                        Conductor.DATE_FORMATTER.format(this._runDate), _runDay);
                this._runDate = _newDate;
            }

            if(_cmd.hasOption("dry-run")) {
                this._isDryRun = true;
            }
            
            if(_cmd.hasOption("prop-file")) {
                this._propertiesFile = (File)_cmd.getParsedOptionValue("prop-file");
            }
            else {
                throw new ParseException("Properties file must be specified.");
            }

            if(_cmd.hasOption("pipeline")) {
                setRunType(RunType.IMMEDIATE);
                setPipeline(_cmd.getOptionValue("pipeline"));
            }
            
            if(_cmd.hasOption("resume")) {
                setRunType(RunType.RESUME);
                setPipeline(_cmd.getOptionValue("resume"));
                if(this._pipelineStep == null) {
                    throw new ParseException("Resume mode requires a pipeline step.");
                }
            }
            
            if(_cmd.hasOption("schedule")) {
                setRunType(RunType.SCHEDULE);
            }
            
            if(this._runType == null) {
                throw new ParseException("You must specify one of --schedule, --pipeline , or --resume.");
            }
            
        }
        catch(ParseException ex) {
            HelpFormatter formatter = new HelpFormatter();
            formatter.setWidth(100);
            formatter.printHelp("run-orch.sh", _cmdOptions);
            throw new ParseException(ex.getMessage());
        }
    }
    
    private void setRunType(RunType _type) throws ParseException {
        if(this._runType != null) {
            throw new ParseException("Run type can be only one of --schedule, --pipeline , or --resume.");
        }
        this._runType = _type;
    }
    
    private void setPipeline(String _param) throws ParseException {
        
        // convert to lower case
        String _name = _param.toLowerCase();

        String[] _split = _name.split("\\.");
        if(_split.length > 2) {
            throw new ParseException("Pipeline name must be <pipeline>.<task>");
        }
        else if(_split.length == 2) {
            this._pipelineName = _split[0];
            this._pipelineStep = _split[1];
        }
        else {
            this._pipelineName = _name;
            this._pipelineStep = null;
        }
    }
    
}
