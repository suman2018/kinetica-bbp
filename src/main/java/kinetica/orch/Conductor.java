package kinetica.orch;

import java.io.File;
import java.io.FileReader;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.Duration;
import java.time.Instant;
import java.time.format.DateTimeFormatter;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Properties;
import java.util.Set;
import java.util.stream.Collectors;

import org.quartz.CronScheduleBuilder;
import org.quartz.JobBuilder;
import org.quartz.JobDataMap;
import org.quartz.JobDetail;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.quartz.Scheduler;
import org.quartz.SchedulerException;
import org.quartz.Trigger;
import org.quartz.TriggerBuilder;
import org.quartz.TriggerKey;
import org.quartz.impl.StdSchedulerFactory;
import org.quartz.impl.matchers.EverythingMatcher;
import org.quartz.impl.matchers.GroupMatcher;
import org.quartz.listeners.JobListenerSupport;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.slf4j.MDC;

import kinetica.orch.YmlPipelineJob.CondFlags;

public class Conductor {

    public enum RunType { SCHEDULE, IMMEDIATE, RESUME };

    private static final Logger LOG = LoggerFactory.getLogger(Conductor.class);

    public static final String DATE_FORMAT = "yyyy-MM-dd";
    public static final DateTimeFormatter DATE_FORMATTER = DateTimeFormatter.ofPattern(DATE_FORMAT);
    public static final String CONFIG_PIPELINE_ROOT = "pipeline.root.path";
    public static final String CONFIG_PIPELINE_STEP = "pipeline.step";
    public static final String CONFIG_PIPELINE_NAME = "pipeline.name";
    public static final String CONFIG_DRY_RUN = "pipeline.dry-run";
    public static final String CONFIG_TABLE_PREFIX = "env.table-prefix";
    public static final String CONFIG_RUN_DATE = "pipeline.run-date";
    public static final String CONFIG_RUN_TYPE = "pipeline.run-type";
    public static final String CONFIG_START_DATE = "pipeline.start-date";
    public static final String CONFIG_YML_FILE = "pipeline.yml-file";
    public static final String CONFIG_COND_FLAGS = "pipeline.cond-flags";
    public static final String CONFIG_REPROCESS_DAYS = "pipeline.retention-days";
    public static final String CRON_PREFIX = "cronexpression.";
    public static final String CONFIG_UTILS_SCHEMA = "env.utils-schema";

    private final Map<String,String> _cronJobs = new HashMap<>();
    private final JobDataMap _jobProperties = new JobDataMap();
    //private final HashMap<String, Class<? extends Job>> _jobList = new HashMap<>();
    private final OrchCmdlineParser _config = new OrchCmdlineParser();
    protected JobExecutionException _immediateEx = null;
    
	private Path _pipelineRoot;
    private Scheduler _scheduler;

	/**
	 * @param args 
	 * 	String orchestrationMode [reprocess|schedule] - trigger immediately vs. use cronexpression 
	 *     from properties file
	 *  String keepData [keepdata|deletedata] - whether the source flat file should be 
	 *     removed from its folder on edge node or not
	 * 	String propertiesFile - relative path to properties file from where jar file is executed 
	 * 	String pipelineName [reference|revenue|scn|recharge] - optional: if orchestration mode 
	 *     is 'reprocess' then the pipeline name needs to be specified
	 * @throws SchedulerException 
	 */
	public static void main(String[] args)  {
        try {
            Conductor _inst = new Conductor();
            _inst.start(args);
        }
        catch (Throwable ex) {
            ex.printStackTrace();
            System.exit(1);
        }
	}
	
	public void start(String[] args) throws Throwable {
        // add job name to logging context
        MDC.put("job", "conductor");
        
        LOG.info("Conductor started.");
       
        this._config.readCommandLineParameters(args);
        setupProperties();
        
        // print properties to log
//        this._jobProperties.entrySet().stream()
//            .sorted((_e1, _e2) -> _e1.getKey().compareTo(_e2.getKey()))
//            .forEach(_entry -> {
//                LOG.info("PARAM: {} = {}", _entry.getKey(), _entry.getValue());
//         });
        
        this._scheduler = StdSchedulerFactory.getDefaultScheduler();
        
        if(this._config._runType == RunType.SCHEDULE) {
            for(Entry<String, String> _entry1: this._cronJobs.entrySet()) {
                String _pipelineName = _entry1.getKey();
                String _cronExpr = _entry1.getValue();
                scheduleCron(_pipelineName, _cronExpr);
            }
        }
        else {
            scheduleImmediate(this._config._pipelineName);
        }
        
        // install the listener
        OrchJobListener _listener = new OrchJobListener();
        this._scheduler.getListenerManager().addJobListener(_listener, EverythingMatcher.allJobs());

        // give the scheduler a second to launch immediate jobs
        this._scheduler.start();
        
        if(this._config._runType == RunType.SCHEDULE) {
            while(true) {
                LOG.info("Active triggers:");
                logTriggers();
                Thread.sleep(10*1000);
            }
        }
        else {
            Thread.sleep(1000);
            this._scheduler.shutdown(true);
            if(this._immediateEx != null) {
                // listener returned an exception
                throw this._immediateEx.getUnderlyingException();
            }
        }
        
        LOG.info("All done!");
	}
	
	private void logTriggers() throws SchedulerException {
	    DateFormat _formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss z");
	    
        for(String group: this._scheduler.getTriggerGroupNames()) {
            Set<TriggerKey> _triggerKeys =  this._scheduler.getTriggerKeys(GroupMatcher.groupEquals(group));
            
            // enumerate each trigger in group
            for(TriggerKey triggerKey : _triggerKeys) {
                Trigger _trigger = this._scheduler.getTrigger(triggerKey);
                
                Date _nextFire = _trigger.getNextFireTime();
                Duration _duration = Duration.between(Instant.now(), _nextFire.toInstant());
                LOG.info("Trigger <{}> will fire on <{}> in <{}>", triggerKey, 
                        _formatter.format(_nextFire), _duration);
            }
        }
	}

    private void setupProperties() throws Exception {
        
        // load properties file
        Properties _properties = new Properties();
        try (FileReader in = new FileReader(this._config._propertiesFile)) {
            _properties.load(in);
        } catch (Exception ex) {
            ex.printStackTrace();
            System.exit(1);
        } 
        
        // copy to the job properties
        _properties.forEach((_key, _val) -> {
            String _keyStr = (String)_key;
            String _valStr = (String)_val;
            
            if(_keyStr.startsWith(CRON_PREFIX)) {
                String _pipeline = _keyStr.substring(CRON_PREFIX.length());
                this._cronJobs.put(_pipeline, _valStr);
            }
            else {
                this._jobProperties.put(_keyStr, _valStr);
            }
        });
        
        // get pipeline root path
        String _pipelineRootStr = _properties.getProperty(CONFIG_PIPELINE_ROOT);
        if(_pipelineRootStr == null) {
            throw new Exception("Config property not found: " + CONFIG_PIPELINE_ROOT);
        }

        this._pipelineRoot = getPipelineRoot(this._config._propertiesFile, _pipelineRootStr);
        this._jobProperties.put(CONFIG_PIPELINE_ROOT, this._pipelineRoot.toString());
        this._jobProperties.put(CONFIG_RUN_TYPE, this._config._runType.toString());
        
        // set pipeline step if we have it
        if(this._config._pipelineStep != null) {
            this._jobProperties.put(CONFIG_PIPELINE_STEP, this._config._pipelineStep);
        }
        
        // set pipeline name if we have it
        if(this._config._pipelineName != null) {
            this._jobProperties.put(CONFIG_PIPELINE_NAME, this._config._pipelineName);
        }
        
        // save conditional flag list to a comma separated list
        if(this._config._condFlags.size() > 0) {
            List<String> _condFlagsList = this._config._condFlags.stream()
                    .map(CondFlags::toString)
                    .collect(Collectors.toList());
            String _condFlagsStr = String.join(",", _condFlagsList);
            this._jobProperties.put(CONFIG_COND_FLAGS, _condFlagsStr);
        }
        
        this._jobProperties.put(CONFIG_DRY_RUN, this._config._isDryRun);
        
        // set the run date
        String _runDateStr = DATE_FORMATTER.format(this._config._runDate);
        this._jobProperties.put(CONFIG_RUN_DATE, _runDateStr);
        
        // set the start date
        String _startDateStr = null;
        if(this._config._startDate != null) {
            _startDateStr = DATE_FORMATTER.format(this._config._startDate);
        }
        this._jobProperties.put(CONFIG_START_DATE, _startDateStr);
    }
    
    private static Path getPipelineRoot(File _propertiesFile, String _pipelineRoot) {
        String _parentDir = _propertiesFile.getParent();
        if(_parentDir == null) {
            _parentDir = "";
        }
        
        Path _configRoot = Paths.get(_parentDir);
        return Paths.get(_configRoot.toString(), _pipelineRoot).normalize();
    }
    
    private void scheduleImmediate(String _pipelineName) throws Exception {
        LOG.info("Scheduling immediate job <{}>", _pipelineName);
        Trigger _trigger = TriggerBuilder.newTrigger()
                .withIdentity(_pipelineName + "Trigger")
                .startNow()
                .build();
        
        scheduleJob(_pipelineName, _trigger);
    }
    
    private void scheduleCron(String _pipelineName, String _cronExpr) throws Exception {
        LOG.info("Scheduling pipeline <{}> with cron <{}>", _pipelineName, _cronExpr);
        
        Trigger _trigger = TriggerBuilder.newTrigger()
                .withIdentity(_pipelineName + "Trigger")
                .withSchedule(CronScheduleBuilder.cronSchedule(_cronExpr))
                .build();
        
        scheduleJob(_pipelineName, _trigger);
    }

    /**
     * Use immediate or scheduled trigger for testing or re-processing. 
     * @throws Exception 
     */
    private void scheduleJob(String _pipelineName, Trigger _trigger) throws Exception {
        
        Path _ymlFileName = Paths.get(_pipelineName, "pipeline.yml");
        Path _ymlFile = this._pipelineRoot.resolve(_ymlFileName);
        if(!_ymlFile.toFile().exists()) {
            throw new Exception("Could not find YML file: " + _ymlFile);
        }
        this._jobProperties.put(CONFIG_YML_FILE, _ymlFile.toString());

        JobDetail job = JobBuilder.newJob(YmlPipelineJob.class)
                .withIdentity(_pipelineName)
                .usingJobData(this._jobProperties)
                .build();

        this._scheduler.scheduleJob(job, _trigger);
    }
    
    public class OrchJobListener extends JobListenerSupport {
        @Override
        public String getName() {
            return this.getClass().getName();
        }
        
        @Override
        public void jobWasExecuted(JobExecutionContext context, JobExecutionException jobException) {
            Conductor.this._immediateEx = jobException;
        }
    }
}
