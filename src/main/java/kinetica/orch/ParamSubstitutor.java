package kinetica.orch;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;

import org.apache.commons.text.StringSubstitutor;
import org.apache.commons.text.lookup.StringLookup;
import org.quartz.JobDataMap;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ParamSubstitutor implements StringLookup {

    private static final Logger LOG = LoggerFactory.getLogger(ParamSubstitutor.class);
    private static final String VAR_PREFIX = "env.";
    private static final String KINETICA_PREFIX = "kinetica.";

    private final Map<String, Object> _configMap;
    
    private ParamSubstitutor(Map<String, Object> _configMap) {
        this._configMap = _configMap;
    }
    
    @Override
    public String lookup(String _key) {
        
        String _keyName = _key;
        String _dateFormat = null;
        
        // see if key has a date format
        if(_key.contains("=")) {
            String[] _split = _key.split("\\=");
            
            _keyName = _split[0];
            _dateFormat = _split[1];
        }
        
        Object _val = this._configMap.get(_keyName);
        if(_val == null) {
            return null;
        }
        
        if(_val instanceof LocalDate) {
            if(_dateFormat == null) {
                return null;
            }

            LocalDate _valDate = (LocalDate)_val;
            DateTimeFormatter _formatter = DateTimeFormatter.ofPattern(_dateFormat);
            return _formatter.format(_valDate);
        }
        
        return this._configMap.get(_keyName).toString();
    }

    public static StringSubstitutor createSubstitutor(JobDataMap _jobMap, String _jobId, String _stepName) throws Exception {
        Map<String, Object> _varMap = new HashMap<>();
        
        for(Entry<String,Object> _entry: _jobMap.entrySet()) {
            String _key = _entry.getKey();
            
            // add env vars
            if( _key.startsWith(VAR_PREFIX)) {
                String _sqlVar = _key.substring(VAR_PREFIX.length());
                Object _val = _entry.getValue();
                
                LOG.debug("Env var: <{} = {}>", _sqlVar, _val);
                _varMap.put(_sqlVar, _val);
            }
            
            // add Kinetica parameters
            if( _key.startsWith(KINETICA_PREFIX)) {
                String _sqlVar = _key.substring(KINETICA_PREFIX.length());
                Object _val = _entry.getValue();
                
                LOG.debug("Kinetica var: <{} = {}>", _sqlVar, _val);
                _varMap.put(_sqlVar, _val);
            }
        }
        
        // add job ID
        _varMap.put("job-id", _jobId);
        LOG.info(_jobMap.getString(Conductor.CONFIG_PIPELINE_NAME));

        // add pipeline name
        _varMap.put("pipeline-name", _jobMap.getString(Conductor.CONFIG_PIPELINE_NAME));
        
        // add pipeline step 
        _varMap.put("pipeline-step", _stepName);

        // add run-date
        LocalDate _runDate = putDate(_varMap, _jobMap, Conductor.CONFIG_RUN_DATE, "run-date");

        // add start-date
        putDate(_varMap, _jobMap, Conductor.CONFIG_START_DATE, "start-date");
        
        // add prev-month
        LocalDate _prevMonth = _runDate.minus(1, ChronoUnit.MONTHS);
        _varMap.put("prev-month", _prevMonth);

        // add month-minus-two
        LocalDate _monthMinusTwo = _runDate.minus(2, ChronoUnit.MONTHS);
        _varMap.put("month-minus-two", _monthMinusTwo);        

        // add month-minus-three
        LocalDate _monthMinusThree = _runDate.minus(3, ChronoUnit.MONTHS);
        _varMap.put("month-minus-three", _monthMinusThree);

        // add prev-year
        LocalDate _prevYear = _runDate.minus(12, ChronoUnit.MONTHS);
        _varMap.put("prev-year", _prevYear);

        // add prev-week
        LocalDate _prevWeek = _runDate.minus(1, ChronoUnit.WEEKS);
        _varMap.put("prev-week", _prevWeek);
        
        // add prev-day
        LocalDate _prevDay = _runDate.minus(1, ChronoUnit.DAYS);
        _varMap.put("prev-day", _prevDay);
        
        // add reprocess-date
        int _retentionDays = getReprocessDays(_jobMap);
        LocalDate _retentionDate = _runDate.minus(_retentionDays, ChronoUnit.DAYS);
        _varMap.put("retention-date", _retentionDate);
        
        ParamSubstitutor _lookup = new ParamSubstitutor(_varMap);
        return new StringSubstitutor(_lookup);
    }
    
    private static int getReprocessDays(JobDataMap _jobMap) {
       
        String _daysStr = _jobMap.getString(Conductor.CONFIG_REPROCESS_DAYS);
        if(_daysStr == null) {
            LOG.warn("Property '{}' is missing. Defaulting to 0.", Conductor.CONFIG_REPROCESS_DAYS);
            return 0;
        }
        
        return Integer.parseInt(_daysStr);
    }
    
    private static LocalDate putDate(Map<String, Object> _varMap, JobDataMap _jobMap, 
            String _jobKey, String _varKey) 
                    throws Exception {
        
        String _runDateStr = _jobMap.getString(_jobKey);
        if(_runDateStr == null) {
            return null;
        }
        
        LocalDate _runDate = LocalDate.parse(_runDateStr, Conductor.DATE_FORMATTER);
        _varMap.put(_varKey, _runDate);
        
        return _runDate;
    }
    
}