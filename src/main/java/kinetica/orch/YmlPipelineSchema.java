package kinetica.orch;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonSubTypes;
import com.fasterxml.jackson.annotation.JsonSubTypes.Type;
import com.fasterxml.jackson.annotation.JsonTypeInfo;
import com.fasterxml.jackson.annotation.JsonTypeInfo.Id;
import com.fasterxml.jackson.annotation.JsonTypeName;

public class YmlPipelineSchema {

    @JsonTypeInfo(use = Id.NAME,
            include = JsonTypeInfo.As.PROPERTY,
            property = "action")

    @JsonSubTypes({
        @Type(value = YmlSqlAction.class, name = "sql"),
        @Type(value = YmlCsvAction.class, name = "csv"),
        @Type(value = YmlSparkAction.class, name = "spark"),
        @Type(value = YmlScriptAction.class, name = "script"),
        @Type(value = YmlValidationAction.class, name = "validation"),
    })

    public abstract static class YmlAction {
        public String name;
        public String description;

        @JsonProperty("execute-if")
        public String executeIf;

        @JsonProperty("execute-if-not")
        public String executeIfNot;
        
        @Override
        public String toString() {
            return String.format("%s (%s)", this.name, this.description);
        }
    }

    @JsonTypeName("sql")
    public static class YmlSqlAction extends YmlAction {
        
        @JsonProperty("sql-script")
        public String sqlSript;
    }
    
    @JsonTypeName("csv")
    public static class YmlCsvAction extends YmlAction {
        
        public String properties;
        
        @JsonProperty("data-file")
        public String dataFile;
    }
    
    @JsonTypeName("spark")
    public static class YmlSparkAction extends YmlAction {
        @JsonProperty("sql-script")
        public String sqlSript;

        @JsonProperty("dest-table")
        public String destTable;
    }
    
    @JsonTypeName("script")
    public static class YmlScriptAction extends YmlAction {
        public String command;
    }

    @JsonTypeName("validation")
    public static class YmlValidationAction extends YmlAction {
        @JsonProperty("sql-script")
        public String sqlSript;
    }
    
    public String name;
    public String description;
    public YmlAction[] workflow;
    
    
}
