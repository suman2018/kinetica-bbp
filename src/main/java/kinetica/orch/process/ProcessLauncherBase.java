package kinetica.orch.process;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Path;
import java.util.List;

import org.slf4j.Logger;

public abstract class ProcessLauncherBase implements AutoCloseable {

    private final Path _workDir;
    private final String _logName;
    
    public ProcessLauncherBase(Path _workDir, String _logName) {
        this._workDir = _workDir;
        this._logName = _logName;
    }
    
    public Path getWorkDir() {
        return this._workDir;
    }
    
    public String getLogName() {
        return this._logName;
    }
    
    /**
     * Launch a process.
     * @param commandArgs
     * @throws Exception
     */
    abstract public void launchProcess(String[] commandArgs) throws Exception;
    
    /**
     * Optional override for path validation.
     * @param _file
     * @return
     */
    public abstract void assertPath(Path _file) throws Exception;
    
    /**
     * Expand wild card patterns to a list of files.
     * @param _path
     * @return
     * @throws Exception
     */
    public abstract List<Path> expandPath(Path _path) throws Exception;
    
    public abstract void setEnv(String _var, String _value);
    
    public abstract String copyTempFile(InputStream _is, String _suffix) throws Exception;
    
    public static void logOutput(BufferedReader _reader, Logger _logger) throws IOException {
        String _line = null;
        
        while ((_line = _reader.readLine()) != null) {
            // strip control chars
            _line = _line.replaceAll("\\p{Cntrl}\\[.*?m", "");
            if(_line.isEmpty()) {
                continue;
            }
            _logger.info(_line);
        }
    }
}
