package kinetica.orch.process;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.PipedInputStream;
import java.io.PipedOutputStream;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Hashtable;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.jcraft.jsch.Channel;
import com.jcraft.jsch.ChannelExec;
import com.jcraft.jsch.ChannelSftp;
import com.jcraft.jsch.JSch;
import com.jcraft.jsch.JSchException;
import com.jcraft.jsch.Session;

import kinetica.orch.YmlPipelineJob;

public class ProcessLauncherRemote extends ProcessLauncherBase {

    private static final Logger LOG = LoggerFactory.getLogger(ProcessLauncherRemote.class);
    private static final JSch JSCH = new JSch();

    private final String _user;
    private final String _password;
    private final String _host;
    
    private final boolean _jumpEnabled;
    private final String _jumpUser;
    private final String _jumpPassword;
    private final String _jumpHost;
    
    private final Hashtable<String, String> _sshConfig = new Hashtable<>();
    private final Hashtable<String, String> _exportVars = new Hashtable<>();

    private Session _jumpSession = null;
    private Session _session = null;
    
    public ProcessLauncherRemote(YmlPipelineJob _job) throws Exception {
        super(_job.getPropertyPath("ssh.execute.path", false), _job.getLogName());
        
        this._host = _job.getProperty("ssh.execute.host");
        this._user = _job.getProperty("ssh.execute.user");
        this._password = _job.getProperty("ssh.execute.password");
        this._jumpEnabled = _job.getPropertyBool("ssh.jump.enabled");
        
        if(this._jumpEnabled) {
            LOG.info("Configuring jump host...");
            this._jumpHost = _job.getProperty("ssh.jump.host");
            this._jumpUser = _job.getProperty("ssh.jump.user");
            this._jumpPassword = _job.getProperty("ssh.jump.password");
        }
        else {
            this._jumpHost = null;
            this._jumpUser = null;
            this._jumpPassword = null;
        }

        this._sshConfig.put("StrictHostKeyChecking", "no");
    }
    
    @Override
    public void setEnv(String _var, String _value) {
        this._exportVars.put(_var, _value);
    }
    
    @Override
    public void close() {
        LOG.info("Closing SSH session.");
        if(this._session != null) {  this._session.disconnect(); }
        if(this._jumpSession != null) {  this._jumpSession.disconnect(); }
    }

    @Override
    public List<Path> expandPath(Path _path) throws Exception {
        // passthru: remote expansion is not supported.
        return Arrays.asList(_path);
    }

    @Override
    public void assertPath(Path _file) throws Exception {
        // passthru: remote assertion is not supported.
    }

    @Override
    public void launchProcess(String[] _cmdArgs) throws Exception {
        
        ChannelExec _execChannel = (ChannelExec)openChannel("exec");
        String _command = formatCommand(_cmdArgs);
        LOG.info("running remote command: {}", _command);

        try(InputStream _iStream = _execChannel.getInputStream();
            PipedOutputStream _oStream = new PipedOutputStream();
            PipedInputStream _iStreamErr = new PipedInputStream(_oStream);
            BufferedReader _reader = new BufferedReader(new InputStreamReader(_iStream));
            BufferedReader _readerErr = new BufferedReader(new InputStreamReader(_iStreamErr));
           ) {
        
            // prepare the error stream so we get error messages
            _execChannel.setCommand(_command);
            _execChannel.setErrStream(_oStream);
            _execChannel.connect();
            
            while(true) {
                // if we don't check for availale this can hang
                if(_iStream.available() > 0) logOutput(_reader, LOG);
                
                // check for error output
                logOutput(_readerErr, LOG);
                
                if(_execChannel.isClosed()) {
                    break;
                }
                Thread.sleep(1000);
                LOG.debug("Waiting on SSH output...");
            }
            
            // log anything we might be missing
            logOutput(_reader, LOG);
            logOutput(_readerErr, LOG);

            int _exitCode = _execChannel.getExitStatus();
            if (_exitCode > 0) {
                throw new Exception(String.format("Remote commaned exited with <%d>: %s", 
                                _exitCode, _cmdArgs));
            }
        }
        finally {
            _execChannel.disconnect();
        }
    }
    
    @Override
    public String copyTempFile(InputStream _is, String _suffix) throws Exception {
        ChannelSftp _sftpChannel = (ChannelSftp)openChannel("sftp");
        String _destFile = String.format("/tmp/%s_%s", getLogName(), _suffix);
        
        try {
            _sftpChannel.connect();
            LOG.info("Copy to remote: {}", _destFile);
            _sftpChannel.put(_is, _destFile);
        }
        finally {
            _sftpChannel.disconnect();
        }
        
        return _destFile;
    }
    
    private Channel openChannel(String _type) throws JSchException {
        if(this._session == null) {
            // open the session
            if(this._jumpEnabled) {
                this._jumpSession = createSession(this._jumpHost, this._jumpUser, this._jumpPassword);
                this._session = jumpSession(this._jumpSession, this._host, this._user, this._password);
            }
            else {
                this._session = createSession(this._host, this._user, this._password);
            }
        }
        
        return this._session.openChannel(_type);
    }
    
    private String formatCommand(String[] _cmdArgs) {
        List<String> _finalArgs = new ArrayList<String>();
        
        // change directory
        _finalArgs.add(String.format("cd %s &&", this.getWorkDir().toString()));
        
        // add env vars
        this._exportVars.forEach((_key, _val) -> {
            _finalArgs.add(String.format("%s=%s", _key, _val));
        });
        
        // add command line params
        _finalArgs.addAll(Arrays.asList(_cmdArgs));
        
        return String.join(" ", _finalArgs);
    }
    
    private Session jumpSession(Session _sesion, String _host, String _user, String _pass) 
            throws JSchException {
        LOG.info("Opening jump to: {}@{}", _user, _host);
        
        int _assignedPort = _sesion.setPortForwardingL(0, _host, 22);
        
        Session _newSession = JSCH.getSession(_user, "127.0.0.1", _assignedPort);
        _newSession.setConfig(this._sshConfig);
        _newSession.setPassword(_pass);
        _newSession.connect();
        
        return _newSession;
    }

    private Session createSession(String _host, String _user, String _pass) 
            throws JSchException {
        LOG.info("Opening session to: {}@{}", _user, _host);
        
        Session _session = JSCH.getSession(_user, _host, 22);
        _session.setConfig(this._sshConfig);
        _session.setPassword(_pass);
        //session.setTimeout(10000);
        _session.connect();
        
        return _session;
    }
}
