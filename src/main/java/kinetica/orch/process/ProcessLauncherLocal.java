package kinetica.orch.process;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.file.DirectoryStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.StandardCopyOption;
import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import kinetica.orch.YmlPipelineJob;

public class ProcessLauncherLocal extends ProcessLauncherBase {

	private static final Logger LOG = LoggerFactory.getLogger(ProcessLauncherLocal.class);
	
	public ProcessLauncherLocal(YmlPipelineJob _job) {
	    super(_job.getPipelineRoot(), _job.getLogName());
	}
	
	@Override
	public void assertPath(Path _path) throws Exception {
	    Path _fullPath = this.getWorkDir().resolve(_path);
	    
        if(!_fullPath.toFile().exists()) {
            throw new Exception(String.format("File not found: %s", _fullPath));
        }
	}
	
    @Override
	public void launchProcess(String[] _cmdArgs) throws Exception {
        ProcessBuilder _pb = new ProcessBuilder(_cmdArgs);
        _pb.directory(this.getWorkDir().toFile());
        _pb.redirectErrorStream(true);
        
        Process _process = _pb.start();
        try(InputStream _is = _process.getInputStream();
            BufferedReader _reader = new BufferedReader(new InputStreamReader(_is))
            )
        {
            logOutput(_reader, LOG);
            _process.waitFor();
        }
        
        if(_process.exitValue() != 0) {
            String _command = String.join(" ", _cmdArgs);
            throw new Exception("Falure launching process: " + _command);
        }
	}

    @Override
    public List<Path> expandPath(Path _path) throws Exception {
        Path _fileName = _path.getFileName();
        Path _fileDir = _path.getParent();
        _fileDir = this.getWorkDir().resolve(_fileDir);
        _fileDir = _fileDir.toAbsolutePath().normalize();
        
        List<Path> _pathFiles = new ArrayList<>();
        try (DirectoryStream<Path> dirStream = Files.newDirectoryStream(
                _fileDir, _fileName.toString())) {
            for(Path _file : dirStream) {
                _pathFiles.add(_file);
            }
        }
        
        return _pathFiles;
    }

    @Override
    public void setEnv(String _var, String _value) {
        // not implimented
    }

    @Override
    public void close() throws Exception {
        // not implimented
    }

    @Override
    public String copyTempFile(InputStream _is, String _suffix) throws Exception {
        Path _tmpFile = Files.createTempFile(getLogName(), _suffix);
        
        LOG.info("Creating file: {}", _tmpFile);
        Files.copy(_is, _tmpFile, StandardCopyOption.REPLACE_EXISTING);
        
        return _tmpFile.toString();
    }
}
