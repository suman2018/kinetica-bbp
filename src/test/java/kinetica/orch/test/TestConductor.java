package kinetica.orch.test;

import org.junit.Test;

import kinetica.orch.Conductor;

public class TestConductor {
    
	private static final String LOCAL_PROPS = "scripts/conductor-local.properties";
    private static final String SSH_PROPS = "scripts/conductor.properties";
    
    @Test
    public void testLaunchSchedule() throws Throwable {
        String[] _args = {
                "--prop-file", LOCAL_PROPS,
                "--schedule"
        };
        new Conductor().start(_args);
    }
    
    @Test
    public void testSqlAction() throws Throwable {
        String[] _args = {
                "--prop-file", LOCAL_PROPS,
                "--pipeline", "test.create_test",
                "--run-date", "2018-10-19"
        };
        new Conductor().start(_args);
    }
    
    @Test
    public void testShowHelp() throws Throwable {
            String[] _args = { "-h" };
            new Conductor().start(_args);
    }
}
