package kinetica.orch.test;

import org.junit.Test;

import kinetica.orch.Conductor;

public class TestHVC {

	public TestHVC() {
		// TODO Auto-generated constructor stub
	}
	
	private static final String LOCAL_PROPS = "scripts/conductor-uc02-local.properties";
    private static final String SSH_PROPS = "scripts/conductor-uc02-ssh.properties";
    
    @Test
    public void testInitHVC() throws Throwable {
        String[] _args = {
                "--prop-file", LOCAL_PROPS,
                "--pipeline", "hvc_initialization",
                "--run-date", "2018-10-19"
        };
        new Conductor().start(_args);
    }
    
    @Test
    public void testMasterHVC() throws Throwable {
        String[] _args = {
                "--prop-file", LOCAL_PROPS,
                "--pipeline", "hvc_master",
                "--run-date", "2018-10-19"
        };
        new Conductor().start(_args);
    }
}
