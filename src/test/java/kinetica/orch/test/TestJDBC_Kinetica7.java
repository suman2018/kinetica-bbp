package kinetica.orch.test;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.Statement;
import java.util.Properties;

import org.junit.Test;

import com.kinetica.jdbc.Driver;

public class TestJDBC_Kinetica7 {
    
    @Test 
    public void testSelect() throws Exception {
        final Driver Kinetica7Driver = new Driver();
        String _url = String.format("jdbc:kinetica://%s:%d", "192.168.122.107", 9191);
        Properties _props = new Properties();
        _props.put("UID", "admin");
        _props.put("PWD", "Kinetica1!");
        String _sqlText = "select 1 from pipeline_log where 1 = 0";
        
        try (Connection _conn = Kinetica7Driver.connect(_url, _props)) {
            try (Statement _stmt = _conn.createStatement()) {
                
                try {
                ResultSet resultSet = _stmt.executeQuery(_sqlText);
                ResultSetMetaData rsmd = resultSet.getMetaData();
                while (resultSet.next()) {
                    String columnValue = resultSet.getString(1);
                    System.out.print(columnValue + " " + rsmd.getColumnName(1));
                    System.out.println("");
                }
                
                }
                catch(Exception _ex) {
                    _ex.printStackTrace();
                }
            }
            catch(Exception _ex) {
                _ex.printStackTrace();
            }
        }
    }
}
