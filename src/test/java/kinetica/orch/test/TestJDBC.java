package kinetica.orch.test;

import java.sql.Connection;
import java.sql.Statement;
import java.util.Properties;

import org.junit.Test;

import com.simba.client.core.jdbc4.SCJDBC4Driver;

public class TestJDBC {
    
    @Test 
    public void testSelect() throws Exception {
        final SCJDBC4Driver DRIVER = new SCJDBC4Driver();
        String _url = String.format("jdbc:simba://%s:%d", "192.168.122.106", 9292);
        Properties _props = new Properties();
        _props.put("UID", "admin");
        _props.put("PWD", "Kinetica1!");
        String _sqlText = "select current_date";
        
        try (Connection _conn = DRIVER.connect(_url, _props)) {
            try (Statement _stmt = _conn.createStatement()) {
                
                try {
                _stmt.execute(_sqlText);
                }
                catch(Exception _ex) {
                    _ex.printStackTrace();
                }
            }
        }
    }
}
