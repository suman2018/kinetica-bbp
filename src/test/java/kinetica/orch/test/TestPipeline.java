package kinetica.orch.test;

import org.junit.Test;

import kinetica.orch.Conductor;

public class TestPipeline {
    private static final String LOCAL_PROPS = "scripts/conductor-uc02-local.properties";
    private static final String SSH_PROPS = "scripts/conductor-uc02-ssh.properties";
    
    @Test
    public void testHelp() throws Throwable {
        String[] _args = {
                "--prop-file", LOCAL_PROPS,
                "--help"
        };
        new Conductor().start(_args);
    }
    
    @Test
    public void testPipeline() throws Throwable {
        String[] _args = {
                "--prop-file", LOCAL_PROPS,
                "--pipeline", "test",
                "--delete_data"
        };
        new Conductor().start(_args);
    }
    
    @Test
    public void testScript() throws Throwable {
        String[] _args = {
                "--prop-file", LOCAL_PROPS,
                "--pipeline", "test.test_script",
        };
        new Conductor().start(_args);
    }
    
    @Test
    public void testSparkSmall() throws Throwable {
        String[] _args = {
                "--prop-file", SSH_PROPS,
                "--pipeline", "test.spark_test_small"
        };
        new Conductor().start(_args);
    }
    
    @Test
    public void testSparkLarge() throws Throwable {
        String[] _args = {
                "--prop-file", SSH_PROPS,
                "--pipeline", "test.spark_test_large"
        };
        new Conductor().start(_args);
    }
    
    @Test
    public void testLoadMultipart() throws Throwable {
        String[] _args = {
                "--prop-file", LOCAL_PROPS,
                "--pipeline", "test.test_multipart"
        };
        new Conductor().start(_args);
    }
    
    @Test
    public void testLoadSingle() throws Throwable {
        String[] _args = {
                "--prop-file", LOCAL_PROPS,
                "--pipeline", "test.test_single"
        };
        new Conductor().start(_args);
    }
    
    @Test
    public void testResultError() throws Throwable {
        String[] _args = {
                "--prop-file", LOCAL_PROPS,
                "--pipeline", "test.result_test"
        };
        new Conductor().start(_args);
    }
    
    @Test
    public void testResume() throws Throwable {
        String[] _args = {
                "--prop-file", LOCAL_PROPS,
                "--resume", "test.delete_test",
                "--start-date", "2018-01-01"
        };
        new Conductor().start(_args);
    }
    
    @Test
    public void testDateFormat() throws Throwable {
        String[] _args = {
                "--prop-file", LOCAL_PROPS,
                "--pipeline", "test.date_format",
                "--start-date", "2018-01-01",
                "--run-date-minus-days", "3"
        };
        new Conductor().start(_args);
    }
}
