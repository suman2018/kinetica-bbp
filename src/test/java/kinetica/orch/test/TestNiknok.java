package kinetica.orch.test;

import org.junit.Test;

import kinetica.orch.Conductor;

public class TestNiknok {
    private static final String LOCAL_PROPS = "scripts/conductor-uc02-local.properties";
    private static final String SSH_PROPS = "scripts/conductor-uc02-ssh.properties";
    
    @Test
    public void testNikNokkPipeline() throws Throwable {
        String[] _args = {
                "--prop-file", LOCAL_PROPS,
                "--pipeline", "niknok",
                "--run-date", "2018-11-30"
        };
        new Conductor().start(_args);
    }
    
    @Test
    public void testNikNokkInit() throws Throwable {
        String[] _args = {
                "--prop-file", LOCAL_PROPS,
                "--pipeline", "niknok.niknokk_init"
        };
        new Conductor().start(_args);
    }
    
    @Test
    public void testLoadCbPrePaidPostpaidStg() throws Throwable {
        String[] _args = {
                "--prop-file", LOCAL_PROPS,
                "--pipeline", "niknok.load-sandbox_cb_prepaid_postpaid_stg",
                "--run-date", "2018-11-30"
        };
        new Conductor().start(_args);
    }
  
    
    @Test
    public void testLoadReinaStg() throws Throwable {
        String[] _args = {
                "--prop-file", LOCAL_PROPS,
                "--pipeline", "niknok.load-reina_rgstrtn_pre_dd_stg",
                "--run-date", "2018-12-11"
        };
        new Conductor().start(_args);
    }
  
    
    @Test
    public void testLoadMultidimStg() throws Throwable {
        String[] _args = {
                "--prop-file", LOCAL_PROPS,
                "--pipeline", "niknok.load-multidim_stg",
                "--run-date", "2018-11-30"
        };
        new Conductor().start(_args);
    };
      
    @Test
    public void testLoadChurnPreStg() throws Throwable {
        String[] _args = {
                "--prop-file", LOCAL_PROPS,
                "--pipeline", "niknok.load-churn_pre_stg",
                "--run-date", "2018-12-11"
        };
        new Conductor().start(_args);
    }
    
    @Test
    public void testLoadChurnPre() throws Throwable {
        String[] _args = {
                "--prop-file", LOCAL_PROPS,
                "--pipeline", "niknok.insert-niknokk_load_churn_pre",
                "--run-date", "2018-12-11"
        };
        new Conductor().start(_args);
    }    

 
    @Test
    public void testLoadSalesPreStg() throws Throwable {
        String[] _args = {
                "--prop-file", LOCAL_PROPS,
                "--pipeline", "niknok.load-sales_pre_stg",
                "--run-date", "2018-12-11"
        };
        new Conductor().start(_args);
    }
    
    
    
    @Test
    public void testLoadDukcapilStg() throws Throwable {
        String[] _args = {
                "--prop-file", LOCAL_PROPS,
                "--pipeline", "niknok.load-ref_dukcapil_nik_map"
        };
        new Conductor().start(_args);
    }
    
    
    @Test
    public void testLoadMultidim() throws Throwable {
        String[] _args = {
                "--prop-file", LOCAL_PROPS,
                "--pipeline", "niknok.insert-niknokk_load_multidim",
                "--run-date", "2018-11-30"
        };
        new Conductor().start(_args);
    }

    @Test
    public void testLoadSalesPre() throws Throwable {
        String[] _args = {
                "--prop-file", LOCAL_PROPS,
                "--pipeline", "niknok.insert-niknokk_load_sales_pre",
                "--run-date", "2018-12-11"
        };
        new Conductor().start(_args);
    }    

    
 

    @Test
    public void testLoadCbPrepaidPostpaid() throws Throwable {
        String[] _args = {
                "--prop-file", LOCAL_PROPS,
                "--pipeline", "niknok.insert-niknokk_load_cb_prepaid_postpaid",
                "--run-date", "2018-11-20"
        };
        new Conductor().start(_args);
    }
    
    @Test
    public void testLoadPreblkCcmClnBaru() throws Throwable {
        String[] _args = {
                "--prop-file", LOCAL_PROPS,
                "--pipeline", "niknok.insert-niknokk_load_preblk_ccm_cln_baru"
        };
        new Conductor().start(_args);
    }

    @Test
    public void testInsertPreRegMonthly() throws Throwable {
        String[] _args = {
                "--prop-file", LOCAL_PROPS,
                "--pipeline", "niknok.insert-niknokk_prereg_monthly",
                "--run-date", "2018-11-30"
        };
        new Conductor().start(_args);
    }

    @Test
    public void testInsertPreRegDaily() throws Throwable {
        String[] _args = {
                "--prop-file", LOCAL_PROPS,
                "--pipeline", "niknok.insert-niknokk_prereg_daily",
                "--run-date", "2018-11-30"
        };
        new Conductor().start(_args);
    }

    @Test
    public void testInsertChurnReg() throws Throwable {
        String[] _args = {
                "--prop-file", LOCAL_PROPS,
                "--pipeline", "niknok.insert-niknokk_churn_reg",
                "--run-date", "2018-11-30"
        };
        new Conductor().start(_args);
    }


    @Test
    public void testInsertSum_NikNok01_MSISDN_Prepaid_Reg() throws Throwable {
        String[] _args = {
                "--prop-file", LOCAL_PROPS,
                "--pipeline", "niknok.insert-niknokk_01_msisdn_prepaid_reg",
                "--run-date", "2018-11-30"
        };
        new Conductor().start(_args);
    }
    
    
    @Test
    public void testInsertSum_NikNok02_NIK_Prepaid_Reg() throws Throwable {
        String[] _args = {
                "--prop-file", LOCAL_PROPS,
                "--pipeline", "niknok.insert-niknokk_02_nik_prepaid_reg",
                "--run-date", "2018-11-30"
        };
        new Conductor().start(_args);
    }

    @Test
    public void testInsertSum_NikNok03_Churn_MSISDN_NIK() throws Throwable {
        String[] _args = {
                "--prop-file", LOCAL_PROPS,
                "--pipeline", "niknok.insert-niknokk_03_churn_msisdn_nik",
                "--run-date", "2018-11-30"
        };
        new Conductor().start(_args);
    }

    @Test
    public void testInsertSum_NikNok04_Waterfall_Churn() throws Throwable {
        String[] _args = {
                "--prop-file", LOCAL_PROPS,
                "--pipeline", "niknok.insert-niknokk_04_waterfall_churn",
                "--run-date", "2018-11-30"
        };
        new Conductor().start(_args);
    }

    @Test
    public void testInsertSum_NikNok05_Segment_NIK_Churn() throws Throwable {
        String[] _args = {
                "--prop-file", LOCAL_PROPS,
                "--pipeline", "niknok.insert-niknokk_05_segment_nik_churn",
                "--run-date", "2018-11-30"
        };
        new Conductor().start(_args);
    }

    
    @Test
    public void testInsertSum_NikNok06_Segment_NIK_All() throws Throwable {
        String[] _args = {
                "--prop-file", LOCAL_PROPS,
                "--pipeline", "niknok.insert-niknokk_06_segment_nik_all",
                "--run-date", "2018-11-30"
        };
        new Conductor().start(_args);
    }

    
    @Test
    public void testInsertSum_NikNok07_Profile_Churn_NIK_Regchnnl() throws Throwable {
        String[] _args = {
                "--prop-file", LOCAL_PROPS,
                "--pipeline", "niknok.insert-niknokk_07_profile_churn_nik_regchnnl",
                "--run-date", "2018-11-30"
        };
        new Conductor().start(_args);
    }

    
    @Test
    public void testInsertSum_NikNok07_Profile_Churn_NIK_Age() throws Throwable {
        String[] _args = {
                "--prop-file", LOCAL_PROPS,
                "--pipeline", "niknok.insert-niknokk_07_profile_churn_nik_age",
                "--run-date", "2018-11-30"
        };
        new Conductor().start(_args);
    }

    @Test
    public void testInsertSum_NikNok08_Churn_Segment() throws Throwable {
        String[] _args = {
                "--prop-file", LOCAL_PROPS,
                "--pipeline", "niknok.insert-niknokk_08_churn_segment",
                "--run-date", "2018-11-30"
        };
        new Conductor().start(_args);
    }

    
    @Test
    public void testInsertSum_NikNok_Cohort_Family_Acq() throws Throwable {
        String[] _args = {
                "--prop-file", LOCAL_PROPS,
                "--pipeline", "niknok.insert-niknokk_cohort_family_acq",
                "--run-date", "2018-11-30"
        };
        new Conductor().start(_args);
    }

    @Test
    public void testInsertSum_NikNok_Summmary1_Family_Acq() throws Throwable {
        String[] _args = {
                "--prop-file", LOCAL_PROPS,
                "--pipeline", "niknok.insert-niknokk_summary1_family_acq",
                "--run-date", "2018-11-30"
        };
        new Conductor().start(_args);
    }

    @Test
    public void testInsertSum_NikNok_Summmary2_Family_Acq() throws Throwable {
        String[] _args = {
                "--prop-file", LOCAL_PROPS,
                "--pipeline", "niknok.insert-niknokk_summary2_family_acq",
                "--run-date", "2018-11-30"
        };
        new Conductor().start(_args);
    }

}
