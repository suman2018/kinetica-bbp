
package kinetica.orch.test;

//import java.sql.Date;
//import java.sql.Timestamp;
//import java.time.LocalDate;
//import java.time.LocalDateTime;
//import java.util.ArrayList;
//import java.util.List;
//
//import org.apache.spark.sql.Dataset;
//import org.apache.spark.sql.Row;
//import org.apache.spark.sql.RowFactory;
//import org.apache.spark.sql.SparkSession;
//import org.apache.spark.sql.types.DataTypes;
//import org.apache.spark.sql.types.StructField;
//import org.apache.spark.sql.types.StructType;
//import org.junit.Test;
//import org.slf4j.Logger;
//import org.slf4j.LoggerFactory;

//import com.kinetica.spark.SparkKineticaDriver;
//import com.thedeanda.lorem.Lorem;

public class TestSparkLoader {

//    private static final Logger LOG = LoggerFactory.getLogger(TestSparkLoader.class);
//
//    @Test
//    public void testWriteParquet() {
//            try(SparkSession sess = SparkSession.builder()
//                .master("local")
//                .appName(SparkKineticaDriver.class.getName())
//                .getOrCreate() )
//        {
//            Dataset<Row> df = sess.read().json("src/test/resources/data/employees.json");
//            df.write().format("parquet")
//                    .save("src/test/resources/data/employees-parquet");
//        }
//    }
//    
//    @Test
//    public void testImportParquetJob() {
//        LOG.info("Submit Parquet file");
//        try (SparkSession sess = SparkSession.builder()
//                .master("local")
//                .appName(SparkKineticaDriver.class.getName())
//                .enableHiveSupport()
//                .getOrCreate())
//        {
//            String[] args = new String[] { "src/main/resources/spark-properties/parquet-test.properties" };
//            new SparkKineticaDriver(args).start(sess);
//        }
//    }
//    
//    @Test
//    public void testImportAvroJob() throws Exception {
//        LOG.info("Submit AVRO file");
//        try (SparkSession sess = SparkSession.builder()
//                .master("local")
//                .appName(SparkKineticaDriver.class.getName())
//                .enableHiveSupport()
//                .getOrCreate())
//        {
//            String[] args = new String[] { "src/main/resources/spark-properties/avro-test.properties" };
//            new SparkKineticaDriver(args).start(sess);
//        }
//    }
//    
//    @Test
//    public void testWriteAvro() {
//            try(SparkSession sess = SparkSession.builder()
//                .master("local")
//                .appName(SparkKineticaDriver.class.getName())
//                .getOrCreate() )
//        {
//            // create a dataset with 1000 rows.
//            Dataset<Row> df = sess.read().json("src/test/resources/data/employees.json");
//
//            sess.sqlContext().setConf("spark.sql.avro.compression.codec", "uncompressed");
//            df.write().format("com.databricks.spark.avro")
//                    .save("src/test/resources/employees-avro");
//        }
//    }
//    
//    @Test
//    public void testCreateTable() throws Exception {
//        try(SparkSession sess = SparkSession.builder()
//                .master("local")
//                .appName(SparkKineticaDriver.class.getName())
//                .enableHiveSupport()
//                .getOrCreate() )
//        {
//            Dataset<Row> df = createTestTable(sess);
//            df.createGlobalTempView("test_table");
//            sess.sql("DROP TABLE IF EXISTS default.sample_table");
//            sess.sql("CREATE TABLE default.sample_table AS SELECT * FROM global_temp.test_table");
//        }
//    }
//    
//    public static Dataset<Row> createTestTable(SparkSession spark) {
//
//        List<StructField> fields = new ArrayList<>();
//        fields.add(DataTypes.createStructField("test_name", DataTypes.StringType, true));
//        fields.add(DataTypes.createStructField("test_timestamp", DataTypes.TimestampType, true));
//        fields.add(DataTypes.createStructField("test_double", DataTypes.DoubleType, true));
//        fields.add(DataTypes.createStructField("test_long", DataTypes.LongType, true));
//        fields.add(DataTypes.createStructField("test_date", DataTypes.DateType, true));
//        StructType schema = DataTypes.createStructType(fields);
//
//        ArrayList<Row> rows = new ArrayList<>();
//        Timestamp nowTs = Timestamp.valueOf(LocalDateTime.now());
//        Date nowDt = Date.valueOf(LocalDate.now());
//        rows.add(RowFactory.create(Lorem.getName(), nowTs, 1.5D, 55L, nowDt));
//        rows.add(RowFactory.create(Lorem.getName(), null, null, null, null));
//        rows.add(RowFactory.create(Lorem.getName(), nowTs, 2.5D, 66L, nowDt));
//
//        return spark.createDataFrame(rows, schema);
//    }
}
