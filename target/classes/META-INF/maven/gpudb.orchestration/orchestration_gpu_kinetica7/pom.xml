<project xmlns="http://maven.apache.org/POM/4.0.0" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
    xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/maven-v4_0_0.xsd">

    <modelVersion>4.0.0</modelVersion>
    <groupId>gpudb.orchestration</groupId>
    <artifactId>orchestration_gpu_kinetica7</artifactId>
    <packaging>jar</packaging>
    <version>v0.2</version>
    <name>orchestration_gpu_kinetica7</name>
    <url>http://maven.apache.org</url>

    <properties>
        <maven.compiler.source>1.8</maven.compiler.source>
        <maven.compiler.target>1.8</maven.compiler.target>
        <spark.version>2.2.0</spark.version>
        <jackson.version>2.6.5</jackson.version>
        <slf4j.version>1.7.25</slf4j.version>
    </properties>

    <repositories>
        <repository>
            <id>kinetica-releases</id>
            <name>Kinetica Releases</name>
            <url>http://files.kinetica.com/nexus/content/repositories/releases/</url>
        </repository>
        <repository>
            <id>kinetica-snapshots</id>
            <name>Kinetica Snapshots</name>
            <url>http://files.kinetica.com/nexus/content/repositories/snapshots/</url>
        </repository>
        <repository>
            <id>kinetica-thirdparty</id>
            <name>Kinetica Thirdparty</name>
            <url>http://files.kinetica.com/nexus/content/repositories/thirdparty</url>
        </repository>
    </repositories>

    <dependencies>
    
        <!-- Kinetica dependencies -->
        
        <dependency>
                <groupId>com.kinetica</groupId>
                <artifactId>kinetica-jdbc</artifactId>
                <version>7.0.10.0</version>
                <classifier>jar-with-dependencies</classifier>
        </dependency>
                
        <dependency>
            <groupId>com.simba.client.core.jdbc4</groupId>
            <artifactId>SCJDBC4Driver</artifactId>
            <version>10.1.4</version>
        </dependency>
        
        <!--  logging  -->
        <dependency>
            <groupId>org.slf4j</groupId>
            <artifactId>slf4j-api</artifactId>
            <version>${slf4j.version}</version>
        </dependency>
        <!-- 
        <dependency>
            <groupId>org.slf4j</groupId>
            <artifactId>slf4j-log4j12</artifactId>
            <version>${slf4j.version}</version>
        </dependency>
         -->
        <dependency>
            <groupId>ch.qos.logback</groupId>
            <artifactId>logback-classic</artifactId>
            <version>1.2.3</version>
        </dependency>
        
        
        <!-- thirdparty dependencies -->
        <dependency>
            <groupId>org.quartz-scheduler</groupId>
            <artifactId>quartz</artifactId>
            <version>2.2.1</version>
        </dependency>
        <dependency>
            <groupId>com.jcraft</groupId>
            <artifactId>jsch</artifactId>
            <version>0.1.55</version>
        </dependency>
        <dependency>
            <groupId>commons-cli</groupId>
            <artifactId>commons-cli</artifactId>
            <version>1.4</version>
        </dependency>
        <dependency>
            <groupId>org.apache.commons</groupId>
            <artifactId>commons-text</artifactId>
            <version>1.4</version>
        </dependency>
        <dependency>
            <groupId>com.fasterxml.jackson.core</groupId>
            <artifactId>jackson-databind</artifactId>
            <version>${jackson.version}</version>
        </dependency>
        <dependency>
            <groupId>com.fasterxml.jackson.dataformat</groupId>
            <artifactId>jackson-dataformat-yaml</artifactId>
            <version>${jackson.version}</version>
        </dependency>
        
        <!-- test scope -->
        <dependency>
            <groupId>junit</groupId>
            <artifactId>junit</artifactId>
            <version>4.11</version>
            <scope>test</scope>
        </dependency>
        
    </dependencies>

    <scm>
        <connection>scm:git:file://.</connection>
        <developerConnection>scm:git:file://.</developerConnection>
        <url>scm:git:file://.</url>
        <tag>HEAD</tag>
    </scm>

    <build>
        <plugins>

            <!--  The buildnumber plugin will take Git commit information and embed it in the JAR manifest.
                  When the program is run it will print the release information to the console. -->
            <plugin>
                <groupId>org.codehaus.mojo</groupId>
                <artifactId>buildnumber-maven-plugin</artifactId>
                <version>1.4</version>
                <executions>
                    <execution>
                        <phase>validate</phase>
                        <goals>
                            <goal>create</goal>
                        </goals>
                    </execution>
                </executions>
                <configuration>
                    <doCheck>false</doCheck>
                    <doUpdate>false</doUpdate>
                    <shortRevisionLength>8</shortRevisionLength>
                </configuration>
            </plugin>
<!-- 
            <plugin>
                <artifactId>maven-assembly-plugin</artifactId>
                <executions>
                    <execution>
                        <phase>package</phase>
                        <goals>
                            <goal>single</goal>
                        </goals>
                    </execution>
                </executions>
                <configuration>
                    <descriptorRefs>
                        <descriptorRef>jar-with-dependencies</descriptorRef>
                    </descriptorRefs>
                    <archive>
                        <manifest>
                            <mainClass>gpu.uc01.Conductor</mainClass>
                        </manifest>
                    </archive>
                </configuration>
            </plugin>
 -->
            <plugin>
                <groupId>org.apache.maven.plugins</groupId>
                <artifactId>maven-jar-plugin</artifactId>
                <version>2.4</version>
                <configuration>
                    <archive>
                        <manifest>
                            <addClasspath>false</addClasspath>
                            <addDefaultImplementationEntries>true</addDefaultImplementationEntries>
                            <addDefaultSpecificationEntries>true</addDefaultSpecificationEntries>
                            <mainClass>kinetica.orch.Conductor</mainClass>
                        </manifest>
                        <manifestEntries>
                            <Build-Time>${maven.build.timestamp}</Build-Time>
                            <Implementation-Version>${scmBranch}-${buildNumber}</Implementation-Version>
                        </manifestEntries>
                    </archive>
                </configuration>
            </plugin>

            <plugin>
                <groupId>org.apache.maven.plugins</groupId>
                <artifactId>maven-shade-plugin</artifactId>
                <version>3.2.1</version>
                <configuration>
                    <!--  <outputFile>${project.build.directory}/${loader.jar.name}</outputFile>  -->
                    <shadedArtifactAttached>false</shadedArtifactAttached>
                    <minimizeJar>true</minimizeJar>
                    <useBaseVersion>false</useBaseVersion>
                    <keepDependenciesWithProvidedScope>false</keepDependenciesWithProvidedScope>
                    <createDependencyReducedPom>false</createDependencyReducedPom>
                    <filters>
                        <filter>
                            <artifact>org.quartz-scheduler:quartz</artifact>
                            <includes>
                                <include>**</include>
                            </includes>
                        </filter>
                        <filter>
                            <artifact>ch.qos.logback:logback-core</artifact>
                            <includes>
                                <include>**</include>
                            </includes>
                        </filter>
                        <filter>
                            <artifact>com.jcraft:jsch</artifact>
                            <includes>
                                <include>**</include>
                            </includes>
                        </filter>
                    </filters>
                </configuration>
                <executions>
                    <execution>
                        <phase>package</phase>
                        <goals>
                            <goal>shade</goal>
                        </goals>
                    </execution>
                </executions>
            </plugin>

            <plugin>
                <groupId>org.apache.maven.plugins</groupId>
                <artifactId>maven-surefire-plugin</artifactId>
                <version>2.12</version>
                <configuration>
                    <excludes>
                        <exclude>**/*.java</exclude>
                    </excludes>
                </configuration>
            </plugin>

        </plugins>
    </build>

</project>
