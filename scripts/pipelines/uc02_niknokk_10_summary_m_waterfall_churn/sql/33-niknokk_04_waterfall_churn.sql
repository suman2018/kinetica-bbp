-- Author: Sadar Baskoro <ssetihajid@kinetica.com>
-- Last Update: 23 Dec 2018
-- Objective: update niknokk_04_waterfall_churn


delete from ${table-prefix}${niknokk-schema}.${table-prefix}niknokk_04_waterfall_churn
where mth = '${run-date=yyyyMM}';

INSERT INTO ${table-prefix}${niknokk-schema}.${table-prefix}niknokk_04_waterfall_churn
SELECT 
	mth,
	subs_churn,
	nik
FROM 
(
	SELECT
	mth,
	COUNT(DISTINCT msisdn) as subs_churn,
	COUNT(DISTINCT CASE WHEN nik IS NOT NULL THEN nik END) as nik
	FROM ${table-prefix}${niknokk-schema}.${table-prefix}niknokk_churn_reg_${run-date=yyyyMM}
	GROUP BY 1
) AAA;
