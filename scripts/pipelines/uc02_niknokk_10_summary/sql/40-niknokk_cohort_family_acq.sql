-- Author: Sadar Baskoro <ssetihajid@kinetica.com>
-- Last Update: 23 Dec 2018
-- Objective: update niknokk_cohort_family_acq

create or replace table ${table-prefix}${niknokk-schema}.${table-prefix}niknokk_cohort_family_acq_tmp1 as 
select a.*, KI_SHARD_KEY(nokk)
from
(
SELECT
	NVL(a.ss_nmbr,'9999999999') nokk,
	CASE WHEN a.nik_id IS NULL THEN 'N' ELSE 'Y' END flag_nik_null,
	nvl(c.region,'UNKNOWN') as region_administrative,
	nvl(c.nama_provinsi,'UNKNOWN') as provinsi_administrative,
	nvl(c.nama_kabupaten,'UNKNOWN') as kabupaten_administrative,
	nvl(c.nama_kecamatan,'UNKNOWN') as kecamatan_administrative,
	a.nik_id,
	a.msisdn,
	nvl(a.rev_mtd,0) as rev
FROM  ${table-prefix}${niknokk-schema}.${table-prefix}niknokk_multidim_${prev-month=yyyyMM} a
LEFT JOIN ${table-prefix}${niknokk-schema}.${table-prefix}niknokk_ref_dukcapil_nik_map c
ON substr(a.ss_nmbr,1,6) = c.kode_kecamatan
INNER JOIN
(
        SELECT msisdn
        FROM ${table-prefix}${niknokk-schema}.${table-prefix}niknokk_preblk_ccm_cln_baru
        WHERE  DATE(trx_timestamp) <= DATE(DATETIME(last_day(date('${prev-month=yyyy-MM-dd}'))))
                AND id_status ='S'
        GROUP BY 1
) b ON a.msisdn=b.msisdn -- change every end of month-1
WHERE
        a.brand<>'kartuHALO'
        and date(a.trx_date) = DATE(DATETIME(last_day(date('${prev-month=yyyy-MM-dd}'))))
) a;

create or replace table ${table-prefix}${niknokk-schema}.${table-prefix}niknokk_cohort_family_acq_tmp2 as
select nokk, flag_nik_null, region_administrative, 
        CASE    
                WHEN region_administrative IN ('Sumbagut','Sumbagteng','Sumbagsel') THEN 'AREA 1'
                WHEN region_administrative IN ('Western Jabotabek','Central Jabotabek','Eastern Jabotabek','Jabar') THEN 'AREA 2'
                WHEN region_administrative IN ('Jateng','Jatim','Balinusra') THEN 'AREA 3'
                WHEN region_administrative IN ('Kalimantan','Sulawesi','Puma') THEN 'AREA 4'
                else 'UNKNOWN'
        END area_administrative, provinsi_administrative, kabupaten_administrative, kecamatan_administrative,
count(distinct nik_id) c_nik,
count(distinct msisdn) c_msisdn,
sum(rev) rev,
KI_SHARD_KEY(nokk)
from ${table-prefix}${niknokk-schema}.${table-prefix}niknokk_cohort_family_acq_tmp1
group by 1,2,3,4,5,6,7;


create or replace table ${table-prefix}${niknokk-schema}.${table-prefix}niknokk_cohort_family_acq_tmp3 as
select a.msisdn, a.nik_id, NVL(a.ss_nmbr,'9999999999') nokk, nvl(rev_mtd,0) rev_mtd, KI_SHARD_KEY(a.msisdn)
from ${table-prefix}${niknokk-schema}.${table-prefix}niknokk_multidim_${run-date=yyyyMM} a
INNER JOIN
(
        SELECT msisdn
        FROM ${table-prefix}${niknokk-schema}.${table-prefix}niknokk_preblk_ccm_cln_baru
        WHERE DATE(trx_timestamp) <= '${run-date=yyyy-MM-dd}'
                AND id_status ='S'
        GROUP BY 1
) b ON a.msisdn=b.msisdn -- change every 10, 20, and end of month, configure from cronjob
WHERE a.brand<>'kartuHALO'
      and date(a.trx_date) = '${run-date=yyyy-MM-dd}';


create or replace table ${table-prefix}${niknokk-schema}.${table-prefix}niknokk_cohort_family_acq_tmp4 as
SELECT
        nokk,
        COUNT(DISTINCT a.nik_id) c_nik,
        COUNT(DISTINCT a.msisdn) c_msisdn,
        SUM(rev_mtd) rev,
	KI_SHARD_KEY(nokk)
FROM ${table-prefix}${niknokk-schema}.${table-prefix}niknokk_cohort_family_acq_tmp3 a
GROUP BY 1;



delete from ${table-prefix}${niknokk-schema}.${table-prefix}niknokk_cohort_family_acq
where dates = concat(concat(CHAR16(last_day('${prev-month=yyyy-MM-dd}')),' VERSUS '),'${run-date=yyyy-MM-dd}');

INSERT INTO ${table-prefix}${niknokk-schema}.${table-prefix}niknokk_cohort_family_acq
SELECT 
	concat(concat(CHAR16(last_day('${prev-month=yyyy-MM-dd}')),' VERSUS '),'${run-date=yyyy-MM-dd}') dates,
	flag_nik_null,
	area_administrative,
	region_administrative,
	provinsi_administrative,
	kabupaten_administrative,
	kecamatan_administrative,
	CASE 
		WHEN a.c_nik=1 AND a.c_msisdn=1 THEN '1 NIK AND 1 MSISDN'
		WHEN a.c_nik=2 AND a.c_msisdn=1 THEN '2 NIK AND 1 MSISDN'
		WHEN a.c_nik=3 AND a.c_msisdn=1 THEN '3 NIK AND 1 MSISDN'
		WHEN a.c_nik=4 AND a.c_msisdn=1 THEN '4 NIK AND 1 MSISDN'
		WHEN a.c_nik=5 AND a.c_msisdn=1 THEN '5 NIK AND 1 MSISDN'
		WHEN a.c_nik>5 AND a.c_msisdn=1 THEN '>5 NIK AND 1 MSISDN'
		WHEN a.c_nik=1 AND a.c_msisdn=2 THEN '1 NIK AND 2 MSISDN'
		WHEN a.c_nik=2 AND a.c_msisdn=2 THEN '2 NIK AND 2 MSISDN'
		WHEN a.c_nik=3 AND a.c_msisdn=2 THEN '3 NIK AND 2 MSISDN'
		WHEN a.c_nik=4 AND a.c_msisdn=2 THEN '4 NIK AND 2 MSISDN'
		WHEN a.c_nik=5 AND a.c_msisdn=2 THEN '5 NIK AND 2 MSISDN'
		WHEN a.c_nik>5 AND a.c_msisdn=2 THEN '>5 NIK AND 2 MSISDN'
		WHEN a.c_nik=1 AND a.c_msisdn=3 THEN '1 NIK AND 3 MSISDN'
		WHEN a.c_nik=2 AND a.c_msisdn=3 THEN '2 NIK AND 3 MSISDN'
		WHEN a.c_nik=3 AND a.c_msisdn=3 THEN '3 NIK AND 3 MSISDN'
		WHEN a.c_nik=4 AND a.c_msisdn=3 THEN '4 NIK AND 3 MSISDN'
		WHEN a.c_nik=5 AND a.c_msisdn=3 THEN '5 NIK AND 3 MSISDN'
		WHEN a.c_nik>5 AND a.c_msisdn=3 THEN '>5 NIK AND 3 MSISDN'
		WHEN a.c_nik=1 AND a.c_msisdn>3 THEN '1 NIK AND >3 MSISDN'
		WHEN a.c_nik=2 AND a.c_msisdn>3 THEN '2 NIK AND >3 MSISDN'
		WHEN a.c_nik=3 AND a.c_msisdn>3 THEN '3 NIK AND >3 MSISDN'
		WHEN a.c_nik=4 AND a.c_msisdn>3 THEN '4 NIK AND >3 MSISDN'
		WHEN a.c_nik=5 AND a.c_msisdn>3 THEN '5 NIK AND >3 MSISDN'
		WHEN a.c_nik>5 AND a.c_msisdn>3 THEN '>5 NIK AND >3 MSISDN'
	END segment_nik_msisdn,
	CASE 
		WHEN b.c_nik=1 AND b.c_msisdn=1 THEN '1 NIK AND 1 MSISDN'
		WHEN b.c_nik=2 AND b.c_msisdn=1 THEN '2 NIK AND 1 MSISDN'
		WHEN b.c_nik=3 AND b.c_msisdn=1 THEN '3 NIK AND 1 MSISDN'
		WHEN b.c_nik=4 AND b.c_msisdn=1 THEN '4 NIK AND 1 MSISDN'
		WHEN b.c_nik=5 AND b.c_msisdn=1 THEN '5 NIK AND 1 MSISDN'
		WHEN b.c_nik>5 AND b.c_msisdn=1 THEN '>5 NIK AND 1 MSISDN'
		WHEN b.c_nik=1 AND b.c_msisdn=2 THEN '1 NIK AND 2 MSISDN'
		WHEN b.c_nik=2 AND b.c_msisdn=2 THEN '2 NIK AND 2 MSISDN'
		WHEN b.c_nik=3 AND b.c_msisdn=2 THEN '3 NIK AND 2 MSISDN'
		WHEN b.c_nik=4 AND b.c_msisdn=2 THEN '4 NIK AND 2 MSISDN'
		WHEN b.c_nik=5 AND b.c_msisdn=2 THEN '5 NIK AND 2 MSISDN'
		WHEN b.c_nik>5 AND b.c_msisdn=2 THEN '>5 NIK AND 2 MSISDN'
		WHEN b.c_nik=1 AND b.c_msisdn=3 THEN '1 NIK AND 3 MSISDN'
		WHEN b.c_nik=2 AND b.c_msisdn=3 THEN '2 NIK AND 3 MSISDN'
		WHEN b.c_nik=3 AND b.c_msisdn=3 THEN '3 NIK AND 3 MSISDN'
		WHEN b.c_nik=4 AND b.c_msisdn=3 THEN '4 NIK AND 3 MSISDN'
		WHEN b.c_nik=5 AND b.c_msisdn=3 THEN '5 NIK AND 3 MSISDN'
		WHEN b.c_nik>5 AND b.c_msisdn=3 THEN '>5 NIK AND 3 MSISDN'
		WHEN b.c_nik=1 AND b.c_msisdn>3 THEN '1 NIK AND >3 MSISDN'
		WHEN b.c_nik=2 AND b.c_msisdn>3 THEN '2 NIK AND >3 MSISDN'
		WHEN b.c_nik=3 AND b.c_msisdn>3 THEN '3 NIK AND >3 MSISDN'
		WHEN b.c_nik=4 AND b.c_msisdn>3 THEN '4 NIK AND >3 MSISDN'
		WHEN b.c_nik=5 AND b.c_msisdn>3 THEN '5 NIK AND >3 MSISDN'
		WHEN b.c_nik>5 AND b.c_msisdn>3 THEN '>5 NIK AND >3 MSISDN'
		WHEN b.NOKK IS NULL THEN 'Churn'
	END segment_nik_msisdn_now,
	COUNT(DISTINCT a.nokk) sum_nokk,
	SUM(a.c_nik) sum_nik,
	SUM(a.c_msisdn) sum_msisdn,
	SUM(a.rev) sum_rev,
	COUNT(DISTINCT b.nokk) sum_nokk_now,
	SUM(b.c_nik) sum_nik_now,
	SUM(b.c_msisdn) sum_msisdn_now,
	SUM(b.rev) sum_rev_now
FROM
${table-prefix}${niknokk-schema}.${table-prefix}niknokk_cohort_family_acq_tmp2 a
LEFT JOIN ${table-prefix}${niknokk-schema}.${table-prefix}niknokk_cohort_family_acq_tmp4 b
ON a.nokk=b.nokk
GROUP BY 2,3,4,5,6,7,8,9;

drop table if exists ${table-prefix}${niknokk-schema}.${table-prefix}niknokk_cohort_family_acq_tmp1;
drop table if exists ${table-prefix}${niknokk-schema}.${table-prefix}niknokk_cohort_family_acq_tmp2;
drop table if exists ${table-prefix}${niknokk-schema}.${table-prefix}niknokk_cohort_family_acq_tmp3;
drop table if exists ${table-prefix}${niknokk-schema}.${table-prefix}niknokk_cohort_family_acq_tmp4;


