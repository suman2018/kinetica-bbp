-- Author: Sadar Baskoro <ssetihajid@kinetica.com>
-- Last Update: 23 Dec 2018
-- Objective: update niknokk_05_segment_nik_churn



create or replace table ${table-prefix}${niknokk-schema}.${table-prefix}niknokk_05_segment_nik_churn_tmp as
select nik, count(msisdn) c_msisdn
from ${table-prefix}${niknokk-schema}.${table-prefix}niknokk_churn_reg_${run-date=yyyyMM}
group by 1;


delete from ${table-prefix}${niknokk-schema}.${table-prefix}niknokk_05_segment_nik_churn
where mth = '${run-date=yyyyMM}';


INSERT INTO ${table-prefix}${niknokk-schema}.${table-prefix}niknokk_05_segment_nik_churn
SELECT
	'${run-date=yyyyMM}' as mth,
	c_msisdn,
	count(distinct nik) as nik
FROM ${table-prefix}${niknokk-schema}.${table-prefix}niknokk_05_segment_nik_churn_tmp
group by 2;

drop table if exists ${table-prefix}${niknokk-schema}.${table-prefix}niknokk_05_segment_nik_churn_tmp;

