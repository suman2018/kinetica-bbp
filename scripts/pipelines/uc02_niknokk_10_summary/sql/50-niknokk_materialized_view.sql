


CREATE OR REPLACE MATERIALIZED VIEW
${table-prefix}${niknokk-schema}_out.${table-prefix}niknokk_01_msisdn_prepaid_reg_v
REFRESH EVERY 1 DAY AS (
SELECT *
FROM ${table-prefix}${niknokk-schema}.${table-prefix}niknokk_01_msisdn_prepaid_reg
);


CREATE OR REPLACE MATERIALIZED VIEW
${table-prefix}${niknokk-schema}_out.${table-prefix}niknokk_02_nik_prepaid_reg_v
REFRESH EVERY 1 DAY AS (
SELECT *
FROM ${table-prefix}${niknokk-schema}.${table-prefix}niknokk_02_nik_prepaid_reg
);

CREATE OR REPLACE MATERIALIZED VIEW
${table-prefix}${niknokk-schema}_out.${table-prefix}niknokk_03_churn_msisdn_nik_v
REFRESH EVERY 1 DAY AS (
SELECT *
FROM ${table-prefix}${niknokk-schema}.${table-prefix}niknokk_03_churn_msisdn_nik
);

CREATE OR REPLACE MATERIALIZED VIEW
${table-prefix}${niknokk-schema}_out.${table-prefix}niknokk_04_waterfall_churn_v
REFRESH EVERY 1 DAY AS (
SELECT *
FROM ${table-prefix}${niknokk-schema}.${table-prefix}niknokk_04_waterfall_churn
);

CREATE OR REPLACE MATERIALIZED VIEW
${table-prefix}${niknokk-schema}_out.${table-prefix}niknokk_05_segment_nik_churn_v
REFRESH EVERY 1 DAY AS (
SELECT *
FROM ${table-prefix}${niknokk-schema}.${table-prefix}niknokk_05_segment_nik_churn
);


CREATE OR REPLACE MATERIALIZED VIEW
${table-prefix}${niknokk-schema}_out.${table-prefix}niknokk_06_segment_nik_all_v
REFRESH EVERY 1 DAY AS (
SELECT *
FROM ${table-prefix}${niknokk-schema}.${table-prefix}niknokk_06_segment_nik_all
);


CREATE OR REPLACE MATERIALIZED VIEW
${table-prefix}${niknokk-schema}_out.${table-prefix}niknokk_07_profile_churn_nik_age_v
REFRESH EVERY 1 DAY AS (
SELECT *
FROM ${table-prefix}${niknokk-schema}.${table-prefix}niknokk_07_profile_churn_nik_age
);

CREATE OR REPLACE MATERIALIZED VIEW
${table-prefix}${niknokk-schema}_out.${table-prefix}niknokk_07_profile_churn_nik_regchnnl_v
REFRESH EVERY 1 DAY AS (
SELECT *
FROM ${table-prefix}${niknokk-schema}.${table-prefix}niknokk_07_profile_churn_nik_regchnnl
);


CREATE OR REPLACE MATERIALIZED VIEW
${table-prefix}${niknokk-schema}_out.${table-prefix}niknokk_08_churn_segment_v
REFRESH EVERY 1 DAY AS (
SELECT *
FROM ${table-prefix}${niknokk-schema}.${table-prefix}niknokk_08_churn_segment
);

CREATE OR REPLACE MATERIALIZED VIEW
${table-prefix}${niknokk-schema}_out.${table-prefix}niknokk_cohort_family_acq_v
REFRESH EVERY 1 DAY AS (
SELECT *
FROM ${table-prefix}${niknokk-schema}.${table-prefix}niknokk_cohort_family_acq
);


CREATE OR REPLACE MATERIALIZED VIEW
${table-prefix}${niknokk-schema}_out.${table-prefix}niknokk_summary1_family_acq_v
REFRESH EVERY 1 DAY AS (
SELECT *
FROM ${table-prefix}${niknokk-schema}.${table-prefix}niknokk_summary1_family_acq
);


CREATE OR REPLACE MATERIALIZED VIEW
${table-prefix}${niknokk-schema}_out.${table-prefix}niknokk_summary2_family_acq_v
REFRESH EVERY 1 DAY AS (
SELECT *
FROM ${table-prefix}${niknokk-schema}.${table-prefix}niknokk_summary2_family_acq
);

alter schema ${table-prefix}${niknokk-schema}_out set protected true;

