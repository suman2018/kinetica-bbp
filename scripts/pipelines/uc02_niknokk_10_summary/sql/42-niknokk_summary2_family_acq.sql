-- Author: Sadar Baskoro <ssetihajid@kinetica.com>
-- Last Update: 23 Dec 2018
-- Objective: update niknokk_summary2_family_acq


create or replace table ${table-prefix}${niknokk-schema}.${table-prefix}niknokk_summary2_family_acq_tmp1 as  
SELECT *,
        KI_SHARD_KEY(kecamatan_lacci) FROM
(
    SELECT
            trx_date,
            a.ss_nmbr nokk,
            CASE WHEN a.nik_id IS NULL THEN 'N' ELSE 'Y' END flag_nik_null,
            nvl(a.area_sales,'UNKNOWN') area_lacci, --- no area_lacci in Multidim
            CASE
                    WHEN a.region_sales = 'SUMBAGUT' THEN '01.Sumbagut'
                    WHEN a.region_sales = 'SUMBAGTENG' THEN '02.Sumbagteng'
                    WHEN a.region_sales = 'SUMBAGSEL' THEN '03.Sumbagsel'
                    WHEN a.region_sales = 'WESTERN JABOTABEK' THEN '04.Western Jabotabek'
                    WHEN a.region_sales = 'CENTRAL JABOTABEK' THEN '05.Central Jabotabek'
                    WHEN a.region_sales = 'EASTERN JABOTABEK' THEN '06.Eastern Jabotabek'
                    WHEN a.region_sales = 'JABAR' THEN '07.Jabar'
                    WHEN a.region_sales = 'JATENG' THEN '08.Jateng'
                    WHEN a.region_sales = 'JATIM' THEN '09.Jatim'
                    WHEN a.region_sales = 'BALINUSRA' THEN '10.Balinusra'
                    WHEN a.region_sales = 'KALIMANTAN' THEN '11.Kalimantan'
                    WHEN a.region_sales = 'SULAWESI' THEN '12.Sulawesi'
                    WHEN a.region_sales = 'PUMA' THEN '13.Puma'
                    WHEN a.region_sales = 'SAD' THEN '14.SAD'
            ELSE 'UNKNOWN'
            END region_lacci,
            nvl(a.cluster,'UNKNOWN') cluster_lacci,
            nvl(a.kabupaten,'UNKNOWN') kabupaten_lacci,
            nvl(a.kecamatan,'UNKNOWN') kecamatan_lacci,
            a.nik_id,
            a.msisdn,
            nvl(rev_mtd,0) as rev_mtd
    FROM ${table-prefix}${niknokk-schema}.${table-prefix}niknokk_multidim_${run-date=yyyyMM} a
    INNER JOIN
    (
            SELECT msisdn
            FROM ${table-prefix}${niknokk-schema}.${table-prefix}niknokk_preblk_ccm_cln_baru
            WHERE  DATE(trx_timestamp) <= '${run-date=yyyy-MM-dd}'
                    AND id_status ='S' ---- change daily D-2 
            GROUP BY 1
    ) b ON a.msisdn=b.msisdn -- berubah daily D-2
    WHERE a.brand<>'kartuHALO'
    and date(a.trx_date) = '${run-date=yyyy-MM-dd}'
);


create or replace table ${table-prefix}${niknokk-schema}.${table-prefix}niknokk_summary2_family_acq_tmp2 as
select trx_date, nokk, flag_nik_null, area_lacci, region_lacci, cluster_lacci, kabupaten_lacci, kecamatan_lacci,
count(distinct nik_id) c_nik, 
count(distinct msisdn) c_msisdn,
sum(rev_mtd) rev,
KI_SHARD_KEY(nokk)
from ${table-prefix}${niknokk-schema}.${table-prefix}niknokk_summary2_family_acq_tmp1
group by 1,2,3,4,5,6,7,8; 




delete from ${table-prefix}${niknokk-schema}.${table-prefix}niknokk_summary2_family_acq 
where date(dt_dt) = '${run-date=yyyy-MM-dd}';

INSERT INTO ${table-prefix}${niknokk-schema}.${table-prefix}niknokk_summary2_family_acq
SELECT 
	dt_dt,
	flag_nik_null,
	NVL(area_lacci,'UNKNOWN') area_lacci,
	NVL(region_lacci,'UNKNOWN') region_lacci,
	NVL(cluster_lacci,'UNKNOWN') cluster_lacci,
	NVL(kabupaten_lacci,'UNKNOWN') kabupaten_lacci,
	NVL(kecamatan_lacci,'UNKNOWN') kecamatan_lacci,
	NVL(segment_nik_msisdn,'UNKNOWN') segment_nik_msisdn,
	sum_nokk,
	sum_nik,
	sum_msisdn,
	rev
FROM 
(
SELECT 
	date(trx_date) dt_dt,
	flag_nik_null,
	area_lacci,
	region_lacci,
	cluster_lacci,
	kabupaten_lacci,
	kecamatan_lacci,
	CASE 
		WHEN c_nik=1 AND c_msisdn=1 THEN '1 NIK AND 1 MSISDN'
		WHEN c_nik=2 AND c_msisdn=1 THEN '2 NIK AND 1 MSISDN'
		WHEN c_nik=3 AND c_msisdn=1 THEN '3 NIK AND 1 MSISDN'
		WHEN c_nik=4 AND c_msisdn=1 THEN '4 NIK AND 1 MSISDN'
		WHEN c_nik=5 AND c_msisdn=1 THEN '5 NIK AND 1 MSISDN'
		WHEN c_nik>5 AND c_msisdn=1 THEN '>5 NIK AND 1 MSISDN'
		WHEN c_nik=1 AND c_msisdn=2 THEN '1 NIK AND 2 MSISDN'
		WHEN c_nik=2 AND c_msisdn=2 THEN '2 NIK AND 2 MSISDN'
		WHEN c_nik=3 AND c_msisdn=2 THEN '3 NIK AND 2 MSISDN'
		WHEN c_nik=4 AND c_msisdn=2 THEN '4 NIK AND 2 MSISDN'
		WHEN c_nik=5 AND c_msisdn=2 THEN '5 NIK AND 2 MSISDN'
		WHEN c_nik>5 AND c_msisdn=2 THEN '>5 NIK AND 2 MSISDN'
		WHEN c_nik=1 AND c_msisdn=3 THEN '1 NIK AND 3 MSISDN'
		WHEN c_nik=2 AND c_msisdn=3 THEN '2 NIK AND 3 MSISDN'
		WHEN c_nik=3 AND c_msisdn=3 THEN '3 NIK AND 3 MSISDN'
		WHEN c_nik=4 AND c_msisdn=3 THEN '4 NIK AND 3 MSISDN'
		WHEN c_nik=5 AND c_msisdn=3 THEN '5 NIK AND 3 MSISDN'
		WHEN c_nik>5 AND c_msisdn=3 THEN '>5 NIK AND 3 MSISDN'
		WHEN c_nik=1 AND c_msisdn>3 THEN '1 NIK AND >3 MSISDN'
		WHEN c_nik=2 AND c_msisdn>3 THEN '2 NIK AND >3 MSISDN'
		WHEN c_nik=3 AND c_msisdn>3 THEN '3 NIK AND >3 MSISDN'
		WHEN c_nik=4 AND c_msisdn>3 THEN '4 NIK AND >3 MSISDN'
		WHEN c_nik=5 AND c_msisdn>3 THEN '5 NIK AND >3 MSISDN'
		WHEN c_nik>5 AND c_msisdn>3 THEN '>5 NIK AND >3 MSISDN'
	END segment_nik_msisdn,
	COUNT(DISTINCT nokk) sum_nokk,
	SUM(c_nik) sum_nik,
	SUM(c_msisdn) sum_msisdn,
	SUM(rev) rev
FROM ${table-prefix}${niknokk-schema}.${table-prefix}niknokk_summary2_family_acq_tmp2
GROUP BY 1,2,3,4,5,6,7,8
) AAAA;

drop table if exists ${table-prefix}${niknokk-schema}.${table-prefix}niknokk_summary2_family_acq_tmp1;
drop table if exists ${table-prefix}${niknokk-schema}.${table-prefix}niknokk_summary2_family_acq_tmp2;



