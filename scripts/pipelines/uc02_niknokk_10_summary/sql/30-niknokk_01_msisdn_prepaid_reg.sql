-- Author: Sadar Baskoro <ssetihajid@kinetica.com>
-- Last Update: 23 Dec 2018
-- Objective: update niknokk_01_msisdn_prepaid_reg


delete from ${table-prefix}${niknokk-schema}.${table-prefix}niknokk_01_msisdn_prepaid_reg
where mth = '${prev-month=yyyyMM}';


INSERT INTO ${table-prefix}${niknokk-schema}.${table-prefix}niknokk_01_msisdn_prepaid_reg
SELECT 
	mth,
	area_lacci,
	f_segment,
	subs
FROM (
select
	'${prev-month=yyyyMM}' mth,
	case
		when a.area_lacci is not null then a.area_lacci
		when b.area_lacci is not null then b.area_lacci
		else 'UNKNOWN'
	end area_lacci,
	case
		when a.simple_wording is null and b.simple_wording = 'Success' then 'NEW REG'
		when a.simple_wording = 'Success' and b.msisdn is null then 'CHURN REG'
		when a.simple_wording = 'Success' and b.simple_wording = 'Success' then 'EXISTING'
		when a.msisdn is null and b.msisdn is not null then 'NEW SALES'
		when a.msisdn is not null and b.msisdn is null then 'CHURN NON REG'
		when a.msisdn is null and b.msisdn is not null then 'EXISTING NON REG'
		else 'UNKNOWN'
	end as f_segment,
	SUM(1) subs
FROM 
(
	SELECT msisdn,area_lacci,simple_wording 
	FROM ${table-prefix}${niknokk-schema}.${table-prefix}niknokk_pre_reg_${prev-month=yyyyMM} 
	GROUP BY 1,2,3
) a FULL JOIN 
(
	SELECT msisdn,area_lacci,simple_wording 
	FROM ${table-prefix}${niknokk-schema}.${table-prefix}niknokk_pre_reg_${run-date=yyyyMM} 
	GROUP BY 1,2,3
) b 
ON a.msisdn = b.msisdn
group by 2,3
) AAA;

