-- Author: Sadar Baskoro <ssetihajid@kinetica.com>
-- Last Update: 23 Dec 2018
-- Objective: update niknokk_02_nik_prepaid_reg


create or replace table ${table-prefix}${niknokk-schema}.${table-prefix}niknokk_02_nik_prepaid_reg_tmp1a as 
SELECT nik_id,area_lacci,count(msisdn) counter, KI_SHARD_KEY(nik_id)
                        FROM ${table-prefix}${niknokk-schema}.${table-prefix}niknokk_pre_reg_${prev-month=yyyyMM}
                        WHERE simple_wording='Success'  AND nik_id is not null
                        GROUP BY 1,2;

create or replace table ${table-prefix}${niknokk-schema}.${table-prefix}niknokk_02_nik_prepaid_reg_tmp1b as
select aa.*
        from
        (
                select aaa.*, row_number()Over(PARTITION BY nik_id ORDER BY counter DESC, area_lacci ASC) as row_num
                from ${table-prefix}${niknokk-schema}.${table-prefix}niknokk_02_nik_prepaid_reg_tmp1a aaa
        ) aa where row_num = 1;

drop table if exists ${table-prefix}${niknokk-schema}.${table-prefix}niknokk_02_nik_prepaid_reg_tmp1a;


create or replace table ${table-prefix}${niknokk-schema}.${table-prefix}niknokk_02_nik_prepaid_reg_tmp2a as
SELECT nik_id,area_lacci,count(msisdn) counter, KI_SHARD_KEY(nik_id)
                        FROM ${table-prefix}${niknokk-schema}.${table-prefix}niknokk_pre_reg_${run-date=yyyyMM}
                        WHERE simple_wording='Success'  AND nik_id is not null
                        GROUP BY 1,2;

create or replace table ${table-prefix}${niknokk-schema}.${table-prefix}niknokk_02_nik_prepaid_reg_tmp2b as
select aa.*
        from
        (
                select aaa.*, row_number()Over(PARTITION BY nik_id ORDER BY counter DESC, area_lacci ASC) as row_num
                from ${table-prefix}${niknokk-schema}.${table-prefix}niknokk_02_nik_prepaid_reg_tmp2a aaa
        ) aa where row_num = 1;


drop table if exists ${table-prefix}${niknokk-schema}.${table-prefix}niknokk_02_nik_prepaid_reg_tmp2a;


delete from ${table-prefix}${niknokk-schema}.${table-prefix}niknokk_02_nik_prepaid_reg
where mth = '${prev-month=yyyyMM}';


INSERT INTO ${table-prefix}${niknokk-schema}.${table-prefix}niknokk_02_nik_prepaid_reg
SELECT 
	mth,
	area_lacci,
	f_segment,
	subs
FROM (
SELECT
'${prev-month=yyyyMM}' mth, 
case
	when a.area_lacci is not null then a.area_lacci
	when b.area_lacci is not null then b.area_lacci
	else 'UNKNOWN'
end as area_lacci,
CASE
	WHEN a.nik_id IS NULL AND b.nik_id IS NOT NULL THEN 'NEW'
	WHEN a.nik_id IS NOT NULL AND b.nik_id IS  NULL THEN 'CHURN'
	WHEN a.nik_id IS NULL AND b.nik_id IS NOT NULL THEN 'EXISTING'
	else 'UNKNOWN'
END AS f_segment,
Sum(1) subs
FROM 
${table-prefix}${niknokk-schema}.${table-prefix}niknokk_02_nik_prepaid_reg_tmp1b a FULL JOIN 
${table-prefix}${niknokk-schema}.${table-prefix}niknokk_02_nik_prepaid_reg_tmp2b b 
ON a.nik_id = b.nik_id
GROUP BY 2,3
) AAA;

drop table if exists ${table-prefix}${niknokk-schema}.${table-prefix}niknokk_02_nik_prepaid_reg_tmp1b;
drop table if exists ${table-prefix}${niknokk-schema}.${table-prefix}niknokk_02_nik_prepaid_reg_tmp2b;

