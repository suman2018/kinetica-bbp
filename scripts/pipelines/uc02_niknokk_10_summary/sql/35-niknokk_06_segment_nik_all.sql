-- Author: Sadar Baskoro <ssetihajid@kinetica.com>
-- Last Update: 23 Dec 2018
-- Objective: update niknokk_06_segment_nik_all


delete from ${table-prefix}${niknokk-schema}.${table-prefix}niknokk_06_segment_nik_all
where mth = '${run-date=yyyyMM}';


INSERT INTO ${table-prefix}${niknokk-schema}.${table-prefix}niknokk_06_segment_nik_all
SELECT 
	mth,
	c_msisdn,
	nik
FROM 
(
SELECT
	month as mth,
	c_msisdn, 
	count(DISTINCT nik_id) nik
FROM 
(
	SELECT month, nik_id, count(msisdn) c_msisdn 
	FROM ${table-prefix}${niknokk-schema}.${table-prefix}niknokk_pre_reg_${run-date=yyyyMM} 
	WHERE simple_wording='Success'  
	GROUP BY 1,2
)x 
GROUP BY 1,2
) DDD;
