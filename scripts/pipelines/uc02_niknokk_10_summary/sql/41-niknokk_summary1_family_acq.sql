-- Author: Sadar Baskoro <ssetihajid@kinetica.com>
-- Last Update: 23 Dec 2018
-- Objective: update niknokk_summary1_family_acq


delete from ${table-prefix}${niknokk-schema}.${table-prefix}niknokk_summary1_family_acq
where date(dt_dt) = '${run-date=yyyy-MM-dd}';

INSERT INTO ${table-prefix}${niknokk-schema}.${table-prefix}niknokk_summary1_family_acq
SELECT 
	dt_dt,
	flag_nik_null,
	nvl(area_administrative,'UNKNOWN') area_administrative,
	nvl(region_administrative,'UNKNOWN') region_administrative,
	nvl(provinsi_administrative,'UNKNOWN') provinsi_administrative,
	nvl(kabupaten_administrative,'UNKNOWN') kabupaten_administrative,
	nvl(kecamatan_administrative,'UNKNOWN') kecamatan_administrative,
	nvl(segment_nik_msisdn,'UNKNOWN') segment_nik_msisdn,
	sum_nokk,
	sum_nik,
	sum_msisdn,
	rev 
FROM 
(
SELECT 
	dt_dt,
	flag_nik_null,
	CASE
		WHEN region_administrative IN ('Sumbagut','Sumbagteng','Sumbagsel') THEN 'AREA 1'
		WHEN region_administrative IN ('Western Jabotabek','Central Jabotabek','Eastern Jabotabek','Jabar') THEN 'AREA 2'
		WHEN region_administrative IN ('Jateng','Jatim','Balinusra') THEN 'AREA 3'
		WHEN region_administrative IN ('Kalimantan','Sulawesi','Puma') THEN 'AREA 4'
		else 'UNKNOWN'
	END area_administrative,
	region_administrative,
	provinsi_administrative,
	kabupaten_administrative,
	kecamatan_administrative,
	CASE 
		WHEN c_nik=1 AND c_msisdn=1 THEN '1 NIK AND 1 MSISDN'
		WHEN c_nik=2 AND c_msisdn=1 THEN '2 NIK AND 1 MSISDN'
		WHEN c_nik=3 AND c_msisdn=1 THEN '3 NIK AND 1 MSISDN'
		WHEN c_nik=4 AND c_msisdn=1 THEN '4 NIK AND 1 MSISDN'
		WHEN c_nik=5 AND c_msisdn=1 THEN '5 NIK AND 1 MSISDN'
		WHEN c_nik>5 AND c_msisdn=1 THEN '>5 NIK AND 1 MSISDN'
		WHEN c_nik=1 AND c_msisdn=2 THEN '1 NIK AND 2 MSISDN'
		WHEN c_nik=2 AND c_msisdn=2 THEN '2 NIK AND 2 MSISDN'
		WHEN c_nik=3 AND c_msisdn=2 THEN '3 NIK AND 2 MSISDN'
		WHEN c_nik=4 AND c_msisdn=2 THEN '4 NIK AND 2 MSISDN'
		WHEN c_nik=5 AND c_msisdn=2 THEN '5 NIK AND 2 MSISDN'
		WHEN c_nik>5 AND c_msisdn=2 THEN '>5 NIK AND 2 MSISDN'
		WHEN c_nik=1 AND c_msisdn=3 THEN '1 NIK AND 3 MSISDN'
		WHEN c_nik=2 AND c_msisdn=3 THEN '2 NIK AND 3 MSISDN'
		WHEN c_nik=3 AND c_msisdn=3 THEN '3 NIK AND 3 MSISDN'
		WHEN c_nik=4 AND c_msisdn=3 THEN '4 NIK AND 3 MSISDN'
		WHEN c_nik=5 AND c_msisdn=3 THEN '5 NIK AND 3 MSISDN'
		WHEN c_nik>5 AND c_msisdn=3 THEN '>5 NIK AND 3 MSISDN'
		WHEN c_nik=1 AND c_msisdn>3 THEN '1 NIK AND >3 MSISDN'
		WHEN c_nik=2 AND c_msisdn>3 THEN '2 NIK AND >3 MSISDN'
		WHEN c_nik=3 AND c_msisdn>3 THEN '3 NIK AND >3 MSISDN'
		WHEN c_nik=4 AND c_msisdn>3 THEN '4 NIK AND >3 MSISDN'
		WHEN c_nik=5 AND c_msisdn>3 THEN '5 NIK AND >3 MSISDN'
		WHEN c_nik>5 AND c_msisdn>3 THEN '>5 NIK AND >3 MSISDN'
	END segment_nik_msisdn,
	count(distinct nokk) sum_nokk,
	sum(c_nik) sum_nik,
	sum(c_msisdn) sum_msisdn,
	sum(rev) rev
FROM
(
select
	date(trx_date) dt_dt,
	a.ss_nmbr nokk,
	case when a.nik_id is null then 'N' else 'Y' end flag_nik_null,
	c.region region_administrative,
	c.nama_provinsi provinsi_administrative,
	c.nama_kabupaten kabupaten_administrative,
	c.nama_kecamatan kecamatan_administrative,
	count(distinct a.nik_id) c_nik,
	count(distinct a.msisdn) c_msisdn,
	sum(nvl(a.rev_mtd,0)) rev
from  ${table-prefix}${niknokk-schema}.${table-prefix}niknokk_multidim_${run-date=yyyyMM} a
inner join 
	(
		select msisdn
		from ${table-prefix}${niknokk-schema}.${table-prefix}niknokk_preblk_ccm_cln_baru
		where  date(trx_timestamp) <= '${run-date=yyyy-MM-dd}' 
			and id_status ='S' ---- change daily d-2 
		group by 1
	) b on a.msisdn=b.msisdn -- berubah daily d-2
left join ${table-prefix}${niknokk-schema}.${table-prefix}niknokk_ref_dukcapil_nik_map c 
on substr(a.ss_nmbr,1,6) = c.kode_kecamatan
where a.brand<>'kartuHALO'
and date(a.trx_date) = '${run-date=yyyy-MM-dd}'
group by 1,2,3,4,5,6,7
)a
group by 1,2,3,4,5,6,7,8
) AAAA;
