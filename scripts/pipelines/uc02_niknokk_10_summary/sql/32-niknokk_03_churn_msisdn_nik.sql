-- Author: Sadar Baskoro <ssetihajid@kinetica.com>
-- Last Update: 23 Dec 2018
-- Objective: update niknokk_03_churn_msisdn_nik


create or replace table ${table-prefix}${niknokk-schema}.${table-prefix}niknokk_03_churn_msisdn_nik_tmp as
select msisdn, nik_id, brand
from ${table-prefix}${niknokk-schema}.${table-prefix}niknokk_pre_reg_${prev-month=yyyyMM}
WHERE simple_wording='Success'
AND  nik_id is not null;

create or replace table ${table-prefix}${niknokk-schema}.${table-prefix}niknokk_03_churn_msisdn_nik_tmp2 as
select msisdn
FROM ${table-prefix}${niknokk-schema}.${table-prefix}niknokk_pre_reg_${run-date=yyyyMM}
WHERE simple_wording='Success' and nik_id is not null
group by 1;

create or replace table ${table-prefix}${niknokk-schema}.${table-prefix}niknokk_03_churn_msisdn_nik_tmp3 as
select msisdn from ${table-prefix}${niknokk-schema}.${table-prefix}niknokk_03_churn_msisdn_nik_tmp group by 1
except 
select msisdn from ${table-prefix}${niknokk-schema}.${table-prefix}niknokk_03_churn_msisdn_nik_tmp2 group by 1;


create or replace table ${table-prefix}${niknokk-schema}.${table-prefix}niknokk_03_churn_msisdn_nik_tmp4 as
                SELECT a.msisdn, a.nik_id, a.brand
                FROM ${table-prefix}${niknokk-schema}.${table-prefix}niknokk_03_churn_msisdn_nik_tmp a 
			INNER JOIN ${table-prefix}${niknokk-schema}.${table-prefix}niknokk_03_churn_msisdn_nik_tmp3 b 
		on a.msisdn = b.msisdn
                GROUP BY 1,2,3;



delete from ${table-prefix}${niknokk-schema}.${table-prefix}niknokk_03_churn_msisdn_nik
where mth = '${run-date=yyyyMM}';

INSERT INTO ${table-prefix}${niknokk-schema}.${table-prefix}niknokk_03_churn_msisdn_nik
SELECT 
	'${run-date=yyyyMM}' mth,
	count(a.msisdn) subs,
        count(DISTINCT a.nik_id) nik
FROM ${table-prefix}${niknokk-schema}.${table-prefix}niknokk_03_churn_msisdn_nik_tmp4 a;

drop table if exists ${table-prefix}${niknokk-schema}.${table-prefix}niknokk_03_churn_msisdn_nik_tmp; 

drop table if exists ${table-prefix}${niknokk-schema}.${table-prefix}niknokk_03_churn_msisdn_nik_tmp2;

drop table if exists ${table-prefix}${niknokk-schema}.${table-prefix}niknokk_03_churn_msisdn_nik_tmp3;  

drop table if exists ${table-prefix}${niknokk-schema}.${table-prefix}niknokk_03_churn_msisdn_nik_tmp4;
