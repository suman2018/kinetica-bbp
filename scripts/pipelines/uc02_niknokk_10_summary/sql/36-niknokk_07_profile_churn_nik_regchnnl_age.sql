-- Author: Sadar Baskoro <ssetihajid@kinetica.com>
-- Last Update: 23 Dec 2018
-- Objective: update niknokk_07_profile_churn_nik_regchnnl

create or replace table ${table-prefix}${niknokk-schema}.${table-prefix}niknokk_07_profile_churn_nik_tmp as
SELECT a.msisdn, a.reg_chnl, a.nik_id, a.reg_dt, a.nik_dob, KI_SHARD_KEY(a.nik_id)
        FROM ${table-prefix}${niknokk-schema}.${table-prefix}niknokk_multidim_${prev-month=yyyyMM} a
        where nik_id is not null;

create or replace table ${table-prefix}${niknokk-schema}.${table-prefix}niknokk_07_profile_churn_nik_tmp2 as
SELECT  a.msisdn, a.reg_chnl, a.nik_id, a.reg_dt, a.nik_dob
        FROM ${table-prefix}${niknokk-schema}.${table-prefix}niknokk_07_profile_churn_nik_tmp a inner join 
	(select nik from ${table-prefix}${niknokk-schema}.${table-prefix}niknokk_churn_reg_${run-date=yyyyMM} group by 1) b 
	on a.nik_id = b.nik;
 

delete from ${table-prefix}${niknokk-schema}.${table-prefix}niknokk_07_profile_churn_nik_regchnnl
where mth = '${prev-month=yyyyMM}';


INSERT INTO ${table-prefix}${niknokk-schema}.${table-prefix}niknokk_07_profile_churn_nik_regchnnl
SELECT 
	'${prev-month=yyyyMM}' mth,
	nvl(reg_chnl,'UNKNOWN') reg_chnl,
	count(distinct nik_id) as nik
FROM ${table-prefix}${niknokk-schema}.${table-prefix}niknokk_07_profile_churn_nik_tmp2
group by 2;


delete from ${table-prefix}${niknokk-schema}.${table-prefix}niknokk_07_profile_churn_nik_age
where mth = '${prev-month=yyyyMM}';


INSERT INTO ${table-prefix}${niknokk-schema}.${table-prefix}niknokk_07_profile_churn_nik_age
SELECT 
	'${prev-month=yyyyMM}' mth,
        nvl(year(reg_dt) - year(nik_dob),-99) AS age,
        Count(DISTINCT nik_id) nik
FROM ${table-prefix}${niknokk-schema}.${table-prefix}niknokk_07_profile_churn_nik_tmp2
group by 2;

drop table if exists ${table-prefix}${niknokk-schema}.${table-prefix}niknokk_07_profile_churn_nik_tmp;
drop table if exists ${table-prefix}${niknokk-schema}.${table-prefix}niknokk_07_profile_churn_nik_tmp2;

