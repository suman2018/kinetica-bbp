-- Author: Sadar Baskoro <ssetihajid@kinetica.com>
-- Last Update: 23 Dec 2018
-- Objective: update niknokk_08_churn_segment

create or replace table ${table-prefix}${niknokk-schema}.${table-prefix}niknokk_08_churn_segment_tmp1 as
        SELECT '${run-date=yyyy-MM-dd}' churn_date, msisdn,brand, MAX(datex) dt_dt
        FROM  ${table-prefix}${niknokk-schema}.${table-prefix}niknokk_churn_pre
        WHERE DATE(datex) >= concat(substring('${run-date=yyyy-MM-dd}',1,7),'-01')
        AND DATE(datex) <= '${run-date=yyyy-MM-dd}'
        GROUP BY 2,3;

create or replace table ${table-prefix}${niknokk-schema}.${table-prefix}niknokk_08_churn_segment_tmp2 as
select msisdn from ${table-prefix}${niknokk-schema}.${table-prefix}niknokk_08_churn_segment_tmp1 group by 1
except 
select msisdn from ${table-prefix}${niknokk-schema}.${table-prefix}niknokk_multidim_${run-date=yyyyMM}
where date(trx_date) = '${run-date=yyyy-MM-dd}'
group by 1;


create or replace table ${table-prefix}${niknokk-schema}.${table-prefix}niknokk_08_churn_segment_tmp3 as
select a.churn_date, a.msisdn, a.brand, a.dt_dt, b.msisdn as msisdn_1, b.simple_wording, c.msisdn as msisdn_2, c.activation_date, d.msisdn as msisdn_3, d.total_revenue as total_revenue_3
from ${table-prefix}${niknokk-schema}.${table-prefix}niknokk_08_churn_segment_tmp1 a 
LEFT JOIN
(
        SELECT msisdn, simple_wording
        FROM ${table-prefix}${niknokk-schema}.${table-prefix}niknokk_preblk_ccm_cln_baru
        GROUP BY 1,2
) b ON a.msisdn=b.msisdn
LEFT JOIN ${table-prefix}${niknokk-schema}.${table-prefix}niknokk_cb_prepaid_postpaid_${prev-month=yyyyMM} c on a.msisdn = c.msisdn
LEFT JOIN ${table-prefix}${niknokk-schema}.${table-prefix}niknokk_cb_prepaid_postpaid_${month-minus-three=yyyyMM} d on a.msisdn = d.msisdn
INNER JOIN ${table-prefix}${niknokk-schema}.${table-prefix}niknokk_08_churn_segment_tmp2 e on a.msisdn = e.msisdn;

create or replace table ${table-prefix}${niknokk-schema}.${table-prefix}niknokk_08_churn_segment_tmp4 as 
select churn_date,
       nvl(brand,'UNKNOWN') brand,
        CASE 
                WHEN simple_wording='Success' THEN 'Success' 
                WHEN msisdn_1 IS NULL THEN 'No Try' 
		ELSE 'Failed' 
        END AS registration_status,
        CASE
                WHEN activation_date IS NULL THEN 'Unknown'
                WHEN datediff('day',dt_dt,activation_date) <= 90 THEN '0-3 mth'
                WHEN datediff('day',dt_dt,activation_date) <= 180 THEN '3-6 mth'
                WHEN datediff('day',dt_dt,activation_date) <= 360 THEN '6-12 mth'
                WHEN datediff('day',dt_dt,activation_date) > 360 THEN '>12 mth'
        END AS los_churn,
        nvl(COUNT(DISTINCT msisdn),0) subs,
        nvl(SUM(total_revenue_3),0) rev
from ${table-prefix}${niknokk-schema}.${table-prefix}niknokk_08_churn_segment_tmp3
group by 1,2,3,4; 

delete from ${table-prefix}${niknokk-schema}.${table-prefix}niknokk_08_churn_segment
where churn_date = '${run-date=yyyy-MM-dd}';

INSERT INTO ${table-prefix}${niknokk-schema}.${table-prefix}niknokk_08_churn_segment
SELECT
	churn_date, 
	brand,
	registration_status,
	los_churn,
	subs,
	rev
FROM ${table-prefix}${niknokk-schema}.${table-prefix}niknokk_08_churn_segment_tmp4;

drop table if exists ${table-prefix}${niknokk-schema}.${table-prefix}niknokk_08_churn_segment_tmp1;
drop table if exists ${table-prefix}${niknokk-schema}.${table-prefix}niknokk_08_churn_segment_tmp2;
drop table if exists ${table-prefix}${niknokk-schema}.${table-prefix}niknokk_08_churn_segment_tmp3;
drop table if exists ${table-prefix}${niknokk-schema}.${table-prefix}niknokk_08_churn_segment_tmp4;

