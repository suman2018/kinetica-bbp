delete from ${table-prefix}${cms-schema}.${table-prefix}cms_rcg_detail
where file_date = date('${run-date=yyyy-MM-dd}')
;

insert into ${table-prefix}${cms-schema}.${table-prefix}cms_rcg_detail
select
	trx_date
	,msisdn
	,trx_rech
	,rech
	,denom
	,sender_user_id
	,date('${run-date=yyyy-MM-dd}') as file_date
	,'${job-id}' as job_id
	,'${pipeline-name}' as pipeline_name
	,'${pipeline-step}' as pipeline_step
from ${table-prefix}${cms_stg-schema}.${table-prefix}cms_rcg_input
;

delete from ${table-prefix}${cms-schema}.${table-prefix}cms_rcg_detail
where file_date < date('${run-date=yyyy-MM-dd}') - interval '30' day
;
