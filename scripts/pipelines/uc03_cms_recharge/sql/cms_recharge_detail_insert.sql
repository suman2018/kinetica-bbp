delete from ${table-prefix}${cms-schema}.${table-prefix}cms_campaign_tracking_detail
where file_date = date('${run-date=yyyy-MM-dd}')
and pipeline_name = '${pipeline-name}'
and pipeline_step = '${pipeline-step}';

--recharge denomination
/* KI_HINT_DICT_PROJECTION */
create or replace temp table ${table-prefix}${cms-schema}.${table-prefix}${pipeline-name}_tmp
as
select 
	cview.category
	,cview.campaign_type
	,cview.msisdn
	,cview.campaign_id
	,cview.campaign_objective_id
	,cview.campaign_name
	,cview.start_period
	,cview.end_period
	,cview.iscontrolgroup
	,cview.campaign_group_id
	,cview.segment
	,cview.los
	,cview.arpu
	,cview.business_group
	,cview.initiated_group
	,cview.region_hlr
	,cview.cluster
	,cview.kabupaten
	,coalesce(rcg.TRX_DATE, date('${run-date=yyyy-MM-dd}')) as trx_date
	,rcg.denom*rcg.trx_rech as metric_1
	,rcg.trx_rech as metric_2
	,qta.quota as metric_3
	,case
		when cview.eligible > 0
			and rcg.denom = crules.value_identify
			and sender.value_identify is not null
			and (sender_cnt.hassender = 1 or sender_cnt.haschannel = 1) 
			then '1' 
		when cview.eligible > 0
			and rcg.denom = crules.value_identify
			and sender_cnt.hascms = 1
			and sender_cnt.hassender = 0 
			and sender_cnt.haschannel = 0 
			then '1'
		else '0' 
	end as content_1
	,0 as is_btl
	,cview.target
	,cview.eligible
	,0 as taker
	,date('${run-date=yyyy-MM-dd}') as file_date
	,'${job-id}'
	,'${pipeline-name}'
	,'${pipeline-step}'
from 
(
	select distinct mltdim.*
	from ${table-prefix}${cms-schema}.${table-prefix}cms_campaign_trackingstream_multidim_v mltdim
	join
	(
		select distinct trx_date
		from ${table-prefix}${cms-schema}.${table-prefix}cms_rcg_detail
		where file_date = date('${run-date=yyyy-MM-dd}')
	) trx_date
	on trx_date.trx_date between date(mltdim.start_period) and date(mltdim.end_period)
	where campaign_group_id = 2
		and campaign_name like '%RECH_DNM%'
) cview
left join
(
	select campaign_id, 
	count(distinct case when taker_identify = 'SENDERUSERID' then campaign_id end) hassender,
	count(distinct case when taker_identify = 'CHANNEL' and value_identify = 'CMS' then campaign_id end) hascms,
	count(distinct case when taker_identify = 'CHANNEL' and value_identify <> 'CMS' then campaign_id end) haschannel
	from ${table-prefix}${cms-schema}.${table-prefix}cms_campaign_rules
	group by 1
) sender_cnt
on sender_cnt.campaign_id = cview.campaign_id
left join 
(
	select 
		msisdn
		,trx_date
		,denom
		,sender_user_id
		,trx_rech
	from ${table-prefix}${cms-schema}.${table-prefix}cms_rcg_detail
	where file_date = date('${run-date=yyyy-MM-dd}')
) rcg
on rcg.msisdn = cview.msisdn
	and rcg.trx_date between date(cview.start_period) and date(cview.end_period)
left join
(
	select distinct campaign_id, value_identify
	from ${table-prefix}${cms-schema}.${table-prefix}cms_campaign_rules
	where taker_identify in ('SENDERUSERID','CHANNEL')
	group by 1,2
) sender
on sender.value_identify like '%'||rcg.sender_user_id||'%'
	and sender.campaign_id = cview.campaign_id
left join 
(
	select distinct
		campaign_id
		,value_identify as value_identify
	from ${table-prefix}${cms-schema}.${table-prefix}cms_campaign_rules
	where taker_identify in ('CREDIT', 'AMOUNT')
) crules
on cview.campaign_id = crules.campaign_id
left join
(
	select quota, campaign_id
	from ${table-prefix}${cms-schema}.${table-prefix}cms_campaign_quota
) qta
on cview.campaign_id = qta.campaign_id
;

insert into ${table-prefix}${cms-schema}.${table-prefix}cms_campaign_tracking_detail
(
	 category
	,campaign_type
	,msisdn
	,campaign_id
	,campaign_objective_id
	,campaign_name
	,start_period
	,end_period
	,iscontrolgroup
	,campaign_group_id
	,segment
	,los
	,arpu
	,business_group
	,initiated_group
	,region_hlr
	,cluster
	,branch
	,trx_date
	,metric_1
	,metric_2
	,metric_3
	,content_1
	,is_btl
	,target
	,eligible
	,taker
	,file_date
	,orch_job_id
	,pipeline_name
	,pipeline_step
)
select * from ${table-prefix}${cms-schema}.${table-prefix}${pipeline-name}_tmp
;

--update takers recharge denomination
create or replace table ${table-prefix}${cms-schema}.${table-prefix}${pipeline-name}_tmp
as
	select 
	category
	,campaign_type
	,msisdn
	,campaign_id
	,campaign_objective_id
	,campaign_name
	,start_period
	,end_period
	,iscontrolgroup
	,campaign_group_id
	,segment
	,los
	,arpu
	,business_group
	,initiated_group
	,region_hlr
	,cluster
	,branch
	,trx_date
	,content_1
	,content_2
	,content_3
	,metric_1
	,metric_2
	,metric_3
	,is_btl
	,target
	,eligible
	,case when (count(*) over (partition by campaign_id, msisdn order by trx_date rows between unbounded preceding and current row) <= metric_3) or metric_3 is null then 1 else 0 end as taker
	,file_date
	,orch_job_id
	,pipeline_name
	,pipeline_step
	from ${table-prefix}${cms-schema}.${table-prefix}cms_campaign_tracking_detail
	where campaign_group_id = 2
		and upper(campaign_name) like '%RECH_DNM%'
		and date('${run-date=yyyy-MM-dd}') between date(start_period) and date(end_period)
		and metric_1 > 0
		and content_1 = '1'
		and eligible > 0
;

delete from ${table-prefix}${cms-schema}.${table-prefix}cms_campaign_tracking_detail
where campaign_group_id = 2
	and upper(campaign_name) like '%RECH_DNM%'
	and date('${run-date=yyyy-MM-dd}') between date(start_period) and date(end_period)
	and metric_1 > 0
	and eligible > 0
	and content_1 = '1'
;

insert into ${table-prefix}${cms-schema}.${table-prefix}cms_campaign_tracking_detail
select * from ${table-prefix}${cms-schema}.${table-prefix}${pipeline-name}_tmp
;




--recharge accumulation with no sender
/* KI_HINT_DICT_PROJECTION */
create or replace temp table ${table-prefix}${cms-schema}.${table-prefix}${pipeline-name}_tmp
as
select 
	cview.category
	,cview.campaign_type
	,cview.msisdn
	,cview.campaign_id
	,cview.campaign_objective_id
	,cview.campaign_name
	,cview.start_period
	,cview.end_period
	,cview.iscontrolgroup
	,cview.campaign_group_id
	,cview.segment
	,cview.los
	,cview.arpu
	,cview.business_group
	,cview.initiated_group
	,cview.region_hlr
	,cview.cluster
	,cview.kabupaten
	,coalesce(rcg.TRX_DATE, date('${run-date=yyyy-MM-dd}')) as trx_date
	,rcg.denom*rcg.trx_rech as metric_1
	,rcg.trx_rech as metric_2
	,crules.value_identify as metric_3
	,0 as is_btl
	,cview.target
	,cview.eligible
	,0 as taker
	,date('${run-date=yyyy-MM-dd}') as file_date
	,'${job-id}'
	,'${pipeline-name}'
	,'${pipeline-step}'
from 
(
	select distinct mltdim.*
	from ${table-prefix}${cms-schema}.${table-prefix}cms_campaign_trackingstream_multidim_v mltdim
	join
	(
		select distinct trx_date
		from ${table-prefix}${cms-schema}.${table-prefix}cms_rcg_detail
		where file_date = date('${run-date=yyyy-MM-dd}')
	) trx_date
	on trx_date.trx_date between date(mltdim.start_period) and date(mltdim.end_period)
	left join 
	(
		select campaign_id, 
		count(distinct case when taker_identify = 'SENDERUSERID' then campaign_id end) hassender,
		count(distinct case when taker_identify = 'CHANNEL' and value_identify = 'CMS' then campaign_id end) hascms,
		count(distinct case when taker_identify = 'CHANNEL' and value_identify <> 'CMS' then campaign_id end) haschannel
		from ${table-prefix}${cms-schema}.${table-prefix}cms_campaign_rules
		group by 1
	) cmp_exclude 
	on cmp_exclude.campaign_id =  mltdim.campaign_id
	where campaign_group_id = 2
		and campaign_name like '%RECH_ACC%'
		and cmp_exclude.hascms = 1 
		and cmp_exclude.hassender = 0
		and cmp_exclude.haschannel = 0
) cview
left join 
(
	select 
		msisdn
		,trx_date
		,denom
		,sender_user_id
		,sum(trx_rech) as trx_rech
	from ${table-prefix}${cms-schema}.${table-prefix}cms_rcg_detail
	where file_date = date('${run-date=yyyy-MM-dd}')
	group by
		msisdn
		,trx_date
		,denom
		,sender_user_id
) rcg
on rcg.msisdn = cview.msisdn
	and rcg.trx_date between date(cview.start_period) and date(cview.end_period)
left join 
(
	select distinct
		campaign_id
		,int(value_identify) as value_identify
	from ${table-prefix}${cms-schema}.${table-prefix}cms_campaign_rules
	where taker_identify = 'SUM_AMOUNT'
) crules
on cview.campaign_id = crules.campaign_id
;


insert into ${table-prefix}${cms-schema}.${table-prefix}cms_campaign_tracking_detail
(
	 category
	,campaign_type
	,msisdn
	,campaign_id
	,campaign_objective_id
	,campaign_name
	,start_period
	,end_period
	,iscontrolgroup
	,campaign_group_id
	,segment
	,los
	,arpu
	,business_group
	,initiated_group
	,region_hlr
	,cluster
	,branch
	,trx_date
	,metric_1
	,metric_2
	,metric_3
	,is_btl
	,target
	,eligible
	,taker
	,file_date
	,orch_job_id
	,pipeline_name
	,pipeline_step
)
select * from ${table-prefix}${cms-schema}.${table-prefix}${pipeline-name}_tmp
;

--recharege accumulation with sender
/* KI_HINT_DICT_PROJECTION */
create or replace temp table ${table-prefix}${cms-schema}.${table-prefix}${pipeline-name}_tmp
as
select 
	cview.category
	,cview.campaign_type
	,cview.msisdn
	,cview.campaign_id
	,cview.campaign_objective_id
	,cview.campaign_name
	,cview.start_period
	,cview.end_period
	,cview.iscontrolgroup
	,cview.campaign_group_id
	,cview.segment
	,cview.los
	,cview.arpu
	,cview.business_group
	,cview.initiated_group
	,cview.region_hlr
	,cview.cluster
	,cview.kabupaten
	,coalesce(rcg.TRX_DATE, date('${run-date=yyyy-MM-dd}')) as trx_date
	,rcg.denom*rcg.trx_rech as metric_1
	,rcg.trx_rech as metric_2
	,crules.value_identify as metric_3
	,char64(crules.value_identify) as content_1
	,0 as is_btl
	,cview.target
	,cview.eligible
	,0 as taker
    ,date('${run-date=yyyy-MM-dd}') as file_date
	,'${job-id}'
	,'${pipeline-name}'
	,'${pipeline-step}'
from 
(
	select distinct mltdim.*
	from ${table-prefix}${cms-schema}.${table-prefix}cms_campaign_trackingstream_multidim_v mltdim
	join
	(
		select distinct trx_date
		from ${table-prefix}${cms-schema}.${table-prefix}cms_rcg_detail
		where file_date = date('${run-date=yyyy-MM-dd}')
	) trx_date
	on trx_date.trx_date between date(mltdim.start_period) and date(mltdim.end_period)
	left join 
	(
		select campaign_id, 
		count(distinct case when taker_identify = 'SENDERUSERID' then campaign_id end) hassender,
		count(distinct case when taker_identify = 'CHANNEL' and value_identify = 'CMS' then campaign_id end) hascms,
		count(distinct case when taker_identify = 'CHANNEL' and value_identify <> 'CMS' then campaign_id end) haschannel
		from ${table-prefix}${cms-schema}.${table-prefix}cms_campaign_rules
		group by 1
	) cmp_include 
	on cmp_include.campaign_id =  mltdim.campaign_id
	where campaign_group_id = 2
		and campaign_name like '%RECH_ACC%'
		and (cmp_include.hassender = 1 or cmp_include.haschannel = 1 )
) cview
join
(
    select distinct campaign_id, value_identify
    from ${table-prefix}${cms-schema}.${table-prefix}cms_campaign_rules
    where taker_identify in ('SENDERUSERID','CHANNEL')
    group by 1,2
) sender
on sender.campaign_id = cview.campaign_id
left join 
(
	select 
		msisdn
		,trx_date
		,denom
		,sender_user_id
		,sum(trx_rech) as trx_rech
	from ${table-prefix}${cms-schema}.${table-prefix}cms_rcg_detail
	where file_date = date('${run-date=yyyy-MM-dd}')
	group by
		msisdn
		,trx_date
		,denom
		,sender_user_id
) rcg
on rcg.msisdn = cview.msisdn
	and sender.value_identify like '%'||rcg.sender_user_id||'%'
	and rcg.trx_date between date(cview.start_period) and date(cview.end_period)
left join 
(
	select distinct
		campaign_id
		,int(value_identify) as value_identify
	from ${table-prefix}${cms-schema}.${table-prefix}cms_campaign_rules
	where taker_identify = 'SUM_AMOUNT'
) crules
on cview.campaign_id = crules.campaign_id
;


insert into ${table-prefix}${cms-schema}.${table-prefix}cms_campaign_tracking_detail
(
	 category
	,campaign_type
	,msisdn
	,campaign_id
	,campaign_objective_id
	,campaign_name
	,start_period
	,end_period
	,iscontrolgroup
	,campaign_group_id
	,segment
	,los
	,arpu
	,business_group
	,initiated_group
	,region_hlr
	,cluster
	,branch
	,trx_date
	,metric_1
	,metric_2
	,metric_3
	,content_1
	,is_btl
	,target
	,eligible
	,taker
	,file_date
	,orch_job_id
	,pipeline_name
	,pipeline_step
)
select * from ${table-prefix}${cms-schema}.${table-prefix}${pipeline-name}_tmp
;

-- devide 2 tmp table
create or replace table ${table-prefix}${cms-schema}.${table-prefix}${pipeline-name}_tmp1
as
   select
        category
        ,campaign_type
        ,msisdn
        ,campaign_id
        ,campaign_objective_id
        ,campaign_name
        ,start_period
        ,end_period
        ,iscontrolgroup
        ,campaign_group_id
        ,segment
        ,los
        ,arpu
        ,business_group
        ,initiated_group
        ,region_hlr
        ,cluster
        ,branch
        ,trx_date
        ,content_1
        ,content_2
        ,content_3
        ,metric_1
        ,metric_2
        ,metric_3
        ,is_btl
        ,target
        ,eligible
        ,case when coalesce((sum(metric_1) over (partition by campaign_id, msisdn, content_1 order by trx_date rows between unbounded preceding and current row)),0) >= metric_3
        and coalesce((sum(metric_1) over (partition by campaign_id, msisdn, content_1 order by trx_date rows between unbounded preceding and 1 preceding)),0) < metric_3
         then 1 else 0 end as first_taker
        ,file_date
        ,orch_job_id
        ,pipeline_name
        ,pipeline_step
        from ${table-prefix}${cms-schema}.${table-prefix}cms_campaign_tracking_detail
        where campaign_group_id = 2
                and upper(campaign_name) like '%RECH_ACC%'
                and date('${run-date=yyyy-MM-dd}') between date(start_period) and date(end_period)
                and metric_1 > 0
                and eligible > 0
;

--update takers for recharge accumulation
create or replace table ${table-prefix}${cms-schema}.${table-prefix}${pipeline-name}_tmp
as
select
category
,campaign_type
,msisdn
,campaign_id
,campaign_objective_id
,campaign_name
,start_period
,end_period
,iscontrolgroup
,campaign_group_id
,segment
,los
,arpu
,business_group
,initiated_group
,region_hlr
,cluster
,branch
,trx_date
,content_1
,content_2
,content_3
,metric_1
,metric_2
,metric_3
,is_btl
,target
,eligible
,max(first_taker) over (partition by campaign_id, msisdn, content_1 order by trx_date desc, first_taker desc rows between unbounded preceding and current row) as taker
,file_date
,orch_job_id
,pipeline_name
,pipeline_step
from ${table-prefix}${cms-schema}.${table-prefix}${pipeline-name}_tmp1 t 
;

delete from ${table-prefix}${cms-schema}.${table-prefix}cms_campaign_tracking_detail
where campaign_group_id = 2
	and upper(campaign_name) like '%RECH_ACC%'
	and date('${run-date=yyyy-MM-dd}') between date(start_period) and date(end_period)
	and metric_1 > 0
	and eligible > 0
;

insert into ${table-prefix}${cms-schema}.${table-prefix}cms_campaign_tracking_detail
select * from ${table-prefix}${cms-schema}.${table-prefix}${pipeline-name}_tmp
;

drop table ${table-prefix}${cms-schema}.${table-prefix}${pipeline-name}_tmp
;
drop table ${table-prefix}${cms-schema}.${table-prefix}${pipeline-name}_tmp1
;

---- add below script to handle recharge campaigns with VA due VA identifier is not exist in RCG transaction table
---- set taker = 0 and recharge amount (revenue) = 0
---- below script must be deleted once VA identifier issue is solved
---- this script is updated by 8 July 2019

update ${table-prefix}${cms-schema}.${table-prefix}cms_campaign_tracking_detail
set metric_1 = 0, taker = 0
where campaign_group_id = 2
	and upper(campaign_name) like '%VA%'
	and date('${run-date=yyyy-MM-dd}') between date(start_period) and date(end_period)
;





