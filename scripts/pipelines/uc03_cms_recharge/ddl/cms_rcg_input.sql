CREATE OR REPLACE TABLE ${table-prefix}${cms_stg-schema}.${table-prefix}cms_rcg_input
(
    "trx_date" date(dict),
    "msisdn" BIGINT(shard_key),
    "trx_rech" INTEGER(dict),
    "rech" BIGINT(dict),
    "denom" INT(dict),
    "sender_user_id" varchar(64,dict)
)