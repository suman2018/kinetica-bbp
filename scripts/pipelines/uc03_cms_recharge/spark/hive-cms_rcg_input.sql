select
TRX_DATE
,cast(MSISDN as bigint)
,cast(TRX_RECH as int)
,CAST(RECH AS BIGINT) AS RECH
,cast(denom as int) as denom
,sender_user_id
from gpu.gpu_urp_daily
where datex = '${run-date=yyyyMMdd}'
	and split_code in ('1', '88')
union all
select
TRX_DATE
,cast(MSISDN as bigint)
,cast(TRX_RECH as int)
,CAST(RECH AS BIGINT) AS RECH
,cast(denom as int) as denom
,sender_user_id
from gpu.gpu_mkios_daily
where datex = '${run-date=yyyyMMdd}'
	and split_code in ('1', '88')