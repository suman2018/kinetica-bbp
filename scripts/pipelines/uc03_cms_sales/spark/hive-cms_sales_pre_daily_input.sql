select
string(date(timestamp(from_unixtime(unix_timestamp(activation_date, 'yyyyMMdd'))))) as activation_date
,int(subs_id) as subs_id
,bigint(msisdn) as msisdn
,paychannel
,int(prefix) as prefix
,area_hlr
,region_hlr
,city_hlr
,int(price_plan_id) as price_plan_id
,offer
,brand
,cust_type_desc
,do_date
,dealer_code
,item_code
,dist_type
,string(date(timestamp(from_unixtime(unix_timestamp(first_usage_date, 'yyyyMMdd'))))) as first_usage_date
,int(lacci_id) as lacci_id
,int(lac) as lac
,int(ci) as ci
,cgi_prepaid
,node
,area_name
,regional_channel
,branch
,sub_branch
,cluster_sales
,kabupaten
,lctn_closing_flag
from gpu.gpu_sales_pre_daily_new
where datex = '${run-date=yyyyMMdd}'