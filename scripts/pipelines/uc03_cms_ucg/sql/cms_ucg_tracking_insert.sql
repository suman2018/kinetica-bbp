delete from ${table-prefix}${cms-schema}.${table-prefix}cms_ucg_tracking
where file_date = date('${run-date=yyyy-MM-dd}');

/* KI_HINT_DICT_PROJECTION */
create or replace temp table ${table-prefix}${cms-schema}.${table-prefix}tracking_temp_step_1
as
select
    last_day(prepost.rep_month) as rep_month
    ,prepost.msisdn as msisdn
    ,prepostbase.brand as brand
    ,prepostbase.area_lacci as area
    ,case when prepostbase.region_lacci = '' then 
		case when lower(prepostbase.region_hlr) like '%sumbagut%' then '01.Sumbagut'
			 when lower(prepostbase.region_hlr) like '%sumbagteng%' then '02.Sumbagteng'
			 when lower(prepostbase.region_hlr) like '%sumbagsel%' then '03.Sumbagsel'
			 when lower(prepostbase.region_hlr) like '%western jabotabek%' then '04.Western Jabotabek'
			 when lower(prepostbase.region_hlr) like '%central jabotabek%' then '05.Central Jabotabek'
			 when lower(prepostbase.region_hlr) like '%eastern jabotabek%' then '06.Eastern Jabotabek'
			 when lower(prepostbase.region_hlr) like '%jabotabek%' and lower(prepostbase.city_hlr) like '%serang%' then '04.Western Jabotabek'
			 when lower(prepostbase.region_hlr) like '%jabotabek%' and lower(prepostbase.city_hlr) like '%bogor%' then '06.Eastern Jabotabek'
			 when lower(prepostbase.region_hlr) like '%jabotabek%' and lower(prepostbase.city_hlr) like '%jakarta%' then '05.Central Jabotabek'
			 when lower(prepostbase.region_hlr) like '%jabotabek%' and lower(prepostbase.city_hlr) like '%karawang%' then '06.Eastern Jabotabek'
			 when lower(prepostbase.region_hlr) like '%jabar%' then '07.Jabar'
			 when lower(prepostbase.region_hlr) like '%jateng%' then '08.Jateng'
			 when lower(prepostbase.region_hlr) like '%jatim%' then '09.Jatim'
			 when lower(prepostbase.region_hlr) like '%balinusra%' then '10.Balinusra'
			 when lower(prepostbase.region_hlr) like '%kalimantan%' then '11.Kalimantan'
			 when lower(prepostbase.region_hlr) like '%sulawesi%' then '12.Sulawesi'
			 when lower(prepostbase.region_hlr) like '%puma%' then '13.Puma'
			 when lower(prepostbase.region_hlr) like '%papua%' then '13.Puma'
		else prepostbase.region_hlr end 
	else prepostbase.region_lacci end as region_lacci
    ,prepostbase.region_hlr as region_hlr
    ,prepostbase.city_hlr as city_hlr
    ,prepostbase.los as los
    ,prepostbase.device_type as device_type
    ,prepostbase.total_revenue as total_revenue_base
    ,prepostbase.rev_broadband as rev_broadband
    ,prepost.total_revenue as total_revenue
    ,1 as is_ucg
	,date('${run-date=yyyy-MM-dd}') as file_date
	,0 as a
	,0 as b
from ${table-prefix}${cms_stg-schema}.${table-prefix}cms_pre_post_input prepost
inner join ${table-prefix}${cms_stg-schema}.${table-prefix}cms_pre_post_input prepostbase
	on prepost.msisdn = prepostbase.msisdn
	and prepostbase.rep_month = (select date(max(rep_month) - interval '1' month) from ${table-prefix}${cms_stg-schema}.${table-prefix}cms_pre_post_input where datetime(rep_month) < (select datetime(max(last_day(description))) from ${table-prefix}${cms_stg-schema}.${table-prefix}cms_ucg_list_input where last_day(description) < last_day('${run-date=yyyy-MM-dd}')))
inner join ${table-prefix}${cms_stg-schema}.${table-prefix}cms_ucg_list_input ucg
	on prepost.msisdn = ucg.msisdn
	and datetime(ucg.description) = (select datetime(max(last_day(description))) from ${table-prefix}${cms_stg-schema}.${table-prefix}cms_ucg_list_input where datetime(last_day(description)) < datetime('${run-date=yyyy-MM-dd}'))
where prepost.rep_month = (select max(rep_month) from ${table-prefix}${cms_stg-schema}.${table-prefix}cms_pre_post_input where datetime(rep_month) = datetime(last_day('${run-date=yyyy-MM-dd}')))
union all
select
    last_day(prepost.rep_month) as rep_month
    ,prepost.msisdn as msisdn
    ,prepostbase.brand as brand
    ,prepostbase.area_lacci as area
    ,case when prepostbase.region_lacci = '' then 
		case when lower(prepostbase.region_hlr) like '%sumbagut%' then '01.Sumbagut'
			 when lower(prepostbase.region_hlr) like '%sumbagteng%' then '02.Sumbagteng'
			 when lower(prepostbase.region_hlr) like '%sumbagsel%' then '03.Sumbagsel'
			 when lower(prepostbase.region_hlr) like '%western jabotabek%' then '04.Western Jabotabek'
			 when lower(prepostbase.region_hlr) like '%central jabotabek%' then '05.Central Jabotabek'
			 when lower(prepostbase.region_hlr) like '%eastern jabotabek%' then '06.Eastern Jabotabek'
			 when lower(prepostbase.region_hlr) like '%jabotabek%' and lower(prepostbase.city_hlr) like '%serang%' then '04.Western Jabotabek'
			 when lower(prepostbase.region_hlr) like '%jabotabek%' and lower(prepostbase.city_hlr) like '%bogor%' then '06.Eastern Jabotabek'
			 when lower(prepostbase.region_hlr) like '%jabotabek%' and lower(prepostbase.city_hlr) like '%jakarta%' then '05.Central Jabotabek'
			 when lower(prepostbase.region_hlr) like '%jabotabek%' and lower(prepostbase.city_hlr) like '%karawang%' then '06.Eastern Jabotabek'
			 when lower(prepostbase.region_hlr) like '%jabar%' then '07.Jabar'
			 when lower(prepostbase.region_hlr) like '%jateng%' then '08.Jateng'
			 when lower(prepostbase.region_hlr) like '%jatim%' then '09.Jatim'
			 when lower(prepostbase.region_hlr) like '%balinusra%' then '10.Balinusra'
			 when lower(prepostbase.region_hlr) like '%kalimantan%' then '11.Kalimantan'
			 when lower(prepostbase.region_hlr) like '%sulawesi%' then '12.Sulawesi'
			 when lower(prepostbase.region_hlr) like '%puma%' then '13.Puma'
			 when lower(prepostbase.region_hlr) like '%papua%' then '13.Puma'
		else prepostbase.region_hlr end 
	else prepostbase.region_lacci end as region_lacci
    ,prepostbase.region_hlr as region_hlr
    ,prepostbase.city_hlr as city_hlr
    ,prepostbase.los as los
    ,prepostbase.device_type as device_type
    ,prepostbase.total_revenue as total_revenue_base
    ,prepostbase.rev_broadband as rev_broadband
    ,prepost.total_revenue as total_revenue
    ,0 as is_ucg
	,date('${run-date=yyyy-MM-dd}') as file_date
	,case when bl.msisdn is not null then bl.msisdn else 0 end as a
	,case when ucg.msisdn is not null then ucg.msisdn else 0 end as b
from ${table-prefix}${cms_stg-schema}.${table-prefix}cms_pre_post_input prepost
inner join ${table-prefix}${cms_stg-schema}.${table-prefix}cms_pre_post_input prepostbase
	on prepost.msisdn = prepostbase.msisdn
	and prepostbase.rep_month = (select date(max(rep_month) - interval '1' month) from ${table-prefix}${cms_stg-schema}.${table-prefix}cms_pre_post_input where datetime(rep_month) < (select datetime(max(last_day(description))) from ${table-prefix}${cms_stg-schema}.${table-prefix}cms_ucg_list_input where last_day(description) < last_day('${run-date=yyyy-MM-dd}')))
	and lower(prepostbase.status) like 'active'
left join ${table-prefix}${cms_stg-schema}.${table-prefix}cms_ara_blacklist_input bl
	on prepost.msisdn = bl.msisdn
	and lower(bl.reason) in ('blacklist_global_api', 'vvip', 'blacklist', 'global', 'complainthandling')
left join ${table-prefix}${cms_stg-schema}.${table-prefix}cms_ucg_list_input ucg
	on prepost.msisdn = ucg.msisdn	
	and datetime(ucg.description) = (select datetime(max(last_day(description))) from ${table-prefix}${cms_stg-schema}.${table-prefix}cms_ucg_list_input where last_day(description) < last_day('${run-date=yyyy-MM-dd}'))
where prepost.rep_month = (select max(rep_month) from ${table-prefix}${cms_stg-schema}.${table-prefix}cms_pre_post_input where datetime(rep_month) = datetime(last_day('${run-date=yyyy-MM-dd}')))
;

create or replace temp table ${table-prefix}${cms-schema}.${table-prefix}tracking_temp_step_2
as
select
	rep_month
    ,msisdn
    ,brand
    ,case when area = '' or lower(area) = 'unknown' then
    	  case when lower(region_lacci) in ('01.sumbagut','02.sumbagteng','03.sumbagsel') then 'AREA 1'
		       when lower(region_lacci) in ('04.western jabotabek','05.central jabotabek','06.eastern jabotabek','07.jabar') then 'AREA 2'
		       when lower(region_lacci) in ('08.jateng','09.jatim','10.balinusra') then 'AREA 3'
		       when lower(region_lacci) in ('11.kalimantan','12.sulawesi','13.puma') then 'AREA 4'
		  else 'UNKNOWN' end
     else area
   	 end as area_lacci
	,region_lacci
    ,region_hlr
    ,city_hlr
    ,los
    ,device_type
    ,total_revenue_base 
    ,rev_broadband
    ,total_revenue
    ,is_ucg
    ,a
    ,b
    ,file_date
from ${table-prefix}${cms-schema}.${table-prefix}tracking_temp_step_1
;

insert into ${table-prefix}${cms-schema}.${table-prefix}cms_ucg_tracking
select 
	rep_month
    ,msisdn
    ,brand
    ,area_lacci
	,region_lacci
    ,region_hlr
    ,city_hlr
    ,los
    ,device_type
    ,total_revenue_base
    ,rev_broadband
    ,total_revenue
    ,is_ucg
	,file_date
	,case when coalesce(los,0)>=0 and coalesce(los,0)<90 then '01. LOS 0 - 3 month'
	      when coalesce(los,0)>=90 and coalesce(los,0)<180 then '02. LOS 3 - 6 month'
		  when coalesce(los,0)>=180 and coalesce(los,0)<360 then '03. LOS 6 - 12 month'
	      when coalesce(los,0)>=360 then '04. LOS > 12 month'
	 else '05. UNK' end as los_segment
	,case when coalesce(rev_broadband,0)=0 then '1. REV_DATA = 0'
	   	  when coalesce(rev_broadband,0)>0 and coalesce(rev_broadband,0)<=20000 then '2. REV_DATA 0 - 20K'
	      when coalesce(rev_broadband,0)>20000 and coalesce(rev_broadband,0)<=70000 then '3. REV_DATA 20K - 70K'
	 else '4. REV_DATA > 70K' end as rev_data_segment
	,case when lower(device_type) like '%smartphone%' then '1. Smartphone' else '2. Non Smartphone' end as smartphone_user
	,case when total_revenue_base=0 then '01. 0k'
		  when total_revenue_base>0 and total_revenue_base<=1000 then '02. 0-1000'
		  when total_revenue_base>1000 and total_revenue_base<=2000 then '03. 1000-2000'
		  when total_revenue_base>2000 and total_revenue_base<=3000 then '04. 2000-3000'
		  when total_revenue_base>3000 and total_revenue_base<=4000 then '05. 3000-4000'
		  when total_revenue_base>4000 and total_revenue_base<=5000 then '06. 4000-5000'
		  when total_revenue_base>5000 and total_revenue_base<=6000 then '07. 5000-6000'
		  when total_revenue_base>6000 and total_revenue_base<=7000 then '08. 6000-7000'
		  when total_revenue_base>7000 and total_revenue_base<=8000 then '09. 7000-8000'
		  when total_revenue_base>8000 and total_revenue_base<=9000 then '10. 8000-9000'
		  when total_revenue_base>9000 and total_revenue_base<=10000 then '11. 9000-10000'
		  when total_revenue_base>10000 and total_revenue_base<=11000 then '12. 10000-11000'
		  when total_revenue_base>11000 and total_revenue_base<=12000 then '13. 11000-12000'
		  when total_revenue_base>12000 and total_revenue_base<=13000 then '14. 12000-13000'
		  when total_revenue_base>13000 and total_revenue_base<=14000 then '15. 13000-14000'
		  when total_revenue_base>14000 and total_revenue_base<=15000 then '16. 14000-15000'
		  when total_revenue_base>15000 and total_revenue_base<=16000 then '17. 15000-16000'
		  when total_revenue_base>16000 and total_revenue_base<=17000 then '18. 16000-17000'
		  when total_revenue_base>17000 and total_revenue_base<=18000 then '19. 17000-18000'
		  when total_revenue_base>18000 and total_revenue_base<=19000 then '20. 18000-19000'
		  when total_revenue_base>19000 and total_revenue_base<=20000 then '21. 19000-20000'
		  when total_revenue_base>20000 and total_revenue_base<=21000 then '22. 20000-21000'
		  when total_revenue_base>21000 and total_revenue_base<=22000 then '23. 21000-22000'
		  when total_revenue_base>22000 and total_revenue_base<=23000 then '24. 22000-23000'
		  when total_revenue_base>23000 and total_revenue_base<=24000 then '25. 23000-24000'
		  when total_revenue_base>24000 and total_revenue_base<=25000 then '26. 24000-25000'
		  when total_revenue_base>25000 and total_revenue_base<=26000 then '27. 25000-26000'
		  when total_revenue_base>26000 and total_revenue_base<=27000 then '28. 26000-27000'
		  when total_revenue_base>27000 and total_revenue_base<=28000 then '29. 27000-28000'
		  when total_revenue_base>28000 and total_revenue_base<=29000 then '30. 28000-29000'
		  when total_revenue_base>29000 and total_revenue_base<=30000 then '31. 29000-30000'
		  when total_revenue_base>30000 and total_revenue_base<=50000 then '32. 30000-50000'
		  when total_revenue_base>50000 and total_revenue_base<=70000 then '33. 50000-70000'
		  when total_revenue_base>70000 and total_revenue_base<=100000 then '34. 70000-100000'
		  when total_revenue_base>100000 and total_revenue_base<=150000 then '35. 100000-150000'
		  when total_revenue_base>150000 then '36. 150k++'
	else 'NA' end as arpu_group
from ${table-prefix}${cms-schema}.${table-prefix}tracking_temp_step_2
where a = 0 
	and b = 0
;

drop table ${table-prefix}${cms-schema}.${table-prefix}tracking_temp_step_1
;
drop table ${table-prefix}${cms-schema}.${table-prefix}tracking_temp_step_2
;
