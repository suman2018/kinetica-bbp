-- brand --
/* KI_HINT_DICT_PROJECTION */
create or replace temp table ${table-prefix}${cms-schema}.${table-prefix}attribute_brand_temp
as
select
	rep_month,
	brand,
	case when datetime(rep_month)=(select datetime(last_day(date(max(rep_month) - interval '2' month))) from ${table-prefix}${cms-schema}.${table-prefix}cms_ucg_tracking) then count(case when is_ucg=1 then msisdn else null end) else null end as mth1_ucg_subs,
	case when datetime(rep_month)=(select datetime(last_day(date(max(rep_month) - interval '2' month))) from ${table-prefix}${cms-schema}.${table-prefix}cms_ucg_tracking) then count(case when is_ucg=0 then msisdn else null end) else null end as mth1_pop_subs,
	case when datetime(rep_month)=(select datetime(last_day(date(max(rep_month) - interval '1' month))) from ${table-prefix}${cms-schema}.${table-prefix}cms_ucg_tracking) then count(case when is_ucg=1 then msisdn else null end) else null end as mth2_ucg_subs,
	case when datetime(rep_month)=(select datetime(last_day(date(max(rep_month) - interval '1' month))) from ${table-prefix}${cms-schema}.${table-prefix}cms_ucg_tracking) then count(case when is_ucg=0 then msisdn else null end) else null end as mth2_pop_subs,
	case when datetime(rep_month)=(select datetime(last_day(date(max(rep_month) - interval '1' month))) from ${table-prefix}${cms-schema}.${table-prefix}cms_ucg_tracking) then sum(case when is_ucg=1 then total_revenue else null end) else null end as mth2_ucg_revenue,
	case when datetime(rep_month)=(select datetime(last_day(date(max(rep_month) - interval '1' month))) from ${table-prefix}${cms-schema}.${table-prefix}cms_ucg_tracking) then sum(case when is_ucg=0 then total_revenue else null end) else null end as mth2_pop_revenue,
	case when rep_month=(select max(rep_month) from ${table-prefix}${cms-schema}.${table-prefix}cms_ucg_tracking) then sum(case when is_ucg=1 then total_revenue else null end) else null end as mth3_ucg_revenue,
	case when rep_month=(select max(rep_month) from ${table-prefix}${cms-schema}.${table-prefix}cms_ucg_tracking) then sum(case when is_ucg=0 then total_revenue else null end) else null end as mth3_pop_revenue
from ${table-prefix}${cms-schema}.${table-prefix}cms_ucg_tracking
where lower(brand) in ('simpati','loop','kartuas')
	and los >= 90
	and datetime(rep_month) <= datetime(last_day('${run-date=yyyy-MM-dd}'))
	and datetime(rep_month) >= (select datetime(max(last_day(description))) from ${table-prefix}${cms_stg-schema}.${table-prefix}cms_ucg_list_input where datetime(last_day(description)) < datetime('${run-date=yyyy-MM-dd}'))

group by 1,2 order by 2 desc
;

insert into ${table-prefix}${cms-schema}.${table-prefix}cms_ucg_attribute_brand
(
	brand,
	mth1_ucg_subs,
	mth1_pop_subs,
	mth2_ucg_subs,
	mth2_pop_subs,
	mth2_ucg_revenue,
	mth2_pop_revenue,
	mth3_ucg_revenue,
	mth3_pop_revenue,
	mth1_pop_arpu,
	mth1_ucg_arpu,
	mth2_pop_arpu,
	mth2_ucg_arpu,
	arpu_uplift,
	file_date
)
select
	brand,
	mth1_ucg_subs,
	mth1_pop_subs,
	mth2_ucg_subs,
	mth2_pop_subs,
	mth2_ucg_revenue,
	mth2_pop_revenue,
	mth3_ucg_revenue,
	mth3_pop_revenue,
	mth1_pop_arpu,
	mth1_ucg_arpu,
	mth2_pop_arpu,
	mth2_ucg_arpu,
	round(((mth2_pop_arpu-mth1_pop_arpu)-(mth2_ucg_arpu-mth1_ucg_arpu))/mth1_pop_arpu*100,2) as arpu_uplift,
	date('${run-date=yyyy-MM-dd}') as file_date
from
	(
		select
			brand,
			mth1_ucg_subs,
			mth1_pop_subs,
			mth2_ucg_subs,
			mth2_pop_subs,
			mth2_ucg_revenue,
			mth2_pop_revenue,
			mth3_ucg_revenue,
			mth3_pop_revenue,
			round(mth2_pop_revenue/mth1_pop_subs,2) as mth1_pop_arpu,
			round(mth2_ucg_revenue/mth1_ucg_subs,2) as mth1_ucg_arpu,
			round(mth3_pop_revenue/mth2_pop_subs,2) as mth2_pop_arpu,
			round(mth3_ucg_revenue/mth2_ucg_subs,2) as mth2_ucg_arpu
		from
			(
				select
					brand,
					sum(mth1_ucg_subs) as mth1_ucg_subs,
					sum(mth1_pop_subs) as mth1_pop_subs,
					sum(mth2_ucg_subs) as mth2_ucg_subs,
					sum(mth2_pop_subs) as mth2_pop_subs,
					sum(mth2_ucg_revenue) as mth2_ucg_revenue,
					sum(mth2_pop_revenue) as mth2_pop_revenue,
					sum(mth3_ucg_revenue) as mth3_ucg_revenue,
					sum(mth3_pop_revenue) as mth3_pop_revenue
				from ${table-prefix}${cms-schema}.${table-prefix}attribute_brand_temp
				group by 1
			)
	)
order by 1 desc
;

drop table ${table-prefix}${cms-schema}.${table-prefix}attribute_brand_temp
;

-- los_segment --
/* KI_HINT_DICT_PROJECTION */
create or replace temp table ${table-prefix}${cms-schema}.${table-prefix}attribute_los_segment_temp
as
select
	rep_month,
	los_segment,
	case when datetime(rep_month)=(select datetime(last_day(date(max(rep_month) - interval '2' month))) from ${table-prefix}${cms-schema}.${table-prefix}cms_ucg_tracking) then count(case when is_ucg=1 then msisdn else null end) else null end as mth1_ucg_subs,
	case when datetime(rep_month)=(select datetime(last_day(date(max(rep_month) - interval '2' month))) from ${table-prefix}${cms-schema}.${table-prefix}cms_ucg_tracking) then count(case when is_ucg=0 then msisdn else null end) else null end as mth1_pop_subs,
	case when datetime(rep_month)=(select datetime(last_day(date(max(rep_month) - interval '1' month))) from ${table-prefix}${cms-schema}.${table-prefix}cms_ucg_tracking) then count(case when is_ucg=1 then msisdn else null end) else null end as mth2_ucg_subs,
	case when datetime(rep_month)=(select datetime(last_day(date(max(rep_month) - interval '1' month))) from ${table-prefix}${cms-schema}.${table-prefix}cms_ucg_tracking) then count(case when is_ucg=0 then msisdn else null end) else null end as mth2_pop_subs,
	case when datetime(rep_month)=(select datetime(last_day(date(max(rep_month) - interval '1' month))) from ${table-prefix}${cms-schema}.${table-prefix}cms_ucg_tracking) then sum(case when is_ucg=1 then total_revenue else null end) else null end as mth2_ucg_revenue,
	case when datetime(rep_month)=(select datetime(last_day(date(max(rep_month) - interval '1' month))) from ${table-prefix}${cms-schema}.${table-prefix}cms_ucg_tracking) then sum(case when is_ucg=0 then total_revenue else null end) else null end as mth2_pop_revenue,
	case when rep_month=(select max(rep_month) from ${table-prefix}${cms-schema}.${table-prefix}cms_ucg_tracking) then sum(case when is_ucg=1 then total_revenue else null end) else null end as mth3_ucg_revenue,
	case when rep_month=(select max(rep_month) from ${table-prefix}${cms-schema}.${table-prefix}cms_ucg_tracking) then sum(case when is_ucg=0 then total_revenue else null end) else null end as mth3_pop_revenue
from ${table-prefix}${cms-schema}.${table-prefix}cms_ucg_tracking
where lower(brand) in ('simpati','loop','kartuas')
	and datetime(rep_month) <= datetime(last_day('${run-date=yyyy-MM-dd}'))
	and datetime(rep_month) >= (select datetime(max(last_day(description))) from ${table-prefix}${cms_stg-schema}.${table-prefix}cms_ucg_list_input where datetime(last_day(description)) < datetime('${run-date=yyyy-MM-dd}'))
group by 1,2 order by 2 desc
;

insert into ${table-prefix}${cms-schema}.${table-prefix}cms_ucg_attribute_los_segment
(
	los_segment,
	mth1_ucg_subs,
	mth1_pop_subs,
	mth2_ucg_subs,
	mth2_pop_subs,
	mth2_ucg_revenue,
	mth2_pop_revenue,
	mth3_ucg_revenue,
	mth3_pop_revenue,
	mth1_pop_arpu,
	mth1_ucg_arpu,
	mth2_pop_arpu,
	mth2_ucg_arpu,
	arpu_uplift,
	file_date
)
select
	los_segment,
	mth1_ucg_subs,
	mth1_pop_subs,
	mth2_ucg_subs,
	mth2_pop_subs,
	mth2_ucg_revenue,
	mth2_pop_revenue,
	mth3_ucg_revenue,
	mth3_pop_revenue,
	mth1_pop_arpu,
	mth1_ucg_arpu,
	mth2_pop_arpu,
	mth2_ucg_arpu,
	round(((mth2_pop_arpu-mth1_pop_arpu)-(mth2_ucg_arpu-mth1_ucg_arpu))/mth1_pop_arpu*100,2) as arpu_uplift,
	date('${run-date=yyyy-MM-dd}') as file_date
from
	(
		select
			los_segment,
			mth1_ucg_subs,
			mth1_pop_subs,
			mth2_ucg_subs,
			mth2_pop_subs,
			mth2_ucg_revenue,
			mth2_pop_revenue,
			mth3_ucg_revenue,
			mth3_pop_revenue,
			round(mth2_pop_revenue/mth1_pop_subs,2) as mth1_pop_arpu,
			round(mth2_ucg_revenue/mth1_ucg_subs,2) as mth1_ucg_arpu,
			round(mth3_pop_revenue/mth2_pop_subs,2) as mth2_pop_arpu,
			round(mth3_ucg_revenue/mth2_ucg_subs,2) as mth2_ucg_arpu
		from
			(
				select
					los_segment,
					sum(mth1_ucg_subs) as mth1_ucg_subs,
					sum(mth1_pop_subs) as mth1_pop_subs,
					sum(mth2_ucg_subs) as mth2_ucg_subs,
					sum(mth2_pop_subs) as mth2_pop_subs,
					sum(mth2_ucg_revenue) as mth2_ucg_revenue,
					sum(mth2_pop_revenue) as mth2_pop_revenue,
					sum(mth3_ucg_revenue) as mth3_ucg_revenue,
					sum(mth3_pop_revenue) as mth3_pop_revenue
				from ${table-prefix}${cms-schema}.${table-prefix}attribute_los_segment_temp
				group by 1
			)
	)
order by 1 desc
;

drop table ${table-prefix}${cms-schema}.${table-prefix}attribute_los_segment_temp
;

-- area --
/* KI_HINT_DICT_PROJECTION */
create or replace temp table ${table-prefix}${cms-schema}.${table-prefix}attribute_area_temp
as
select
	rep_month,
	area_lacci as area,
	case when datetime(rep_month)=(select datetime(last_day(date(max(rep_month) - interval '2' month))) from ${table-prefix}${cms-schema}.${table-prefix}cms_ucg_tracking) then count(case when is_ucg=1 then msisdn else null end) else null end as mth1_ucg_subs,
	case when datetime(rep_month)=(select datetime(last_day(date(max(rep_month) - interval '2' month))) from ${table-prefix}${cms-schema}.${table-prefix}cms_ucg_tracking) then count(case when is_ucg=0 then msisdn else null end) else null end as mth1_pop_subs,
	case when datetime(rep_month)=(select datetime(last_day(date(max(rep_month) - interval '1' month))) from ${table-prefix}${cms-schema}.${table-prefix}cms_ucg_tracking) then count(case when is_ucg=1 then msisdn else null end) else null end as mth2_ucg_subs,
	case when datetime(rep_month)=(select datetime(last_day(date(max(rep_month) - interval '1' month))) from ${table-prefix}${cms-schema}.${table-prefix}cms_ucg_tracking) then count(case when is_ucg=0 then msisdn else null end) else null end as mth2_pop_subs,
	case when datetime(rep_month)=(select datetime(last_day(date(max(rep_month) - interval '1' month))) from ${table-prefix}${cms-schema}.${table-prefix}cms_ucg_tracking) then sum(case when is_ucg=1 then total_revenue else null end) else null end as mth2_ucg_revenue,
	case when datetime(rep_month)=(select last_day(date(max(rep_month) - interval '1' month)) from ${table-prefix}${cms-schema}.${table-prefix}cms_ucg_tracking) then sum(case when is_ucg=0 then total_revenue else null end) else null end as mth2_pop_revenue,
	case when rep_month=(select max(rep_month) from ${table-prefix}${cms-schema}.${table-prefix}cms_ucg_tracking) then sum(case when is_ucg=1 then total_revenue else null end) else null end as mth3_ucg_revenue,
	case when rep_month=(select max(rep_month) from ${table-prefix}${cms-schema}.${table-prefix}cms_ucg_tracking) then sum(case when is_ucg=0 then total_revenue else null end) else null end as mth3_pop_revenue
from ${table-prefix}${cms-schema}.${table-prefix}cms_ucg_tracking
where lower(brand) in ('simpati','loop','kartuas')
	and los >= 90
	and datetime(rep_month) <= datetime(last_day('${run-date=yyyy-MM-dd}'))
	and datetime(rep_month) >= (select datetime(max(last_day(description))) from ${table-prefix}${cms_stg-schema}.${table-prefix}cms_ucg_list_input where datetime(last_day(description)) < datetime('${run-date=yyyy-MM-dd}'))
group by 1,2 order by 2 desc
;

insert into ${table-prefix}${cms-schema}.${table-prefix}cms_ucg_attribute_area
(
	area,
	mth1_ucg_subs,
	mth1_pop_subs,
	mth2_ucg_subs,
	mth2_pop_subs,
	mth2_ucg_revenue,
	mth2_pop_revenue,
	mth3_ucg_revenue,
	mth3_pop_revenue,
	mth1_pop_arpu,
	mth1_ucg_arpu,
	mth2_pop_arpu,
	mth2_ucg_arpu,
	arpu_uplift,
	file_date
)
select
	area,
	mth1_ucg_subs,
	mth1_pop_subs,
	mth2_ucg_subs,
	mth2_pop_subs,
	mth2_ucg_revenue,
	mth2_pop_revenue,
	mth3_ucg_revenue,
	mth3_pop_revenue,
	mth1_pop_arpu,
	mth1_ucg_arpu,
	mth2_pop_arpu,
	mth2_ucg_arpu,
	round(((mth2_pop_arpu-mth1_pop_arpu)-(mth2_ucg_arpu-mth1_ucg_arpu))/mth1_pop_arpu*100,2) as arpu_uplift,
	date('${run-date=yyyy-MM-dd}') as file_date
from
	(
		select
			area,
			mth1_ucg_subs,
			mth1_pop_subs,
			mth2_ucg_subs,
			mth2_pop_subs,
			mth2_ucg_revenue,
			mth2_pop_revenue,
			mth3_ucg_revenue,
			mth3_pop_revenue,
			round(mth2_pop_revenue/mth1_pop_subs,2) as mth1_pop_arpu,
			round(mth2_ucg_revenue/mth1_ucg_subs,2) as mth1_ucg_arpu,
			round(mth3_pop_revenue/mth2_pop_subs,2) as mth2_pop_arpu,
			round(mth3_ucg_revenue/mth2_ucg_subs,2) as mth2_ucg_arpu
		from
			(
				select
					area,
					sum(mth1_ucg_subs) as mth1_ucg_subs,
					sum(mth1_pop_subs) as mth1_pop_subs,
					sum(mth2_ucg_subs) as mth2_ucg_subs,
					sum(mth2_pop_subs) as mth2_pop_subs,
					sum(mth2_ucg_revenue) as mth2_ucg_revenue,
					sum(mth2_pop_revenue) as mth2_pop_revenue,
					sum(mth3_ucg_revenue) as mth3_ucg_revenue,
					sum(mth3_pop_revenue) as mth3_pop_revenue
				from ${table-prefix}${cms-schema}.${table-prefix}attribute_area_temp
				group by 1
			)
	)
order by 1 desc
;

drop table ${table-prefix}${cms-schema}.${table-prefix}attribute_area_temp
;

-- region --
/* KI_HINT_DICT_PROJECTION */
create or replace temp table ${table-prefix}${cms-schema}.${table-prefix}attribute_region_temp
as
select
	rep_month,
	region_lacci as region,
	case when datetime(rep_month)=(select datetime(last_day(date(max(rep_month) - interval '2' month))) from ${table-prefix}${cms-schema}.${table-prefix}cms_ucg_tracking) then count(case when is_ucg=1 then msisdn else null end) else null end as mth1_ucg_subs,
	case when datetime(rep_month)=(select datetime(last_day(date(max(rep_month) - interval '2' month))) from ${table-prefix}${cms-schema}.${table-prefix}cms_ucg_tracking) then count(case when is_ucg=0 then msisdn else null end) else null end as mth1_pop_subs,
	case when datetime(rep_month)=(select datetime(last_day(date(max(rep_month) - interval '1' month))) from ${table-prefix}${cms-schema}.${table-prefix}cms_ucg_tracking) then count(case when is_ucg=1 then msisdn else null end) else null end as mth2_ucg_subs,
	case when datetime(rep_month)=(select datetime(last_day(date(max(rep_month) - interval '1' month))) from ${table-prefix}${cms-schema}.${table-prefix}cms_ucg_tracking) then count(case when is_ucg=0 then msisdn else null end) else null end as mth2_pop_subs,
	case when datetime(rep_month)=(select datetime(last_day(date(max(rep_month) - interval '1' month))) from ${table-prefix}${cms-schema}.${table-prefix}cms_ucg_tracking) then sum(case when is_ucg=1 then total_revenue else null end) else null end as mth2_ucg_revenue,
	case when datetime(rep_month)=(select datetime(last_day(date(max(rep_month) - interval '1' month))) from ${table-prefix}${cms-schema}.${table-prefix}cms_ucg_tracking) then sum(case when is_ucg=0 then total_revenue else null end) else null end as mth2_pop_revenue,
	case when rep_month=(select max(rep_month) from ${table-prefix}${cms-schema}.${table-prefix}cms_ucg_tracking) then sum(case when is_ucg=1 then total_revenue else null end) else null end as mth3_ucg_revenue,
	case when rep_month=(select max(rep_month) from ${table-prefix}${cms-schema}.${table-prefix}cms_ucg_tracking) then sum(case when is_ucg=0 then total_revenue else null end) else null end as mth3_pop_revenue
from ${table-prefix}${cms-schema}.${table-prefix}cms_ucg_tracking
where lower(brand) in ('simpati','loop','kartuas')
	and los >= 90
	and datetime(rep_month) <= datetime(last_day('${run-date=yyyy-MM-dd}'))
	and datetime(rep_month) >= (select datetime(max(last_day(description))) from ${table-prefix}${cms_stg-schema}.${table-prefix}cms_ucg_list_input where datetime(last_day(description)) < datetime('${run-date=yyyy-MM-dd}'))
group by 1,2 order by 2 desc
;

insert into ${table-prefix}${cms-schema}.${table-prefix}cms_ucg_attribute_region
(
	region,
	mth1_ucg_subs,
	mth1_pop_subs,
	mth2_ucg_subs,
	mth2_pop_subs,
	mth2_ucg_revenue,
	mth2_pop_revenue,
	mth3_ucg_revenue,
	mth3_pop_revenue,
	mth1_pop_arpu,
	mth1_ucg_arpu,
	mth2_pop_arpu,
	mth2_ucg_arpu,
	arpu_uplift,
	file_date
)
select
	region,
	mth1_ucg_subs,
	mth1_pop_subs,
	mth2_ucg_subs,
	mth2_pop_subs,
	mth2_ucg_revenue,
	mth2_pop_revenue,
	mth3_ucg_revenue,
	mth3_pop_revenue,
	mth1_pop_arpu,
	mth1_ucg_arpu,
	mth2_pop_arpu,
	mth2_ucg_arpu,
	round(((mth2_pop_arpu-mth1_pop_arpu)-(mth2_ucg_arpu-mth1_ucg_arpu))/mth1_pop_arpu*100,2) as arpu_uplift,
	date('${run-date=yyyy-MM-dd}') as file_date
from
	(
		select
			region,
			mth1_ucg_subs,
			mth1_pop_subs,
			mth2_ucg_subs,
			mth2_pop_subs,
			mth2_ucg_revenue,
			mth2_pop_revenue,
			mth3_ucg_revenue,
			mth3_pop_revenue,
			round(mth2_pop_revenue/mth1_pop_subs,2) as mth1_pop_arpu,
			round(mth2_ucg_revenue/mth1_ucg_subs,2) as mth1_ucg_arpu,
			round(mth3_pop_revenue/mth2_pop_subs,2) as mth2_pop_arpu,
			round(mth3_ucg_revenue/mth2_ucg_subs,2) as mth2_ucg_arpu
		from
			(
				select
					region,
					sum(mth1_ucg_subs) as mth1_ucg_subs,
					sum(mth1_pop_subs) as mth1_pop_subs,
					sum(mth2_ucg_subs) as mth2_ucg_subs,
					sum(mth2_pop_subs) as mth2_pop_subs,
					sum(mth2_ucg_revenue) as mth2_ucg_revenue,
					sum(mth2_pop_revenue) as mth2_pop_revenue,
					sum(mth3_ucg_revenue) as mth3_ucg_revenue,
					sum(mth3_pop_revenue) as mth3_pop_revenue
				from ${table-prefix}${cms-schema}.${table-prefix}attribute_region_temp
				group by 1
			)
	)
order by 1 desc
;

drop table ${table-prefix}${cms-schema}.${table-prefix}attribute_region_temp
;

-- smartphone_user --
/* KI_HINT_DICT_PROJECTION */
create or replace temp table ${table-prefix}${cms-schema}.${table-prefix}attribute_smartphone_user_temp
as
select
	rep_month,
	smartphone_user,
	case when datetime(rep_month)=(select datetime(last_day(date(max(rep_month) - interval '2' month))) from ${table-prefix}${cms-schema}.${table-prefix}cms_ucg_tracking) then count(case when is_ucg=1 then msisdn else null end) else null end as mth1_ucg_subs,
	case when datetime(rep_month)=(select datetime(last_day(date(max(rep_month) - interval '2' month))) from ${table-prefix}${cms-schema}.${table-prefix}cms_ucg_tracking) then count(case when is_ucg=0 then msisdn else null end) else null end as mth1_pop_subs,
	case when datetime(rep_month)=(select datetime(last_day(date(max(rep_month) - interval '1' month))) from ${table-prefix}${cms-schema}.${table-prefix}cms_ucg_tracking) then count(case when is_ucg=1 then msisdn else null end) else null end as mth2_ucg_subs,
	case when datetime(rep_month)=(select datetime(last_day(date(max(rep_month) - interval '1' month))) from ${table-prefix}${cms-schema}.${table-prefix}cms_ucg_tracking) then count(case when is_ucg=0 then msisdn else null end) else null end as mth2_pop_subs,
	case when datetime(rep_month)=(select datetime(last_day(date(max(rep_month) - interval '1' month))) from ${table-prefix}${cms-schema}.${table-prefix}cms_ucg_tracking) then sum(case when is_ucg=1 then total_revenue else null end) else null end as mth2_ucg_revenue,
	case when datetime(rep_month)=(select datetime(last_day(date(max(rep_month) - interval '1' month))) from ${table-prefix}${cms-schema}.${table-prefix}cms_ucg_tracking) then sum(case when is_ucg=0 then total_revenue else null end) else null end as mth2_pop_revenue,
	case when rep_month=(select max(rep_month) from ${table-prefix}${cms-schema}.${table-prefix}cms_ucg_tracking) then sum(case when is_ucg=1 then total_revenue else null end) else null end as mth3_ucg_revenue,
	case when rep_month=(select max(rep_month) from ${table-prefix}${cms-schema}.${table-prefix}cms_ucg_tracking) then sum(case when is_ucg=0 then total_revenue else null end) else null end as mth3_pop_revenue
from ${table-prefix}${cms-schema}.${table-prefix}cms_ucg_tracking
where lower(brand) in ('simpati','loop','kartuas')
	and los >= 90
	and datetime(rep_month) <= datetime(last_day('${run-date=yyyy-MM-dd}'))
	and datetime(rep_month) >= (select datetime(max(last_day(description))) from ${table-prefix}${cms_stg-schema}.${table-prefix}cms_ucg_list_input where datetime(last_day(description)) < datetime('${run-date=yyyy-MM-dd}'))
group by 1,2 order by 2 desc
;

insert into ${table-prefix}${cms-schema}.${table-prefix}cms_ucg_attribute_smartphone_user
(
	smartphone_user,
	mth1_ucg_subs,
	mth1_pop_subs,
	mth2_ucg_subs,
	mth2_pop_subs,
	mth2_ucg_revenue,
	mth2_pop_revenue,
	mth3_ucg_revenue,
	mth3_pop_revenue,
	mth1_pop_arpu,
	mth1_ucg_arpu,
	mth2_pop_arpu,
	mth2_ucg_arpu,
	arpu_uplift,
	file_date
)
select
	smartphone_user,
	mth1_ucg_subs,
	mth1_pop_subs,
	mth2_ucg_subs,
	mth2_pop_subs,
	mth2_ucg_revenue,
	mth2_pop_revenue,
	mth3_ucg_revenue,
	mth3_pop_revenue,
	mth1_pop_arpu,
	mth1_ucg_arpu,
	mth2_pop_arpu,
	mth2_ucg_arpu,
	round(((mth2_pop_arpu-mth1_pop_arpu)-(mth2_ucg_arpu-mth1_ucg_arpu))/mth1_pop_arpu*100,2) as arpu_uplift,
	date('${run-date=yyyy-MM-dd}') as file_date
from
	(
		select
			smartphone_user,
			mth1_ucg_subs,
			mth1_pop_subs,
			mth2_ucg_subs,
			mth2_pop_subs,
			mth2_ucg_revenue,
			mth2_pop_revenue,
			mth3_ucg_revenue,
			mth3_pop_revenue,
			round(mth2_pop_revenue/mth1_pop_subs,2) as mth1_pop_arpu,
			round(mth2_ucg_revenue/mth1_ucg_subs,2) as mth1_ucg_arpu,
			round(mth3_pop_revenue/mth2_pop_subs,2) as mth2_pop_arpu,
			round(mth3_ucg_revenue/mth2_ucg_subs,2) as mth2_ucg_arpu
		from
			(
				select
					smartphone_user,
					sum(mth1_ucg_subs) as mth1_ucg_subs,
					sum(mth1_pop_subs) as mth1_pop_subs,
					sum(mth2_ucg_subs) as mth2_ucg_subs,
					sum(mth2_pop_subs) as mth2_pop_subs,
					sum(mth2_ucg_revenue) as mth2_ucg_revenue,
					sum(mth2_pop_revenue) as mth2_pop_revenue,
					sum(mth3_ucg_revenue) as mth3_ucg_revenue,
					sum(mth3_pop_revenue) as mth3_pop_revenue
				from ${table-prefix}${cms-schema}.${table-prefix}attribute_smartphone_user_temp
				group by 1
			)
	)
order by 1 desc
;

drop table ${table-prefix}${cms-schema}.${table-prefix}attribute_smartphone_user_temp
;

-- rev_data_segment --
/* KI_HINT_DICT_PROJECTION */
create or replace temp table ${table-prefix}${cms-schema}.${table-prefix}attribute_rev_data_segment_temp
as
select
	rep_month,
	rev_data_segment,
	case when datetime(rep_month)=(select datetime(last_day(date(max(rep_month) - interval '2' month))) from ${table-prefix}${cms-schema}.${table-prefix}cms_ucg_tracking) then count(case when is_ucg=1 then msisdn else null end) else null end as mth1_ucg_subs,
	case when datetime(rep_month)=(select datetime(last_day(date(max(rep_month) - interval '2' month))) from ${table-prefix}${cms-schema}.${table-prefix}cms_ucg_tracking) then count(case when is_ucg=0 then msisdn else null end) else null end as mth1_pop_subs,
	case when datetime(rep_month)=(select datetime(last_day(date(max(rep_month) - interval '1' month))) from ${table-prefix}${cms-schema}.${table-prefix}cms_ucg_tracking) then count(case when is_ucg=1 then msisdn else null end) else null end as mth2_ucg_subs,
	case when datetime(rep_month)=(select datetime(last_day(date(max(rep_month) - interval '1' month))) from ${table-prefix}${cms-schema}.${table-prefix}cms_ucg_tracking) then count(case when is_ucg=0 then msisdn else null end) else null end as mth2_pop_subs,
	case when datetime(rep_month)=(select datetime(last_day(date(max(rep_month) - interval '1' month))) from ${table-prefix}${cms-schema}.${table-prefix}cms_ucg_tracking) then sum(case when is_ucg=1 then total_revenue else null end) else null end as mth2_ucg_revenue,
	case when datetime(rep_month)=(select datetime(last_day(date(max(rep_month) - interval '1' month))) from ${table-prefix}${cms-schema}.${table-prefix}cms_ucg_tracking) then sum(case when is_ucg=0 then total_revenue else null end) else null end as mth2_pop_revenue,
	case when rep_month=(select max(rep_month) from ${table-prefix}${cms-schema}.${table-prefix}cms_ucg_tracking) then sum(case when is_ucg=1 then total_revenue else null end) else null end as mth3_ucg_revenue,
	case when rep_month=(select max(rep_month) from ${table-prefix}${cms-schema}.${table-prefix}cms_ucg_tracking) then sum(case when is_ucg=0 then total_revenue else null end) else null end as mth3_pop_revenue
from ${table-prefix}${cms-schema}.${table-prefix}cms_ucg_tracking
where lower(brand) in ('simpati','loop','kartuas')
	and los >= 90
	and datetime(rep_month) <= datetime(last_day('${run-date=yyyy-MM-dd}'))
	and datetime(rep_month) >= (select datetime(max(last_day(description))) from ${table-prefix}${cms_stg-schema}.${table-prefix}cms_ucg_list_input where datetime(last_day(description)) < datetime('${run-date=yyyy-MM-dd}'))
group by 1,2 order by 2 desc
;

insert into ${table-prefix}${cms-schema}.${table-prefix}cms_ucg_attribute_rev_data_segment
(
	rev_data_segment,
	mth1_ucg_subs,
	mth1_pop_subs,
	mth2_ucg_subs,
	mth2_pop_subs,
	mth2_ucg_revenue,
	mth2_pop_revenue,
	mth3_ucg_revenue,
	mth3_pop_revenue,
	mth1_pop_arpu,
	mth1_ucg_arpu,
	mth2_pop_arpu,
	mth2_ucg_arpu,
	arpu_uplift,
	file_date
)
select
	rev_data_segment,
	mth1_ucg_subs,
	mth1_pop_subs,
	mth2_ucg_subs,
	mth2_pop_subs,
	mth2_ucg_revenue,
	mth2_pop_revenue,
	mth3_ucg_revenue,
	mth3_pop_revenue,
	mth1_pop_arpu,
	mth1_ucg_arpu,
	mth2_pop_arpu,
	mth2_ucg_arpu,
	round(((mth2_pop_arpu-mth1_pop_arpu)-(mth2_ucg_arpu-mth1_ucg_arpu))/mth1_pop_arpu*100,2) as arpu_uplift,
	date('${run-date=yyyy-MM-dd}') as file_date
from
	(
		select
			rev_data_segment,
			mth1_ucg_subs,
			mth1_pop_subs,
			mth2_ucg_subs,
			mth2_pop_subs,
			mth2_ucg_revenue,
			mth2_pop_revenue,
			mth3_ucg_revenue,
			mth3_pop_revenue,
			round(mth2_pop_revenue/mth1_pop_subs,2) as mth1_pop_arpu,
			round(mth2_ucg_revenue/mth1_ucg_subs,2) as mth1_ucg_arpu,
			round(mth3_pop_revenue/mth2_pop_subs,2) as mth2_pop_arpu,
			round(mth3_ucg_revenue/mth2_ucg_subs,2) as mth2_ucg_arpu
		from
			(
				select
					rev_data_segment,
					sum(mth1_ucg_subs) as mth1_ucg_subs,
					sum(mth1_pop_subs) as mth1_pop_subs,
					sum(mth2_ucg_subs) as mth2_ucg_subs,
					sum(mth2_pop_subs) as mth2_pop_subs,
					sum(mth2_ucg_revenue) as mth2_ucg_revenue,
					sum(mth2_pop_revenue) as mth2_pop_revenue,
					sum(mth3_ucg_revenue) as mth3_ucg_revenue,
					sum(mth3_pop_revenue) as mth3_pop_revenue
				from ${table-prefix}${cms-schema}.${table-prefix}attribute_rev_data_segment_temp
				group by 1
			)
	)
order by 1 desc
;

drop table ${table-prefix}${cms-schema}.${table-prefix}attribute_rev_data_segment_temp;

-- arpu_group --
/* KI_HINT_DICT_PROJECTION */
create or replace temp table ${table-prefix}${cms-schema}.${table-prefix}attribute_arpu_group_temp
as
select
	rep_month,
	arpu_group,
	case when datetime(rep_month)=(select datetime(last_day(date(max(rep_month) - interval '2' month))) from ${table-prefix}${cms-schema}.${table-prefix}cms_ucg_tracking) then count(case when is_ucg=1 then msisdn else null end) else null end as mth1_ucg_subs,
	case when datetime(rep_month)=(select datetime(last_day(date(max(rep_month) - interval '2' month))) from ${table-prefix}${cms-schema}.${table-prefix}cms_ucg_tracking) then count(case when is_ucg=0 then msisdn else null end) else null end as mth1_pop_subs,
	case when datetime(rep_month)=(select datetime(last_day(date(max(rep_month) - interval '1' month))) from ${table-prefix}${cms-schema}.${table-prefix}cms_ucg_tracking) then count(case when is_ucg=1 then msisdn else null end) else null end as mth2_ucg_subs,
	case when datetime(rep_month)=(select datetime(last_day(date(max(rep_month) - interval '1' month))) from ${table-prefix}${cms-schema}.${table-prefix}cms_ucg_tracking) then count(case when is_ucg=0 then msisdn else null end) else null end as mth2_pop_subs,
	case when datetime(rep_month)=(select datetime(last_day(date(max(rep_month) - interval '1' month))) from ${table-prefix}${cms-schema}.${table-prefix}cms_ucg_tracking) then sum(case when is_ucg=1 then total_revenue else null end) else null end as mth2_ucg_revenue,
	case when datetime(rep_month)=(select datetime(last_day(date(max(rep_month) - interval '1' month))) from ${table-prefix}${cms-schema}.${table-prefix}cms_ucg_tracking) then sum(case when is_ucg=0 then total_revenue else null end) else null end as mth2_pop_revenue,
	case when rep_month=(select max(rep_month) from ${table-prefix}${cms-schema}.${table-prefix}cms_ucg_tracking) then sum(case when is_ucg=1 then total_revenue else null end) else null end as mth3_ucg_revenue,
	case when rep_month=(select max(rep_month) from ${table-prefix}${cms-schema}.${table-prefix}cms_ucg_tracking) then sum(case when is_ucg=0 then total_revenue else null end) else null end as mth3_pop_revenue
from ${table-prefix}${cms-schema}.${table-prefix}cms_ucg_tracking
where lower(brand) in ('simpati','loop','kartuas')
	and los >= 90
	and datetime(rep_month) <= datetime(last_day('${run-date=yyyy-MM-dd}'))
	and datetime(rep_month) >= (select datetime(max(last_day(description))) from ${table-prefix}${cms_stg-schema}.${table-prefix}cms_ucg_list_input where datetime(last_day(description)) < datetime('${run-date=yyyy-MM-dd}'))
group by 1,2 order by 2 desc
;

insert into ${table-prefix}${cms-schema}.${table-prefix}cms_ucg_attribute_arpu_group
(
	arpu_group,
	mth1_ucg_subs,
	mth1_pop_subs,
	mth2_ucg_subs,
	mth2_pop_subs,
	mth2_ucg_revenue,
	mth2_pop_revenue,
	mth3_ucg_revenue,
	mth3_pop_revenue,
	mth1_pop_arpu,
	mth1_ucg_arpu,
	mth2_pop_arpu,
	mth2_ucg_arpu,
	arpu_uplift,
	file_date
)
select
	arpu_group,
	mth1_ucg_subs,
	mth1_pop_subs,
	mth2_ucg_subs,
	mth2_pop_subs,
	mth2_ucg_revenue,
	mth2_pop_revenue,
	mth3_ucg_revenue,
	mth3_pop_revenue,
	mth1_pop_arpu,
	mth1_ucg_arpu,
	mth2_pop_arpu,
	mth2_ucg_arpu,
	round(((mth2_pop_arpu-mth1_pop_arpu)-(mth2_ucg_arpu-mth1_ucg_arpu))/mth1_pop_arpu*100,2) as arpu_uplift,
	date('${run-date=yyyy-MM-dd}') as file_date
from
	(
		select
			arpu_group,
			mth1_ucg_subs,
			mth1_pop_subs,
			mth2_ucg_subs,
			mth2_pop_subs,
			mth2_ucg_revenue,
			mth2_pop_revenue,
			mth3_ucg_revenue,
			mth3_pop_revenue,
			round(mth2_pop_revenue/mth1_pop_subs,2) as mth1_pop_arpu,
			round(mth2_ucg_revenue/mth1_ucg_subs,2) as mth1_ucg_arpu,
			round(mth3_pop_revenue/mth2_pop_subs,2) as mth2_pop_arpu,
			round(mth3_ucg_revenue/mth2_ucg_subs,2) as mth2_ucg_arpu
		from
			(
				select
					arpu_group,
					sum(mth1_ucg_subs) as mth1_ucg_subs,
					sum(mth1_pop_subs) as mth1_pop_subs,
					sum(mth2_ucg_subs) as mth2_ucg_subs,
					sum(mth2_pop_subs) as mth2_pop_subs,
					sum(mth2_ucg_revenue) as mth2_ucg_revenue,
					sum(mth2_pop_revenue) as mth2_pop_revenue,
					sum(mth3_ucg_revenue) as mth3_ucg_revenue,
					sum(mth3_pop_revenue) as mth3_pop_revenue
				from ${table-prefix}${cms-schema}.${table-prefix}attribute_arpu_group_temp
				group by 1
			)
	)
order by 1 desc
;

drop table ${table-prefix}${cms-schema}.${table-prefix}attribute_arpu_group_temp
;
