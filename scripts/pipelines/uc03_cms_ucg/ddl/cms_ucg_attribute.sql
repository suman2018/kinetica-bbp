create or replace table ${table-prefix}${cms-schema}.${table-prefix}cms_ucg_attribute_brand
(
	"brand" varchar(16, dict),
	"mth1_ucg_subs" bigint,
	"mth1_pop_subs" bigint,
	"mth2_ucg_subs" bigint,
	"mth2_pop_subs" bigint,
	"mth2_ucg_revenue" double,
	"mth2_pop_revenue" double,
	"mth3_ucg_revenue" double,
	"mth3_pop_revenue" double,
	"mth1_pop_arpu" double,
	"mth1_ucg_arpu" double,
	"mth2_pop_arpu" double,
	"mth2_ucg_arpu" double,
	"arpu_uplift"double,
	"file_date" date(dict)
)
;

create or replace table ${table-prefix}${cms-schema}.${table-prefix}cms_ucg_attribute_los_segment
(
	"los_segment" varchar(32, dict),
	"mth1_ucg_subs" bigint,
	"mth1_pop_subs" bigint,
	"mth2_ucg_subs" bigint,
	"mth2_pop_subs" bigint,
	"mth2_ucg_revenue" double,
	"mth2_pop_revenue" double,
	"mth3_ucg_revenue" double,
	"mth3_pop_revenue" double,
	"mth1_pop_arpu" double,
	"mth1_ucg_arpu" double,
	"mth2_pop_arpu" double,
	"mth2_ucg_arpu" double,
	"arpu_uplift"double,
	"file_date" date(dict)
)
;

create or replace table ${table-prefix}${cms-schema}.${table-prefix}cms_ucg_attribute_area
(
	"area" varchar(32, dict),
	"mth1_ucg_subs" bigint,
	"mth1_pop_subs" bigint,
	"mth2_ucg_subs" bigint,
	"mth2_pop_subs" bigint,
	"mth2_ucg_revenue" double,
	"mth2_pop_revenue" double,
	"mth3_ucg_revenue" double,
	"mth3_pop_revenue" double,
	"mth1_pop_arpu" double,
	"mth1_ucg_arpu" double,
	"mth2_pop_arpu" double,
	"mth2_ucg_arpu" double,
	"arpu_uplift"double,
	"file_date" date(dict)
)
;

create or replace table ${table-prefix}${cms-schema}.${table-prefix}cms_ucg_attribute_region
(
	"region" varchar(32, dict),
	"mth1_ucg_subs" bigint,
	"mth1_pop_subs" bigint,
	"mth2_ucg_subs" bigint,
	"mth2_pop_subs" bigint,
	"mth2_ucg_revenue" double,
	"mth2_pop_revenue" double,
	"mth3_ucg_revenue" double,
	"mth3_pop_revenue" double,
	"mth1_pop_arpu" double,
	"mth1_ucg_arpu" double,
	"mth2_pop_arpu" double,
	"mth2_ucg_arpu" double,
	"arpu_uplift"double,
	"file_date" date(dict)
)
;

create or replace table ${table-prefix}${cms-schema}.${table-prefix}cms_ucg_attribute_smartphone_user
(
	"smartphone_user" varchar(32, dict),
	"mth1_ucg_subs" bigint,
	"mth1_pop_subs" bigint,
	"mth2_ucg_subs" bigint,
	"mth2_pop_subs" bigint,
	"mth2_ucg_revenue" double,
	"mth2_pop_revenue" double,
	"mth3_ucg_revenue" double,
	"mth3_pop_revenue" double,
	"mth1_pop_arpu" double,
	"mth1_ucg_arpu" double,
	"mth2_pop_arpu" double,
	"mth2_ucg_arpu" double,
	"arpu_uplift"double,
	"file_date" date(dict)
)
;

create or replace table ${table-prefix}${cms-schema}.${table-prefix}cms_ucg_attribute_rev_data_segment
(
	"rev_data_segment" varchar(32, dict),
	"mth1_ucg_subs" bigint,
	"mth1_pop_subs" bigint,
	"mth2_ucg_subs" bigint,
	"mth2_pop_subs" bigint,
	"mth2_ucg_revenue" double,
	"mth2_pop_revenue" double,
	"mth3_ucg_revenue" double,
	"mth3_pop_revenue" double,
	"mth1_pop_arpu" double,
	"mth1_ucg_arpu" double,
	"mth2_pop_arpu" double,
	"mth2_ucg_arpu" double,
	"arpu_uplift"double,
	"file_date" date(dict)
)
;

create or replace table ${table-prefix}${cms-schema}.${table-prefix}cms_ucg_attribute_arpu_group
(
	"arpu_group" varchar(32, dict),
	"mth1_ucg_subs" bigint,
	"mth1_pop_subs" bigint,
	"mth2_ucg_subs" bigint,
	"mth2_pop_subs" bigint,
	"mth2_ucg_revenue" double,
	"mth2_pop_revenue" double,
	"mth3_ucg_revenue" double,
	"mth3_pop_revenue" double,
	"mth1_pop_arpu" double,
	"mth1_ucg_arpu" double,
	"mth2_pop_arpu" double,
	"mth2_ucg_arpu" double,
	"arpu_uplift"double,
	"file_date" date(dict)
)
;