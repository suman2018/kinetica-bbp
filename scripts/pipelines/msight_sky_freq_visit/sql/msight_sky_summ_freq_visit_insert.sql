CREATE OR REPLACE TABLE ${table-prefix}${msight_sky-schema}.${table-prefix}sky_summ_freq_visit_stg AS 
SELECT
building_name,
propinsi,
kabupaten,
kecamatan,
kelurahan,
cast(latitude as decimal(9,6)) as latitude,
cast(longitude as decimal(9,6)) as longitude,
CAST (qty1 as integer) as qty1,
CAST (qty2 as integer) as qty2,
CAST (qty3 as integer) as qty3,
CAST (qty4 as integer) as qty4,
CAST (qty5 as integer) as qty5,
CAST (qty6 as integer) as qty6,
CAST (qty7 as integer) as qty7,
CAST (qty8 as integer) as qty8,
CAST (qty9 as integer) as qty9,
CAST (qty10 as integer) as qty10,
CAST (qty11 as integer) as qty11,
CAST (qty12 as integer) as qty12,
CAST (qty13 as integer) as qty13,
CAST (qty14 as integer) as qty14,
CAST (qty15 as integer) as qty15,
CAST (qty16 as integer) as qty16,
CAST (qty17 as integer) as qty17,
CAST (qty18 as integer) as qty18,
CAST (qty19 as integer) as qty19,
CAST (qty20 as integer) as qty20,
CAST (qty21 as integer) as qty21,
CAST (qty22 as integer) as qty22,
CAST (qty23 as integer) as qty23,
CAST (qty24 as integer) as qty24,
CAST (qty25 as integer) as qty25,
CAST (qty26 as integer) as qty26,
CAST (qty27 as integer) as qty27,
CAST (qty28 as integer) as qty28,
CAST (qty29 as integer) as qty29,
CAST (qty30 as integer) as qty30,
ki_shard_key (building_name)
FROM ${table-prefix}${msight_sky-schema}.${table-prefix}sky_summ_freq_visit_input;


alter table ${table-prefix}${msight_sky-schema}.${table-prefix}sky_summ_freq_visit_stg modify column propinsi varchar(16, dict) ;
alter table ${table-prefix}${msight_sky-schema}.${table-prefix}sky_summ_freq_visit_stg modify column kabupaten varchar(32, dict) ;
alter table ${table-prefix}${msight_sky-schema}.${table-prefix}sky_summ_freq_visit_stg modify column kecamatan varchar(32, dict) ;
alter table ${table-prefix}${msight_sky-schema}.${table-prefix}sky_summ_freq_visit_stg modify column kelurahan varchar(32, dict) ;


alter table ${table-prefix}${msight_sky-schema}.${table-prefix}sky_summ_freq_visit_stg modify column qty1 integer (dict);
alter table ${table-prefix}${msight_sky-schema}.${table-prefix}sky_summ_freq_visit_stg modify column qty2 integer (dict);
alter table ${table-prefix}${msight_sky-schema}.${table-prefix}sky_summ_freq_visit_stg modify column qty3 integer (dict);
alter table ${table-prefix}${msight_sky-schema}.${table-prefix}sky_summ_freq_visit_stg modify column qty4 integer (dict);
alter table ${table-prefix}${msight_sky-schema}.${table-prefix}sky_summ_freq_visit_stg modify column qty5 integer (dict);
alter table ${table-prefix}${msight_sky-schema}.${table-prefix}sky_summ_freq_visit_stg modify column qty6 integer (dict);
alter table ${table-prefix}${msight_sky-schema}.${table-prefix}sky_summ_freq_visit_stg modify column qty7 integer (dict);
alter table ${table-prefix}${msight_sky-schema}.${table-prefix}sky_summ_freq_visit_stg modify column qty8 integer (dict);
alter table ${table-prefix}${msight_sky-schema}.${table-prefix}sky_summ_freq_visit_stg modify column qty9 integer (dict);
alter table ${table-prefix}${msight_sky-schema}.${table-prefix}sky_summ_freq_visit_stg modify column qty10 integer (dict);
alter table ${table-prefix}${msight_sky-schema}.${table-prefix}sky_summ_freq_visit_stg modify column qty11 integer (dict);
alter table ${table-prefix}${msight_sky-schema}.${table-prefix}sky_summ_freq_visit_stg modify column qty12 integer (dict);
alter table ${table-prefix}${msight_sky-schema}.${table-prefix}sky_summ_freq_visit_stg modify column qty13 integer (dict);
alter table ${table-prefix}${msight_sky-schema}.${table-prefix}sky_summ_freq_visit_stg modify column qty14 integer (dict);
alter table ${table-prefix}${msight_sky-schema}.${table-prefix}sky_summ_freq_visit_stg modify column qty15 integer (dict);
alter table ${table-prefix}${msight_sky-schema}.${table-prefix}sky_summ_freq_visit_stg modify column qty16 integer (dict);
alter table ${table-prefix}${msight_sky-schema}.${table-prefix}sky_summ_freq_visit_stg modify column qty17 integer (dict);
alter table ${table-prefix}${msight_sky-schema}.${table-prefix}sky_summ_freq_visit_stg modify column qty18 integer (dict);
alter table ${table-prefix}${msight_sky-schema}.${table-prefix}sky_summ_freq_visit_stg modify column qty19 integer (dict);
alter table ${table-prefix}${msight_sky-schema}.${table-prefix}sky_summ_freq_visit_stg modify column qty20 integer (dict);
alter table ${table-prefix}${msight_sky-schema}.${table-prefix}sky_summ_freq_visit_stg modify column qty21 integer (dict);
alter table ${table-prefix}${msight_sky-schema}.${table-prefix}sky_summ_freq_visit_stg modify column qty22 integer (dict);
alter table ${table-prefix}${msight_sky-schema}.${table-prefix}sky_summ_freq_visit_stg modify column qty23 integer (dict);
alter table ${table-prefix}${msight_sky-schema}.${table-prefix}sky_summ_freq_visit_stg modify column qty24 integer (dict);
alter table ${table-prefix}${msight_sky-schema}.${table-prefix}sky_summ_freq_visit_stg modify column qty25 integer (dict);
alter table ${table-prefix}${msight_sky-schema}.${table-prefix}sky_summ_freq_visit_stg modify column qty26 integer (dict);
alter table ${table-prefix}${msight_sky-schema}.${table-prefix}sky_summ_freq_visit_stg modify column qty27 integer (dict);
alter table ${table-prefix}${msight_sky-schema}.${table-prefix}sky_summ_freq_visit_stg modify column qty28 integer (dict);
alter table ${table-prefix}${msight_sky-schema}.${table-prefix}sky_summ_freq_visit_stg modify column qty29 integer (dict);
alter table ${table-prefix}${msight_sky-schema}.${table-prefix}sky_summ_freq_visit_stg modify column qty30 integer (dict);



delete from ${table-prefix}${msight_sky-schema}.${table-prefix}sky_summ_freq_visit where months = '${run-date=yyyyMM}';

insert into ${table-prefix}${msight_sky-schema}.${table-prefix}sky_summ_freq_visit
select 
 '${run-date=yyyyMM}' as months
,a.* from ${table-prefix}${msight_sky-schema}.${table-prefix}sky_summ_freq_visit_stg a;

DROP TABLE IF EXISTS ${table-prefix}${msight_sky-schema}.${table-prefix}sky_summ_freq_visit_stg;
DROP TABLE IF EXISTS ${table-prefix}${msight_sky-schema}.${table-prefix}sky_summ_freq_visit_input;
