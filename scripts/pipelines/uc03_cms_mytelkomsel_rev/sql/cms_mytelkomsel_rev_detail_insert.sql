delete from ${table-prefix}${cms-schema}.${table-prefix}cms_campaign_tracking_detail
where file_date = date('${run-date=yyyy-MM-dd}')
and pipeline_name = '${pipeline-name}'
and pipeline_step = '${pipeline-step}';

--MYTSEL_REV takers
/* KI_HINT_DICT_PROJECTION */
create or replace temp table ${table-prefix}${cms-schema}.${table-prefix}${pipeline-name}_tmp
as
select distinct
	cview.category
	,cview.campaign_type
	,cview.msisdn
	,cview.campaign_id
	,cview.campaign_objective_id
	,cview.campaign_name
	,cview.start_period
	,cview.end_period
	,cview.iscontrolgroup
	,cview.campaign_group_id
	,cview.segment
	,cview.los
	,cview.arpu
	,cview.business_group
	,cview.initiated_group
	--,cview.area_sales
	,cview.region_hlr
	,cview.cluster
	,cview.kabupaten
	,chg.trx_date as trx_date
	,chg.rev as metric_1
	,chg.content_id as content_1
	,chg.vas_code as content_2
	,crules.is_btl
	,cview.target
	,cview.eligible
	,1 as taker
	,date('${run-date=yyyy-MM-dd}') as file_date
	,'${job-id}' as job_id
	,'${pipeline-name}' as pipeline_name
	,'${pipeline-step}' as pipeline_step
from 
(
	select distinct mltdim.*
	from ${table-prefix}${cms-schema}.${table-prefix}cms_campaign_trackingstream_multidim_v mltdim
	join
	(
		select distinct trx_date
		from ${table-prefix}${cms-schema}.${table-prefix}cms_chg_detail
		where file_date = date('${run-date=yyyy-MM-dd}')
	) trx_date
	on trx_date.trx_date between date(mltdim.start_period) and date(mltdim.end_period)
	where campaign_group_id = 4
		and upper(campaign_name) LIKE '%MYTSEL_REV%'
) cview
join ${table-prefix}${cms-schema}.${table-prefix}cms_campaign_rules crules
	on crules.campaign_id = cview.campaign_id
join ${table-prefix}${cms-schema}.${table-prefix}cms_chg_detail chg
	on chg.msisdn = cview.msisdn
	and(
		(crules.taker_identify = 'VAS_CODE' and crules.value_identify like chg.vas_code||'%')
		or (crules.taker_identify = 'VAS_CODE' and chg.vas_code like char64(crules.value_identify)||'%')
		or (crules.taker_identify = 'CONTENT_ID' and crules.value_identify like chg.content_id||'%')
		)
	and chg.file_date = date('${run-date=yyyy-MM-dd}')
	and chg.activation_channel_id in ('a0','i1','p0')
	and chg.trx_date between date(cview.start_period) and date(cview.end_period)
	and cview.eligible > 0
;

insert into ${table-prefix}${cms-schema}.${table-prefix}cms_campaign_tracking_detail
(
	 category
	,campaign_type
	,msisdn
	,campaign_id
	,campaign_objective_id
	,campaign_name
	,start_period
	,end_period
	,iscontrolgroup
	,campaign_group_id
	,segment
	,los
	,arpu
	,business_group
	,initiated_group
	--,area
	,region_hlr
	,cluster
	,branch
	,trx_date
	,metric_1
	,content_1
	,content_2
	,is_btl
	,target
	,eligible
	,taker
	,file_date
	,orch_job_id
	,pipeline_name
	,pipeline_step
)
select * from ${table-prefix}${cms-schema}.${table-prefix}${pipeline-name}_tmp
;

--MYTSEL_REV non-takers
/* KI_HINT_DICT_PROJECTION */
create or replace temp table ${table-prefix}${cms-schema}.${table-prefix}${pipeline-name}_tmp
as
select
     cview.category
    ,cview.campaign_type
    ,cview.msisdn
    ,cview.campaign_id
    ,cview.campaign_objective_id
    ,cview.campaign_name
    ,cview.start_period
    ,cview.end_period
    ,cview.iscontrolgroup
    ,cview.campaign_group_id
    ,cview.segment
    ,cview.los
    ,cview.arpu
    ,cview.business_group
    ,cview.initiated_group
    ,cview.region_hlr
    ,cview.cluster
    ,cview.kabupaten
    ,date('${run-date=yyyy-MM-dd}') as trx_date
    ,0 as is_btl
    ,cview.target
	,cview.eligible
    ,0 as taker
    ,date('${run-date=yyyy-MM-dd}') as file_date
    ,'${job-id}' as job_id
    ,'${pipeline-name}' as pipeline_name
    ,'${pipeline-step}' as pipeline_step
from 
(
	select distinct mltdim.*
	from ${table-prefix}${cms-schema}.${table-prefix}cms_campaign_trackingstream_multidim_v mltdim
	join
	(
		select distinct trx_date
		from ${table-prefix}${cms-schema}.${table-prefix}cms_chg_detail
		where file_date = date('${run-date=yyyy-MM-dd}')
	) trx_date
	on trx_date.trx_date between date(mltdim.start_period) and date(mltdim.end_period)
	where campaign_group_id = 4
		and upper(campaign_name) LIKE '%MYTSEL_REV%'
) cview
left join 
(
	select distinct campaign_id, msisdn
	from ${table-prefix}${cms-schema}.${table-prefix}cms_campaign_tracking_detail dtl
	where orch_job_id = '${job-id}'
) dtl
on cview.campaign_id = dtl.campaign_id
	and cview.msisdn = dtl.msisdn
where dtl.msisdn is null
;

insert into ${table-prefix}${cms-schema}.${table-prefix}cms_campaign_tracking_detail
(
	 category
	,campaign_type
	,msisdn
	,campaign_id
	,campaign_objective_id
	,campaign_name
	,start_period
	,end_period
	,iscontrolgroup
	,campaign_group_id
	,segment
	,los
	,arpu
	,business_group
	,initiated_group
	,region_hlr
	,cluster
	,branch
	,trx_date
	,is_btl
	,target
	,eligible
	,taker
	,file_date
	,orch_job_id
	,pipeline_name
	,pipeline_step
)
select * from ${table-prefix}${cms-schema}.${table-prefix}${pipeline-name}_tmp
;

drop table ${table-prefix}${cms-schema}.${table-prefix}${pipeline-name}_tmp
;