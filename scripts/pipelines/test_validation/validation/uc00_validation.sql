SELECT COUNT(REV) FROM ${table-prefix}revenue.${table-prefix}chg_rcg_staging_${run-date=yyyyMMdd};
SELECT COUNT(REV) FROM ${table-prefix}revenue.${table-prefix}revenue_summary WHERE FILE_DATE = '${run-date=yyyy-MM-dd}';

SELECT SUM(REV) FROM ${table-prefix}revenue.${table-prefix}chg_rcg_staging_${run-date=yyyyMMdd};
SELECT SUM(REV) FROM ${table-prefix}revenue.${table-prefix}revenue_summary WHERE FILE_DATE = '${run-date=yyyy-MM-dd}';

SELECT COUNT(DUR) FROM ${table-prefix}revenue.${table-prefix}chg_rcg_staging_${run-date=yyyyMMdd};
SELECT COUNT(DUR) FROM ${table-prefix}revenue.${table-prefix}revenue_summary WHERE FILE_DATE = '${run-date=yyyy-MM-dd}';

SELECT SUM(DUR) FROM ${table-prefix}revenue.${table-prefix}chg_rcg_staging_${run-date=yyyyMMdd};
SELECT SUM(DUR) FROM ${table-prefix}revenue.${table-prefix}revenue_summary WHERE FILE_DATE = '${run-date=yyyy-MM-dd}';