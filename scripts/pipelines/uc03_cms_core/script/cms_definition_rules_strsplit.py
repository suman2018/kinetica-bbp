################################################################################
#                                                                              #
# String Tokenizer Split To Table UDF                                          #
# ---------------------------------------------------------------------------- #
# This UDF takes in:                                                           #
#    -strcol: column in the input table that should be split                   #
#    -keycol: column that will be preserved in                                 #
#    -sep: separator by which the strcol is split                              #
# This UDF splits strcol by the sep and inserts a new row for each token found #
# This is a sample code to run the function                                    #
#                                                                              #
################################################################################

import sys
import collections
from gpudb import GPUdb, GPUdbTable
from time import sleep


db_host      = sys.argv[1] if len(sys.argv) > 1 else '127.0.0.1'
db_port      = sys.argv[2] if len(sys.argv) > 2 else '9191'
db_user      = sys.argv[3] if len(sys.argv) > 3 else 'admin'
db_pass      = sys.argv[4] if len(sys.argv) > 4 else 'Kinetica1!'
table_prefix = sys.argv[5] if len(sys.argv) > 4 else 'dev_'

proc_name = 'strtok_split_to_table'

#set input and output tables
INPUT_TABLE = table_prefix+'cms_definition_rules_input'
OUTPUT_TABLE = table_prefix+'cms_definition_rules_strsplit'
OUTPUT_COLLECTION = table_prefix+'cms_stg'

#set columns for output table
keycols = collections.OrderedDict()
keycols["campid"] = ["string", "char256", "dict"]
keycols["group"] = ["string", "char256", "dict"]
keycols["name"] = ["string", "char256", "dict"]

# Connect to Kinetica
h_db = GPUdb(host=db_host, port=db_port, username=db_user, password=db_pass)

#Create output table
columns = [[ "idx", "int" ]]
for keycol in keycols:
    columns += [ [keycol] + keycols[keycol] ]
columns += [[ "tkn", "string", "char256", "dict" ]]

if h_db.has_table( table_name = OUTPUT_TABLE )['table_exists']:
    h_db.clear_table( OUTPUT_TABLE )
o_tbl = GPUdbTable( columns, OUTPUT_TABLE, db = h_db, options={'collection_name':OUTPUT_COLLECTION} )

print("Executing proc...")
response = h_db.execute_proc(
    proc_name=proc_name,
    params={"keycols":','.join(keycols.keys()),"strcol":"value","sep":","},
    bin_params={},
    input_table_names=[INPUT_TABLE],
    input_column_names={},
    output_table_names=[OUTPUT_TABLE],
    options={}
)

if response['status_info']['status'] == 'ERROR':
    print(response['status_info']['message'])
    raise Exception("Error occured, check gpudb-proc.log for more info")
run_id=response['run_id']

sleep(1)
while h_db.show_proc_status(run_id=response['run_id'])['overall_statuses'][run_id] == 'running':
    print("running...")
    status=h_db.show_proc_status(run_id=run_id)["statuses"][run_id]
    for v in set(status.values()):
        print(v + " count: " + str(status.values().count(v)))
    sleep(3)

if h_db.show_proc_status(run_id=response['run_id'])['overall_statuses'][response['run_id']] == 'error':
    print(h_db.show_proc_status(run_id=response['run_id']))
    raise Exception("Error occured, check gpudb-proc.log for more info")

print("Proc executed successfully:")
print(response)
print("Check 'gpudb.log' or 'gpudb-proc.log' for execution information")
print("")

