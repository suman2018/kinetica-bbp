CREATE OR REPLACE TABLE ${table-prefix}${cms_stg-schema}.${table-prefix}cms_multidim_input
(
    "msisdn" BIGINT(shard_key) NOT NULL,
    "los" INTEGER(dict),
    "rev_avg_l3m" double,
    "area_sales" VARCHAR(8, dict),
    "region_sales" VARCHAR(32, dict),
    "cluster" VARCHAR(32, dict),
    "kabupaten" VARCHAR(32, dict),
    "lte_usim_user_flag" VARCHAR(8, dict),
    "pre2post_flag" VARCHAR(1,dict),
    "nik_id" VARCHAR(32),
    "device_type" VARCHAR(32,dict),
    "region_hlr" VARCHAR(32,dict)
)