CREATE OR REPLACE TABLE ${table-prefix}${cms_stg-schema}.${table-prefix}cms_definition_property_input
(
    "campid" VARCHAR(64, shard_key, dict) NOT NULL,
    "owner" VARCHAR(8, dict) NOT NULL,
    "productgroup" VARCHAR(16, dict) NOT NULL,
    "objective" VARCHAR(64, dict) NOT NULL,
    "typ" VARCHAR(32, dict) NOT NULL
)
