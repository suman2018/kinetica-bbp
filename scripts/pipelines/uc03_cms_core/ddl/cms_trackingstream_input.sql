CREATE OR REPLACE TABLE ${table-prefix}${cms_stg-schema}.${table-prefix}cms_trackingstream_input
(
    "campaignid" VARCHAR(64,dict) NOT NULL,
    "occuredon" BIGINT(dict) NOT NULL,
    "msisdn" BIGINT(shard_key) NOT NULL,
    "streamtype" VARCHAR(32,dict) NOT NULL,
    "iscontrolgroup" VARCHAR(8,dict) NOT NULL,
    "attr_revenue" BIGINT(dict) NOT NULL,
    "attr_recharge" INTEGER(dict) NOT NULL,
    "attr_loc_kecamatan" VARCHAR(32,dict) NOT NULL,
    "attr_loc_kabupaten" VARCHAR(32,dict) NOT NULL,
    "attr_loc_region" VARCHAR(16,dict) NOT NULL,
    "attr_cluster" VARCHAR(32,dict) NOT NULL
)