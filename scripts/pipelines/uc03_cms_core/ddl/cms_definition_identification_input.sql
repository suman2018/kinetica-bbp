CREATE OR REPLACE TABLE ${table-prefix}${cms_stg-schema}.${table-prefix}cms_definition_identification_input
(
    "campid" VARCHAR(64, shard_key, dict) NOT NULL,
    "stage" VARCHAR(8, dict) NOT NULL,
    "name" VARCHAR(128, dict) NOT NULL,
    "shortdesc" VARCHAR(256, dict) NOT NULL,
    "longdesc" VARCHAR(256, dict) NOT NULL,
    "starton" VARCHAR(32) NOT NULL,
    "endon" VARCHAR(32) NOT NULL,
    "campcontext" VARCHAR(64, dict) NOT NULL,
    "file_date" date(dict)
)
