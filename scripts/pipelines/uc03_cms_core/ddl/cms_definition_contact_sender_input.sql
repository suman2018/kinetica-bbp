CREATE OR REPLACE TABLE ${table-prefix}${cms_stg-schema}.${table-prefix}cms_definition_contact_sender_input
(
    "campid" VARCHAR(64, shard_key, dict) NOT NULL,
    "sender" VARCHAR(256, dict) NOT NULL,
    "wording" VARCHAR(data) NOT NULL
)


