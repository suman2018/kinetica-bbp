CREATE OR REPLACE TABLE ${table-prefix}${cms_stg-schema}.${table-prefix}cms_definition_contact_input
(
    "campid" VARCHAR(64, shard_key, dict) NOT NULL,
    "type" VARCHAR(64, dict) NOT NULL,
    "channel" VARCHAR(8, dict) NOT NULL,
    "name" VARCHAR(32, dict) NOT NULL,
    "value" VARCHAR(256, dict) NOT NULL,
    "wording" VARCHAR(data) NOT NULL,
    "file_date" date(dict)
)