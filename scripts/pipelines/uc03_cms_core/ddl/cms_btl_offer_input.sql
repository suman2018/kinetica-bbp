create or replace replicated table ${table-prefix}${cms_stg-schema}.${table-prefix}cms_btl_offer_input
(
    "content_id" VARCHAR(64),
    "channel" VARCHAR(16,dict),
    "in_cms" VARCHAR(1,dict),
    "detail_quota" VARCHAR(data),
    "ch_business_product" VARCHAR(16, dict)
)