CREATE OR REPLACE TABLE ${table-prefix}${cms_stg-schema}.${table-prefix}cms_definition_keyvalues_input
(
    "campid" VARCHAR(64, shard_key, dict) NOT NULL,
    "element" VARCHAR(32, dict) NOT NULL,
    "group" VARCHAR(8, dict) NOT NULL,
    "name" VARCHAR(128, dict) NOT NULL,
    "value" VARCHAR(data) NOT NULL,
    "operator" VARCHAR(1, dict) NOT NULL,
    "file_date" date(dict)
)