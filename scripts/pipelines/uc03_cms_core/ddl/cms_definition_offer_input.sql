CREATE OR REPLACE TABLE ${table-prefix}${cms_stg-schema}.${table-prefix}cms_definition_offer_input
(
    "campid" VARCHAR(64, shard_key, dict) NOT NULL,
    "serviceprovider" VARCHAR(16, dict) NOT NULL,
    "productfamily" VARCHAR(64, dict) NOT NULL,
    "businessproduct" VARCHAR(64, dict) NOT NULL,
    "productid" VARCHAR(16, dict) NOT NULL,
    "issubscription" VARCHAR(8, dict) NOT NULL,
    "adn" VARCHAR(1, dict) NOT NULL,
    "keyword" VARCHAR(1, dict) NOT NULL,
    "file_date" date(dict)
)
