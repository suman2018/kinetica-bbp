CREATE OR REPLACE TABLE ${table-prefix}${cms_stg-schema}.${table-prefix}cms_blacklist_input
(
    "campaign_id" VARCHAR(64, dict) NOT NULL,
    "occurred_on" VARCHAR(32, dict) NOT NULL,
    "msisdn" BIGINT(shard_key) NOT NULL,
    "streamtype" VARCHAR(16, dict) NOT NULL
);