CREATE OR REPLACE TABLE ${table-prefix}${cms_stg-schema}.${table-prefix}cms_definition_rules_input
(
    "campid" VARCHAR(64, shard_key, dict) NOT NULL,
    "group" VARCHAR(32, dict) NOT NULL,
    "name" VARCHAR(32, dict) NOT NULL,
    "value" VARCHAR(data) NOT NULL,
    "operator" VARCHAR(16, dict) NOT NULL,
    "aggregation_metric" VARCHAR(8, dict) NOT NULL,
    "aggregation_period" INTEGER(dict) ,
    "correlation_metric" VARCHAR(8, dict) NOT NULL,
    "correlation_period" INTEGER(dict),
    "offsetmetric" VARCHAR(8, dict) NOT NULL,
    "offeset_period" INTEGER(dict),
    "file_date" date(dict)
)
