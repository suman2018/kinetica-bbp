select distinct
campid
,type
,channel
,name
,value
,wording
,string(date(from_unixtime(unix_timestamp(file_date, 'yyyyMMdd')))) as file_date
from gpu.v_stg_cms_definition_contact