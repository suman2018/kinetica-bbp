select 
  cast(msisdn as bigint)
  , los
  , rev_avg_l3m
  , area_sales
  , region_sales
  , cluster
  , kabupaten
  , lte_usim_user_flag
  , pre2post_flag
  , nik_id
  , device_type
  , region_hlr
from gpu.v_multidim4
where cast(updt_dt as date) = date '${run-date=yyyy-MM-dd}'