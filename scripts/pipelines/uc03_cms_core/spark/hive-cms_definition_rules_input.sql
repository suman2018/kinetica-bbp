SELECT DISTINCT
campid
,group
,name
,value
,operator
,aggregation_metric
,CAST(aggregation_period AS INTEGER)
,correlation_metric
,CAST(correlation_period AS INTEGER)
,offsetmetric
,CAST(offeset_period AS INTEGER)
,string(date(from_unixtime(unix_timestamp(file_date, 'yyyyMMdd')))) as file_date
FROM gpu.v_stg_cms_definition_rules