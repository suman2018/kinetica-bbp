select 
    campid,
    max(value) as sender,
    max(wording) as wording     
from gpu.v_stg_cms_definition_contact
where upper(name) = 'SENDER'
and upper(type) like 'ELIGIBLE%'
group by campid