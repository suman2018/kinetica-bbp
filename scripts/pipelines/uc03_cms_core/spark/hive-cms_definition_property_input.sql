SELECT campid, MIN(owner) AS owner, MIN(productgroup) AS productgroup, MIN(objective) as objective, MIN(typ) as typ
FROM (
SELECT
campid
,CASE WHEN name = 'OWNER' THEN value END AS owner
,CASE WHEN name = 'PRODUCTGROUP' THEN value END AS productgroup
,CASE WHEN name = 'OBJECTIVE' THEN value END AS objective
,CASE WHEN name = 'TYPE' THEN value END AS typ
FROM gpu.v_stg_cms_definition_keyvalues
WHERE element = 'properties'
) 
GROUP BY 1
