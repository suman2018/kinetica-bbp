SELECT DISTINCT
campid
,element
,group
,name
,value
,operator
,string(date(from_unixtime(unix_timestamp(file_date, 'yyyyMMdd')))) as file_date
FROM gpu.v_stg_cms_definition_keyvalues