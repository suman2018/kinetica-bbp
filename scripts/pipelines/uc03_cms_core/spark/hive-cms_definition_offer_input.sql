SELECT DISTINCT
campid
,serviceprovider
,productfamily
,businessproduct
,productid
,issubscription
,adn
,keyword
,string(date(from_unixtime(unix_timestamp(file_date, 'yyyyMMdd')))) as file_date
FROM gpu.v_stg_cms_definition_offer
