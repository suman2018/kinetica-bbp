SELECT DISTINCT
campid
,stage
,name
,shortdesc
,longdesc
,cast(from_unixtime(unix_timestamp(starton, 'yyyyMMddHHmmss')) as string) as starton
,cast(from_unixtime(unix_timestamp(endon, 'yyyyMMddHHmmss')) as string) as endon
,campcontext
,string(date(from_unixtime(unix_timestamp(file_date, 'yyyyMMdd')))) as file_date
FROM gpu.v_stg_cms_definition_identification
where cast(from_unixtime(unix_timestamp(starton, 'yyyyMMddHHmmss')) as string) is not null
and cast(from_unixtime(unix_timestamp(endon, 'yyyyMMddHHmmss')) as string) is not null