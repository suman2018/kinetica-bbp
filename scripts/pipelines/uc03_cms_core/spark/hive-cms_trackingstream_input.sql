SELECT
	campaignid
	,CAST(occuredon AS BIGINT)
	,CAST(trim(identifier) AS BIGINT) as msisdn
	,streamtype
	,iscontrolgroup
	,CAST(attr_revenue AS BIGINT)
	,attr_recharge
	,attr_loc_kecamatan
	,attr_loc_kabupaten
	,attr_loc_region
	,attr_cluster
FROM gpu.v_stg_cms_trackingstream
where filedate = '${run-date=yyyyMMdd}'
and CAST(trim(identifier) AS BIGINT) is not null