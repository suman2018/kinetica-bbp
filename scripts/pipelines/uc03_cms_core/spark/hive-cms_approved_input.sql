SELECT DISTINCT
campaignid as campaign_id,
occurredon as occurred_on,
cast(identifier AS bigint) as msisdn,
streamtype
FROM gpu.gpu_cms_approved
where datex = '${run-date=yyyyMMdd}'
and cast(identifier AS bigint) is not null