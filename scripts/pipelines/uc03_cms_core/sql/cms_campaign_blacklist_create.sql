CREATE OR REPLACE TABLE ${table-prefix}${cms-schema}.${table-prefix}cms_campaign_blacklist
(
    "campaignid" VARCHAR(64, dict) NOT NULL,
    "occurredon" TIMESTAMP,
    "msisdn" BIGINT(shard_key) NOT NULL,
    "file_date" date(dict)
)
;

INSERT INTO ${table-prefix}${cms-schema}.${table-prefix}cms_campaign_blacklist
SELECT
        campaignid AS campaignid
        ,TIMESTAMP_FROM_DATE_TIME(char16(char4(substr(char16(cast(occuredon as bigint)),1,4))||'-'||char2(substr(char16(cast(occuredon as bigint)),5,2))||'-'||
        char2(substr(char16(cast(occuredon as bigint)),7,2)))
        ,char8(char2(substr(char16(cast(occuredon as bigint)),9,2))||':'||char2(substr(char16(cast(occuredon as bigint)),11,2))||':'||
        char2(substr(char16(cast(occuredon as bigint)),13,2)))) AS occurredon
        ,msisdn
        ,'${run-date=yyyy-MM-dd}' as file_date
FROM ${table-prefix}${cms_stg-schema}.${table-prefix}cms_trackingstream_input
WHERE UPPER(streamtype) = 'BLACKLIST'
UNION ALL
SELECT
        campaign_id AS campaignid
        ,TIMESTAMP_FROM_DATE_TIME(substr(occurred_on, 1,10), substr(occurred_on, 12,8)) AS occurredon
        ,msisdn
        ,'${run-date=yyyy-MM-dd}' as file_date
FROM ${table-prefix}${cms_stg-schema}.${table-prefix}cms_blacklist_input
;
