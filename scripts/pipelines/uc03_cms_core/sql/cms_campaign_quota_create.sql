create or replace replicated table ${table-prefix}${cms-schema}.${table-prefix}cms_campaign_quota_tmp
(
campaign_id varchar(64,dict),
quota varchar(data)
)
;

insert into ${table-prefix}${cms-schema}.${table-prefix}cms_campaign_quota_tmp
select 
campid as campaign_id
,value as quota
from ${table-prefix}${cms_stg-schema}.${table-prefix}cms_definition_keyvalues_input
where upper("element") = 'QUOTA'
and upper(name) = 'SUBSCRIBER-LIMIT'
;

alter table ${table-prefix}${cms-schema}.${table-prefix}cms_campaign_quota_tmp modify quota varchar(16)
;

create or replace replicated table ${table-prefix}${cms-schema}.${table-prefix}cms_campaign_quota
as
select distinct 
campaign_id,
int(substring(quota,7,length(quota)-6)) as quota
from ${table-prefix}${cms-schema}.${table-prefix}cms_campaign_quota_tmp
;

drop table if exists ${table-prefix}${cms-schema}.${table-prefix}cms_campaign_quota_tmp
;
