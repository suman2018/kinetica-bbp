/* KI_HINT_UPDATE_ON_EXISTING_PK */
insert into ${table-prefix}${cms-schema}.${table-prefix}cms_campaign_sender
select 
	campid
	,sender
	,wording
from ${table-prefix}${cms_stg-schema}.${table-prefix}cms_definition_contact_sender_input