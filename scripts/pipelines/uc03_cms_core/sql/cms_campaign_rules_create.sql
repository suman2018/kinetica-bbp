create or replace replicated table ${table-prefix}${cms-schema}.${table-prefix}cms_campaign_rules
(
    "campaign_id" VARCHAR(64, dict),
    "grp" VARCHAR(32, dict),
    "taker_identify" VARCHAR(32, dict),
    "value_identify" VARCHAR(256, dict) NOT NULL,
    "is_btl" INTEGER NOT NULL,
    "file_date" DATE NOT NULL
)
;

insert into ${table-prefix}${cms-schema}.${table-prefix}cms_campaign_rules
select
    char64(campid) as campaign_id,
    char32("group") as grp,
    char32(name)   as taker_identify,
    tkn    as value_identify,
    max(case when atlbtl.content_id is null then 0 else 1 end) as is_btl,
    date('${run-date=yyyy-MM-dd}') as file_date
from ${table-prefix}${cms_stg-schema}.${table-prefix}cms_definition_rules_strsplit
left join ${table-prefix}${cms_stg-schema}.${table-prefix}cms_btl_offer_input atlbtl
	on tkn = atlbtl.content_id
group by 1,2,3,4
;

