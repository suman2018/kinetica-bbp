create or replace replicated table ${table-prefix}${cms-schema}.${table-prefix}cms_campaign_definition(
   campaign_id               varchar(64, dict) not null
  ,start_period              timestamp
  ,end_period                timestamp
  ,product_group             varchar(16, dict)
  ,campaign_group_id	     int(dict)
  ,campaign_objective_id     int(dict)
  ,campaign_name             varchar(128, dict)
  ,business_group            varchar(8, dict)
  ,initiated_group           varchar(32, dict)
  ,sender                    varchar(256, dict)
  ,campaign_type             varchar(32, dict)
  ,wording                   varchar(data)
  ,segment                   varchar(256, dict)
  ,file_date                 date(dict)
)
;

insert into ${table-prefix}${cms-schema}.${table-prefix}cms_campaign_definition
select
    a.campaign_id,
    a.start_period,
    a.end_period,
    case	
        when substr(a.campaign_id,9,1) = 'A' then 'VOICESMS'
	    when substr(a.campaign_id,9,1) = 'B' then 'BROADBAND'
	    when substr(a.campaign_id,9,1) = 'C' then 'LOYALTY'
	    when substr(a.campaign_id,9,1) = 'D' then 'DEVICE BUNDLING'
        when substr(a.campaign_id,9,1) = 'E' then 'DIGITAL'
        when substr(a.campaign_id,9,1) = 'F' then 'RECHARGE'
        when substr(a.campaign_id,9,1) = 'G' then 'INFORMATION'
        when substr(a.campaign_id,9,1) = 'H' then 'MULTISERVICE'
    end as product_group,
    e.campaign_group_id,
    e.campaign_objective_id,
    a.campaign_name,
    e.owner        as business_group,
    case
        when e.owner = 'REG10' then '01. Area Sumatra'         --area 1
        when e.owner = 'REG11' then '02. Sumbagut'             --area 1
        when e.owner = 'REG12' then '03. Sumbagteng'           --area 1
        when e.owner = 'REG13' then '04. Sumbagsel'            --area 1
        when e.owner = 'REG20' then '05. Area Jabotabek Jabar' --area 2
        when e.owner = 'REG21' then '06. Jabotabek West'       --area 2
        when e.owner = 'REG22' then '07. Jabotabek Central'    --area 2
        when e.owner = 'REG23' then '08. Jabotabek East'       --area 2
        when e.owner = 'REG24' then '09. Jabar'                --area 2
        when e.owner = 'REG30' then '10. Area Jawa Bali'       --area 3
        when e.owner = 'REG31' then '11. Jateng'               --area 3
        when e.owner = 'REG32' then '12. Jatim'                --area 3
        when e.owner = 'REG33' then '13. Balinusra'            --area 3
        when e.owner = 'REG40' then '14. Area Pamasuka'        --area 4
        when e.owner = 'REG41' then '15. Kalimantan'           --area 4
        when e.owner = 'REG42' then '16. Sulawesi'             --area 4
        when e.owner = 'REG43' then '17. Puma'                 --area 4
        else '18. HQ'
    end as initiated_group,
    c.sender,
    e.typ           as campaign_type, 
    c.wording, 
    a.segment,
    '${run-date=yyyy-MM-dd}' as file_date
--    d.taker_identify, 
--    d.value_identify,
--    null as status --a.status
from
(
    select 
        ci.campid       as campaign_id
        ,min(datetime(ci.starton    ))  as start_period
        ,min(datetime(ci.endon      ))  as end_period
        ,min(ci.name       )  as campaign_name
        ,min(ci.longdesc   )  as segment
        --,min(cs.cmpgn_sttus)  as status
    from ${table-prefix}${cms_stg-schema}.${table-prefix}cms_definition_identification_input ci
    where upper(substring(ci.campid, 11, 1)) in ('A', 'B', 'C', 'D', 'E', 'F')
    	and abs(datediff('DAY', date(datetime(ci.starton)), date(datetime(ci.endon)))) <= 30
	group by 1
) a
left join
(
	select 
	    campid as cmp_id,
        sender,
        wording
    from ${table-prefix}${cms_stg-schema}.${table-prefix}cms_definition_contact_sender_input
) c on a.campaign_id = c.cmp_id
left join
(
	select
		 prop.campid
		,prop.owner
		,prop.productgroup
		,obj.campaign_group_id as campaign_group_id
		,obj.campaign_objective_id as campaign_objective_id
		,prop.typ
	from ${table-prefix}${cms_stg-schema}.${table-prefix}cms_definition_property_input prop
	join ${table-prefix}${cms-schema}.${table-prefix}cms_campaign_objective obj
	on upper(prop.objective) = upper(obj.campaign_objective_desc)
) e on a.campaign_id = e.campid
;
