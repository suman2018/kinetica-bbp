delete from ${table-prefix}${cms-schema}.${table-prefix}cms_campaign_trackingstream
where file_date = date('${run-date=yyyy-MM-dd}')
;

INSERT INTO ${table-prefix}${cms-schema}.${table-prefix}cms_campaign_trackingstream
SELECT
        campaignid
        ,TIMESTAMP_FROM_DATE_TIME(char16(char4(substr(char16(cast(occuredon as bigint)),1,4))||'-'||char2(substr(char16(cast(occuredon as bigint)),5,2))||'-'||
        char2(substr(char16(cast(occuredon as bigint)),7,2)))
        ,char8(char2(substr(char16(cast(occuredon as bigint)),9,2))||':'||char2(substr(char16(cast(occuredon as bigint)),11,2))||':'||
        char2(substr(char16(cast(occuredon as bigint)),13,2)))) AS occurredon
        ,msisdn
        ,streamtype
        ,iscontrolgroup
        ,attr_revenue
        ,attr_recharge
        ,attr_loc_kecamatan
        ,attr_loc_kabupaten
        ,attr_loc_region
        ,attr_cluster
        ,'${run-date=yyyy-MM-dd}' as file_date
FROM ${table-prefix}${cms_stg-schema}.${table-prefix}cms_trackingstream_input
UNION ALL
SELECT
        campaign_id AS campaignid
        ,TIMESTAMP_FROM_DATE_TIME(substr(occurred_on, 1,10), substr(occurred_on, 12,8)) AS occurredon
        ,msisdn
        ,streamtype
        ,NULL AS iscontrolgroup
        ,CAST(NULL AS BIGINT) AS attr_revenue
        ,CAST(NULL AS INTEGER) AS attr_recharge
        ,NULL AS attr_loc_kecamatan
        ,NULL AS attr_loc_kabupaten
        ,NULL AS attr_loc_region
        ,NULL AS attr_cluster
        ,'${run-date=yyyy-MM-dd}' as file_date
FROM ${table-prefix}${cms_stg-schema}.${table-prefix}cms_approved_input
UNION ALL
SELECT
        campaign_id AS campaignid
        ,TIMESTAMP_FROM_DATE_TIME(substr(occurred_on, 1,10), substr(occurred_on, 12,8)) AS occurredon
        ,msisdn
        ,streamtype
        ,NULL AS iscontrolgroup
        ,CAST(NULL AS BIGINT) AS attr_revenue
        ,CAST(NULL AS INTEGER) AS attr_recharge
        ,NULL AS attr_loc_kecamatan
        ,NULL AS attr_loc_kabupaten
        ,NULL AS attr_loc_region
        ,NULL AS attr_cluster
        ,'${run-date=yyyy-MM-dd}' as file_date
FROM ${table-prefix}${cms_stg-schema}.${table-prefix}cms_submitted_input
;

delete from ${table-prefix}${cms-schema}.${table-prefix}cms_campaign_trackingstream
where file_date < date('${run-date=yyyy-MM-dd}') - interval '30' day
;
