create or replace materialized view ${table-prefix}${cms-schema}.${table-prefix}cms_campaign_trackingstream_multidim_v
as
select
    cdef.product_group as category
    ,cdef.campaign_type
    ,ctrk.msisdn
    ,cdef.campaign_id
    ,cdef.campaign_objective_id
    ,cdef.campaign_name
    ,cdef.start_period
    ,cdef.end_period
    ,ctrk.iscontrolgroup
    ,cdef.campaign_group_id
    ,cdef.segment
    ,mdim.los
    ,mdim.rev_avg_l3m as arpu
    ,cdef.business_group
    ,cdef.initiated_group
--    ,mdim.area_sales
    ,mdim.region_hlr
    ,mdim.cluster
    ,mdim.kabupaten
    ,mdim.lte_usim_user_flag
    ,mdim.nik_id
    ,mdim.device_type
    ,mdim.pre2post_flag
    ,cdef.file_date
    ,ctrk.target
    ,ctrk.eligible
from ${table-prefix}${cms-schema}.${table-prefix}cms_campaign_definition cdef
join (
	select 
	tgt.msisdn
	,tgt.campaign_id
	,coalesce(elgb.iscontrolgroup, 'false') as iscontrolgroup
	,elgb.eligible
	,tgt.target
	from 
	( 
		select msisdn, campaign_id, iscontrolgroup, count(*) as target
		from ${table-prefix}${cms-schema}.${table-prefix}cms_campaign_trackingstream
		where upper(streamtype) = 'SUBMITTEDTARGET'
		group by 1,2,3
	) tgt
	left join 
	(
		select msisdn, campaign_id, iscontrolgroup,  count(*) as eligible
		from ${table-prefix}${cms-schema}.${table-prefix}cms_campaign_trackingstream
		where upper(streamtype) = 'ELIGIBLE'
		group by 1,2,3
	) elgb
	on elgb.msisdn = tgt.msisdn
		and elgb.campaign_id = tgt.campaign_id
) ctrk
on cdef.campaign_id = ctrk.campaign_id
left join ${table-prefix}${cms_stg-schema}.${table-prefix}cms_multidim_input mdim
	on ctrk.msisdn = mdim.msisdn
;

alter schema ${table-prefix}${cms-schema} set protected true
;
