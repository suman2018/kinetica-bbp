delete from ${table-prefix}${cms-schema}.${table-prefix}cms_campaign_tracking_detail
where file_date = date('${run-date=yyyy-MM-dd}')
and pipeline_name = '${pipeline-name}'
and pipeline_step = '${pipeline-step}';

/* KI_HINT_DICT_PROJECTION */
create or replace temp table ${table-prefix}${cms-schema}.${table-prefix}${pipeline-name}_tmp
as
select 
	cview.category
	,cview.campaign_type
	,cview.msisdn
	,cview.campaign_id
	,cview.campaign_objective_id
	,cview.campaign_name
	,cview.start_period
	,cview.end_period
	,cview.iscontrolgroup
	,cview.campaign_group_id
	,cview.segment
	,cview.los
	,cview.arpu
	,cview.business_group
	,cview.initiated_group
	--,cview.area_sales
	,cview.region_hlr
	,cview.cluster
	,cview.kabupaten
	,date('${run-date=yyyy-MM-dd}') as trx_date
	,case when cview.eligible > 0 and (pst.msisdn is not null or upper(cview.pre2post_flag) = 'Y') then 1 else 0 end as metric_3
	,0 as is_btl
	,cview.target
	,cview.eligible
	,0 as taker
	,date('${run-date=yyyy-MM-dd}') as file_date
	,'${job-id}'
	,'${pipeline-name}'
	,'${pipeline-step}'
from ${table-prefix}${cms-schema}.${table-prefix}cms_campaign_trackingstream_multidim_v cview
left join ${table-prefix}${cms_stg-schema}.${table-prefix}cms_sales_post_daily_input pst
	on pst.msisdn = cview.msisdn
where cview.campaign_group_id = 8
	and date('${run-date=yyyy-MM-dd}') between date(cview.start_period) and date(cview.end_period)
;

insert into ${table-prefix}${cms-schema}.${table-prefix}cms_campaign_tracking_detail
(
	 category
	,campaign_type
	,msisdn
	,campaign_id
	,campaign_objective_id
	,campaign_name
	,start_period
	,end_period
	,iscontrolgroup
	,campaign_group_id
	,segment
	,los
	,arpu
	,business_group
	,initiated_group
	--,area
	,region_hlr
	,cluster
	,branch
	,trx_date
	,metric_3
	,is_btl
	,target
	,eligible
	,taker
	,file_date
	,orch_job_id
	,pipeline_name
	,pipeline_step
)
select * from ${table-prefix}${cms-schema}.${table-prefix}${pipeline-name}_tmp
;

--update takers
create or replace table ${table-prefix}${cms-schema}.${table-prefix}${pipeline-name}_tmp
as
	select 
	category
	,campaign_type
	,msisdn
	,campaign_id
	,campaign_objective_id
	,campaign_name
	,start_period
	,end_period
	,iscontrolgroup
	,campaign_group_id
	,segment
	,los
	,arpu
	,business_group
	,initiated_group
	--,area
	,region_hlr
	,cluster
	,branch
	,trx_date
	,content_1
	,content_2
	,content_3
	,metric_1
	,metric_2
	,metric_3
	,is_btl
	,target
	,eligible
	,case when (sum(metric_3) over (partition by campaign_id, msisdn order by trx_date rows between unbounded preceding and current row) = 1) and metric_3 = 1 then 1 else 0 end as taker
	,file_date
	,orch_job_id
	,pipeline_name
	,pipeline_step
	from ${table-prefix}${cms-schema}.${table-prefix}cms_campaign_tracking_detail
	where campaign_group_id = 8
		and date('${run-date=yyyy-MM-dd}') between date(start_period) and date(end_period)
;

delete from ${table-prefix}${cms-schema}.${table-prefix}cms_campaign_tracking_detail
where campaign_group_id = 8
	and date('${run-date=yyyy-MM-dd}') between date(start_period) and date(end_period)
;

insert into ${table-prefix}${cms-schema}.${table-prefix}cms_campaign_tracking_detail
select * from ${table-prefix}${cms-schema}.${table-prefix}${pipeline-name}_tmp
;


drop table ${table-prefix}${cms-schema}.${table-prefix}${pipeline-name}_tmp
;