select
string(date(timestamp(from_unixtime(unix_timestamp(activation_date, 'yyyyMMdd'))))) as activation_date
,int(subs_id) as subs_id                  
,bigint(trim(msisdn)) as msisdn                   
,int(account_id) as account_id               
,int(prefix) as prefix                   
,area_hlr
,region_hlr
,city_hlr
,int(price_plan_id) as price_plan_id            
,offer
,brand
,int(bill_cycle) as bill_cycle               
,cust_type
,cust_type_desc
,cust_subtype
,cust_subtype_desc
,pre_to_post_flag
,cust_type_change_flag
,prev_cust_type
,prev_cust_subtype
,ownership_change_flag
,string(date(timestamp(from_unixtime(unix_timestamp(first_usage_date, 'yyyyMMdd'))))) as first_usage_date
,int(lacci_id) as lacci_id                 
,int(lac) as lac                      
,int(ci) as ci                       
,cgi_postpaid
,node
,area_name
,regional_channel
,branch
,sub_branch
,cluster_sales
,kabupaten
,lctn_closing_flag
from gpu.gpu_sales_post_daily_new
where datex = '${run-date=yyyyMMdd}'