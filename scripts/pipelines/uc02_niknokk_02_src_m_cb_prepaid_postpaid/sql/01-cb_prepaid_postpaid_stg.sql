-- Author: Sadar Baskoro <ssetihajid@kinetica.com>
-- Last Update: 23 Dec 2018
-- Objective: create cb prepaid postpaid monthly, values will be replaced with new data for running date


drop table if exists ${table-prefix}${niknokk-schema}.${table-prefix}niknokk_cb_prepaid_postpaid_${run-date=yyyyMM};

alter table ${table-prefix}${niknokk-schema}.${table-prefix}niknokk_cb_prepaid_postpaid_stg rename to ${table-prefix}niknokk_cb_prepaid_postpaid_${run-date=yyyyMM};

