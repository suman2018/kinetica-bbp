CREATE OR REPLACE TABLE ${table-prefix}${cms_stg-schema}.${table-prefix}cms_digital_tracking_file_input
(
    "idfa" VARCHAR(64, dict),
    "gps_adid" VARCHAR(64, dict),
    "activity_kind" VARCHAR(8, dict),
    "event_name" VARCHAR(16, dict),
    "advertisingid" VARCHAR(64, dict),
    "userid" VARCHAR(32, dict),
    "packagename" VARCHAR(64, dict),
    "packageprice" double,
    "packageid" VARCHAR(64, dict),
    "transactionid" VARCHAR(32, dict, shard_key),
    "created_at" VARCHAR(16, dict),
    "app_version_short" VARCHAR(32, dict)
)