delete from ${table-prefix}${cms-schema}.${table-prefix}cms_campaign_tracking_detail
where file_date = date('${run-date=yyyy-MM-dd}')
and pipeline_name = '${pipeline-name}'
and pipeline_step = '${pipeline-step}';


/* KI_HINT_DICT_PROJECTION */
create or replace table ${table-prefix}${cms-schema}.${table-prefix}${pipeline-name}_tmp
as
select *,ki_shard_key(msisdn) from (
select 
    cast(-10000*rand() as bigint) as msisdn
    ,'' as campaign_id
    ,12 as campaign_group_id
    ,date('${run-date=yyyy-MM-dd}') as trx_date
    ,digi.packageid as content_1
    ,digi.userid as content_2
    ,digi.packagename as content_3
    ,digi.packageprice as metric_1
    ,case when digi.advertisingid = null then 0 else 1 end as taker
    ,date('${run-date=yyyy-MM-dd}') as file_date
    ,'${job-id}' as orch_job_id
    ,'${pipeline-name}' as pipeline_name
    ,'${pipeline-step}' as pipeline_step
from ${table-prefix}${cms_stg-schema}.${table-prefix}cms_digital_tracking_file_input digi
)
;

insert into ${table-prefix}${cms-schema}.${table-prefix}cms_campaign_tracking_detail
(
   msisdn
    ,campaign_id
    ,campaign_group_id
    ,trx_date
    ,content_1
    ,content_2
    ,content_3
    ,metric_1
    ,taker
    ,file_date
    ,orch_job_id
    ,pipeline_name
    ,pipeline_step
    ,start_period
    ,end_period
)
select
     msisdn
    ,campaign_id
    ,campaign_group_id
    ,trx_date
    ,content_1
    ,content_2
    ,content_3
    ,metric_1
    ,taker
    ,file_date
    ,orch_job_id
    ,pipeline_name
    ,pipeline_step
    ,cast(null as timestamp) as start_period
    ,cast(null as timestamp) as end_period
 from ${table-prefix}${cms-schema}.${table-prefix}${pipeline-name}_tmp
;

drop table ${table-prefix}${cms-schema}.${table-prefix}${pipeline-name}_tmp
;
