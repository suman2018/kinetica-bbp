create or replace table ${table-prefix}${msight_ooh-schema}.${table-prefix}ooh_summ_visit_week_input
(
    "months" varchar(8, dict),
    "location_code" varchar(32, dict),
    "id_dtl"        varchar(8, dict),
    "id_bearing"    varchar(8, dict), 
    "id_distance_ac" varchar(8, dict),
    "id_distance_cb" varchar(8, dict),
    "visittime_weekenddays"           varchar(8, dict),
    "total"         long (dict) 
)

