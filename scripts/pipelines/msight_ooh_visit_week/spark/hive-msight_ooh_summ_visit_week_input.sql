select
months,
location_code, 
cast(id_dtl as string) as id_dtl,
cast(id_bearing as string) as id_bearing, 
cast(id_distance_ac as string) as id_distance_ac,
cast(id_distance_cb as string) id_distance_cb,
visittime_weekenddays,
total
from skp.ooh_summ_visit_week_${run-date=yyyyMM}

