create or replace table ${table-prefix}${msight_ooh-schema}.${table-prefix}ooh_summ_visit_time_stg as
SELECT
months,
location_code,
cast(id_dtl as decimal(2,1)) as id_dtl,
cast(id_bearing as integer) as id_bearing,
cast(id_distance_ac as integer) as id_distance_ac,
cast(id_distance_cb as integer) as id_distance_cb,
visittime_daynight,
CAST (total as integer) as total,
ki_shard_key (id_bearing, id_dtl, id_distance_ac, id_distance_cb) 
FROM ${table-prefix}${msight_ooh-schema}.${table-prefix}ooh_summ_visit_time_input;

alter table ${table-prefix}${msight_ooh-schema}.${table-prefix}ooh_summ_visit_time_stg modify column months varchar(8, dict) ;
alter table ${table-prefix}${msight_ooh-schema}.${table-prefix}ooh_summ_visit_time_stg modify column location_code varchar(32, dict) ;

alter table ${table-prefix}${msight_ooh-schema}.${table-prefix}ooh_summ_visit_time_stg modify column visittime_daynight varchar(8, dict) ;
alter table ${table-prefix}${msight_ooh-schema}.${table-prefix}ooh_summ_visit_time_stg modify column total integer (dict);

delete from ${table-prefix}${msight_ooh-schema}.${table-prefix}ooh_summ_visit_time where months = '${run-date=yyyyMM}';

insert into ${table-prefix}${msight_ooh-schema}.${table-prefix}ooh_summ_visit_time
select * from ${table-prefix}${msight_ooh-schema}.${table-prefix}ooh_summ_visit_time_stg;

DROP TABLE IF EXISTS ${table-prefix}${msight_ooh-schema}.${table-prefix}ooh_summ_visit_time_stg;
DROP TABLE IF EXISTS ${table-prefix}${msight_ooh-schema}.${table-prefix}ooh_summ_visit_time_input;

