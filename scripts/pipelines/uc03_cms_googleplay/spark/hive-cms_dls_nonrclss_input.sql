select
cast(dt_dt as string)
,mnth
,cast(msisdn as bigint)
,lac
,ci
,lacci_id
,actvtn_chnnl_id
,nd_typ_nme
,content_id
,vascode
,l3
,service
,divisi
,region
,clustr
,clm_revenue
,tot_trx
,tot_revenue
from gpu.dd_dls_nonrclss_new
where datex = '${run-date=yyyyMMdd}'