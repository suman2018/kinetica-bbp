delete from ${table-prefix}${cms-schema}.${table-prefix}cms_campaign_tracking_detail
where file_date = date('${run-date=yyyy-MM-dd}')
and pipeline_name = '${pipeline-name}'
and pipeline_step = '${pipeline-step}';

/* KI_HINT_DICT_PROJECTION */
create or replace temp table ${table-prefix}${cms-schema}.${table-prefix}${pipeline-name}_tmp
as
select
    cview.category
    ,cview.campaign_type
    ,cview.msisdn
    ,cview.campaign_id
    ,cview.campaign_objective_id
    ,cview.campaign_name
    ,cview.start_period
    ,cview.end_period
    ,cview.iscontrolgroup
    ,cview.campaign_group_id
    ,cview.segment
    ,cview.los
    ,cview.arpu
    ,cview.business_group
    ,cview.initiated_group
    --,cview.area_sales
    ,cview.region_hlr
    ,cview.cluster
    ,cview.kabupaten
    ,COALESCE(clss.dt_dt, date('${run-date=yyyy-MM-dd}')) as trx_date
    ,clss.tot_revenue as metric_1
    ,crules.value_identify as metric_3
    ,0 as is_btl
    ,cview.target
    ,cview.eligible
    ,0 as taker
    ,date('${run-date=yyyy-MM-dd}') as file_date
	,'${job-id}' as job_id
	,'${pipeline-name}' as pipeline_name
	,'${pipeline-step}' as pipeline_step
from 
(
	select *
	from ${table-prefix}${cms-schema}.${table-prefix}cms_campaign_trackingstream_multidim_v
	where campaign_group_id = 7
        and upper(campaign_name) like '%GPLAY%'
        and date('${run-date=yyyy-MM-dd}') between date(start_period) and date(end_period)
)
cview
left join
(
	select 
		dt_dt
		, msisdn
		, sum(tot_revenue) as tot_revenue
	from ${table-prefix}${cms-schema}.${table-prefix}cms_dls_nonrclss_detail
    where upper(service) = 'GOOGLE PLAYSTORE'
    	and file_date = date('${run-date=yyyy-MM-dd}')
    	and tot_revenue > 0
    group by 1,2
) clss
on cview.msisdn = clss.msisdn
	and clss.dt_dt between date(start_period) and date(end_period)
left join 
(
	select distinct
		campaign_id
		,int(value_identify) as value_identify
	from ${table-prefix}${cms-schema}.${table-prefix}cms_campaign_rules
	where taker_identify = 'SUM_AMOUNT'
) crules
on cview.campaign_id = crules.campaign_id
;
	
insert into ${table-prefix}${cms-schema}.${table-prefix}cms_campaign_tracking_detail
(
	 category
	,campaign_type
	,msisdn
	,campaign_id
	,campaign_objective_id
	,campaign_name
	,start_period
	,end_period
	,iscontrolgroup
	,campaign_group_id
	,segment
	,los
	,arpu
	,business_group
	,initiated_group
	--,area
	,region_hlr
	,cluster
	,branch
	,trx_date
	,metric_1
	,metric_3
	,is_btl
	,target
	,eligible
	,taker
	,file_date
	,orch_job_id
	,pipeline_name
	,pipeline_step
)
select * from ${table-prefix}${cms-schema}.${table-prefix}${pipeline-name}_tmp
;

---
create or replace temp table ${table-prefix}${cms-schema}.${table-prefix}${pipeline-name}_tmp1
as 
       select
        category
        ,campaign_type
        ,msisdn
        ,campaign_id
        ,campaign_objective_id
        ,campaign_name
        ,start_period
        ,end_period
        ,iscontrolgroup
        ,campaign_group_id
        ,segment
        ,los
        ,arpu
        ,business_group
        ,initiated_group
        --,area
        ,region_hlr
        ,cluster
        ,branch
        ,trx_date
        ,content_1
        ,content_2
        ,content_3
        ,metric_1
        ,metric_2
        ,metric_3
        ,is_btl
        ,target
        ,eligible
        ,case when coalesce((sum(metric_1) over (partition by campaign_id, msisdn order by trx_date rows between unbounded preceding and current row)),0) >= metric_3
        and coalesce((sum(metric_1) over (partition by campaign_id, msisdn order by trx_date rows between unbounded preceding and 1 preceding)),0) < metric_3
         then 1 else 0 end as first_taker
        ,file_date
        ,orch_job_id
        ,pipeline_name
        ,pipeline_step
        from ${table-prefix}${cms-schema}.${table-prefix}cms_campaign_tracking_detail
        where campaign_group_id = 7
        and upper(campaign_name) like '%GPLAY%'
        and date('${run-date=yyyy-MM-dd}') between date(start_period) and date(end_period)
                and metric_1 > 0
                and eligible > 0
; 


--update takers
create or replace table ${table-prefix}${cms-schema}.${table-prefix}${pipeline-name}_tmp
as
select
	category
	,campaign_type
	,msisdn
	,campaign_id
	,campaign_objective_id
	,campaign_name
	,start_period
	,end_period
	,iscontrolgroup
	,campaign_group_id
	,segment
	,los
	,arpu
	,business_group
	,initiated_group
	--,area
	,region_hlr
	,cluster
	,branch
	,trx_date
	,content_1
	,content_2
	,content_3
	,metric_1
	,metric_2
	,metric_3
	,is_btl
	,target
	,eligible
	,max(first_taker) over (partition by campaign_id, msisdn order by trx_date desc, first_taker desc rows between unbounded preceding and current row) as taker
	,file_date
	,orch_job_id
	,pipeline_name
	,pipeline_step
from ${table-prefix}${cms-schema}.${table-prefix}${pipeline-name}_tmp1
;

delete from ${table-prefix}${cms-schema}.${table-prefix}cms_campaign_tracking_detail
where campaign_group_id = 7
    and upper(campaign_name) like '%GPLAY%'
    and date('${run-date=yyyy-MM-dd}') between date(start_period) and date(end_period)
	and metric_1 > 0
	and eligible > 0
;

insert into ${table-prefix}${cms-schema}.${table-prefix}cms_campaign_tracking_detail
select * from ${table-prefix}${cms-schema}.${table-prefix}${pipeline-name}_tmp
;

drop table ${table-prefix}${cms-schema}.${table-prefix}${pipeline-name}_tmp
;
drop table ${table-prefix}${cms-schema}.${table-prefix}${pipeline-name}_tmp1
;

