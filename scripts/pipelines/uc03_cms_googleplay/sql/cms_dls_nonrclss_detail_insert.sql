delete from ${table-prefix}${cms-schema}.${table-prefix}cms_dls_nonrclss_detail
where file_date = date('${run-date=yyyy-MM-dd}')
;

insert into ${table-prefix}${cms-schema}.${table-prefix}cms_dls_nonrclss_detail
select
	t.*
	,date('${run-date=yyyy-MM-dd}') as file_date
	,'${job-id}' as job_id
	,'${pipeline-name}' as pipeline_name
	,'${pipeline-step}' as pipeline_step
from ${table-prefix}${cms_stg-schema}.${table-prefix}cms_dls_nonrclss_input t
;

delete from ${table-prefix}${cms-schema}.${table-prefix}cms_dls_nonrclss_detail
where file_date < date('${run-date=yyyy-MM-dd}') - interval '30' day
;
