CREATE OR REPLACE TABLE ${table-prefix}${cms_stg-schema}.${table-prefix}cms_dls_nonrclss_input
(
    "dt_dt" date(dict),
    "mnth" VARCHAR(8, dict),
    "msisdn" LONG(shard_key),
    "lac" VARCHAR(16, dict),
    "ci" VARCHAR(16, dict),
    "lacci_id" VARCHAR(4, dict),
    "actvtn_chnnl_id" VARCHAR(8, dict),
    "nd_typ_nme" VARCHAR(64, dict),
    "content_id" VARCHAR(64, dict),
    "vascode" VARCHAR(32, dict),
    "l3" VARCHAR(32, dict),
    "service" VARCHAR(32, dict),
    "divisi" VARCHAR(32, dict),
    "region" VARCHAR(32, dict),
    "clustr" VARCHAR(32, dict),
    "clm_revenue" REAL,
    "tot_trx" REAL,
    "tot_revenue" REAL
)