-- this implements the script: hvc_summary_traffic.sh
-- seems to depend on HVC_TRAFFIC_ROLLING_STG being dropped and recreated and staged from external file.
-- seems to depend on HVC_TRAFFIC_ROLLING being dropped and recreated.
-- seems to depend on HVC_TRAFFIC_SUMM being dropped and recreated.

-- seems to depend on hvc_summary_cb existing and being maintained (hvc_summary_cb.sh)
-- seems to depend on hvc_summary_cb_new existing and being maintained (hvc_summary_cb.sh)

-- And an overwrite again. Might convert to a CTA. Maybe temp?
-- insert overwrite table it.hvc_traffic_summ partition (cut_off_dt='${period_dash}')

delete from ${table-prefix}${hvc-schema}.${table-prefix}hvc_traffic_summ where cut_off_dt = DATE  '${run-date=yyyy-MM-dd}' ;


create or replace temp table ${table-prefix}${hvc-schema}.${table-prefix}hvc_traffic_summ_temp as 
select
    char16(a.cut_off_report) as cut_off_report ,  -- @Mate: I added cast
    coalesce(b.los_segment,'<= 3 MONTHS') as los_segment,
    char8(coalesce(b.arpu_segment,'0-50K')) as arpu_segment,
    char4(coalesce(b.hvc_segment,'LAC')) as hvc_segment,
    char8(coalesce(b.brnd_nme, 'UNKNOWN')) as brand,
    char8(coalesce(b.area_name, 'UNKNOWN')) as area_name,
    coalesce(b.region_name, 'UNKNOWN') as region_name,
    coalesce(b.cluster_name, 'UNKNOWN') as cluster_name,
    
    sum(rev_total_broadband) as rev_total_broadband,
    sum(rev_broadband_pckg) as rev_broadband_pckg,
    sum(rev_broadband_payu) as rev_broadband_payu,
    sum(total_payload_broadband) as total_payload_broadband,
    sum(payload_pckg) as payload_pckg,
    sum(payload_payu) as payload_payu,
    char8(arpu_segment_broadband),
    char8(arpu_segment_broadband_pckg),
    char8(arpu_segment_broadband_payu),
    count(case when coalesce(rev_total_broadband,0) > 0 then a.msisdn end) as broadband_subs,
    count(case when coalesce(rev_broadband_pckg,0) > 0 then a.msisdn end) as broadband_pckg_subs,
    count(case when coalesce(rev_broadband_payu,0) > 0 then a.msisdn end) as broadband_payu_subs,
    count(case when dominant_rev_broadband='NO DOMINANT' then a.msisdn end) as no_dominant_rev_broad_subs,
    count(case when dominant_rev_broadband='BROADBAND PACKAGE' then a.msisdn end) as dominant_rev_broad_pckg_subs,
    count(case when dominant_rev_broadband='BROADBAND PAYU' then a.msisdn end) as dominant_rev_broad_payu_subs,
    count(case when dominant_payload_broadband='NO DOMINANT' then a.msisdn end) as no_dominant_payload_broad_subs,
    count(case when dominant_payload_broadband='BROADBAND PACKAGE' then a.msisdn end) as dominant_payload_broad_pckg_subs,
    count(case when dominant_payload_broadband='BROADBAND PAYU' then a.msisdn end) as dominant_payload_broad_payu_subs,
    sum(rev_total_sms) as rev_total_sms,
    sum(rev_sms_pckg) as rev_sms_pckg,
    sum(rev_sms_payu) as rev_sms_payu,
    sum(total_cnt_sms) as total_cnt_sms,
    sum(cnt_sms_pckg) as cnt_sms_pckg,
    sum(cnt_sms_payu) as cnt_sms_payu,
    char8(arpu_segment_sms),
    char8(arpu_segment_sms_pckg),
    char8(arpu_segment_sms_payu),
    count(case when coalesce(rev_total_sms,0) > 0 then a.msisdn end) as sms_subs,
    count(case when coalesce(rev_sms_pckg,0) > 0 then a.msisdn end) as sms_pckg_subs,
    count(case when coalesce(rev_sms_payu,0) > 0 then a.msisdn end) as sms_payu_subs,
    count(case when dominant_rev_sms='NO DOMINANT' then a.msisdn end) as no_dominant_rev_sms_subs,
    count(case when dominant_rev_sms='SMS PACKAGE' then a.msisdn end) as dominant_rev_sms_pckg_subs,
    count(case when dominant_rev_sms='SMS PAYU' then a.msisdn end) as dominant_rev_sms_payu_subs,
    count(case when dominant_payload_sms='NO DOMINANT' then a.msisdn end) as no_dominant_cnt_sms_subs,
    count(case when dominant_payload_sms='SMS PACKAGE' then a.msisdn end) as dominant_cnt_sms_pckg_subs,
    count(case when dominant_payload_sms='SMS PAYU' then a.msisdn end) as dominant_cnt_sms_payu_subs,

    sum(rev_total_voice) as rev_total_voice,
    sum(rev_voice_pckg) as rev_voice_pckg,
    sum(rev_voice_payu) as rev_voice_payu,
    sum(total_dur_voice) as total_dur_voice,
    sum(dur_voice_pckg) as dur_voice_pckg,
    sum(dur_voice_payu) as dur_voice_payu,
    char8(arpu_segment_voice),
    char8(arpu_segment_voice_pckg),
    char8(arpu_segment_voice_payu),
    count(case when coalesce(rev_total_voice,0) > 0 then a.msisdn end) as voice_subs,
    count(case when coalesce(rev_voice_pckg,0) > 0 then a.msisdn end) as voice_pckg_subs,
    count(case when coalesce(rev_voice_payu,0) > 0 then a.msisdn end) as voice_payu_subs,
    count(case when dominant_rev_voice='NO DOMINANT' then a.msisdn end) as no_dominant_rev_voice_subs,
    count(case when dominant_rev_voice='VOICE PACKAGE' then a.msisdn end) as dominant_rev_voice_pckg_subs,
    count(case when dominant_rev_voice='VOICE PAYU' then a.msisdn end) as dominant_rev_voice_payu_subs,
    count(case when dominant_payload_voice='NO DOMINANT' then a.msisdn end) as no_dominant_dur_voice_subs,
    count(case when dominant_payload_voice='VOICE PACKAGE' then a.msisdn end) as dominant_dur_voice_pckg_subs,
    count(case when dominant_payload_voice='VOICE PAYU' then a.msisdn end) as dominant_dur_voice_payu_subs,
    a.cut_off_dt

    from
(
    select *
    from ${table-prefix}${hvc-schema}.${table-prefix}hvc_traffic_rolling where cut_off_dt = '${run-date=yyyy-MM-dd}'   --@Mate: '${period_dash}'
) a
left join
(
    select * from ${table-prefix}${hvc-schema}.${table-prefix}hvc_summary_cb where cut_off_dt = '${run-date=yyyy-MM-dd}'  --@Mate:  '${period_dash}'
        and brnd_nme in ('LOOP','KartuAS','simPATI')
) b
on a.msisdn = b.msisdn and a.cut_off_dt = b.cut_off_dt
group by a.cut_off_report, los_segment, arpu_segment, hvc_segment, brnd_nme, area_name, region_name, cluster_name,
  arpu_segment_broadband, arpu_segment_broadband_pckg, arpu_segment_broadband_payu,
  arpu_segment_sms, arpu_segment_sms_pckg, arpu_segment_sms_payu,
  arpu_segment_voice, arpu_segment_voice_pckg, arpu_segment_voice_payu, a.cut_off_dt;

insert into ${table-prefix}${hvc-schema}.${table-prefix}hvc_traffic_summ
select * from ${table-prefix}${hvc-schema}.${table-prefix}hvc_traffic_summ_temp;

drop table IF EXISTS ${table-prefix}${hvc-schema}.${table-prefix}hvc_traffic_summ_temp ;


