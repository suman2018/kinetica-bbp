-- insert overwrite table it.hvc_traffic_mom_summ partition (cut_off_dt='${period_now}')
delete from ${table-prefix}${hvc-schema}.${table-prefix}hvc_traffic_mom_summ where cut_off_dt = '${run-date=yyyy-MM-dd}';

insert into ${table-prefix}${hvc-schema}.${table-prefix}hvc_traffic_mom_summ
select
  -- cast(coalesce(a.cut_off_dt, b.cut_off_dt) as varchar) as cut_off_dt,  @Mate: new engine fixes this with upgrade
  '${run-date=yyyy-MM-dd}',

  coalesce(a.los_segment, b.los_segment) as los_segment,
  char8(coalesce(a.arpu_segment, b.arpu_segment)) as arpu_segment,
  char4(coalesce(a.hvc_segment, b.hvc_segment)) as hvc_segment,
  char8(coalesce(a.brand, b.brand)) as brand,
  char8(coalesce(a.area, b.area)) as area,
  coalesce(a.region, b.region) as region_name,
  coalesce(a.cluster_name, b.cluster_name) as cluster_name,
  sum(coalesce(a.rev_total_broadband, 0)) as rev_total_broadband,
  sum(coalesce(a.rev_broadband_pckg, 0)) as rev_broadband_pckg,
  sum(coalesce(a.rev_broadband_payu, 0)) as rev_broadband_payu,
  sum(coalesce(a.total_payload_broadband, 0)) as total_payload_broadband,
  sum(coalesce(a.payload_pckg, 0)) as payload_pckg,
  sum(coalesce(a.payload_payu, 0)) as payload_payu,
  sum(coalesce(a.rev_total_sms, 0)) as rev_total_sms,
  sum(coalesce(a.rev_sms_pckg, 0)) as rev_sms_pckg,
  sum(coalesce(a.rev_sms_payu, 0)) as rev_sms_payu,
  sum(coalesce(a.total_cnt_sms, 0)) as total_cnt_sms,
  sum(coalesce(a.cnt_sms_pckg, 0)) as cnt_sms_pckg,
  sum(coalesce(a.cnt_sms_payu, 0)) as cnt_sms_payu,
  sum(coalesce(a.rev_total_voice, 0)) as rev_total_voice,
  sum(coalesce(a.rev_voice_pckg, 0)) as rev_voice_pckg,
  sum(coalesce(a.rev_voice_payu, 0)) as rev_voice_payu,
  sum(coalesce(a.total_dur_voice, 0)) as total_dur_voice,
  sum(coalesce(a.dur_voice_pckg, 0)) as dur_voice_pckg,
  sum(coalesce(a.dur_voice_payu, 0)) as dur_voice_payu,
  sum(coalesce(b.rev_total_broadband, 0)) as rev_total_broadband_lm,
  sum(coalesce(b.rev_broadband_pckg, 0)) as rev_broadband_pckg_lm,
  sum(coalesce(b.rev_broadband_payu, 0)) as rev_broadband_payu_lm,
  sum(coalesce(b.total_payload_broadband, 0)) as total_payload_broadband_lm,
  sum(coalesce(b.payload_pckg, 0)) as payload_pckg_lm,
  sum(coalesce(b.payload_payu, 0)) as payload_payu_lm,
  sum(coalesce(b.rev_total_sms, 0)) as rev_total_sms_lm,
  sum(coalesce(b.rev_sms_pckg, 0)) as rev_sms_pckg_lm,
  sum(coalesce(b.rev_sms_payu, 0)) as rev_sms_payu_lm,
  sum(coalesce(b.total_cnt_sms, 0)) as total_cnt_sms_lm,
  sum(coalesce(b.cnt_sms_pckg, 0)) as cnt_sms_pckg_lm,
  sum(coalesce(b.cnt_sms_payu, 0)) as cnt_sms_payu_lm,
  sum(coalesce(b.rev_total_voice, 0)) as rev_total_voice_lm,
  sum(coalesce(b.rev_voice_pckg, 0)) as rev_voice_pckg_lm,
  sum(coalesce(b.rev_voice_payu, 0)) as rev_voice_payu_lm,
  sum(coalesce(b.total_dur_voice, 0)) as total_dur_voice_lm,
  sum(coalesce(b.dur_voice_pckg, 0)) as dur_voice_pckg_lm,
  sum(coalesce(b.dur_voice_payu, 0)) as dur_voice_payu_lm,
  '${run-date=yyyy-MM-dd}'
  from (select * from ${table-prefix}${hvc-schema}.${table-prefix}hvc_traffic_summ
  where cut_off_dt = '${run-date=yyyy-MM-dd}'  -- @Mate:  '${period_now}'
  ) a full outer join
  (select * from ${table-prefix}${hvc-schema}.${table-prefix}hvc_traffic_summ where cut_off_dt = ( DATE '${prev-month=yyyy-MM-dd}') 
  ) b
   on a.cut_off_dt=b.cut_off_dt and a.los_segment=b.los_segment and a.arpu_segment=b.arpu_segment and a.hvc_segment=b.hvc_segment
   and a.brand=b.brand and a.area=b.area and a.region=b.region and a.cluster_name=b.cluster_name
   group by  
   coalesce(a.los_segment, b.los_segment),
   char8(coalesce(a.arpu_segment, b.arpu_segment)) ,
  char4(coalesce(a.hvc_segment, b.hvc_segment)) ,
  char8(coalesce(a.brand, b.brand)) ,
  char8(coalesce(a.area, b.area)) ,
  coalesce(a.region, b.region) ,
  coalesce(a.cluster_name, b.cluster_name)
;

