-- @Mate: this might not be replicated as the underlying tables might not be either. Check!!!
delete from ${table-prefix}${hvc-schema}.${table-prefix}hvc_summary_homework where cut_off_dt = '${run-date=yyyy-MM-dd}';

create or replace temp table ${table-prefix}${hvc-schema}.${table-prefix}hvc_summary_homework_temp as 
select imsi ,area_name as work_area_name, region_name as work_region_name, 
cluster_name as work_cluster_name, ki_shard_key (imsi)
    from ${table-prefix}${hvc-schema}.${table-prefix}hvc_work_staypoint_vw aa
    left join
    (
        ---select * from abe_laccima_ref
        select area_nme as area_name, regional_chnnl as region_name,clstr_sls as cluster_name, kelurahan, kecamatan, kabupaten, propinsi 
        from ${table-prefix}${laccima-schema}.${table-prefix}laccima_ref_${run-date=yyyyMMdd}
        group by 1, 2, 3, 4, 5, 6, 7
    ) bb
    on aa.work1_kelurahan_name = bb.kelurahan and 
       aa.work1_kecamatan_name = bb.kecamatan and 
       aa.work1_kabupaten_name = bb.kabupaten and 
       aa.work1_province_name = bb.propinsi
;

-- insert overwrite table it.hvc_summary_homework partition (cut_off_dt='${period_dash}')
insert into ${table-prefix}${hvc-schema}.${table-prefix}hvc_summary_homework
--create or replace table hvc.hvc_summary_homework as
select
hvc_segment,
brnd_nme,
home_area_name,
home_region_name,
home_cluster_name,
work_area_name,
work_region_name,
work_cluster_name,
count(a.msisdn) as tot_subs,
cut_off_dt 
from
(
    select cut_off_dt,msisdn,brnd_nme,hvc_segment
    from ${table-prefix}${hvc-schema}.${table-prefix}hvc_summary_cb
    where brnd_nme in ('LOOP','KartuAS','simPATI','kartuHALO')
        and cut_off_dt = '${run-date=yyyy-MM-dd}' -- @Mate'${period_dash}'
) a
left join
(
    select imsi, area_name as home_area_name, region_name as home_region_name, cluster_name as home_cluster_name from
    ${table-prefix}${hvc-schema}.${table-prefix}hvc_home_staypoint_vw aa
    left join
    (
        --select * from abe_laccima_ref
        select area_nme as area_name, regional_chnnl as region_name,clstr_sls as cluster_name, kelurahan, kecamatan, kabupaten, propinsi 
        from ${table-prefix}${laccima-schema}.${table-prefix}laccima_ref_${run-date=yyyyMMdd}
        group by 1, 2, 3, 4, 5, 6, 7
    ) bb
    on aa.home1_kelurahan_name = bb.kelurahan and aa.home1_kecamatan_name = bb.kecamatan and aa.home1_kabupaten_name = bb.kabupaten and aa.home1_province_name = bb.propinsi
) d
on a.msisdn=d.imsi
left join ${table-prefix}${hvc-schema}.${table-prefix}hvc_summary_homework_temp e 
on a.msisdn = e.imsi
group by
    a.cut_off_dt,
    hvc_segment,
    brnd_nme,
    home_area_name,
    home_region_name,
    home_cluster_name,
    work_area_name,
    work_region_name,
    work_cluster_name;
    
