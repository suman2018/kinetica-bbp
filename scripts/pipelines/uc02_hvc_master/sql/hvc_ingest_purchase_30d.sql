/* KI_HINT_DICT_PROJECTION */
create or replace temp table ${table-prefix}${hvc-schema}.${table-prefix}hvc_purchase_rolling_daily_stg as ( 
select msisdn, (case when UPPER(L3_NAME) = 'MKIOSK' then 'MKIOS' else UPPER(ACTIVATION_CHANNEL_ID) end) as activation_channel_id, 
    sum(trx) as tot_trx, ki_shard_key (msisdn) 
from ${table-prefix}${revenue-schema}.${table-prefix}chg_rcg_staging_${run-date=yyyyMMdd} a
where service_filter = 'VAS' and L2_name not in ('SMS Non P2P', 'Voice Non P2P')
group by 1, 2
);

--- insertion into rolling 30d detail
delete from ${table-prefix}${hvc-schema}.${table-prefix}hvc_purchase_rolling_30d where cut_off_dt = '${run-date=yyyy-MM-dd}';

insert into ${table-prefix}${hvc-schema}.${table-prefix}hvc_purchase_rolling_30d
    select '${run-date=yyyy-MM-dd}' as cut_off_dt , a.*
    from ${table-prefix}${hvc-schema}.${table-prefix}hvc_purchase_rolling_daily_stg a;

create or replace view ${table-prefix}${hvc-schema}.${table-prefix}hvc_purchase_rolling_daily_stg_v as 
SELECT
'${run-date=yyyy-MM-dd}' cut_off_report, 
MSISDN,
MAX(CASE WHEN RANK_CHANNEL = 1 THEN F.ACT_CHNNL_NM ELSE '-' END) AS Channel_1st_id,
MAX(CASE WHEN RANK_CHANNEL = 2 THEN F.ACT_CHNNL_NM ELSE '-' END) AS Channel_2nd_ID,
MAX(CASE WHEN RANK_CHANNEL = 3 THEN F.ACT_CHNNL_NM ELSE '-' END) AS Channel_3rd_ID,
MAX(CASE WHEN RANK_CHANNEL = 4 THEN F.ACT_CHNNL_NM ELSE '-' END) AS Channel_4th_ID,
MAX(CASE WHEN RANK_CHANNEL = 5 THEN F.ACT_CHNNL_NM ELSE '-' END) AS Channel_5th_ID,
sum(CASE WHEN RANK_CHANNEL = 1 THEN F.TRX ELSE 0 END) AS trx_channel_1st,
sum(CASE WHEN RANK_CHANNEL = 2 THEN F.TRX ELSE 0 END) AS trx_channel_2nd,
sum(CASE WHEN RANK_CHANNEL = 3 THEN F.TRX ELSE 0 END) AS trx_channel_3rd,
sum(CASE WHEN RANK_CHANNEL = 4 THEN F.TRX ELSE 0 END) AS trx_channel_4th,
sum(CASE WHEN RANK_CHANNEL = 5 THEN F.TRX ELSE 0 END) AS trx_channel_5th,
sum(F.TRX) AS trx_all_channel,
DATE '${run-date=yyyy-MM-dd}'  as cut_off_dt
FROM
(
SELECT MSISDN, 
    (case when activation_channel_id = 'MKIOS' then 'MKIOS' else COALESCE(ACT_CHNNL_NME,'UNKNOWN') end) as act_chnnl_nm, 
    TRX,
    row_number() OVER (partition by MSISDN ORDER BY TRX DESC, 
        (case when activation_channel_id = 'MKIOS' then 'MKIOS' else activation_channel_id end) ASC) AS RANK_CHANNEL
FROM (
        select msisdn, activation_channel_id, sum(tot_trx) as TRX  
        from ${table-prefix}${hvc-schema}.${table-prefix}hvc_purchase_rolling_30d Z
        where cut_off_dt between  DATE '${run-date=yyyy-MM-dd}' - interval 29 day and  DATE '${run-date=yyyy-MM-dd}'
        group by msisdn, activation_channel_id
    ) a
    left join ${table-prefix}${reference-schema}.${table-prefix}channel_ref b 
        ON a.activation_channel_id = UPPER(b.act_chnnl_id)
) F group by msisdn;


---insert overwrite table it.hvc_purchase_rolling partition (cut_off_dt='${period_dash}')

 /* KI_HINT_DICT_PROJECTION */
create or replace temp table ${table-prefix}${hvc-schema}.${table-prefix}hvc_purchase_rolling_temp as 
select
    cut_off_report,
    msisdn, 
    channel_1st_id,
    channel_2nd_id,
    channel_3rd_id,
    channel_4th_id,
    channel_5th_id,
    trx_channel_1st,
    trx_channel_2nd,
    trx_channel_3rd,
    trx_channel_4th,
    trx_channel_5th,
    trx_all_channel,
    case
        when channel_1st_id <> 'UNKNOWN' and channel_1st_id <> '-' then channel_1st_id
        when channel_2nd_id <> 'UNKNOWN' and channel_2nd_id <> '-' then channel_2nd_id
        when channel_3rd_id <> 'UNKNOWN' and channel_3rd_id <> '-' then channel_3rd_id
        when channel_4th_id <> 'UNKNOWN' and channel_4th_id <> '-' then channel_4th_id
        when channel_5th_id <> 'UNKNOWN' and channel_5th_id <> '-' then channel_5th_id
        else 'ALL UNKNOWN'
    end as not_unk_channel,
    cut_off_dt   -- @Mate: hive has this as partition property of table. 
from ${table-prefix}${hvc-schema}.${table-prefix}hvc_purchase_rolling_daily_stg_v
;

delete from  ${table-prefix}${hvc-schema}.${table-prefix}hvc_purchase_rolling where cut_off_dt = '${run-date=yyyy-MM-dd}';

insert into ${table-prefix}${hvc-schema}.${table-prefix}hvc_purchase_rolling
    select * from  ${table-prefix}${hvc-schema}.${table-prefix}hvc_purchase_rolling_temp;
