delete from ${table-prefix}${hvc-schema}.${table-prefix}hvc_summary_demography where cut_off_dt = '${run-date=yyyy-MM-dd}';

insert into ${table-prefix}${hvc-schema}.${table-prefix}hvc_summary_demography
select
    hvc_segment,
    brnd_nme,
    age,
    gndr,
    mrtl_stts,
    first_rank_category,
    first_rank_apps,
    sum(vol_games_byte) as tot_vol_games_byte,
    sum(vol_socialnet_byte) as tot_vol_socialnet_byte,
    sum(vol_video_byte) as tot_vol_video_byte,
    sum(vol_music_byte) as tot_vol_music_byte,
    sum(vol_communications_byte) as tot_vol_communications_byte,
    sum(vol_sports_byte) as tot_vol_sports_byte,
    sum(vol_ecommerce_byte) as tot_vol_vol_ecommerce_byte,
    sum(vol_travel_food_byte) as tot_vol_travel_food_byte,
    sum(vol_news_lifestyle_byte) as tot_vol_news_lifestyle_byte,
    count(a.msisdn) as tot_subs,
    a.cut_off_dt 
from
(
    select cut_off_dt, msisdn, brnd_nme, hvc_segment
    from ${table-prefix}${hvc-schema}.${table-prefix}hvc_summary_cb where brnd_nme in ('LOOP','KartuAS','simPATI','kartuHALO')
        and cut_off_dt = '${run-date=yyyy-MM-dd}' -- @Mate:  '${period_dash}'
) a
left join
(
    select *
    from ${table-prefix}${hvc-schema}.${table-prefix}hvc_bcp_persona aa
    where aa.cut_off_dt = (select max(cut_off_dt) as dates from ${table-prefix}${hvc-schema}.${table-prefix}hvc_bcp_persona )
) b
on a.msisdn = b.msisdn
left join
(
    select msisdn, age, gndr, mrtl_stts from ${table-prefix}${hvc-schema}.${table-prefix}hvc_itdaml_demography
    where cut_off_report = (select max(cut_off_report) as dates from ${table-prefix}${hvc-schema}.${table-prefix}hvc_itdaml_demography )
) c
on a.msisdn = c.msisdn
group by
    a.cut_off_dt,
    hvc_segment,
    brnd_nme,
    age,
    gndr,
    mrtl_stts,
    first_rank_category,
    first_rank_apps;
