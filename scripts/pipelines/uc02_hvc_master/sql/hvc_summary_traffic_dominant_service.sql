delete from ${table-prefix}${hvc-schema}.${table-prefix}hvc_traffic_dominant_area_summ where cut_off_dt = DATE '${run-date=yyyy-MM-dd}';

insert into ${table-prefix}${hvc-schema}.${table-prefix}hvc_traffic_dominant_area_summ
select
    area,
    hvc_segment,
    c1,
    cut_off_dt
from
(
    select
        cut_off_dt,
        area,
        hvc_segment,

        case
            when rev_total_broadband = greatest then 'BROADBAND'
            when rev_total_sms = greatest then 'SMS'
            when rev_total_voice = greatest then 'VOICE'
        end as c1
    from
    (
        select
            cut_off_dt,
            area,
            hvc_segment,
            sum(rev_total_broadband) as rev_total_broadband,
            sum(rev_total_sms) as rev_total_sms,
            sum(rev_total_voice) as rev_total_voice,
            case
                when sum(rev_total_broadband) >= sum(rev_total_sms) and sum(rev_total_broadband) >= sum(rev_total_voice) then sum(rev_total_broadband)
                when sum(rev_total_sms) >= sum(rev_total_broadband) and sum(rev_total_sms) >= sum(rev_total_voice) then sum(rev_total_sms)
                when sum(rev_total_voice) >= sum(rev_total_broadband) and sum(rev_total_voice) >= sum(rev_total_sms) then sum(rev_total_voice)
            end as greatest

        from ${table-prefix}${hvc-schema}.${table-prefix}hvc_traffic_summ
        where area not in ('UNKNOWN') and cut_off_dt = '${run-date=yyyy-MM-dd}'   --@Mate: '${period_dash}'
        group by cut_off_dt, area, hvc_segment
    )

    union all
    
    select
        cut_off_dt,
        area,
        hvc_segment,
        case
            when rev_total_broadband = greatest then 'BROADBAND'
            when rev_total_sms = greatest then 'SMS'
            when rev_total_voice = greatest then 'VOICE'
        end as c1
    from
    (
        select
            cut_off_dt,
            area,
            'NULL' as hvc_segment,
            sum(rev_total_broadband) as rev_total_broadband,
            sum(rev_total_sms) as rev_total_sms,
            sum(rev_total_voice) as rev_total_voice,
            case
                when sum(rev_total_broadband) >= sum(rev_total_sms) and sum(rev_total_broadband) >= sum(rev_total_voice) then sum(rev_total_broadband)
                when sum(rev_total_sms) >= sum(rev_total_broadband) and sum(rev_total_sms) >= sum(rev_total_voice) then sum(rev_total_sms)
                when sum(rev_total_voice) >= sum(rev_total_broadband) and sum(rev_total_voice) >= sum(rev_total_sms) then sum(rev_total_voice)
            end as greatest

        from ${table-prefix}${hvc-schema}.${table-prefix}hvc_traffic_summ
        where area not in ('UNKNOWN') and cut_off_dt = '${run-date=yyyy-MM-dd}' -- @Mate:  '${period_dash}'
        group by cut_off_dt, area
    )
) a;

delete from ${table-prefix}${hvc-schema}.${table-prefix}hvc_traffic_dominant_region_summ where cut_off_dt = DATE '${run-date=yyyy-MM-dd}';

insert into ${table-prefix}${hvc-schema}.${table-prefix}hvc_traffic_dominant_region_summ
select
    area,
    region,
    hvc_segment,
    c1,
    cut_off_dt
from
(
    select
        cut_off_dt,
        area,
        region,
        hvc_segment,

        case
            when rev_total_broadband = greatest then 'BROADBAND'
            when rev_total_sms = greatest then 'SMS'
            when rev_total_voice = greatest then 'VOICE'
        end as c1

    from
    (
        select
            cut_off_dt,
            area,
            region,
            hvc_segment,

            sum(rev_total_broadband) as rev_total_broadband,
            sum(rev_total_sms) as rev_total_sms,
            sum(rev_total_voice) as rev_total_voice,
            case
                when sum(rev_total_broadband) >= sum(rev_total_sms) and sum(rev_total_broadband) >= sum(rev_total_voice) then sum(rev_total_broadband)
                when sum(rev_total_sms) >= sum(rev_total_broadband) and sum(rev_total_sms) >= sum(rev_total_voice) then sum(rev_total_sms)
                when sum(rev_total_voice) >= sum(rev_total_broadband) and sum(rev_total_voice) >= sum(rev_total_sms) then sum(rev_total_voice)
            end as greatest

        from ${table-prefix}${hvc-schema}.${table-prefix}hvc_traffic_summ
        where area not in ('UNKNOWN') and cut_off_dt = '${run-date=yyyy-MM-dd}'   --@Mate: '${period_dash}'
        group by cut_off_dt, area, region, hvc_segment
    )

    union all
select
        cut_off_dt,
        area,
        region,
        hvc_segment,

         case
            when rev_total_broadband = greatest then 'BROADBAND'
            when rev_total_sms = greatest then 'SMS'
            when rev_total_voice = greatest then 'VOICE'
        end as c1
    from
    (
        select
            cut_off_dt,
            area,
            region,
            'NULL' as hvc_segment,

            sum(rev_total_broadband) as rev_total_broadband,
            sum(rev_total_sms) as rev_total_sms,
            sum(rev_total_voice) as rev_total_voice,
            case
                when sum(rev_total_broadband) >= sum(rev_total_sms) and sum(rev_total_broadband) >= sum(rev_total_voice) then sum(rev_total_broadband)
                when sum(rev_total_sms) >= sum(rev_total_broadband) and sum(rev_total_sms) >= sum(rev_total_voice) then sum(rev_total_sms)
                when sum(rev_total_voice) >= sum(rev_total_broadband) and sum(rev_total_voice) >= sum(rev_total_sms) then sum(rev_total_voice)
            end as greatest

        from ${table-prefix}${hvc-schema}.${table-prefix}hvc_traffic_summ
        where area not in ('UNKNOWN') and cut_off_dt = '${run-date=yyyy-MM-dd}'   --@Mate: '${period_dash}'
        group by cut_off_dt, area, region
    )
)a;

delete from ${table-prefix}${hvc-schema}.${table-prefix}hvc_traffic_dominant_cluster_summ where cut_off_dt = DATE '${run-date=yyyy-MM-dd}';

insert into ${table-prefix}${hvc-schema}.${table-prefix}hvc_traffic_dominant_cluster_summ
select
    area,
    region,
    cluster,
    hvc_segment,
    c1,
    cut_off_dt
from
(
    select
        cut_off_dt,
        area,
        region,
        cluster_name as cluster,
        hvc_segment,

        case
            when rev_total_broadband = greatest then 'BROADBAND'
            when rev_total_sms = greatest then 'SMS'
            when rev_total_voice = greatest then 'VOICE'
        end as c1

    from
    (
        select
            cut_off_dt,
            area,
            region,
            hvc_segment,
            cluster_name,

            sum(rev_total_broadband) as rev_total_broadband,
            sum(rev_total_sms) as rev_total_sms,
            sum(rev_total_voice) as rev_total_voice,
            case
                when sum(rev_total_broadband) >= sum(rev_total_sms) and sum(rev_total_broadband) >= sum(rev_total_voice) then sum(rev_total_broadband)
                when sum(rev_total_sms) >= sum(rev_total_broadband) and sum(rev_total_sms) >= sum(rev_total_voice) then sum(rev_total_sms)
                when sum(rev_total_voice) >= sum(rev_total_broadband) and sum(rev_total_voice) >= sum(rev_total_sms) then sum(rev_total_voice)
            end as greatest

        from ${table-prefix}${hvc-schema}.${table-prefix}hvc_traffic_summ
        where area not in ('UNKNOWN') and cut_off_dt = '${run-date=yyyy-MM-dd}'   --@Mate: '${period_dash}'
        group by cut_off_dt, area, region, cluster_name, hvc_segment
    )

    union all

    select
        cut_off_dt,
        area,
        region,
        cluster_name as cluster,
        'NULL' as hvc_segment,

         case
            when rev_total_broadband = greatest then 'BROADBAND'
            when rev_total_sms = greatest then 'SMS'
            when rev_total_voice = greatest then 'VOICE'
        end as c1

    from
    (
        select
            cut_off_dt,
            area,
            region,
            'NULL' as hvc_segment,
            cluster_name,

            sum(rev_total_broadband) as rev_total_broadband,
            sum(rev_total_sms) as rev_total_sms,
            sum(rev_total_voice) as rev_total_voice,
            case
                when sum(rev_total_broadband) >= sum(rev_total_sms) and sum(rev_total_broadband) >= sum(rev_total_voice) then sum(rev_total_broadband)
                when sum(rev_total_sms) >= sum(rev_total_broadband) and sum(rev_total_sms) >= sum(rev_total_voice) then sum(rev_total_sms)
                when sum(rev_total_voice) >= sum(rev_total_broadband) and sum(rev_total_voice) >= sum(rev_total_sms) then sum(rev_total_voice)
            end as greatest

        from ${table-prefix}${hvc-schema}.${table-prefix}hvc_traffic_summ
        where area not in ('UNKNOWN') and cut_off_dt = '${run-date=yyyy-MM-dd}'  --@Mate: '${period_dash}'
        group by cut_off_dt, area, region, cluster_name
    )
)a;
