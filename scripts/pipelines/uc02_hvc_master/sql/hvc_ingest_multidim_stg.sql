-- Objective: running hvc multidim ingestion

/* KI_HINT_DICT_PROJECTION */
create or replace table ${table-prefix}${hvc-schema}.${table-prefix}hvc_multidim_${run-date=yyyyMMdd} as 
select 
	trx_date
	,msisdn
	,nik_id
	,ss_nmbr
	,brand
	,reg_stts
    ,reg_chnl
	,area_sales
	,region_lacci
	,region_sales
	,cluster
	,kabupaten
	,kecamatan
	,reg_dt
	,nik_dob
	,activation_date
	,rev_mtd 
	,pre2post_flag
	,char16(char32(msisdn)) as msisdn_char
	,ki_shard_key(msisdn)
from ${table-prefix}${hvc-schema}.${table-prefix}hvc_multidim_stg;


alter table ${table-prefix}${hvc-schema}.${table-prefix}hvc_multidim_${run-date=yyyyMMdd} modify brand varchar(32,dict);
alter table ${table-prefix}${hvc-schema}.${table-prefix}hvc_multidim_${run-date=yyyyMMdd} modify area_sales varchar(16,dict);
alter table ${table-prefix}${hvc-schema}.${table-prefix}hvc_multidim_${run-date=yyyyMMdd} modify region_lacci varchar(64,dict);
alter table ${table-prefix}${hvc-schema}.${table-prefix}hvc_multidim_${run-date=yyyyMMdd} modify region_sales varchar(64,dict);
alter table ${table-prefix}${hvc-schema}.${table-prefix}hvc_multidim_${run-date=yyyyMMdd} modify cluster varchar(64,dict);
alter table ${table-prefix}${hvc-schema}.${table-prefix}hvc_multidim_${run-date=yyyyMMdd} modify kabupaten varchar(64,dict);
alter table ${table-prefix}${hvc-schema}.${table-prefix}hvc_multidim_${run-date=yyyyMMdd} modify kecamatan varchar(64,dict);
alter table ${table-prefix}${hvc-schema}.${table-prefix}hvc_multidim_${run-date=yyyyMMdd} modify reg_chnl varchar(64,dict);
alter table ${table-prefix}${hvc-schema}.${table-prefix}hvc_multidim_${run-date=yyyyMMdd} modify pre2post_flag varchar(1,dict);

drop table if exists ${table-prefix}${hvc-schema}.${table-prefix}hvc_multidim_stg;

---DROP TABLE IF EXISTS ${table-prefix}${hvc-schema}.${table-prefix}hvc_multidim_${retention-date=yyyyMMdd};


