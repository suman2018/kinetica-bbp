CREATE OR REPLACE MATERIALIZED VIEW ${table-prefix}${hvc-schema}_out.${table-prefix}hvc_cb_rolling_v
REFRESH EVERY 1 DAY AS (
select *
from ${table-prefix}${hvc-schema}.${table-prefix}hvc_cb_rolling
);

CREATE OR REPLACE MATERIALIZED VIEW ${table-prefix}${hvc-schema}_out.${table-prefix}hvc_rgb_rolling_v
REFRESH EVERY 1 DAY AS (
select *
from ${table-prefix}${hvc-schema}.${table-prefix}hvc_rgb_rolling
);

CREATE OR REPLACE MATERIALIZED VIEW ${table-prefix}${hvc-schema}_out.${table-prefix}hvc_rgb_rolling_30d_v
REFRESH EVERY 1 DAY AS (
select *
from ${table-prefix}${hvc-schema}.${table-prefix}hvc_rgb_rolling_30d
);

CREATE OR REPLACE MATERIALIZED VIEW ${table-prefix}${hvc-schema}_out.${table-prefix}hvc_rech_rolling_v
REFRESH EVERY 1 DAY AS (
select *
from ${table-prefix}${hvc-schema}.${table-prefix}hvc_rech_rolling
);

CREATE OR REPLACE MATERIALIZED VIEW ${table-prefix}${hvc-schema}_out.${table-prefix}hvc_rech_rolling_30d_v
REFRESH EVERY 1 DAY AS (
select *
from ${table-prefix}${hvc-schema}.${table-prefix}hvc_rech_rolling_30d
);

CREATE OR REPLACE MATERIALIZED VIEW ${table-prefix}${hvc-schema}_out.${table-prefix}hvc_traffic_rolling_v
REFRESH EVERY 1 DAY AS (
select *
from ${table-prefix}${hvc-schema}.${table-prefix}hvc_traffic_rolling
);

CREATE OR REPLACE MATERIALIZED VIEW ${table-prefix}${hvc-schema}_out.${table-prefix}hvc_traffic_rolling_30d_v
REFRESH EVERY 1 DAY AS (
select *
from ${table-prefix}${hvc-schema}.${table-prefix}hvc_traffic_rolling_30d
);



CREATE OR REPLACE MATERIALIZED VIEW ${table-prefix}${hvc-schema}_out.${table-prefix}hvc_purchase_rolling_v
REFRESH EVERY 1 DAY AS (
select *
from ${table-prefix}${hvc-schema}.${table-prefix}hvc_purchase_rolling
);


CREATE OR REPLACE MATERIALIZED VIEW ${table-prefix}${hvc-schema}_out.${table-prefix}hvc_purchase_rolling_30d_v
REFRESH EVERY 1 DAY AS (
select *
from ${table-prefix}${hvc-schema}.${table-prefix}hvc_purchase_rolling_30d
);


CREATE OR REPLACE MATERIALIZED VIEW ${table-prefix}${hvc-schema}_out.${table-prefix}hvc_cse_mgt_rolling_v
REFRESH EVERY 1 DAY AS (
select *
from ${table-prefix}${hvc-schema}.${table-prefix}hvc_cse_mgt_rolling
);


CREATE OR REPLACE MATERIALIZED VIEW ${table-prefix}${hvc-schema}_out.${table-prefix}hvc_summary_cb_v
REFRESH EVERY 1 DAY AS (
select *
from ${table-prefix}${hvc-schema}.${table-prefix}hvc_summary_cb
);


CREATE OR REPLACE MATERIALIZED VIEW ${table-prefix}${hvc-schema}_out.${table-prefix}hvc_summary_rgb_2_v
REFRESH EVERY 1 DAY AS (
select *
from ${table-prefix}${hvc-schema}.${table-prefix}hvc_summary_rgb_2
);


CREATE OR REPLACE MATERIALIZED VIEW ${table-prefix}${hvc-schema}_out.${table-prefix}hvc_summary_mainpage_2_v
REFRESH EVERY 1 DAY AS (
select *
from ${table-prefix}${hvc-schema}.${table-prefix}hvc_summary_mainpage_2
);


CREATE OR REPLACE MATERIALIZED VIEW ${table-prefix}${hvc-schema}_out.${table-prefix}hvc_summary_recharge_det_v
REFRESH EVERY 1 DAY AS (
select *
from ${table-prefix}${hvc-schema}.${table-prefix}hvc_summary_recharge_det
);


CREATE OR REPLACE MATERIALIZED VIEW ${table-prefix}${hvc-schema}_out.${table-prefix}hvc_summary_recharge_summ_v
REFRESH EVERY 1 DAY AS (
select *
from ${table-prefix}${hvc-schema}.${table-prefix}hvc_summary_recharge_summ
);


CREATE OR REPLACE MATERIALIZED VIEW ${table-prefix}${hvc-schema}_out.${table-prefix}hvc_traffic_summ_v
REFRESH EVERY 1 DAY AS (
select *
from ${table-prefix}${hvc-schema}.${table-prefix}hvc_traffic_summ
);


CREATE OR REPLACE MATERIALIZED VIEW ${table-prefix}${hvc-schema}_out.${table-prefix}hvc_purchase_summ_v
REFRESH EVERY 1 DAY AS (
select *
from ${table-prefix}${hvc-schema}.${table-prefix}hvc_purchase_summ
);


CREATE OR REPLACE MATERIALIZED VIEW ${table-prefix}${hvc-schema}_out.${table-prefix}hvc_cse_mgt_summ_v
REFRESH EVERY 1 DAY AS (
select *
from ${table-prefix}${hvc-schema}.${table-prefix}hvc_cse_mgt_summ
);


CREATE OR REPLACE MATERIALIZED VIEW ${table-prefix}${hvc-schema}_out.${table-prefix}hvc_segment_movement_summ_2_v
REFRESH EVERY 1 DAY AS (
select *
from ${table-prefix}${hvc-schema}.${table-prefix}hvc_segment_movement_summ_2
);


CREATE OR REPLACE MATERIALIZED VIEW ${table-prefix}${hvc-schema}_out.${table-prefix}hvc_segment_movement_summ_2_bl_v
REFRESH EVERY 1 DAY AS (
select *
from ${table-prefix}${hvc-schema}.${table-prefix}hvc_segment_movement_summ_2_bl
);


CREATE OR REPLACE MATERIALIZED VIEW ${table-prefix}${hvc-schema}_out.${table-prefix}hvc_traffic_mom_summ_v
REFRESH EVERY 1 DAY AS (
select *
from ${table-prefix}${hvc-schema}.${table-prefix}hvc_traffic_mom_summ
);


CREATE OR REPLACE MATERIALIZED VIEW ${table-prefix}${hvc-schema}_out.${table-prefix}hvc_traffic_dominant_area_summ_v
REFRESH EVERY 1 DAY AS (
select *
from ${table-prefix}${hvc-schema}.${table-prefix}hvc_traffic_dominant_area_summ
);


CREATE OR REPLACE MATERIALIZED VIEW ${table-prefix}${hvc-schema}_out.${table-prefix}hvc_traffic_dominant_region_summ_v
REFRESH EVERY 1 DAY AS (
select *
from ${table-prefix}${hvc-schema}.${table-prefix}hvc_traffic_dominant_region_summ
);


CREATE OR REPLACE MATERIALIZED VIEW ${table-prefix}${hvc-schema}_out.${table-prefix}hvc_traffic_dominant_cluster_summ_v
REFRESH EVERY 1 DAY AS (
select *
from ${table-prefix}${hvc-schema}.${table-prefix}hvc_traffic_dominant_cluster_summ
);


CREATE OR REPLACE MATERIALIZED VIEW ${table-prefix}${hvc-schema}_out.${table-prefix}hvc_summary_homework_v
REFRESH EVERY 1 DAY AS (
select *
from ${table-prefix}${hvc-schema}.${table-prefix}hvc_summary_homework
);


CREATE OR REPLACE MATERIALIZED VIEW ${table-prefix}${hvc-schema}_out.${table-prefix}hvc_summary_demography_v
REFRESH EVERY 1 DAY AS (
select *
from ${table-prefix}${hvc-schema}.${table-prefix}hvc_summary_demography
);

alter schema ${table-prefix}${hvc-schema}_out set protected true;
