delete from ${table-prefix}${hvc-schema}.${table-prefix}hvc_summary_cb where cut_off_dt = '${run-date=yyyy-MM-dd}';

create or replace temp table ${table-prefix}${hvc-schema}.${table-prefix}hvc_summary_cb_temp as 
select
    a.cut_off_report,
    a.msisdn,
    a.brnd_nme,    -- @MATE: this did not have an alias prefix so I guessed a
    a.los,
    a.los_segment,
    case when trim(a.area_name)='' then 'UNKNOWN' else a.area_name end as area_name,
    case when trim(a.region_name)='' then 'UNKNOWN' else a.region_name end as region_name,
    case when trim(a.cluster_name)='' then 'UNKNOWN' else a.cluster_name end as cluster_name,
    a.pre2post_flg,   -- @MATE: this did not have an alias prefix so I guessed a
    a.flg_youth,      -- @MATE: this did not have an alias prefix so I guessed a
    coalesce(b.arpu_segment, '0-50K') as arpu_segment,
    case
        when a.los <= 90 and coalesce(b.rev_total, 0) <= 50000 then 'LAC'
        when a.los <= 90 and coalesce(b.rev_total, 0) < 70000 then 'MAC'
        when a.los <= 90 and coalesce(b.rev_total, 0) >= 70000 then 'HAC'
        when (a.los >90 and a.los < 360) and coalesce(b.rev_total, 0) <= 50000 then 'LPC'
        when (a.los >90 and a.los < 360) and coalesce(b.rev_total, 0) < 70000 then 'MPC'
        when (a.los >90 and a.los < 360) and coalesce(b.rev_total, 0) >= 70000 then 'HPC'
        when a.los >= 360 and coalesce(b.rev_total, 0) <= 50000 then 'LVC'
        when a.los >= 360 and coalesce(b.rev_total, 0) < 70000 then 'MVC'
        when a.los >= 360 and coalesce(b.rev_total, 0) >= 70000 then 'HVC'
    end as HVC_segment,
    '${run-date=yyyy-MM-dd}'
from
(
    select * from
    (
        select a.*,
        row_number() over(partition by msisdn order by los desc,brnd_nme desc) rn
        from ${table-prefix}${hvc-schema}.${table-prefix}hvc_cb_rolling a
        where cut_off_dt = '${run-date=yyyy-MM-dd}'   --'${period_dash}'
    ) b
    where rn=1
) a
left join
(
    select *
    from ${table-prefix}${hvc-schema}.${table-prefix}hvc_rgb_rolling
    where cut_off_dt = '${run-date=yyyy-MM-dd}'   --'${period_dash}'
) b
    on a.msisdn = b.msisdn and a.cut_off_dt = b.cut_off_dt;
    
insert into ${table-prefix}${hvc-schema}.${table-prefix}hvc_summary_cb
select * from ${table-prefix}${hvc-schema}.${table-prefix}hvc_summary_cb_temp;

drop table ${table-prefix}${hvc-schema}.${table-prefix}hvc_summary_cb_temp;


