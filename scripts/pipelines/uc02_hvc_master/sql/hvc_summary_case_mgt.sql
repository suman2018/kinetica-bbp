delete from ${table-prefix}${hvc-schema}.${table-prefix}hvc_cse_mgt_summ where cut_off_dt = DATE '${run-date=yyyy-MM-dd}';

insert into ${table-prefix}${hvc-schema}.${table-prefix}hvc_cse_mgt_summ
select
    cast(a.cut_off_dt as varchar) as cut_off_report,
    coalesce(los_segment,'<= 3 MONTHS') as los_segment,
    coalesce(arpu_segment,'0-50K') as arpu_segment,
    coalesce(hvc_segment,'LAC') as hvc_segment,
    coalesce(brnd_nme, 'UNKNOWN') as brand,
    coalesce(area_name, 'UNKNOWN') as area_name,
    coalesce(region_name, 'UNKNOWN') as region_name,
    coalesce(cluster_name, 'UNKNOWN') as cluster_name,
    case when trim(cstmr_typ)='' then 'UNKNOWN' else cstmr_typ end as cstmr_typ,
    case_typ1,
    case_typ2,
    case_typ3,
    case_sttus,
    case_sttus_final,
    count(distinct a.msisdn) as subs,
    count(case_id) as cnt_case_id,
    speed_of_service,
    sum(coalesce(duration,0)) as duration,
    duration_category,
    a.cut_off_dt
from
(
    select * from
    ${table-prefix}${hvc-schema}.${table-prefix}hvc_cse_mgt_rolling
    where cut_off_dt = '${run-date=yyyy-MM-dd}'   -- @Mate: '${period_dash}' 
) a
left join
(
    select * from ${table-prefix}${hvc-schema}.${table-prefix}hvc_summary_cb
    where cut_off_dt = '${run-date=yyyy-MM-dd}'  --@Mate:'${period_dash}'
        and brnd_nme in ('LOOP','KartuAS','simPATI')
) b
on a.msisdn = b.msisdn
group by cstmr_typ, case_typ1, case_typ2, case_typ3, case_sttus, case_sttus_final, speed_of_service, duration_category,
 los_segment, arpu_segment, hvc_segment, area_name, region_name,cluster_name, brnd_nme, a.cut_off_dt;
