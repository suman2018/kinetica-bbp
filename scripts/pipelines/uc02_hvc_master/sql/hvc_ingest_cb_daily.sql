-- create staging for cb_pre and cb_post staging
create or replace temp table ${table-prefix}${hvc-schema}.${table-prefix}hvc_cb_rolling_stg1 as (
select msisdn, brnd_nme, los,  coalesce(cluster, 'UNKNOWN') as cluster, ki_shard_key(a.msisdn) from (
        select msisdn, (case BRAND when 'kartuHALO' then 'simPATI' when 'UNKNOWN' then 'simPATI' else BRAND end) as brnd_nme, 
            datediff(date '${run-date=yyyy-MM-dd}', ACTIVATION_DATE) as los, cluster 
        from ${table-prefix}${scn-schema}.${table-prefix}cb_pre_staging_${run-date=yyyyMMdd}
        ---group by msisdn, brand, activation_date, cluster
        union ALL
        select msisdn, (case when BRAND = 'KartuHALO' then 'kartuHALO' else BRAND end)  as brnd_nme, 
            datediff(date '${run-date=yyyy-MM-dd}', ACTIVATION_DATE) as los, cluster 
        from ${table-prefix}${scn-schema}.${table-prefix}cb_post_staging_${run-date=yyyyMMdd}
        ---group by msisdn, brand, activation_date, cluster
        ) a 
); 


create or replace temp table ${table-prefix}${hvc-schema}.${table-prefix}hvc_cb_rolling_stg2  as (
select 
    '${run-date=yyyy-MM-dd}' as cut_off_report,
    a.msisdn, brnd_nme, los, cluster, 
    coalesce(c.pre2post_flg, 'N') as pre2post_flg, 
    (case when b.msisdn is not null and a.msisdn = b.msisdn then 'Y' else 'N' end) flg_youth, 
    DATE '${run-date=yyyy-MM-dd}' as cut_off_dt,
    ki_shard_key(a.msisdn) 
    from ${table-prefix}${hvc-schema}.${table-prefix}hvc_cb_rolling_stg1 a 
    left outer join 
        (select msisdn, max(month_id) as month_id  from ${table-prefix}${hvc-schema}.${table-prefix}hvc_community_cb_dou 
            where month_id < '${run-date=yyyyMM}'  group by msisdn) b on a.msisdn = b.msisdn
    left outer join
        (select long(msisdn) as msisdn, max(pre2post_flag) pre2post_flg 
            from ${table-prefix}${hvc-schema}.${table-prefix}hvc_multidim_${run-date=yyyyMMdd}
            group by 1) c on a.msisdn = c.msisdn
); 

--- ${table-prefix}${niknokk-schema}.${table-prefix}

--- create or replace temp table ${hvc-schema}.${table-prefix}hvc_cb_rolling as
create or replace temp table ${table-prefix}${hvc-schema}.${table-prefix}hvc_cb_rolling_temp as    
    select
        cut_off_report,
        msisdn,
        brnd_nme,
        coalesce(los, 0) as los,
        case
            WHEN COALESCE(LOS, 0) <= 90 THEN '<= 3 MONTHS'
             WHEN COALESCE(LOS, 0) <= 180 THEN '3 MONTHS - 6 MONTHS'
             WHEN COALESCE(LOS, 0) <= 360 THEN '6 MONTHS - 1 YEAR'
             WHEN COALESCE(LOS, 0) <= 1080 THEN '1 YEAR - 3 YEARS'
             WHEN COALESCE(LOS, 0) <= 2520 THEN '3 YEARS - 7 YEARS'
             WHEN COALESCE(LOS, 0) > 2520 THEN '> 7 YEARS'
             ELSE 'UNKNOWN'
        end as los_segment,
        coalesce(b.area_name, 'UNKNOWN') as area_name,
        coalesce(b.region_name, 'UNKNOWN') as region_name,
        coalesce(b.cluster_name, 'UNKNOWN') as cluster_name,
        pre2post_flg,
        flg_youth,
        --- '${run-date=yyyy-MM-dd}'
        cut_off_dt   -- @Mate: hive has this as partition property of table. 
from ${table-prefix}${hvc-schema}.${table-prefix}hvc_cb_rolling_stg2 a 
left outer join (select area_nme as area_name, regional_chnnl region_name, clstr_sls as cluster_name 
from ${table-prefix}${laccima-schema}.${table-prefix}laccima_ref_${run-date=yyyyMMdd} group by area_nme, regional_chnnl, clstr_sls) b
        on a.cluster=b.CLUSTER_NAME;

-- @Mate: validate that this is drop/recreate and not append
delete from ${table-prefix}${hvc-schema}.${table-prefix}hvc_cb_rolling
where cut_off_dt = '${run-date=yyyy-MM-dd}';

insert into ${table-prefix}${hvc-schema}.${table-prefix}hvc_cb_rolling 
    select * from ${table-prefix}${hvc-schema}.${table-prefix}hvc_cb_rolling_temp;

drop table if exists ${table-prefix}${hvc-schema}.${table-prefix}hvc_cb_rolling_stg1 ;
drop table if exists ${table-prefix}${hvc-schema}.${table-prefix}hvc_cb_rolling_stg2 ;
drop table if exists ${table-prefix}${hvc-schema}.${table-prefix}hvc_cb_rolling_temp ;



