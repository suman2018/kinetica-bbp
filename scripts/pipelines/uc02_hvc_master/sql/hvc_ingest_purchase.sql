---insert overwrite table it.hvc_purchase_rolling partition (cut_off_dt='${period_dash}')

delete from  ${table-prefix}${hvc-schema}.${table-prefix}hvc_purchase_rolling where cut_off_dt = '${run-date=yyyy-MM-dd}';

create or replace temp table ${table-prefix}${hvc-schema}.${table-prefix}hvc_purchase_rolling_temp as 
select
    cut_off_report,
    msisdn, channel_1st_id,
    channel_2nd_id,
    channel_3rd_id,
    channel_4th_id,
    channel_5th_id,
    trx_channel_1st,
    trx_channel_2nd,
    trx_channel_3rd,
    trx_channel_4th,
    trx_channel_5th,
    trx_all_channel,
    case
        when channel_1st_id <> 'UNKNOWN' then channel_1st_id
        when channel_2nd_id <> 'UNKNOWN' then channel_2nd_id
        when channel_3rd_id <> 'UNKNOWN' then channel_3rd_id
        when channel_4th_id <> 'UNKNOWN' then channel_4th_id
        when channel_5th_id <> 'UNKNOWN' then channel_5th_id
        else 'ALL UNKNOWN'
    end as not_unk_channel,
    date(char16(cut_off_report)) as cut_off_dt   -- @Mate: hive has this as partition property of table. 
from ${table-prefix}${hvc-schema}.${table-prefix}hvc_purchase_rolling_stg
;

insert into ${table-prefix}${hvc-schema}.${table-prefix}hvc_purchase_rolling
    select * from  ${table-prefix}${hvc-schema}.${table-prefix}hvc_purchase_rolling_temp;

drop table ${table-prefix}${hvc-schema}.${table-prefix}hvc_purchase_rolling_temp;