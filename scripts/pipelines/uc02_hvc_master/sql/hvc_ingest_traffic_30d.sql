/* KI_HINT_DICT_PROJECTION */
create or replace temp table ${table-prefix}${hvc-schema}.${table-prefix}hvc_traffic_rolling_daily_stg as (
select msisdn, 
sum(case when trim(l1_name) = 'Broadband' and trim(l2_name) <> 'APN/PAYU' then rev else 0 end) as rev_broadband_pkg,
sum(case when trim(l1_name) = 'Broadband' and trim(l2_name)  = 'APN/PAYU' then rev else 0 end) as rev_broadbrand_payu,
sum( coalesce(vol_free,0) )  as payload_pkg,
sum( coalesce(vol_rnd ,0) - coalesce(vol_free,0) ) as payload_payu,
sum(case when trim(l1_name) = 'SMS P2P' and trim(l2_name)  = 'SMS Package' then rev else 0 end) as rev_sms_pkg,
sum(case when trim(l1_name) = 'SMS P2P' and trim(l2_name) <> 'SMS Package' then rev else 0 end) as rev_sms_payu,
sum(case when trim(l1_name) = 'SMS P2P' then (trx-trx_c) else 0 end ) as cnt_sms_pkg,
sum(case when trim(l1_name) = 'SMS P2P'   and trim(l2_name) <> 'SMS Package' then trx_c else 0 end) as cnt_sms_payu,
sum(case when trim(l1_name) = 'Voice P2P' and trim(l2_name)  = 'Voice Package' then rev else 0 end) as rev_voice_pkg,
sum(case when trim(l1_name) = 'Voice P2P' and trim(l2_name) <> 'Voice Package' then rev else 0 end) as rev_voice_payu,
sum( coalesce(dur_free,0) ) as dur_voice_pkg,
sum( coalesce(dur_c,0) )  as dur_voice_payu,
ki_shard_key (msisdn) 
from (
    select msisdn, l1_name, l2_name, rev, vol_free, vol_rnd, trx, trx_c, dur_free, dur_c  
        from ${table-prefix}${revenue-schema}.${table-prefix}chg_rcg_staging_${run-date=yyyyMMdd}
)
group by msisdn
);


--- insertion into rolling 30d detail
delete from ${table-prefix}${hvc-schema}.${table-prefix}hvc_traffic_rolling_30d where cut_off_dt = '${run-date=yyyy-MM-dd}';

insert into ${table-prefix}${hvc-schema}.${table-prefix}hvc_traffic_rolling_30d
    select '${run-date=yyyy-MM-dd}' as cut_off_dt , a.*
    from ${table-prefix}${hvc-schema}.${table-prefix}hvc_traffic_rolling_daily_stg a;

/* KI_HINT_DICT_PROJECTION */
create or replace temp table ${table-prefix}${hvc-schema}.${table-prefix}hvc_traffic_rolling_30d_stg as 
select 
'${run-date=yyyy-MM-dd}' as cut_off_report, 
msisdn, 
sum(rev_broadband_pckg) as rev_broadband_pckg,
sum(rev_broadband_payu) as rev_broadband_payu,
sum(payload_pckg) as payload_pckg,
sum(payload_payu) as payload_payu,
sum(rev_sms_pckg) as rev_sms_pckg,
sum(rev_sms_payu) as rev_sms_payu,
sum(cnt_sms_pckg) as cnt_sms_pckg,
sum(cnt_sms_payu) as cnt_sms_payu,
sum(rev_voice_pckg) as rev_voice_pckg,
sum(rev_voice_payu) as rev_voice_payu,
sum(dur_voice_pckg) as dur_voice_pckg,
sum(dur_voice_payu) as dur_voice_payu,
DATE '${run-date=yyyy-MM-dd}' as cut_off_dt,
ki_shard_key(msisdn)
from ${table-prefix}${hvc-schema}.${table-prefix}hvc_traffic_rolling_30d
    where cut_off_dt between DATE '${run-date=yyyy-MM-dd}' - interval 29 day and  DATE '${run-date=yyyy-MM-dd}'
group by msisdn
;



---- create or replace temp table ${hvc-schema}.${table-prefix}hvc_traffic_rolling as 
/* KI_HINT_DICT_PROJECTION */
create or replace temp table ${table-prefix}${hvc-schema}.${table-prefix}hvc_traffic_rolling_temp as 
select     
    cut_off_report,
    msisdn,
    (rev_broadband_pckg+rev_broadband_payu) as rev_total_broadband,
    rev_broadband_pckg as rev_broadband_pckg,
    rev_broadband_payu as rev_broadband_payu,
    (payload_pckg + payload_payu) as total_payload_broadband,
    payload_pckg as payload_pckg,
    payload_payu as payload_payu,
    case
        WHEN (REV_BROADBAND_PCKG + REV_BROADBAND_PAYU)< 50000 THEN '0-50K'
        WHEN (REV_BROADBAND_PCKG + REV_BROADBAND_PAYU)< 65000 THEN '50-65K'
        WHEN (REV_BROADBAND_PCKG + REV_BROADBAND_PAYU)< 70000 THEN '65-70K'
        WHEN (REV_BROADBAND_PCKG + REV_BROADBAND_PAYU)< 120000 THEN '70-120K'
        WHEN (REV_BROADBAND_PCKG + REV_BROADBAND_PAYU)< 150000 THEN '120-150K'
        WHEN (REV_BROADBAND_PCKG + REV_BROADBAND_PAYU)< 180000 THEN '150-180K'
        WHEN (REV_BROADBAND_PCKG + REV_BROADBAND_PAYU)< 210000 THEN '180-210K'
        WHEN (REV_BROADBAND_PCKG + REV_BROADBAND_PAYU)< 240000 THEN '210-240K'
        WHEN (REV_BROADBAND_PCKG + REV_BROADBAND_PAYU)>= 240000 THEN '>240K'
    end as arpu_broadband_segment,
    case
        WHEN REV_BROADBAND_PCKG < 50000 THEN '0-50K'
        WHEN REV_BROADBAND_PCKG < 65000 THEN '50-65K'
        WHEN REV_BROADBAND_PCKG < 70000 THEN '65-70K'
        WHEN REV_BROADBAND_PCKG < 120000 THEN '70-120K'
        WHEN REV_BROADBAND_PCKG < 150000 THEN '120-150K'
        WHEN REV_BROADBAND_PCKG < 180000 THEN '150-180K'
        WHEN REV_BROADBAND_PCKG < 210000 THEN '180-210K'
        WHEN REV_BROADBAND_PCKG < 240000 THEN '210-240K'
        WHEN REV_BROADBAND_PCKG >= 240000 THEN '>240K'
    end as arpu_broadband_pckg_segment,
    case
        WHEN REV_BROADBAND_PAYU < 50000 THEN '0-50K'
        WHEN REV_BROADBAND_PAYU < 65000 THEN '50-65K'
        WHEN REV_BROADBAND_PAYU < 70000 THEN '65-70K'
        WHEN REV_BROADBAND_PAYU < 120000 THEN '70-120K'
        WHEN REV_BROADBAND_PAYU < 150000 THEN '120-150K'
        WHEN REV_BROADBAND_PAYU < 180000 THEN '150-180K'
        WHEN REV_BROADBAND_PAYU < 210000 THEN '180-210K'
        WHEN REV_BROADBAND_PAYU < 240000 THEN '210-240K'
        WHEN REV_BROADBAND_PAYU >= 240000 THEN '>240K'
    end as arpu_broadband_payu_segment,
    case
        when rev_broadband_payu = rev_broadband_pckg then 'NO DOMINANT'
        when rev_broadband_payu > rev_broadband_pckg then 'BROADBAND PAYU'
        else 'BROADBAND PACKAGE'
    end as dominant_rev_broadband,
    case
        when payload_payu = payload_pckg then 'NO DOMINANT'
        when payload_payu > payload_pckg then 'BROADBAND PAYU'
        else 'BROADBAND PACKAGE'
    end as dominant_payload_broadband,
    (rev_sms_pckg+rev_sms_payu) as rev_total_sms,
    rev_sms_pckg as rev_sms_pckg,
    rev_sms_payu as rev_sms_payu,
    (cnt_sms_pckg+cnt_sms_payu) as total_cnt_sms,
    cnt_sms_pckg as cnt_sms_pckg,
    cnt_sms_payu as cnt_sms_payu,
    case
        WHEN (REV_SMS_PCKG + REV_SMS_PAYU)< 50000 THEN '0-50K'
        WHEN (REV_SMS_PCKG + REV_SMS_PAYU)< 65000 THEN '50-65K'
        WHEN (REV_SMS_PCKG + REV_SMS_PAYU)< 70000 THEN '65-70K'
        WHEN (REV_SMS_PCKG + REV_SMS_PAYU)< 120000 THEN '70-120K'
        WHEN (REV_SMS_PCKG + REV_SMS_PAYU)< 150000 THEN '120-150K'
        WHEN (REV_SMS_PCKG + REV_SMS_PAYU)< 180000 THEN '150-180K'
        WHEN (REV_SMS_PCKG + REV_SMS_PAYU)< 210000 THEN '180-210K'
        WHEN (REV_SMS_PCKG + REV_SMS_PAYU)< 240000 THEN '210-240K'
        WHEN (REV_SMS_PCKG + REV_SMS_PAYU)>= 240000 THEN '>240K'
    end as arpu_sms_segment,
    case
        WHEN REV_SMS_PCKG < 50000 THEN '0-50K'
        WHEN REV_SMS_PCKG < 65000 THEN '50-65K'
        WHEN REV_SMS_PCKG < 70000 THEN '65-70K'
        WHEN REV_SMS_PCKG < 120000 THEN '70-120K'
        WHEN REV_SMS_PCKG < 150000 THEN '120-150K'
        WHEN REV_SMS_PCKG < 180000 THEN '150-180K'
        WHEN REV_SMS_PCKG < 210000 THEN '180-210K'
        WHEN REV_SMS_PCKG < 240000 THEN '210-240K'
        WHEN REV_SMS_PCKG >= 240000 THEN '>240K'
        end as arpu_sms_pckg_segment,
    case
        WHEN REV_SMS_PAYU < 50000 THEN '0-50K'
        WHEN REV_SMS_PAYU < 65000 THEN '50-65K'
        WHEN REV_SMS_PAYU < 70000 THEN '65-70K'
        WHEN REV_SMS_PAYU < 120000 THEN '70-120K'
        WHEN REV_SMS_PAYU < 150000 THEN '120-150K'
        WHEN REV_SMS_PAYU < 180000 THEN '150-180K'
        WHEN REV_SMS_PAYU < 210000 THEN '180-210K'
        WHEN REV_SMS_PAYU < 240000 THEN '210-240K'
        WHEN REV_SMS_PAYU >= 240000 THEN '>240K'
    end as arpu_sms_payu_segment,
    case
        when rev_sms_payu = rev_sms_pckg then 'NO DOMINANT'
        when rev_sms_payu > rev_sms_pckg then 'SMS PAYU'
        else 'SMS PACKAGE'
    end as dominant_rev_sms,
    case
        when cnt_sms_payu = cnt_sms_pckg then 'NO DOMINANT'
        when cnt_sms_payu > cnt_sms_pckg then 'SMS PAYU'
    else 'SMS PACKAGE'
    end as dominant_cnt_sms,
    (rev_voice_pckg+rev_voice_payu) as rev_total_voice,
    rev_voice_pckg as rev_voice_pckg,
    rev_voice_payu as rev_voice_payu,
    (dur_voice_pckg+dur_voice_payu) as total_dur_voice,
    dur_voice_pckg as dur_voice_pckg,
    dur_voice_payu as dur_voice_payu,
    case
        WHEN (REV_VOICE_PCKG + REV_VOICE_PAYU)< 50000 THEN '0-50K'
        WHEN (REV_VOICE_PCKG + REV_VOICE_PAYU)< 65000 THEN '50-65K'
        WHEN (REV_VOICE_PCKG + REV_VOICE_PAYU)< 70000 THEN '65-70K'
        WHEN (REV_VOICE_PCKG + REV_VOICE_PAYU)< 120000 THEN '70-120K'
        WHEN (REV_VOICE_PCKG + REV_VOICE_PAYU)< 150000 THEN '120-150K'
        WHEN (REV_VOICE_PCKG + REV_VOICE_PAYU)< 180000 THEN '150-180K'
        WHEN (REV_VOICE_PCKG + REV_VOICE_PAYU)< 210000 THEN '180-210K'
        WHEN (REV_VOICE_PCKG + REV_VOICE_PAYU)< 240000 THEN '210-240K'
        WHEN (REV_VOICE_PCKG + REV_VOICE_PAYU)>= 240000 THEN '>240K'
    end as arpu_voice_segment,
    case
        WHEN REV_VOICE_PCKG < 50000 THEN '0-50K'
        WHEN REV_VOICE_PCKG < 65000 THEN '50-65K'
        WHEN REV_VOICE_PCKG < 70000 THEN '65-70K'
        WHEN REV_VOICE_PCKG < 120000 THEN '70-120K'
        WHEN REV_VOICE_PCKG < 150000 THEN '120-150K'
        WHEN REV_VOICE_PCKG < 180000 THEN '150-180K'
        WHEN REV_VOICE_PCKG < 210000 THEN '180-210K'
        WHEN REV_VOICE_PCKG < 240000 THEN '210-240K'
        WHEN REV_VOICE_PCKG >= 240000 THEN '>240K'
    end as arpu_voice_pckg_segment,
    case
        WHEN REV_VOICE_PAYU < 50000 THEN '0-50K'
        WHEN REV_VOICE_PAYU < 65000 THEN '50-65K'
        WHEN REV_VOICE_PAYU < 70000 THEN '65-70K'
        WHEN REV_VOICE_PAYU < 120000 THEN '70-120K'
        WHEN REV_VOICE_PAYU < 150000 THEN '120-150K'
        WHEN REV_VOICE_PAYU < 180000 THEN '150-180K'
        WHEN REV_VOICE_PAYU < 210000 THEN '180-210K'
        WHEN REV_VOICE_PAYU < 240000 THEN '210-240K'
        WHEN REV_VOICE_PAYU >= 240000 THEN '>240K'
    end as arpu_voice_payu_segment,
    case
        when rev_voice_payu = rev_voice_pckg then 'NO DOMINANT'
        when rev_voice_payu > rev_voice_pckg then 'VOICE PAYU'
        else 'VOICE PACKAGE'
    end as dominant_rev_voice,
    case
        when dur_voice_payu = dur_voice_pckg then 'NO DOMINANT'
        when dur_voice_payu > dur_voice_pckg then 'VOICE PAYU'
        else 'VOICE PACKAGE'
    end as dominant_dur_voice
    ,date(char16(cut_off_report)) as cut_off_dt
from
(
    select
        cut_off_report,
        msisdn,
        coalesce(rev_broadband_pckg, 0) as rev_broadband_pckg,
        coalesce(rev_broadband_payu, 0) as rev_broadband_payu,
        coalesce(payload_pckg, 0) as payload_pckg,
        coalesce(payload_payu, 0) as payload_payu,
        coalesce(rev_sms_pckg, 0) as rev_sms_pckg,
        coalesce(rev_sms_payu, 0) as rev_sms_payu,
        coalesce(cnt_sms_pckg, 0) as cnt_sms_pckg,
        coalesce(cnt_sms_payu, 0) as cnt_sms_payu,
        coalesce(rev_voice_pckg, 0) as rev_voice_pckg,
        coalesce(rev_voice_payu, 0) as rev_voice_payu,
        coalesce(dur_voice_pckg, 0) as dur_voice_pckg,
        coalesce(dur_voice_payu, 0) as dur_voice_payu
    from ${table-prefix}${hvc-schema}.${table-prefix}hvc_traffic_rolling_30d_stg
) a
;

delete from ${table-prefix}${hvc-schema}.${table-prefix}hvc_traffic_rolling where cut_off_dt = '${run-date=yyyy-MM-dd}';

insert into ${table-prefix}${hvc-schema}.${table-prefix}hvc_traffic_rolling
select * from ${table-prefix}${hvc-schema}.${table-prefix}hvc_traffic_rolling_temp;


