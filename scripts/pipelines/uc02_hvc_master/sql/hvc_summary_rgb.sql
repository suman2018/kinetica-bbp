delete from ${table-prefix}${hvc-schema}.${table-prefix}hvc_summary_rgb_2 where cut_off_dt = DATE '${run-date=yyyy-MM-dd}';

insert  into ${table-prefix}${hvc-schema}.${table-prefix}hvc_summary_rgb_2
select
    cast(cut_off_report as varchar),
    brand,
    cluster_name,
    region_name,
    area_name,
    los_segment,
    arpu_segment,
    hvc_segment,
    count(case when rev_total > 0 then msisdn end) as subs,
    count(case when rev_voice > 0 then msisdn end) as subs_voice,
    count(case when rev_sms > 0 then msisdn end) as subs_sms,
    count(case when rev_broad > 0 then msisdn end) as subs_broad,
    count(case when rev_digital > 0 then msisdn end) as subs_digital,
    count(case when rev_roaming > 0 then msisdn end) as subs_roaming,
    count(case when rev_other > 0 then msisdn end) as subs_other,
    count(case when rev_total = 0 then msisdn end) as non_rgb,
    count(case when rev_voice = 0 then msisdn end) as non_rgb_voice,
    count(case when rev_sms = 0 then msisdn end) as non_rgb_sms,
    count(case when rev_broad = 0 then msisdn end) as non_rgb_broad,
    count(case when rev_digital = 0 then msisdn end) as non_rgb_digital,
    count(case when rev_roaming = 0 then msisdn end) as non_rgb_roaming,
    count(case when rev_other = 0 then msisdn end) as non_rgb_other,
    sum(rev_total) as rev_total,
    sum(rev_voice) as rev_voice,
    sum(rev_sms) as rev_sms,
    sum(rev_broad) as rev_broad,
    sum(rev_digital) as rev_digital,
    sum(rev_roaming) as rev_roaming,
    sum(rev_other) as rev_other,
    cut_off_dt
from
(
    select
        a.cut_off_report, 
        coalesce(a.brnd_nme, 'UNKNOWN') as brand,   -- @MATE: I prefixed brnd_nme with alias "a"
        a.msisdn,
        coalesce(a.cluster_name, 'UNKNOWN') as cluster_name,
        coalesce(a.region_name,'UNKNOWN') as region_name,
        coalesce(a.area_name, 'UNKNOWN') as area_name,
        coalesce(a.los_segment,'<= 3 MONTHS') as los_segment,
        coalesce(a.arpu_segment, '0-50K') as arpu_segment,
        coalesce(a.hvc_segment,'LAC') as hvc_segment,
        coalesce(b.rev_total,0) as rev_total,
        coalesce(b.rev_voice,0) as rev_voice,
        coalesce(b.rev_sms,0) as rev_sms,
        coalesce(b.rev_broad,0) as rev_broad,
        coalesce(b.rev_digital,0) as rev_digital,
        coalesce(b.rev_roaming,0) as rev_roaming,
        coalesce(b.rev_other,0) as rev_other,
        a.cut_off_dt
        --'${run-date=yyyy-MM-dd}'  -- Mate: might not be necessary.
    from
    (
        select *
        from ${table-prefix}${hvc-schema}.${table-prefix}hvc_summary_cb
        where cut_off_dt = '${run-date=yyyy-MM-dd}' -- @MATE: Fix this!!! '${period_dash}'
            and brnd_nme in ('LOOP','KartuAS','simPATI')
    ) a
    left join
    (
        select * from ${table-prefix}${hvc-schema}.${table-prefix}hvc_rgb_rolling
        where cut_off_dt = '${run-date=yyyy-MM-dd}'  -- @MATE: Fix this!!! '${period_dash}'
    ) b
    on a.msisdn = b.msisdn
) aaa
group by cut_off_report, brand, cluster_name, region_name, area_name, los_segment, arpu_segment, hvc_segment, cut_off_dt;
