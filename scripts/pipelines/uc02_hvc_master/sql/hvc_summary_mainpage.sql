-- this implements the script hvc_summary_mainpage.sh

-- seems to depend on HVC_SUMMARY_MAINPAGE being dropped and recreated.
-- seems to depend on HVC_SUMMARY_MAINPAGE_2 being dropped and recreated.

-- depends on script hvc_ingest_cb_baseline.sh

-- there is an apparent overwrite of table HVC_SUMMARY_MAINPAGE SQL never called. I ignored for now.

-- this table is actually overwritten. Is it a daily snapshot to be added to something or simply recreated?
-- insert overwrite table it.hvc_summary_mainpage_2 partition (cut_off_dt='${period_dash}')
delete from ${table-prefix}${hvc-schema}.${table-prefix}hvc_summary_mainpage_2 where cut_off_dt = '${run-date=yyy-MM-dd}';

insert into ${table-prefix}${hvc-schema}.${table-prefix}hvc_summary_mainpage_2
select
    '${run-date=yyyy-MM-dd}', -- @Mate: ${period_dash}',
    coalesce(a.los_segment, b.los_segment) los_segment,
    coalesce(a.arpu_segment, b.arpu_segment) arpu_segment,
    coalesce(a.hvc_segment, b.hvc_segment) hvc_segment,
    coalesce(a.brand, b.brand) brand, coalesce(a.cluster_name, b.cluster_name) cluster_name,
    coalesce(a.region, b.region) as region_name,
    coalesce(a.area, b.area) as area_name,

    coalesce(a.subs, 0) as subs,
    coalesce(a.subs_voice, 0) as subs_voice,
    coalesce(a.subs_sms, 0) as subs_sms,
    coalesce(a.subs_broad, 0) as subs_broad,
    coalesce(a.subs_digital, 0) as subs_digital,
    coalesce(a.subs_roaming, 0) as subs_roaming,
    coalesce(a.subs_other, 0) as subs_other,
    coalesce(a.non_rgb, 0) as non_rgb,
    coalesce(a.non_rgb_voice, 0) as non_rgb_voice,
    coalesce(a.non_rgb_sms, 0) as non_rgb_sms,
    coalesce(a.non_rgb_broad, 0) as non_rgb_broad,
    coalesce(a.non_rgb_digital, 0) as non_rgb_digital,
    coalesce(a.non_rgb_roaming, 0) as non_rgb_roaming,
    coalesce(a.non_rgb_other, 0) as non_rgb_other,
    coalesce(a.rev_total, 0) as rev_total,
    coalesce(a.rev_voice, 0) as rev_voice,
    coalesce(a.rev_sms, 0) as rev_sms,
    coalesce(a.rev_broad, 0) as rev_broad,
    coalesce(a.rev_digital, 0) as rev_digital,
    coalesce(a.rev_roaming, 0) as rev_roaming,
    coalesce(a.rev_other, 0) as rev_other,

    coalesce(b.subs, 0) as subs_bl,
    coalesce(b.subs_voice, 0) as subs_voice_bl,
    coalesce(b.subs_sms, 0) as subs_sms_bl,
    coalesce(b.subs_broad, 0) as subs_broad_bl,
    coalesce(b.subs_digital, 0) as subs_digital_bl,
    coalesce(b.subs_roaming, 0) as subs_roaming_bl,
    coalesce(b.subs_other, 0) as subs_other_bl,
    coalesce(b.non_rgb, 0) as non_rgb_bl,
    coalesce(b.non_rgb_voice, 0) as non_rgb_voice_bl,
    coalesce(b.non_rgb_sms, 0) as non_rgb_sms_bl,
    coalesce(b.non_rgb_broad, 0) as non_rgb_broad_bl,
    coalesce(b.non_rgb_digital, 0) as non_rgb_digital_bl,
    coalesce(b.non_rgb_roaming, 0) as non_rgb_roaming_bl,
    coalesce(b.non_rgb_other, 0) as non_rgb_other_bl,
    coalesce(b.rev_total, 0) as rev_total_bl,
    coalesce(b.rev_voice, 0) as rev_voice_bl,
    coalesce(b.rev_sms, 0) as rev_sms_bl,
    coalesce(b.rev_broad, 0) as rev_broad_bl,
    coalesce(b.rev_digital, 0) as rev_digital_bl,
    coalesce(b.rev_roaming, 0) as rev_roaming_bl,
    coalesce(b.rev_other, 0) as rev_other_bl,
    '${run-date=yyyy-MM-dd}'

from
(
    select * from ${table-prefix}${hvc-schema}.${table-prefix}hvc_summary_rgb_2
    where cut_off_dt = '${run-date=yyyy-MM-dd}' -- @Mate: '${period_dash}'
        and brand in ('LOOP','KartuAS','simPATI')
) a
full outer join
(
    select a.*
    from ${table-prefix}${hvc-schema}.${table-prefix}hvc_summary_baseline a
    inner join
    (
        select distinct
            case
                -- when cut_off_dt != '${period_baseline}' and cut_off_dt < '${period_baseline}' then cut_off_dt @MATE fix this!!!
                when cut_off_dt != '${run-date=yyyy-MM-dd}' and cut_off_dt < '${run-date=yyyy-MM-dd}' then cut_off_dt
                -- when cut_off_dt != '${period_baseline}' then '${period_baseline}'  @MATE fix this!!!
                when cut_off_dt != '${run-date=yyyy-MM-dd}' then cast('${run-date=yyyy-MM-dd}' as date)
                else cut_off_dt end as cut_off_dt
        from
        (
            select max(cut_off_dt) as cut_off_dt
            from ${table-prefix}${hvc-schema}.${table-prefix}hvc_summary_baseline
        ) a
    ) b
        on a.cut_off_dt = b.cut_off_dt
) b
    on a.los_segment = b.los_segment and a.arpu_segment = b.arpu_segment
        and a.hvc_segment = b.hvc_segment and a.brand = b.brand and a.cluster_name = b.cluster_name
        and a.region=b.region and a.area=b.area;

