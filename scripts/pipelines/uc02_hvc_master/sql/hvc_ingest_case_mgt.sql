-- insert overwrite table it.hvc_cse_mgt_rolling partition (cut_off_dt='${period_dash}') select
-- optimize this! No need for stage, can go directly into this table

delete from ${table-prefix}${hvc-schema}.${table-prefix}hvc_cse_mgt_rolling where cut_off_dt = '${run-date=yyyy-MM-dd}';

insert into ${table-prefix}${hvc-schema}.${table-prefix}hvc_cse_mgt_rolling
select
    cut_off_dttime,
    msisdn,
    case_id,
    cstmr_typ,
    case_typ1,
    case_typ2,
    case_typ3,
    case_crtn_dt_tm,
    case_clsd_dt_tm,
    case_sttus,
    case_sttus_final,
    speed_of_service,
    duration,
    duration_category,
    '${run-date=yyyy-MM-dd}' as cut_off_dt
from ${table-prefix}${hvc-schema}.${table-prefix}hvc_cse_mgt_rolling_stg
;
