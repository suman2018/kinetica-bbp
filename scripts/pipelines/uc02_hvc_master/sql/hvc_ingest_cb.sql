-- @Mate: validate that this is drop/recreate and not append

delete from ${table-prefix}${hvc-schema}.${table-prefix}hvc_cb_rolling
where cut_off_dt = '${run-date=yyyy-MM-dd}';
 

--- create or replace temp table ${hvc-schema}.${table-prefix}hvc_cb_rolling as

create or replace temp table ${table-prefix}${hvc-schema}.${table-prefix}hvc_cb_rolling_temp as    
    select
        cut_off_report,
        msisdn,
        brnd_nme,
        coalesce(los, 0) as los,
        case
            WHEN COALESCE(LOS, 0) <= 90 THEN '<= 3 MONTHS'
             WHEN COALESCE(LOS, 0) <= 180 THEN '3 MONTHS - 6 MONTHS'
             WHEN COALESCE(LOS, 0) <= 360 THEN '6 MONTHS - 1 YEAR'
             WHEN COALESCE(LOS, 0) <= 1080 THEN '1 YEAR - 3 YEARS'
             WHEN COALESCE(LOS, 0) <= 2520 THEN '3 YEARS - 7 YEARS'
             WHEN COALESCE(LOS, 0) > 2520 THEN '> 7 YEARS'
             ELSE 'UNKNOWN'
        end as los_segment,
        coalesce(b.area_name, 'UNKNOWN') as area_name,
        coalesce(b.region_name, 'UNKNOWN') as region_name,
        coalesce(b.cluster_name, 'UNKNOWN') as cluster_name,
        pre2post_flg,
        flg_youth,
        --- '${run-date=yyyy-MM-dd}'
        date(char16(cut_off_report ))  as cut_off_dt   -- @Mate: hive has this as partition property of table. 
from ${table-prefix}${hvc-schema}.${table-prefix}hvc_cb_rolling_stg a 
left join (select area_nme as area_name, regional_chnnl region_name, clstr_sls as cluster_name 
from ${table-prefix}${laccima-schema}.${table-prefix}laccima_ref_${run-date=yyyyMMdd} group by area_nme, regional_chnnl, clstr_sls) b
        on a.mst_dmnt_clstr_nme=b.CLUSTER_NAME;

insert into ${table-prefix}${hvc-schema}.${table-prefix}hvc_cb_rolling 
    select * from ${table-prefix}${hvc-schema}.${table-prefix}hvc_cb_rolling_temp;

drop table ${table-prefix}${hvc-schema}.${table-prefix}hvc_cb_rolling_temp;
