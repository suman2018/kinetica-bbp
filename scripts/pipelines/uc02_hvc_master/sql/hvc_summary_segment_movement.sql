--insert overwrite table it.hvc_segment_movement_summ_2 partition (cut_off_dt='${period_now}')
delete from ${table-prefix}${hvc-schema}.${table-prefix}hvc_segment_movement_summ_2 where cut_off_dt = '${run-date=yyyy-MM-dd}';

insert into ${table-prefix}${hvc-schema}.${table-prefix}hvc_segment_movement_summ_2
select
    '${run-date=yyyy-MM-dd}' as cut_off_report , -- @Mate: '${period_now}'
    a.hvc_segment as hvc_segment_now,
    b.hvc_segment as hvc_segment_before,
    count(case when b.hvc_segment is null then a.msisdn end) as sales,
    count(case when a.hvc_segment is null then b.msisdn end) as churn,
    count(case when a.hvc_segment=b.hvc_segment then a.msisdn end) as stay,
    count(case when a.hvc_segment<>b.hvc_segment then a.msisdn end) as "move",
    DATE '${run-date=yyyy-MM-dd}' as cut_off_dt
from
(
    select cut_off_dt, msisdn, hvc_segment from
    ${table-prefix}${hvc-schema}.${table-prefix}hvc_summary_cb where cut_off_dt = '${run-date=yyyy-MM-dd}'   -- @Mate: '${period_now}'
        and brnd_nme in ('LOOP','KartuAS','simPATI')
) a
full outer join
(
    select cut_off_dt, msisdn, hvc_segment from ${table-prefix}${hvc-schema}.${table-prefix}hvc_summary_cb
    where cut_off_dt = cast('${run-date=yyyy-MM-dd}' as date) - interval '1' day  -- @Mate: '${period_before}'
        and brnd_nme in ('LOOP','KartuAS','simPATI')
) b
    on a.msisdn = b.msisdn
group by a.cut_off_dt,b.cut_off_dt, a.hvc_segment, b.hvc_segment;

delete from ${table-prefix}${hvc-schema}.${table-prefix}hvc_segment_movement_summ_2_bl where cut_off_dt = '${run-date=yyyy-MM-dd}';

-- insert overwrite table it.hvc_segment_movement_summ_2_bl partition (cut_off_dt='${period_now}')
insert into ${table-prefix}${hvc-schema}.${table-prefix}hvc_segment_movement_summ_2_bl
select
    '${run-date=yyyy-MM-dd}' as cut_off_report , -- @Mate: '${period_now}' 
    a.hvc_segment as hvc_segment_now,
    b.hvc_segment as hvc_segment_before,
    count(case when a.brnd_nme in ('LOOP','KartuAS','simPATI') and b.msisdn is null then a.msisdn end) as sales,
    count(case when a.msisdn is null or  a.brnd_nme='kartuHALO' then b.msisdn end) as churn,
    count(case when a.brnd_nme in ('LOOP','KartuAS','simPATI') and a.hvc_segment=b.hvc_segment then b.msisdn end) as stay,
    count(case when a.brnd_nme in ('LOOP','KartuAS','simPATI') and a.hvc_segment<>b.hvc_segment and b.msisdn is not null then a.msisdn end) as "move",
    count(case when a.msisdn is null and b.msisdn is not null then coalesce(b.msisdn,0) end) as churn_subs,
    count(case when a.brnd_nme='kartuHALO' then b.msisdn end) as move_postpaid,
    '${run-date=yyyy-MM-dd}' as cut_off_dt 
from
(
    select cut_off_dt, msisdn, hvc_segment,brnd_nme from ${table-prefix}${hvc-schema}.${table-prefix}hvc_summary_cb
    where cut_off_dt = '${run-date=yyyy-MM-dd}'  -- @Mate: '${period_now}'
) a
full outer join
(
    select DATE '2017-12-31' as cut_off_dt, msisdn, 'HVC' as hvc_segment from ${table-prefix}${hvc-schema}.${table-prefix}hvc_cb_baseline   ---_201712 -- @Mate: fix this table hvc_cb_baseline_201712 it is 1 time load. get data
    where brnd_nme in ('LOOP','KartuAS','simPATI') --and cut_off_report = '201712'  --- change to last year
) b
    on a.msisdn = b.msisdn
group by a.hvc_segment, b.hvc_segment;
