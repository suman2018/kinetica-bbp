--- insert overwrite table it.hvc_purchase_summ partition (cut_off_dt='${period_dash}')
delete from ${table-prefix}${hvc-schema}.${table-prefix}hvc_purchase_summ where cut_off_dt = DATE '${run-date=yyyy-MM-dd}';

insert into ${table-prefix}${hvc-schema}.${table-prefix}hvc_purchase_summ
select * from
(
    select
        a.cut_off_report,  -- @Mate: I added cast
        coalesce(los_segment,'<= 3 MONTHS') as los_segment,
        coalesce(arpu_segment,'0-50K') as arpu_segment,
        coalesce(hvc_segment,'LAC') as hvc_segment,
        brnd_nme,
        area_name,
        region_name,
        cluster_name,
        channel_1st_id  as channel_name,
        count(distinct a.msisdn) as subs,
        sum(trx_channel_1st) as trx_channel,
        a.cut_off_dt 
    from
    (
        select * from ${table-prefix}${hvc-schema}.${table-prefix}hvc_purchase_rolling
        where cut_off_dt = '${run-date=yyyy-MM-dd}' -- @Mate: '${period_dash}'
     ) a
     left join
     (
         select * from ${table-prefix}${hvc-schema}.${table-prefix}hvc_summary_cb
         where cut_off_dt = '${run-date=yyyy-MM-dd}' -- @Mate: '${period_dash}'
             and brnd_nme in ('LOOP','KartuAS','simPATI')
     ) b
         on a.msisdn = b.msisdn
     group by a.cut_off_report, los_segment, arpu_segment, hvc_segment, brnd_nme, area_name, region_name, cluster_name,channel_1st_id,a.cut_off_dt

     union all

     select
         a.cut_off_report,   --- @Mate: I added cast
         coalesce(los_segment,'<= 3 MONTHS') as los_segment,
         coalesce(arpu_segment,'0-50K') as arpu_segment,
         coalesce(hvc_segment,'LAC') as hvc_segment,
         brnd_nme,
         area_name,
         region_name,
         cluster_name,
         'ALL'  as channel_name,
         count(distinct a.msisdn) as subs,
         sum(trx_all_channel) as trx_channel,
         a.cut_off_dt
     from
     (
         select * from ${table-prefix}${hvc-schema}.${table-prefix}hvc_purchase_rolling
         where cut_off_dt = '${run-date=yyyy-MM-dd}' -- @Mate: '${period_dash}'
     ) a
     left join
     (
         select * from ${table-prefix}${hvc-schema}.${table-prefix}hvc_summary_cb
         where cut_off_dt = '${run-date=yyyy-MM-dd}' -- @Mate: '${period_dash}'
             and brnd_nme in ('LOOP','KartuAS','simPATI')
     ) b
         on a.msisdn = b.msisdn
     group by a.cut_off_report, los_segment, arpu_segment, hvc_segment, brnd_nme, area_name, region_name, cluster_name, a.cut_off_dt
) a;

