-- this implements hvc_ingest_postpaid_cb_rev.sh
-- Agy from TK said this logic is sound, an append.

-- insert overwrite table it.hvc_ingest_postpaid_cb_rev partition (cut_off_dt='${period_dash}')

delete from ${table-prefix}${hvc-schema}.${table-prefix}hvc_segment_postpaid_movement_summ 
where cut_off_report = '${run-date=yyyyMM}';

insert into ${table-prefix}${hvc-schema}.${table-prefix}hvc_segment_postpaid_movement_summ
select 
'${run-date=yyyyMM}' as cut_off_report,
a.area_hlr as area_now,
a.region_hlr as region_now,
a.city as city_now,
b.area_hlr as area_before,
b.region_hlr as region_before,
b.city as city_before,
a.hvc_segment AS hvc_segment_now,
b.hvc_segment AS hvc_segment_before,
COUNT(CASE WHEN B.hvc_segment IS NULL and b.msisdn is NULL  THEN A.msisdn END) AS sales,
COUNT(CASE WHEN A.hvc_segment IS NULL THEN B.msisdn END) AS churn,
COUNT(CASE WHEN A.hvc_segment=B.hvc_segment THEN A.msisdn END) AS stay,
COUNT(CASE WHEN A.hvc_segment<>B.hvc_segment THEN A.msisdn END) AS "move",
'${run-date=yyyy-MM-dd}' as cut_off_dt
FROM
(select cut_off_report, msisdn, hvc_segment, area_hlr, region_hlr, city ,cut_off_dt from ${table-prefix}${hvc-schema}.${table-prefix}hvc_summary_postpaid_cb_rev 
where cut_off_report='${run-date=yyyyMM}') A 
FULL OUTER JOIN
(select cut_off_report, msisdn, hvc_segment, area_hlr, region_hlr, city ,cut_off_dt from ${table-prefix}${hvc-schema}.${table-prefix}hvc_summary_postpaid_cb_rev 
where 
cut_off_report=
CONCAT(YEAR(DATE '${run-date=yyyy-MM-dd}' - INTERVAL '1' MONTH) ,  RIGHT(100 + MONTH(DATE '${run-date=yyyy-MM-dd}' - INTERVAL '1' MONTH),2))
) B
ON A.msisdn=b.msisdn
GROUP BY a.hvc_segment, b.hvc_segment, a.area_hlr, a.region_hlr, a.city, b.area_hlr, b.region_hlr, b.city
;

delete from  ${table-prefix}${hvc-schema}.${table-prefix}hvc_segment_postpaid_movement_summ_bl
where cut_off_report = '${run-date=yyyyMM}';

insert into ${table-prefix}${hvc-schema}.${table-prefix}hvc_segment_postpaid_movement_summ_bl
select
'${run-date=yyyyMM}' as cut_off_report,
a.hvc_segment AS hvc_segment_now,
b.hvc_segment AS hvc_segment_before,
COUNT(CASE WHEN B.msisdn is null THEN A.msisdn END) AS sales,
COUNT(CASE WHEN A.hvc_segment IS NULL THEN B.msisdn END) AS churn,
COUNT(CASE WHEN A.hvc_segment=B.hvc_segment THEN b.msisdn END) AS stay,
COUNT(CASE WHEN A.hvc_segment<>B.hvc_segment and b.msisdn is NOT NULL THEN A.msisdn END) AS "move",
'${run-date=yyyy-MM-dd}' as cut_off_dt
FROM
(select cut_off_report, msisdn, hvc_segment,cut_off_dt from ${table-prefix}${hvc-schema}.${table-prefix}hvc_summary_postpaid_cb_rev
where cut_off_report='${run-date=yyyyMM}') A
FULL OUTER JOIN
(select msisdn, 'HVC' as hvc_segment from ${table-prefix}${hvc-schema}.${table-prefix}hvc_cb_baseline where brnd_nme in ('kartuHALO')) B
ON A.msisdn=b.msisdn
GROUP BY a.hvc_segment, b.hvc_segment
;
