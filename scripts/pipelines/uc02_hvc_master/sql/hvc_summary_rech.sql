-- this implements the script: hvc_summary_rech.sh

-- seems to depend on HVC_SUMMARY_RECHARGE_STG being dropped and recreated.
-- seems to depend on HVC_SUMMARY_RECHARGE_DET being dropped and recreated.
-- seems to depend on HVC_SUMMARY_RECHARGE_SUMM being dropped and recreated.

-- seems to depend on hvc_rech_rolling existing and being maintained
-- seems to depend on hvc_summary_cb existing and being maintained

-- this is named as a stg table but no external load in code. But can CTA since it is dropped and recreated. Maybe a temp?

/* KI_HINT_DICT_PROJECTION */
create or replace temp table ${table-prefix}${hvc-schema}.${table-prefix}hvc_summary_recharge_stg as
(
    select
        a.cut_off_report,
        a.msisdn,
        coalesce(b.los_segment,'<= 3 MONTHS') as los_segment,
        coalesce(b.arpu_segment,'0-50K') as arpu_segment,
        coalesce(b.hvc_segment,'LAC') as hvc_segment,
        coalesce(b.brnd_nme, 'UNKNOWN') as brand,
        coalesce(b.area_name, 'UNKNOWN') as area_name,
        coalesce(b.region_name, 'UNKNOWN') as region_name,
        coalesce(b.cluster_name, 'UNKNOWN') as cluster_name,
        chnnl_grp,
        chnnl_ctgry,
        chnnl_nme,
        coalesce(rchrg_trx,0) as rchrg_trx,
        coalesce(rchrg_amt,0) as rchrg_amt,
        a.cut_off_dt 
    from
    (
        select * from ${table-prefix}${hvc-schema}.${table-prefix}hvc_rech_rolling where cut_off_dt = '${run-date=yyyy-MM-dd}' --@Mate: '${period_dash}' 
    ) a
    left join
    (
        select * from ${table-prefix}${hvc-schema}.${table-prefix}hvc_summary_cb a where cut_off_dt = '${run-date=yyyy-MM-dd}' --@Mate: '${period_dash}'
            and brnd_nme in ('LOOP','KartuAS','simPATI')
    ) b
    on a.msisdn = b.msisdn and a.cut_off_dt = b.cut_off_dt
);

delete from ${table-prefix}${hvc-schema}.${table-prefix}hvc_summary_recharge_det where cut_off_dt = DATE '${run-date=yyyy-MM-dd}';

-- insert overwrite table it.hvc_summary_recharge_det partition (cut_off_dt='${period_dash}')
insert into ${table-prefix}${hvc-schema}.${table-prefix}hvc_summary_recharge_det
select
    cut_off_report,  -- synonymous with cut_off_dt based on logic here.
    los_segment,
    arpu_segment,
    hvc_segment,
    brand,
    area_name as area,       -- @Mate: was area
    region_name as region,   -- @Mate: was region
    cluster_name,
    chnnl_grp,
    chnnl_ctgry,
    chnnl_nme,
    sum(rchrg_trx) as rchrg_trx,
    sum(rchrg_amt) as rchrg_amt,
    count(distinct msisdn) as count_msisdn,
    cut_off_dt 
from ${table-prefix}${hvc-schema}.${table-prefix}hvc_summary_recharge_stg
group by
     cut_off_report,
     brand,
     area_name,      -- @Mate: was area
     region_name,    -- @Mate: was region
     cluster_name,
     los_segment,
     arpu_segment,
     hvc_segment,
     chnnl_grp,
     chnnl_ctgry,
     chnnl_nme,
     cut_off_dt;

-- And an overwrite again. Probably not a CTA or temp, bk this is actual summary table...in Hive these were meant to be
-- pushed to Teradata. Here we are actually appending.
-- insert overwrite table it.hvc_summary_recharge_summ partition (cut_off_dt='${period_dash}')
delete from ${table-prefix}${hvc-schema}.${table-prefix}hvc_summary_recharge_summ where cut_off_dt = DATE '${run-date=yyyy-MM-dd}';

insert into ${table-prefix}${hvc-schema}.${table-prefix}hvc_summary_recharge_summ
select
    cut_off_report,
    los_segment,
    arpu_segment,
    hvc_segment,
    brand,
    area_name,       -- @Mate: was area
    region_name,     -- @Mate: was region
    cluster_name,
    sum(rchrg_trx) as rchrg_trx,
    sum(rchrg_amt) as rchrg_amt,
    count(distinct msisdn) as count_msisdn,
    cut_off_dt 
from ${table-prefix}${hvc-schema}.${table-prefix}hvc_summary_recharge_stg
group by cut_off_report,
    brand,
    area_name,      -- @Mate: was area
    region_name,         -- @Mate: was region
    cluster_name,  los_segment, arpu_segment,hvc_segment, cut_off_dt;
