-- this implements hvc_ingest_recharge.sh
-- Agy from TK said this logic is sound, an append.

delete from ${table-prefix}${hvc-schema}.${table-prefix}hvc_rech_rolling where cut_off_dt = '${run-date=yyyy-MM-dd}';


create or replace temp table ${table-prefix}${hvc-schema}.${table-prefix}hvc_rech_rolling_temp as 
select
    cut_off_report,
    msisdn,
    chnnl_grp,
    chnnl_ctgry,
    chnnl_nme,
    coalesce(rchrg_trx,0) as rchrg_trx,
    coalesce(rchrg_amt,0) as rchrg_amt,
    case
        WHEN COALESCE(RCHRG_AMT,0) < 50000 THEN '0-50K'
        WHEN COALESCE(RCHRG_AMT,0) < 65000 THEN '50-65K'
        WHEN COALESCE(RCHRG_AMT,0) < 70000 THEN '65-70K'
        WHEN COALESCE(RCHRG_AMT,0) < 120000 THEN '70-120K'
        WHEN COALESCE(RCHRG_AMT,0) < 150000 THEN '120-150K'
        WHEN COALESCE(RCHRG_AMT,0) < 180000 THEN '150-180K'
        WHEN COALESCE(RCHRG_AMT,0) < 210000 THEN '180-210K'
        WHEN COALESCE(RCHRG_AMT,0) < 240000 THEN '210-240K'
        WHEN COALESCE(RCHRG_AMT,0) >= 240000 THEN '>240K'
    end as rchrg_segment,
    date(char16(cut_off_report)) as cut_off_dt
from ${table-prefix}${hvc-schema}.${table-prefix}hvc_rech_stg
;

--- insert into it.hvc_rech_rolling partition (cut_off_dt='${period_dash}')
insert into ${table-prefix}${hvc-schema}.${table-prefix}hvc_rech_rolling
    select * from  ${table-prefix}${hvc-schema}.${table-prefix}hvc_rech_rolling_temp;

    drop table ${table-prefix}${hvc-schema}.${table-prefix}hvc_rech_rolling_temp;
