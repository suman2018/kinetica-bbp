
--- update last month data

delete from ${table-prefix}${cms-schema}.${table-prefix}cms_performance_campaign
where date(periode) = date(datetime(last_day(date('${run-date=yyyy-MM-dd}') - interval '1' month)));


insert into ${table-prefix}${cms-schema}.${table-prefix}cms_performance_campaign  
select a.periode, a.eom, a.theme, a.category, a.campaign_name, a.campaign_group_id, a.start_period, a.end_period,
a.tot_campaign, a.target, a.eligible, a.taker, b.unique_target, b.unique_taker, 
a.revenue, a.recharge, a.control, a.taker_control, a.revenue_control, a.uplift, a.last_trx_date
from 
(
select periode, 1 eom, campaign_objective_desc as theme, category, campaign_name, campaign_group_id,
min(start_period) start_period,
max(end_period) end_period,
count(distinct campaign_id) as tot_campaign,
sum(target) as target, 
sum(eligible) as eligible,
sum(taker) as taker,
sum(case when campaign_group_id in (1,4,6,7) then revenue_taker else 0 end) revenue,
sum(case when campaign_group_id = 2 then revenue_taker else 0 end) recharge,
sum(control) as control,
sum(taker_control) as taker_control,
sum(case when campaign_group_id in (1,4,6,7) then revenue_control else 0 end) revenue_control,
sum((((arpu_target_after - arpu_target_before) - (arpu_control_after - arpu_control_before))/nullif((arpu_target_before * 1.0),0)) * eligible)/nullif(sum(eligible),0) uplift, 
max(last_trx_date) as last_trx_date
from ${table-prefix}${cms-schema}.${table-prefix}cms_summary_bycampaignid
where periode = date(datetime(last_day(date('${run-date=yyyy-MM-dd}') - interval '1' month)))
and eom = 1
group by 1,3,4,5,6 
) a left join 
(
select date(datetime(last_day(trx_date))) as periode, 1 eom, c.campaign_objective_desc theme, a.category, a.campaign_name, a.campaign_group_id,
count(distinct msisdn) unique_target,
count(distinct case when taker > 0 and iscontrolgroup = 'false' then msisdn end) unique_taker
from ${table-prefix}${cms-schema}.${table-prefix}cms_campaign_tracking_detail a 
left join 
(
	select distinct campaign_objective_id, campaign_objective_desc
	from ${table-prefix}${cms-schema}.${table-prefix}cms_campaign_objective
) c on a.campaign_objective_id = c.campaign_objective_id
where a.pipeline_step not in ('cms_usage_detail_insert', 'cms_campaign_digital_detail_insert')
and (trx_date between date(datetime(dateadd('day', 1, last_day(date('${run-date=yyyy-MM-dd}') - interval '2' month)))) 
and date(datetime(last_day(date('${run-date=yyyy-MM-dd}') - interval '1' month))))
group by 1,3,4,5,6
) b on a.periode = b.periode 
and a.eom = b.eom   
and a.theme = b.theme
and a.category = b.category
and a.campaign_name = b.campaign_name 
and a.campaign_group_id = b.campaign_group_id;


--- update last 3 months data

delete from ${table-prefix}${cms-schema}.${table-prefix}cms_performance_campaign
where eom = 0;


insert into ${table-prefix}${cms-schema}.${table-prefix}cms_performance_campaign  
select a.periode, a.eom, a.theme, a.category, a.campaign_name, a.campaign_group_id, a.start_period, a.end_period,
a.tot_campaign, a.target, a.eligible, a.taker, b.unique_target, b.unique_taker, 
a.revenue, a.recharge, a.control, a.taker_control, a.revenue_control, a.uplift, a.last_trx_date
from 
(
select periode, eom, campaign_objective_desc as theme, category, campaign_name, campaign_group_id,
min(start_period) start_period,
max(end_period) end_period,
count(distinct campaign_id) as tot_campaign,
sum(target) as target, 
sum(eligible) as eligible,
sum(taker) as taker,
sum(case when campaign_group_id in (1,4,6,7) then revenue_taker else 0 end) revenue,
sum(case when campaign_group_id = 2 then revenue_taker else 0 end) recharge,
sum(control) as control,
sum(taker_control) as taker_control,
sum(case when campaign_group_id in (1,4,6,7) then revenue_control else 0 end) revenue_control,
sum((((arpu_target_after - arpu_target_before) - (arpu_control_after - arpu_control_before))/nullif((arpu_target_before * 1.0),0)) * eligible)/nullif(sum(eligible),0) uplift, 
max(last_trx_date) as last_trx_date
from ${table-prefix}${cms-schema}.${table-prefix}cms_summary_bycampaignid
where eom = 0
group by 1,2,3,4,5,6 
) a left join 
(
select date(datetime(last_day(trx_date))) as periode, 0 eom, c.campaign_objective_desc theme, a.category, a.campaign_name, a.campaign_group_id,
count(distinct msisdn) unique_target,
count(distinct case when taker > 0 and iscontrolgroup = 'false' then msisdn end) unique_taker
from ${table-prefix}${cms-schema}.${table-prefix}cms_campaign_tracking_detail a 
left join 
(
	select distinct campaign_objective_id, campaign_objective_desc
	from ${table-prefix}${cms-schema}.${table-prefix}cms_campaign_objective
) c on a.campaign_objective_id = c.campaign_objective_id
where a.pipeline_step not in ('cms_usage_detail_insert', 'cms_campaign_digital_detail_insert')
and 
(
   	(trx_date between date(datetime(dateadd('day', 1, last_day(date('${run-date=yyyy-MM-dd}') - interval '3' month)))) and date(date('${run-date=yyyy-MM-dd}') - interval '2' month))
	or (trx_date between date(datetime(dateadd('day', 1, last_day(date('${run-date=yyyy-MM-dd}') - interval '2' month)))) and date(date('${run-date=yyyy-MM-dd}') - interval '1' month))
	or (trx_date between date(datetime(dateadd('day', 1, last_day(date('${run-date=yyyy-MM-dd}') - interval '1' month)))) and date(date('${run-date=yyyy-MM-dd}')))
)
group by 1,3,4,5,6
) b on a.periode = b.periode 
and a.eom = b.eom   
and a.theme = b.theme
and a.category = b.category
and a.campaign_name = b.campaign_name 
and a.campaign_group_id = b.campaign_group_id;




