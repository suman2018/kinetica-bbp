
--- update last month data

delete from ${table-prefix}${cms-schema}.${table-prefix}cms_performance_summary
where date(periode) = date(datetime(last_day(date('${run-date=yyyy-MM-dd}') - interval '1' month)));


insert into ${table-prefix}${cms-schema}.${table-prefix}cms_performance_summary
select a.periode, a.eom, a.brand, a.campaign_category, a.tot_campaign, a.target, a.eligible, a.taker, b.unique_target, b.unique_taker, 
a.revenue, a.recharge, a.control, a.taker_control, a.revenue_control, a.uplift, a.last_trx_date
from 
(
select periode, 1 eom, brand, 'Overall' campaign_category,
count(distinct campaign_id) as tot_campaign,
sum(target) as target, 
sum(eligible) as eligible,
sum(taker) as taker,
sum(case when campaign_group_id in (1,4,6,7) then revenue_taker else 0 end) revenue,
sum(case when campaign_group_id = 2 then revenue_taker else 0 end) recharge,
sum(control) as control,
sum(taker_control) as taker_control,
sum(case when campaign_group_id in (1,4,6,7) then revenue_control else 0 end) revenue_control,
sum((((arpu_target_after - arpu_target_before) - (arpu_control_after - arpu_control_before))/(nullif(arpu_target_before,0) * 1.0)) * eligible)/nullif(sum(eligible),0) uplift, 
max(last_trx_date) as last_trx_date
from ${table-prefix}${cms-schema}.${table-prefix}cms_summary_bycampaignid
where campaign_group_id not in (11,12)
and date(periode) = date(datetime(last_day(date('${run-date=yyyy-MM-dd}') - interval '1' month)))
and eom = 1
group by 1,3 
) a left join 
(
select date(datetime(last_day(trx_date))) as periode, 1 eom, 
case
	when upper(substring(a.campaign_id, 11, 1)) = 'A' then 'Simpati'
	when upper(substring(a.campaign_id, 11, 1)) = 'B' then 'KartuAs'
	when upper(substring(a.campaign_id, 11, 1)) = 'C' then 'AllPrepaid'
	when upper(substring(a.campaign_id, 11, 1)) = 'D' then 'KartuHALO'
	when upper(substring(a.campaign_id, 11, 1)) = 'E' then 'AllBrands'
	when upper(substring(a.campaign_id, 11, 1)) = 'F' then 'Loop'
end as brand,'Overall' campaign_category,
count(distinct msisdn) unique_target,
count(distinct case when taker > 0 and iscontrolgroup = 'false' then msisdn end) unique_taker
from ${table-prefix}${cms-schema}.${table-prefix}cms_campaign_tracking_detail a 
where a.pipeline_step not in ('cms_usage_detail_insert', 'cms_campaign_digital_detail_insert')
and (trx_date between date(datetime(dateadd('day', 1, last_day(date('${run-date=yyyy-MM-dd}') - interval '2' month)))) 
and date(datetime(last_day(date('${run-date=yyyy-MM-dd}') - interval '1' month))))
and a.campaign_group_id not in (11,12)
group by 1,3
) b on a.periode = b.periode 
and a.eom = b.eom  
and a.brand = b.brand 
and a.campaign_category = b.campaign_category;

insert into ${table-prefix}${cms-schema}.${table-prefix}cms_performance_summary
select a.periode, a.eom, a.brand, a.campaign_category, a.tot_campaign, a.target, a.eligible, a.taker, b.unique_target, b.unique_taker, 
a.revenue, a.recharge, a.control, a.taker_control, a.revenue_control, a.uplift, a.last_trx_date
from 
(
select periode, 1 eom, 'Total' brand, 'Overall' campaign_category,
count(distinct campaign_id) as tot_campaign,
sum(target) as target, 
sum(eligible) as eligible,
sum(taker) as taker,
sum(case when campaign_group_id in (1,4,6,7) then revenue_taker else 0 end) revenue,
sum(case when campaign_group_id = 2 then revenue_taker else 0 end) recharge,
sum(control) as control,
sum(taker_control) as taker_control,
sum(case when campaign_group_id in (1,4,6,7) then revenue_control else 0 end) revenue_control,
sum((((arpu_target_after - arpu_target_before) - (arpu_control_after - arpu_control_before))/(nullif(arpu_target_before,0) * 1.0)) * eligible)/nullif(sum(eligible),0) uplift, 
max(last_trx_date) as last_trx_date
from ${table-prefix}${cms-schema}.${table-prefix}cms_summary_bycampaignid
where campaign_group_id not in (11,12)
and date(periode) = date(datetime(last_day(date('${run-date=yyyy-MM-dd}') - interval '1' month)))
and eom = 1
group by 1
) a left join 
(
select date(datetime(last_day(trx_date))) as periode, 1 eom, 'Total' brand,'Overall' campaign_category,
count(distinct msisdn) unique_target,
count(distinct case when taker > 0 and iscontrolgroup = 'false' then msisdn end) unique_taker
from ${table-prefix}${cms-schema}.${table-prefix}cms_campaign_tracking_detail a 
where a.pipeline_step not in ('cms_usage_detail_insert', 'cms_campaign_digital_detail_insert')
and (trx_date between date(datetime(dateadd('day', 1, last_day(date('${run-date=yyyy-MM-dd}') - interval '2' month)))) 
and date(datetime(last_day(date('${run-date=yyyy-MM-dd}') - interval '1' month))))
and a.campaign_group_id not in (11,12)
group by 1
) b on a.periode = b.periode 
and a.eom = b.eom  
and a.brand = b.brand 
and a.campaign_category = b.campaign_category;


--- insert dahboard 1 summary for usage

insert into ${table-prefix}${cms-schema}.${table-prefix}cms_performance_summary
select a.periode, a.eom, a.brand, a.campaign_category, a.tot_campaign, a.target, a.eligible, a.taker, b.unique_target, b.unique_taker, 
a.revenue, a.recharge, a.control, a.taker_control, a.revenue_control, a.uplift, a.last_trx_date
from 
(
select periode, 1 eom, brand, 'Usage' campaign_category,
count(distinct campaign_id) as tot_campaign,
sum(target) as target, 
sum(eligible) as eligible,
sum(taker) as taker,
sum(case when campaign_group_id in (1,4,6,7) then revenue_taker else 0 end) revenue,
sum(case when campaign_group_id = 2 then revenue_taker else 0 end) recharge,
sum(control) as control,
sum(taker_control) as taker_control,
sum(case when campaign_group_id in (1,4,6,7) then revenue_control else 0 end) revenue_control,
sum((((arpu_target_after - arpu_target_before) - (arpu_control_after - arpu_control_before))/(nullif(arpu_target_before,0) * 1.0)) * eligible)/nullif(sum(eligible),0) uplift, 
max(last_trx_date) as last_trx_date
from ${table-prefix}${cms-schema}.${table-prefix}cms_summary_bycampaignid
where campaign_group_id in (1,4,6,7)
and date(periode) = date(datetime(last_day(date('${run-date=yyyy-MM-dd}') - interval '1' month)))
and eom = 1
group by 1,3 
) a left join 
(
select date(datetime(last_day(trx_date))) as periode, 1 eom, 
case
	when upper(substring(a.campaign_id, 11, 1)) = 'A' then 'Simpati'
	when upper(substring(a.campaign_id, 11, 1)) = 'B' then 'KartuAs'
	when upper(substring(a.campaign_id, 11, 1)) = 'C' then 'AllPrepaid'
	when upper(substring(a.campaign_id, 11, 1)) = 'D' then 'KartuHALO'
	when upper(substring(a.campaign_id, 11, 1)) = 'E' then 'AllBrands'
	when upper(substring(a.campaign_id, 11, 1)) = 'F' then 'Loop'
end as brand,'Usage' campaign_category,
count(distinct msisdn) unique_target,
count(distinct case when taker > 0 and iscontrolgroup = 'false' then msisdn end) unique_taker
from ${table-prefix}${cms-schema}.${table-prefix}cms_campaign_tracking_detail a 
where a.pipeline_step not in ('cms_usage_detail_insert', 'cms_campaign_digital_detail_insert')
and (trx_date between date(datetime(dateadd('day', 1, last_day(date('${run-date=yyyy-MM-dd}') - interval '2' month)))) 
and date(datetime(last_day(date('${run-date=yyyy-MM-dd}') - interval '1' month))))
and a.campaign_group_id in (1,4,6,7)
group by 1,3
) b on a.periode = b.periode 
and a.eom = b.eom  
and a.brand = b.brand 
and a.campaign_category = b.campaign_category;


insert into ${table-prefix}${cms-schema}.${table-prefix}cms_performance_summary
select a.periode, a.eom, a.brand, a.campaign_category, a.tot_campaign, a.target, a.eligible, a.taker, b.unique_target, b.unique_taker, 
a.revenue, a.recharge, a.control, a.taker_control, a.revenue_control, a.uplift, a.last_trx_date
from 
(
select periode, 1 eom, 'Total' brand, 'Usage' campaign_category,
count(distinct campaign_id) as tot_campaign,
sum(target) as target, 
sum(eligible) as eligible,
sum(taker) as taker,
sum(case when campaign_group_id in (1,4,6,7) then revenue_taker else 0 end) revenue,
sum(case when campaign_group_id = 2 then revenue_taker else 0 end) recharge,
sum(control) as control,
sum(taker_control) as taker_control,
sum(case when campaign_group_id in (1,4,6,7) then revenue_control else 0 end) revenue_control,
sum((((arpu_target_after - arpu_target_before) - (arpu_control_after - arpu_control_before))/(nullif(arpu_target_before,0) * 1.0)) * eligible)/nullif(sum(eligible),0) uplift, 
max(last_trx_date) as last_trx_date
from ${table-prefix}${cms-schema}.${table-prefix}cms_summary_bycampaignid
where campaign_group_id in (1,4,6,7)
and date(periode) = date(datetime(last_day(date('${run-date=yyyy-MM-dd}') - interval '1' month)))
and eom = 1
group by 1
) a left join 
(
select date(datetime(last_day(trx_date))) as periode, 1 eom, 'Total' brand,'Usage' campaign_category,
count(distinct msisdn) unique_target,
count(distinct case when taker > 0 and iscontrolgroup = 'false' then msisdn end) unique_taker
from ${table-prefix}${cms-schema}.${table-prefix}cms_campaign_tracking_detail a 
where a.pipeline_step not in ('cms_usage_detail_insert', 'cms_campaign_digital_detail_insert')
and (trx_date between date(datetime(dateadd('day', 1, last_day(date('${run-date=yyyy-MM-dd}') - interval '2' month)))) 
and date(datetime(last_day(date('${run-date=yyyy-MM-dd}') - interval '1' month))))
and a.campaign_group_id in (1,4,6,7)
group by 1
) b on a.periode = b.periode 
and a.eom = b.eom  
and a.brand = b.brand 
and a.campaign_category = b.campaign_category;


--- insert dahboard 1 summary for recharge

insert into ${table-prefix}${cms-schema}.${table-prefix}cms_performance_summary
select a.periode, a.eom, a.brand, a.campaign_category, a.tot_campaign, a.target, a.eligible, a.taker, b.unique_target, b.unique_taker, 
a.revenue, a.recharge, a.control, a.taker_control, a.revenue_control, a.uplift, a.last_trx_date
from 
(
select periode, 1 eom, brand, 'Recharge' campaign_category,
count(distinct campaign_id) as tot_campaign,
sum(target) as target, 
sum(eligible) as eligible,
sum(taker) as taker,
sum(case when campaign_group_id in (1,4,6,7) then revenue_taker else 0 end) revenue,
sum(case when campaign_group_id = 2 then revenue_taker else 0 end) recharge,
sum(control) as control,
sum(taker_control) as taker_control,
sum(case when campaign_group_id in (1,4,6,7) then revenue_control else 0 end) revenue_control,
sum((((arpu_target_after - arpu_target_before) - (arpu_control_after - arpu_control_before))/(nullif(arpu_target_before,0) * 1.0)) * eligible)/nullif(sum(eligible),0) uplift, 
max(last_trx_date) as last_trx_date
from ${table-prefix}${cms-schema}.${table-prefix}cms_summary_bycampaignid
where campaign_group_id = 2
and date(periode) = date(datetime(last_day(date('${run-date=yyyy-MM-dd}') - interval '1' month)))
and eom = 1
group by 1,3 
) a left join 
(
select date(datetime(last_day(trx_date))) as periode, 1 eom, 
case
	when upper(substring(a.campaign_id, 11, 1)) = 'A' then 'Simpati'
	when upper(substring(a.campaign_id, 11, 1)) = 'B' then 'KartuAs'
	when upper(substring(a.campaign_id, 11, 1)) = 'C' then 'AllPrepaid'
	when upper(substring(a.campaign_id, 11, 1)) = 'D' then 'KartuHALO'
	when upper(substring(a.campaign_id, 11, 1)) = 'E' then 'AllBrands'
	when upper(substring(a.campaign_id, 11, 1)) = 'F' then 'Loop'
end as brand,'Recharge' campaign_category,
count(distinct msisdn) unique_target,
count(distinct case when taker > 0 and iscontrolgroup = 'false' then msisdn end) unique_taker
from ${table-prefix}${cms-schema}.${table-prefix}cms_campaign_tracking_detail a 
where a.pipeline_step not in ('cms_usage_detail_insert', 'cms_campaign_digital_detail_insert')
and (trx_date between date(datetime(dateadd('day', 1, last_day(date('${run-date=yyyy-MM-dd}') - interval '2' month)))) 
and date(datetime(last_day(date('${run-date=yyyy-MM-dd}') - interval '1' month))))
and a.campaign_group_id = 2
group by 1,3
) b on a.periode = b.periode 
and a.eom = b.eom  
and a.brand = b.brand 
and a.campaign_category = b.campaign_category;


insert into ${table-prefix}${cms-schema}.${table-prefix}cms_performance_summary
select a.periode, a.eom, a.brand, a.campaign_category, a.tot_campaign, a.target, a.eligible, a.taker, b.unique_target, b.unique_taker, 
a.revenue, a.recharge, a.control, a.taker_control, a.revenue_control, a.uplift, a.last_trx_date
from 
(
select periode, 1 eom, 'Total' brand, 'Recharge' campaign_category,
count(distinct campaign_id) as tot_campaign,
sum(target) as target, 
sum(eligible) as eligible,
sum(taker) as taker,
sum(case when campaign_group_id in (1,4,6,7) then revenue_taker else 0 end) revenue,
sum(case when campaign_group_id = 2 then revenue_taker else 0 end) recharge,
sum(control) as control,
sum(taker_control) as taker_control,
sum(case when campaign_group_id in (1,4,6,7) then revenue_control else 0 end) revenue_control,
sum((((arpu_target_after - arpu_target_before) - (arpu_control_after - arpu_control_before))/(nullif(arpu_target_before,0) * 1.0)) * eligible)/nullif(sum(eligible),0) uplift, 
max(last_trx_date) as last_trx_date
from ${table-prefix}${cms-schema}.${table-prefix}cms_summary_bycampaignid
where campaign_group_id = 2
and date(periode) = date(datetime(last_day(date('${run-date=yyyy-MM-dd}') - interval '1' month)))
and eom = 1
group by 1
) a left join 
(
select date(datetime(last_day(trx_date))) as periode, 1 eom, 'Total' brand,'Recharge' campaign_category,
count(distinct msisdn) unique_target,
count(distinct case when taker > 0 and iscontrolgroup = 'false' then msisdn end) unique_taker
from ${table-prefix}${cms-schema}.${table-prefix}cms_campaign_tracking_detail a 
where a.pipeline_step not in ('cms_usage_detail_insert', 'cms_campaign_digital_detail_insert')
and (trx_date between date(datetime(dateadd('day', 1, last_day(date('${run-date=yyyy-MM-dd}') - interval '2' month)))) 
and date(datetime(last_day(date('${run-date=yyyy-MM-dd}') - interval '1' month))))
and a.campaign_group_id = 2
group by 1
) b on a.periode = b.periode 
and a.eom = b.eom  
and a.brand = b.brand 
and a.campaign_category = b.campaign_category;


--- update last 3 months data for running month 

delete from ${table-prefix}${cms-schema}.${table-prefix}cms_performance_summary
where eom = 0;


insert into ${table-prefix}${cms-schema}.${table-prefix}cms_performance_summary
select a.periode, a.eom, a.brand, a.campaign_category, a.tot_campaign, a.target, a.eligible, a.taker, b.unique_target, b.unique_taker, 
a.revenue, a.recharge, a.control, a.taker_control, a.revenue_control, a.uplift, a.last_trx_date
from 
(
select periode, eom, brand, 'Overall' campaign_category,
count(distinct campaign_id) as tot_campaign,
sum(target) as target, 
sum(eligible) as eligible,
sum(taker) as taker,
sum(case when campaign_group_id in (1,4,6,7) then revenue_taker else 0 end) revenue,
sum(case when campaign_group_id = 2 then revenue_taker else 0 end) recharge,
sum(control) as control,
sum(taker_control) as taker_control,
sum(case when campaign_group_id in (1,4,6,7) then revenue_control else 0 end) revenue_control,
sum((((arpu_target_after - arpu_target_before) - (arpu_control_after - arpu_control_before))/(nullif(arpu_target_before,0) * 1.0)) * eligible)/nullif(sum(eligible),0) uplift, 
max(last_trx_date) as last_trx_date
from ${table-prefix}${cms-schema}.${table-prefix}cms_summary_bycampaignid
where campaign_group_id not in (11,12)
and eom = 0 
group by 1,2,3 
) a left join 
(
select date(datetime(last_day(trx_date))) as periode, 0 eom, 
case
	when upper(substring(a.campaign_id, 11, 1)) = 'A' then 'Simpati'
	when upper(substring(a.campaign_id, 11, 1)) = 'B' then 'KartuAs'
	when upper(substring(a.campaign_id, 11, 1)) = 'C' then 'AllPrepaid'
	when upper(substring(a.campaign_id, 11, 1)) = 'D' then 'KartuHALO'
	when upper(substring(a.campaign_id, 11, 1)) = 'E' then 'AllBrands'
	when upper(substring(a.campaign_id, 11, 1)) = 'F' then 'Loop'
end as brand,'Overall' campaign_category,
count(distinct msisdn) unique_target,
count(distinct case when taker > 0 and iscontrolgroup = 'false' then msisdn end) unique_taker
from ${table-prefix}${cms-schema}.${table-prefix}cms_campaign_tracking_detail a 
where a.pipeline_step not in ('cms_usage_detail_insert', 'cms_campaign_digital_detail_insert')
and (
   	(trx_date between date(datetime(dateadd('day', 1, last_day(date('${run-date=yyyy-MM-dd}') - interval '3' month)))) and date(date('${run-date=yyyy-MM-dd}') - interval '2' month))
	or (trx_date between date(datetime(dateadd('day', 1, last_day(date('${run-date=yyyy-MM-dd}') - interval '2' month)))) and date(date('${run-date=yyyy-MM-dd}') - interval '1' month))
	or (trx_date between date(datetime(dateadd('day', 1, last_day(date('${run-date=yyyy-MM-dd}') - interval '1' month)))) and date(date('${run-date=yyyy-MM-dd}')))
)
and a.campaign_group_id not in (11,12)
group by 1,3
) b on a.periode = b.periode 
and a.eom = b.eom  
and a.brand = b.brand 
and a.campaign_category = b.campaign_category;

insert into ${table-prefix}${cms-schema}.${table-prefix}cms_performance_summary
select a.periode, a.eom, a.brand, a.campaign_category, a.tot_campaign, a.target, a.eligible, a.taker, b.unique_target, b.unique_taker, 
a.revenue, a.recharge, a.control, a.taker_control, a.revenue_control, a.uplift, a.last_trx_date
from 
(
select periode, eom, 'Total' brand, 'Overall' campaign_category,
count(distinct campaign_id) as tot_campaign,
sum(target) as target, 
sum(eligible) as eligible,
sum(taker) as taker,
sum(case when campaign_group_id in (1,4,6,7) then revenue_taker else 0 end) revenue,
sum(case when campaign_group_id = 2 then revenue_taker else 0 end) recharge,
sum(control) as control,
sum(taker_control) as taker_control,
sum(case when campaign_group_id in (1,4,6,7) then revenue_control else 0 end) revenue_control,
sum((((arpu_target_after - arpu_target_before) - (arpu_control_after - arpu_control_before))/(nullif(arpu_target_before,0) * 1.0)) * eligible)/nullif(sum(eligible),0) uplift, 
max(last_trx_date) as last_trx_date
from ${table-prefix}${cms-schema}.${table-prefix}cms_summary_bycampaignid
where campaign_group_id not in (11,12)
and eom = 0
group by 1,2
) a left join 
(
select date(datetime(last_day(trx_date))) as periode, 0 eom, 'Total' brand,'Overall' campaign_category,
count(distinct msisdn) unique_target,
count(distinct case when taker > 0 and iscontrolgroup = 'false' then msisdn end) unique_taker
from ${table-prefix}${cms-schema}.${table-prefix}cms_campaign_tracking_detail a 
where a.pipeline_step not in ('cms_usage_detail_insert', 'cms_campaign_digital_detail_insert')
and (
   	(trx_date between date(datetime(dateadd('day', 1, last_day(date('${run-date=yyyy-MM-dd}') - interval '3' month)))) and date(date('${run-date=yyyy-MM-dd}') - interval '2' month))
	or (trx_date between date(datetime(dateadd('day', 1, last_day(date('${run-date=yyyy-MM-dd}') - interval '2' month)))) and date(date('${run-date=yyyy-MM-dd}') - interval '1' month))
	or (trx_date between date(datetime(dateadd('day', 1, last_day(date('${run-date=yyyy-MM-dd}') - interval '1' month)))) and date(date('${run-date=yyyy-MM-dd}')))
)
and a.campaign_group_id not in (11,12)
group by 1
) b on a.periode = b.periode 
and a.eom = b.eom  
and a.brand = b.brand 
and a.campaign_category = b.campaign_category;


--- insert dahboard 1 summary for Usage eom 

insert into ${table-prefix}${cms-schema}.${table-prefix}cms_performance_summary
select a.periode, a.eom, a.brand, a.campaign_category, a.tot_campaign, a.target, a.eligible, a.taker, b.unique_target, b.unique_taker, 
a.revenue, a.recharge, a.control, a.taker_control, a.revenue_control, a.uplift, a.last_trx_date
from 
(
select periode, eom, brand, 'Usage' campaign_category,
count(distinct campaign_id) as tot_campaign,
sum(target) as target, 
sum(eligible) as eligible,
sum(taker) as taker,
sum(case when campaign_group_id in (1,4,6,7) then revenue_taker else 0 end) revenue,
sum(case when campaign_group_id = 2 then revenue_taker else 0 end) recharge,
sum(control) as control,
sum(taker_control) as taker_control,
sum(case when campaign_group_id in (1,4,6,7) then revenue_control else 0 end) revenue_control,
sum((((arpu_target_after - arpu_target_before) - (arpu_control_after - arpu_control_before))/(nullif(arpu_target_before,0) * 1.0)) * eligible)/nullif(sum(eligible),0) uplift, 
max(last_trx_date) as last_trx_date
from ${table-prefix}${cms-schema}.${table-prefix}cms_summary_bycampaignid
where campaign_group_id in (1,4,6,7)
and eom = 0 
group by 1,2,3 
) a left join 
(
select date(datetime(last_day(trx_date))) as periode, 0 eom, 
case
	when upper(substring(a.campaign_id, 11, 1)) = 'A' then 'Simpati'
	when upper(substring(a.campaign_id, 11, 1)) = 'B' then 'KartuAs'
	when upper(substring(a.campaign_id, 11, 1)) = 'C' then 'AllPrepaid'
	when upper(substring(a.campaign_id, 11, 1)) = 'D' then 'KartuHALO'
	when upper(substring(a.campaign_id, 11, 1)) = 'E' then 'AllBrands'
	when upper(substring(a.campaign_id, 11, 1)) = 'F' then 'Loop'
end as brand,'Usage' campaign_category,
count(distinct msisdn) unique_target,
count(distinct case when taker > 0 and iscontrolgroup = 'false' then msisdn end) unique_taker
from ${table-prefix}${cms-schema}.${table-prefix}cms_campaign_tracking_detail a 
where a.pipeline_step not in ('cms_usage_detail_insert', 'cms_campaign_digital_detail_insert')
and (
   	(trx_date between date(datetime(dateadd('day', 1, last_day(date('${run-date=yyyy-MM-dd}') - interval '3' month)))) and date(date('${run-date=yyyy-MM-dd}') - interval '2' month))
	or (trx_date between date(datetime(dateadd('day', 1, last_day(date('${run-date=yyyy-MM-dd}') - interval '2' month)))) and date(date('${run-date=yyyy-MM-dd}') - interval '1' month))
	or (trx_date between date(datetime(dateadd('day', 1, last_day(date('${run-date=yyyy-MM-dd}') - interval '1' month)))) and date(date('${run-date=yyyy-MM-dd}')))
)
and a.campaign_group_id in (1,4,6,7)
group by 1,3
) b on a.periode = b.periode 
and a.eom = b.eom  
and a.brand = b.brand 
and a.campaign_category = b.campaign_category;

insert into ${table-prefix}${cms-schema}.${table-prefix}cms_performance_summary
select a.periode, a.eom, a.brand, a.campaign_category, a.tot_campaign, a.target, a.eligible, a.taker, b.unique_target, b.unique_taker, 
a.revenue, a.recharge, a.control, a.taker_control, a.revenue_control, a.uplift, a.last_trx_date
from 
(
select periode, eom, 'Total' brand, 'Usage' campaign_category,
count(distinct campaign_id) as tot_campaign,
sum(target) as target, 
sum(eligible) as eligible,
sum(taker) as taker,
sum(case when campaign_group_id in (1,4,6,7) then revenue_taker else 0 end) revenue,
sum(case when campaign_group_id = 2 then revenue_taker else 0 end) recharge,
sum(control) as control,
sum(taker_control) as taker_control,
sum(case when campaign_group_id in (1,4,6,7) then revenue_control else 0 end) revenue_control,
sum((((arpu_target_after - arpu_target_before) - (arpu_control_after - arpu_control_before))/(nullif(arpu_target_before,0) * 1.0)) * eligible)/nullif(sum(eligible),0) uplift, 
max(last_trx_date) as last_trx_date
from ${table-prefix}${cms-schema}.${table-prefix}cms_summary_bycampaignid
where campaign_group_id in (1,4,6,7)
and eom = 0
group by 1,2
) a left join 
(
select date(datetime(last_day(trx_date))) as periode, 0 eom, 'Total' brand,'Usage' campaign_category,
count(distinct msisdn) unique_target,
count(distinct case when taker > 0 and iscontrolgroup = 'false' then msisdn end) unique_taker
from ${table-prefix}${cms-schema}.${table-prefix}cms_campaign_tracking_detail a 
where a.pipeline_step not in ('cms_usage_detail_insert', 'cms_campaign_digital_detail_insert')
and (
   	(trx_date between date(datetime(dateadd('day', 1, last_day(date('${run-date=yyyy-MM-dd}') - interval '3' month)))) and date(date('${run-date=yyyy-MM-dd}') - interval '2' month))
	or (trx_date between date(datetime(dateadd('day', 1, last_day(date('${run-date=yyyy-MM-dd}') - interval '2' month)))) and date(date('${run-date=yyyy-MM-dd}') - interval '1' month))
	or (trx_date between date(datetime(dateadd('day', 1, last_day(date('${run-date=yyyy-MM-dd}') - interval '1' month)))) and date(date('${run-date=yyyy-MM-dd}')))
)
and a.campaign_group_id in (1,4,6,7)
group by 1
) b on a.periode = b.periode 
and a.eom = b.eom  
and a.brand = b.brand 
and a.campaign_category = b.campaign_category;


--- insert dahboard 1 summary for Recharge eom 

insert into ${table-prefix}${cms-schema}.${table-prefix}cms_performance_summary
select a.periode, a.eom, a.brand, a.campaign_category, a.tot_campaign, a.target, a.eligible, a.taker, b.unique_target, b.unique_taker, 
a.revenue, a.recharge, a.control, a.taker_control, a.revenue_control, a.uplift, a.last_trx_date
from 
(
select periode, eom, brand, 'Recharge' campaign_category,
count(distinct campaign_id) as tot_campaign,
sum(target) as target, 
sum(eligible) as eligible,
sum(taker) as taker,
sum(case when campaign_group_id in (1,4,6,7) then revenue_taker else 0 end) revenue,
sum(case when campaign_group_id = 2 then revenue_taker else 0 end) recharge,
sum(control) as control,
sum(taker_control) as taker_control,
sum(case when campaign_group_id in (1,4,6,7) then revenue_control else 0 end) revenue_control,
sum((((arpu_target_after - arpu_target_before) - (arpu_control_after - arpu_control_before))/(nullif(arpu_target_before,0) * 1.0)) * eligible)/nullif(sum(eligible),0) uplift, 
max(last_trx_date) as last_trx_date
from ${table-prefix}${cms-schema}.${table-prefix}cms_summary_bycampaignid
where campaign_group_id = 2
and eom = 0 
group by 1,2,3 
) a left join 
(
select date(datetime(last_day(trx_date))) as periode, 0 eom, 
case
	when upper(substring(a.campaign_id, 11, 1)) = 'A' then 'Simpati'
	when upper(substring(a.campaign_id, 11, 1)) = 'B' then 'KartuAs'
	when upper(substring(a.campaign_id, 11, 1)) = 'C' then 'AllPrepaid'
	when upper(substring(a.campaign_id, 11, 1)) = 'D' then 'KartuHALO'
	when upper(substring(a.campaign_id, 11, 1)) = 'E' then 'AllBrands'
	when upper(substring(a.campaign_id, 11, 1)) = 'F' then 'Loop'
end as brand,'Recharge' campaign_category,
count(distinct msisdn) unique_target,
count(distinct case when taker > 0 and iscontrolgroup = 'false' then msisdn end) unique_taker
from ${table-prefix}${cms-schema}.${table-prefix}cms_campaign_tracking_detail a 
where a.pipeline_step not in ('cms_usage_detail_insert', 'cms_campaign_digital_detail_insert')
and (
   	(trx_date between date(datetime(dateadd('day', 1, last_day(date('${run-date=yyyy-MM-dd}') - interval '3' month)))) and date(date('${run-date=yyyy-MM-dd}') - interval '2' month))
	or (trx_date between date(datetime(dateadd('day', 1, last_day(date('${run-date=yyyy-MM-dd}') - interval '2' month)))) and date(date('${run-date=yyyy-MM-dd}') - interval '1' month))
	or (trx_date between date(datetime(dateadd('day', 1, last_day(date('${run-date=yyyy-MM-dd}') - interval '1' month)))) and date(date('${run-date=yyyy-MM-dd}')))
)
and a.campaign_group_id = 2
group by 1,3
) b on a.periode = b.periode 
and a.eom = b.eom  
and a.brand = b.brand 
and a.campaign_category = b.campaign_category;

insert into ${table-prefix}${cms-schema}.${table-prefix}cms_performance_summary
select a.periode, a.eom, a.brand, a.campaign_category, a.tot_campaign, a.target, a.eligible, a.taker, b.unique_target, b.unique_taker, 
a.revenue, a.recharge, a.control, a.taker_control, a.revenue_control, a.uplift, a.last_trx_date
from 
(
select periode, eom, 'Total' brand, 'Recharge' campaign_category,
count(distinct campaign_id) as tot_campaign,
sum(target) as target, 
sum(eligible) as eligible,
sum(taker) as taker,
sum(case when campaign_group_id in (1,4,6,7) then revenue_taker else 0 end) revenue,
sum(case when campaign_group_id = 2 then revenue_taker else 0 end) recharge,
sum(control) as control,
sum(taker_control) as taker_control,
sum(case when campaign_group_id in (1,4,6,7) then revenue_control else 0 end) revenue_control,
sum((((arpu_target_after - arpu_target_before) - (arpu_control_after - arpu_control_before))/(nullif(arpu_target_before,0) * 1.0)) * eligible)/nullif(sum(eligible),0) uplift, 
max(last_trx_date) as last_trx_date
from ${table-prefix}${cms-schema}.${table-prefix}cms_summary_bycampaignid
where campaign_group_id = 2
and eom = 0
group by 1,2
) a left join 
(
select date(datetime(last_day(trx_date))) as periode, 0 eom, 'Total' brand,'Recharge' campaign_category,
count(distinct msisdn) unique_target,
count(distinct case when taker > 0 and iscontrolgroup = 'false' then msisdn end) unique_taker
from ${table-prefix}${cms-schema}.${table-prefix}cms_campaign_tracking_detail a 
where a.pipeline_step not in ('cms_usage_detail_insert', 'cms_campaign_digital_detail_insert')
and (
   	(trx_date between date(datetime(dateadd('day', 1, last_day(date('${run-date=yyyy-MM-dd}') - interval '3' month)))) and date(date('${run-date=yyyy-MM-dd}') - interval '2' month))
	or (trx_date between date(datetime(dateadd('day', 1, last_day(date('${run-date=yyyy-MM-dd}') - interval '2' month)))) and date(date('${run-date=yyyy-MM-dd}') - interval '1' month))
	or (trx_date between date(datetime(dateadd('day', 1, last_day(date('${run-date=yyyy-MM-dd}') - interval '1' month)))) and date(date('${run-date=yyyy-MM-dd}')))
)
and a.campaign_group_id = 2
group by 1
) b on a.periode = b.periode 
and a.eom = b.eom  
and a.brand = b.brand 
and a.campaign_category = b.campaign_category;





