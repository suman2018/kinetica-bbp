create or replace table ${table-prefix}${cms-schema}.${table-prefix}cms_brand
as
select
	distinct brand
from ${table-prefix}${cms-schema}.${table-prefix}cms_summary_bycampaignid
union
select
	'SELECT ALL BRANDS' as category
;

create or replace materialized view ${table-prefix}${cms_out-schema}.${table-prefix}cms_brand_v
refresh every 24 hour
as
select * from ${table-prefix}${cms-schema}.${table-prefix}cms_brand
;


create or replace table ${table-prefix}${cms-schema}.${table-prefix}cms_area
as
select
	distinct area
from ${table-prefix}${cms-schema}.${table-prefix}cms_summary_bycampaignid
union
select
	'SELECT ALL AREAS' as category
;


create or replace materialized view ${table-prefix}${cms_out-schema}.${table-prefix}cms_area_v
refresh every 24 hour
as
select * from ${table-prefix}${cms-schema}.${table-prefix}cms_area
;


create or replace table ${table-prefix}${cms-schema}.${table-prefix}cms_region
as
select
	distinct initiated_business
from ${table-prefix}${cms-schema}.${table-prefix}cms_summary_bycampaignid
union
select
	'SELECT ALL REGIONS' as category
;


create or replace materialized view ${table-prefix}${cms_out-schema}.${table-prefix}cms_region_v
refresh every 24 hour
as
select * from ${table-prefix}${cms-schema}.${table-prefix}cms_region
;

create or replace table ${table-prefix}${cms-schema}.${table-prefix}cms_theme
as
select
	distinct campaign_objective_desc as theme
from ${table-prefix}${cms-schema}.${table-prefix}cms_campaign_objective
union
select
	'SELECT ALL THEMES' as category
;

create or replace materialized view ${table-prefix}${cms_out-schema}.${table-prefix}cms_theme_v
refresh every 24 hour
as
select
	theme
from ${table-prefix}${cms-schema}.${table-prefix}cms_theme
;



create or replace table ${table-prefix}${cms-schema}.${table-prefix}cms_category
as
select
	distinct category
from ${table-prefix}${cms-schema}.${table-prefix}cms_summary_bycampaignid
union
select
	'SELECT ALL CATEGORIES' as category
;


create or replace materialized view ${table-prefix}${cms_out-schema}.${table-prefix}cms_category_v
refresh every 24 hour
as
select * from ${table-prefix}${cms-schema}.${table-prefix}cms_category
;


create or replace materialized view ${table-prefix}${cms_out-schema}.${table-prefix}cms_cluster_geo_v
refresh every 24 hour
as
select cluster
, wkt
from utils.cluster
;

create or replace materialized view ${table-prefix}${cms_out-schema}.${table-prefix}cms_cluster_v
refresh every 24 hour
as
select distinct cluster
from (
   select cluster, wkt from utils.cluster
)
union
select 
	'ALL CLUSTERS' as cluster
;



create or replace table ${table-prefix}${cms-schema}.${table-prefix}cms_periode
as
select distinct periode
from ${table-prefix}${cms-schema}.${table-prefix}cms_summary_bycampaignid
;


---- exclude this view due make query cache error ------
--create or replace materialized view ${table-prefix}${cms_out-schema}.${table-prefix}cms_periode_v
--refresh every 24 hour
--as
--select * from ${table-prefix}${cms-schema}.${table-prefix}cms_periode
--;

create or replace materialized view ${table-prefix}${cms_out-schema}.${table-prefix}cms_wording_v
refresh every 24 hour
as
select campaign_id, wording
from ${table-prefix}${cms-schema}.${table-prefix}cms_campaign_sender
;

alter schema ${table-prefix}cms_out set protected true
;
