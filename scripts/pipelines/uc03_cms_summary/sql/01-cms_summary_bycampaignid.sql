--- summary per campaign_id
--- update last month data

delete from ${table-prefix}${cms-schema}.${table-prefix}cms_summary_bycampaignid
where date(periode) = date(datetime(last_day(date('${run-date=yyyy-MM-dd}') - interval '1' month)));

insert into ${table-prefix}${cms-schema}.${table-prefix}cms_summary_bycampaignid
select 
	main.periode,
	1 eom,
	main.category, 
	main.campaign_type,
	main.campaign_id,
	main.campaign_objective_id,
	main.campaign_objective_desc,
	main.campaign_name,
	date(main.start_period) as start_period,
	date(main.end_period) as end_period,
	main.campaign_group_id,
	main.business_group,
	main.initiated_group as initiated_business,
	main.area, 
	main.is_btl,
	main.brand,
	case 
		when main.max_trx_date < date(main.start_period) then 'NOT YET STARTED'
		when main.max_trx_date >= date(main.start_period) and main.max_trx_date < date(main.end_period) then 'IN PROGRESS'
		else 'COMPLETED'
	end campaign_status,
	main.target,
	main.eligible,
	main.taker,
	main.unique_taker,
	main.revenue_taker,
	main.control,
	main.taker_control,
	main.unique_taker_control,
	main.revenue_control,
	rev.revenue_target_before/nullif(main.eligible,0) as arpu_target_before,
	rev.revenue_target_after/nullif(main.eligible,0) as arpu_target_after,
	rev.revenue_control_before/nullif(main.control,0) as arpu_control_before,
	rev.revenue_control_after/nullif(main.control,0) as arpu_control_after,
	main.max_trx_date 
from 
(
select 
	last_day(a.trx_date) as periode,
	a.category,
	a.campaign_type,
	a.campaign_id,
	a.campaign_objective_id,
	c.campaign_objective_desc,
	a.campaign_name, 
	a.start_period,
	a.end_period,	
	a.campaign_group_id,
	a.business_group,
	a.initiated_group,
	case
                when a.initiated_group = '01. Area Sumatra'         then 'AREA 1'
                when a.initiated_group = '02. Sumbagut'             then 'AREA 1'
                when a.initiated_group = '03. Sumbagteng'           then 'AREA 1'
                when a.initiated_group = '04. Sumbagsel'            then 'AREA 1'
                when a.initiated_group = '05. Area Jabotabek Jabar' then 'AREA 2'
                when a.initiated_group = '06. Jabotabek West'       then 'AREA 2'
                when a.initiated_group = '07. Jabotabek Central'    then 'AREA 2'
                when a.initiated_group = '08. Jabotabek East'       then 'AREA 2'
                when a.initiated_group = '09. Jabar'                then 'AREA 2'
                when a.initiated_group = '10. Area Jawa Bali'       then 'AREA 3'
                when a.initiated_group = '11. Jateng'               then 'AREA 3'
                when a.initiated_group = '12. Jatim'                then 'AREA 3'
                when a.initiated_group = '13. Balinusra'            then 'AREA 3'
                when a.initiated_group = '14. Area Pamasuka'        then 'AREA 4'
                when a.initiated_group = '15. Kalimantan'           then 'AREA 4'
                when a.initiated_group = '16. Sulawesi'             then 'AREA 4'
                when a.initiated_group = '17. Puma'                 then 'AREA 4'
                when a.initiated_group = '18. HQ'                   then 'HQ'
	end as area,
	a.is_btl,
	case
		when upper(substring(a.campaign_id, 11, 1)) = 'A' then 'Simpati'
		when upper(substring(a.campaign_id, 11, 1)) = 'B' then 'KartuAs'
		when upper(substring(a.campaign_id, 11, 1)) = 'C' then 'AllPrepaid'
		when upper(substring(a.campaign_id, 11, 1)) = 'D' then 'KartuHALO'
		when upper(substring(a.campaign_id, 11, 1)) = 'E' then 'AllBrands'
		when upper(substring(a.campaign_id, 11, 1)) = 'F' then 'Loop'
	end as brand,
	count(distinct a.msisdn) as target,
	count(distinct case when a.iscontrolgroup = 'false' and a.eligible > 0 then a.msisdn end) as eligible,
	sum
		(
	    	case
	    		when a.iscontrolgroup = 'false' and a.campaign_group_id = 2 and a.taker > 0 then a.metric_2
	    		when a.iscontrolgroup = 'false' and a.campaign_group_id <> 2 and a.taker > 0 then a.taker
	    		else 0 
	    	end
	    ) as taker,
	count(distinct case when a.iscontrolgroup = 'false' and a.taker > 0 then a.msisdn end) as unique_taker,
	sum(case when a.iscontrolgroup = 'false' and a.taker > 0 then a.metric_1 else 0 end) as revenue_taker,
	count(distinct case when a.iscontrolgroup = 'true' and a.eligible > 0 then a.msisdn end) as control,
	sum(case when a.iscontrolgroup = 'true' and a.taker > 0 then a.taker else 0 end) as taker_control,
	count(distinct case when a.iscontrolgroup = 'true' and a.taker > 0 then a.msisdn end) as unique_taker_control,
	sum(case when a.iscontrolgroup = 'true' and a.taker > 0 then a.metric_1 else 0 end) as revenue_control,
	max(a.trx_date) as max_trx_date
from ${table-prefix}${cms-schema}.${table-prefix}cms_campaign_tracking_detail a 
left join 
(
	select distinct campaign_objective_id, campaign_objective_desc
	from ${table-prefix}${cms-schema}.${table-prefix}cms_campaign_objective
) c on a.campaign_objective_id = c.campaign_objective_id
where a.pipeline_step not in ('cms_usage_detail_insert', 'cms_campaign_digital_detail_insert')
and (
 	(trx_date) between date(datetime(dateadd('day', 1, last_day(date('${run-date=yyyy-MM-dd}') - interval '2' month)))) 
         and date(datetime(last_day(date('${run-date=yyyy-MM-dd}') - interval '1' month)))
)
group by 1,2,3,4,5,6,7,8,9,10,11,12,13,14,15
) main 
left join
(
	select 
		perf.campaign_id
		,sum(case when datediff(perf.start_period,chg.trx_date) >  0 and iscontrolgroup = 'false' then chg.rev else 0 end) as revenue_target_before
		,sum(case when datediff(chg.trx_date,perf.start_period) >= 0 and iscontrolgroup = 'false' then chg.rev else 0 end) as revenue_target_after
		,sum(case when datediff(perf.start_period,chg.trx_date) >  0 and iscontrolgroup = 'true' then chg.rev else 0 end)  as revenue_control_before
		,sum(case when datediff(chg.trx_date,perf.start_period) >= 0 and iscontrolgroup = 'true' then chg.rev else 0 end)  as revenue_control_after
	from
	(
	    select distinct campaign_id, msisdn, start_period, end_period, periode, iscontrolgroup, case when diff < 30 then diff else 30 end as "offset"  
	    from 
	    (
	    	select campaign_id, msisdn, start_period, end_period, last_day(trx_date) periode, iscontrolgroup, 
	    	datediff('day', date('${run-date=yyyy-MM-dd}'), date(start_period)) as diff
	    	from ${table-prefix}${cms-schema}.${table-prefix}cms_campaign_tracking_detail
	    	where eligible > 0	
	    		and pipeline_step not in ('cms_usage_detail_insert', 'cms_campaign_digital_detail_insert')
				and (
   				(
   			          (trx_date) between date(datetime(dateadd('day',1,last_day(date('${run-date=yyyy-MM-dd}') - interval '2' month)))) 
                                  and date(datetime(last_day(date('${run-date=yyyy-MM-dd}') - interval '1' month)))
				)
	    		and (
	    		last_day(date(start_period)) = last_day(trx_date)
				or last_day(date(end_period)) = last_day(trx_date))
	    		)
	    )
	) perf
		left join ${table-prefix}${cms-schema}.${table-prefix}cms_chg_detail chg
			on chg.msisdn = perf.msisdn
			and chg.trx_date between dateadd('day', -abs(perf."offset") -1 , date(start_period)) and dateadd('day', abs(perf."offset"),date(start_period))
	group by 1
)rev
on main.campaign_id = rev.campaign_id;


--- summary per campaign_id
--- update last 3 months data: for running month and last 2 months with same date period


delete from ${table-prefix}${cms-schema}.${table-prefix}cms_summary_bycampaignid
where eom = 0;

insert into ${table-prefix}${cms-schema}.${table-prefix}cms_summary_bycampaignid
select 
	main.periode,
	0 eom,
	main.category, 
	main.campaign_type,
	main.campaign_id,
	main.campaign_objective_id,
	main.campaign_objective_desc,
	main.campaign_name,
	date(main.start_period) as start_period,
	date(main.end_period) as end_period,
	main.campaign_group_id,
	main.business_group,
	main.initiated_group as initiated_business,
	main.area, 
	main.is_btl,
	main.brand,
	case 
		when main.max_trx_date < date(main.start_period) then 'NOT YET STARTED'
		when main.max_trx_date >= date(main.start_period) and main.max_trx_date < date(main.end_period) then 'IN PROGRESS'
		else 'COMPLETED'
	end campaign_status,
	main.target,
	main.eligible,
	main.taker,
	main.unique_taker,
	main.revenue_taker,
	main.control,
	main.taker_control,
	main.unique_taker_control,
	main.revenue_control,
	rev.revenue_target_before/nullif(main.eligible,0) as arpu_target_before,
	rev.revenue_target_after/nullif(main.eligible,0) as arpu_target_after,
	rev.revenue_control_before/nullif(main.control,0) as arpu_control_before,
	rev.revenue_control_after/nullif(main.control,0) as arpu_control_after,
	main.max_trx_date 
from 
(
select 
	last_day(a.trx_date) as periode,
	a.category,
	a.campaign_type,
	a.campaign_id,
	a.campaign_objective_id,
	c.campaign_objective_desc,
	a.campaign_name, 
	a.start_period,
	a.end_period,	
	a.campaign_group_id,
	a.business_group,
	a.initiated_group,
	case
                when a.initiated_group = '01. Area Sumatra'         then 'AREA 1'
                when a.initiated_group = '02. Sumbagut'             then 'AREA 1'
                when a.initiated_group = '03. Sumbagteng'           then 'AREA 1'
                when a.initiated_group = '04. Sumbagsel'            then 'AREA 1'
                when a.initiated_group = '05. Area Jabotabek Jabar' then 'AREA 2'
                when a.initiated_group = '06. Jabotabek West'       then 'AREA 2'
                when a.initiated_group = '07. Jabotabek Central'    then 'AREA 2'
                when a.initiated_group = '08. Jabotabek East'       then 'AREA 2'
                when a.initiated_group = '09. Jabar'                then 'AREA 2'
                when a.initiated_group = '10. Area Jawa Bali'       then 'AREA 3'
                when a.initiated_group = '11. Jateng'               then 'AREA 3'
                when a.initiated_group = '12. Jatim'                then 'AREA 3'
                when a.initiated_group = '13. Balinusra'            then 'AREA 3'
                when a.initiated_group = '14. Area Pamasuka'        then 'AREA 4'
                when a.initiated_group = '15. Kalimantan'           then 'AREA 4'
                when a.initiated_group = '16. Sulawesi'             then 'AREA 4'
                when a.initiated_group = '17. Puma'                 then 'AREA 4'
                when a.initiated_group = '18. HQ'                   then 'HQ'
	end as area,
	a.is_btl,
	case
		when upper(substring(a.campaign_id, 11, 1)) = 'A' then 'Simpati'
		when upper(substring(a.campaign_id, 11, 1)) = 'B' then 'KartuAs'
		when upper(substring(a.campaign_id, 11, 1)) = 'C' then 'AllPrepaid'
		when upper(substring(a.campaign_id, 11, 1)) = 'D' then 'KartuHALO'
		when upper(substring(a.campaign_id, 11, 1)) = 'E' then 'AllBrands'
		when upper(substring(a.campaign_id, 11, 1)) = 'F' then 'Loop'
	end as brand,
	count(distinct a.msisdn) as target,
	count(distinct case when a.iscontrolgroup = 'false' and a.eligible > 0 then a.msisdn end) as eligible,
	sum
		(
	    	case
	    		when a.iscontrolgroup = 'false' and a.campaign_group_id = 2 and a.taker > 0 then a.metric_2
	    		when a.iscontrolgroup = 'false' and a.campaign_group_id <> 2 and a.taker > 0 then a.taker
	    		else 0 
	    	end
	    ) as taker,
	count(distinct case when a.iscontrolgroup = 'false' and a.taker > 0 then a.msisdn end) as unique_taker,
	sum(case when a.iscontrolgroup = 'false' and a.taker > 0 then a.metric_1 else 0 end) as revenue_taker,
	count(distinct case when a.iscontrolgroup = 'true' and a.eligible > 0 then a.msisdn end) as control,
	sum(case when a.iscontrolgroup = 'true' and a.taker > 0 then a.taker else 0 end) as taker_control,
	count(distinct case when a.iscontrolgroup = 'true' and a.taker > 0 then a.msisdn end) as unique_taker_control,
	sum(case when a.iscontrolgroup = 'true' and a.taker > 0 then a.metric_1 else 0 end) as revenue_control,
	max(a.trx_date) as max_trx_date
from ${table-prefix}${cms-schema}.${table-prefix}cms_campaign_tracking_detail a 
left join 
(
	select distinct campaign_objective_id, campaign_objective_desc
	from ${table-prefix}${cms-schema}.${table-prefix}cms_campaign_objective
) c on a.campaign_objective_id = c.campaign_objective_id
where a.pipeline_step not in ('cms_usage_detail_insert', 'cms_campaign_digital_detail_insert')
and (
   	   (trx_date between date(datetime(dateadd('day', 1, last_day(date('${run-date=yyyy-MM-dd}') - interval '3' month)))) and date(date('${run-date=yyyy-MM-dd}') - interval '2' month))
	or (trx_date between date(datetime(dateadd('day', 1, last_day(date('${run-date=yyyy-MM-dd}') - interval '2' month)))) and date(date('${run-date=yyyy-MM-dd}') - interval '1' month))
	or (trx_date between date(datetime(dateadd('day', 1, last_day(date('${run-date=yyyy-MM-dd}') - interval '1' month)))) and date(date('${run-date=yyyy-MM-dd}')))
)
group by 1,2,3,4,5,6,7,8,9,10,11,12,13,14,15
) main 
left join
(
	select 
		perf.campaign_id
		,sum(case when datediff(perf.start_period,chg.trx_date) >  0 and iscontrolgroup = 'false' then chg.rev else 0 end) as revenue_target_before
		,sum(case when datediff(chg.trx_date,perf.start_period) >= 0 and iscontrolgroup = 'false' then chg.rev else 0 end) as revenue_target_after
		,sum(case when datediff(perf.start_period,chg.trx_date) >  0 and iscontrolgroup = 'true' then chg.rev else 0 end)  as revenue_control_before
		,sum(case when datediff(chg.trx_date,perf.start_period) >= 0 and iscontrolgroup = 'true' then chg.rev else 0 end)  as revenue_control_after
	from
	(
	    select distinct campaign_id, msisdn, start_period, end_period, periode, iscontrolgroup, case when diff < 30 then diff else 30 end as "offset"  
	    from 
	    (
	    	select campaign_id, msisdn, start_period, end_period, last_day(trx_date) periode, iscontrolgroup, 
	    	datediff('day', date('${run-date=yyyy-MM-dd}'), date(start_period)) as diff
	    	from ${table-prefix}${cms-schema}.${table-prefix}cms_campaign_tracking_detail
	    	where eligible > 0	
	    		and pipeline_step not in ('cms_usage_detail_insert', 'cms_campaign_digital_detail_insert')
				and (
   				   (trx_date between date(datetime(dateadd('day', 1, last_day(date('${run-date=yyyy-MM-dd}') - interval '3' month)))) and date(date('${run-date=yyyy-MM-dd}') - interval '2' month))
				or (trx_date between date(datetime(dateadd('day', 1, last_day(date('${run-date=yyyy-MM-dd}') - interval '2' month)))) and date(date('${run-date=yyyy-MM-dd}') - interval '1' month))
				or (trx_date between date(datetime(dateadd('day', 1, last_day(date('${run-date=yyyy-MM-dd}') - interval '1' month)))) and date(date('${run-date=yyyy-MM-dd}')))
				)
	    		and (
	    		last_day(date(start_period)) = last_day(trx_date)
				or last_day(date(end_period)) = last_day(trx_date))
	    )
	) perf
	left join ${table-prefix}${cms-schema}.${table-prefix}cms_chg_detail chg
	on chg.msisdn = perf.msisdn
            and chg.trx_date between dateadd('day',-abs(perf."offset")-1,date(start_period)) and dateadd('day',abs(perf."offset"),date(start_period))
	group by 1
) rev
on main.campaign_id = rev.campaign_id ;

