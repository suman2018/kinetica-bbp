
--- update last month data

delete from ${table-prefix}${cms-schema}.${table-prefix}cms_performance_campaignid
where date(periode) = date(datetime(last_day(date('${run-date=yyyy-MM-dd}') - interval '1' month)));


insert into ${table-prefix}${cms-schema}.${table-prefix}cms_performance_campaignid  
select *
from ${table-prefix}${cms-schema}.${table-prefix}cms_summary_bycampaignid
where date(periode) = date(datetime(last_day(date('${run-date=yyyy-MM-dd}') - interval '1' month)))
and eom = 1;


--- update current month data


delete from ${table-prefix}${cms-schema}.${table-prefix}cms_performance_campaignid
where date(periode) = date(datetime(last_day(date('${run-date=yyyy-MM-dd}'))));


insert into ${table-prefix}${cms-schema}.${table-prefix}cms_performance_campaignid  
select *
from ${table-prefix}${cms-schema}.${table-prefix}cms_summary_bycampaignid
where date(periode) = date(datetime(last_day(date('${run-date=yyyy-MM-dd}'))))
and eom = 0;
