
create or replace materialized view ${table-prefix}${cms_out-schema}.${table-prefix}cms_summary_bycampaignid_v
refresh every 1 day
as
select *
from ${table-prefix}${cms-schema}.${table-prefix}cms_summary_bycampaignid
;

create or replace materialized view ${table-prefix}${cms_out-schema}.${table-prefix}cms_summary_bycampaignid_byregionhlr_v
refresh every 1 day
as
select *
from ${table-prefix}${cms-schema}.${table-prefix}cms_summary_bycampaignid_byregionhlr
;


create or replace materialized view ${table-prefix}${cms_out-schema}.${table-prefix}cms_summary_bycampaignid_bycluster_v
refresh every 1 day
as
select *
from ${table-prefix}${cms-schema}.${table-prefix}cms_summary_bycampaignid_bycluster
;


create or replace materialized view ${table-prefix}${cms_out-schema}.${table-prefix}cms_performance_summary_v
refresh every 1 day
as
select *
from ${table-prefix}${cms-schema}.${table-prefix}cms_performance_summary
;


create or replace materialized view ${table-prefix}${cms_out-schema}.${table-prefix}cms_performance_area_v
refresh every 1 day
as
select *
from ${table-prefix}${cms-schema}.${table-prefix}cms_performance_area
;


create or replace materialized view ${table-prefix}${cms_out-schema}.${table-prefix}cms_performance_region_v
refresh every 1 day
as
select *
from ${table-prefix}${cms-schema}.${table-prefix}cms_performance_region
;


create or replace materialized view ${table-prefix}${cms_out-schema}.${table-prefix}cms_performance_campaign_v
refresh every 1 day
as
select *
from ${table-prefix}${cms-schema}.${table-prefix}cms_performance_campaign
;


create or replace materialized view ${table-prefix}${cms_out-schema}.${table-prefix}cms_performance_campaignid_v
refresh every 1 day
as
select *
from ${table-prefix}${cms-schema}.${table-prefix}cms_performance_campaignid
;


create or replace materialized view ${table-prefix}${cms_out-schema}.${table-prefix}cms_unique_taker_v
refresh every 1 day
as
select *
from ${table-prefix}${cms-schema}.${table-prefix}cms_unique_taker
;


create or replace materialized view ${table-prefix}${cms_out-schema}.${table-prefix}cms_performance_atlbtl_v
refresh every 1 day
as
select *
from ${table-prefix}${cms-schema}.${table-prefix}cms_performance_atlbtl
;





