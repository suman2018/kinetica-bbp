/* KI_HINT_DICT_PROJECTION */
create or replace materialized view ${table-prefix}${cms_out-schema}.${table-prefix}cms_campaign_performance_dd_v
refresh every 24 hour
as
select
         category
        ,campaign_type
        ,campaign_name
        ,campaign_group_id
        ,campaign_id
        ,campaign_objective_id
        ,campaign_objective_desc as theme
        ,business_group
        ,case
                when upper(substring(campaign_id, 11, 1)) = 'A' then 'Simpati'
                when upper(substring(campaign_id, 11, 1)) = 'B' then 'KartuAs'
                when upper(substring(campaign_id, 11, 1)) = 'C' then 'AllPrepaid'
                when upper(substring(campaign_id, 11, 1)) = 'D' then 'KartuHALO'
                when upper(substring(campaign_id, 11, 1)) = 'E' then 'AllBrands'
                when upper(substring(campaign_id, 11, 1)) = 'F' then 'Loop'
        end as brand
        ,initiated_business
    	,case
                when initiated_business = '01. Area Sumatra'         then 'AREA 1'
                when initiated_business = '02. Sumbagut'             then 'AREA 1'
                when initiated_business = '03. Sumbagteng'           then 'AREA 1'
                when initiated_business = '04. Sumbagsel'            then 'AREA 1'
                when initiated_business = '05. Area Jabotabek Jabar' then 'AREA 2'
                when initiated_business = '06. Jabotabek West'       then 'AREA 2'
                when initiated_business = '07. Jabotabek Central'    then 'AREA 2'
                when initiated_business = '08. Jabotabek East'       then 'AREA 2'
                when initiated_business = '09. Jabar'                then 'AREA 2'
                when initiated_business = '10. Area Jawa Bali'       then 'AREA 3'
                when initiated_business = '11. Jateng'               then 'AREA 3'
                when initiated_business = '12. Jatim'                then 'AREA 3'
                when initiated_business = '13. Balinusra'            then 'AREA 3'
                when initiated_business = '14. Area Pamasuka'        then 'AREA 4'
                when initiated_business = '15. Kalimantan'           then 'AREA 4'
                when initiated_business = '16. Sulawesi'             then 'AREA 4'
                when initiated_business = '17. Puma'                 then 'AREA 4'
                when initiated_business = '18. HQ'                   then 'HQ'
		 end as area
        ,start_period
        ,end_period
        ,segment_name
        ,trx_date
        ,is_btl
        ,target_submitted as target
        ,target_eligible as eligible
        ,taker
        ,revenue
        ,date(datetime(file_date)) as file_date
        ,orch_job_id
        ,pipeline_name
        ,pipeline_step
from ${table-prefix}${cms-schema}.${table-prefix}cms_campaign_performance_dd
