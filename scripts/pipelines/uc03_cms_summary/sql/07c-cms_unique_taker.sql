delete from ${table-prefix}${cms-schema}.${table-prefix}cms_unique_taker
where date(periode) = date(datetime(last_day(date('${run-date=yyyy-MM-dd}'))));


insert into ${table-prefix}${cms-schema}.${table-prefix}cms_unique_taker 
select 
	last_day(a.trx_date) as periode,
	a.category,
	a.campaign_type,
	a.campaign_id,
	a.campaign_objective_id,
	c.campaign_objective_desc,
	a.campaign_name, 
	a.start_period,
	a.end_period,	
	a.campaign_group_id,
	a.business_group,
	a.initiated_group,
	case
                when a.initiated_group = '01. Area Sumatra'         then 'AREA 1'
                when a.initiated_group = '02. Sumbagut'             then 'AREA 1'
                when a.initiated_group = '03. Sumbagteng'           then 'AREA 1'
                when a.initiated_group = '04. Sumbagsel'            then 'AREA 1'
                when a.initiated_group = '05. Area Jabotabek Jabar' then 'AREA 2'
                when a.initiated_group = '06. Jabotabek West'       then 'AREA 2'
                when a.initiated_group = '07. Jabotabek Central'    then 'AREA 2'
                when a.initiated_group = '08. Jabotabek East'       then 'AREA 2'
                when a.initiated_group = '09. Jabar'                then 'AREA 2'
                when a.initiated_group = '10. Area Jawa Bali'       then 'AREA 3'
                when a.initiated_group = '11. Jateng'               then 'AREA 3'
                when a.initiated_group = '12. Jatim'                then 'AREA 3'
                when a.initiated_group = '13. Balinusra'            then 'AREA 3'
                when a.initiated_group = '14. Area Pamasuka'        then 'AREA 4'
                when a.initiated_group = '15. Kalimantan'           then 'AREA 4'
                when a.initiated_group = '16. Sulawesi'             then 'AREA 4'
                when a.initiated_group = '17. Puma'                 then 'AREA 4'
                when a.initiated_group = '18. HQ'                   then 'HQ'
	end as area,
	a.is_btl,
	case
		when upper(substring(a.campaign_id, 11, 1)) = 'A' then 'Simpati'
		when upper(substring(a.campaign_id, 11, 1)) = 'B' then 'KartuAs'
		when upper(substring(a.campaign_id, 11, 1)) = 'C' then 'AllPrepaid'
		when upper(substring(a.campaign_id, 11, 1)) = 'D' then 'KartuHALO'
		when upper(substring(a.campaign_id, 11, 1)) = 'E' then 'AllBrands'
		when upper(substring(a.campaign_id, 11, 1)) = 'F' then 'Loop'
	end as brand,
	msisdn, 
	sum
		(
	    	case
	    		when a.iscontrolgroup = 'false' and a.campaign_group_id = 2 and a.taker > 0 then a.metric_2
	    		when a.iscontrolgroup = 'false' and a.campaign_group_id <> 2 and a.taker > 0 then a.taker
	    		else 0 
	    	end
	    ) as taker,
	sum(case when a.iscontrolgroup = 'true' and a.taker > 0 then a.taker else 0 end) as taker_control,
	count(distinct case when a.iscontrolgroup = 'false' and a.eligible > 0 then a.msisdn end) as eligible,
	count(distinct case when a.iscontrolgroup = 'true' and a.eligible > 0 then a.msisdn end) as control
from ${table-prefix}${cms-schema}.${table-prefix}cms_campaign_tracking_detail a 
left join 
(
	select distinct campaign_objective_id, campaign_objective_desc
	from ${table-prefix}${cms-schema}.${table-prefix}cms_campaign_objective
) c on a.campaign_objective_id = c.campaign_objective_id
where (a.taker > 0 or a.eligible > 0)
and a.pipeline_step not in ('cms_usage_detail_insert', 'cms_campaign_digital_detail_insert')
and last_day(date(trx_date)) = last_day(date('${run-date=yyyy-MM-dd}'))
group by 1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16;


-- retention data is 3 months only (running month and last 2 months)

delete from ${table-prefix}${cms-schema}.${table-prefix}cms_unique_taker
where date(periode) <= date(datetime(last_day(date('${run-date=yyyy-MM-dd}') - interval '3' month)));


