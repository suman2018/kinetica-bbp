
--- update last month data

delete from ${table-prefix}${cms-schema}.${table-prefix}cms_performance_area
where date(periode) = date(datetime(last_day(date('${run-date=yyyy-MM-dd}') - interval '1' month)));


insert into ${table-prefix}${cms-schema}.${table-prefix}cms_performance_area
select a.periode, a.eom, a.brand, a.theme, a.category, a.area, a.tot_campaign, a.target, a.eligible, a.taker, b.unique_target, b.unique_taker, 
a.revenue, a.recharge, a.control, a.taker_control, a.revenue_control, a.uplift, a.last_trx_date
from 
(
select periode, 1 eom, brand, campaign_objective_desc as theme, category, area, 
count(distinct campaign_id) as tot_campaign,
sum(target) as target, 
sum(eligible) as eligible,
sum(taker) as taker,
sum(case when campaign_group_id in (1,4,6,7) then revenue_taker else 0 end) revenue,
sum(case when campaign_group_id = 2 then revenue_taker else 0 end) recharge,
sum(control) as control,
sum(taker_control) as taker_control,
sum(case when campaign_group_id in (1,4,6,7) then revenue_control else 0 end) revenue_control,
sum((((arpu_target_after - arpu_target_before) - (arpu_control_after - arpu_control_before))/nullif((arpu_target_before * 1.0),0)) * eligible)/nullif(sum(eligible),0) uplift, 
max(last_trx_date) as last_trx_date
from ${table-prefix}${cms-schema}.${table-prefix}cms_summary_bycampaignid
where periode = date(datetime(last_day(date('${run-date=yyyy-MM-dd}') - interval '1' month)))
and eom = 1
group by 1,3,4,5,6 
) a left join 
(
select date(datetime(last_day(trx_date))) as periode, 1 eom, 
case
	when upper(substring(a.campaign_id, 11, 1)) = 'A' then 'Simpati'
	when upper(substring(a.campaign_id, 11, 1)) = 'B' then 'KartuAs'
	when upper(substring(a.campaign_id, 11, 1)) = 'C' then 'AllPrepaid'
	when upper(substring(a.campaign_id, 11, 1)) = 'D' then 'KartuHALO'
	when upper(substring(a.campaign_id, 11, 1)) = 'E' then 'AllBrands'
	when upper(substring(a.campaign_id, 11, 1)) = 'F' then 'Loop'
end as brand, c.campaign_objective_desc theme, a.category, 
case
    when a.initiated_group = '01. Area Sumatra'         then 'AREA 1'
    when a.initiated_group = '02. Sumbagut'             then 'AREA 1'
    when a.initiated_group = '03. Sumbagteng'           then 'AREA 1'
    when a.initiated_group = '04. Sumbagsel'            then 'AREA 1'
    when a.initiated_group = '05. Area Jabotabek Jabar' then 'AREA 2'
    when a.initiated_group = '06. Jabotabek West'       then 'AREA 2'
    when a.initiated_group = '07. Jabotabek Central'    then 'AREA 2'
    when a.initiated_group = '08. Jabotabek East'       then 'AREA 2'
    when a.initiated_group = '09. Jabar'                then 'AREA 2'
    when a.initiated_group = '10. Area Jawa Bali'       then 'AREA 3'
    when a.initiated_group = '11. Jateng'               then 'AREA 3'
    when a.initiated_group = '12. Jatim'                then 'AREA 3'
    when a.initiated_group = '13. Balinusra'            then 'AREA 3'
    when a.initiated_group = '14. Area Pamasuka'        then 'AREA 4'
    when a.initiated_group = '15. Kalimantan'           then 'AREA 4'
    when a.initiated_group = '16. Sulawesi'             then 'AREA 4'
    when a.initiated_group = '17. Puma'                 then 'AREA 4'
    when a.initiated_group = '18. HQ'                   then 'HQ'
end as area,
count(distinct msisdn) unique_target,
count(distinct case when taker > 0 and iscontrolgroup = 'false' then msisdn end) unique_taker
from ${table-prefix}${cms-schema}.${table-prefix}cms_campaign_tracking_detail a 
left join 
(
	select distinct campaign_objective_id, campaign_objective_desc
	from ${table-prefix}${cms-schema}.${table-prefix}cms_campaign_objective
) c on a.campaign_objective_id = c.campaign_objective_id
where a.pipeline_step not in ('cms_usage_detail_insert', 'cms_campaign_digital_detail_insert')
and (trx_date between date(datetime(dateadd('day', 1, last_day(date('${run-date=yyyy-MM-dd}') - interval '2' month)))) 
and date(datetime(last_day(date('${run-date=yyyy-MM-dd}') - interval '1' month))))
group by 1,3,4,5,6
) b on a.periode = b.periode 
and a.eom = b.eom  
and a.brand = b.brand 
and a.theme = b.theme
and a.category = b.category
and a.area = b.area;

insert into ${table-prefix}${cms-schema}.${table-prefix}cms_performance_area
select a.periode, a.eom, a.brand, a.theme, a.category, a.area, a.tot_campaign, a.target, a.eligible, a.taker, b.unique_target, b.unique_taker, 
a.revenue, a.recharge, a.control, a.taker_control, a.revenue_control, a.uplift, a.last_trx_date
from 
(
select periode, 1 eom, 'Total' brand, 'Total' theme, 'Total' category, area, 
count(distinct campaign_id) as tot_campaign,
sum(target) as target, 
sum(eligible) as eligible,
sum(taker) as taker,
sum(case when campaign_group_id in (1,4,6,7) then revenue_taker else 0 end) revenue,
sum(case when campaign_group_id = 2 then revenue_taker else 0 end) recharge,
sum(control) as control,
sum(taker_control) as taker_control,
sum(case when campaign_group_id in (1,4,6,7) then revenue_control else 0 end) revenue_control,
sum((((arpu_target_after - arpu_target_before) - (arpu_control_after - arpu_control_before))/nullif((arpu_target_before * 1.0),0)) * eligible)/nullif(sum(eligible),0) uplift, 
max(last_trx_date) as last_trx_date
from ${table-prefix}${cms-schema}.${table-prefix}cms_summary_bycampaignid
where periode = date(datetime(last_day(date('${run-date=yyyy-MM-dd}') - interval '1' month)))
and eom = 1
group by 1,6 
) a left join 
(
select date(datetime(last_day(trx_date))) as periode, 1 eom, 'Total' brand, 'Total' theme, 'Total' category, 
case
    when a.initiated_group = '01. Area Sumatra'         then 'AREA 1'
    when a.initiated_group = '02. Sumbagut'             then 'AREA 1'
    when a.initiated_group = '03. Sumbagteng'           then 'AREA 1'
    when a.initiated_group = '04. Sumbagsel'            then 'AREA 1'
    when a.initiated_group = '05. Area Jabotabek Jabar' then 'AREA 2'
    when a.initiated_group = '06. Jabotabek West'       then 'AREA 2'
    when a.initiated_group = '07. Jabotabek Central'    then 'AREA 2'
    when a.initiated_group = '08. Jabotabek East'       then 'AREA 2'
    when a.initiated_group = '09. Jabar'                then 'AREA 2'
    when a.initiated_group = '10. Area Jawa Bali'       then 'AREA 3'
    when a.initiated_group = '11. Jateng'               then 'AREA 3'
    when a.initiated_group = '12. Jatim'                then 'AREA 3'
    when a.initiated_group = '13. Balinusra'            then 'AREA 3'
    when a.initiated_group = '14. Area Pamasuka'        then 'AREA 4'
    when a.initiated_group = '15. Kalimantan'           then 'AREA 4'
    when a.initiated_group = '16. Sulawesi'             then 'AREA 4'
    when a.initiated_group = '17. Puma'                 then 'AREA 4'
    when a.initiated_group = '18. HQ'                   then 'HQ'
end as area,
count(distinct msisdn) unique_target,
count(distinct case when taker > 0 and iscontrolgroup = 'false' then msisdn end) unique_taker
from ${table-prefix}${cms-schema}.${table-prefix}cms_campaign_tracking_detail a 
left join 
(
	select distinct campaign_objective_id, campaign_objective_desc
	from ${table-prefix}${cms-schema}.${table-prefix}cms_campaign_objective
) c on a.campaign_objective_id = c.campaign_objective_id
where a.pipeline_step not in ('cms_usage_detail_insert', 'cms_campaign_digital_detail_insert')
and (trx_date between date(datetime(dateadd('day', 1, last_day(date('${run-date=yyyy-MM-dd}') - interval '2' month)))) 
and date(datetime(last_day(date('${run-date=yyyy-MM-dd}') - interval '1' month))))
group by 1,6
) b on a.periode = b.periode 
and a.eom = b.eom  
and a.brand = b.brand 
and a.theme = b.theme
and a.category = b.category
and a.area = b.area;


insert into ${table-prefix}${cms-schema}.${table-prefix}cms_performance_area
select a.periode, a.eom, a.brand, a.theme, a.category, a.area, a.tot_campaign, a.target, a.eligible, a.taker, b.unique_target, b.unique_taker, 
a.revenue, a.recharge, a.control, a.taker_control, a.revenue_control, a.uplift, a.last_trx_date
from 
(
select periode, 1 eom, 'Total' brand, 'Total' theme, category, area, 
count(distinct campaign_id) as tot_campaign,
sum(target) as target, 
sum(eligible) as eligible,
sum(taker) as taker,
sum(case when campaign_group_id in (1,4,6,7) then revenue_taker else 0 end) revenue,
sum(case when campaign_group_id = 2 then revenue_taker else 0 end) recharge,
sum(control) as control,
sum(taker_control) as taker_control,
sum(case when campaign_group_id in (1,4,6,7) then revenue_control else 0 end) revenue_control,
sum((((arpu_target_after - arpu_target_before) - (arpu_control_after - arpu_control_before))/nullif((arpu_target_before * 1.0),0)) * eligible)/nullif(sum(eligible),0) uplift, 
max(last_trx_date) as last_trx_date
from ${table-prefix}${cms-schema}.${table-prefix}cms_summary_bycampaignid
where periode = date(datetime(last_day(date('${run-date=yyyy-MM-dd}') - interval '1' month)))
and eom = 1
group by 1,5,6 
) a left join 
(
select date(datetime(last_day(trx_date))) as periode, 1 eom, 'Total' brand, 'Total' theme, a.category, 
case
    when a.initiated_group = '01. Area Sumatra'         then 'AREA 1'
    when a.initiated_group = '02. Sumbagut'             then 'AREA 1'
    when a.initiated_group = '03. Sumbagteng'           then 'AREA 1'
    when a.initiated_group = '04. Sumbagsel'            then 'AREA 1'
    when a.initiated_group = '05. Area Jabotabek Jabar' then 'AREA 2'
    when a.initiated_group = '06. Jabotabek West'       then 'AREA 2'
    when a.initiated_group = '07. Jabotabek Central'    then 'AREA 2'
    when a.initiated_group = '08. Jabotabek East'       then 'AREA 2'
    when a.initiated_group = '09. Jabar'                then 'AREA 2'
    when a.initiated_group = '10. Area Jawa Bali'       then 'AREA 3'
    when a.initiated_group = '11. Jateng'               then 'AREA 3'
    when a.initiated_group = '12. Jatim'                then 'AREA 3'
    when a.initiated_group = '13. Balinusra'            then 'AREA 3'
    when a.initiated_group = '14. Area Pamasuka'        then 'AREA 4'
    when a.initiated_group = '15. Kalimantan'           then 'AREA 4'
    when a.initiated_group = '16. Sulawesi'             then 'AREA 4'
    when a.initiated_group = '17. Puma'                 then 'AREA 4'
    when a.initiated_group = '18. HQ'                   then 'HQ'
end as area,
count(distinct msisdn) unique_target,
count(distinct case when taker > 0 and iscontrolgroup = 'false' then msisdn end) unique_taker
from ${table-prefix}${cms-schema}.${table-prefix}cms_campaign_tracking_detail a 
left join 
(
	select distinct campaign_objective_id, campaign_objective_desc
	from ${table-prefix}${cms-schema}.${table-prefix}cms_campaign_objective
) c on a.campaign_objective_id = c.campaign_objective_id
where a.pipeline_step not in ('cms_usage_detail_insert', 'cms_campaign_digital_detail_insert')
and (trx_date between date(datetime(dateadd('day', 1, last_day(date('${run-date=yyyy-MM-dd}') - interval '2' month))))
and date(datetime(last_day(date('${run-date=yyyy-MM-dd}') - interval '1' month))))
group by 1,5,6
) b on a.periode = b.periode 
and a.eom = b.eom  
and a.brand = b.brand 
and a.theme = b.theme
and a.category = b.category
and a.area = b.area;


insert into ${table-prefix}${cms-schema}.${table-prefix}cms_performance_area
select a.periode, a.eom, a.brand, a.theme, a.category, a.area, a.tot_campaign, a.target, a.eligible, a.taker, b.unique_target, b.unique_taker, 
a.revenue, a.recharge, a.control, a.taker_control, a.revenue_control, a.uplift, a.last_trx_date
from 
(
select periode, 1 eom, 'Total' brand, campaign_objective_desc as theme, category, area, 
count(distinct campaign_id) as tot_campaign,
sum(target) as target, 
sum(eligible) as eligible,
sum(taker) as taker,
sum(case when campaign_group_id in (1,4,6,7) then revenue_taker else 0 end) revenue,
sum(case when campaign_group_id = 2 then revenue_taker else 0 end) recharge,
sum(control) as control,
sum(taker_control) as taker_control,
sum(case when campaign_group_id in (1,4,6,7) then revenue_control else 0 end) revenue_control,
sum((((arpu_target_after - arpu_target_before) - (arpu_control_after - arpu_control_before))/nullif((arpu_target_before * 1.0),0)) * eligible)/nullif(sum(eligible),0) uplift, 
max(last_trx_date) as last_trx_date
from ${table-prefix}${cms-schema}.${table-prefix}cms_summary_bycampaignid
where periode = date(datetime(last_day(date('${run-date=yyyy-MM-dd}') - interval '1' month)))
and eom = 1
group by 1,4,5,6 
) a left join 
(
select date(datetime(last_day(trx_date))) as periode, 1 eom, 'Total' brand, c.campaign_objective_desc theme, a.category, 
case
    when a.initiated_group = '01. Area Sumatra'         then 'AREA 1'
    when a.initiated_group = '02. Sumbagut'             then 'AREA 1'
    when a.initiated_group = '03. Sumbagteng'           then 'AREA 1'
    when a.initiated_group = '04. Sumbagsel'            then 'AREA 1'
    when a.initiated_group = '05. Area Jabotabek Jabar' then 'AREA 2'
    when a.initiated_group = '06. Jabotabek West'       then 'AREA 2'
    when a.initiated_group = '07. Jabotabek Central'    then 'AREA 2'
    when a.initiated_group = '08. Jabotabek East'       then 'AREA 2'
    when a.initiated_group = '09. Jabar'                then 'AREA 2'
    when a.initiated_group = '10. Area Jawa Bali'       then 'AREA 3'
    when a.initiated_group = '11. Jateng'               then 'AREA 3'
    when a.initiated_group = '12. Jatim'                then 'AREA 3'
    when a.initiated_group = '13. Balinusra'            then 'AREA 3'
    when a.initiated_group = '14. Area Pamasuka'        then 'AREA 4'
    when a.initiated_group = '15. Kalimantan'           then 'AREA 4'
    when a.initiated_group = '16. Sulawesi'             then 'AREA 4'
    when a.initiated_group = '17. Puma'                 then 'AREA 4'
    when a.initiated_group = '18. HQ'                   then 'HQ'
end as area,
count(distinct msisdn) unique_target,
count(distinct case when taker > 0 and iscontrolgroup = 'false' then msisdn end) unique_taker
from ${table-prefix}${cms-schema}.${table-prefix}cms_campaign_tracking_detail a 
left join 
(
	select distinct campaign_objective_id, campaign_objective_desc
	from ${table-prefix}${cms-schema}.${table-prefix}cms_campaign_objective
) c on a.campaign_objective_id = c.campaign_objective_id
where a.pipeline_step not in ('cms_usage_detail_insert', 'cms_campaign_digital_detail_insert')
and (trx_date between date(datetime(dateadd('day', 1, last_day(date('${run-date=yyyy-MM-dd}') - interval '2' month)))) 
and date(datetime(last_day(date('${run-date=yyyy-MM-dd}') - interval '1' month))))
group by 1,4,5,6
) b on a.periode = b.periode 
and a.eom = b.eom  
and a.brand = b.brand 
and a.theme = b.theme
and a.category = b.category
and a.area = b.area;



insert into ${table-prefix}${cms-schema}.${table-prefix}cms_performance_area
select a.periode, a.eom, a.brand, a.theme, a.category, a.area, a.tot_campaign, a.target, a.eligible, a.taker, b.unique_target, b.unique_taker, 
a.revenue, a.recharge, a.control, a.taker_control, a.revenue_control, a.uplift, a.last_trx_date
from 
(
select periode, 1 eom, 'Total' brand, campaign_objective_desc as theme, 'Total' category, area, 
count(distinct campaign_id) as tot_campaign,
sum(target) as target, 
sum(eligible) as eligible,
sum(taker) as taker,
sum(case when campaign_group_id in (1,4,6,7) then revenue_taker else 0 end) revenue,
sum(case when campaign_group_id = 2 then revenue_taker else 0 end) recharge,
sum(control) as control,
sum(taker_control) as taker_control,
sum(case when campaign_group_id in (1,4,6,7) then revenue_control else 0 end) revenue_control,
sum((((arpu_target_after - arpu_target_before) - (arpu_control_after - arpu_control_before))/nullif((arpu_target_before * 1.0),0)) * eligible)/nullif(sum(eligible),0) uplift, 
max(last_trx_date) as last_trx_date
from ${table-prefix}${cms-schema}.${table-prefix}cms_summary_bycampaignid
where periode = date(datetime(last_day(date('${run-date=yyyy-MM-dd}') - interval '1' month)))
and eom = 1
group by 1,4,6 
) a left join 
(
select date(datetime(last_day(trx_date))) as periode, 1 eom, 'Total' brand, c.campaign_objective_desc theme, 'Total' category, 
case
    when a.initiated_group = '01. Area Sumatra'         then 'AREA 1'
    when a.initiated_group = '02. Sumbagut'             then 'AREA 1'
    when a.initiated_group = '03. Sumbagteng'           then 'AREA 1'
    when a.initiated_group = '04. Sumbagsel'            then 'AREA 1'
    when a.initiated_group = '05. Area Jabotabek Jabar' then 'AREA 2'
    when a.initiated_group = '06. Jabotabek West'       then 'AREA 2'
    when a.initiated_group = '07. Jabotabek Central'    then 'AREA 2'
    when a.initiated_group = '08. Jabotabek East'       then 'AREA 2'
    when a.initiated_group = '09. Jabar'                then 'AREA 2'
    when a.initiated_group = '10. Area Jawa Bali'       then 'AREA 3'
    when a.initiated_group = '11. Jateng'               then 'AREA 3'
    when a.initiated_group = '12. Jatim'                then 'AREA 3'
    when a.initiated_group = '13. Balinusra'            then 'AREA 3'
    when a.initiated_group = '14. Area Pamasuka'        then 'AREA 4'
    when a.initiated_group = '15. Kalimantan'           then 'AREA 4'
    when a.initiated_group = '16. Sulawesi'             then 'AREA 4'
    when a.initiated_group = '17. Puma'                 then 'AREA 4'
    when a.initiated_group = '18. HQ'                   then 'HQ'
end as area,
count(distinct msisdn) unique_target,
count(distinct case when taker > 0 and iscontrolgroup = 'false' then msisdn end) unique_taker
from ${table-prefix}${cms-schema}.${table-prefix}cms_campaign_tracking_detail a 
left join 
(
	select distinct campaign_objective_id, campaign_objective_desc
	from ${table-prefix}${cms-schema}.${table-prefix}cms_campaign_objective
) c on a.campaign_objective_id = c.campaign_objective_id
where a.pipeline_step not in ('cms_usage_detail_insert', 'cms_campaign_digital_detail_insert')
and (trx_date between date(datetime(dateadd('day', 1, last_day(date('${run-date=yyyy-MM-dd}') - interval '2' month)))) 
and date(datetime(last_day(date('${run-date=yyyy-MM-dd}') - interval '1' month))))
group by 1,4,6
) b on a.periode = b.periode 
and a.eom = b.eom  
and a.brand = b.brand 
and a.theme = b.theme
and a.category = b.category
and a.area = b.area;



insert into ${table-prefix}${cms-schema}.${table-prefix}cms_performance_area
select a.periode, a.eom, a.brand, a.theme, a.category, a.area, a.tot_campaign, a.target, a.eligible, a.taker, b.unique_target, b.unique_taker, 
a.revenue, a.recharge, a.control, a.taker_control, a.revenue_control, a.uplift, a.last_trx_date
from 
(
select periode, 1 eom, brand, campaign_objective_desc as theme, 'Total' category, area, 
count(distinct campaign_id) as tot_campaign,
sum(target) as target, 
sum(eligible) as eligible,
sum(taker) as taker,
sum(case when campaign_group_id in (1,4,6,7) then revenue_taker else 0 end) revenue,
sum(case when campaign_group_id = 2 then revenue_taker else 0 end) recharge,
sum(control) as control,
sum(taker_control) as taker_control,
sum(case when campaign_group_id in (1,4,6,7) then revenue_control else 0 end) revenue_control,
sum((((arpu_target_after - arpu_target_before) - (arpu_control_after - arpu_control_before))/nullif((arpu_target_before * 1.0),0)) * eligible)/nullif(sum(eligible),0) uplift, 
max(last_trx_date) as last_trx_date
from ${table-prefix}${cms-schema}.${table-prefix}cms_summary_bycampaignid
where periode = date(datetime(last_day(date('${run-date=yyyy-MM-dd}') - interval '1' month)))
and eom = 1
group by 1,3,4,6 
) a left join 
(
select date(datetime(last_day(trx_date))) as periode, 1 eom,
case
	when upper(substring(a.campaign_id, 11, 1)) = 'A' then 'Simpati'
	when upper(substring(a.campaign_id, 11, 1)) = 'B' then 'KartuAs'
	when upper(substring(a.campaign_id, 11, 1)) = 'C' then 'AllPrepaid'
	when upper(substring(a.campaign_id, 11, 1)) = 'D' then 'KartuHALO'
	when upper(substring(a.campaign_id, 11, 1)) = 'E' then 'AllBrands'
	when upper(substring(a.campaign_id, 11, 1)) = 'F' then 'Loop'
end as brand,
c.campaign_objective_desc theme, 'Total' category, 
case
    when a.initiated_group = '01. Area Sumatra'         then 'AREA 1'
    when a.initiated_group = '02. Sumbagut'             then 'AREA 1'
    when a.initiated_group = '03. Sumbagteng'           then 'AREA 1'
    when a.initiated_group = '04. Sumbagsel'            then 'AREA 1'
    when a.initiated_group = '05. Area Jabotabek Jabar' then 'AREA 2'
    when a.initiated_group = '06. Jabotabek West'       then 'AREA 2'
    when a.initiated_group = '07. Jabotabek Central'    then 'AREA 2'
    when a.initiated_group = '08. Jabotabek East'       then 'AREA 2'
    when a.initiated_group = '09. Jabar'                then 'AREA 2'
    when a.initiated_group = '10. Area Jawa Bali'       then 'AREA 3'
    when a.initiated_group = '11. Jateng'               then 'AREA 3'
    when a.initiated_group = '12. Jatim'                then 'AREA 3'
    when a.initiated_group = '13. Balinusra'            then 'AREA 3'
    when a.initiated_group = '14. Area Pamasuka'        then 'AREA 4'
    when a.initiated_group = '15. Kalimantan'           then 'AREA 4'
    when a.initiated_group = '16. Sulawesi'             then 'AREA 4'
    when a.initiated_group = '17. Puma'                 then 'AREA 4'
    when a.initiated_group = '18. HQ'                   then 'HQ'
end as area,
count(distinct msisdn) unique_target,
count(distinct case when taker > 0 and iscontrolgroup = 'false' then msisdn end) unique_taker
from ${table-prefix}${cms-schema}.${table-prefix}cms_campaign_tracking_detail a 
left join 
(
	select distinct campaign_objective_id, campaign_objective_desc
	from ${table-prefix}${cms-schema}.${table-prefix}cms_campaign_objective
) c on a.campaign_objective_id = c.campaign_objective_id
where a.pipeline_step not in ('cms_usage_detail_insert', 'cms_campaign_digital_detail_insert')
and (trx_date between date(datetime(dateadd('day', 1, last_day(date('${run-date=yyyy-MM-dd}') - interval '2' month)))) 
and date(datetime(last_day(date('${run-date=yyyy-MM-dd}') - interval '1' month))))
group by 1,3,4,6
) b on a.periode = b.periode 
and a.eom = b.eom  
and a.brand = b.brand 
and a.theme = b.theme
and a.category = b.category
and a.area = b.area;


insert into ${table-prefix}${cms-schema}.${table-prefix}cms_performance_area
select a.periode, a.eom, a.brand, a.theme, a.category, a.area, a.tot_campaign, a.target, a.eligible, a.taker, b.unique_target, b.unique_taker, 
a.revenue, a.recharge, a.control, a.taker_control, a.revenue_control, a.uplift, a.last_trx_date
from 
(
select periode, 1 eom, brand, 'Total' theme, 'Total' category, area, 
count(distinct campaign_id) as tot_campaign,
sum(target) as target, 
sum(eligible) as eligible,
sum(taker) as taker,
sum(case when campaign_group_id in (1,4,6,7) then revenue_taker else 0 end) revenue,
sum(case when campaign_group_id = 2 then revenue_taker else 0 end) recharge,
sum(control) as control,
sum(taker_control) as taker_control,
sum(case when campaign_group_id in (1,4,6,7) then revenue_control else 0 end) revenue_control,
sum((((arpu_target_after - arpu_target_before) - (arpu_control_after - arpu_control_before))/nullif((arpu_target_before * 1.0),0)) * eligible)/nullif(sum(eligible),0) uplift, 
max(last_trx_date) as last_trx_date
from ${table-prefix}${cms-schema}.${table-prefix}cms_summary_bycampaignid
where periode = date(datetime(last_day(date('${run-date=yyyy-MM-dd}') - interval '1' month)))
and eom = 1
group by 1,3,6 
) a left join 
(
select date(datetime(last_day(trx_date))) as periode, 1 eom, 
case
	when upper(substring(a.campaign_id, 11, 1)) = 'A' then 'Simpati'
	when upper(substring(a.campaign_id, 11, 1)) = 'B' then 'KartuAs'
	when upper(substring(a.campaign_id, 11, 1)) = 'C' then 'AllPrepaid'
	when upper(substring(a.campaign_id, 11, 1)) = 'D' then 'KartuHALO'
	when upper(substring(a.campaign_id, 11, 1)) = 'E' then 'AllBrands'
	when upper(substring(a.campaign_id, 11, 1)) = 'F' then 'Loop'
end as brand,
'Total' theme, 'Total' category, 
case
    when a.initiated_group = '01. Area Sumatra'         then 'AREA 1'
    when a.initiated_group = '02. Sumbagut'             then 'AREA 1'
    when a.initiated_group = '03. Sumbagteng'           then 'AREA 1'
    when a.initiated_group = '04. Sumbagsel'            then 'AREA 1'
    when a.initiated_group = '05. Area Jabotabek Jabar' then 'AREA 2'
    when a.initiated_group = '06. Jabotabek West'       then 'AREA 2'
    when a.initiated_group = '07. Jabotabek Central'    then 'AREA 2'
    when a.initiated_group = '08. Jabotabek East'       then 'AREA 2'
    when a.initiated_group = '09. Jabar'                then 'AREA 2'
    when a.initiated_group = '10. Area Jawa Bali'       then 'AREA 3'
    when a.initiated_group = '11. Jateng'               then 'AREA 3'
    when a.initiated_group = '12. Jatim'                then 'AREA 3'
    when a.initiated_group = '13. Balinusra'            then 'AREA 3'
    when a.initiated_group = '14. Area Pamasuka'        then 'AREA 4'
    when a.initiated_group = '15. Kalimantan'           then 'AREA 4'
    when a.initiated_group = '16. Sulawesi'             then 'AREA 4'
    when a.initiated_group = '17. Puma'                 then 'AREA 4'
    when a.initiated_group = '18. HQ'                   then 'HQ'
end as area,
count(distinct msisdn) unique_target,
count(distinct case when taker > 0 and iscontrolgroup = 'false' then msisdn end) unique_taker
from ${table-prefix}${cms-schema}.${table-prefix}cms_campaign_tracking_detail a 
left join 
(
	select distinct campaign_objective_id, campaign_objective_desc
	from ${table-prefix}${cms-schema}.${table-prefix}cms_campaign_objective
) c on a.campaign_objective_id = c.campaign_objective_id
where a.pipeline_step not in ('cms_usage_detail_insert', 'cms_campaign_digital_detail_insert')
and (trx_date between date(datetime(dateadd('day', 1, last_day(date('${run-date=yyyy-MM-dd}') - interval '2' month)))) 
and date(datetime(last_day(date('${run-date=yyyy-MM-dd}') - interval '1' month))))
group by 1,3,6
) b on a.periode = b.periode 
and a.eom = b.eom  
and a.brand = b.brand 
and a.theme = b.theme
and a.category = b.category
and a.area = b.area;


insert into ${table-prefix}${cms-schema}.${table-prefix}cms_performance_area
select a.periode, a.eom, a.brand, a.theme, a.category, a.area, a.tot_campaign, a.target, a.eligible, a.taker, b.unique_target, b.unique_taker, 
a.revenue, a.recharge, a.control, a.taker_control, a.revenue_control, a.uplift, a.last_trx_date
from 
(
select periode, 1 eom, brand, 'Total' theme, category, area, 
count(distinct campaign_id) as tot_campaign,
sum(target) as target, 
sum(eligible) as eligible,
sum(taker) as taker,
sum(case when campaign_group_id in (1,4,6,7) then revenue_taker else 0 end) revenue,
sum(case when campaign_group_id = 2 then revenue_taker else 0 end) recharge,
sum(control) as control,
sum(taker_control) as taker_control,
sum(case when campaign_group_id in (1,4,6,7) then revenue_control else 0 end) revenue_control,
sum((((arpu_target_after - arpu_target_before) - (arpu_control_after - arpu_control_before))/nullif((arpu_target_before * 1.0),0)) * eligible)/nullif(sum(eligible),0) uplift, 
max(last_trx_date) as last_trx_date
from ${table-prefix}${cms-schema}.${table-prefix}cms_summary_bycampaignid
where periode = date(datetime(last_day(date('${run-date=yyyy-MM-dd}') - interval '1' month)))
and eom = 1
group by 1,3,5,6 
) a left join 
(
select date(datetime(last_day(trx_date))) as periode, 1 eom, 
case
	when upper(substring(a.campaign_id, 11, 1)) = 'A' then 'Simpati'
	when upper(substring(a.campaign_id, 11, 1)) = 'B' then 'KartuAs'
	when upper(substring(a.campaign_id, 11, 1)) = 'C' then 'AllPrepaid'
	when upper(substring(a.campaign_id, 11, 1)) = 'D' then 'KartuHALO'
	when upper(substring(a.campaign_id, 11, 1)) = 'E' then 'AllBrands'
	when upper(substring(a.campaign_id, 11, 1)) = 'F' then 'Loop'
end as brand,
'Total' theme, a.category, 
case
    when a.initiated_group = '01. Area Sumatra'         then 'AREA 1'
    when a.initiated_group = '02. Sumbagut'             then 'AREA 1'
    when a.initiated_group = '03. Sumbagteng'           then 'AREA 1'
    when a.initiated_group = '04. Sumbagsel'            then 'AREA 1'
    when a.initiated_group = '05. Area Jabotabek Jabar' then 'AREA 2'
    when a.initiated_group = '06. Jabotabek West'       then 'AREA 2'
    when a.initiated_group = '07. Jabotabek Central'    then 'AREA 2'
    when a.initiated_group = '08. Jabotabek East'       then 'AREA 2'
    when a.initiated_group = '09. Jabar'                then 'AREA 2'
    when a.initiated_group = '10. Area Jawa Bali'       then 'AREA 3'
    when a.initiated_group = '11. Jateng'               then 'AREA 3'
    when a.initiated_group = '12. Jatim'                then 'AREA 3'
    when a.initiated_group = '13. Balinusra'            then 'AREA 3'
    when a.initiated_group = '14. Area Pamasuka'        then 'AREA 4'
    when a.initiated_group = '15. Kalimantan'           then 'AREA 4'
    when a.initiated_group = '16. Sulawesi'             then 'AREA 4'
    when a.initiated_group = '17. Puma'                 then 'AREA 4'
    when a.initiated_group = '18. HQ'                   then 'HQ'
end as area,
count(distinct msisdn) unique_target,
count(distinct case when taker > 0 and iscontrolgroup = 'false' then msisdn end) unique_taker
from ${table-prefix}${cms-schema}.${table-prefix}cms_campaign_tracking_detail a 
left join 
(
	select distinct campaign_objective_id, campaign_objective_desc
	from ${table-prefix}${cms-schema}.${table-prefix}cms_campaign_objective
) c on a.campaign_objective_id = c.campaign_objective_id
where a.pipeline_step not in ('cms_usage_detail_insert', 'cms_campaign_digital_detail_insert')
and (trx_date between date(datetime(dateadd('day', 1, last_day(date('${run-date=yyyy-MM-dd}') - interval '2' month)))) 
and date(datetime(last_day(date('${run-date=yyyy-MM-dd}') - interval '1' month))))
group by 1,3,5,6
) b on a.periode = b.periode 
and a.eom = b.eom  
and a.brand = b.brand 
and a.theme = b.theme
and a.category = b.category
and a.area = b.area;


--- update last 3 months data for running month eom = 0

delete from ${table-prefix}${cms-schema}.${table-prefix}cms_performance_area
where eom = 0;


insert into ${table-prefix}${cms-schema}.${table-prefix}cms_performance_area
select a.periode, a.eom, a.brand, a.theme, a.category, a.area, a.tot_campaign, a.target, a.eligible, a.taker, b.unique_target, b.unique_taker, 
a.revenue, a.recharge, a.control, a.taker_control, a.revenue_control, a.uplift, a.last_trx_date
from 
(
select periode, eom, brand, campaign_objective_desc as theme, category, area, 
count(distinct campaign_id) as tot_campaign,
sum(target) as target, 
sum(eligible) as eligible,
sum(taker) as taker,
sum(case when campaign_group_id in (1,4,6,7) then revenue_taker else 0 end) revenue,
sum(case when campaign_group_id = 2 then revenue_taker else 0 end) recharge,
sum(control) as control,
sum(taker_control) as taker_control,
sum(case when campaign_group_id in (1,4,6,7) then revenue_control else 0 end) revenue_control,
sum((((arpu_target_after - arpu_target_before) - (arpu_control_after - arpu_control_before))/nullif((arpu_target_before * 1.0),0)) * eligible)/nullif(sum(eligible),0) uplift, 
max(last_trx_date) as last_trx_date
from ${table-prefix}${cms-schema}.${table-prefix}cms_summary_bycampaignid
where eom = 0 
group by 1,2,3,4,5,6 
) a left join 
(
select date(datetime(last_day(trx_date))) as periode, 0 eom, 
case
	when upper(substring(a.campaign_id, 11, 1)) = 'A' then 'Simpati'
	when upper(substring(a.campaign_id, 11, 1)) = 'B' then 'KartuAs'
	when upper(substring(a.campaign_id, 11, 1)) = 'C' then 'AllPrepaid'
	when upper(substring(a.campaign_id, 11, 1)) = 'D' then 'KartuHALO'
	when upper(substring(a.campaign_id, 11, 1)) = 'E' then 'AllBrands'
	when upper(substring(a.campaign_id, 11, 1)) = 'F' then 'Loop'
end as brand, c.campaign_objective_desc theme, a.category, 
case
    when a.initiated_group = '01. Area Sumatra'         then 'AREA 1'
    when a.initiated_group = '02. Sumbagut'             then 'AREA 1'
    when a.initiated_group = '03. Sumbagteng'           then 'AREA 1'
    when a.initiated_group = '04. Sumbagsel'            then 'AREA 1'
    when a.initiated_group = '05. Area Jabotabek Jabar' then 'AREA 2'
    when a.initiated_group = '06. Jabotabek West'       then 'AREA 2'
    when a.initiated_group = '07. Jabotabek Central'    then 'AREA 2'
    when a.initiated_group = '08. Jabotabek East'       then 'AREA 2'
    when a.initiated_group = '09. Jabar'                then 'AREA 2'
    when a.initiated_group = '10. Area Jawa Bali'       then 'AREA 3'
    when a.initiated_group = '11. Jateng'               then 'AREA 3'
    when a.initiated_group = '12. Jatim'                then 'AREA 3'
    when a.initiated_group = '13. Balinusra'            then 'AREA 3'
    when a.initiated_group = '14. Area Pamasuka'        then 'AREA 4'
    when a.initiated_group = '15. Kalimantan'           then 'AREA 4'
    when a.initiated_group = '16. Sulawesi'             then 'AREA 4'
    when a.initiated_group = '17. Puma'                 then 'AREA 4'
    when a.initiated_group = '18. HQ'                   then 'HQ'
end as area,
count(distinct msisdn) unique_target,
count(distinct case when taker > 0 and iscontrolgroup = 'false' then msisdn end) unique_taker
from ${table-prefix}${cms-schema}.${table-prefix}cms_campaign_tracking_detail a 
left join 
(
	select distinct campaign_objective_id, campaign_objective_desc
	from ${table-prefix}${cms-schema}.${table-prefix}cms_campaign_objective
) c on a.campaign_objective_id = c.campaign_objective_id
where a.pipeline_step not in ('cms_usage_detail_insert', 'cms_campaign_digital_detail_insert')
and 
(
   	(trx_date between date(datetime(dateadd('day', 1, last_day(date('${run-date=yyyy-MM-dd}') - interval '3' month)))) 
and date(date('${run-date=yyyy-MM-dd}') - interval '2' month))
	or (trx_date between date(datetime(dateadd('day', 1, last_day(date('${run-date=yyyy-MM-dd}') - interval '2' month)))) and date(date('${run-date=yyyy-MM-dd}') - interval '1' month))
	or (trx_date between date(datetime(dateadd('day', 1, last_day(date('${run-date=yyyy-MM-dd}') - interval '1' month)))) and date(date('${run-date=yyyy-MM-dd}')))
)
group by 1,3,4,5,6
) b on a.periode = b.periode 
and a.eom = b.eom  
and a.brand = b.brand 
and a.theme = b.theme
and a.category = b.category
and a.area = b.area;

insert into ${table-prefix}${cms-schema}.${table-prefix}cms_performance_area
select a.periode, a.eom, a.brand, a.theme, a.category, a.area, a.tot_campaign, a.target, a.eligible, a.taker, b.unique_target, b.unique_taker, 
a.revenue, a.recharge, a.control, a.taker_control, a.revenue_control, a.uplift, a.last_trx_date
from 
(
select periode, eom, 'Total' brand, 'Total' theme, 'Total' category, area, 
count(distinct campaign_id) as tot_campaign,
sum(target) as target, 
sum(eligible) as eligible,
sum(taker) as taker,
sum(case when campaign_group_id in (1,4,6,7) then revenue_taker else 0 end) revenue,
sum(case when campaign_group_id = 2 then revenue_taker else 0 end) recharge,
sum(control) as control,
sum(taker_control) as taker_control,
sum(case when campaign_group_id in (1,4,6,7) then revenue_control else 0 end) revenue_control,
sum((((arpu_target_after - arpu_target_before) - (arpu_control_after - arpu_control_before))/nullif((arpu_target_before * 1.0),0)) * eligible)/nullif(sum(eligible),0) uplift, 
max(last_trx_date) as last_trx_date
from ${table-prefix}${cms-schema}.${table-prefix}cms_summary_bycampaignid
where eom = 0
group by 1,2,6 
) a left join 
(
select date(datetime(last_day(trx_date))) as periode, 0 eom, 'Total' brand, 'Total' theme, 'Total' category, 
case
    when a.initiated_group = '01. Area Sumatra'         then 'AREA 1'
    when a.initiated_group = '02. Sumbagut'             then 'AREA 1'
    when a.initiated_group = '03. Sumbagteng'           then 'AREA 1'
    when a.initiated_group = '04. Sumbagsel'            then 'AREA 1'
    when a.initiated_group = '05. Area Jabotabek Jabar' then 'AREA 2'
    when a.initiated_group = '06. Jabotabek West'       then 'AREA 2'
    when a.initiated_group = '07. Jabotabek Central'    then 'AREA 2'
    when a.initiated_group = '08. Jabotabek East'       then 'AREA 2'
    when a.initiated_group = '09. Jabar'                then 'AREA 2'
    when a.initiated_group = '10. Area Jawa Bali'       then 'AREA 3'
    when a.initiated_group = '11. Jateng'               then 'AREA 3'
    when a.initiated_group = '12. Jatim'                then 'AREA 3'
    when a.initiated_group = '13. Balinusra'            then 'AREA 3'
    when a.initiated_group = '14. Area Pamasuka'        then 'AREA 4'
    when a.initiated_group = '15. Kalimantan'           then 'AREA 4'
    when a.initiated_group = '16. Sulawesi'             then 'AREA 4'
    when a.initiated_group = '17. Puma'                 then 'AREA 4'
    when a.initiated_group = '18. HQ'                   then 'HQ'
end as area,
count(distinct msisdn) unique_target,
count(distinct case when taker > 0 and iscontrolgroup = 'false' then msisdn end) unique_taker
from ${table-prefix}${cms-schema}.${table-prefix}cms_campaign_tracking_detail a 
left join 
(
	select distinct campaign_objective_id, campaign_objective_desc
	from ${table-prefix}${cms-schema}.${table-prefix}cms_campaign_objective
) c on a.campaign_objective_id = c.campaign_objective_id
where a.pipeline_step not in ('cms_usage_detail_insert', 'cms_campaign_digital_detail_insert')
and 
(
   	(trx_date between date(datetime(dateadd('day', 1, last_day(date('${run-date=yyyy-MM-dd}') - interval '3' month)))) 
and date(date('${run-date=yyyy-MM-dd}') - interval '2' month))
	or (trx_date between date(datetime(dateadd('day', 1, last_day(date('${run-date=yyyy-MM-dd}') - interval '2' month)))) and date(date('${run-date=yyyy-MM-dd}') - interval '1' month))
	or (trx_date between date(datetime(dateadd('day', 1, last_day(date('${run-date=yyyy-MM-dd}') - interval '1' month)))) and date(date('${run-date=yyyy-MM-dd}')))
)
group by 1,6
) b on a.periode = b.periode 
and a.eom = b.eom  
and a.brand = b.brand 
and a.theme = b.theme
and a.category = b.category
and a.area = b.area;


insert into ${table-prefix}${cms-schema}.${table-prefix}cms_performance_area
select a.periode, a.eom, a.brand, a.theme, a.category, a.area, a.tot_campaign, a.target, a.eligible, a.taker, b.unique_target, b.unique_taker, 
a.revenue, a.recharge, a.control, a.taker_control, a.revenue_control, a.uplift, a.last_trx_date
from 
(
select periode, eom, 'Total' brand, 'Total' theme, category, area, 
count(distinct campaign_id) as tot_campaign,
sum(target) as target, 
sum(eligible) as eligible,
sum(taker) as taker,
sum(case when campaign_group_id in (1,4,6,7) then revenue_taker else 0 end) revenue,
sum(case when campaign_group_id = 2 then revenue_taker else 0 end) recharge,
sum(control) as control,
sum(taker_control) as taker_control,
sum(case when campaign_group_id in (1,4,6,7) then revenue_control else 0 end) revenue_control,
sum((((arpu_target_after - arpu_target_before) - (arpu_control_after - arpu_control_before))/nullif((arpu_target_before * 1.0),0)) * eligible)/nullif(sum(eligible),0) uplift, 
max(last_trx_date) as last_trx_date
from ${table-prefix}${cms-schema}.${table-prefix}cms_summary_bycampaignid
where eom = 0
group by 1,2,5,6 
) a left join 
(
select date(datetime(last_day(trx_date))) as periode, 0 eom, 'Total' brand, 'Total' theme, a.category, 
case
    when a.initiated_group = '01. Area Sumatra'         then 'AREA 1'
    when a.initiated_group = '02. Sumbagut'             then 'AREA 1'
    when a.initiated_group = '03. Sumbagteng'           then 'AREA 1'
    when a.initiated_group = '04. Sumbagsel'            then 'AREA 1'
    when a.initiated_group = '05. Area Jabotabek Jabar' then 'AREA 2'
    when a.initiated_group = '06. Jabotabek West'       then 'AREA 2'
    when a.initiated_group = '07. Jabotabek Central'    then 'AREA 2'
    when a.initiated_group = '08. Jabotabek East'       then 'AREA 2'
    when a.initiated_group = '09. Jabar'                then 'AREA 2'
    when a.initiated_group = '10. Area Jawa Bali'       then 'AREA 3'
    when a.initiated_group = '11. Jateng'               then 'AREA 3'
    when a.initiated_group = '12. Jatim'                then 'AREA 3'
    when a.initiated_group = '13. Balinusra'            then 'AREA 3'
    when a.initiated_group = '14. Area Pamasuka'        then 'AREA 4'
    when a.initiated_group = '15. Kalimantan'           then 'AREA 4'
    when a.initiated_group = '16. Sulawesi'             then 'AREA 4'
    when a.initiated_group = '17. Puma'                 then 'AREA 4'
    when a.initiated_group = '18. HQ'                   then 'HQ'
end as area,
count(distinct msisdn) unique_target,
count(distinct case when taker > 0 and iscontrolgroup = 'false' then msisdn end) unique_taker
from ${table-prefix}${cms-schema}.${table-prefix}cms_campaign_tracking_detail a 
left join 
(
	select distinct campaign_objective_id, campaign_objective_desc
	from ${table-prefix}${cms-schema}.${table-prefix}cms_campaign_objective
) c on a.campaign_objective_id = c.campaign_objective_id
where a.pipeline_step not in ('cms_usage_detail_insert', 'cms_campaign_digital_detail_insert')
and 
(
   	(trx_date between date(datetime(dateadd('day', 1, last_day(date('${run-date=yyyy-MM-dd}') - interval '3' month)))) and date(date('${run-date=yyyy-MM-dd}') - interval '2' month))
	or (trx_date between date(datetime(dateadd('day', 1, last_day(date('${run-date=yyyy-MM-dd}') - interval '2' month)))) and date(date('${run-date=yyyy-MM-dd}') - interval '1' month))
	or (trx_date between date(datetime(dateadd('day', 1, last_day(date('${run-date=yyyy-MM-dd}') - interval '1' month)))) and date(date('${run-date=yyyy-MM-dd}')))
)
group by 1,5,6
) b on a.periode = b.periode 
and a.eom = b.eom  
and a.brand = b.brand 
and a.theme = b.theme
and a.category = b.category
and a.area = b.area;


insert into ${table-prefix}${cms-schema}.${table-prefix}cms_performance_area
select a.periode, a.eom, a.brand, a.theme, a.category, a.area, a.tot_campaign, a.target, a.eligible, a.taker, b.unique_target, b.unique_taker, 
a.revenue, a.recharge, a.control, a.taker_control, a.revenue_control, a.uplift, a.last_trx_date
from 
(
select periode, eom, 'Total' brand, campaign_objective_desc as theme, category, area, 
count(distinct campaign_id) as tot_campaign,
sum(target) as target, 
sum(eligible) as eligible,
sum(taker) as taker,
sum(case when campaign_group_id in (1,4,6,7) then revenue_taker else 0 end) revenue,
sum(case when campaign_group_id = 2 then revenue_taker else 0 end) recharge,
sum(control) as control,
sum(taker_control) as taker_control,
sum(case when campaign_group_id in (1,4,6,7) then revenue_control else 0 end) revenue_control,
sum((((arpu_target_after - arpu_target_before) - (arpu_control_after - arpu_control_before))/nullif((arpu_target_before * 1.0),0)) * eligible)/nullif(sum(eligible),0) uplift, 
max(last_trx_date) as last_trx_date
from ${table-prefix}${cms-schema}.${table-prefix}cms_summary_bycampaignid
where eom = 0
group by 1,2,4,5,6 
) a left join 
(
select date(datetime(last_day(trx_date))) as periode, 0 eom, 'Total' brand, c.campaign_objective_desc theme, a.category, 
case
    when a.initiated_group = '01. Area Sumatra'         then 'AREA 1'
    when a.initiated_group = '02. Sumbagut'             then 'AREA 1'
    when a.initiated_group = '03. Sumbagteng'           then 'AREA 1'
    when a.initiated_group = '04. Sumbagsel'            then 'AREA 1'
    when a.initiated_group = '05. Area Jabotabek Jabar' then 'AREA 2'
    when a.initiated_group = '06. Jabotabek West'       then 'AREA 2'
    when a.initiated_group = '07. Jabotabek Central'    then 'AREA 2'
    when a.initiated_group = '08. Jabotabek East'       then 'AREA 2'
    when a.initiated_group = '09. Jabar'                then 'AREA 2'
    when a.initiated_group = '10. Area Jawa Bali'       then 'AREA 3'
    when a.initiated_group = '11. Jateng'               then 'AREA 3'
    when a.initiated_group = '12. Jatim'                then 'AREA 3'
    when a.initiated_group = '13. Balinusra'            then 'AREA 3'
    when a.initiated_group = '14. Area Pamasuka'        then 'AREA 4'
    when a.initiated_group = '15. Kalimantan'           then 'AREA 4'
    when a.initiated_group = '16. Sulawesi'             then 'AREA 4'
    when a.initiated_group = '17. Puma'                 then 'AREA 4'
    when a.initiated_group = '18. HQ'                   then 'HQ'
end as area,
count(distinct msisdn) unique_target,
count(distinct case when taker > 0 and iscontrolgroup = 'false' then msisdn end) unique_taker
from ${table-prefix}${cms-schema}.${table-prefix}cms_campaign_tracking_detail a 
left join 
(
	select distinct campaign_objective_id, campaign_objective_desc
	from ${table-prefix}${cms-schema}.${table-prefix}cms_campaign_objective
) c on a.campaign_objective_id = c.campaign_objective_id
where a.pipeline_step not in ('cms_usage_detail_insert', 'cms_campaign_digital_detail_insert')
and 
(
   	(trx_date between date(datetime(dateadd('day', 1, last_day(date('${run-date=yyyy-MM-dd}') - interval '3' month)))) and date(date('${run-date=yyyy-MM-dd}') - interval '2' month))
	or (trx_date between date(datetime(dateadd('day', 1, last_day(date('${run-date=yyyy-MM-dd}') - interval '2' month)))) and date(date('${run-date=yyyy-MM-dd}') - interval '1' month))
	or (trx_date between date(datetime(dateadd('day', 1, last_day(date('${run-date=yyyy-MM-dd}') - interval '1' month)))) and date(date('${run-date=yyyy-MM-dd}')))
)
group by 1,4,5,6
) b on a.periode = b.periode 
and a.eom = b.eom  
and a.brand = b.brand 
and a.theme = b.theme
and a.category = b.category
and a.area = b.area;



insert into ${table-prefix}${cms-schema}.${table-prefix}cms_performance_area
select a.periode, a.eom, a.brand, a.theme, a.category, a.area, a.tot_campaign, a.target, a.eligible, a.taker, b.unique_target, b.unique_taker, 
a.revenue, a.recharge, a.control, a.taker_control, a.revenue_control, a.uplift, a.last_trx_date
from 
(
select periode, eom, 'Total' brand, campaign_objective_desc as theme, 'Total' category, area, 
count(distinct campaign_id) as tot_campaign,
sum(target) as target, 
sum(eligible) as eligible,
sum(taker) as taker,
sum(case when campaign_group_id in (1,4,6,7) then revenue_taker else 0 end) revenue,
sum(case when campaign_group_id = 2 then revenue_taker else 0 end) recharge,
sum(control) as control,
sum(taker_control) as taker_control,
sum(case when campaign_group_id in (1,4,6,7) then revenue_control else 0 end) revenue_control,
sum((((arpu_target_after - arpu_target_before) - (arpu_control_after - arpu_control_before))/nullif((arpu_target_before * 1.0),0)) * eligible)/nullif(sum(eligible),0) uplift, 
max(last_trx_date) as last_trx_date
from ${table-prefix}${cms-schema}.${table-prefix}cms_summary_bycampaignid
where eom = 0
group by 1,2,4,6 
) a left join 
(
select date(datetime(last_day(trx_date))) as periode, 0 eom, 'Total' brand, c.campaign_objective_desc theme, 'Total' category, 
case
    when a.initiated_group = '01. Area Sumatra'         then 'AREA 1'
    when a.initiated_group = '02. Sumbagut'             then 'AREA 1'
    when a.initiated_group = '03. Sumbagteng'           then 'AREA 1'
    when a.initiated_group = '04. Sumbagsel'            then 'AREA 1'
    when a.initiated_group = '05. Area Jabotabek Jabar' then 'AREA 2'
    when a.initiated_group = '06. Jabotabek West'       then 'AREA 2'
    when a.initiated_group = '07. Jabotabek Central'    then 'AREA 2'
    when a.initiated_group = '08. Jabotabek East'       then 'AREA 2'
    when a.initiated_group = '09. Jabar'                then 'AREA 2'
    when a.initiated_group = '10. Area Jawa Bali'       then 'AREA 3'
    when a.initiated_group = '11. Jateng'               then 'AREA 3'
    when a.initiated_group = '12. Jatim'                then 'AREA 3'
    when a.initiated_group = '13. Balinusra'            then 'AREA 3'
    when a.initiated_group = '14. Area Pamasuka'        then 'AREA 4'
    when a.initiated_group = '15. Kalimantan'           then 'AREA 4'
    when a.initiated_group = '16. Sulawesi'             then 'AREA 4'
    when a.initiated_group = '17. Puma'                 then 'AREA 4'
    when a.initiated_group = '18. HQ'                   then 'HQ'
end as area,
count(distinct msisdn) unique_target,
count(distinct case when taker > 0 and iscontrolgroup = 'false' then msisdn end) unique_taker
from ${table-prefix}${cms-schema}.${table-prefix}cms_campaign_tracking_detail a 
left join 
(
	select distinct campaign_objective_id, campaign_objective_desc
	from ${table-prefix}${cms-schema}.${table-prefix}cms_campaign_objective
) c on a.campaign_objective_id = c.campaign_objective_id
where a.pipeline_step not in ('cms_usage_detail_insert', 'cms_campaign_digital_detail_insert')
and 
(
   	(trx_date between date(datetime(dateadd('day', 1, last_day(date('${run-date=yyyy-MM-dd}') - interval '3' month)))) and date(date('${run-date=yyyy-MM-dd}') - interval '2' month))
	or (trx_date between date(datetime(dateadd('day', 1, last_day(date('${run-date=yyyy-MM-dd}') - interval '2' month)))) and date(date('${run-date=yyyy-MM-dd}') - interval '1' month))
	or (trx_date between date(datetime(dateadd('day', 1, last_day(date('${run-date=yyyy-MM-dd}') - interval '1' month)))) and date(date('${run-date=yyyy-MM-dd}')))
)
group by 1,4,6
) b on a.periode = b.periode 
and a.eom = b.eom  
and a.brand = b.brand 
and a.theme = b.theme
and a.category = b.category
and a.area = b.area;



insert into ${table-prefix}${cms-schema}.${table-prefix}cms_performance_area
select a.periode, a.eom, a.brand, a.theme, a.category, a.area, a.tot_campaign, a.target, a.eligible, a.taker, b.unique_target, b.unique_taker, 
a.revenue, a.recharge, a.control, a.taker_control, a.revenue_control, a.uplift, a.last_trx_date
from 
(
select periode, eom, brand, campaign_objective_desc as theme, 'Total' category, area, 
count(distinct campaign_id) as tot_campaign,
sum(target) as target, 
sum(eligible) as eligible,
sum(taker) as taker,
sum(case when campaign_group_id in (1,4,6,7) then revenue_taker else 0 end) revenue,
sum(case when campaign_group_id = 2 then revenue_taker else 0 end) recharge,
sum(control) as control,
sum(taker_control) as taker_control,
sum(case when campaign_group_id in (1,4,6,7) then revenue_control else 0 end) revenue_control,
sum((((arpu_target_after - arpu_target_before) - (arpu_control_after - arpu_control_before))/nullif((arpu_target_before * 1.0),0)) * eligible)/nullif(sum(eligible),0) uplift, 
max(last_trx_date) as last_trx_date
from ${table-prefix}${cms-schema}.${table-prefix}cms_summary_bycampaignid
where eom = 0
group by 1,2,3,4,6 
) a left join 
(
select date(datetime(last_day(trx_date))) as periode, 0 eom,
case
	when upper(substring(a.campaign_id, 11, 1)) = 'A' then 'Simpati'
	when upper(substring(a.campaign_id, 11, 1)) = 'B' then 'KartuAs'
	when upper(substring(a.campaign_id, 11, 1)) = 'C' then 'AllPrepaid'
	when upper(substring(a.campaign_id, 11, 1)) = 'D' then 'KartuHALO'
	when upper(substring(a.campaign_id, 11, 1)) = 'E' then 'AllBrands'
	when upper(substring(a.campaign_id, 11, 1)) = 'F' then 'Loop'
end as brand,
c.campaign_objective_desc theme, 'Total' category, 
case
    when a.initiated_group = '01. Area Sumatra'         then 'AREA 1'
    when a.initiated_group = '02. Sumbagut'             then 'AREA 1'
    when a.initiated_group = '03. Sumbagteng'           then 'AREA 1'
    when a.initiated_group = '04. Sumbagsel'            then 'AREA 1'
    when a.initiated_group = '05. Area Jabotabek Jabar' then 'AREA 2'
    when a.initiated_group = '06. Jabotabek West'       then 'AREA 2'
    when a.initiated_group = '07. Jabotabek Central'    then 'AREA 2'
    when a.initiated_group = '08. Jabotabek East'       then 'AREA 2'
    when a.initiated_group = '09. Jabar'                then 'AREA 2'
    when a.initiated_group = '10. Area Jawa Bali'       then 'AREA 3'
    when a.initiated_group = '11. Jateng'               then 'AREA 3'
    when a.initiated_group = '12. Jatim'                then 'AREA 3'
    when a.initiated_group = '13. Balinusra'            then 'AREA 3'
    when a.initiated_group = '14. Area Pamasuka'        then 'AREA 4'
    when a.initiated_group = '15. Kalimantan'           then 'AREA 4'
    when a.initiated_group = '16. Sulawesi'             then 'AREA 4'
    when a.initiated_group = '17. Puma'                 then 'AREA 4'
    when a.initiated_group = '18. HQ'                   then 'HQ'
end as area,
count(distinct msisdn) unique_target,
count(distinct case when taker > 0 and iscontrolgroup = 'false' then msisdn end) unique_taker
from ${table-prefix}${cms-schema}.${table-prefix}cms_campaign_tracking_detail a 
left join 
(
	select distinct campaign_objective_id, campaign_objective_desc
	from ${table-prefix}${cms-schema}.${table-prefix}cms_campaign_objective
) c on a.campaign_objective_id = c.campaign_objective_id
where a.pipeline_step not in ('cms_usage_detail_insert', 'cms_campaign_digital_detail_insert')
and 
(
   	(trx_date between date(datetime(dateadd('day', 1, last_day(date('${run-date=yyyy-MM-dd}') - interval '3' month)))) and date(date('${run-date=yyyy-MM-dd}') - interval '2' month))
	or (trx_date between date(datetime(dateadd('day', 1, last_day(date('${run-date=yyyy-MM-dd}') - interval '2' month)))) and date(date('${run-date=yyyy-MM-dd}') - interval '1' month))
	or (trx_date between date(datetime(dateadd('day', 1, last_day(date('${run-date=yyyy-MM-dd}') - interval '1' month)))) and date(date('${run-date=yyyy-MM-dd}')))
)
group by 1,3,4,6
) b on a.periode = b.periode 
and a.eom = b.eom  
and a.brand = b.brand 
and a.theme = b.theme
and a.category = b.category
and a.area = b.area;


insert into ${table-prefix}${cms-schema}.${table-prefix}cms_performance_area
select a.periode, a.eom, a.brand, a.theme, a.category, a.area, a.tot_campaign, a.target, a.eligible, a.taker, b.unique_target, b.unique_taker, 
a.revenue, a.recharge, a.control, a.taker_control, a.revenue_control, a.uplift, a.last_trx_date
from 
(
select periode, eom, brand, 'Total' theme, 'Total' category, area, 
count(distinct campaign_id) as tot_campaign,
sum(target) as target, 
sum(eligible) as eligible,
sum(taker) as taker,
sum(case when campaign_group_id in (1,4,6,7) then revenue_taker else 0 end) revenue,
sum(case when campaign_group_id = 2 then revenue_taker else 0 end) recharge,
sum(control) as control,
sum(taker_control) as taker_control,
sum(case when campaign_group_id in (1,4,6,7) then revenue_control else 0 end) revenue_control,
sum((((arpu_target_after - arpu_target_before) - (arpu_control_after - arpu_control_before))/nullif((arpu_target_before * 1.0),0)) * eligible)/nullif(sum(eligible),0) uplift, 
max(last_trx_date) as last_trx_date
from ${table-prefix}${cms-schema}.${table-prefix}cms_summary_bycampaignid
where eom = 0
group by 1,2,3,6 
) a left join 
(
select date(datetime(last_day(trx_date))) as periode, 0 eom, 
case
	when upper(substring(a.campaign_id, 11, 1)) = 'A' then 'Simpati'
	when upper(substring(a.campaign_id, 11, 1)) = 'B' then 'KartuAs'
	when upper(substring(a.campaign_id, 11, 1)) = 'C' then 'AllPrepaid'
	when upper(substring(a.campaign_id, 11, 1)) = 'D' then 'KartuHALO'
	when upper(substring(a.campaign_id, 11, 1)) = 'E' then 'AllBrands'
	when upper(substring(a.campaign_id, 11, 1)) = 'F' then 'Loop'
end as brand,
'Total' theme, 'Total' category, 
case
    when a.initiated_group = '01. Area Sumatra'         then 'AREA 1'
    when a.initiated_group = '02. Sumbagut'             then 'AREA 1'
    when a.initiated_group = '03. Sumbagteng'           then 'AREA 1'
    when a.initiated_group = '04. Sumbagsel'            then 'AREA 1'
    when a.initiated_group = '05. Area Jabotabek Jabar' then 'AREA 2'
    when a.initiated_group = '06. Jabotabek West'       then 'AREA 2'
    when a.initiated_group = '07. Jabotabek Central'    then 'AREA 2'
    when a.initiated_group = '08. Jabotabek East'       then 'AREA 2'
    when a.initiated_group = '09. Jabar'                then 'AREA 2'
    when a.initiated_group = '10. Area Jawa Bali'       then 'AREA 3'
    when a.initiated_group = '11. Jateng'               then 'AREA 3'
    when a.initiated_group = '12. Jatim'                then 'AREA 3'
    when a.initiated_group = '13. Balinusra'            then 'AREA 3'
    when a.initiated_group = '14. Area Pamasuka'        then 'AREA 4'
    when a.initiated_group = '15. Kalimantan'           then 'AREA 4'
    when a.initiated_group = '16. Sulawesi'             then 'AREA 4'
    when a.initiated_group = '17. Puma'                 then 'AREA 4'
    when a.initiated_group = '18. HQ'                   then 'HQ'
end as area,
count(distinct msisdn) unique_target,
count(distinct case when taker > 0 and iscontrolgroup = 'false' then msisdn end) unique_taker
from ${table-prefix}${cms-schema}.${table-prefix}cms_campaign_tracking_detail a 
left join 
(
	select distinct campaign_objective_id, campaign_objective_desc
	from ${table-prefix}${cms-schema}.${table-prefix}cms_campaign_objective
) c on a.campaign_objective_id = c.campaign_objective_id
where a.pipeline_step not in ('cms_usage_detail_insert', 'cms_campaign_digital_detail_insert')
and 
(
   	(trx_date between date(datetime(dateadd('day', 1, last_day(date('${run-date=yyyy-MM-dd}') - interval '3' month)))) and date(date('${run-date=yyyy-MM-dd}') - interval '2' month))
	or (trx_date between date(datetime(dateadd('day', 1, last_day(date('${run-date=yyyy-MM-dd}') - interval '2' month)))) and date(date('${run-date=yyyy-MM-dd}') - interval '1' month))
	or (trx_date between date(datetime(dateadd('day', 1, last_day(date('${run-date=yyyy-MM-dd}') - interval '1' month)))) and date(date('${run-date=yyyy-MM-dd}')))
)
group by 1,3,6
) b on a.periode = b.periode 
and a.eom = b.eom  
and a.brand = b.brand 
and a.theme = b.theme
and a.category = b.category
and a.area = b.area;


insert into ${table-prefix}${cms-schema}.${table-prefix}cms_performance_area
select a.periode, a.eom, a.brand, a.theme, a.category, a.area, a.tot_campaign, a.target, a.eligible, a.taker, b.unique_target, b.unique_taker, 
a.revenue, a.recharge, a.control, a.taker_control, a.revenue_control, a.uplift, a.last_trx_date
from 
(
select periode, eom, brand, 'Total' theme, category, area, 
count(distinct campaign_id) as tot_campaign,
sum(target) as target, 
sum(eligible) as eligible,
sum(taker) as taker,
sum(case when campaign_group_id in (1,4,6,7) then revenue_taker else 0 end) revenue,
sum(case when campaign_group_id = 2 then revenue_taker else 0 end) recharge,
sum(control) as control,
sum(taker_control) as taker_control,
sum(case when campaign_group_id in (1,4,6,7) then revenue_control else 0 end) revenue_control,
sum((((arpu_target_after - arpu_target_before) - (arpu_control_after - arpu_control_before))/nullif((arpu_target_before * 1.0),0)) * eligible)/nullif(sum(eligible),0) uplift, 
max(last_trx_date) as last_trx_date
from ${table-prefix}${cms-schema}.${table-prefix}cms_summary_bycampaignid
where eom = 0
group by 1,2,3,5,6 
) a left join 
(
select date(datetime(last_day(trx_date))) as periode, 0 eom, 
case
	when upper(substring(a.campaign_id, 11, 1)) = 'A' then 'Simpati'
	when upper(substring(a.campaign_id, 11, 1)) = 'B' then 'KartuAs'
	when upper(substring(a.campaign_id, 11, 1)) = 'C' then 'AllPrepaid'
	when upper(substring(a.campaign_id, 11, 1)) = 'D' then 'KartuHALO'
	when upper(substring(a.campaign_id, 11, 1)) = 'E' then 'AllBrands'
	when upper(substring(a.campaign_id, 11, 1)) = 'F' then 'Loop'
end as brand,
'Total' theme, a.category, 
case
    when a.initiated_group = '01. Area Sumatra'         then 'AREA 1'
    when a.initiated_group = '02. Sumbagut'             then 'AREA 1'
    when a.initiated_group = '03. Sumbagteng'           then 'AREA 1'
    when a.initiated_group = '04. Sumbagsel'            then 'AREA 1'
    when a.initiated_group = '05. Area Jabotabek Jabar' then 'AREA 2'
    when a.initiated_group = '06. Jabotabek West'       then 'AREA 2'
    when a.initiated_group = '07. Jabotabek Central'    then 'AREA 2'
    when a.initiated_group = '08. Jabotabek East'       then 'AREA 2'
    when a.initiated_group = '09. Jabar'                then 'AREA 2'
    when a.initiated_group = '10. Area Jawa Bali'       then 'AREA 3'
    when a.initiated_group = '11. Jateng'               then 'AREA 3'
    when a.initiated_group = '12. Jatim'                then 'AREA 3'
    when a.initiated_group = '13. Balinusra'            then 'AREA 3'
    when a.initiated_group = '14. Area Pamasuka'        then 'AREA 4'
    when a.initiated_group = '15. Kalimantan'           then 'AREA 4'
    when a.initiated_group = '16. Sulawesi'             then 'AREA 4'
    when a.initiated_group = '17. Puma'                 then 'AREA 4'
    when a.initiated_group = '18. HQ'                   then 'HQ'
end as area,
count(distinct msisdn) unique_target,
count(distinct case when taker > 0 and iscontrolgroup = 'false' then msisdn end) unique_taker
from ${table-prefix}${cms-schema}.${table-prefix}cms_campaign_tracking_detail a 
left join 
(
	select distinct campaign_objective_id, campaign_objective_desc
	from ${table-prefix}${cms-schema}.${table-prefix}cms_campaign_objective
) c on a.campaign_objective_id = c.campaign_objective_id
where a.pipeline_step not in ('cms_usage_detail_insert', 'cms_campaign_digital_detail_insert')
and 
(
   	(trx_date between date(datetime(dateadd('day', 1, last_day(date('${run-date=yyyy-MM-dd}') - interval '3' month)))) and date(date('${run-date=yyyy-MM-dd}') - interval '2' month))
	or (trx_date between date(datetime(dateadd('day', 1, last_day(date('${run-date=yyyy-MM-dd}') - interval '2' month)))) and date(date('${run-date=yyyy-MM-dd}') - interval '1' month))
	or (trx_date between date(datetime(dateadd('day', 1, last_day(date('${run-date=yyyy-MM-dd}') - interval '1' month)))) and date(date('${run-date=yyyy-MM-dd}')))
)
group by 1,3,5,6
) b on a.periode = b.periode 
and a.eom = b.eom  
and a.brand = b.brand 
and a.theme = b.theme
and a.category = b.category
and a.area = b.area;




