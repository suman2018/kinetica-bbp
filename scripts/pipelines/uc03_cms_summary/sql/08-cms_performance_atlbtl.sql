
--- update last month data

delete from ${table-prefix}${cms-schema}.${table-prefix}cms_performance_atlbtl
where date(periode) = date(datetime(last_day(date('${run-date=yyyy-MM-dd}') - interval '1' month)));


insert into ${table-prefix}${cms-schema}.${table-prefix}cms_performance_atlbtl 
select a.periode, a.eom, a.brand, a.theme, a.category, a.area, a.is_btl, a.tot_campaign, a.target, a.eligible, a.taker, a.taker_usage, a.taker_recharge, 
a.revenue, a.recharge, a.control, a.taker_control, a.revenue_control, a.uplift, a.last_trx_date
from 
(
select periode, 1 eom, brand, campaign_objective_desc as theme, category, area, is_btl,  
count(distinct campaign_id) as tot_campaign,
sum(target) as target, 
sum(eligible) as eligible,
sum(taker) as taker,
sum(case when campaign_group_id in (1,4,6,7) then taker else 0 end) taker_usage,
sum(case when campaign_group_id = 2 then taker else 0 end) taker_recharge,
sum(case when campaign_group_id in (1,4,6,7) then revenue_taker else 0 end) revenue,
sum(case when campaign_group_id = 2 then revenue_taker else 0 end) recharge,
sum(control) as control,
sum(taker_control) as taker_control,
sum(case when campaign_group_id in (1,4,6,7) then revenue_control else 0 end) revenue_control,
sum((((arpu_target_after - arpu_target_before) - (arpu_control_after - arpu_control_before))/nullif((arpu_target_before * 1.0),0)) * eligible)/nullif(sum(eligible),0) uplift, 
max(last_trx_date) as last_trx_date
from ${table-prefix}${cms-schema}.${table-prefix}cms_summary_bycampaignid
where date(periode) = date(datetime(last_day(date('${run-date=yyyy-MM-dd}') - interval '1' month)))
and eom = 1
group by 1,3,4,5,6,7
) a ;



--- update last 3 months data eom


delete from ${table-prefix}${cms-schema}.${table-prefix}cms_performance_atlbtl
where eom = 0;


insert into ${table-prefix}${cms-schema}.${table-prefix}cms_performance_atlbtl 
select a.periode, a.eom, a.brand, a.theme, a.category, a.area, a.is_btl, a.tot_campaign, a.target, a.eligible, a.taker, a.taker_usage, a.taker_recharge, 
a.revenue, a.recharge, a.control, a.taker_control, a.revenue_control, a.uplift, a.last_trx_date
from 
(
select periode, eom, brand, campaign_objective_desc as theme, category, area, is_btl,  
count(distinct campaign_id) as tot_campaign,
sum(target) as target, 
sum(eligible) as eligible,
sum(taker) as taker,
sum(case when campaign_group_id in (1,4,6,7) then taker else 0 end) taker_usage,
sum(case when campaign_group_id = 2 then taker else 0 end) taker_recharge,
sum(case when campaign_group_id in (1,4,6,7) then revenue_taker else 0 end) revenue,
sum(case when campaign_group_id = 2 then revenue_taker else 0 end) recharge,
sum(control) as control,
sum(taker_control) as taker_control,
sum(case when campaign_group_id in (1,4,6,7) then revenue_control else 0 end) revenue_control,
sum((((arpu_target_after - arpu_target_before) - (arpu_control_after - arpu_control_before))/nullif((arpu_target_before * 1.0),0)) * eligible)/nullif(sum(eligible),0) uplift, 
max(last_trx_date) as last_trx_date
from ${table-prefix}${cms-schema}.${table-prefix}cms_summary_bycampaignid
where eom = 0
group by 1,2,3,4,5,6,7
) a ;





