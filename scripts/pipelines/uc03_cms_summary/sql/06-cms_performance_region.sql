
--- delete last month data

delete from ${table-prefix}${cms-schema}.${table-prefix}cms_performance_region
where date(periode) = date(datetime(last_day(date('${run-date=yyyy-MM-dd}') - interval '1' month)));


insert into ${table-prefix}${cms-schema}.${table-prefix}cms_performance_region 
select a.periode, a.eom, a.brand, a.theme, a.category, a.initiated_business, a.tot_campaign, a.target, a.eligible, a.taker, b.unique_target, b.unique_taker, 
a.revenue, a.recharge, a.control, a.taker_control, a.revenue_control, a.uplift, a.last_trx_date
from 
(
select periode, 1 eom, brand, campaign_objective_desc as theme, category, initiated_business, 
count(distinct campaign_id) as tot_campaign,
sum(target) as target, 
sum(eligible) as eligible,
sum(taker) as taker,
sum(case when campaign_group_id in (1,4,6,7) then revenue_taker else 0 end) revenue,
sum(case when campaign_group_id = 2 then revenue_taker else 0 end) recharge,
sum(control) as control,
sum(taker_control) as taker_control,
sum(case when campaign_group_id in (1,4,6,7) then revenue_control else 0 end) revenue_control,
sum((((arpu_target_after - arpu_target_before) - (arpu_control_after - arpu_control_before))/nullif((arpu_target_before * 1.0),0)) * eligible)/nullif(sum(eligible),0) uplift, 
max(last_trx_date) as last_trx_date
from ${table-prefix}${cms-schema}.${table-prefix}cms_summary_bycampaignid
where periode = date(datetime(last_day(date('${run-date=yyyy-MM-dd}') - interval '1' month)))
and eom = 1
group by 1,3,4,5,6 
) a left join 
(
select date(datetime(last_day(trx_date))) as periode, 1 eom, 
case
	when upper(substring(a.campaign_id, 11, 1)) = 'A' then 'Simpati'
	when upper(substring(a.campaign_id, 11, 1)) = 'B' then 'KartuAs'
	when upper(substring(a.campaign_id, 11, 1)) = 'C' then 'AllPrepaid'
	when upper(substring(a.campaign_id, 11, 1)) = 'D' then 'KartuHALO'
	when upper(substring(a.campaign_id, 11, 1)) = 'E' then 'AllBrands'
	when upper(substring(a.campaign_id, 11, 1)) = 'F' then 'Loop'
end as brand, c.campaign_objective_desc theme, a.category, a.initiated_group as initiated_business,
count(distinct msisdn) unique_target,
count(distinct case when taker > 0 and iscontrolgroup = 'false' then msisdn end) unique_taker
from ${table-prefix}${cms-schema}.${table-prefix}cms_campaign_tracking_detail a 
left join 
(
	select distinct campaign_objective_id, campaign_objective_desc
	from ${table-prefix}${cms-schema}.${table-prefix}cms_campaign_objective
) c on a.campaign_objective_id = c.campaign_objective_id
where a.pipeline_step not in ('cms_usage_detail_insert', 'cms_campaign_digital_detail_insert')
and (trx_date between date(datetime(dateadd('day', 1, last_day(date('${run-date=yyyy-MM-dd}') - interval '2' month)))) 
and date(datetime(last_day(date('${run-date=yyyy-MM-dd}') - interval '1' month))))
group by 1,3,4,5,6
) b on a.periode = b.periode 
and a.eom = b.eom  
and a.brand = b.brand 
and a.theme = b.theme
and a.category = b.category
and a.initiated_business = b.initiated_business;

insert into ${table-prefix}${cms-schema}.${table-prefix}cms_performance_region
select a.periode, a.eom, a.brand, a.theme, a.category, a.initiated_business, a.tot_campaign, a.target, a.eligible, a.taker, b.unique_target, b.unique_taker, 
a.revenue, a.recharge, a.control, a.taker_control, a.revenue_control, a.uplift, a.last_trx_date
from 
(
select periode, 1 eom, 'Total' brand, 'Total' theme, 'Total' category, initiated_business, 
count(distinct campaign_id) as tot_campaign,
sum(target) as target, 
sum(eligible) as eligible,
sum(taker) as taker,
sum(case when campaign_group_id in (1,4,6,7) then revenue_taker else 0 end) revenue,
sum(case when campaign_group_id = 2 then revenue_taker else 0 end) recharge,
sum(control) as control,
sum(taker_control) as taker_control,
sum(case when campaign_group_id in (1,4,6,7) then revenue_control else 0 end) revenue_control,
sum((((arpu_target_after - arpu_target_before) - (arpu_control_after - arpu_control_before))/nullif((arpu_target_before * 1.0),0)) * eligible)/nullif(sum(eligible),0) uplift, 
max(last_trx_date) as last_trx_date
from ${table-prefix}${cms-schema}.${table-prefix}cms_summary_bycampaignid
where periode = date(datetime(last_day(date('${run-date=yyyy-MM-dd}') - interval '1' month)))
and eom = 1
group by 1,6 
) a left join 
(
select date(datetime(last_day(trx_date))) as periode, 1 eom, 'Total' brand, 'Total' theme, 'Total' category, 
a.initiated_group as initiated_business,
count(distinct msisdn) unique_target,
count(distinct case when taker > 0 and iscontrolgroup = 'false' then msisdn end) unique_taker
from ${table-prefix}${cms-schema}.${table-prefix}cms_campaign_tracking_detail a 
left join 
(
	select distinct campaign_objective_id, campaign_objective_desc
	from ${table-prefix}${cms-schema}.${table-prefix}cms_campaign_objective
) c on a.campaign_objective_id = c.campaign_objective_id
where a.pipeline_step not in ('cms_usage_detail_insert', 'cms_campaign_digital_detail_insert')
and (trx_date between date(datetime(dateadd('day', 1, last_day(date('${run-date=yyyy-MM-dd}') - interval '2' month)))) 
and date(datetime(last_day(date('${run-date=yyyy-MM-dd}') - interval '1' month))))
group by 1,6
) b on a.periode = b.periode 
and a.eom = b.eom  
and a.brand = b.brand 
and a.theme = b.theme
and a.category = b.category
and a.initiated_business = b.initiated_business;


insert into ${table-prefix}${cms-schema}.${table-prefix}cms_performance_region
select a.periode, a.eom, a.brand, a.theme, a.category, a.initiated_business, a.tot_campaign, a.target, a.eligible, a.taker, b.unique_target, b.unique_taker, 
a.revenue, a.recharge, a.control, a.taker_control, a.revenue_control, a.uplift, a.last_trx_date
from 
(
select periode, 1 eom, 'Total' brand, 'Total' theme, category, initiated_business, 
count(distinct campaign_id) as tot_campaign,
sum(target) as target, 
sum(eligible) as eligible,
sum(taker) as taker,
sum(case when campaign_group_id in (1,4,6,7) then revenue_taker else 0 end) revenue,
sum(case when campaign_group_id = 2 then revenue_taker else 0 end) recharge,
sum(control) as control,
sum(taker_control) as taker_control,
sum(case when campaign_group_id in (1,4,6,7) then revenue_control else 0 end) revenue_control,
sum((((arpu_target_after - arpu_target_before) - (arpu_control_after - arpu_control_before))/nullif((arpu_target_before * 1.0),0)) * eligible)/nullif(sum(eligible),0) uplift, 
max(last_trx_date) as last_trx_date
from ${table-prefix}${cms-schema}.${table-prefix}cms_summary_bycampaignid
where periode = date(datetime(last_day(date('${run-date=yyyy-MM-dd}') - interval '1' month)))
and eom = 1
group by 1,5,6 
) a left join 
(
select date(datetime(last_day(trx_date))) as periode, 1 eom, 'Total' brand, 'Total' theme, a.category, 
a.initiated_group as initiated_business,
count(distinct msisdn) unique_target,
count(distinct case when taker > 0 and iscontrolgroup = 'false' then msisdn end) unique_taker
from ${table-prefix}${cms-schema}.${table-prefix}cms_campaign_tracking_detail a 
left join 
(
	select distinct campaign_objective_id, campaign_objective_desc
	from ${table-prefix}${cms-schema}.${table-prefix}cms_campaign_objective
) c on a.campaign_objective_id = c.campaign_objective_id
where a.pipeline_step not in ('cms_usage_detail_insert', 'cms_campaign_digital_detail_insert')
and (trx_date between date(datetime(dateadd('day', 1, last_day(date('${run-date=yyyy-MM-dd}') - interval '2' month)))) 
and date(datetime(last_day(date('${run-date=yyyy-MM-dd}') - interval '1' month))))
group by 1,5,6
) b on a.periode = b.periode 
and a.eom = b.eom  
and a.brand = b.brand 
and a.theme = b.theme
and a.category = b.category
and a.initiated_business = b.initiated_business;


insert into ${table-prefix}${cms-schema}.${table-prefix}cms_performance_region
select a.periode, a.eom, a.brand, a.theme, a.category, a.initiated_business, a.tot_campaign, a.target, a.eligible, a.taker, b.unique_target, b.unique_taker, 
a.revenue, a.recharge, a.control, a.taker_control, a.revenue_control, a.uplift, a.last_trx_date
from 
(
select periode, 1 eom, 'Total' brand, campaign_objective_desc as theme, category, initiated_business, 
count(distinct campaign_id) as tot_campaign,
sum(target) as target, 
sum(eligible) as eligible,
sum(taker) as taker,
sum(case when campaign_group_id in (1,4,6,7) then revenue_taker else 0 end) revenue,
sum(case when campaign_group_id = 2 then revenue_taker else 0 end) recharge,
sum(control) as control,
sum(taker_control) as taker_control,
sum(case when campaign_group_id in (1,4,6,7) then revenue_control else 0 end) revenue_control,
sum((((arpu_target_after - arpu_target_before) - (arpu_control_after - arpu_control_before))/nullif((arpu_target_before * 1.0),0)) * eligible)/nullif(sum(eligible),0) uplift, 
max(last_trx_date) as last_trx_date
from ${table-prefix}${cms-schema}.${table-prefix}cms_summary_bycampaignid
where periode = date(datetime(last_day(date('${run-date=yyyy-MM-dd}') - interval '1' month)))
and eom = 1
group by 1,4,5,6 
) a left join 
(
select date(datetime(last_day(trx_date))) as periode, 1 eom, 'Total' brand, c.campaign_objective_desc theme, a.category, 
a.initiated_group as initiated_business,
count(distinct msisdn) unique_target,
count(distinct case when taker > 0 and iscontrolgroup = 'false' then msisdn end) unique_taker
from ${table-prefix}${cms-schema}.${table-prefix}cms_campaign_tracking_detail a 
left join 
(
	select distinct campaign_objective_id, campaign_objective_desc
	from ${table-prefix}${cms-schema}.${table-prefix}cms_campaign_objective
) c on a.campaign_objective_id = c.campaign_objective_id
where a.pipeline_step not in ('cms_usage_detail_insert', 'cms_campaign_digital_detail_insert')
and (trx_date between date(datetime(dateadd('day', 1, (last_day(date('${run-date=yyyy-MM-dd}') - interval '2' month))))) 
and date(datetime(last_day(date('${run-date=yyyy-MM-dd}') - interval '1' month))))
group by 1,4,5,6
) b on a.periode = b.periode 
and a.eom = b.eom  
and a.brand = b.brand 
and a.theme = b.theme
and a.category = b.category
and a.initiated_business = b.initiated_business;



insert into ${table-prefix}${cms-schema}.${table-prefix}cms_performance_region
select a.periode, a.eom, a.brand, a.theme, a.category, a.initiated_business, a.tot_campaign, a.target, a.eligible, a.taker, b.unique_target, b.unique_taker, 
a.revenue, a.recharge, a.control, a.taker_control, a.revenue_control, a.uplift, a.last_trx_date
from 
(
select periode, 1 eom, 'Total' brand, campaign_objective_desc as theme, 'Total' category, initiated_business, 
count(distinct campaign_id) as tot_campaign,
sum(target) as target, 
sum(eligible) as eligible,
sum(taker) as taker,
sum(case when campaign_group_id in (1,4,6,7) then revenue_taker else 0 end) revenue,
sum(case when campaign_group_id = 2 then revenue_taker else 0 end) recharge,
sum(control) as control,
sum(taker_control) as taker_control,
sum(case when campaign_group_id in (1,4,6,7) then revenue_control else 0 end) revenue_control,
sum((((arpu_target_after - arpu_target_before) - (arpu_control_after - arpu_control_before))/nullif((arpu_target_before * 1.0),0)) * eligible)/nullif(sum(eligible),0) uplift, 
max(last_trx_date) as last_trx_date
from ${table-prefix}${cms-schema}.${table-prefix}cms_summary_bycampaignid
where periode = date(datetime(last_day(date('${run-date=yyyy-MM-dd}') - interval '1' month)))
and eom = 1
group by 1,4,6 
) a left join 
(
select date(datetime(last_day(trx_date))) as periode, 1 eom, 'Total' brand, c.campaign_objective_desc theme, 'Total' category, 
a.initiated_group as initiated_business,
count(distinct msisdn) unique_target,
count(distinct case when taker > 0 and iscontrolgroup = 'false' then msisdn end) unique_taker
from ${table-prefix}${cms-schema}.${table-prefix}cms_campaign_tracking_detail a 
left join 
(
	select distinct campaign_objective_id, campaign_objective_desc
	from ${table-prefix}${cms-schema}.${table-prefix}cms_campaign_objective
) c on a.campaign_objective_id = c.campaign_objective_id
where a.pipeline_step not in ('cms_usage_detail_insert', 'cms_campaign_digital_detail_insert')
and (trx_date between date(datetime(dateadd('day', 1, last_day(date('${run-date=yyyy-MM-dd}') - interval '2' month)))) 
and date(datetime(last_day(date('${run-date=yyyy-MM-dd}') - interval '1' month))))
group by 1,4,6
) b on a.periode = b.periode 
and a.eom = b.eom  
and a.brand = b.brand 
and a.theme = b.theme
and a.category = b.category
and a.initiated_business = b.initiated_business;



insert into ${table-prefix}${cms-schema}.${table-prefix}cms_performance_region
select a.periode, a.eom, a.brand, a.theme, a.category, a.initiated_business, a.tot_campaign, a.target, a.eligible, a.taker, b.unique_target, b.unique_taker, 
a.revenue, a.recharge, a.control, a.taker_control, a.revenue_control, a.uplift, a.last_trx_date
from 
(
select periode, 1 eom, brand, campaign_objective_desc as theme, 'Total' category, initiated_business, 
count(distinct campaign_id) as tot_campaign,
sum(target) as target, 
sum(eligible) as eligible,
sum(taker) as taker,
sum(case when campaign_group_id in (1,4,6,7) then revenue_taker else 0 end) revenue,
sum(case when campaign_group_id = 2 then revenue_taker else 0 end) recharge,
sum(control) as control,
sum(taker_control) as taker_control,
sum(case when campaign_group_id in (1,4,6,7) then revenue_control else 0 end) revenue_control,
sum((((arpu_target_after - arpu_target_before) - (arpu_control_after - arpu_control_before))/nullif((arpu_target_before * 1.0),0)) * eligible)/nullif(sum(eligible),0) uplift, 
max(last_trx_date) as last_trx_date
from ${table-prefix}${cms-schema}.${table-prefix}cms_summary_bycampaignid
where periode = date(datetime(last_day(date('${run-date=yyyy-MM-dd}') - interval '1' month)))
and eom = 1
group by 1,3,4,6 
) a left join 
(
select date(datetime(last_day(trx_date))) as periode, 1 eom,
case
	when upper(substring(a.campaign_id, 11, 1)) = 'A' then 'Simpati'
	when upper(substring(a.campaign_id, 11, 1)) = 'B' then 'KartuAs'
	when upper(substring(a.campaign_id, 11, 1)) = 'C' then 'AllPrepaid'
	when upper(substring(a.campaign_id, 11, 1)) = 'D' then 'KartuHALO'
	when upper(substring(a.campaign_id, 11, 1)) = 'E' then 'AllBrands'
	when upper(substring(a.campaign_id, 11, 1)) = 'F' then 'Loop'
end as brand,
c.campaign_objective_desc theme, 'Total' category, 
a.initiated_group as initiated_business,
count(distinct msisdn) unique_target,
count(distinct case when taker > 0 and iscontrolgroup = 'false' then msisdn end) unique_taker
from ${table-prefix}${cms-schema}.${table-prefix}cms_campaign_tracking_detail a 
left join 
(
	select distinct campaign_objective_id, campaign_objective_desc
	from ${table-prefix}${cms-schema}.${table-prefix}cms_campaign_objective
) c on a.campaign_objective_id = c.campaign_objective_id
where a.pipeline_step not in ('cms_usage_detail_insert', 'cms_campaign_digital_detail_insert')
and (trx_date between date(datetime(dateadd('day', 1, last_day(date('${run-date=yyyy-MM-dd}') - interval '2' month)))) 
and date(datetime(last_day(date('${run-date=yyyy-MM-dd}') - interval '1' month))))
group by 1,3,4,6
) b on a.periode = b.periode 
and a.eom = b.eom  
and a.brand = b.brand 
and a.theme = b.theme
and a.category = b.category
and a.initiated_business = b.initiated_business;


insert into ${table-prefix}${cms-schema}.${table-prefix}cms_performance_region
select a.periode, a.eom, a.brand, a.theme, a.category, a.initiated_business, a.tot_campaign, a.target, a.eligible, a.taker, b.unique_target, b.unique_taker, 
a.revenue, a.recharge, a.control, a.taker_control, a.revenue_control, a.uplift, a.last_trx_date
from 
(
select periode, 1 eom, brand, 'Total' theme, 'Total' category, initiated_business, 
count(distinct campaign_id) as tot_campaign,
sum(target) as target, 
sum(eligible) as eligible,
sum(taker) as taker,
sum(case when campaign_group_id in (1,4,6,7) then revenue_taker else 0 end) revenue,
sum(case when campaign_group_id = 2 then revenue_taker else 0 end) recharge,
sum(control) as control,
sum(taker_control) as taker_control,
sum(case when campaign_group_id in (1,4,6,7) then revenue_control else 0 end) revenue_control,
sum((((arpu_target_after - arpu_target_before) - (arpu_control_after - arpu_control_before))/nullif((arpu_target_before * 1.0),0)) * eligible)/nullif(sum(eligible),0) uplift, 
max(last_trx_date) as last_trx_date
from ${table-prefix}${cms-schema}.${table-prefix}cms_summary_bycampaignid
where periode = date(datetime(last_day(date('${run-date=yyyy-MM-dd}') - interval '1' month)))
and eom = 1
group by 1,3,6 
) a left join 
(
select date(datetime(last_day(trx_date))) as periode, 1 eom, 
case
	when upper(substring(a.campaign_id, 11, 1)) = 'A' then 'Simpati'
	when upper(substring(a.campaign_id, 11, 1)) = 'B' then 'KartuAs'
	when upper(substring(a.campaign_id, 11, 1)) = 'C' then 'AllPrepaid'
	when upper(substring(a.campaign_id, 11, 1)) = 'D' then 'KartuHALO'
	when upper(substring(a.campaign_id, 11, 1)) = 'E' then 'AllBrands'
	when upper(substring(a.campaign_id, 11, 1)) = 'F' then 'Loop'
end as brand,
'Total' theme, 'Total' category, 
a.initiated_group as initiated_business,
count(distinct msisdn) unique_target,
count(distinct case when taker > 0 and iscontrolgroup = 'false' then msisdn end) unique_taker
from ${table-prefix}${cms-schema}.${table-prefix}cms_campaign_tracking_detail a 
left join 
(
	select distinct campaign_objective_id, campaign_objective_desc
	from ${table-prefix}${cms-schema}.${table-prefix}cms_campaign_objective
) c on a.campaign_objective_id = c.campaign_objective_id
where a.pipeline_step not in ('cms_usage_detail_insert', 'cms_campaign_digital_detail_insert')
and (trx_date between date(datetime(dateadd('day', 1, last_day(date('${run-date=yyyy-MM-dd}') - interval '2' month)))) 
and date(datetime(last_day(date('${run-date=yyyy-MM-dd}') - interval '1' month))))
group by 1,3,6
) b on a.periode = b.periode 
and a.eom = b.eom  
and a.brand = b.brand 
and a.theme = b.theme
and a.category = b.category
and a.initiated_business = b.initiated_business;


insert into ${table-prefix}${cms-schema}.${table-prefix}cms_performance_region
select a.periode, a.eom, a.brand, a.theme, a.category, a.initiated_business, a.tot_campaign, a.target, a.eligible, a.taker, b.unique_target, b.unique_taker, 
a.revenue, a.recharge, a.control, a.taker_control, a.revenue_control, a.uplift, a.last_trx_date
from 
(
select periode, 1 eom, brand, 'Total' theme, category, initiated_business, 
count(distinct campaign_id) as tot_campaign,
sum(target) as target, 
sum(eligible) as eligible,
sum(taker) as taker,
sum(case when campaign_group_id in (1,4,6,7) then revenue_taker else 0 end) revenue,
sum(case when campaign_group_id = 2 then revenue_taker else 0 end) recharge,
sum(control) as control,
sum(taker_control) as taker_control,
sum(case when campaign_group_id in (1,4,6,7) then revenue_control else 0 end) revenue_control,
sum((((arpu_target_after - arpu_target_before) - (arpu_control_after - arpu_control_before))/nullif((arpu_target_before * 1.0),0)) * eligible)/nullif(sum(eligible),0) uplift, 
max(last_trx_date) as last_trx_date
from ${table-prefix}${cms-schema}.${table-prefix}cms_summary_bycampaignid
where periode = date(datetime(last_day(date('${run-date=yyyy-MM-dd}') - interval '1' month)))
and eom = 1
group by 1,3,5,6 
) a left join 
(
select date(datetime(last_day(trx_date))) as periode, 1 eom, 
case
	when upper(substring(a.campaign_id, 11, 1)) = 'A' then 'Simpati'
	when upper(substring(a.campaign_id, 11, 1)) = 'B' then 'KartuAs'
	when upper(substring(a.campaign_id, 11, 1)) = 'C' then 'AllPrepaid'
	when upper(substring(a.campaign_id, 11, 1)) = 'D' then 'KartuHALO'
	when upper(substring(a.campaign_id, 11, 1)) = 'E' then 'AllBrands'
	when upper(substring(a.campaign_id, 11, 1)) = 'F' then 'Loop'
end as brand,
'Total' theme, a.category, 
a.initiated_group as initiated_business,
count(distinct msisdn) unique_target,
count(distinct case when taker > 0 and iscontrolgroup = 'false' then msisdn end) unique_taker
from ${table-prefix}${cms-schema}.${table-prefix}cms_campaign_tracking_detail a 
left join 
(
	select distinct campaign_objective_id, campaign_objective_desc
	from ${table-prefix}${cms-schema}.${table-prefix}cms_campaign_objective
) c on a.campaign_objective_id = c.campaign_objective_id
where a.pipeline_step not in ('cms_usage_detail_insert', 'cms_campaign_digital_detail_insert')
and (trx_date between date(datetime(dateadd('day', 1, last_day(date('${run-date=yyyy-MM-dd}') - interval '2' month)))) 
and date(datetime(last_day(date('${run-date=yyyy-MM-dd}') - interval '1' month))))
group by 1,3,5,6
) b on a.periode = b.periode 
and a.eom = b.eom  
and a.brand = b.brand 
and a.theme = b.theme
and a.category = b.category
and a.initiated_business = b.initiated_business;


--- update last month data

delete from ${table-prefix}${cms-schema}.${table-prefix}cms_performance_region
where eom = 0;


insert into ${table-prefix}${cms-schema}.${table-prefix}cms_performance_region
select a.periode, a.eom, a.brand, a.theme, a.category, a.initiated_business, a.tot_campaign, a.target, a.eligible, a.taker, b.unique_target, b.unique_taker, 
a.revenue, a.recharge, a.control, a.taker_control, a.revenue_control, a.uplift, a.last_trx_date
from 
(
select periode, eom, brand, campaign_objective_desc as theme, category, initiated_business, 
count(distinct campaign_id) as tot_campaign,
sum(target) as target, 
sum(eligible) as eligible,
sum(taker) as taker,
sum(case when campaign_group_id in (1,4,6,7) then revenue_taker else 0 end) revenue,
sum(case when campaign_group_id = 2 then revenue_taker else 0 end) recharge,
sum(control) as control,
sum(taker_control) as taker_control,
sum(case when campaign_group_id in (1,4,6,7) then revenue_control else 0 end) revenue_control,
sum((((arpu_target_after - arpu_target_before) - (arpu_control_after - arpu_control_before))/nullif((arpu_target_before * 1.0),0)) * eligible)/nullif(sum(eligible),0) uplift, 
max(last_trx_date) as last_trx_date
from ${table-prefix}${cms-schema}.${table-prefix}cms_summary_bycampaignid
where eom = 0 
group by 1,2,3,4,5,6 
) a left join 
(
select date(datetime(last_day(trx_date))) as periode, 0 eom, 
case
	when upper(substring(a.campaign_id, 11, 1)) = 'A' then 'Simpati'
	when upper(substring(a.campaign_id, 11, 1)) = 'B' then 'KartuAs'
	when upper(substring(a.campaign_id, 11, 1)) = 'C' then 'AllPrepaid'
	when upper(substring(a.campaign_id, 11, 1)) = 'D' then 'KartuHALO'
	when upper(substring(a.campaign_id, 11, 1)) = 'E' then 'AllBrands'
	when upper(substring(a.campaign_id, 11, 1)) = 'F' then 'Loop'
end as brand, c.campaign_objective_desc theme, a.category, 
a.initiated_group as initiated_business,
count(distinct msisdn) unique_target,
count(distinct case when taker > 0 and iscontrolgroup = 'false' then msisdn end) unique_taker
from ${table-prefix}${cms-schema}.${table-prefix}cms_campaign_tracking_detail a 
left join 
(
	select distinct campaign_objective_id, campaign_objective_desc
	from ${table-prefix}${cms-schema}.${table-prefix}cms_campaign_objective
) c on a.campaign_objective_id = c.campaign_objective_id
where a.pipeline_step not in ('cms_usage_detail_insert', 'cms_campaign_digital_detail_insert')
and 
(
   	(trx_date between date(datetime(dateadd('day', 1, last_day(date('${run-date=yyyy-MM-dd}') - interval '3' month)))) 
and date(date('${run-date=yyyy-MM-dd}') - interval '2' month))
	or (trx_date between date(datetime(dateadd('day', 1, last_day(date('${run-date=yyyy-MM-dd}') - interval '2' month)))) 
and date(date('${run-date=yyyy-MM-dd}') - interval '1' month))
	or (trx_date between date(datetime(dateadd('day', 1, last_day(date('${run-date=yyyy-MM-dd}') - interval '1' month)))) 
and date(date('${run-date=yyyy-MM-dd}')))
)
group by 1,3,4,5,6
) b on a.periode = b.periode 
and a.eom = b.eom  
and a.brand = b.brand 
and a.theme = b.theme
and a.category = b.category
and a.initiated_business = b.initiated_business;

insert into ${table-prefix}${cms-schema}.${table-prefix}cms_performance_region
select a.periode, a.eom, a.brand, a.theme, a.category, a.initiated_business, a.tot_campaign, a.target, a.eligible, a.taker, b.unique_target, b.unique_taker, 
a.revenue, a.recharge, a.control, a.taker_control, a.revenue_control, a.uplift, a.last_trx_date
from 
(
select periode, eom, 'Total' brand, 'Total' theme, 'Total' category, initiated_business, 
count(distinct campaign_id) as tot_campaign,
sum(target) as target, 
sum(eligible) as eligible,
sum(taker) as taker,
sum(case when campaign_group_id in (1,4,6,7) then revenue_taker else 0 end) revenue,
sum(case when campaign_group_id = 2 then revenue_taker else 0 end) recharge,
sum(control) as control,
sum(taker_control) as taker_control,
sum(case when campaign_group_id in (1,4,6,7) then revenue_control else 0 end) revenue_control,
sum((((arpu_target_after - arpu_target_before) - (arpu_control_after - arpu_control_before))/nullif((arpu_target_before * 1.0),0)) * eligible)/nullif(sum(eligible),0) uplift, 
max(last_trx_date) as last_trx_date
from ${table-prefix}${cms-schema}.${table-prefix}cms_summary_bycampaignid
where eom = 0
group by 1,2,6 
) a left join 
(
select date(datetime(last_day(trx_date))) as periode, 0 eom, 'Total' brand, 'Total' theme, 'Total' category, 
a.initiated_group as initiated_business,
count(distinct msisdn) unique_target,
count(distinct case when taker > 0 and iscontrolgroup = 'false' then msisdn end) unique_taker
from ${table-prefix}${cms-schema}.${table-prefix}cms_campaign_tracking_detail a 
left join 
(
	select distinct campaign_objective_id, campaign_objective_desc
	from ${table-prefix}${cms-schema}.${table-prefix}cms_campaign_objective
) c on a.campaign_objective_id = c.campaign_objective_id
where a.pipeline_step not in ('cms_usage_detail_insert', 'cms_campaign_digital_detail_insert')
and 
(
   	(trx_date between date(datetime(dateadd('day', 1, last_day(date('${run-date=yyyy-MM-dd}') - interval '3' month)))) and date(date('${run-date=yyyy-MM-dd}') - interval '2' month))
	or (trx_date between date(datetime(dateadd('day', 1, last_day(date('${run-date=yyyy-MM-dd}') - interval '2' month)))) and date(date('${run-date=yyyy-MM-dd}') - interval '1' month))
	or (trx_date between date(datetime(dateadd('day', 1, last_day(date('${run-date=yyyy-MM-dd}') - interval '1' month)))) and date(date('${run-date=yyyy-MM-dd}')))
)
group by 1,6
) b on a.periode = b.periode 
and a.eom = b.eom  
and a.brand = b.brand 
and a.theme = b.theme
and a.category = b.category
and a.initiated_business = b.initiated_business;


insert into ${table-prefix}${cms-schema}.${table-prefix}cms_performance_region
select a.periode, a.eom, a.brand, a.theme, a.category, a.initiated_business, a.tot_campaign, a.target, a.eligible, a.taker, b.unique_target, b.unique_taker, 
a.revenue, a.recharge, a.control, a.taker_control, a.revenue_control, a.uplift, a.last_trx_date
from 
(
select periode, eom, 'Total' brand, 'Total' theme, category, initiated_business, 
count(distinct campaign_id) as tot_campaign,
sum(target) as target, 
sum(eligible) as eligible,
sum(taker) as taker,
sum(case when campaign_group_id in (1,4,6,7) then revenue_taker else 0 end) revenue,
sum(case when campaign_group_id = 2 then revenue_taker else 0 end) recharge,
sum(control) as control,
sum(taker_control) as taker_control,
sum(case when campaign_group_id in (1,4,6,7) then revenue_control else 0 end) revenue_control,
sum((((arpu_target_after - arpu_target_before) - (arpu_control_after - arpu_control_before))/nullif((arpu_target_before * 1.0),0)) * eligible)/nullif(sum(eligible),0) uplift, 
max(last_trx_date) as last_trx_date
from ${table-prefix}${cms-schema}.${table-prefix}cms_summary_bycampaignid
where eom = 0
group by 1,2,5,6 
) a left join 
(
select date(datetime(last_day(trx_date))) as periode, 0 eom, 'Total' brand, 'Total' theme, a.category, 
a.initiated_group as initiated_business,
count(distinct msisdn) unique_target,
count(distinct case when taker > 0 and iscontrolgroup = 'false' then msisdn end) unique_taker
from ${table-prefix}${cms-schema}.${table-prefix}cms_campaign_tracking_detail a 
left join 
(
	select distinct campaign_objective_id, campaign_objective_desc
	from ${table-prefix}${cms-schema}.${table-prefix}cms_campaign_objective
) c on a.campaign_objective_id = c.campaign_objective_id
where a.pipeline_step not in ('cms_usage_detail_insert', 'cms_campaign_digital_detail_insert')
and 
(
   	(trx_date between date(datetime(dateadd('day', 1, last_day(date('${run-date=yyyy-MM-dd}') - interval '3' month)))) and date(date('${run-date=yyyy-MM-dd}') - interval '2' month))
	or (trx_date between date(datetime(dateadd('day', 1, last_day(date('${run-date=yyyy-MM-dd}') - interval '2' month)))) and date(date('${run-date=yyyy-MM-dd}') - interval '1' month))
	or (trx_date between date(datetime(dateadd('day', 1, last_day(date('${run-date=yyyy-MM-dd}') - interval '1' month)))) and date(date('${run-date=yyyy-MM-dd}')))
)
group by 1,5,6
) b on a.periode = b.periode 
and a.eom = b.eom  
and a.brand = b.brand 
and a.theme = b.theme
and a.category = b.category
and a.initiated_business = b.initiated_business;


insert into ${table-prefix}${cms-schema}.${table-prefix}cms_performance_region
select a.periode, a.eom, a.brand, a.theme, a.category, a.initiated_business, a.tot_campaign, a.target, a.eligible, a.taker, b.unique_target, b.unique_taker, 
a.revenue, a.recharge, a.control, a.taker_control, a.revenue_control, a.uplift, a.last_trx_date
from 
(
select periode, eom, 'Total' brand, campaign_objective_desc as theme, category, initiated_business, 
count(distinct campaign_id) as tot_campaign,
sum(target) as target, 
sum(eligible) as eligible,
sum(taker) as taker,
sum(case when campaign_group_id in (1,4,6,7) then revenue_taker else 0 end) revenue,
sum(case when campaign_group_id = 2 then revenue_taker else 0 end) recharge,
sum(control) as control,
sum(taker_control) as taker_control,
sum(case when campaign_group_id in (1,4,6,7) then revenue_control else 0 end) revenue_control,
sum((((arpu_target_after - arpu_target_before) - (arpu_control_after - arpu_control_before))/nullif((arpu_target_before * 1.0),0)) * eligible)/nullif(sum(eligible),0) uplift, 
max(last_trx_date) as last_trx_date
from ${table-prefix}${cms-schema}.${table-prefix}cms_summary_bycampaignid
where eom = 0
group by 1,2,4,5,6 
) a left join 
(
select date(datetime(last_day(trx_date))) as periode, 0 eom, 'Total' brand, c.campaign_objective_desc theme, a.category, 
a.initiated_group as initiated_business,
count(distinct msisdn) unique_target,
count(distinct case when taker > 0 and iscontrolgroup = 'false' then msisdn end) unique_taker
from ${table-prefix}${cms-schema}.${table-prefix}cms_campaign_tracking_detail a 
left join 
(
	select distinct campaign_objective_id, campaign_objective_desc
	from ${table-prefix}${cms-schema}.${table-prefix}cms_campaign_objective
) c on a.campaign_objective_id = c.campaign_objective_id
where a.pipeline_step not in ('cms_usage_detail_insert', 'cms_campaign_digital_detail_insert')
and 
(
   	(trx_date between date(datetime(dateadd('day', 1, last_day(date('${run-date=yyyy-MM-dd}') - interval '3' month)))) and date(date('${run-date=yyyy-MM-dd}') - interval '2' month))
	or (trx_date between date(datetime(dateadd('day', 1, last_day(date('${run-date=yyyy-MM-dd}') - interval '2' month)))) and date(date('${run-date=yyyy-MM-dd}') - interval '1' month))
	or (trx_date between date(datetime(dateadd('day', 1, last_day(date('${run-date=yyyy-MM-dd}') - interval '1' month)))) and date(date('${run-date=yyyy-MM-dd}')))
)
group by 1,4,5,6
) b on a.periode = b.periode 
and a.eom = b.eom  
and a.brand = b.brand 
and a.theme = b.theme
and a.category = b.category
and a.initiated_business = b.initiated_business;



insert into ${table-prefix}${cms-schema}.${table-prefix}cms_performance_region
select a.periode, a.eom, a.brand, a.theme, a.category, a.initiated_business, a.tot_campaign, a.target, a.eligible, a.taker, b.unique_target, b.unique_taker, 
a.revenue, a.recharge, a.control, a.taker_control, a.revenue_control, a.uplift, a.last_trx_date
from 
(
select periode, eom, 'Total' brand, campaign_objective_desc as theme, 'Total' category, initiated_business, 
count(distinct campaign_id) as tot_campaign,
sum(target) as target, 
sum(eligible) as eligible,
sum(taker) as taker,
sum(case when campaign_group_id in (1,4,6,7) then revenue_taker else 0 end) revenue,
sum(case when campaign_group_id = 2 then revenue_taker else 0 end) recharge,
sum(control) as control,
sum(taker_control) as taker_control,
sum(case when campaign_group_id in (1,4,6,7) then revenue_control else 0 end) revenue_control,
sum((((arpu_target_after - arpu_target_before) - (arpu_control_after - arpu_control_before))/nullif((arpu_target_before * 1.0),0)) * eligible)/nullif(sum(eligible),0) uplift, 
max(last_trx_date) as last_trx_date
from ${table-prefix}${cms-schema}.${table-prefix}cms_summary_bycampaignid
where eom = 0
group by 1,2,4,6 
) a left join 
(
select date(datetime(last_day(trx_date))) as periode, 0 eom, 'Total' brand, c.campaign_objective_desc theme, 'Total' category, 
a.initiated_group as initiated_business,
count(distinct msisdn) unique_target,
count(distinct case when taker > 0 and iscontrolgroup = 'false' then msisdn end) unique_taker
from ${table-prefix}${cms-schema}.${table-prefix}cms_campaign_tracking_detail a 
left join 
(
	select distinct campaign_objective_id, campaign_objective_desc
	from ${table-prefix}${cms-schema}.${table-prefix}cms_campaign_objective
) c on a.campaign_objective_id = c.campaign_objective_id
where a.pipeline_step not in ('cms_usage_detail_insert', 'cms_campaign_digital_detail_insert')
and 
(
   	(trx_date between date(datetime(dateadd('day', 1, last_day(date('${run-date=yyyy-MM-dd}') - interval '3' month)))) and date(date('${run-date=yyyy-MM-dd}') - interval '2' month))
	or (trx_date between date(datetime(dateadd('day', 1, last_day(date('${run-date=yyyy-MM-dd}') - interval '2' month)))) and date(date('${run-date=yyyy-MM-dd}') - interval '1' month))
	or (trx_date between date(datetime(dateadd('day', 1, last_day(date('${run-date=yyyy-MM-dd}') - interval '1' month)))) and date(date('${run-date=yyyy-MM-dd}')))
)
group by 1,4,6
) b on a.periode = b.periode 
and a.eom = b.eom  
and a.brand = b.brand 
and a.theme = b.theme
and a.category = b.category
and a.initiated_business = b.initiated_business;



insert into ${table-prefix}${cms-schema}.${table-prefix}cms_performance_region
select a.periode, a.eom, a.brand, a.theme, a.category, a.initiated_business, a.tot_campaign, a.target, a.eligible, a.taker, b.unique_target, b.unique_taker, 
a.revenue, a.recharge, a.control, a.taker_control, a.revenue_control, a.uplift, a.last_trx_date
from 
(
select periode, eom, brand, campaign_objective_desc as theme, 'Total' category, initiated_business, 
count(distinct campaign_id) as tot_campaign,
sum(target) as target, 
sum(eligible) as eligible,
sum(taker) as taker,
sum(case when campaign_group_id in (1,4,6,7) then revenue_taker else 0 end) revenue,
sum(case when campaign_group_id = 2 then revenue_taker else 0 end) recharge,
sum(control) as control,
sum(taker_control) as taker_control,
sum(case when campaign_group_id in (1,4,6,7) then revenue_control else 0 end) revenue_control,
sum((((arpu_target_after - arpu_target_before) - (arpu_control_after - arpu_control_before))/nullif((arpu_target_before * 1.0),0)) * eligible)/nullif(sum(eligible),0) uplift, 
max(last_trx_date) as last_trx_date
from ${table-prefix}${cms-schema}.${table-prefix}cms_summary_bycampaignid
where eom = 0
group by 1,2,3,4,6 
) a left join 
(
select date(datetime(last_day(trx_date))) as periode, 0 eom,
case
	when upper(substring(a.campaign_id, 11, 1)) = 'A' then 'Simpati'
	when upper(substring(a.campaign_id, 11, 1)) = 'B' then 'KartuAs'
	when upper(substring(a.campaign_id, 11, 1)) = 'C' then 'AllPrepaid'
	when upper(substring(a.campaign_id, 11, 1)) = 'D' then 'KartuHALO'
	when upper(substring(a.campaign_id, 11, 1)) = 'E' then 'AllBrands'
	when upper(substring(a.campaign_id, 11, 1)) = 'F' then 'Loop'
end as brand,
c.campaign_objective_desc theme, 'Total' category, 
a.initiated_group as initiated_business,
count(distinct msisdn) unique_target,
count(distinct case when taker > 0 and iscontrolgroup = 'false' then msisdn end) unique_taker
from ${table-prefix}${cms-schema}.${table-prefix}cms_campaign_tracking_detail a 
left join 
(
	select distinct campaign_objective_id, campaign_objective_desc
	from ${table-prefix}${cms-schema}.${table-prefix}cms_campaign_objective
) c on a.campaign_objective_id = c.campaign_objective_id
where a.pipeline_step not in ('cms_usage_detail_insert', 'cms_campaign_digital_detail_insert')
and 
(
   	(trx_date between date(datetime(dateadd('day', 1, last_day(date('${run-date=yyyy-MM-dd}') - interval '3' month)))) and date(date('${run-date=yyyy-MM-dd}') - interval '2' month))
	or (trx_date between date(datetime(dateadd('day', 1, last_day(date('${run-date=yyyy-MM-dd}') - interval '2' month)))) and date(date('${run-date=yyyy-MM-dd}') - interval '1' month))
	or (trx_date between date(datetime(dateadd('day', 1, last_day(date('${run-date=yyyy-MM-dd}') - interval '1' month)))) and date(date('${run-date=yyyy-MM-dd}')))
)
group by 1,3,4,6
) b on a.periode = b.periode 
and a.eom = b.eom  
and a.brand = b.brand 
and a.theme = b.theme
and a.category = b.category
and a.initiated_business = b.initiated_business;


insert into ${table-prefix}${cms-schema}.${table-prefix}cms_performance_region
select a.periode, a.eom, a.brand, a.theme, a.category, a.initiated_business, a.tot_campaign, a.target, a.eligible, a.taker, b.unique_target, b.unique_taker, 
a.revenue, a.recharge, a.control, a.taker_control, a.revenue_control, a.uplift, a.last_trx_date
from 
(
select periode, eom, brand, 'Total' theme, 'Total' category, initiated_business, 
count(distinct campaign_id) as tot_campaign,
sum(target) as target, 
sum(eligible) as eligible,
sum(taker) as taker,
sum(case when campaign_group_id in (1,4,6,7) then revenue_taker else 0 end) revenue,
sum(case when campaign_group_id = 2 then revenue_taker else 0 end) recharge,
sum(control) as control,
sum(taker_control) as taker_control,
sum(case when campaign_group_id in (1,4,6,7) then revenue_control else 0 end) revenue_control,
sum((((arpu_target_after - arpu_target_before) - (arpu_control_after - arpu_control_before))/nullif((arpu_target_before * 1.0),0)) * eligible)/nullif(sum(eligible),0) uplift, 
max(last_trx_date) as last_trx_date
from ${table-prefix}${cms-schema}.${table-prefix}cms_summary_bycampaignid
where eom = 0
group by 1,2,3,6 
) a left join 
(
select date(datetime(last_day(trx_date))) as periode, 0 eom, 
case
	when upper(substring(a.campaign_id, 11, 1)) = 'A' then 'Simpati'
	when upper(substring(a.campaign_id, 11, 1)) = 'B' then 'KartuAs'
	when upper(substring(a.campaign_id, 11, 1)) = 'C' then 'AllPrepaid'
	when upper(substring(a.campaign_id, 11, 1)) = 'D' then 'KartuHALO'
	when upper(substring(a.campaign_id, 11, 1)) = 'E' then 'AllBrands'
	when upper(substring(a.campaign_id, 11, 1)) = 'F' then 'Loop'
end as brand,
'Total' theme, 'Total' category, 
a.initiated_group as initiated_business,
count(distinct msisdn) unique_target,
count(distinct case when taker > 0 and iscontrolgroup = 'false' then msisdn end) unique_taker
from ${table-prefix}${cms-schema}.${table-prefix}cms_campaign_tracking_detail a 
left join 
(
	select distinct campaign_objective_id, campaign_objective_desc
	from ${table-prefix}${cms-schema}.${table-prefix}cms_campaign_objective
) c on a.campaign_objective_id = c.campaign_objective_id
where a.pipeline_step not in ('cms_usage_detail_insert', 'cms_campaign_digital_detail_insert')
and 
(
   	(trx_date between date(datetime(dateadd('day', 1, last_day(date('${run-date=yyyy-MM-dd}') - interval '3' month)))) and date(date('${run-date=yyyy-MM-dd}') - interval '2' month))
	or (trx_date between date(datetime(dateadd('day', 1, last_day(date('${run-date=yyyy-MM-dd}') - interval '2' month)))) and date(date('${run-date=yyyy-MM-dd}') - interval '1' month))
	or (trx_date between date(datetime(dateadd('day', 1, last_day(date('${run-date=yyyy-MM-dd}') - interval '1' month)))) and date(date('${run-date=yyyy-MM-dd}')))
)
group by 1,3,6
) b on a.periode = b.periode 
and a.eom = b.eom  
and a.brand = b.brand 
and a.theme = b.theme
and a.category = b.category
and a.initiated_business = b.initiated_business;


insert into ${table-prefix}${cms-schema}.${table-prefix}cms_performance_region
select a.periode, a.eom, a.brand, a.theme, a.category, a.initiated_business, a.tot_campaign, a.target, a.eligible, a.taker, b.unique_target, b.unique_taker, 
a.revenue, a.recharge, a.control, a.taker_control, a.revenue_control, a.uplift, a.last_trx_date
from 
(
select periode, eom, brand, 'Total' theme, category, initiated_business, 
count(distinct campaign_id) as tot_campaign,
sum(target) as target, 
sum(eligible) as eligible,
sum(taker) as taker,
sum(case when campaign_group_id in (1,4,6,7) then revenue_taker else 0 end) revenue,
sum(case when campaign_group_id = 2 then revenue_taker else 0 end) recharge,
sum(control) as control,
sum(taker_control) as taker_control,
sum(case when campaign_group_id in (1,4,6,7) then revenue_control else 0 end) revenue_control,
sum((((arpu_target_after - arpu_target_before) - (arpu_control_after - arpu_control_before))/nullif((arpu_target_before * 1.0),0)) * eligible)/nullif(sum(eligible),0) uplift, 
max(last_trx_date) as last_trx_date
from ${table-prefix}${cms-schema}.${table-prefix}cms_summary_bycampaignid
where eom = 0
group by 1,2,3,5,6 
) a left join 
(
select date(datetime(last_day(trx_date))) as periode, 0 eom, 
case
	when upper(substring(a.campaign_id, 11, 1)) = 'A' then 'Simpati'
	when upper(substring(a.campaign_id, 11, 1)) = 'B' then 'KartuAs'
	when upper(substring(a.campaign_id, 11, 1)) = 'C' then 'AllPrepaid'
	when upper(substring(a.campaign_id, 11, 1)) = 'D' then 'KartuHALO'
	when upper(substring(a.campaign_id, 11, 1)) = 'E' then 'AllBrands'
	when upper(substring(a.campaign_id, 11, 1)) = 'F' then 'Loop'
end as brand,
'Total' theme, a.category, 
a.initiated_group as initiated_business,
count(distinct msisdn) unique_target,
count(distinct case when taker > 0 and iscontrolgroup = 'false' then msisdn end) unique_taker
from ${table-prefix}${cms-schema}.${table-prefix}cms_campaign_tracking_detail a 
left join 
(
	select distinct campaign_objective_id, campaign_objective_desc
	from ${table-prefix}${cms-schema}.${table-prefix}cms_campaign_objective
) c on a.campaign_objective_id = c.campaign_objective_id
where a.pipeline_step not in ('cms_usage_detail_insert', 'cms_campaign_digital_detail_insert')
and 
(
   	(trx_date between date(datetime(dateadd('day', 1, last_day(date('${run-date=yyyy-MM-dd}') - interval '3' month)))) and date(date('${run-date=yyyy-MM-dd}') - interval '2' month))
	or (trx_date between date(datetime(dateadd('day', 1, last_day(date('${run-date=yyyy-MM-dd}') - interval '2' month)))) and date(date('${run-date=yyyy-MM-dd}') - interval '1' month))
	or (trx_date between date(datetime(dateadd('day', 1, last_day(date('${run-date=yyyy-MM-dd}') - interval '1' month)))) and date(date('${run-date=yyyy-MM-dd}')))
)
group by 1,3,5,6
) b on a.periode = b.periode 
and a.eom = b.eom  
and a.brand = b.brand 
and a.theme = b.theme
and a.category = b.category
and a.initiated_business = b.initiated_business;




