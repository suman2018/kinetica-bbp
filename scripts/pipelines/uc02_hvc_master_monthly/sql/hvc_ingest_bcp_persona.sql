
delete from ${table-prefix}${hvc-schema}.${table-prefix}hvc_bcp_persona where cut_off_dt = '${run-date=yyyy-MM-dd}';

insert into ${table-prefix}${hvc-schema}.${table-prefix}hvc_bcp_persona 
select
    msisdn,
    regional_channel,
    city,
    vol_games_byte,
    vol_socialnet_byte,
    vol_video_byte,
    vol_music_byte,
    vol_communications_byte,
    vol_sports_byte,
    vol_ecommerce_byte,
    vol_travel_food_byte,
    vol_news_lifestyle_byte,
    first_rank_category,
    second_rank_category,
    third_rank_category,
    fourth_rank_category,
    fifth_rank_category,
    first_rank_apps,
    second_rank_apps,
    third_rank_apps,
    fourth_rank_apps,
    fifth_rank_apps,
    '${run-date=yyyy-MM-dd}' as cut_off_date
from ${table-prefix}${hvc-schema}.${table-prefix}hvc_bcp_persona_stg;
