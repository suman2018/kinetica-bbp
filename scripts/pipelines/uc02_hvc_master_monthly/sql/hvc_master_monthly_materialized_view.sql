CREATE OR REPLACE MATERIALIZED VIEW ${table-prefix}${hvc-schema}_out.${table-prefix}hvc_summary_baseline_v
REFRESH EVERY 1 DAY AS (
select *
from ${table-prefix}${hvc-schema}.${table-prefix}hvc_summary_baseline
);


CREATE OR REPLACE MATERIALIZED VIEW ${table-prefix}${hvc-schema}_out.${table-prefix}hvc_itdaml_demography_v
REFRESH EVERY 1 DAY AS (
select *
from ${table-prefix}${hvc-schema}.${table-prefix}hvc_itdaml_demography
);


CREATE OR REPLACE MATERIALIZED VIEW ${table-prefix}${hvc-schema}_out.${table-prefix}hvc_home_staypoint_v
REFRESH EVERY 1 DAY AS (
select imsi, 
home1_lat, 
home1_lon, 
home1_days, 
home1_duration, 
home1_kelurahan_name, 
home1_kecamatan_name, 
home1_kabupaten_name, 
home1_province_name, 
home2_lat, 
home2_lon, 
home2_days, 
home2_duration, 
home2_kelurahan_name, 
home2_kecamatan_name, 
home2_kabupaten_name, 
home2_province_name, 
home3_lat, 
home3_lon, 
home3_days, 
home3_duration, 
home3_kelurahan_name, 
home3_kecamatan_name, 
home3_kabupaten_name, 
home3_province_name, 
home4_lat, 
home4_lon, 
home4_days, 
home4_duration, 
home4_kelurahan_name, 
home4_kecamatan_name, 
home4_kabupaten_name, 
home4_province_name, 
mo_id
from ${table-prefix}${hvc-schema}.${table-prefix}hvc_home_staypoint_vw
);


CREATE OR REPLACE MATERIALIZED VIEW ${table-prefix}${hvc-schema}_out.${table-prefix}hvc_work_staypoint_v
REFRESH EVERY 1 DAY AS (
select imsi, 
work1_lat, 
work1_lon, 
work1_days, 
work1_duration, 
work1_kelurahan_name, 
work1_kecamatan_name, 
work1_kabupaten_name, 
work1_province_name, 
work2_lat, 
work2_lon, 
work2_days, 
work2_duration, 
work2_kelurahan_name, 
work2_kecamatan_name, 
work2_kabupaten_name, 
work2_province_name, 
work3_lat, 
work3_lon, 
work3_days, 
work3_duration, 
work3_kelurahan_name, 
work3_kecamatan_name, 
work3_kabupaten_name, 
work3_province_name, 
work4_lat, 
work4_lon, 
work4_days, 
work4_duration, 
work4_kelurahan_name, 
work4_kecamatan_name, 
work4_kabupaten_name, 
work4_province_name, 
mo_id
from ${table-prefix}${hvc-schema}.${table-prefix}hvc_work_staypoint_vw);


CREATE OR REPLACE MATERIALIZED VIEW ${table-prefix}${hvc-schema}_out.${table-prefix}hvc_bcp_persona_v
REFRESH EVERY 1 DAY AS (
select *
from ${table-prefix}${hvc-schema}.${table-prefix}hvc_bcp_persona);


CREATE OR REPLACE MATERIALIZED VIEW ${table-prefix}${hvc-schema}_out.${table-prefix}hvc_community_cb_dou_v
REFRESH EVERY 1 DAY AS (
select *
from ${table-prefix}${hvc-schema}.${table-prefix}hvc_community_cb_dou);


CREATE OR REPLACE MATERIALIZED VIEW ${table-prefix}${hvc-schema}_out.${table-prefix}hvc_postpaid_cb_rev_v
REFRESH EVERY 1 DAY AS (
select *
from ${table-prefix}${hvc-schema}.${table-prefix}hvc_postpaid_cb_rev
);


CREATE OR REPLACE MATERIALIZED VIEW ${table-prefix}${hvc-schema}_out.${table-prefix}hvc_summary_postpaid_cb_rev_v
REFRESH EVERY 1 DAY AS (
select *
from ${table-prefix}${hvc-schema}.${table-prefix}hvc_summary_postpaid_cb_rev);


CREATE OR REPLACE MATERIALIZED VIEW ${table-prefix}${hvc-schema}_out.${table-prefix}hvc_summary_postpaid_mainpage_v
REFRESH EVERY 1 DAY AS (
select *
from ${table-prefix}${hvc-schema}.${table-prefix}hvc_summary_postpaid_mainpage);


CREATE OR REPLACE MATERIALIZED VIEW ${table-prefix}${hvc-schema}_out.${table-prefix}hvc_segment_postpaid_movement_summ_v
REFRESH EVERY 1 DAY AS (
select *
from ${table-prefix}${hvc-schema}.${table-prefix}hvc_segment_postpaid_movement_summ);


CREATE OR REPLACE MATERIALIZED VIEW ${table-prefix}${hvc-schema}_out.${table-prefix}hvc_segment_postpaid_movement_summ_bl_v
REFRESH EVERY 1 DAY AS (
select *
from ${table-prefix}${hvc-schema}.${table-prefix}hvc_segment_postpaid_movement_summ_bl
);

alter schema ${table-prefix}${hvc-schema}_out set protected true;
