-- this implements hvc_ingest_postpaid_cb_rev.sh
-- Agy from TK said this logic is sound, an append.

-- insert overwrite table it.hvc_ingest_postpaid_cb_rev partition (cut_off_dt='${period_dash}')

delete from ${table-prefix}${hvc-schema}.${table-prefix}hvc_community_cb_dou
where month_id = '${run-date=yyyyMM}';

insert into ${table-prefix}${hvc-schema}.${table-prefix}hvc_community_cb_dou 
SELECT '${run-date=yyyyMM}', MSISDN 
  FROM ${table-prefix}${hvc-schema}.${table-prefix}hvc_community_cb_dou_stg 
;
