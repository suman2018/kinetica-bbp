-- implements hvc_ingest_baseline.sh
-- partition (cut_off_dt='${period_baseline}') do not drop recreate. Just add.
-- this is run 1x/month?

delete from ${table-prefix}${hvc-schema}.${table-prefix}hvc_summary_baseline where cut_off_report = '${run-date=yyyyMM}';
---- cut_off_date = '${run-date=yyyy-MM-dd}';

insert into ${table-prefix}${hvc-schema}.${table-prefix}hvc_summary_baseline
---create or replace table ${hvc-schema}.${table-prefix}hvc_summary_baseline_${run-date=yyyyMM} as 
select
    '${run-date=yyyyMM}' as cut_off_report,         -- ${period_baseline}' as cut_off_report,
    coalesce(brand, 'UNKNOWN') as brand,
    coalesce(cluster_name, 'UNKNOWN') as cluster_name,
    coalesce(region_name, 'UNKNOWN') as region,
    coalesce(area_name, 'UNKNOWN') as area,
    los_segment, arpu_segment, hvc_segment,

    count(distinct case when rev_total > 0 then msisdn end) as subs,
    count(distinct case when rev_voice > 0 then msisdn end) as subs_voice,
    count(distinct case when rev_sms > 0 then msisdn end) as subs_sms,
    count(distinct case when rev_broad > 0 then msisdn end) as subs_broad,
    count(distinct case when rev_digital > 0 then msisdn end) as subs_digital,
    count(distinct case when rev_roaming > 0 then msisdn end) as subs_roaming,
    count(distinct case when rev_other > 0 then msisdn end) as subs_other,

    count(distinct case when rev_total = 0 then msisdn end) as non_rgb,
    count(distinct case when rev_voice = 0 then msisdn end) as non_rgb_voice,
    count(distinct case when rev_sms = 0 then msisdn end) as non_rgb_sms,
    count(distinct case when rev_broad = 0 then msisdn end) as non_rgb_broad,
    count(distinct case when rev_digital = 0 then msisdn end) as non_rgb_digital,
    count(distinct case when rev_roaming = 0 then msisdn end) as non_rgb_roaming,
    count(distinct case when rev_other = 0 then msisdn end) as non_rgb_other,

    sum(rev_total) as rev_total,
    sum(rev_voice) as rev_voice,
    sum(rev_sms) as rev_sms,
    sum(rev_broad) as rev_broad,
    sum(rev_digital) as rev_digital,
    sum(rev_roaming) as rev_roaming,
    sum(rev_other) as rev_other,
    '${run-date=yyyy-MM-dd}' as cut_off_date
from
(
    select
          msisdn,
          brand,
          los,
          rev_total,
          rev_voice,
          rev_sms,
          rev_broad,
          rev_digital,
          rev_roaming,
          rev_other,
          CASE 
          WHEN TRIM(AREA_NAME)='' THEN 'UNKNOWN' 
          WHEN TRIM(AREA_NAME)='JABOTABEK & JABAR' THEN 'AREA 2'
          WHEN TRIM(AREA_NAME)='JAWA - BALI - NUSRA' THEN  'AREA 3'
          WHEN TRIM(AREA_NAME)='PAMASUKA' THEN  'AREA 4'
          WHEN TRIM(AREA_NAME)='SUMATERA' THEN 'AREA 1'
          ELSE TRIM(AREA_NAME) END AS area_name, 
          CASE 
          WHEN TRIM(REGION_NAME)='' THEN 'UNKNOWN' 
          WHEN TRIM(REGION_NAME)='BALI NUSRA' THEN 'BALINUSRA'
          WHEN TRIM(REGION_NAME)='JATENG-DIY' THEN 'JATENG'
          WHEN TRIM(REGION_NAME)='MALUKU DAN PAPUA' OR TRIM(REGION_NAME)='PAPUA' THEN 'PUMA'
          ELSE TRIM(REGION_NAME) END AS region_name, 
          CASE WHEN TRIM(CLUSTER_NAME)='' THEN 'UNKNOWN' ELSE TRIM(CLUSTER_NAME) END AS cluster_name,     
          los_segment,
          arpu_segment,
          hvc_segment
    from ${table-prefix}${hvc-schema}.${table-prefix}hvc_baseline_stg 
    where brand in ('LOOP','KartuAS','simPATI')
) a
group by brand, cluster_name, region_name, area_name, los_segment, arpu_segment, hvc_segment;
