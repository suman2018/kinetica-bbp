DROP VIEW IF EXISTS ${table-prefix}${hvc-schema}.${table-prefix}hvc_work_staypoint_vw;

DROP TABLE IF EXISTS ${table-prefix}${hvc-schema}.${table-prefix}hvc_work_staypoint_${run-date=yyyyMM};     

alter table ${table-prefix}${hvc-schema}.${table-prefix}work_staypoint_mm_stg RENAME TO 
	${table-prefix}hvc_work_staypoint_${run-date=yyyyMM};

create or replace view ${table-prefix}${hvc-schema}.${table-prefix}hvc_work_staypoint_vw as 
	select * from ${table-prefix}${hvc-schema}.${table-prefix}hvc_work_staypoint_${run-date=yyyyMM};

