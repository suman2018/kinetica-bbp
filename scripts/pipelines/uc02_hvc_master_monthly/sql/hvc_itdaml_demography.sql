delete from ${table-prefix}${hvc-schema}.${table-prefix}hvc_itdaml_demography
where cut_off_report = '${run-date=yyyyMM}';


insert into ${table-prefix}${hvc-schema}.${table-prefix}hvc_itdaml_demography
select '${run-date=yyyyMM}' as cut_off_report,
    a.*
from ${table-prefix}${hvc-schema}.${table-prefix}itdaml_new_demography_stg a;
