-- this implements hvc_ingest_postpaid_cb_rev.sh
-- Agy from TK said this logic is sound, an append.

-- insert overwrite table it.hvc_ingest_postpaid_cb_rev partition (cut_off_dt='${period_dash}')

delete from ${table-prefix}${hvc-schema}.${table-prefix}hvc_summary_postpaid_cb_rev
where cut_off_report = '${run-date=yyyyMM}';

insert into ${table-prefix}${hvc-schema}.${table-prefix}hvc_summary_postpaid_cb_rev 
select 
  cut_off_report, a.msisdn, tcash_flg, bill_cycle, inv_month, tot_bill_amount, coalesce(los,0) as los, coalesce(los_segment, '<= 3 MONTHS') as los_segment, 
  area_hlr, region_hlr, city, coalesce(flg_youth, 'N') as flg_youth, 
  case
 when los <= 90 and coalesce(tot_bill_amount, 0) <= 50000 then 'LAC'
 when los <= 90 and coalesce(tot_bill_amount, 0) < 70000 then 'MAC'
 when los <= 90 and coalesce(tot_bill_amount, 0) >= 70000 then 'HAC'
 when (los >90 and los < 360) and coalesce(tot_bill_amount, 0) <= 50000 then 'LPC'
 when (los >90 and los < 360) and coalesce(tot_bill_amount, 0) < 70000 then 'MPC'
 when (los >90 and los < 360) and coalesce(tot_bill_amount, 0) >= 70000 then 'HPC'
 when los >= 360 and coalesce(tot_bill_amount, 0) <= 50000 then 'LVC'
 when los >= 360 and coalesce(tot_bill_amount, 0) < 70000 then 'MVC'
 when los >= 360 and coalesce(tot_bill_amount, 0) >= 70000 then 'HVC'
 end as hvc_segment, 
 /*'UNKNOWN' as hvc_segment,*/
 case
when coalesce(tot_bill_amount,0) <= 50000 then '0-50K'
when coalesce(tot_bill_amount,0) < 65000 then '50-65K'
when coalesce(tot_bill_amount,0) < 70000 then '65-70K'
when coalesce(tot_bill_amount,0) < 120000 then '70-120K'
when coalesce(tot_bill_amount,0) < 150000 then '120-150K'
when coalesce(tot_bill_amount,0) < 180000 then '150-180K'
when coalesce(tot_bill_amount,0) < 210000 then '180-210K'
when coalesce(tot_bill_amount,0) < 240000 then '210-240K'
when coalesce(tot_bill_amount,0) >= 240000 then '>240K'
end as arpu_segment,
  DATE '${run-date=yyyy-MM-dd}'
  from ${table-prefix}${hvc-schema}.${table-prefix}hvc_postpaid_cb_rev a
  where a.cut_off_report = '${run-date=yyyyMM}'
;

