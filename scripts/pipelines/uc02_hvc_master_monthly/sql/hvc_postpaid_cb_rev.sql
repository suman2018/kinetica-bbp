-- this implements hvc_ingest_postpaid_cb_rev.sh
-- Agy from TK said this logic is sound, an append.

-- insert overwrite table it.hvc_ingest_postpaid_cb_rev partition (cut_off_dt='${period_dash}')

delete from ${table-prefix}${hvc-schema}.${table-prefix}hvc_postpaid_cb_rev
where cut_off_report = '${run-date=yyyyMM}';

insert into ${table-prefix}${hvc-schema}.${table-prefix}hvc_postpaid_cb_rev 
SELECT '${run-date=yyyyMM}', A.MSISDN, TCASH_FLG, BILL_CYCLE, INV_MONTH, TOT_BILL_AMOUNT, cast (LOS as integer), LOS_SEGMENT, B.AREA_HLR , B.REGION_HLR, 
B.CITY, FLG_YOUTH, DATE '${run-date=yyyy-MM-dd}'  FROM (
 SELECT msisdn, LOS, LOS_SEGMENT, hvc_segment, arpu_segment, flg_youth FROM ${table-prefix}${hvc-schema}.${table-prefix}HVC_SUMMARY_CB WHERE cut_off_dt='${run-date=yyyy-MM-dd}' and brnd_nme = 'kartuHALO') A
 LEFT JOIN 
 (SELECT msisdn, tcash_flg, max(bill_cycle) as bill_cycle, inv_month, sum(cast(tot_bill_amount as bigint)) as tot_bill_amount, area_hlr, region_hlr, city 
  FROM ${table-prefix}${hvc-schema}.${table-prefix}HVC_POSTPAID_CB_REV_STG
  group by msisdn, tcash_flg, inv_month, area_hlr, region_hlr, city
 ) B 
 ON A.msisdn=B.msisdn;
