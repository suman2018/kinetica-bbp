-- this implements hvc_ingest_postpaid_cb_rev.sh
-- Agy from TK said this logic is sound, an append.

-- insert overwrite table it.hvc_ingest_postpaid_cb_rev partition (cut_off_dt='${period_dash}')

delete from ${table-prefix}${hvc-schema}.${table-prefix}hvc_summary_postpaid_mainpage
where cut_off_report = '${run-date=yyyyMM}';

insert into ${table-prefix}${hvc-schema}.${table-prefix}hvc_summary_postpaid_mainpage 
select cut_off_report, tcash_flg, bill_cycle, inv_month, los_segment, area_hlr, region_hlr, city, 
flg_youth, hvc_segment, arpu_segment, sum(tot_bill_amount) as total_bill_amount, 
count(msisdn) as subs,
DATE '${run-date=yyyy-MM-dd}' 
FROM ${table-prefix}${hvc-schema}.${table-prefix}hvc_summary_postpaid_cb_rev 
where cut_off_report = '${run-date=yyyyMM}'
GROUP BY cut_off_report, tcash_flg, bill_cycle, inv_month, los_segment, area_hlr, region_hlr, city, flg_youth, 
hvc_segment, arpu_segment;
