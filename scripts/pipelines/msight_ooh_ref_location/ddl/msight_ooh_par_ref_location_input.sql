create or replace table ${table-prefix}${msight_ooh-schema}.${table-prefix}par_ref_ooh_location_input
(
"area" varchar(32, dict)
,"location_code" varchar(16, dict)
,"location_name" varchar(16, dict)
,"type" varchar(2, dict)
,"lat" varchar(16, dict)
,"lon" varchar(16, dict)
,"propinsi" varchar(16, dict)
,"min_bearing" varchar(4)
,"max_bearing" varchar(4)
,"dtl" varchar(4)
,"distance" varchar(4)
)

