delete from ${table-prefix}${msight_ooh-schema}.${table-prefix}par_ref_ooh_location;
-- where months = '${run-date=yyyyMM}';

insert into ${table-prefix}${msight_ooh-schema}.${table-prefix}par_ref_ooh_location
select * from ${table-prefix}${msight_ooh-schema}.${table-prefix}par_ref_ooh_location_input;

DROP TABLE IF EXISTS ${table-prefix}${msight_ooh-schema}.${table-prefix}par_ref_ooh_location_input;

---ALTER TABLE ${table-prefix}${msight_ooh-schema}.${table-prefix}msight_ooh_par_ref_location_input RENAME TO ${table-prefix}msight_ooh_par_ref_location;

