/* KI_HINT_DICT_PROJECTION */
CREATE OR REPLACE TABLE ${table-prefix}${msight_sky-schema}.${table-prefix}sky_summ_hourly_profile_stg AS 
SELECT
trx_date,
calling_hour,
building_name,
cast(longitude as decimal(9,6)) as longitude,
cast(latitude as decimal(9,6)) as latitude,
CAST (unique_msisdn as integer) as unique_msisdn,
CAST (male as integer) as male,
CAST (female as integer) as female,
CAST (age_15 as integer) as age_15,
CAST (age16_20 as integer) as age16_20,
CAST (age21_25 as integer) as age21_25,
CAST (age26_30 as integer) as age26_30,
CAST (age31_35 as integer) as age31_35,
CAST (age36_40 as integer) as age36_40,
CAST (age41_45 as integer) as age41_45,
CAST (age46_50 as integer) as age46_50,
CAST (age51_55 as integer) as age51_55,
CAST (age_56 as integer) as age_56,
CAST (married as integer) as married,
CAST (single as integer) as single,
CAST (new_entertainment as integer) as new_entertainment,
CAST (new_fashion as integer) as new_fashion,
CAST (new_football as integer) as new_football,
CAST (new_other_sports as integer) as new_other_sports,
CAST (new_outdoor_activity as integer) as new_outdoor_activity,
CAST (new_health as integer) as new_health,
CAST (new_lifestyle as integer) as new_lifestyle,
CAST (new_shopping as integer) as new_shopping,
CAST (application_store as integer) as application_store,
CAST (automotive as integer) as automotive,
CAST (baby_care as integer) as baby_care,
CAST (banking as integer) as banking,
CAST (beauty as integer) as beauty,
CAST (chatting as integer) as chatting,
CAST (ecommerce as integer) as ecommerce,
CAST (education as integer) as education,
CAST (payment_and_finance as integer) as payment_and_finance,
CAST (culinary as integer) as culinary,
CAST (games as integer) as games,
CAST (government as integer) as government,
CAST (property as integer) as property,
CAST (photography as integer) as photography,
CAST (job_search as integer) as job_search,
CAST (music as integer) as music,
CAST (news as integer) as news,
CAST (productivity as integer) as productivity,
CAST (social_media as integer) as social_media,
CAST (sports as integer) as sports,
CAST (technology as integer) as technology,
CAST (transportation as integer) as transportation,
CAST (travel as integer) as travel,
CAST (video as integer) as video,
CAST (arpu_very_low as integer) as arpu_very_low,
CAST (arpu_low as integer) as arpu_low,
CAST (arpu_medium as integer) as arpu_medium,
CAST (arpu_high as integer) as arpu_high,
CAST (arpu_very_high as integer) as arpu_very_high,
CAST (arpu_top_usage as integer) as arpu_top_usage ,
ki_shard_key (building_name) 
FROM ${table-prefix}${msight_sky-schema}.${table-prefix}sky_summ_hourly_profile_input;


alter table ${table-prefix}${msight_sky-schema}.${table-prefix}sky_summ_hourly_profile_stg modify column trx_date varchar(8, dict) ;
alter table ${table-prefix}${msight_sky-schema}.${table-prefix}sky_summ_hourly_profile_stg modify column calling_hour varchar(2, dict) ;

alter table ${table-prefix}${msight_sky-schema}.${table-prefix}sky_summ_hourly_profile_stg modify column unique_msisdn integer (dict);
alter table ${table-prefix}${msight_sky-schema}.${table-prefix}sky_summ_hourly_profile_stg modify column male integer (dict);
alter table ${table-prefix}${msight_sky-schema}.${table-prefix}sky_summ_hourly_profile_stg modify column female integer (dict);
alter table ${table-prefix}${msight_sky-schema}.${table-prefix}sky_summ_hourly_profile_stg modify column age_15 integer (dict);
alter table ${table-prefix}${msight_sky-schema}.${table-prefix}sky_summ_hourly_profile_stg modify column age16_20 integer (dict);
alter table ${table-prefix}${msight_sky-schema}.${table-prefix}sky_summ_hourly_profile_stg modify column age21_25 integer (dict);
alter table ${table-prefix}${msight_sky-schema}.${table-prefix}sky_summ_hourly_profile_stg modify column age26_30 integer (dict);
alter table ${table-prefix}${msight_sky-schema}.${table-prefix}sky_summ_hourly_profile_stg modify column age31_35 integer (dict);
alter table ${table-prefix}${msight_sky-schema}.${table-prefix}sky_summ_hourly_profile_stg modify column age36_40 integer (dict);
alter table ${table-prefix}${msight_sky-schema}.${table-prefix}sky_summ_hourly_profile_stg modify column age41_45 integer (dict);
alter table ${table-prefix}${msight_sky-schema}.${table-prefix}sky_summ_hourly_profile_stg modify column age46_50 integer (dict);
alter table ${table-prefix}${msight_sky-schema}.${table-prefix}sky_summ_hourly_profile_stg modify column age51_55 integer (dict);
alter table ${table-prefix}${msight_sky-schema}.${table-prefix}sky_summ_hourly_profile_stg modify column age_56 integer (dict);
alter table ${table-prefix}${msight_sky-schema}.${table-prefix}sky_summ_hourly_profile_stg modify column married integer (dict);
alter table ${table-prefix}${msight_sky-schema}.${table-prefix}sky_summ_hourly_profile_stg modify column single integer (dict);
alter table ${table-prefix}${msight_sky-schema}.${table-prefix}sky_summ_hourly_profile_stg modify column new_entertainment integer (dict);
alter table ${table-prefix}${msight_sky-schema}.${table-prefix}sky_summ_hourly_profile_stg modify column new_fashion integer (dict);
alter table ${table-prefix}${msight_sky-schema}.${table-prefix}sky_summ_hourly_profile_stg modify column new_football integer (dict);
alter table ${table-prefix}${msight_sky-schema}.${table-prefix}sky_summ_hourly_profile_stg modify column new_other_sports integer (dict);
alter table ${table-prefix}${msight_sky-schema}.${table-prefix}sky_summ_hourly_profile_stg modify column new_outdoor_activity integer (dict);
alter table ${table-prefix}${msight_sky-schema}.${table-prefix}sky_summ_hourly_profile_stg modify column new_health integer (dict);
alter table ${table-prefix}${msight_sky-schema}.${table-prefix}sky_summ_hourly_profile_stg modify column new_lifestyle integer (dict);
alter table ${table-prefix}${msight_sky-schema}.${table-prefix}sky_summ_hourly_profile_stg modify column new_shopping integer (dict);
alter table ${table-prefix}${msight_sky-schema}.${table-prefix}sky_summ_hourly_profile_stg modify column application_store integer (dict);
alter table ${table-prefix}${msight_sky-schema}.${table-prefix}sky_summ_hourly_profile_stg modify column automotive integer (dict);
alter table ${table-prefix}${msight_sky-schema}.${table-prefix}sky_summ_hourly_profile_stg modify column baby_care integer (dict);
alter table ${table-prefix}${msight_sky-schema}.${table-prefix}sky_summ_hourly_profile_stg modify column banking integer (dict);
alter table ${table-prefix}${msight_sky-schema}.${table-prefix}sky_summ_hourly_profile_stg modify column beauty integer (dict);
alter table ${table-prefix}${msight_sky-schema}.${table-prefix}sky_summ_hourly_profile_stg modify column chatting integer (dict);
alter table ${table-prefix}${msight_sky-schema}.${table-prefix}sky_summ_hourly_profile_stg modify column ecommerce integer (dict);
alter table ${table-prefix}${msight_sky-schema}.${table-prefix}sky_summ_hourly_profile_stg modify column education integer (dict);
alter table ${table-prefix}${msight_sky-schema}.${table-prefix}sky_summ_hourly_profile_stg modify column payment_and_finance integer (dict);
alter table ${table-prefix}${msight_sky-schema}.${table-prefix}sky_summ_hourly_profile_stg modify column culinary integer (dict);
alter table ${table-prefix}${msight_sky-schema}.${table-prefix}sky_summ_hourly_profile_stg modify column games integer (dict);
alter table ${table-prefix}${msight_sky-schema}.${table-prefix}sky_summ_hourly_profile_stg modify column government integer (dict);
alter table ${table-prefix}${msight_sky-schema}.${table-prefix}sky_summ_hourly_profile_stg modify column property integer (dict);
alter table ${table-prefix}${msight_sky-schema}.${table-prefix}sky_summ_hourly_profile_stg modify column photography integer (dict);
alter table ${table-prefix}${msight_sky-schema}.${table-prefix}sky_summ_hourly_profile_stg modify column job_search integer (dict);
alter table ${table-prefix}${msight_sky-schema}.${table-prefix}sky_summ_hourly_profile_stg modify column music integer (dict);
alter table ${table-prefix}${msight_sky-schema}.${table-prefix}sky_summ_hourly_profile_stg modify column news integer (dict);
alter table ${table-prefix}${msight_sky-schema}.${table-prefix}sky_summ_hourly_profile_stg modify column productivity integer (dict);
alter table ${table-prefix}${msight_sky-schema}.${table-prefix}sky_summ_hourly_profile_stg modify column social_media integer (dict);
alter table ${table-prefix}${msight_sky-schema}.${table-prefix}sky_summ_hourly_profile_stg modify column sports integer (dict);
alter table ${table-prefix}${msight_sky-schema}.${table-prefix}sky_summ_hourly_profile_stg modify column technology integer (dict);
alter table ${table-prefix}${msight_sky-schema}.${table-prefix}sky_summ_hourly_profile_stg modify column transportation integer (dict);
alter table ${table-prefix}${msight_sky-schema}.${table-prefix}sky_summ_hourly_profile_stg modify column travel integer (dict);
alter table ${table-prefix}${msight_sky-schema}.${table-prefix}sky_summ_hourly_profile_stg modify column video integer (dict);
alter table ${table-prefix}${msight_sky-schema}.${table-prefix}sky_summ_hourly_profile_stg modify column arpu_very_low integer (dict);
alter table ${table-prefix}${msight_sky-schema}.${table-prefix}sky_summ_hourly_profile_stg modify column arpu_low integer (dict);
alter table ${table-prefix}${msight_sky-schema}.${table-prefix}sky_summ_hourly_profile_stg modify column arpu_medium integer (dict);
alter table ${table-prefix}${msight_sky-schema}.${table-prefix}sky_summ_hourly_profile_stg modify column arpu_high integer (dict);
alter table ${table-prefix}${msight_sky-schema}.${table-prefix}sky_summ_hourly_profile_stg modify column arpu_very_high integer (dict);
alter table ${table-prefix}${msight_sky-schema}.${table-prefix}sky_summ_hourly_profile_stg modify column arpu_top_usage integer (dict);

delete from ${table-prefix}${msight_sky-schema}.${table-prefix}sky_summ_hourly_profile where months = '${run-date=yyyyMM}';

insert into ${table-prefix}${msight_sky-schema}.${table-prefix}sky_summ_hourly_profile
select '${run-date=yyyyMM}' as months,
* from ${table-prefix}${msight_sky-schema}.${table-prefix}sky_summ_hourly_profile_stg;

DROP TABLE IF EXISTS ${table-prefix}${msight_sky-schema}.${table-prefix}sky_summ_hourly_profile_input;
DROP TABLE IF EXISTS ${table-prefix}${msight_sky-schema}.${table-prefix}sky_summ_hourly_profile_stg;
