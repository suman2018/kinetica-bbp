create or replace table ${table-prefix}${hvc-schema}.${table-prefix}hvc_segment_movement_summ_2
( 
   cut_off_report     varchar(16, dict)     
  ,hvc_segment_now    varchar(4, shard_key)
  ,hvc_segment_before varchar(4, dict)     
  ,sales              bigint                
  ,churn              bigint                
  ,stay               bigint                
  ,"move"               bigint                
  ,cut_off_dt         date     
);
