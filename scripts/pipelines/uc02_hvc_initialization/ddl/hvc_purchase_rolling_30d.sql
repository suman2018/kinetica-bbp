--  hive table partitioned by (cut_off_dt string)
create or replace table ${table-prefix}${hvc-schema}.${table-prefix}hvc_purchase_rolling_30d
(
   cut_off_dt      		 date(dict)     
  ,msisdn          		 long(shard_key)
  ,activation_channel_id varchar(8, dict)
  ,tot_trx               integer               
);
