-- needs optimal shard strategy
create or replace table ${table-prefix}${hvc-schema}.${table-prefix}hvc_summary_demography
(
  hvc_segment                  varchar(4, dict)     
  ,brnd_nme                    varchar(16, shard_key)
  ,age                         integer(shard_key)    
  ,gndr                        varchar(1, dict)     
  ,mrtl_stts                   varchar(32, dict)     
  ,first_rank_category         varchar(16, dict)     
  ,first_rank_apps             varchar(64, dict)     
  ,tot_vol_games_byte          bigint                
  ,tot_vol_socialnet_byte      bigint                
  ,tot_vol_video_byte          bigint                
  ,tot_vol_music_byte          bigint                
  ,tot_vol_communications_byte bigint                
  ,tot_vol_sports_byte         bigint                
  ,tot_vol_ecommerce_byte      bigint                
  ,tot_vol_travel_food_byte    bigint                
  ,tot_vol_news_lifestyle_byte bigint                
  ,tot_subs                    bigint not null       
  ,cut_off_dt                  date (dict)              
);
