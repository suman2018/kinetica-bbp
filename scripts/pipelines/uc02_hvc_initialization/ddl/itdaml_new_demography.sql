create or replace table ${table-prefix}${hvc-schema}.${table-prefix}hvc_itdaml_demography (
   cut_off_report varchar(16, dict)     
  ,msisdn         long (shard_key)
  ,pre_post       varchar(4, dict)     
  ,nokk           varchar(16)           
  ,nik            varchar(16)           
  ,age            integer               
  ,gndr           varchar(1, dict)     
  ,mrtl_stts      varchar(32, dict)     
  ,hshld_stts     varchar(16, dict)     
  ,month          varchar(8, dict)     
) ;


