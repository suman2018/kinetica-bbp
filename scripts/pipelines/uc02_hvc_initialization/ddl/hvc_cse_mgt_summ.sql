create or replace table ${table-prefix}${hvc-schema}.${table-prefix}hvc_cse_mgt_summ
( 
   cut_off_report    varchar(16, dict)     
  ,los_segment       varchar(32, dict)     
  ,arpu_segment      varchar(8, dict)     
  ,hvc_segment       varchar(4, dict)     
  ,brand             varchar(16, shard_key)
  ,area              varchar(8, dict)     
  ,region            varchar(32, shard_key)
  ,cluster_name      varchar(32, dict)     
  ,cstmr_typ         varchar(16, dict)     
  ,case_typ1         varchar(64, dict)     
  ,case_typ2         varchar(64, dict)     
  ,case_typ3         varchar(128, dict)    
  ,case_sttus        varchar(32, dict)     
  ,case_sttus_final  varchar(32, dict)     
  ,subs              double                
  ,cnt_case_id       double                
  ,speed_of_service  varchar(8, dict)     
  ,duration          double                
  ,duration_category varchar(16, dict)     
  ,cut_off_dt        date (dict)
);
