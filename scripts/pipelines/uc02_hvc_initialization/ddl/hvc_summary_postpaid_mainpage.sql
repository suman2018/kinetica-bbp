create or replace table ${table-prefix}${hvc-schema}.${table-prefix}hvc_summary_postpaid_mainpage 
(
   cut_off_report    varchar(16, dict)         
  ,tcash_flg         varchar(1, dict)         
  ,bill_cycle        varchar(4, dict)         
  ,inv_month         integer            
  ,los_segment       varchar(32, dict)
  ,area_hlr          varchar(8, dict)        
  ,region_hlr        varchar(32, dict)        
  ,city              varchar(32, dict)        
  ,flg_youth         varchar(1, dict) 
  ,hvc_segment       varchar(8, dict) 
  ,arpu_segment      varchar(8, dict)         
  ,tot_bill_amount double             
  ,subs              bigint   
  ,cut_off_dt        date (dict)
);
