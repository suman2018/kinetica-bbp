-- in hive, parttioned by cut_off_dt as a property of table.
create or replace table ${table-prefix}${hvc-schema}.${table-prefix}hvc_rgb_rolling  
(
    brand                varchar(16, dict),         
    cut_off_report       varchar(16, dict),      
    msisdn               long(shard_key),    
    area                 varchar(8, dict),               
    region               varchar(32, dict),       
    cluster_name         varchar(32, dict),     
    rev_total            integer(dict),            
    rev_voice            integer(dict),            
    rev_sms              integer(dict),            
    rev_broad            integer(dict),       
    rev_digital          integer(dict),         
    rev_roaming          integer(dict),         
    rev_other            integer(dict),
    arpu_segment         varchar(8, dict),
    arpu_voice_segment   varchar(8, dict),
    arpu_sms_segment     varchar(8, dict),
    arpu_broad_segment   varchar(8, dict),
    arpu_digital_segment varchar(8, dict),
    arpu_roaming_segment varchar(8, dict),
    arpu_other_segment   varchar(8, dict) ,
    cut_off_dt           date(dict)
);

