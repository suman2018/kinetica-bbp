create or replace table ${table-prefix}${hvc-schema}.${table-prefix}hvc_traffic_dominant_area_summ
(
    area                   varchar(8, shard_key),
    hvc_segment            varchar(4, shard_key),
    dominant_service_area  varchar(16, dict),
    cut_off_dt             date(dict)
);
