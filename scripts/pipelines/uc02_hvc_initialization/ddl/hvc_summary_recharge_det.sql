create or replace table ${table-prefix}${hvc-schema}.${table-prefix}hvc_summary_recharge_det
(
   cut_off_report varchar(16, dict)     
  ,los_segment    varchar(32, dict)     
  ,arpu_segment   varchar(8, dict)     
  ,hvc_segment    varchar(4, dict)     
  ,brand          varchar(16, shard_key)
  ,area           varchar(8, dict)     
  ,region         varchar(32, shard_key)
  ,cluster_name   varchar(32, dict)     
  ,chnnl_grp      varchar(32, dict)     
  ,chnnl_ctgry    varchar(32, dict)     
  ,chnnl_nme      varchar(64, dict)     
  ,rchrg_trx      double                
  ,rchrg_amt      double                
  ,count_msisdn   double                
  ,cut_off_dt     date(dict)      
);
