create or replace table ${table-prefix}${hvc-schema}.${table-prefix}hvc_traffic_summ
 (
  cut_off_report                    varchar(16, dict)     
  ,los_segment                      varchar(32, shard_key)     
  ,arpu_segment                     varchar(8, shard_key)     
  ,hvc_segment                      varchar(4, shard_key)     
  ,brand                            varchar(16, dict)
  ,area                             varchar(8, dict)     
  ,region                           varchar(32, dict)
  ,cluster_name                     varchar(32, dict)     
  ,rev_total_broadband              bigint                
  ,rev_broadband_pckg               bigint                
  ,rev_broadband_payu               bigint                
  ,total_payload_broadband          bigint                
  ,payload_pckg                     bigint                
  ,payload_payu                     bigint                
  ,arpu_segment_broadband           varchar(8, dict)     
  ,arpu_segment_broadband_pckg      varchar(8, dict)     
  ,arpu_segment_broadband_payu      varchar(8, dict)     
  ,broadband_subs                   integer
  ,broadband_pckg_subs              integer
  ,broadband_payu_subs              integer
  ,no_dominant_rev_broad_subs       integer
  ,dominant_rev_broad_pckg_subs     integer
  ,dominant_rev_broad_payu_subs     integer
  ,no_dominant_payload_broad_subs   integer
  ,dominant_payload_broad_pckg_subs integer
  ,dominant_payload_broad_payu_subs integer               
  ,rev_total_sms                    bigint                
  ,rev_sms_pckg                     bigint                
  ,rev_sms_payu                     bigint                
  ,total_cnt_sms                    integer                
  ,cnt_sms_pckg                     integer               
  ,cnt_sms_payu                     integer           
  ,arpu_segment_sms                 varchar(8, dict)     
  ,arpu_segment_sms_pckg            varchar(8, dict)     
  ,arpu_segment_sms_payu            varchar(8, dict)     
  ,sms_subs                         integer                
  ,sms_pckg_subs                    integer               
  ,sms_payu_subs                    integer               
  ,no_dominant_rev_sms_subs         integer               
  ,dominant_rev_sms_pckg_subs       integer               
  ,dominant_rev_sms_payu_subs       integer               
  ,no_dominant_cnt_sms_subs         integer               
  ,dominant_cnt_sms_pckg_subs       integer               
  ,dominant_cnt_sms_payu_subs       integer               
  ,rev_total_voice                  bigint                
  ,rev_voice_pckg                   bigint                
  ,rev_voice_payu                   bigint                
  ,total_dur_voice                  bigint                
  ,dur_voice_pckg                   bigint                
  ,dur_voice_payu                   bigint                
  ,arpu_segment_voice               varchar(8, dict)     
  ,arpu_segment_voice_pckg          varchar(8, dict)     
  ,arpu_segment_voice_payu          varchar(8, dict)     
  ,voice_subs                       integer               
  ,voice_pckg_subs                  integer               
  ,voice_payu_subs                  integer               
  ,no_dominant_rev_voice_subs       integer               
  ,dominant_rev_voice_pckg_subs     integer               
  ,dominant_rev_voice_payu_subs     integer               
  ,no_dominant_dur_voice_subs       integer               
  ,dominant_dur_voice_pckg_subs     integer               
  ,dominant_dur_voice_payu_subs     integer               
  ,cut_off_dt                       date(dict)   
);
