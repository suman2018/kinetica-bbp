-- mimics the above loading into this. I have this just so SQL below works.
create or replace table ${table-prefix}${hvc-schema}.${table-prefix}hvc_cb_rolling
(
   cut_off_report varchar(16, dict)     
  ,msisdn         long(shard_key)
  ,brnd_nme       varchar(16, dict)     
  ,los            bigint                
  ,los_segment    varchar(32, dict)     
  ,area_name      varchar(8, dict)     
  ,region_name    varchar(32, dict)     
  ,cluster_name   varchar(32, dict)     
  ,pre2post_flg   varchar(1, dict)     
  ,flg_youth      varchar(1, dict)     
  ,cut_off_dt     date(dict)     
);

