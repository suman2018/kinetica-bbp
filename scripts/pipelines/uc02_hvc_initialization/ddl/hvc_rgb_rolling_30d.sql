-- in hive, parttioned by cut_off_dt as a property of table.
create or replace table ${table-prefix}${hvc-schema}.${table-prefix}hvc_rgb_rolling_30d  
(
    cut_off_dt           date(dict),
    msisdn               long(shard_key),    
    rev_total            integer(dict),            
    rev_voice            integer(dict),            
    rev_sms              integer(dict),            
    rev_broad            integer(dict),       
    rev_digital          integer(dict),         
    rev_roaming          integer(dict),         
    rev_other            integer(dict)
    
);

