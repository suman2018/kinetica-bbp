create or replace table ${table-prefix}${hvc-schema}.${table-prefix}hvc_traffic_dominant_cluster_summ
(
    area                  varchar(8,  shard_key),
    region                varchar(32, shard_key),
    cluster               varchar(32, dict),
    hvc_segment           varchar(4, dict),
    dominant_service_area varchar(16, dict),
    cut_off_dt            date(dict)
);
