create or replace table ${table-prefix}${hvc-schema}.${table-prefix}hvc_summary_postpaid_cb_rev
(
   cut_off_report  varchar(16, dict)        
  ,msisdn          long (shard_key)   
  ,tcash_flg       varchar(1, dict)         
  ,bill_cycle      varchar(4, dict)         
  ,inv_month       integer                  
  ,tot_bill_amount double                   
  ,los             integer          
  ,los_segment     varchar(32, dict) 
  ,area_hlr        varchar(8, dict)        
  ,region_hlr      varchar(32, dict)        
  ,city            varchar(32, dict)        
  ,flg_youth       varchar(1, dict) 
  ,hvc_segment     varchar(8, dict)
  ,arpu_segment    varchar(8, dict)         
  ,cut_off_dt      date (dict)              
);
