create or replace table ${table-prefix}${hvc-schema}.${table-prefix}hvc_rech_rolling_30d
(
  cut_off_dt      date(dict)   
  ,msisdn         long(shard_key)
  ,chnnl_grp      varchar(32, dict)     
  ,chnnl_ctgry    varchar(32, dict)     
  ,chnnl_nme      varchar(64, dict)     
  ,rchrg_trx      integer(dict)                
  ,rchrg_amt      integer(dict)                        
);
