create or replace table ${table-prefix}${hvc-schema}.${table-prefix}hvc_summary_rgb_2
(
  cut_off_report  varchar(16, dict)     
  ,brand           varchar(16, shard_key)
  ,cluster_name    varchar(32, dict)     
  ,region          varchar(32, shard_key)
  ,area            varchar(8, dict)     
  ,los_segment     varchar(32, dict)     
  ,arpu_segment    varchar(8, dict)     
  ,hvc_segment     varchar(4, dict)     
  ,subs            bigint                
  ,subs_voice      bigint                
  ,subs_sms        bigint                
  ,subs_broad      bigint                
  ,subs_digital    bigint                
  ,subs_roaming    bigint                
  ,subs_other      bigint                
  ,non_rgb         bigint                
  ,non_rgb_voice   bigint                
  ,non_rgb_sms     bigint                
  ,non_rgb_broad   bigint                
  ,non_rgb_digital bigint                
  ,non_rgb_roaming bigint                
  ,non_rgb_other   bigint                
  ,rev_total       double                
  ,rev_voice       double                
  ,rev_sms         double                
  ,rev_broad       double                
  ,rev_digital     double                
  ,rev_roaming     double                
  ,rev_other       double                
  ,cut_off_dt      date(dict)      
);
