create or replace table ${table-prefix}${hvc-schema}.${table-prefix}hvc_summary_mainpage_2
(
  cut_off_report     varchar(16, dict)     
  ,los_segment        varchar(32, dict)     
  ,arpu_segment       varchar(8, dict)     
  ,hvc_segment        varchar(4, dict)     
  ,brand              varchar(16, shard_key)
  ,cluster_name       varchar(32, dict)     
  ,region             varchar(32, shard_key)
  ,area               varchar(8, dict)     
  ,subs               bigint                
  ,subs_voice         bigint                
  ,subs_sms           bigint                
  ,subs_broad         bigint                
  ,subs_digital       bigint                
  ,subs_roaming       bigint                
  ,subs_other         bigint                
  ,non_rgb            bigint                
  ,non_rgb_voice      bigint                
  ,non_rgb_sms        bigint                
  ,non_rgb_broad      bigint                
  ,non_rgb_digital    bigint                
  ,non_rgb_roaming    bigint                
  ,non_rgb_other      bigint                
  ,rev_total          double                
  ,rev_voice          double                
  ,rev_sms            double                
  ,rev_broad          double                
  ,rev_digital        double                
  ,rev_roaming        double                
  ,rev_other          double                
  ,subs_bl            bigint                
  ,subs_voice_bl      bigint                
  ,subs_sms_bl        bigint                
  ,subs_broad_bl      bigint                
  ,subs_digital_bl    bigint                
  ,subs_roaming_bl    bigint                
  ,subs_other_bl      bigint                
  ,non_rgb_bl         bigint                
  ,non_rgb_voice_bl   bigint                
  ,non_rgb_sms_bl     bigint                
  ,non_rgb_broad_bl   bigint                
  ,non_rgb_digital_bl bigint                
  ,non_rgb_roaming_bl bigint                
  ,non_rgb_other_bl   bigint                
  ,rev_total_bl       double                
  ,rev_voice_bl       double                
  ,rev_sms_bl         double                
  ,rev_broad_bl       double                
  ,rev_digital_bl     double                
  ,rev_roaming_bl     double                
  ,rev_other_bl       double                
  ,cut_off_dt         date
);
