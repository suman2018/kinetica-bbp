create or replace table ${table-prefix}${hvc-schema}.${table-prefix}hvc_bcp_persona
(
   msisdn                  long (shard_key)
  ,regional_channel        varchar(32, dict)     
  ,city                    varchar(32, dict)     
  ,vol_games_byte          bigint                
  ,vol_socialnet_byte      bigint                
  ,vol_video_byte          bigint                
  ,vol_music_byte          bigint                
  ,vol_communications_byte bigint                
  ,vol_sports_byte         bigint                
  ,vol_ecommerce_byte      bigint                
  ,vol_travel_food_byte    bigint                
  ,vol_news_lifestyle_byte bigint                
  ,first_rank_category     varchar(16, dict)     
  ,second_rank_category    varchar(16, dict)     
  ,third_rank_category     varchar(16, dict)     
  ,fourth_rank_category    varchar(16, dict)     
  ,fifth_rank_category     varchar(16, dict)     
  ,first_rank_apps         varchar(64, dict)     
  ,second_rank_apps        varchar(64, dict)     
  ,third_rank_apps         varchar(64, dict)     
  ,fourth_rank_apps        varchar(64, dict)     
  ,fifth_rank_apps         varchar(64, dict)     
  ,cut_off_dt              date (dict)                
);
