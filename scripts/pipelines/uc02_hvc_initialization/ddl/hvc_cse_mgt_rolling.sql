-- in hive: partitioned by (cut_off_dt)
create or replace table ${table-prefix}${hvc-schema}.${table-prefix}hvc_cse_mgt_rolling
(
   cut_off_dttime    varchar(32, dict)     
  ,msisdn            long(shard_key)
  ,case_id           varchar(16, dict)           
  ,cstmr_typ         varchar(16, dict)     
  ,case_typ1         varchar(64, dict)     
  ,case_typ2         varchar(64, dict)     
  ,case_typ3         varchar(128, dict)    
  ,case_crtn_dt_tm   varchar(32, dict)           
  ,case_clsd_dt_tm   varchar(32, dict)           
  ,case_sttus        varchar(32, dict)     
  ,case_sttus_final  varchar(32, dict)     
  ,speed_of_service  varchar(8, dict)     
  ,duration          double                
  ,duration_category varchar(16, dict)     
  ,cut_off_dt        date (dict)                 
);
