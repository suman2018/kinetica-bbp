-- mimics the above loading into this. I have this just so SQL below works.
create or replace table ${table-prefix}${hvc-schema}.${table-prefix}hvc_cb_baseline
(
   msisdn    bigint(shard_key)
  ,brnd_nme  varchar(16, dict)
  ,area      varchar(8, dict) 
  ,region    varchar(32, dict)
  ,cluster   varchar(32, dict)
  ,rev_total float            
) ;