create or replace table ${table-prefix}${hvc-schema}.${table-prefix}hvc_segment_postpaid_movement_summ_bl 
(
   cut_off_report     varchar(16,dict) not null
  ,hvc_segment_now    varchar(8, shard_key)         
  ,hvc_segment_before varchar(8, shard_key)         
  ,sales              bigint not null    
  ,churn              bigint (dict) not null    
  ,stay               bigint (dict) not null    
  ,"move"               bigint (dict) not null    
  ,cut_off_dt         date (dict)
);
