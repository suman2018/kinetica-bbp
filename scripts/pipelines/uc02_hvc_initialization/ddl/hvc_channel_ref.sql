-- mimics the above loading into this. I have this just so SQL below works.
create or replace replicated table ${table-prefix}${reference-schema}.${table-prefix}channel_ref
(
   file_date     varchar(16, dict)
  ,act_chnnl_id  varchar(4, dict) 
  ,act_chnnl_nme varchar(64, dict)
  ,chnnl_prfx    varchar(1, dict) 
  ,chnnl_dscrptn varchar(64, dict)
) ;
