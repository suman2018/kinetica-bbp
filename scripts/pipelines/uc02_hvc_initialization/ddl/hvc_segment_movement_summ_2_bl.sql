-- partitioned by (cut_off_dt string)
create or replace table ${table-prefix}${hvc-schema}.${table-prefix}hvc_segment_movement_summ_2_bl
(
    cut_off_report       varchar(16, dict),                   -- @Mate: derived from cut_off_dt, synonymous
    hvc_segment_now      varchar(4, dict),
    hvc_segment_before   varchar(4, dict),
    sales                long,
    churn                long,
    stay                 long,
    "move"                 long,
    churn_subs           long, 
    move_postpaid        long,             -- derived from msisdn
    cut_off_dt           date (dict)
);
