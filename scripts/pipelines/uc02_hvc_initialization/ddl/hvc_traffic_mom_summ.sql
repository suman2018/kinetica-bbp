create or replace table ${table-prefix}${hvc-schema}.${table-prefix}hvc_traffic_mom_summ
(
  cut_off_report              varchar(16, shard_key)   
  ,los_segment                varchar(32) not null     
  ,arpu_segment               varchar(8, dict) not null
  ,hvc_segment                varchar(4) not null      
  ,brand                      varchar(16, dict) not null
  ,area                       varchar(8, dict) not null
  ,region                     varchar(32) not null     
  ,cluster_name               varchar(32, shard_key)   
  ,rev_total_broadband        bigint                   
  ,rev_broadband_pckg         bigint                   
  ,rev_broadband_payu         bigint                   
  ,total_payload_broadband    bigint                   
  ,payload_pckg               bigint                   
  ,payload_payu               bigint                   
  ,rev_total_sms              bigint                   
  ,rev_sms_pckg               bigint not null          
  ,rev_sms_payu               bigint                   
  ,total_cnt_sms              bigint                   
  ,cnt_sms_pckg               bigint (dict) not null          
  ,cnt_sms_payu               bigint (dict) not null          
  ,rev_total_voice            bigint                   
  ,rev_voice_pckg             bigint                   
  ,rev_voice_payu             bigint                   
  ,total_dur_voice            bigint                   
  ,dur_voice_pckg             bigint                   
  ,dur_voice_payu             bigint                   
  ,rev_total_broadband_lm     bigint                   
  ,rev_broadband_pckg_lm      bigint                   
  ,rev_broadband_payu_lm      bigint                   
  ,total_payload_broadband_lm bigint                   
  ,payload_pckg_lm            bigint                   
  ,payload_payu_lm            bigint                   
  ,rev_total_sms_lm           bigint                   
  ,rev_sms_pckg_lm            bigint not null          
  ,rev_sms_payu_lm            bigint                   
  ,total_cnt_sms_lm           bigint                   
  ,cnt_sms_pckg_lm            bigint (dict) not null          
  ,cnt_sms_payu_lm            bigint (dict) not null          
  ,rev_total_voice_lm         bigint                   
  ,rev_voice_pckg_lm          bigint                   
  ,rev_voice_payu_lm          bigint                   
  ,total_dur_voice_lm         bigint                   
  ,dur_voice_pckg_lm          bigint                   
  ,dur_voice_payu_lm          bigint                   
  ,cut_off_dt                 date (dict) not null            
);
