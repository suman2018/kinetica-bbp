-- in hive, parttioned by cut_off_dt as a property of table.
create or replace table ${table-prefix}${hvc-schema}.${table-prefix}hvc_traffic_rolling_30d
(
  cut_off_dt                   date (dict)
  ,msisdn                      long(shard_key)
  ,rev_broadband_pckg          bigint(dict)                
  ,rev_broadband_payu          bigint(dict)          
  ,payload_pckg                bigint(dict)          
  ,payload_payu                bigint(dict)          
  ,rev_sms_pckg                bigint(dict)          
  ,rev_sms_payu                bigint(dict)          
  ,cnt_sms_pckg                integer(dict)          
  ,cnt_sms_payu                integer(dict)          
  ,rev_voice_pckg              bigint(dict)
  ,rev_voice_payu              bigint(dict)          
  ,dur_voice_pckg              bigint(dict)          
  ,dur_voice_payu              bigint(dict)          
 );
