create or replace table ${table-prefix}${hvc-schema}.${table-prefix}hvc_community_cb_dou (
   month_id        varchar(8, dict)     
  ,msisdn          long (shard_key)
);
