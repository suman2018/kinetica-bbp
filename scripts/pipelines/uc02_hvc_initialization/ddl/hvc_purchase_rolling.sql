--  hive table partitioned by (cut_off_dt string)
create or replace table ${table-prefix}${hvc-schema}.${table-prefix}hvc_purchase_rolling
(
   cut_off_report  varchar(16, dict)     
  ,msisdn          long(shard_key)
  ,channel_1st_id  varchar(64, dict)     
  ,channel_2nd_id  varchar(64, dict)     
  ,channel_3rd_id  varchar(64, dict)     
  ,channel_4th_id  varchar(64, dict)     
  ,channel_5th_id  varchar(64, dict)     
  ,trx_channel_1st integer               
  ,trx_channel_2nd integer               
  ,trx_channel_3rd integer               
  ,trx_channel_4th integer               
  ,trx_channel_5th integer               
  ,trx_all_channel integer               
  ,not_unk_channel varchar(64, dict)     
  ,cut_off_dt      date(dict)                  
);
