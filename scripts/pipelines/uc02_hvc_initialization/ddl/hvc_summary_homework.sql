create or replace table ${table-prefix}${hvc-schema}.${table-prefix}hvc_summary_homework
(
   hvc_segment       varchar(4, dict)     
  ,brnd_nme          varchar(16, shard_key)
  ,home_area_name    varchar(8, dict)      
  ,home_region_name  varchar(32, shard_key)
  ,home_cluster_name varchar(32, dict)     
  ,work_area_name    varchar(8, dict)      
  ,work_region_name  varchar(32, dict)     
  ,work_cluster_name varchar(32, dict)     
  ,tot_subs          bigint not null       
  ,cut_off_dt        date (dict)                 
);
