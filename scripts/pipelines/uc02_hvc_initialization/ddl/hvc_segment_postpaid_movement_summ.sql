create or replace table ${table-prefix}${hvc-schema}.${table-prefix}hvc_segment_postpaid_movement_summ
(
   cut_off_report     varchar(16,dict) not null
  ,area_now           varchar(8, dict)
  ,region_now         varchar(32, dict)
  ,city_now           varchar(32, dict)
  ,area_before        varchar(8, dict)
  ,region_before      varchar(32, dict)
  ,city_before        varchar(32, dict)
  ,hvc_segment_now    varchar(8, shard_key)         
  ,hvc_segment_before varchar(8, shard_key)         
  ,sales              bigint (dict) not null    
  ,churn              bigint (dict) not null    
  ,stay               bigint (dict) not null    
  ,"move"               bigint (dict) not null    
  ,cut_off_dt         date (dict)
);
