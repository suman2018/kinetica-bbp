create or replace table ${table-prefix}${hvc-schema}.${table-prefix}hvc_rech_rolling
(
  cut_off_report  varchar(16, dict)     
  ,msisdn         long (shard_key)
  ,chnnl_grp      varchar(32, dict)     
  ,chnnl_ctgry    varchar(32, dict)     
  ,chnnl_nme      varchar(64, dict)     
  ,rchrg_trx      integer(dict)                
  ,rchrg_amt      integer(dict)          
  ,rchrg_segment  varchar(8, dict)     
  ,cut_off_dt     date (dict)                 
);
