create or replace table ${table-prefix}${hvc-schema}.${table-prefix}hvc_purchase_summ
( 
  cut_off_report varchar(16, dict)     
  ,los_segment    varchar(32, dict)     
  ,arpu_segment   varchar(8, dict)     
  ,hvc_segment    varchar(4, dict)     
  ,brand          varchar(16, shard_key)
  ,area           varchar(8, shard_key)
  ,region         varchar(32, dict)     
  ,cluster_name   varchar(32, dict)     
  ,channel_name   varchar(64, dict)     
  ,subs           bigint                
  ,trx_channel    bigint                
  ,cut_off_dt     date (dict)                 
);
