create or replace table ${table-prefix}${cms_stg-schema}.${table-prefix}cms_awrd_prgm_f_input
(
    "dt_dt" date(dict),
    "msisdn" bigint(shard_key),
    "chnnl" varchar(32, dict),
    "mssg" varchar(32, dict),
    "pnt" int(dict),
    "trnsctn_sttus_cd" int(dict),
    "ctgry" varchar(32, dict),
    "prty_awrd_typ" varchar(8, dict),
    "awrd_prgrm_trnsctn_dttm" varchar(32, dict)
)