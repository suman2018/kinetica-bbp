select
dt_dt
,bigint(msisdn) as msisdn
,chnnl
,mssg
,int(pnt) as pnt
,int(trnsctn_sttus_cd) as trnsctn_sttus_cd
,ctgry
,prty_awrd_typ
,awrd_prgrm_trnsctn_dttm
from gpu.awrd_prgm_f
where periode = '${run-date=yyyyMMdd}'