/* KI_HINT_DICT_PROJECTION */
create or replace temp table ${table-prefix}${hvc-schema}.${table-prefix}hvc_rgb_rolling_daily_stg as 
select msisdn, 
coalesce(sum(rev),0) as rev_total, 
sum(case when trim(l1_name) = 'Voice P2P' then rev else 0 end) as rev_voice,
sum(case when trim(l1_name) = 'SMS P2P' then rev else 0 end) as rev_sms,
sum(case when trim(l1_name) = 'Broadband' then rev else 0 end) as rev_broad,
sum(case when trim(l1_name) = 'Digital Services' then rev else 0 end) as rev_digital,
sum(case when trim(l1_name) = 'International Roaming' then rev else 0 end) as rev_roaming,
sum(case when trim(l1_name) = 'Others' then rev else 0 end) as rev_other, ki_shard_key(msisdn)
FROM ${table-prefix}${revenue-schema}.${table-prefix}chg_rcg_staging_${run-date=yyyyMMdd}
group by msisdn
;


--- insertion into rolling 30d detail
delete from ${table-prefix}${hvc-schema}.${table-prefix}hvc_rgb_rolling_30d where cut_off_dt = '${run-date=yyyy-MM-dd}';

insert into ${table-prefix}${hvc-schema}.${table-prefix}hvc_rgb_rolling_30d
    select '${run-date=yyyy-MM-dd}' as cut_off_dt , a.*
    from ${table-prefix}${hvc-schema}.${table-prefix}hvc_rgb_rolling_daily_stg a;

 DROP TABLE IF EXISTS  ${table-prefix}${hvc-schema}.${table-prefix}hvc_rgb_rolling_daily_stg;


