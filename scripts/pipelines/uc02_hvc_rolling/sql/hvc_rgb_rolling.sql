-- create view to summarize the rolling
/* KI_HINT_DICT_PROJECTION */
create or replace temp table ${table-prefix}${hvc-schema}.${table-prefix}hvc_rgb_rolling_30d_stg1 as 
select 
'${run-date=yyyy-MM-dd}' as cut_off_report, 
msisdn, 
sum(rev_total) as rev_total, 
sum(rev_voice) as rev_voice,
sum(rev_sms) as rev_sms, 
sum(rev_broad) as rev_broad,
sum(rev_digital) as rev_digital,
sum(rev_roaming) as rev_roaming, 
sum(rev_other) as rev_other,
DATE '${run-date=yyyy-MM-dd}' as cut_off_dt,
ki_shard_key(msisdn)
from ${table-prefix}${hvc-schema}.${table-prefix}hvc_rgb_rolling_30d
where cut_off_dt between  DATE '${run-date=yyyy-MM-dd}' - interval '29' day and  DATE '${run-date=yyyy-MM-dd}'
group by msisdn;

/* KI_HINT_DICT_PROJECTION */
create or replace temp table ${table-prefix}${hvc-schema}.${table-prefix}hvc_rgb_rolling_30d_stg2 as 
select 
cut_off_report, 
a.msisdn, 
coalesce(b.brnd_nme, 'UNKNOWN') as brand,
coalesce(b.cluster_name, 'UNKNOWN') as cluster_name,
rev_total, 
rev_voice,
rev_sms, 
rev_broad,
rev_digital,
rev_roaming, 
rev_other,
cut_off_dt,
ki_shard_key(a.msisdn)
FROM ${table-prefix}${hvc-schema}.${table-prefix}hvc_rgb_rolling_30d_stg1 a
    left outer join
    (select msisdn, brnd_nme, cluster_name from     
        (select msisdn, brnd_nme, cluster_name, row_number() over(partition by msisdn order by los desc,brnd_nme desc) rn
            from ${table-prefix}${hvc-schema}.${table-prefix}hvc_cb_rolling
                        where cut_off_dt = DATE '${run-date=yyyy-MM-dd}'
        ) a where rn =1) b      
on a.msisdn=b.msisdn
;


/* KI_HINT_DICT_PROJECTION */
create or replace temp table ${table-prefix}${hvc-schema}.${table-prefix}hvc_rgb_rolling_temp as
select
        a.brand,
        a.cut_off_report,
        a.msisdn,
        coalesce(b.area_name, 'UNKNOWN') as area_name,
        coalesce(b.region_name, 'UNKNOWN') as region_name,
        coalesce(a.cluster_name, 'UNKNOWN') as cluster_name,
        coalesce(rev_total,0) as rev_total,
        coalesce(rev_voice,0) as rev_voice,
        coalesce(rev_sms,0) as rev_sms,
        coalesce(rev_broad,0) as rev_broad,
        coalesce(rev_digital,0) as rev_digital,
        coalesce(rev_roaming,0) as rev_roaming,
        coalesce(rev_other,0) as rev_other,
        case
        WHEN COALESCE(REV_TOTAL,0) <= 50000 THEN '0-50K'
        WHEN COALESCE(REV_TOTAL,0) < 65000 THEN '50-65K'
        WHEN COALESCE(REV_TOTAL,0) < 70000 THEN '65-70K'
        WHEN COALESCE(REV_TOTAL,0) < 120000 THEN '70-120K'
        WHEN COALESCE(REV_TOTAL,0) < 150000 THEN '120-150K'
        WHEN COALESCE(REV_TOTAL,0) < 180000 THEN '150-180K'
        WHEN COALESCE(REV_TOTAL,0) < 210000 THEN '180-210K'
        WHEN COALESCE(REV_TOTAL,0) < 240000 THEN '210-240K'
        WHEN COALESCE(REV_TOTAL,0) >= 240000 THEN '>240K'
        end as arpu_segment,
        case
        WHEN COALESCE(REV_VOICE,0) <= 50000 THEN '0-50K'
        WHEN COALESCE(REV_VOICE,0) < 65000 THEN '50-65K'
        WHEN COALESCE(REV_VOICE,0) < 70000 THEN '65-70K'
        WHEN COALESCE(REV_VOICE,0) < 120000 THEN '70-120K'
        WHEN COALESCE(REV_VOICE,0) < 150000 THEN '120-150K'
        WHEN COALESCE(REV_VOICE,0) < 180000 THEN '150-180K'
        WHEN COALESCE(REV_VOICE,0) < 210000 THEN '180-210K'
        WHEN COALESCE(REV_VOICE,0) < 240000 THEN '210-240K'
        WHEN COALESCE(REV_VOICE,0) >= 240000 THEN '>240K'
        end as arpu_voice_segment,
        case
        WHEN COALESCE(REV_SMS,0) <= 50000 THEN '0-50K'
        WHEN COALESCE(REV_SMS,0) < 65000 THEN '50-65K'
        WHEN COALESCE(REV_SMS,0) < 70000 THEN '65-70K'
        WHEN COALESCE(REV_SMS,0) < 120000 THEN '70-120K'
        WHEN COALESCE(REV_SMS,0) < 150000 THEN '120-150K'
        WHEN COALESCE(REV_SMS,0) < 180000 THEN '150-180K'
        WHEN COALESCE(REV_SMS,0) < 210000 THEN '180-210K'
        WHEN COALESCE(REV_SMS,0) < 240000 THEN '210-240K'
        WHEN COALESCE(REV_SMS,0) >= 240000 THEN '>240K'
        end as arpu_sms_segment,
        case
        WHEN COALESCE(REV_BROAD,0) <= 50000 THEN '0-50K'
        WHEN COALESCE(REV_BROAD,0) < 65000 THEN '50-65K'
        WHEN COALESCE(REV_BROAD,0) < 70000 THEN '65-70K'
        WHEN COALESCE(REV_BROAD,0) < 120000 THEN '70-120K'
        WHEN COALESCE(REV_BROAD,0) < 150000 THEN '120-150K'
        WHEN COALESCE(REV_BROAD,0) < 180000 THEN '150-180K'
        WHEN COALESCE(REV_BROAD,0) < 210000 THEN '180-210K'
        WHEN COALESCE(REV_BROAD,0) < 240000 THEN '210-240K'
        WHEN COALESCE(REV_BROAD,0) >= 240000 THEN '>240K'
        end as arpu_broad_segment,
        case
        WHEN COALESCE(REV_DIGITAL,0) <= 50000 THEN '0-50K'
        WHEN COALESCE(REV_DIGITAL,0) < 65000 THEN '50-65K'
        WHEN COALESCE(REV_DIGITAL,0) < 70000 THEN '65-70K'
        WHEN COALESCE(REV_DIGITAL,0) < 120000 THEN '70-120K'
        WHEN COALESCE(REV_DIGITAL,0) < 150000 THEN '120-150K'
        WHEN COALESCE(REV_DIGITAL,0) < 180000 THEN '150-180K'
        WHEN COALESCE(REV_DIGITAL,0) < 210000 THEN '180-210K'
        WHEN COALESCE(REV_DIGITAL,0) < 240000 THEN '210-240K'
        WHEN COALESCE(REV_DIGITAL,0) >= 240000 THEN '>240K'
        end as arpu_digital_segment,
        case
        WHEN COALESCE(REV_ROAMING,0) <= 50000 THEN '0-50K'
        WHEN COALESCE(REV_ROAMING,0) < 65000 THEN '50-65K'
        WHEN COALESCE(REV_ROAMING,0) < 70000 THEN '65-70K'
        WHEN COALESCE(REV_ROAMING,0) < 120000 THEN '70-120K'
        WHEN COALESCE(REV_ROAMING,0) < 150000 THEN '120-150K'
        WHEN COALESCE(REV_ROAMING,0) < 180000 THEN '150-180K'
        WHEN COALESCE(REV_ROAMING,0) < 210000 THEN '180-210K'
        WHEN COALESCE(REV_ROAMING,0) < 240000 THEN '210-240K'
        WHEN COALESCE(REV_ROAMING,0) >= 240000 THEN '>240K'
        end as arpu_roaming_segment,
        case
        WHEN COALESCE(REV_OTHER,0) <= 50000 THEN '0-50K'
        WHEN COALESCE(REV_OTHER,0) < 65000 THEN '50-65K'
        WHEN COALESCE(REV_OTHER,0) < 70000 THEN '65-70K'
        WHEN COALESCE(REV_OTHER,0) < 120000 THEN '70-120K'
        WHEN COALESCE(REV_OTHER,0) < 150000 THEN '120-150K'
        WHEN COALESCE(REV_OTHER,0) < 180000 THEN '150-180K'
        WHEN COALESCE(REV_OTHER,0) < 210000 THEN '180-210K'
        WHEN COALESCE(REV_OTHER,0) < 240000 THEN '210-240K'
        WHEN COALESCE(REV_OTHER,0) >= 240000 THEN '>240K'
        end as arpu_other_segment,
        a.cut_off_dt,
        ki_shard_key (msisdn)
from  ${table-prefix}${hvc-schema}.${table-prefix}hvc_rgb_rolling_30d_stg2 a 
left join ---agy_laccima b
        (select area_nme as area_name, regional_chnnl region_name, clstr_sls 
            from ${table-prefix}${laccima-schema}.${table-prefix}laccima_ref_${run-date=yyyyMMdd} 
                 group by area_nme, regional_chnnl, clstr_sls) b
        on a.cluster_name=b.clstr_sls;

delete from ${table-prefix}${hvc-schema}.${table-prefix}hvc_rgb_rolling where cut_off_dt = '${run-date=yyyy-MM-dd}';

insert into ${table-prefix}${hvc-schema}.${table-prefix}hvc_rgb_rolling 
select * from ${table-prefix}${hvc-schema}.${table-prefix}hvc_rgb_rolling_temp;

-- drop temp table 
drop table if exists ${table-prefix}${hvc-schema}.${table-prefix}hvc_rgb_rolling_temp;


