/* housekeeping source table of hvc rolling */

DROP TABLE IF EXISTS ${table-prefix}laccima_ref_${retention-date=yyyyMMdd};

DROP TABLE IF EXISTS ${table-prefix}mkios_staging_${retention-date=yyyyMMdd};
DROP TABLE IF EXISTS ${table-prefix}urp_staging_${retention-date=yyyyMMdd};

DROP TABLE IF EXISTS ${table-prefix}cb_post_staging_${retention-date=yyyyMMdd};
DROP TABLE IF EXISTS ${table-prefix}cb_pre_staging_${retention-date=yyyyMMdd};

DROP TABLE IF EXISTS ${table-prefix}chg_rcg_staging_${retention-date=yyyyMMdd};

