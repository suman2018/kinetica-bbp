/* KI_HINT_DICT_PROJECTION */
create or replace temp table ${table-prefix}${hvc-schema}.${table-prefix}hvc_traffic_rolling_daily_stg as (
select msisdn, 
sum(case when trim(l1_name) = 'Broadband' and trim(l2_name) <> 'APN/PAYU' then rev else 0 end) as rev_broadband_pkg,
sum(case when trim(l1_name) = 'Broadband' and trim(l2_name)  = 'APN/PAYU' then rev else 0 end) as rev_broadbrand_payu,
sum( payload_pkg )  as payload_pkg,
sum( payload_payu ) as payload_payu,
sum(case when trim(l1_name) = 'SMS P2P' and trim(l2_name)  = 'SMS Package' then rev else 0 end) as rev_sms_pkg,
sum(case when trim(l1_name) = 'SMS P2P' and trim(l2_name) <> 'SMS Package' then rev else 0 end) as rev_sms_payu,
sum(case when trim(l1_name) = 'SMS P2P' then (trx-trx_c) else 0 end ) as cnt_sms_pkg,
sum(case when trim(l1_name) = 'SMS P2P'   and trim(l2_name) <> 'SMS Package' then trx_c else 0 end) as cnt_sms_payu,
sum(case when trim(l1_name) = 'Voice P2P' and trim(l2_name)  = 'Voice Package' then rev else 0 end) as rev_voice_pkg,
sum(case when trim(l1_name) = 'Voice P2P' and trim(l2_name) <> 'Voice Package' then rev else 0 end) as rev_voice_payu,
sum( coalesce(dur_free,0) ) as dur_voice_pkg,
sum( coalesce(dur_c,0) )  as dur_voice_payu,
ki_shard_key (msisdn) 
from (
    select msisdn, l1_name, l2_name, rev, 
	    coalesce(vol_free,0) as payload_pkg,
	    coalesce(vol_rnd,0) - coalesce(vol_free,0) as payload_payu, 
	    trx, trx_c, dur_free, dur_c  
	from ${table-prefix}${revenue-schema}.${table-prefix}chg_rcg_staging_${run-date=yyyyMMdd}
)
group by msisdn
);


--- insertion into rolling 30d detail
delete from ${table-prefix}${hvc-schema}.${table-prefix}hvc_traffic_rolling_30d where cut_off_dt = '${run-date=yyyy-MM-dd}';

insert into ${table-prefix}${hvc-schema}.${table-prefix}hvc_traffic_rolling_30d
    select '${run-date=yyyy-MM-dd}' as cut_off_dt , a.*
    from ${table-prefix}${hvc-schema}.${table-prefix}hvc_traffic_rolling_daily_stg a;

DROP TABLE IF EXISTS ${table-prefix}${hvc-schema}.${table-prefix}hvc_traffic_rolling_daily_stg;



