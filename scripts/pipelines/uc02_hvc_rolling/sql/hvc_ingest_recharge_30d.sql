--- create temp table for current day 
 /* KI_HINT_DICT_PROJECTION */
create or replace temp table ${table-prefix}${hvc-schema}.${table-prefix}hvc_rech_rolling_daily_stg as (
select msisdn, chnnl_grp, chnnl_ctgry, chnnl_nme, sum(rchrg_trx) as rchrg_trx, sum(rchrg_amt) as rchrg_amt 
, ki_shard_key ( msisdn)  FROM (
select long(round(msisdn)) as msisdn, channel_group as chnnl_grp, channel_category as chnnl_ctgry, channel as chnnl_nme,
rech as rchrg_amt , trx_rech as rchrg_trx 
from ${table-prefix}${recharge-schema}.${table-prefix}mkios_staging_${run-date=yyyyMMdd}
UNION ALL
select long(round(msisdn)) as msisdn, channel_group as chnnl_grp, channel_category as chnnl_ctgry, channel as chnnl_nme,
rech as rchrg_amt , trx_rech as rchrg_trx 
from ${table-prefix}${recharge-schema}.${table-prefix}urp_staging_${run-date=yyyyMMdd}
) a 
group by 1, 2, 3, 4 );

--- insertion into rolling 30d detail
delete from ${table-prefix}${hvc-schema}.${table-prefix}hvc_rech_rolling_30d where cut_off_dt = '${run-date=yyyy-MM-dd}';

insert into ${table-prefix}${hvc-schema}.${table-prefix}hvc_rech_rolling_30d
    select '${run-date=yyyy-MM-dd}' as cut_off_dt , a.* 
    from ${table-prefix}${hvc-schema}.${table-prefix}hvc_rech_rolling_daily_stg a;

-- drop temp table 
drop table if exists ${table-prefix}${hvc-schema}.${table-prefix}hvc_rech_rolling_daily_stg;
