/* KI_HINT_DICT_PROJECTION */
create or replace temp table ${table-prefix}${hvc-schema}.${table-prefix}hvc_purchase_rolling_daily_stg as ( 
select msisdn, (case when UPPER(L3_NAME) = 'MKIOSK' then 'MKIOS' else UPPER(ACTIVATION_CHANNEL_ID) end) as activation_channel_id, 
    sum(trx) as tot_trx, ki_shard_key (msisdn) 
from ${table-prefix}${revenue-schema}.${table-prefix}chg_rcg_staging_${run-date=yyyyMMdd} a
where service_filter = 'VAS' and L2_name not in ('SMS Non P2P', 'Voice Non P2P')
group by 1, 2
);

--- insertion into rolling 30d detail
delete from ${table-prefix}${hvc-schema}.${table-prefix}hvc_purchase_rolling_30d where cut_off_dt = '${run-date=yyyy-MM-dd}';

insert into ${table-prefix}${hvc-schema}.${table-prefix}hvc_purchase_rolling_30d
    select '${run-date=yyyy-MM-dd}' as cut_off_dt , a.*
    from ${table-prefix}${hvc-schema}.${table-prefix}hvc_purchase_rolling_daily_stg a;

DROP TABLE IF EXISTS ${table-prefix}${hvc-schema}.${table-prefix}hvc_purchase_rolling_daily_stg;

