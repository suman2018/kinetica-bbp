create or replace table ${table-prefix}${msight_ooh-schema}.${table-prefix}ooh_summ_visit_week
(
    "months"         			varchar(8, dict)
   ,"location_code"  			varchar(32, dict)
   ,"id_dtl"         			decimal(2,1, shard_key)
   ,"id_bearing"     			integer (shard_key)
   ,"id_distance_ac" 			integer (shard_key)
   ,"id_distance_cb" 			integer (shard_key)
   ,"visittime_weekenddays"     varchar(8, dict)
   ,"total"          			integer (dict) 
)

