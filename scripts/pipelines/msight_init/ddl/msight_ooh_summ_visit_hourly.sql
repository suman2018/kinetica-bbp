create or replace table ${table-prefix}${msight_ooh-schema}.${table-prefix}ooh_summ_visit_hourly
(
    "months"         varchar(8, dict),
    "location_code"  varchar(32, dict),
    "id_dtl"         decimal(2,1),
    "id_bearing"     integer(dict), 
    "id_distance_ac" integer(dict),
    "id_distance_cb" integer(dict),
    "trxdate"        varchar(16, dict),
    "hour"           integer(shard_key, dict),
    "traffic"        integer(dict),
    "unique_traffic" integer(dict)
)


