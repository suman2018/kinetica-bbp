create or replace replicated table ${table-prefix}${msight_ooh-schema}.${table-prefix}par_ref_ooh_location
(
 "area"          varchar(32, dict)
,"location_code" varchar(16, dict)
,"location_name" varchar(16, dict)
,"type"          varchar(2, dict)
,"lat"           long (dict)
,"lon"           long (dict)
,"propinsi" varchar(16, dict)
,"min_bearing" long (dict)
,"max_bearing" long (dict)
,"dtl" long (dict)
,"distance" long (dict)
)

