-- Author: Sadar Baskoro <ssetihajid@kinetica.com>
-- Last Update: 14 Jan 2019
-- Objective: run daily pre_reg data


drop table if exists ${table-prefix}${niknokk-schema}.${table-prefix}niknokk_pre_reg_${run-date=yyyyMM};

create or replace table ${table-prefix}${niknokk-schema}.${table-prefix}niknokk_pre_reg_${run-date=yyyyMM} as 
SELECT /* KI_HINT_ALLOW_PARTIAL_PASSDOWN */
	a.month,
	a.msisdn,
        a.brand,
	a.imei,
	a.area_lacci,
	a.region_lacci, 
	a.nik_id,
	a.reg_stts,
	b.simple_wording
FROM  
(
	SELECT '${run-date=yyyyMM}' month, msisdn,brand,'' imei, area_sales as area_lacci, 
	region_lacci,nik_id, reg_stts  
	FROM ${table-prefix}${niknokk-schema}.${table-prefix}niknokk_multidim_${run-date=yyyyMM}
	WHERE date(trx_date) = '${run-date=yyyy-MM-dd}' 
	GROUP BY 2,3,5,6,7,8
)  a  LEFT JOIN 
(
 	SELECT msisdn, simple_wording 
 	FROM ${table-prefix}${niknokk-schema}.${table-prefix}niknokk_preblk_ccm_cln_baru
 	WHERE  
		simple_wording = 'Success'		
		AND date(trx_timestamp) <= '${run-date=yyyy-MM-dd}' 
 	GROUP BY 1,2
) b ON a.msisdn=b.msisdn INNER JOIN
(
	 SELECT msisdn, area, brand
    	 FROM ${table-prefix}${niknokk-schema}.${table-prefix}niknokk_sales_pre
    	 WHERE date(datex) >= concat(SUBSTRING('${run-date=yyyy-MM-dd}',1,7),'-01') 
    	 and date(datex) <= '${run-date=yyyy-MM-dd}'
    	 GROUP BY 1,2,3
) c on a.msisdn = c.msisdn  
WHERE a.brand <> 'kartuHALO';

alter table ${table-prefix}${niknokk-schema}.${table-prefix}niknokk_pre_reg_${run-date=yyyyMM} modify brand varchar(16,dict);
alter table ${table-prefix}${niknokk-schema}.${table-prefix}niknokk_pre_reg_${run-date=yyyyMM} modify area_lacci varchar(8,dict); 
alter table ${table-prefix}${niknokk-schema}.${table-prefix}niknokk_pre_reg_${run-date=yyyyMM} modify region_lacci varchar(32,dict);
alter table ${table-prefix}${niknokk-schema}.${table-prefix}niknokk_pre_reg_${run-date=yyyyMM} modify simple_wording varchar(8,dict);
