
create or replace table TEST.test_date (
    job_id          varchar(8)
    ,run_date1       varchar(16)
    ,run_date2      varchar(16)
    ,prev_month     varchar(16)
    ,prev_week      varchar(16)
    ,prev_day       varchar(16)
    ,start_date     varchar(16)
    ,retention_date     varchar(16)
    
);

insert into TEST.test_date 
values ( 
    '${job-id}',
    '${run-date=yyyy-MM-dd}', 
    '${run-date=yyyyMM}',
    '${prev-month=yyyy-MM-dd}',
    '${prev-week=yyyy-MM-dd}',
    '${prev-day=yyyy-MM-dd}',
    '${start-date=yyyy-MM-dd}',
    '${retention-date=yyyy-MM-dd}'
);
