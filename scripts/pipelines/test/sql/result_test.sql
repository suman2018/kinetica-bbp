create or replace table TEST.bar (
    current_timestamp             varchar(16)
);

create or replace table TEST.foo as (
    select current_timestamp from bar limit 1
    union ALL
    select DATE '2018-11-22' from bar limit 1
);
