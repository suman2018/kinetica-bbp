delete from ${table-prefix}cms_campaign_tracking_detail
where file_date < date('2019-02-01')
;

/* KI_HINT_DICT_PROJECTION */
create or replace table ${table-prefix}cms_campaign_tracking_detail_tmp_1
as
select 
category
,campaign_type
,msisdn
,char64(substr(campaign_id,1,15)||'_'||char64(abs(hash(campaign_id)))) as campaign_id
,campaign_objective_id
,campaign_name
,start_period - interval 7 days as start_period
,end_period - interval 7 days as end_period
,iscontrolgroup
,campaign_group_id
,segment
,los
,arpu
,business_group
,initiated_group
,region_hlr
,cluster
,branch
,date(trx_date - interval 7 days) as trx_date 
,content_1
,content_2
,content_3
,metric_1
,metric_2
,metric_3
,is_btl
,target
,eligible
,taker
,date(file_date - interval 7 days) as file_date
,orch_job_id
,pipeline_name
,pipeline_step
from 
(
select *, rand() as rnd
from ${table-prefix}cms_campaign_tracking_detail
where file_date = date('2019-02-01')
)
order by rnd
limit 1000000
;

/* KI_HINT_DICT_PROJECTION */
create or replace table ${table-prefix}cms_campaign_tracking_detail_tmp_2
as
select 
category
,campaign_type
,msisdn
,char64(substr(campaign_id,1,15)||'_'||char64(abs(hash(campaign_id)))) as campaign_id
,campaign_objective_id
,campaign_name
,start_period - interval 7 days as start_period
,end_period - interval 7 days as end_period
,iscontrolgroup
,campaign_group_id
,segment
,los
,arpu
,business_group
,initiated_group
,region_hlr
,cluster
,branch
,date(trx_date - interval 7 days) as trx_date 
,content_1
,content_2
,content_3
,metric_1
,metric_2
,metric_3
,is_btl
,target
,eligible
,taker
,date(file_date - interval 7 days) as file_date
,orch_job_id
,pipeline_name
,pipeline_step
from 
(
select *, rand() as rnd
from ${table-prefix}cms_campaign_tracking_detail
where file_date = date('2019-02-02')
)
order by rnd
limit 1000000
;

/* KI_HINT_DICT_PROJECTION */
create or replace table ${table-prefix}cms_campaign_tracking_detail_tmp_3
as
select 
category
,campaign_type
,msisdn
,char64(substr(campaign_id,1,15)||'_'||char64(abs(hash(campaign_id)))) as campaign_id
,campaign_objective_id
,campaign_name
,start_period - interval 7 days as start_period
,end_period - interval 7 days as end_period
,iscontrolgroup
,campaign_group_id
,segment
,los
,arpu
,business_group
,initiated_group
,region_hlr
,cluster
,branch
,date(trx_date - interval 7 days) as trx_date 
,content_1
,content_2
,content_3
,metric_1
,metric_2
,metric_3
,is_btl
,target
,eligible
,taker
,date(file_date - interval 7 days) as file_date
,orch_job_id
,pipeline_name
,pipeline_step
from 
(
select *, rand() as rnd
from ${table-prefix}cms_campaign_tracking_detail
where file_date = date('2019-02-03')
)
order by rnd
limit 1000000
;

/* KI_HINT_DICT_PROJECTION */
create or replace table ${table-prefix}cms_campaign_tracking_detail_tmp_4
as
select 
category
,campaign_type
,msisdn
,char64(substr(campaign_id,1,15)||'_'||char64(abs(hash(campaign_id)))) as campaign_id
,campaign_objective_id
,campaign_name
,start_period - interval 7 days as start_period
,end_period - interval 7 days as end_period
,iscontrolgroup
,campaign_group_id
,segment
,los
,arpu
,business_group
,initiated_group
,region_hlr
,cluster
,branch
,date(trx_date - interval 7 days) as trx_date 
,content_1
,content_2
,content_3
,metric_1
,metric_2
,metric_3
,is_btl
,target
,eligible
,taker
,date(file_date - interval 7 days) as file_date
,orch_job_id
,pipeline_name
,pipeline_step
from 
(
select *, rand() as rnd
from ${table-prefix}cms_campaign_tracking_detail
where file_date = date('2019-02-04')
)
order by rnd
limit 1000000
;

/* KI_HINT_DICT_PROJECTION */
create or replace table ${table-prefix}cms_campaign_tracking_detail_tmp_5
as
select 
category
,campaign_type
,msisdn
,char64(substr(campaign_id,1,15)||'_'||char64(abs(hash(campaign_id)))) as campaign_id
,campaign_objective_id
,campaign_name
,start_period - interval 7 days as start_period
,end_period - interval 7 days as end_period
,iscontrolgroup
,campaign_group_id
,segment
,los
,arpu
,business_group
,initiated_group
,region_hlr
,cluster
,branch
,date(trx_date - interval 7 days) as trx_date 
,content_1
,content_2
,content_3
,metric_1
,metric_2
,metric_3
,is_btl
,target
,eligible
,taker
,date(file_date - interval 7 days) as file_date
,orch_job_id
,pipeline_name
,pipeline_step
from 
(
select *, rand() as rnd
from ${table-prefix}cms_campaign_tracking_detail
where file_date = date('2019-02-05')
)
order by rnd
limit 1000000
;

/* KI_HINT_DICT_PROJECTION */
create or replace table ${table-prefix}cms_campaign_tracking_detail_tmp_6
as
select 
category
,campaign_type
,msisdn
,char64(substr(campaign_id,1,15)||'_'||char64(abs(hash(campaign_id)))) as campaign_id
,campaign_objective_id
,campaign_name
,start_period - interval 7 days as start_period
,end_period - interval 7 days as end_period
,iscontrolgroup
,campaign_group_id
,segment
,los
,arpu
,business_group
,initiated_group
,region_hlr
,cluster
,branch
,date(trx_date - interval 7 days) as trx_date 
,content_1
,content_2
,content_3
,metric_1
,metric_2
,metric_3
,is_btl
,target
,eligible
,taker
,date(file_date - interval 7 days) as file_date
,orch_job_id
,pipeline_name
,pipeline_step
from 
(
select *, rand() as rnd
from ${table-prefix}cms_campaign_tracking_detail
where file_date = date('2019-02-06')
)
order by rnd
limit 1000000
;

/* KI_HINT_DICT_PROJECTION */
create or replace table ${table-prefix}cms_campaign_tracking_detail_tmp_7
as
select 
category
,campaign_type
,msisdn
,char64(substr(campaign_id,1,15)||'_'||char64(abs(hash(campaign_id)))) as campaign_id
,campaign_objective_id
,campaign_name
,start_period - interval 7 days as start_period
,end_period - interval 7 days as end_period
,iscontrolgroup
,campaign_group_id
,segment
,los
,arpu
,business_group
,initiated_group
,region_hlr
,cluster
,branch
,date(trx_date - interval 7 days) as trx_date 
,content_1
,content_2
,content_3
,metric_1
,metric_2
,metric_3
,is_btl
,target
,eligible
,taker
,date(file_date - interval 7 days) as file_date
,orch_job_id
,pipeline_name
,pipeline_step
from 
(
select *, rand() as rnd
from ${table-prefix}cms_campaign_tracking_detail
where file_date = date('2019-02-07')
)
order by rnd
limit 1000000
;

/* KI_HINT_DICT_PROJECTION */
create or replace table ${table-prefix}cms_campaign_tracking_detail_tmp
as
select * from ${table-prefix}cms_campaign_tracking_detail_tmp_1
union all
select * from ${table-prefix}cms_campaign_tracking_detail_tmp_2
union all
select * from ${table-prefix}cms_campaign_tracking_detail_tmp_3
union all
select * from ${table-prefix}cms_campaign_tracking_detail_tmp_4
union all
select * from ${table-prefix}cms_campaign_tracking_detail_tmp_5
union all
select * from ${table-prefix}cms_campaign_tracking_detail_tmp_6
union all
select * from ${table-prefix}cms_campaign_tracking_detail_tmp_7
;

alter table ${table-prefix}cms_campaign_tracking_detail_tmp modify trx_date date(dict) NOT NULL;
alter table ${table-prefix}cms_campaign_tracking_detail_tmp modify file_date date(dict) NOT NULL;

insert into ${table-prefix}cms_campaign_tracking_detail
select * from ${table-prefix}cms_campaign_tracking_detail_tmp;

/* KI_HINT_DICT_PROJECTION */
create or replace table ${table-prefix}cms_campaign_tracking_detail_tmp
as
select
category
,campaign_type
,msisdn
,char64(substr(campaign_id,1,15)||'_'||char64(abs(hash(campaign_id, 1)))) as campaign_id
,campaign_objective_id
,campaign_name
,start_period - interval 7 days as start_period
,end_period - interval 7 days as end_period
,iscontrolgroup
,campaign_group_id
,segment
,los
,arpu
,business_group
,initiated_group
,region_hlr
,cluster
,branch
,date(trx_date - interval 7 days) as trx_date 
,content_1
,content_2
,content_3
,metric_1
,metric_2
,metric_3
,is_btl
,target
,eligible
,taker
,date(file_date - interval 7 days) as file_date
,orch_job_id
,pipeline_name
,pipeline_step
from ${table-prefix}cms_campaign_tracking_detail
where file_date < date('2019-02-01')
;

alter table ${table-prefix}cms_campaign_tracking_detail_tmp modify trx_date date(dict) NOT NULL;
alter table ${table-prefix}cms_campaign_tracking_detail_tmp modify file_date date(dict) NOT NULL;

insert into ${table-prefix}cms_campaign_tracking_detail
select * from ${table-prefix}cms_campaign_tracking_detail_tmp;

/* KI_HINT_DICT_PROJECTION */
create or replace table ${table-prefix}cms_campaign_tracking_detail_tmp
as
select
category
,campaign_type
,msisdn
,char64(substr(campaign_id,1,15)||'_'||char64(abs(hash(campaign_id, 2)))) as campaign_id
,campaign_objective_id
,campaign_name
,start_period - interval 14 days as start_period
,end_period - interval 14 days as end_period
,iscontrolgroup
,campaign_group_id
,segment
,los
,arpu
,business_group
,initiated_group
,region_hlr
,cluster
,branch
,date(trx_date - interval 14 days) as trx_date 
,content_1
,content_2
,content_3
,metric_1
,metric_2
,metric_3
,is_btl
,target
,eligible
,taker
,date(file_date - interval 14 days) as file_date
,orch_job_id
,pipeline_name
,pipeline_step
from ${table-prefix}cms_campaign_tracking_detail
where file_date < date('2019-02-01')
;

alter table ${table-prefix}cms_campaign_tracking_detail_tmp modify trx_date date(dict) NOT NULL;
alter table ${table-prefix}cms_campaign_tracking_detail_tmp modify file_date date(dict) NOT NULL;

insert into ${table-prefix}cms_campaign_tracking_detail
select * from ${table-prefix}cms_campaign_tracking_detail_tmp;

/* KI_HINT_DICT_PROJECTION */
create or replace table ${table-prefix}cms_campaign_tracking_detail_tmp
as
select
category
,campaign_type
,msisdn
,char64(substr(campaign_id,1,15)||'_'||char64(abs(hash(campaign_id, 3)))) as campaign_id
,campaign_objective_id
,campaign_name
,start_period - interval 28 days as start_period
,end_period - interval 28 days as end_period
,iscontrolgroup
,campaign_group_id
,segment
,los
,arpu
,business_group
,initiated_group
,region_hlr
,cluster
,branch
,date(trx_date - interval 28 days) as trx_date 
,content_1
,content_2
,content_3
,metric_1
,metric_2
,metric_3
,is_btl
,target
,eligible
,taker
,date(file_date - interval 28 days) as file_date
,orch_job_id
,pipeline_name
,pipeline_step
from ${table-prefix}cms_campaign_tracking_detail
where file_date < date('2019-02-01')
;

alter table ${table-prefix}cms_campaign_tracking_detail_tmp modify trx_date date(dict) NOT NULL;
alter table ${table-prefix}cms_campaign_tracking_detail_tmp modify file_date date(dict) NOT NULL;

insert into ${table-prefix}cms_campaign_tracking_detail
select * from ${table-prefix}cms_campaign_tracking_detail_tmp;

/* KI_HINT_DICT_PROJECTION */
create or replace table ${table-prefix}cms_campaign_tracking_detail_tmp
as
select
category
,campaign_type
,msisdn
,char64(substr(campaign_id,1,15)||'_'||char64(abs(hash(campaign_id, 4)))) as campaign_id
,campaign_objective_id
,campaign_name
,start_period - interval 56 days as start_period
,end_period - interval 56 days as end_period
,iscontrolgroup
,campaign_group_id
,segment
,los
,arpu
,business_group
,initiated_group
,region_hlr
,cluster
,branch
,date(trx_date - interval 56 days) as trx_date 
,content_1
,content_2
,content_3
,metric_1
,metric_2
,metric_3
,is_btl
,target
,eligible
,taker
,date(file_date - interval 56 days) as file_date
,orch_job_id
,pipeline_name
,pipeline_step
from ${table-prefix}cms_campaign_tracking_detail
where file_date < date('2019-02-01')
;

alter table ${table-prefix}cms_campaign_tracking_detail_tmp modify trx_date date(dict) NOT NULL;
alter table ${table-prefix}cms_campaign_tracking_detail_tmp modify file_date date(dict) NOT NULL;

insert into ${table-prefix}cms_campaign_tracking_detail
select * from ${table-prefix}cms_campaign_tracking_detail_tmp;

drop table if exists ${table-prefix}cms_campaign_tracking_detail_tmp;