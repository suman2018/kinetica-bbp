-- Author: Sadar Baskoro <ssetihajid@kinetica.com>
-- Last Update: 23 Dec 2018
-- Objective: update churn reg table


create or replace table ${table-prefix}${niknokk-schema}.${table-prefix}niknokk_churn_reg_${run-date=yyyyMM}_tmp as 
	SELECT
        	msisdn,
        	brand,
        	nik_id,
		KI_SHARD_KEY(nik_id)
    	FROM ${table-prefix}${niknokk-schema}.${table-prefix}niknokk_pre_reg_${prev-month=yyyyMM}
    	WHERE simple_wording='Success'
            AND nik_id is not null
	group by 1,2,3;
            
create or replace table ${table-prefix}${niknokk-schema}.${table-prefix}niknokk_churn_reg_${run-date=yyyyMM}_tmp2 as
                SELECT nik_id,KI_SHARD_KEY(nik_id)
                FROM ${table-prefix}${niknokk-schema}.${table-prefix}niknokk_pre_reg_${run-date=yyyyMM}
                WHERE simple_wording='Success'
                    AND nik_id is not null
                GROUP BY 1;

create or replace table ${table-prefix}${niknokk-schema}.${table-prefix}niknokk_churn_reg_${run-date=yyyyMM}_tmp3 as
select nik_id from ${table-prefix}${niknokk-schema}.${table-prefix}niknokk_churn_reg_${run-date=yyyyMM}_tmp
except 
select nik_id from ${table-prefix}${niknokk-schema}.${table-prefix}niknokk_churn_reg_${run-date=yyyyMM}_tmp2;


create or replace table ${table-prefix}${niknokk-schema}.${table-prefix}niknokk_churn_reg_${run-date=yyyyMM}_tmp4 as            
select  a.*
from ${table-prefix}${niknokk-schema}.${table-prefix}niknokk_churn_reg_${run-date=yyyyMM}_tmp a inner join 
${table-prefix}${niknokk-schema}.${table-prefix}niknokk_churn_reg_${run-date=yyyyMM}_tmp3 b 
on a.nik_id = b.nik_id;  


drop table if exists ${table-prefix}${niknokk-schema}.${table-prefix}niknokk_churn_reg_${run-date=yyyyMM};

create or replace table ${table-prefix}${niknokk-schema}.${table-prefix}niknokk_churn_reg_${run-date=yyyyMM} as
SELECT
    '${run-date=yyyyMM}' mth,
    a.msisdn,
    a.brand,
    a.nik_id as nik
FROM ${table-prefix}${niknokk-schema}.${table-prefix}niknokk_churn_reg_${run-date=yyyyMM}_tmp4 a;

drop table if exists ${table-prefix}${niknokk-schema}.${table-prefix}niknokk_churn_reg_${run-date=yyyyMM}_tmp;
drop table if exists ${table-prefix}${niknokk-schema}.${table-prefix}niknokk_churn_reg_${run-date=yyyyMM}_tmp2;
drop table if exists ${table-prefix}${niknokk-schema}.${table-prefix}niknokk_churn_reg_${run-date=yyyyMM}_tmp3;
drop table if exists ${table-prefix}${niknokk-schema}.${table-prefix}niknokk_churn_reg_${run-date=yyyyMM}_tmp4;
  
