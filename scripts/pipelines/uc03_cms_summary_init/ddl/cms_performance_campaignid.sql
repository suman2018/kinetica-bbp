

create or replace table ${table-prefix}${cms-schema}.${table-prefix}cms_performance_campaignid(
   periode 					date(dict) not null       
  ,eom 						integer(dict) not null    
  ,category 				varchar(16, dict)         
  ,campaign_type 			varchar(32, dict)         
  ,campaign_id 				varchar(64, shard_key, dict) not null
  ,campaign_objective_id   	integer(dict)             
  ,campaign_objective_desc 	varchar(64, dict)         
  ,campaign_name           	varchar(128, dict)        
  ,start_period            	date(dict)               
  ,end_period              	date(dict)                
  ,campaign_group_id       	integer(dict)             
  ,business_group          	varchar(8, dict)          
  ,initiated_business      	varchar(32, dict)         
  ,area                    	varchar(8, dict)          
  ,is_btl                  	integer(dict)             
  ,brand                   	varchar(16, dict)         
  ,campaign_status         	varchar(16, dict) not null
  ,target                  bigint     
  ,eligible                bigint     
  ,taker                   bigint              
  ,unique_taker            bigint     
  ,revenue_taker           bigint              
  ,control                 bigint     
  ,taker_control           bigint              
  ,unique_taker_control    bigint     
  ,revenue_control         bigint              
  ,arpu_target_before      bigint              
  ,arpu_target_after       bigint              
  ,arpu_control_before     bigint              
  ,arpu_control_after      bigint              
  ,last_trx_date            date(dict)               
) ;
