
create or replace table ${table-prefix}${cms-schema}.${table-prefix}cms_performance_campaign(
  periode 					date(shard_key, dict) not null       
  ,eom 						integer(shard_key, dict) not null   
  ,theme           varchar(64, shard_key, dict)     
  ,category        varchar(16, shard_key, dict)     
  ,campaign_name     varchar(128, shard_key, dict)    
  ,campaign_group_id integer(shard_key, dict)
  ,start_period   date(dict)
  ,end_period   date(dict)
  ,tot_campaign      bigint 
  ,target            bigint          
  ,eligible          bigint          
  ,taker             bigint          
  ,unique_target     bigint          
  ,unique_taker      bigint          
  ,revenue           bigint          
  ,recharge          bigint          
  ,control           bigint          
  ,taker_control     bigint          
  ,revenue_control   bigint          
  ,uplift            decimal(38,4) 
  ,last_trx_date   date(dict)
) ;
