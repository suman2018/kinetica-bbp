

create or replace table ${table-prefix}${cms-schema}.${table-prefix}cms_performance_summary(
  periode 					date(shard_key, dict) not null       
  ,eom 						integer(shard_key, dict) not null   
  ,brand                   	varchar(16, shard_key, dict)        
  ,campaign_category 		varchar(8, shard_key, dict) not null
  ,tot_campaign      bigint     
  ,target            bigint             
  ,eligible          bigint             
  ,taker             bigint             
  ,unique_target     bigint             
  ,unique_taker      bigint             
  ,revenue           bigint             
  ,recharge          bigint             
  ,control           bigint             
  ,taker_control     bigint             
  ,revenue_control   bigint             
  ,uplift            decimal(38,4)    
  ,last_trx_date   date(dict)
) ;