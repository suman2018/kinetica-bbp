
create or replace table ${table-prefix}${cms-schema}.${table-prefix}cms_summary_bycampaignid_byregionhlr(
   periode 					date(dict) not null       
  ,eom 						integer(dict) not null    
  ,category 				varchar(16, dict)         
  ,campaign_type 			varchar(32, dict)         
  ,campaign_id 				varchar(64, shard_key, dict) not null
  ,campaign_objective_id   	integer(dict)             
  ,campaign_objective_desc 	varchar(64, dict)         
  ,campaign_name           	varchar(128, dict)        
  ,start_period            	date(dict)               
  ,end_period              	date(dict)                
  ,campaign_group_id       	integer(dict)             
  ,business_group          	varchar(8, dict)          
  ,initiated_business      	varchar(32, dict)         
  ,area                    	varchar(8, dict)          
  ,is_btl                  	integer(dict)             
  ,brand                   	varchar(16, dict)
  ,region_hlr               varchar(32, dict)         
  ,campaign_status         	varchar(16, dict) not null
  ,target                  int      
  ,eligible                int     
  ,taker                   int              
  ,unique_taker            int     
  ,revenue_taker           bigint              
  ,control                 int     
  ,taker_control           int              
  ,unique_taker_control    int     
  ,revenue_control         bigint              
  ,arpu_target_before      int              
  ,arpu_target_after       int              
  ,arpu_control_before     int              
  ,arpu_control_after      int              
  ,last_trx_date            date(dict)                
) ;


