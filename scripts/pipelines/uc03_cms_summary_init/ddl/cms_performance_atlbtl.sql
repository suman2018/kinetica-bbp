
create or replace table ${table-prefix}${cms-schema}.${table-prefix}cms_performance_atlbtl(
  periode 					date(shard_key, dict) not null       
  ,eom 						integer(shard_key, dict) not null   
  ,brand                   	varchar(16, shard_key, dict)        
 ,theme           varchar(64, shard_key, dict)     
  ,category        varchar(16, shard_key, dict) 
  ,area            varchar(8, shard_key, dict)      
  ,is_btl          integer(shard_key, dict)         
  ,tot_campaign    bigint 
  ,target          bigint          
  ,eligible        bigint          
  ,taker           bigint
  ,taker_usage	   bigint
  ,taker_recharge  bigint
  ,revenue         bigint          
  ,recharge        bigint          
  ,control         bigint          
  ,taker_control   bigint          
  ,revenue_control bigint          
  ,uplift          decimal(38,4)  
  ,last_trx_date   date(dict)
) ;