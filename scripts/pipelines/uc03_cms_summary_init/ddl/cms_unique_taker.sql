
create or replace table ${table-prefix}${cms-schema}.${table-prefix}cms_unique_taker (
	periode 					date(dict) not null
	,category 					varchar(16, dict)
	,campaign_type 				varchar(32, dict)
	,campaign_id 				varchar(64, dict) not null
	,campaign_objective_id   	integer(dict)
	,campaign_objective_desc 	varchar(64, dict) 
	,campaign_name           	varchar(128, dict) 
	,start_period            	timestamp               
  	,end_period              	timestamp
	,campaign_group_id       	integer(dict)
	,business_group          	varchar(8, dict)
	,initiated_group      		varchar(32, dict)
	,area                    	varchar(8, dict)
	,is_btl                  	integer(dict)
	,brand                   	varchar(16, dict)
	,msisdn 					bigint (shard_key) not null
	,taker 						int
	,taker_control 				int
	,eligible 					int
	,control 					int
);
