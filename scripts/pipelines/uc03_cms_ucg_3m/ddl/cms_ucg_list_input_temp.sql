create or replace temp table ${table-prefix}${cms_stg-schema}.${table-prefix}cms_ucg_list_input_temp
(
    "msisdn" bigint(shard_key),
    "description" date(dict)
)