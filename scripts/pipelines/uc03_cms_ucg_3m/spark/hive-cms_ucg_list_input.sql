select
	cast(msisdn as bigint),
	string(last_day(date(from_unixtime(unix_timestamp(substring(description,9,6), 'yyyyMM'))))) as description
from gpu.gpu_mck_ucg_list 
where last_day(from_unixtime(unix_timestamp(substring(description,9,6), 'yyyyMM')))<'${run-date=yyyy-MM-dd}'