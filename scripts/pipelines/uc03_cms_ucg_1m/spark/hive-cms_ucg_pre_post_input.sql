select
    string(last_day(date(from_unixtime(unix_timestamp(mth, 'yyyyMM'))))) as rep_month,
	cast(msisdn as bigint),
    brand,
    area_lacci,
    region_lacci,
    region_hlr,
    city_hlr,
    cast(ltrim(los) as int) as los,
    status,
    device_type,
    rev as total_revenue,
    rev_broadband
from gpu.cb_pre_post_uc2 where last_day(from_unixtime(unix_timestamp(mth, 'yyyyMM')))=last_day('${run-date=yyyy-MM-dd}')