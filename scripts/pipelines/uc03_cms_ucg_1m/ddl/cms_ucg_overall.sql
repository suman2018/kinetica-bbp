create or replace table ${table-prefix}${cms-schema}.${table-prefix}ucg_overall
(
	"rep_month" date,
	"total_subs_pop" bigint,
    "total_subs_ucg" bigint,
    "total_revenue_pop" double,
    "total_revenue_ucg" double,
    "arpu_pop_exc" INTEGER,
    "arpu_ucg_exc" INTEGER,
    "arpu_pop_inc" INTEGER,
    "arpu_ucg_inc" INTEGER,
    "mom_arpu_pop_exc" INTEGER,
    "mom_arpu_ucg_exc" INTEGER,
    "mom_gap_exc" double,
    "mom_uplift_exc" double,
    "mom_arpu_pop_inc" INTEGER,
    "mom_arpu_ucg_inc" INTEGER,
    "mom_gap_inc" double,
    "mom_uplift_inc" double,
    "file_date" DATE(dict) NOT NULL
)