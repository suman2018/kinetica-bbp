insert into ${table-prefix}${cms_stg-schema}.${table-prefix}cms_ucg_list_input
(
	msisdn
	,description
)
select
	msisdn
	,description
from ${table-prefix}${cms_stg-schema}.${table-prefix}cms_ucg_list_input_temp;

drop table ${table-prefix}${cms_stg-schema}.${table-prefix}cms_ucg_list_input_temp;