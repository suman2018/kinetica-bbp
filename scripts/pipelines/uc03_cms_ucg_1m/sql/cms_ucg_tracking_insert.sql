delete from ${table-prefix}${cms-schema}.${table-prefix}ucg_tracking
where file_date = date('${run-date=yyyy-MM-dd}');

/* KI_HINT_DICT_PROJECTION */ 
create or replace temp table ${table-prefix}${cms-schema}.${table-prefix}${pipeline-name}_tmp
as
select
    last_day(prepost.rep_month)
    ,prepost.msisdn
    ,prepostbase.brand
    ,case when lower(prepostbase.region_lacci) in ('01.sumbagut','02.sumbagteng','03.sumbagsel') then 'AREA 1'
    	  when lower(prepostbase.region_lacci) in ('04.western jabotabek','05.central jabotabek','06.eastern jabotabek','07.jabar') then 'AREA 2'
    	  when lower(prepostbase.region_lacci) in ('08.jateng','09.Jatim','10.balinusra') then 'AREA 3'
    	  when lower(prepostbase.region_lacci) in ('11.kalimantan','12.sulawesi','13.puma') then 'AREA 4'
    	  when lower(prepostbase.region_hlr) like '%sumbagut%' then 'AREA 1'
		  when lower(prepostbase.region_hlr) like '%sumbagteng%' then 'AREA 1'
		  when lower(prepostbase.region_hlr) like '%sumbagsel%' then 'AREA 1'
		  when lower(prepostbase.region_hlr) like '%jabotabek%' then 'AREA 2'
		  when lower(prepostbase.region_hlr) like '%jabar%' then 'AREA 2'
		  when lower(prepostbase.region_hlr) like '%jateng%' then 'AREA 3'
		  when lower(prepostbase.region_hlr) like '%jatim%' then 'AREA 3'
		  when lower(prepostbase.region_hlr) like '%balinusra%' then 'AREA 3'
		  when lower(prepostbase.region_hlr) like '%kalimantan%' then 'AREA 4'
		  when lower(prepostbase.region_hlr) like '%sulawesi%' then 'AREA 4'
		  when lower(prepostbase.region_hlr) like '%puma%' then 'AREA 4'
		  when lower(prepostbase.region_hlr) like '%papua%' then 'AREA 4'
    	  else prepostbase.area_lacci
   	end as area_lacci
    ,case when prepostbase.region_lacci = '' then 
		case
			when lower(prepostbase.region_hlr) like '%sumbagut%' then '01.Sumbagut'
			when lower(prepostbase.region_hlr) like '%sumbagteng%' then '02.Sumbagteng'
			when lower(prepostbase.region_hlr) like '%sumbagsel%' then '03.Sumbagsel'
			when lower(prepostbase.region_hlr) like '%western jabotabek%' then '04.Western Jabotabek'
			when lower(prepostbase.region_hlr) like '%central jabotabek%' then '05.Central Jabotabek'
			when lower(prepostbase.region_hlr) like '%eastern jabotabek%' then '06.Eastern Jabotabek'
			when lower(prepostbase.region_hlr) like '%jabotabek%' and lower(prepostbase.city_hlr) like '%serang%' then '04.Western Jabotabek'
			when lower(prepostbase.region_hlr) like '%jabotabek%' and lower(prepostbase.city_hlr) like '%bogor%' then '06.Eastern Jabotabek'
			when lower(prepostbase.region_hlr) like '%jabotabek%' and lower(prepostbase.city_hlr) like '%jakarta%' then '05.Central Jabotabek'
			when lower(prepostbase.region_hlr) like '%jabotabek%' and lower(prepostbase.city_hlr) like '%karawang%' then '06.Eastern Jabotabek'
			when lower(prepostbase.region_hlr) like '%jabar%' then '07.Jabar'
			when lower(prepostbase.region_hlr) like '%jateng%' then '08.Jateng'
			when lower(prepostbase.region_hlr) like '%jatim%' then '09.Jatim'
			when lower(prepostbase.region_hlr) like '%balinusra%' then '10.Balinusra'
			when lower(prepostbase.region_hlr) like '%kalimantan%' then '11.Kalimantan'
			when lower(prepostbase.region_hlr) like '%sulawesi%' then '12.Sulawesi'
			when lower(prepostbase.region_hlr) like '%puma%' then '13.Puma'
			when lower(prepostbase.region_hlr) like '%papua%' then '13.Puma'
		else prepostbase.region_hlr end 
	else prepostbase.region_lacci end as region_lacci
    ,prepostbase.region_hlr
    ,prepostbase.city_hlr
    ,prepostbase.los
    ,prepostbase.device_type
    ,prepostbase.total_revenue as total_revenue_base
    ,prepostbase.rev_broadband
    ,prepost.total_revenue
    ,1 as is_ucg
	,date('${run-date=yyyy-MM-dd}') as file_date
from ${table-prefix}${cms_stg-schema}.${table-prefix}cms_pre_post_input prepost
inner join ${table-prefix}${cms_stg-schema}.${table-prefix}cms_pre_post_input prepostbase
	on prepost.msisdn = prepostbase.msisdn
	and prepostbase.rep_month = (select min(rep_month) from ${table-prefix}${cms_stg-schema}.${table-prefix}cms_pre_post_input where rep_month < (select max(last_day(description)) from ${table-prefix}${cms_stg-schema}.${table-prefix}cms_ucg_list_input where last_day(description) < '${run-date=yyyy-MM-dd}'))
inner join ${table-prefix}${cms_stg-schema}.${table-prefix}cms_ucg_list_input ucg
	on prepost.msisdn = ucg.msisdn
	and ucg.description = (select max(last_day(description)) from ${table-prefix}${cms_stg-schema}.${table-prefix}cms_ucg_list_input where last_day(description) < '${run-date=yyyy-MM-dd}')
where prepost.rep_month = (select max(rep_month) from ${table-prefix}${cms_stg-schema}.${table-prefix}cms_pre_post_input where rep_month = last_day('${run-date=yyyy-MM-dd}'))
union all
select
    last_day(prepost.rep_month)
    ,prepost.msisdn
    ,prepostbase.brand
    ,case when lower(prepostbase.region_lacci) in ('01.sumbagut','02.sumbagteng','03.sumbagsel') then 'AREA 1'
    	  when lower(prepostbase.region_lacci) in ('04.western jabotabek','05.central jabotabek','06.eastern jabotabek','07.jabar') then 'AREA 2'
    	  when lower(prepostbase.region_lacci) in ('08.jateng','09.Jatim','10.balinusra') then 'AREA 3'
    	  when lower(prepostbase.region_lacci) in ('11.kalimantan','12.sulawesi','13.puma') then 'AREA 4'
    	  when lower(prepostbase.region_hlr) like '%sumbagut%' then 'AREA 1'
		  when lower(prepostbase.region_hlr) like '%sumbagteng%' then 'AREA 1'
		  when lower(prepostbase.region_hlr) like '%sumbagsel%' then 'AREA 1'
		  when lower(prepostbase.region_hlr) like '%jabotabek%' then 'AREA 2'
		  when lower(prepostbase.region_hlr) like '%jabar%' then 'AREA 2'
		  when lower(prepostbase.region_hlr) like '%jateng%' then 'AREA 3'
		  when lower(prepostbase.region_hlr) like '%jatim%' then 'AREA 3'
		  when lower(prepostbase.region_hlr) like '%balinusra%' then 'AREA 3'
		  when lower(prepostbase.region_hlr) like '%kalimantan%' then 'AREA 4'
		  when lower(prepostbase.region_hlr) like '%sulawesi%' then 'AREA 4'
		  when lower(prepostbase.region_hlr) like '%puma%' then 'AREA 4'
		  when lower(prepostbase.region_hlr) like '%papua%' then 'AREA 4'
    	  else prepostbase.area_lacci
   	end as area_lacci
    ,case when prepostbase.region_lacci = '' then 
		case
			when lower(prepostbase.region_hlr) like '%sumbagut%' then '01.Sumbagut'
			when lower(prepostbase.region_hlr) like '%sumbagteng%' then '02.Sumbagteng'
			when lower(prepostbase.region_hlr) like '%sumbagsel%' then '03.Sumbagsel'
			when lower(prepostbase.region_hlr) like '%western jabotabek%' then '04.Western Jabotabek'
			when lower(prepostbase.region_hlr) like '%central jabotabek%' then '05.Central Jabotabek'
			when lower(prepostbase.region_hlr) like '%eastern jabotabek%' then '06.Eastern Jabotabek'
			when lower(prepostbase.region_hlr) like '%jabotabek%' and lower(prepostbase.city_hlr) like '%serang%' then '04.Western Jabotabek'
			when lower(prepostbase.region_hlr) like '%jabotabek%' and lower(prepostbase.city_hlr) like '%bogor%' then '06.Eastern Jabotabek'
			when lower(prepostbase.region_hlr) like '%jabotabek%' and lower(prepostbase.city_hlr) like '%jakarta%' then '05.Central Jabotabek'
			when lower(prepostbase.region_hlr) like '%jabotabek%' and lower(prepostbase.city_hlr) like '%karawang%' then '06.Eastern Jabotabek'
			when lower(prepostbase.region_hlr) like '%jabar%' then '07.Jabar'
			when lower(prepostbase.region_hlr) like '%jateng%' then '08.Jateng'
			when lower(prepostbase.region_hlr) like '%jatim%' then '09.Jatim'
			when lower(prepostbase.region_hlr) like '%balinusra%' then '10.Balinusra'
			when lower(prepostbase.region_hlr) like '%kalimantan%' then '11.Kalimantan'
			when lower(prepostbase.region_hlr) like '%sulawesi%' then '12.Sulawesi'
			when lower(prepostbase.region_hlr) like '%puma%' then '13.Puma'
			when lower(prepostbase.region_hlr) like '%papua%' then '13.Puma'
		else prepostbase.region_hlr end 
	else prepostbase.region_lacci end as region_lacci
    ,prepostbase.region_hlr
    ,prepostbase.city_hlr
    ,prepostbase.los
    ,prepostbase.device_type
    ,prepostbase.total_revenue as total_revenue_base
    ,prepostbase.rev_broadband
    ,prepost.total_revenue
    ,0 as is_ucg
	,date('${run-date=yyyy-MM-dd}') as file_date
from
(
	select
		*
	from ${table-prefix}${cms_stg-schema}.${table-prefix}cms_pre_post_input
	where rep_month = (select min(rep_month) from ${table-prefix}${cms_stg-schema}.${table-prefix}cms_pre_post_input where rep_month < (select max(last_day(description)) from ${table-prefix}${cms_stg-schema}.${table-prefix}cms_ucg_list_input where last_day(description) < '${run-date=yyyy-MM-dd}'))
		and lower(status) = 'active'
) prepostbase
inner join
(
	select 
		rep_month, msisdn, total_revenue
	from ${table-prefix}${cms_stg-schema}.${table-prefix}cms_pre_post_input
	where rep_month = (select max(rep_month) from ${table-prefix}${cms_stg-schema}.${table-prefix}cms_pre_post_input where rep_month = last_day('${run-date=yyyy-MM-dd}'))
) prepost
	on prepostbase.msisdn = prepost.msisdn
left join
(
	select
		msisdn
	from ${table-prefix}${cms_stg-schema}.${table-prefix}cms_ucg_list_input
	where description = (select max(last_day(description)) from ${table-prefix}${cms_stg-schema}.${table-prefix}cms_ucg_list_input where last_day(description) < '${run-date=yyyy-MM-dd}')
) ucg
	on prepost.msisdn = ucg.msisdn
left join
(
	select
		msisdn
	from ${table-prefix}${cms_stg-schema}.${table-prefix}cms_ara_blacklist_input
	where lower(reason) in ('blacklist_global_api', 'vvip', 'blacklist', 'global', 'complainthandling')
) bl
	on prepost.msisdn = bl.msisdn
where ucg.msisdn is null
	and bl.msisdn is null
;

insert into ${table-prefix}${cms-schema}.${table-prefix}ucg_tracking
(
	rep_month
    ,msisdn
    ,brand
    ,area_lacci
    ,region_lacci
    ,region_hlr
    ,city_hlr
    ,los
    ,device_type
    ,total_revenue_base
    ,rev_broadband
    ,total_revenue
    ,is_ucg
	,file_date
)
select * from ${table-prefix}${cms-schema}.${table-prefix}${pipeline-name}_tmp
;

drop table ${table-prefix}${cms-schema}.${table-prefix}${pipeline-name}_tmp
;