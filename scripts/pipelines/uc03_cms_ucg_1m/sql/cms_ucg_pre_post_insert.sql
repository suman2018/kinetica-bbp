insert into ${table-prefix}${cms_stg-schema}.${table-prefix}cms_pre_post_input
(
	rep_month
    ,msisdn
    ,brand
    ,area_lacci
    ,region_lacci 
    ,region_hlr
    ,city_hlr
    ,los   
    ,device_type
    ,status
    ,total_revenue
    ,rev_broadband
)
select
	rep_month
    ,msisdn
    ,brand
    ,area_lacci
    ,region_lacci 
    ,region_hlr
    ,city_hlr
    ,los  
    ,device_type
    ,status
    ,total_revenue
    ,rev_broadband
from ${table-prefix}${cms_stg-schema}.${table-prefix}cms_pre_post_input_temp;

drop table ${table-prefix}${cms_stg-schema}.${table-prefix}cms_pre_post_input_temp;