create or replace view ${table-prefix}${cms-schema}.${table-prefix}ucg_attribute_v as
select 
	rep_month,
	is_ucg,brand,
	los_segment,
	area,
	region,
	smartphone_user,
	rev_data_segment,
	arpu_group,
	count(msisdn) as total_subs,
	sum(total_revenue) as total_revenue 
from ${table-prefix}${cms-schema}.${table-prefix}ucg_attribute 
group by rep_month,
		 is_ucg,
		 brand,
		 los_segment,
		 area,
		 region,
		 smartphone_user,
		 rev_data_segment,
		 arpu_group