delete from ${table-prefix}${cms-schema}.${table-prefix}ucg_churn
where file_date = date('${run-date=yyyy-MM-dd}');

/* KI_HINT_DICT_PROJECTION */ 
create or replace temp table ${table-prefix}${cms-schema}.${table-prefix}temp_step_1a as
select
	rep_month
	,count(case when coalesce(los,0)<90 and is_ucg=1 then msisdn else null end) as ucg_los_0_3
	,count(case when coalesce(los,0)>=90 and coalesce(los,0)<180 and is_ucg=1 then msisdn else null end) as ucg_los_3_6
	,count(case when coalesce(los,0)>=180 and coalesce(los,0)<360 and is_ucg=1 then msisdn else null end) as ucg_los_6_12
	,count(case when coalesce(los,0)>=360 and is_ucg=1 then msisdn else null end) as ucg_los_12
	,count(case when is_ucg=1 then msisdn else null end) as ucg_los_all
	,count(case when coalesce(los,0)<90 and is_ucg=0 then msisdn else null end) as pop_los_0_3
	,count(case when coalesce(los,0)>=90 and coalesce(los,0)<180 and is_ucg=0 then msisdn else null end) as pop_los_3_6
	,count(case when coalesce(los,0)>=180 and coalesce(los,0)<360 and is_ucg=0 then msisdn else null end) as pop_los_6_12
	,count(case when coalesce(los,0)>=360 and is_ucg=0 then msisdn else null end) as pop_los_12
	,count(case when is_ucg=0 then msisdn else null end) as pop_los_all
from ${table-prefix}${cms-schema}.${table-prefix}ucg_tracking
group by rep_month
order by rep_month
;

/* KI_HINT_DICT_PROJECTION */ 
create or replace temp table ${table-prefix}${cms-schema}.${table-prefix}temp_step_1b as
select
	rep_month
	,count(case when coalesce(los,0)<90 and is_ucg=1 then msisdn else null end) as ucg_los_0_3
	,count(case when coalesce(los,0)>=90 and coalesce(los,0)<180 and is_ucg=1 then msisdn else null end) as ucg_los_3_6
	,count(case when coalesce(los,0)>=180 and coalesce(los,0)<360 and is_ucg=1 then msisdn else null end) as ucg_los_6_12
	,count(case when coalesce(los,0)>=360 and is_ucg=1 then msisdn else null end) as ucg_los_12
	,count(case when is_ucg=1 then msisdn else null end) as ucg_los_all
	,count(case when coalesce(los,0)<90 and is_ucg=0 then msisdn else null end) as pop_los_0_3
	,count(case when coalesce(los,0)>=90 and coalesce(los,0)<180 and is_ucg=0 then msisdn else null end) as pop_los_3_6
	,count(case when coalesce(los,0)>=180 and coalesce(los,0)<360 and is_ucg=0 then msisdn else null end) as pop_los_6_12
	,count(case when coalesce(los,0)>=360 and is_ucg=0 then msisdn else null end) as pop_los_12
	,count(case when is_ucg=0 then msisdn else null end) as pop_los_all
from ${table-prefix}${cms-schema}.${table-prefix}ucg_tracking
where rep_month = (select min(rep_month) from ${table-prefix}${cms-schema}.${table-prefix}ucg_tracking)
group by rep_month
order by rep_month
;

insert into ${table-prefix}${cms-schema}.${table-prefix}ucg_churn
(
	rep_month
	,subs_ucg_los_0_3
	,subs_ucg_los_3_6
	,subs_ucg_los_6_12
	,subs_ucg_los_12
	,subs_ucg_all
	,subs_pop_los_0_3
	,subs_pop_los_3_6
	,subs_pop_los_6_12
	,subs_pop_los_12
	,subs_pop_all
	,ucg_churn_los_0_3
	,ucg_churn_los_3_6
	,ucg_churn_los_6_12
	,ucg_churn_los_12
	,ucg_total_churn
	,pop_churn_los_0_3
	,pop_churn_los_3_6
	,pop_churn_los_6_12
	,pop_churn_los_12
	,pop_total_churn
    ,file_date
    ,orch_job_id
    ,pipeline_name
    ,pipeline_step
)
select
	a.rep_month
	,a.ucg_los_0_3
	,a.ucg_los_3_6
	,a.ucg_los_6_12
	,a.ucg_los_12
	,a.ucg_los_all
	,a.pop_los_0_3
	,a.pop_los_3_6
	,a.pop_los_6_12
	,a.pop_los_12
	,a.pop_los_all
	,b.ucg_los_0_3 - a.ucg_los_0_3
	,b.ucg_los_3_6 - a.ucg_los_3_6
	,b.ucg_los_6_12 - a.ucg_los_6_12
	,b.ucg_los_12 - a.ucg_los_12
	,b.ucg_los_all - a.ucg_los_all
	,b.pop_los_0_3 - a.pop_los_0_3
	,b.pop_los_3_6 - a.pop_los_3_6
	,b.pop_los_6_12 - a.pop_los_6_12
	,b.pop_los_12 - a.pop_los_12
	,b.pop_los_all - a.pop_los_all
	,date('${run-date=yyyy-MM-dd}')
from ${table-prefix}${cms-schema}.${table-prefix}temp_step_1a a
full outer join ${table-prefix}${cms-schema}.${table-prefix}temp_step_1b b
on 1=1
order by rep_month
;

drop table ${table-prefix}${cms-schema}.${table-prefix}temp_step_1a
;
drop table ${table-prefix}${cms-schema}.${table-prefix}temp_step_1b
;