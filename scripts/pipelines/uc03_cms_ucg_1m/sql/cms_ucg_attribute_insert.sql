delete from ${table-prefix}${cms-schema}.${table-prefix}ucg_attribute
where file_date = date('${run-date=yyyy-MM-dd}');

create or replace temp table ${table-prefix}${cms-schema}.${table-prefix}${pipeline-name}temp_step_1
as
select 
	rep_month
	,is_ucg
	,brand
	,region_lacci as region
	,area_lacci as area
	,case when coalesce(los,0)>=0 and coalesce(los,0)<90 then '01. LOS 0 - 3 month'
	      when coalesce(los,0)>=90 and coalesce(los,0)<180 then '02. LOS 3 - 6 month'
		  when coalesce(los,0)>=180 and coalesce(los,0)<360 then '03. LOS 6 - 12 month'
	      when coalesce(los,0)>=360 then '04. LOS > 12 month'
	 else '05. UNK' end as los_segment
	,coalesce(total_revenue_base,0) as total_rev
	,case when coalesce(rev_broadband,0)=0 then '1. REV_DATA = 0'
	   	  when coalesce(rev_broadband,0)>0 and coalesce(rev_broadband,0)<=20000 then '2. REV_DATA 0 - 20K'
	      when coalesce(rev_broadband,0)>20000 and coalesce(rev_broadband,0)<=70000 then '3. REV_DATA 20K - 70K'
	 else '4. REV_DATA > 70K' end as rev_data_segment
	,case when lower(device_type) like '%smartphone%' then '1. Smartphone' else '2. Non Smartphone' end as smartphone_user
	,msisdn
	,total_revenue
from ${table-prefix}${cms-schema}.${table-prefix}ucg_tracking where rep_month=last_day('${run-date=yyyy-MM-dd}')
;

create or replace temp table ${table-prefix}${cms-schema}.${table-prefix}${pipeline-name}temp_step_2
as
select
	rep_month
	,is_ucg
	,brand
	,region
	,area
	,los_segment
	,arpu_group
	,rev_data_segment
	,smartphone_user
	,msisdn
	,total_revenue
from
(
	select
		rep_month
		,is_ucg
		,brand
		,region
		,area
		,los_segment
		,case when total_rev=0 then '01. 0k'
		      when total_rev>0 and total_rev<=1000 then '02. 0-1000'
		      when total_rev>1000 and total_rev<=2000 then '03. 1000-2000'
		      when total_rev>2000 and total_rev<=3000 then '04. 2000-3000'
		      when total_rev>3000 and total_rev<=4000 then '05. 3000-4000'
		      when total_rev>4000 and total_rev<=5000 then '06. 4000-5000'
		      when total_rev>5000 and total_rev<=6000 then '07. 5000-6000'
		      when total_rev>6000 and total_rev<=7000 then '08. 6000-7000'
		      when total_rev>7000 and total_rev<=8000 then '09. 7000-8000'
		      when total_rev>8000 and total_rev<=9000 then '10. 8000-9000'
		      when total_rev>9000 and total_rev<=10000 then '11. 9000-10000'
		      when total_rev>10000 and total_rev<=11000 then '12. 10000-11000'
		      when total_rev>11000 and total_rev<=12000 then '13. 11000-12000'
		      when total_rev>12000 and total_rev<=13000 then '14. 12000-13000'
		      when total_rev>13000 and total_rev<=14000 then '15. 13000-14000'
		      when total_rev>14000 and total_rev<=15000 then '16. 14000-15000'
		      when total_rev>15000 and total_rev<=16000 then '17. 15000-16000'
		      when total_rev>16000 and total_rev<=17000 then '18. 16000-17000'
		      when total_rev>17000 and total_rev<=18000 then '19. 17000-18000'
		      when total_rev>18000 and total_rev<=19000 then '20. 18000-19000'
		      when total_rev>19000 and total_rev<=20000 then '21. 19000-20000'
		      when total_rev>20000 and total_rev<=21000 then '22. 20000-21000'
		      when total_rev>21000 and total_rev<=22000 then '23. 21000-22000'
		      when total_rev>22000 and total_rev<=23000 then '24. 22000-23000'
		      when total_rev>23000 and total_rev<=24000 then '25. 23000-24000'
		      when total_rev>24000 and total_rev<=25000 then '26. 24000-25000'
		      when total_rev>25000 and total_rev<=26000 then '27. 25000-26000'
		      when total_rev>26000 and total_rev<=27000 then '28. 26000-27000'
		      when total_rev>27000 and total_rev<=28000 then '29. 27000-28000'
		      when total_rev>28000 and total_rev<=29000 then '30. 28000-29000'
		      when total_rev>29000 and total_rev<=30000 then '31. 29000-30000'
		      when total_rev>30000 and total_rev<=50000 then '32. 30000-50000'
		      when total_rev>50000 and total_rev<=70000 then '33. 50000-70000'
		      when total_rev>70000 and total_rev<=100000 then '34. 70000-100000'
		      when total_rev>100000 and total_rev<=150000 then '35. 100000-150000'
		      when total_rev>150000 then '36. 150k++'
		else 'NA' end as arpu_group
		,rev_data_segment
		,smartphone_user
		,msisdn
		,total_revenue
	from ${table-prefix}${cms-schema}.${table-prefix}${pipeline-name}temp_step_1
)
;

insert into ${table-prefix}${cms-schema}.${table-prefix}ucg_attribute
(
	rep_month
	,is_ucg
	,brand
	,region
	,area
	,los_segment
	,arpu_group
	,rev_data_segment
	,smartphone_user
	,msisdn
	,total_revenue
	,file_date
)
select
	rep_month
	,is_ucg
	,brand
	,region
	,area
	,los_segment
	,arpu_group
	,rev_data_segment
	,smartphone_user
	,msisdn
	,total_revenue
	,date('${run-date=yyyy-MM-dd}') as file_date
from ${table-prefix}${cms-schema}.${table-prefix}${pipeline-name}temp_step_2
;

drop table ${table-prefix}${cms-schema}.${table-prefix}${pipeline-name}temp_step_1
;
drop table ${table-prefix}${cms-schema}.${table-prefix}${pipeline-name}temp_step_2
;