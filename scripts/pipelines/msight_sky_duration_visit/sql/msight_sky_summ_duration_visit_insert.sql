CREATE OR REPLACE TABLE ${table-prefix}${msight_sky-schema}.${table-prefix}sky_summ_duration_visit_stg AS 
SELECT
trx_date,
building_name,
propinsi,
kabupaten,
kecamatan,
kelurahan,
cast(latitude as decimal(9,6)) as latitude,
cast(longitude as decimal(9,6)) as longitude,
CAST (hour1 as integer) as hour1,
CAST (hour2 as integer) as hour2,
CAST (hour3 as integer) as hour3,
CAST (hour4 as integer) as hour4,
CAST (hour5 as integer) as hour5,
CAST (hour6 as integer) as hour6,
CAST (hour7 as integer) as hour7,
CAST (hour8 as integer) as hour8,
CAST (hour9 as integer) as hour9,
CAST (hour10 as integer) as hour10,
CAST (hour11 as integer) as hour11,
CAST (hour12 as integer) as hour12,
CAST (hour13 as integer) as hour13,
CAST (hour14 as integer) as hour14,
CAST (hour15 as integer) as hour15,
CAST (hour16 as integer) as hour16,
CAST (hour17 as integer) as hour17,
CAST (hour18 as integer) as hour18,
CAST (hour19 as integer) as hour19,
CAST (hour20 as integer) as hour20,
CAST (hour21 as integer) as hour21,
CAST (hour22 as integer) as hour22,
CAST (hour23 as integer) as hour23,
CAST (hour24 as integer) as hour24,
ki_shard_key (building_name) 
FROM ${table-prefix}${msight_sky-schema}.${table-prefix}sky_summ_duration_visit_input;


alter table ${table-prefix}${msight_sky-schema}.${table-prefix}sky_summ_duration_visit_stg modify column trx_date varchar(8, dict) ;
alter table ${table-prefix}${msight_sky-schema}.${table-prefix}sky_summ_duration_visit_stg modify column propinsi varchar(32, dict) ;
alter table ${table-prefix}${msight_sky-schema}.${table-prefix}sky_summ_duration_visit_stg modify column kabupaten varchar(32, dict) ;
alter table ${table-prefix}${msight_sky-schema}.${table-prefix}sky_summ_duration_visit_stg modify column kecamatan varchar(32, dict) ;
alter table ${table-prefix}${msight_sky-schema}.${table-prefix}sky_summ_duration_visit_stg modify column kelurahan varchar(32, dict) ;

alter table ${table-prefix}${msight_sky-schema}.${table-prefix}sky_summ_duration_visit_stg modify column hour1 integer (dict);
alter table ${table-prefix}${msight_sky-schema}.${table-prefix}sky_summ_duration_visit_stg modify column hour2 integer (dict);
alter table ${table-prefix}${msight_sky-schema}.${table-prefix}sky_summ_duration_visit_stg modify column hour3 integer (dict);
alter table ${table-prefix}${msight_sky-schema}.${table-prefix}sky_summ_duration_visit_stg modify column hour4 integer (dict);
alter table ${table-prefix}${msight_sky-schema}.${table-prefix}sky_summ_duration_visit_stg modify column hour5 integer (dict);
alter table ${table-prefix}${msight_sky-schema}.${table-prefix}sky_summ_duration_visit_stg modify column hour6 integer (dict);
alter table ${table-prefix}${msight_sky-schema}.${table-prefix}sky_summ_duration_visit_stg modify column hour7 integer (dict);
alter table ${table-prefix}${msight_sky-schema}.${table-prefix}sky_summ_duration_visit_stg modify column hour8 integer (dict);
alter table ${table-prefix}${msight_sky-schema}.${table-prefix}sky_summ_duration_visit_stg modify column hour9 integer (dict);
alter table ${table-prefix}${msight_sky-schema}.${table-prefix}sky_summ_duration_visit_stg modify column hour10 integer (dict);
alter table ${table-prefix}${msight_sky-schema}.${table-prefix}sky_summ_duration_visit_stg modify column hour11 integer (dict);
alter table ${table-prefix}${msight_sky-schema}.${table-prefix}sky_summ_duration_visit_stg modify column hour12 integer (dict);
alter table ${table-prefix}${msight_sky-schema}.${table-prefix}sky_summ_duration_visit_stg modify column hour13 integer (dict);
alter table ${table-prefix}${msight_sky-schema}.${table-prefix}sky_summ_duration_visit_stg modify column hour14 integer (dict);
alter table ${table-prefix}${msight_sky-schema}.${table-prefix}sky_summ_duration_visit_stg modify column hour15 integer (dict);
alter table ${table-prefix}${msight_sky-schema}.${table-prefix}sky_summ_duration_visit_stg modify column hour16 integer (dict);
alter table ${table-prefix}${msight_sky-schema}.${table-prefix}sky_summ_duration_visit_stg modify column hour17 integer (dict);
alter table ${table-prefix}${msight_sky-schema}.${table-prefix}sky_summ_duration_visit_stg modify column hour18 integer (dict);
alter table ${table-prefix}${msight_sky-schema}.${table-prefix}sky_summ_duration_visit_stg modify column hour19 integer (dict);
alter table ${table-prefix}${msight_sky-schema}.${table-prefix}sky_summ_duration_visit_stg modify column hour20 integer (dict);
alter table ${table-prefix}${msight_sky-schema}.${table-prefix}sky_summ_duration_visit_stg modify column hour21 integer (dict);
alter table ${table-prefix}${msight_sky-schema}.${table-prefix}sky_summ_duration_visit_stg modify column hour22 integer (dict);
alter table ${table-prefix}${msight_sky-schema}.${table-prefix}sky_summ_duration_visit_stg modify column hour23 integer (dict);
alter table ${table-prefix}${msight_sky-schema}.${table-prefix}sky_summ_duration_visit_stg modify column hour24 integer (dict);

delete from ${table-prefix}${msight_sky-schema}.${table-prefix}sky_summ_duration_visit where months = '${run-date=yyyyMM}';

insert into ${table-prefix}${msight_sky-schema}.${table-prefix}sky_summ_duration_visit
select 
 '${run-date=yyyyMM}' as months
,a.* from ${table-prefix}${msight_sky-schema}.${table-prefix}sky_summ_duration_visit_stg a;

DROP TABLE IF EXISTS ${table-prefix}${msight_sky-schema}.${table-prefix}sky_summ_duration_visit_stg;
DROP TABLE IF EXISTS ${table-prefix}${msight_sky-schema}.${table-prefix}sky_summ_duration_visit_input;
