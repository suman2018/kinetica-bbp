create or replace table ${table-prefix}${cms-schema}.${table-prefix}cms_campaign_sender
(
    "campaign_id" VARCHAR(64, dict, primary_key) NOT NULL,
    "sender" VARCHAR(256, dict) NOT NULL,
    "wording" VARCHAR(data) NOT NULL
)