CREATE OR REPLACE TABLE ${table-prefix}${cms-schema}.${table-prefix}cms_rcg_detail
(
    trx_date date(dict),
    msisdn BIGINT(shard_key),
    trx_rech INTEGER(dict),
    rech BIGINT(dict),
    denom INT(dict),
    sender_user_id varchar(64,dict),
   	file_date DATE(dict) NOT NULL,
    orch_job_id varchar(8, dict) NOT NULL,
    pipeline_name varchar(64, dict) NOT NULL,
    pipeline_step varchar(64, dict) NOT NULL
)