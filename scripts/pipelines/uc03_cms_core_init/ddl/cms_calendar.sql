create or replace replicated table ${utils-schema}.calendar
as
select cast(dt_dt as varchar) as dt_dt, day(dt_dt) as dt_day, week(dt_dt) as dt_week, month(dt_dt) dt_month, year(dt_dt) as dt_year
from (
select date(beginning + interval i day) as dt_dt 
from (select date('1970-01-01') as beginning)
join iter
on i < 50000
)