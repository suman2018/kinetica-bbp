create or replace table ${table-prefix}${cms-schema}.${table-prefix}cms_campaign_performance_dd
(
    "category" VARCHAR(16, dict),
    "campaign_type" VARCHAR(32, dict),
    "campaign_name" VARCHAR(128, dict),
    "campaign_group_id" INTEGER(dict),
    "campaign_id" VARCHAR(64, dict, shard_key),
    "campaign_objective_id" INTEGER(dict),
    "campaign_objective_desc" VARCHAR(64, dict),
    "business_group" VARCHAR(8, dict),
    "initiated_business" VARCHAR(32, dict),
    "start_period" TIMESTAMP,
    "end_period" TIMESTAMP,
    "segment_name" VARCHAR(256, dict),
    "trx_date" DATE(dict) NOT NULL,
    "is_btl" INTEGER,
	"target_submitted" INTEGER,
    "target_eligible" INTEGER,
    "control_eligible" INTEGER,
    "revenue" BIGINT,
    "taker" INTEGER,
    "revenue_control" BIGINT,
    "taker_control" INTEGER,
	"file_date" DATE(dict) NOT NULL,
    "orch_job_id" VARCHAR(8, dict) NOT NULL,
    "pipeline_name" VARCHAR(64, dict) NOT NULL,
    "pipeline_step" VARCHAR(64, dict) NOT NULL
)

    