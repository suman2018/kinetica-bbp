create or replace table ${table-prefix}${cms-schema}.${table-prefix}cms_chg_detail
(
    msisdn bigint(shard_key) NOT NULL, --needed for arpu
    trx_date date(dict) NOT NULL, --needed for arpu
    content_id varchar(64, dict),
    pricing_item_id integer(dict),
    vas_code varchar(64,dict),
    activation_channel_id varchar(8, dict),
    rev bigint(dict), --needed for arpu
    dur bigint(dict),
    trx_c integer(dict),
	file_date DATE(dict) NOT NULL,
    orch_job_id varchar(8, dict) NOT NULL,
    pipeline_name varchar(64, dict) NOT NULL,
    pipeline_step varchar(64, dict) NOT NULL
)

