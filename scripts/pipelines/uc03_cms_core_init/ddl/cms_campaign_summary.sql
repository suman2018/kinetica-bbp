create or replace table ${table-prefix}${cms-schema}.${table-prefix}cms_campaign_summary
(
    periode date(dict) not null,
    category varchar(16, dict),
    campaign_type varchar(32, dict),
    campaign_id varchar(64, shard_key, dict) not null,
    campaign_objective_id integer,
    campaign_objective_desc varchar(64, dict),
    campaign_name varchar(128, dict),
    start_period timestamp,
    end_period timestamp,
    campaign_group_id integer(dict),
    segment varchar(256, dict),
    business_group varchar(8, dict),
    initiated_business varchar(32, dict),
    region_hlr varchar(32, dict),
    cluster varchar(32, dict),
    branch varchar(64, dict),
    target int(dict) not null,
    eligible int(dict) not null,
    control int(dict) not null,
    revenue bigint,
    taker int(dict),
    unique_taker int(dict),
    revenue_control bigint,
    taker_control int(dict),
    unique_taker_control int(dict),
    arpu_before int,
    arpu_after int,
    arpu_control_before int,
    arpu_control_after int,
    file_date DATE(dict) NOT NULL,
    orch_job_id VARCHAR(8, dict) NOT NULL,
    pipeline_name VARCHAR(64, dict) NOT NULL,
    pipeline_step VARCHAR(64, dict) NOT NULL
)