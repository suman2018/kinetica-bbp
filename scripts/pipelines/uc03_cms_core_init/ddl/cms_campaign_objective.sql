create or replace replicated table ${table-prefix}${cms-schema}.${table-prefix}cms_campaign_objective
(
	campaign_objective_id int,
	campaign_group_id int,
	campaign_objective_desc varchar(64),
	campaign_group_desc varchar(64)
)
;