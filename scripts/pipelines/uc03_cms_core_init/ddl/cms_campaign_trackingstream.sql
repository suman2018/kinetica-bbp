CREATE OR REPLACE TABLE ${table-prefix}${cms-schema}.${table-prefix}cms_campaign_trackingstream
(
    "campaign_id" VARCHAR(64,dict) NOT NULL,
    "occurredon" TIMESTAMP,
    "msisdn" BIGINT(shard_key, dict) NOT NULL,
    "streamtype" VARCHAR(32,dict) NOT NULL,
    "iscontrolgroup" VARCHAR(8,dict),
    "attr_revenue" BIGINT,
    "attr_recharge" INTEGER,
    "attr_loc_kecamatan" VARCHAR(32,dict),
    "attr_loc_kabupaten" VARCHAR(32,dict),
    "attr_loc_region" VARCHAR(16,dict),
    "attr_cluster" VARCHAR(32,dict),
    "file_date" date(dict)
)
;