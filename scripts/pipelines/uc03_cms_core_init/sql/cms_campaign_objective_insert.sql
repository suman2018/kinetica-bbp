insert into ${table-prefix}${cms-schema}.${table-prefix}cms_campaign_objective select 1,  3, 'ACQUIRE HIGH-VALUE SUBS FROM COMPETITORS'    , 'NEW SALES / PREPAID REGISTRATION'     ;
insert into ${table-prefix}${cms-schema}.${table-prefix}cms_campaign_objective select 2,  3, 'DRIVE SIM REGISTRATION OF SUBS'              , 'NEW SALES / PREPAID REGISTRATION' ;
insert into ${table-prefix}${cms-schema}.${table-prefix}cms_campaign_objective select 3,  4, 'DRIVE ADOPTION OF MYTSEL APP AMONG SUBS'     , 'MyTelkomsel App' ;
insert into ${table-prefix}${cms-schema}.${table-prefix}cms_campaign_objective select 4,  1, 'GAIN BIGGER SHARE OF WALLET FROM MULTI-SIMER', 'REVENUE_USAGE' ;
insert into ${table-prefix}${cms-schema}.${table-prefix}cms_campaign_objective select 5,  1, 'DRIVE UPTAKE OF COMBO PACKS'                 , 'REVENUE_USAGE' ;
insert into ${table-prefix}${cms-schema}.${table-prefix}cms_campaign_objective select 6,  5, 'MIGRATE CUSTOMERS FROM 3G TO 4G'             , '4G MIGRATION' ;
insert into ${table-prefix}${cms-schema}.${table-prefix}cms_campaign_objective select 7,  10, 'DRIVE HIGHER SMARTPHONE PENETRATION'        , 'SMARTPHONE PENETRATION' ;
insert into ${table-prefix}${cms-schema}.${table-prefix}cms_campaign_objective select 8,  1, 'INCREASE PENETRATION OF DATA IN LEGACY BASE' , 'REVENUE_USAGE' ;
insert into ${table-prefix}${cms-schema}.${table-prefix}cms_campaign_objective select 9,  1, 'UPSELL DATA PACKAGES TO CURRENT DATA BASE'   , 'REVENUE_USAGE' ;
insert into ${table-prefix}${cms-schema}.${table-prefix}cms_campaign_objective select 10, 1, 'DRIVE VOICE PACKAGE PENETRATION'             , 'REVENUE_USAGE' ;
insert into ${table-prefix}${cms-schema}.${table-prefix}cms_campaign_objective select 11, 1, 'INCREASE HOLDING OF DIGITAL PRODUCTS'        , 'REVENUE_USAGE' ;
insert into ${table-prefix}${cms-schema}.${table-prefix}cms_campaign_objective select 11, 6, 'INCREASE HOLDING OF DIGITAL PRODUCTS'        , 'UPOINT' ;
insert into ${table-prefix}${cms-schema}.${table-prefix}cms_campaign_objective select 11, 7, 'INCREASE HOLDING OF DIGITAL PRODUCTS'        , 'GOOGLE PLAY' ;
insert into ${table-prefix}${cms-schema}.${table-prefix}cms_campaign_objective select 12, 2, 'REDUCE NUMBER OF INACTIVE SUBS / CHURN'      , 'RECHARGE (RECH Burning/Accumulation)' ;
insert into ${table-prefix}${cms-schema}.${table-prefix}cms_campaign_objective select 13, 1, 'WIN-BACK DATA LAPSERS'                       , 'REVENUE_USAGE ' ;
insert into ${table-prefix}${cms-schema}.${table-prefix}cms_campaign_objective select 14, 2, 'DRIVE HIGHER RECHARGE BALANCE'               , 'RECHARGE (RECH Burning/Accumulation)' ;
insert into ${table-prefix}${cms-schema}.${table-prefix}cms_campaign_objective select 15, 8, 'DRIVE PRE-TO-POST CONVERSION'                , 'PRE2POST' ;
insert into ${table-prefix}${cms-schema}.${table-prefix}cms_campaign_objective select 16, 9, 'LOYALTY'                                     , 'POIN (LOYALTY)' ;
insert into ${table-prefix}${cms-schema}.${table-prefix}cms_campaign_objective select 17, 13,'INFORMATION'                                 , 'INFORMATION' ;
insert into ${table-prefix}${cms-schema}.${table-prefix}cms_campaign_objective select 18, 0,'TESTING'                                      , 'OTHERS' ;
insert into ${table-prefix}${cms-schema}.${table-prefix}cms_campaign_objective select 19, 0,'OTHERS'                                       , 'OTHERS' ;



