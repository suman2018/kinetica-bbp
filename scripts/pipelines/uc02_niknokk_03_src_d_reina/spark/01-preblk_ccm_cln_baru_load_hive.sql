-- Author: Sadar Baskoro <ssetihajid@kinetica.com>
-- Last Update: 10 Jan 2019
-- Objective: load reina to staging table

select
trim(msisdn) as msisdn, 
trim(dt_dt_reg) as dt_dt_reg, 
trim(status_reg_ori) as status_reg_ori,
trim(simple_wording) as simple_wording,
trim(id_status) as id_status,
trim(code_status) as code_status,
trim(nik) as nik,
trim(nokk) as nokk,
trim(flag_nik) as flag_nik,
trim(flag_nokk) as flag_nokk
from gpu.gpu_preblk_ccm_cln_baru
where datex = '${run-date=yyyyMMdd}'
