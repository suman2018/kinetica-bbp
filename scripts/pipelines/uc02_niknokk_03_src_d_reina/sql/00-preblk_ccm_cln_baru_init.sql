create or replace table ${table-prefix}${niknokk-schema}.${table-prefix}niknokk_preblk_ccm_cln_baru_hive 
(
	msisdn	VARCHAR(16, shard_key)
	,dt_dt_reg	VARCHAR(32)
	,status_reg_ori VARCHAR(32)
	,simple_wording	VARCHAR(64, dict)
	,id_status	VARCHAR(1)
	,code_status	VARCHAR(32)
	,nik 	VARCHAR(32)
	,nokk 	VARCHAR(64)
	,flag_nik 	VARCHAR(32)
	,flag_nokk 	VARCHAR(32)
);