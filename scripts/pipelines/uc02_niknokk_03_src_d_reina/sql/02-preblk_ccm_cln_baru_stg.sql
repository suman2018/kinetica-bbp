/* KI_HINT_DICT_PROJECTION */
create or replace table ${table-prefix}${niknokk-schema}.${table-prefix}niknokk_preblk_ccm_cln_baru as
SELECT timestamp(date('${run-date=yyyy-MM-dd}')) AS trx_date,
char32('UNKNOWN') AS subs_id, 
char16(msisdn) as msisdn,
char16('UNKNOWN') AS channel, 
char64(nik) as nik, 
char64(nokk) as no_kk,
timestamp(datetime(dt_dt_reg)) as trx_timestamp,
timestamp(datetime(dt_dt_reg)) as insert_timestamp,
timestamp(datetime(dt_dt_reg)) as update_timestamp,
char64(simple_wording) as simple_wording,
cast(code_status as int) as code_status,
char1(id_status) as id_status, 
char16('UNKNOWN') AS adn,
char16('UNKNOWN') AS reg_type,
KI_SHARD_KEY(msisdn)
FROM ${table-prefix}${niknokk-schema}.${table-prefix}niknokk_preblk_ccm_cln_baru_hive;

--alter table ${table-prefix}${niknokk-schema}.${table-prefix}niknokk_preblk_ccm_cln_baru modify trx_date timestamp;
--alter table ${table-prefix}${niknokk-schema}.${table-prefix}niknokk_preblk_ccm_cln_baru modify subs_id varchar(32);
alter table ${table-prefix}${niknokk-schema}.${table-prefix}niknokk_preblk_ccm_cln_baru modify channel varchar(16, dict);
--alter table ${table-prefix}${niknokk-schema}.${table-prefix}niknokk_preblk_ccm_cln_baru modify nik varchar(32);
--alter table ${table-prefix}${niknokk-schema}.${table-prefix}niknokk_preblk_ccm_cln_baru modify no_kk varchar(64);
--alter table ${table-prefix}${niknokk-schema}.${table-prefix}niknokk_preblk_ccm_cln_baru modify trx_timestamp timestamp;
--alter table ${table-prefix}${niknokk-schema}.${table-prefix}niknokk_preblk_ccm_cln_baru modify insert_timestamp timestamp;
--alter table ${table-prefix}${niknokk-schema}.${table-prefix}niknokk_preblk_ccm_cln_baru modify update_timestamp timestamp;
alter table ${table-prefix}${niknokk-schema}.${table-prefix}niknokk_preblk_ccm_cln_baru modify simple_wording varchar(64, dict);
--alter table ${table-prefix}${niknokk-schema}.${table-prefix}niknokk_preblk_ccm_cln_baru modify code_status integer;
--alter table ${table-prefix}${niknokk-schema}.${table-prefix}niknokk_preblk_ccm_cln_baru modify id_status varchar(1);
alter table ${table-prefix}${niknokk-schema}.${table-prefix}niknokk_preblk_ccm_cln_baru modify adn varchar(16, dict);
alter table ${table-prefix}${niknokk-schema}.${table-prefix}niknokk_preblk_ccm_cln_baru modify reg_type varchar(16, dict);

drop table if exists ${table-prefix}${niknokk-schema}.${table-prefix}niknokk_preblk_ccm_cln_baru_hive ;

