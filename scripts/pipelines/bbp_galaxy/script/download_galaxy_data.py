import sys, os, glob, datetime, re, json, datetime
from azure.storage.blob import ContainerClient
from gpudb import gpudb

def filenameParser(file_name ):
    file_date = file_name.split("_")[0]
    file_time = file_name.split("_")[1].split(".")[0]
    date = file_date[:4] + '-' + file_date[4:6] + '-' + file_date[6:]
    time = file_time[:2] + ':' + file_time[2:4] + ':' + file_time[4:]
    date_time = datetime.datetime.strptime((date + ' '+ time), '%Y-%m-%d %H:%M:%S')
    return datetime.datetime.timestamp(date_time)

filename_regex = "[2][0][2-9][0-9](0[1-9]|1[012])(0[1-9]|[12]\d|3[01])_[0-2][0-9][0-6][0-9][0-6][0-9].csv"
containername = "asbchiller"
sas_token = "sv=2018-03-28&si=asbchiller-16FC216379E&sr=c&sig=JwTo2cKpZ8chHAaQz4%2FzN4txs%2BUWVg4gYLJV6Z%2BJsd4%3D"
connection_string = "DefaultEndpointsProtocol=https;AccountName=asbchiller;EndpointSuffix=core.windows.net"
local_drive = "/mnt/"

#host_ip = sys.argv[1] if len(sys.argv) > 1 else '127.0.0.1'
#host_port = sys.argv[2] if len(sys.argv) > 2 else '9191'
#host_username = sys.argv[3] if len(sys.argv) > 3 else 'admin'
#host_password = sys.argv[4] if len(sys.argv) > 4 else 'Kinetica1!'
#table_prefix = sys.argv[5] if len(sys.argv) > 5 else 'dev_'
#on_error = sys.argv[6] if len(sys.argv) > 6 else 'PERMISSIVE'

host_ip = "127.0.0.1"
host_port = "9191"
host_username = "admin"
host_password = "Kinetica1!"
table_prefix = "dev_"
on_error = "PERMISSIVE"

collection = table_prefix + "bbp"
data_table = table_prefix + "galaxy_data"
metadata_table = table_prefix + "galaxy_metadata"


conn_db = gpudb.GPUdb(encoding='BINARY', username=host_username, password=host_password, host=host_ip, port=host_port)
container = ContainerClient.from_connection_string(conn_str=connection_string, container_name=containername, credential=sas_token)

query_sql = "Select Filename from " + metadata_table + " order by run_datetime desc limit 1;"
response = conn_db.execute_sql(query_sql, encoding='json')
if response['status_info']['status'] == 'OK':
    data = json.loads(response['json_encoded_response'])['column_1']
    if (len(data)>0):
        last_file_loaded = data[0]
    elif (len(data)==0):
        last_file_loaded = "20000101_010101.csv"

#files = glob.glob(local_drive+"*.csv")
#files.sort(key=os.path.getmtime)
#last_file_loaded = files[len(files) - 1].split("/")[2]

last_file_timestamp = filenameParser(last_file_loaded)
new_filename_list = []
blob_list = container.list_blobs()
for blob in blob_list:
    if re.match(filename_regex, blob.name) and filenameParser(blob.name) > last_file_timestamp:
        new_filename_list.append(blob.name)
        container.get_blob_client
        blob_client = container.get_blob_client(blob=blob.name)
        download_file_path = os.path.join(local_drive, blob.name)
        print("Downloading blob : " + download_file_path)
        with open(download_file_path, "wb") as download_file:
            download_file.write(blob_client.download_blob().readall())
print("\nTotal of {} New Files found\n".format(len(new_filename_list)))
        
#if len(new_filename_list) > 0:   
#    with open('/orch_dev/logs/newfilelist.txt', 'w') as filehandle:
#        for new_filename in new_filename_list:
#            filehandle.write('%s\n' % new_filename)

for new_filename in new_filename_list:
    print("Loading "+ new_filename + " into " + data_table)
    load_sql = "LOAD INTO "+ data_table + " FROM FILE PATHS '/data/persist/mnt/" + new_filename + "' FORMAT TEXT ( DELIMITER = ',', INCLUDES HEADER = FALSE) WITH OPTIONS (ON ERROR = " + on_error + ", LOADING MODE = DISTRIBUTED LOCAL);"   
    response = conn_db.execute_sql(load_sql, encoding='json')
    
    if response['status_info']['status'] == 'ERROR':
        print(new_filename + " Failed to Load")
        print(response['status_info']['message'])
        raise Exception("Error occured, check gpudb-proc.log for more info")
    elif response['status_info']['status'] == 'OK':
        print(new_filename + " Loaded Successfully")
        insert_sql = "Insert into " + metadata_table + " (Run_Datetime, Filename, Status) VALUES ('"+ datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S.%f") + "', '" + new_filename + "', 'Loaded');" 
        resp = conn_db.execute_sql(insert_sql, encoding='json')
        if resp['status_info']['status'] == 'OK':
            print("Metadata Updated \n")
print("FINISHED")