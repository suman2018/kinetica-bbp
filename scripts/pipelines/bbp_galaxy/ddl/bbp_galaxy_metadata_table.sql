CREATE OR REPLACE TABLE ${table-prefix}${bbp-schema}.${table-prefix}galaxy_metadata
(
   "Run_Datetime" DATETIME NOT NULL,
   "Filename" VARCHAR (32) NOT NULL,
   "Status" VARCHAR (8, shard_key) NOT NULL
)
TIER STRATEGY 
(
	( ( VRAM 1, RAM 5, PERSIST 5 ) )
)