CREATE OR REPLACE TABLE ${table-prefix}${bbp-schema}.${table-prefix}galaxy_data
(
   "Tag" VARCHAR (128, dict),
   "Time" VARCHAR (shard_key, 32),
   "Value" VARCHAR (32, dict),
   "Quality" INTEGER
)
TIER STRATEGY 
(
    ( ( VRAM 1, RAM 5, PERSIST 5 ) )
)
;

