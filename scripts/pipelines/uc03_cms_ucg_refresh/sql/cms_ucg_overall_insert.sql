/* KI_HINT_DICT_PROJECTION */ 
create or replace temp table ${table-prefix}${cms-schema}.${table-prefix}overall_temp_step_1
as 
select
	rep_month
	,count(case when is_ucg=1 and lower(brand) in ('simpati','loop','kartuas') and los>=90 then msisdn else null end) as total_subs_ucg
    ,count(case when is_ucg=0 and lower(brand) in ('simpati','loop','kartuas') and los>=90 then msisdn else null end) as total_subs_pop
    ,sum(case when is_ucg=1 and lower(brand) in ('simpati','loop','kartuas') and los>=90 then total_revenue else null end) as total_revenue_ucg
    ,sum(case when is_ucg=0 and lower(brand) in ('simpati','loop','kartuas') and los>=90 then total_revenue else null end) as total_revenue_pop
    ,null as arpu_pop_exc
    ,null as arpu_ucg_exc
    ,null as arpu_pop_inc
    ,null as arpu_ucg_inc
    ,null as mom_arpu_pop_exc
    ,null as mom_arpu_ucg_exc
    ,null as mom_gap_exc
    ,null as mom_uplift_exc
    ,null as mom_arpu_pop_inc
    ,null as mom_arpu_ucg_inc
    ,null as mom_gap_inc 
    ,null as mom_uplift_inc
from ${table-prefix}${cms-schema}.${table-prefix}cms_ucg_tracking
where datetime(rep_month) >= (select datetime(max(last_day(description))) from ${table-prefix}${cms_stg-schema}.${table-prefix}cms_ucg_list_input where last_day(description) < last_day('${run-date=yyyy-MM-dd}'))
and datetime(rep_month) <= datetime('${run-date=yyyy-MM-dd}')
group by rep_month
order by rep_month
;

/* KI_HINT_DICT_PROJECTION */ 
create or replace temp table ${table-prefix}${cms-schema}.${table-prefix}overall_temp_step_2
as
select 
	b.*
	,int(total_revenue_pop / total_subs_pop) as arpu_pop_exc
	,int(total_revenue_ucg / total_subs_ucg) as arpu_ucg_exc
	,int(case when total_subs_pop_last_month is null then total_revenue_pop / total_subs_pop else total_revenue_pop / total_subs_pop_last_month end) as arpu_pop_inc
	,int(case when total_subs_ucg_last_month is null then total_revenue_ucg / total_subs_ucg else total_revenue_ucg / total_subs_ucg_last_month end) as arpu_ucg_inc
from 
(
	select 
		a.rep_month
		,a.total_subs_ucg
		,a.total_subs_pop
		,a.total_revenue_ucg
		,a.total_revenue_pop
		,lag(total_subs_pop,1) over (partition by null order by rep_month ) as total_subs_pop_last_month
		,lag(total_subs_ucg,1) over (partition by null order by rep_month ) as total_subs_ucg_last_month
	from ${table-prefix}${cms-schema}.${table-prefix}overall_temp_step_1 a 
) b
;

/* KI_HINT_DICT_PROJECTION */ 
create or replace temp table ${table-prefix}${cms-schema}.${table-prefix}overall_temp_step_3 
as
select
	b.*
	,arpu_pop_exc - arpu_pop_exc_last_month as mom_arpu_pop_exc
	,arpu_ucg_exc - arpu_ucg_exc_last_month as mom_arpu_ucg_exc
	,arpu_pop_inc - arpu_pop_inc_last_month as mom_arpu_pop_inc
	,arpu_ucg_inc - arpu_ucg_inc_last_month as mom_arpu_ucg_inc
from 
(
	select 
		a.rep_month
		,a.total_subs_ucg
		,a.total_subs_pop
		,a.total_revenue_ucg
		,a.total_revenue_pop
		,a.arpu_pop_exc
		,a.arpu_ucg_exc
		,a.arpu_pop_inc
		,a.arpu_ucg_inc
		,lag(arpu_pop_exc,1) over (partition by null order by rep_month ) as arpu_pop_exc_last_month
		,lag(arpu_ucg_exc,1) over (partition by null order by rep_month ) as arpu_ucg_exc_last_month
		,lag(arpu_pop_inc,1) over (partition by null order by rep_month ) as arpu_pop_inc_last_month
		,lag(arpu_ucg_inc,1) over (partition by null order by rep_month ) as arpu_ucg_inc_last_month
	from ${table-prefix}${cms-schema}.${table-prefix}overall_temp_step_2 a 
) b
;

/* KI_HINT_DICT_PROJECTION */ 
create or replace temp table ${table-prefix}${cms-schema}.${table-prefix}overall_temp_step_4 
as
select
	a.rep_month
	,a.total_subs_ucg
	,a.total_subs_pop
	,a.total_revenue_ucg
	,a.total_revenue_pop
	,a.arpu_pop_exc
	,a.arpu_ucg_exc
	,a.arpu_pop_inc
	,a.arpu_ucg_inc
	,a.mom_arpu_pop_exc
	,a.mom_arpu_ucg_exc
	,a.mom_arpu_pop_inc
	,a.mom_arpu_ucg_inc
	,a.arpu_pop_exc_last_month
	,a.arpu_pop_inc_last_month
	,round((double(mom_arpu_pop_exc) - double(mom_arpu_ucg_exc))/1000,2) as mom_gap_exc
	,round((double(mom_arpu_pop_exc) - double(mom_arpu_ucg_exc))/double(arpu_pop_exc_last_month)*100,2) as mom_uplift_exc
	,round((double(mom_arpu_pop_inc) - double(mom_arpu_ucg_inc))/1000,2) as mom_gap_inc
	,round((double(mom_arpu_pop_inc) - double(mom_arpu_ucg_inc))/double(arpu_pop_inc_last_month)*100,2) as mom_uplift_inc
from ${table-prefix}${cms-schema}.${table-prefix}overall_temp_step_3 a
;

insert into ${table-prefix}${cms-schema}.${table-prefix}cms_ucg_overall
(
	rep_month
	,total_subs_pop
    ,total_subs_ucg
    ,total_revenue_pop
    ,total_revenue_ucg
    ,arpu_pop_exc
    ,arpu_ucg_exc
    ,arpu_pop_inc
    ,arpu_ucg_inc
    ,mom_arpu_pop_exc
    ,mom_arpu_ucg_exc
    ,mom_gap_exc
    ,mom_uplift_exc
    ,mom_arpu_pop_inc
    ,mom_arpu_ucg_inc
    ,mom_gap_inc
    ,mom_uplift_inc
    ,file_date
)
select
	ucg.rep_month
	,ucg.total_subs_pop
    ,ucg.total_subs_ucg
    ,ucg.total_revenue_pop
    ,ucg.total_revenue_ucg
    ,ucg.arpu_pop_exc
    ,ucg.arpu_ucg_exc
    ,ucg.arpu_pop_inc
    ,ucg.arpu_ucg_inc
    ,ucg.mom_arpu_pop_exc
    ,ucg.mom_arpu_ucg_exc
    ,ucg.mom_gap_exc
    ,ucg.mom_uplift_exc
    ,ucg.mom_arpu_pop_inc
    ,ucg.mom_arpu_ucg_inc
    ,ucg.mom_gap_inc 
    ,ucg.mom_uplift_inc
	,date('${run-date=yyyy-MM-dd}') as file_date
from ${table-prefix}${cms-schema}.${table-prefix}overall_temp_step_4 ucg;

drop table ${table-prefix}${cms-schema}.${table-prefix}overall_temp_step_1;
drop table ${table-prefix}${cms-schema}.${table-prefix}overall_temp_step_2;
drop table ${table-prefix}${cms-schema}.${table-prefix}overall_temp_step_3;
drop table ${table-prefix}${cms-schema}.${table-prefix}overall_temp_step_4;
