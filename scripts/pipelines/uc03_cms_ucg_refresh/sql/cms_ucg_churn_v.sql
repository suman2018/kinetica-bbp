create or replace view ${table-prefix}${cms-schema}.${table-prefix}cms_ucg_churn_ucg_all_v as
select
	rep_month as ucg
	,subs_ucg_all as active
	,ucg_total_churn as churn
	,subs_ucg_all + ucg_total_churn as total_subs
	,floor(cast(subs_ucg_all as double)/(cast(subs_ucg_all as double) + cast(ucg_total_churn as double))*100) as perc_active
	,floor(cast(ucg_total_churn as double)/(cast(subs_ucg_all as double) + cast(ucg_total_churn as double))*100) as perc_churn
from ${table-prefix}${cms-schema}.${table-prefix}cms_ucg_churn
;
	
create or replace view ${table-prefix}${cms-schema}.${table-prefix}cms_ucg_churn_pop_all_v as
select
	rep_month as pop
	,subs_pop_all as active
	,pop_total_churn as churn
	,subs_pop_all + pop_total_churn as total_subs
	,floor(cast(subs_pop_all as double)/(cast(subs_pop_all as double) + cast(pop_total_churn as double))*100) as perc_active
	,floor(cast(pop_total_churn as double)/(cast(subs_pop_all as double) + cast(pop_total_churn as double))*100) as perc_churn
from ${table-prefix}${cms-schema}.${table-prefix}cms_ucg_churn
;

create or replace view ${table-prefix}${cms-schema}.${table-prefix}cms_ucg_churn_detail_v as
select
	rep_month as ucg
	,subs_ucg_los_0_3 as los_0_3_month_active
	,subs_ucg_los_0_3 - (select subs_ucg_los_0_3 from ${table-prefix}${cms-schema}.${table-prefix}cms_ucg_churn where rep_month = (select min(rep_month) from ${table-prefix}${cms-schema}.${table-prefix}cms_ucg_tracking)) as los_0_3_month_churn
	,subs_ucg_los_3_6 as los_3_6_month_active
	,subs_ucg_los_3_6 - (select subs_ucg_los_3_6 from ${table-prefix}${cms-schema}.${table-prefix}cms_ucg_churn where rep_month = (select min(rep_month) from ${table-prefix}${cms-schema}.${table-prefix}cms_ucg_tracking)) as los_3_6_month_churn
	,subs_ucg_los_6_12 as los_6_12_month_active
	,subs_ucg_los_6_12 - (select subs_ucg_los_6_12 from ${table-prefix}${cms-schema}.${table-prefix}cms_ucg_churn where rep_month = (select min(rep_month) from ${table-prefix}${cms-schema}.${table-prefix}cms_ucg_tracking)) as los_6_12_month_churn
	,subs_ucg_los_12 as los_over_12_month_active
	,subs_ucg_los_12 - (select subs_ucg_los_12 from ${table-prefix}${cms-schema}.${table-prefix}cms_ucg_churn where rep_month = (select min(rep_month) from ${table-prefix}${cms-schema}.${table-prefix}cms_ucg_tracking)) as los_over_12_month_churn
from ${table-prefix}${cms-schema}.${table-prefix}cms_ucg_churn
;

create or replace view ${table-prefix}${cms-schema}.${table-prefix}cms_ucg_churn_overall_v as
select 
	rep_month as "period",
	'Active' as category,
	round(double(subs_ucg_los_0_3/1000000.0),2) as los_0_3_month,
	round(double(subs_ucg_los_3_6/1000000.0),2) as los_3_6_month,
	round(double(subs_ucg_los_6_12/1000000.0),2) as los_6_12_month,
	round(double(subs_ucg_los_12/1000000.0),2) as los_over_12_month,
	round(double((subs_ucg_los_0_3 + subs_ucg_los_3_6 + subs_ucg_los_6_12 + subs_ucg_los_12)/1000000.0),2) as total
from ${table-prefix}${cms-schema}.${table-prefix}cms_ucg_churn
where rep_month=(select min(rep_month) from ${table-prefix}${cms-schema}.${table-prefix}cms_ucg_churn)
union all
select 
	rep_month as "period",
	'Active' as category,
	round(double(subs_ucg_los_0_3/1000000.0),2) as los_0_3_month,
	round(double(subs_ucg_los_3_6/1000000.0),2) as los_3_6_month,
	round(double(subs_ucg_los_6_12/1000000.0),2) as los_6_12_month,
	round(double(subs_ucg_los_12/1000000.0),2) as los_over_12_month,
	round(double((subs_ucg_los_0_3 + subs_ucg_los_3_6 + subs_ucg_los_6_12 + subs_ucg_los_12)/1000000.0),2) as total
from ${table-prefix}${cms-schema}.${table-prefix}cms_ucg_churn
where rep_month=(select max(rep_month) from ${table-prefix}${cms-schema}.${table-prefix}cms_ucg_churn)
union all
select 
	rep_month as "period",
	'Churn' as category,
	round(double(ucg_churn_los_0_3/1000000.0),2) as los_0_3_month,
	round(double(ucg_churn_los_3_6/1000000.0),2) as los_3_6_month,
	round(double(ucg_churn_los_6_12/1000000.0),2) as los_6_12_month,
	round(double(ucg_churn_los_12/1000000.0),2) as los_over_12_month,
	round(double((ucg_churn_los_0_3 + ucg_churn_los_3_6 + ucg_churn_los_6_12 + ucg_churn_los_12)/1000000.0),2) as total
from ${table-prefix}${cms-schema}.${table-prefix}cms_ucg_churn
where rep_month=(select max(rep_month) from ${table-prefix}${cms-schema}.${table-prefix}cms_ucg_churn)
;
