delete from ${table-prefix}${cms_stg-schema}.${table-prefix}cms_ucg_list_input
where datetime(description) = datetime(last_day(date(date('${run-date=yyyy-MM-dd}') - interval '1' month)));

insert into ${table-prefix}${cms_stg-schema}.${table-prefix}cms_ucg_list_input
(
	msisdn
	,description
)
select
	msisdn
	,description
from ${table-prefix}${cms_stg-schema}.${table-prefix}cms_ucg_list_input_temp
where description in (select max(description) from ${table-prefix}${cms_stg-schema}.${table-prefix}cms_ucg_list_input_temp);

drop table ${table-prefix}${cms_stg-schema}.${table-prefix}cms_ucg_list_input_temp;
