/* KI_HINT_DICT_PROJECTION */ 
create or replace temp table ${table-prefix}${cms-schema}.${table-prefix}rev_impact_tmp_step_1 as 
select
	rep_month
	,count(case when coalesce(los,0)>=0 and lower(brand) in ('simpati','loop','kartuas') and is_ucg=1 then msisdn else null end) as ucg_los_over_0m_subs 
	,count(case when coalesce(los,0)>=90 and lower(brand) in ('simpati','loop','kartuas') and is_ucg=1 then msisdn else null end) as ucg_los_over_3m_subs  
    ,count(case when coalesce(los,0)>=180 and lower(brand) in ('simpati','loop','kartuas') and is_ucg=1 then msisdn else null end) as ucg_los_over_6m_subs 
    ,count(case when coalesce(los,0)>=360 and lower(brand) in ('simpati','loop','kartuas') and is_ucg=1 then msisdn else null end) as ucg_los_over_12m_subs 
    ,count(case when coalesce(los,0)>=0 and lower(brand) in ('simpati','loop','kartuas') and is_ucg=0 then msisdn else null end) as pop_los_over_0m_subs
    ,count(case when coalesce(los,0)>=90 and lower(brand) in ('simpati','loop','kartuas') and is_ucg=0 then msisdn else null end) as pop_los_over_3m_subs 
    ,count(case when coalesce(los,0)>=180 and lower(brand) in ('simpati','loop','kartuas') and is_ucg=0 then msisdn else null end) as pop_los_over_6m_subs 
    ,count(case when coalesce(los,0)>=360 and lower(brand) in ('simpati','loop','kartuas') and is_ucg=0 then msisdn else null end) as pop_los_over_12m_subs 
    ,sum(case when coalesce(los,0)>=0 and lower(brand) in ('simpati','loop','kartuas') and is_ucg=1 then total_revenue else null end) as ucg_los_over_0m_tot_rev 
    ,sum(case when coalesce(los,0)>=90 and lower(brand) in ('simpati','loop','kartuas') and is_ucg=1 then total_revenue else null end) as ucg_los_over_3m_tot_rev 
    ,sum(case when coalesce(los,0)>=180 and lower(brand) in ('simpati','loop','kartuas') and is_ucg=1 then total_revenue else null end) as ucg_los_over_6m_tot_rev 
    ,sum(case when coalesce(los,0)>=360 and lower(brand) in ('simpati','loop','kartuas') and is_ucg=1 then total_revenue else null end) as ucg_los_over_12m_tot_rev 
    ,sum(case when coalesce(los,0)>=0 and lower(brand) in ('simpati','loop','kartuas') and is_ucg=0 then total_revenue else null end) as pop_los_over_0m_tot_rev
    ,sum(case when coalesce(los,0)>=90 and lower(brand) in ('simpati','loop','kartuas') and is_ucg=0 then total_revenue else null end) as pop_los_over_3m_tot_rev 
    ,sum(case when coalesce(los,0)>=180 and lower(brand) in ('simpati','loop','kartuas') and is_ucg=0 then total_revenue else null end) as pop_los_over_6m_tot_rev 
    ,sum(case when coalesce(los,0)>=360 and lower(brand) in ('simpati','loop','kartuas') and is_ucg=0 then total_revenue else null end) as pop_los_over_12m_tot_rev
    ,round(sum(case when coalesce(los,0)>=90 and coalesce(los,0)<=180 and is_ucg=0 and lower(brand) in ('simpati','loop','kartuas') then total_revenue else null end)/1000000000000,2) as los_over_3m_revenue
    ,round(sum(case when coalesce(los,0)>=180 and coalesce(los,0)<=360 and is_ucg=0  and lower(brand) in ('simpati','loop','kartuas') then total_revenue else null end)/1000000000000,2) as los_over_6m_revenue 
    ,round(sum(case when coalesce(los,0)>=360 and is_ucg=0  and lower(brand) in ('simpati','loop','kartuas') then total_revenue else null end)/1000000000000,2) as los_over_12m_revenue
from ${table-prefix}${cms-schema}.${table-prefix}cms_ucg_tracking
where datetime(rep_month) >= (select datetime(max(last_day(description))) from ${table-prefix}${cms_stg-schema}.${table-prefix}cms_ucg_list_input where last_day(description) < last_day('${run-date=yyyy-MM-dd}'))
and datetime(rep_month) <= datetime('${run-date=yyyy-MM-dd}')
group by rep_month
order by rep_month
;

/* KI_HINT_DICT_PROJECTION */ 
create or replace temp table ${table-prefix}${cms-schema}.${table-prefix}rev_impact_tmp_step_2 as
select 
	b.*
	,round(b.pop_los_over_3m_tot_rev / b.pop_los_over_3m_subs,2) as arpu_over_3m_pop_exc
	,round(b.ucg_los_over_3m_tot_rev / b.ucg_los_over_3m_subs,2) as arpu_over_3m_ucg_exc
	,round(b.pop_los_over_6m_tot_rev / b.pop_los_over_6m_subs,2) as arpu_over_6m_pop_exc
	,round(b.ucg_los_over_6m_tot_rev / b.ucg_los_over_6m_subs,2) as arpu_over_6m_ucg_exc
	,round(b.pop_los_over_12m_tot_rev / b.pop_los_over_12m_subs,2) as arpu_over_12m_pop_exc
	,round(b.ucg_los_over_12m_tot_rev / b.ucg_los_over_12m_subs,2) as arpu_over_12m_ucg_exc
	,round(case when b.pop_los_over_3m_subs_last_month is null then b.pop_los_over_3m_tot_rev / b.pop_los_over_3m_subs else b.pop_los_over_3m_tot_rev / b.pop_los_over_3m_subs_last_month end,2) as arpu_over_3m_pop_inc
	,round(case when b.ucg_los_over_3m_subs_last_month is null then b.ucg_los_over_3m_tot_rev / b.ucg_los_over_3m_subs else b.ucg_los_over_3m_tot_rev / b.ucg_los_over_3m_subs_last_month end,2) as arpu_over_3m_ucg_inc
	,round(case when b.pop_los_over_6m_subs_last_month is null then b.pop_los_over_6m_tot_rev / b.pop_los_over_6m_subs else b.pop_los_over_6m_tot_rev / b.pop_los_over_6m_subs_last_month end,2) as arpu_over_6m_pop_inc
	,round(case when b.ucg_los_over_6m_subs_last_month is null then b.ucg_los_over_6m_tot_rev / b.ucg_los_over_6m_subs else b.ucg_los_over_6m_tot_rev / b.ucg_los_over_6m_subs_last_month end,2) as arpu_over_6m_ucg_inc
	,round(case when b.pop_los_over_12m_subs_last_month is null then b.pop_los_over_12m_tot_rev / b.pop_los_over_12m_subs else b.pop_los_over_12m_tot_rev / b.pop_los_over_12m_subs_last_month end,2) as arpu_over_12m_pop_inc
	,round(case when b.ucg_los_over_12m_subs_last_month is null then b.ucg_los_over_12m_tot_rev / b.ucg_los_over_12m_subs else b.ucg_los_over_12m_tot_rev / b.ucg_los_over_12m_subs_last_month end,2) as arpu_over_12m_ucg_inc
from 
(
	select 
		*
		,lag(a.ucg_los_over_3m_subs,1) over (partition by null order by a.rep_month ) as ucg_los_over_3m_subs_last_month
		,lag(a.ucg_los_over_6m_subs,1) over (partition by null order by a.rep_month ) as ucg_los_over_6m_subs_last_month
		,lag(a.ucg_los_over_12m_subs,1) over (partition by null order by a.rep_month ) as ucg_los_over_12m_subs_last_month
		,lag(a.pop_los_over_3m_subs,1) over (partition by null order by a.rep_month ) as pop_los_over_3m_subs_last_month
		,lag(a.pop_los_over_6m_subs,1) over (partition by null order by a.rep_month ) as pop_los_over_6m_subs_last_month
		,lag(a.pop_los_over_12m_subs,1) over (partition by null order by a.rep_month ) as pop_los_over_12m_subs_last_month
	from ${table-prefix}${cms-schema}.${table-prefix}rev_impact_tmp_step_1 a 
) b
;

/* KI_HINT_DICT_PROJECTION */ 
create or replace temp table ${table-prefix}${cms-schema}.${table-prefix}rev_impact_tmp_step_3 as
select 
	b.*
	,round(case when b.arpu_over_3m_ucg_exc_last_month is null then null else b.arpu_over_3m_ucg_exc - b.arpu_over_3m_ucg_exc_last_month end,2) as mom_arpu_over_3m_ucg_exc
	,round(case when b.arpu_over_6m_ucg_exc_last_month is null then null else b.arpu_over_6m_ucg_exc - b.arpu_over_6m_ucg_exc_last_month end,2) as mom_arpu_over_6m_ucg_exc
	,round(case when b.arpu_over_12m_ucg_exc_last_month is null then null else b.arpu_over_12m_ucg_exc - b.arpu_over_12m_ucg_exc_last_month end,2) as mom_arpu_over_12m_ucg_exc
	,round(case when b.arpu_over_3m_pop_exc_last_month is null then null else b.arpu_over_3m_pop_exc - b.arpu_over_3m_pop_exc_last_month end,2) as mom_arpu_over_3m_pop_exc
	,round(case when b.arpu_over_6m_pop_exc_last_month is null then null else b.arpu_over_6m_pop_exc - b.arpu_over_6m_pop_exc_last_month end,2) as mom_arpu_over_6m_pop_exc
	,round(case when b.arpu_over_12m_pop_exc_last_month is null then null else b.arpu_over_12m_pop_exc - b.arpu_over_12m_pop_exc_last_month end,2) as mom_arpu_over_12m_pop_exc
	,round(case when b.arpu_over_3m_ucg_inc_last_month is null then null else b.arpu_over_3m_ucg_inc - b.arpu_over_3m_ucg_inc_last_month end,2) as mom_arpu_over_3m_ucg_inc
	,round(case when b.arpu_over_6m_ucg_inc_last_month is null then null else b.arpu_over_6m_ucg_inc - b.arpu_over_6m_ucg_inc_last_month end,2) as mom_arpu_over_6m_ucg_inc
	,round(case when b.arpu_over_12m_ucg_inc_last_month is null then null else b.arpu_over_12m_ucg_inc - b.arpu_over_12m_ucg_inc_last_month end,2) as mom_arpu_over_12m_ucg_inc
	,round(case when b.arpu_over_3m_pop_inc_last_month is null then null else b.arpu_over_3m_pop_inc - b.arpu_over_3m_pop_inc_last_month end,2) as mom_arpu_over_3m_pop_inc
	,round(case when b.arpu_over_6m_pop_inc_last_month is null then null else b.arpu_over_6m_pop_inc - b.arpu_over_6m_pop_inc_last_month end,2) as mom_arpu_over_6m_pop_inc
	,round(case when b.arpu_over_12m_pop_inc_last_month is null then null else arpu_over_12m_pop_inc - b.arpu_over_12m_pop_inc_last_month end,2) as mom_arpu_over_12m_pop_inc
from
(
	select 
		*
		,lag(a.arpu_over_3m_ucg_exc,1) over (partition by null order by a.rep_month ) as arpu_over_3m_ucg_exc_last_month
		,lag(a.arpu_over_6m_ucg_exc,1) over (partition by null order by a.rep_month ) as arpu_over_6m_ucg_exc_last_month
		,lag(a.arpu_over_12m_ucg_exc,1) over (partition by null order by a.rep_month ) as arpu_over_12m_ucg_exc_last_month
		,lag(a.arpu_over_3m_pop_exc,1) over (partition by null order by a.rep_month ) as arpu_over_3m_pop_exc_last_month
		,lag(a.arpu_over_6m_pop_exc,1) over (partition by null order by a.rep_month ) as arpu_over_6m_pop_exc_last_month
		,lag(a.arpu_over_12m_pop_exc,1) over (partition by null order by a.rep_month ) as arpu_over_12m_pop_exc_last_month
		,lag(a.arpu_over_3m_ucg_inc,1) over (partition by null order by a.rep_month ) as arpu_over_3m_ucg_inc_last_month
		,lag(a.arpu_over_6m_ucg_inc,1) over (partition by null order by a.rep_month ) as arpu_over_6m_ucg_inc_last_month
		,lag(a.arpu_over_12m_ucg_inc,1) over (partition by null order by a.rep_month ) as arpu_over_12m_ucg_inc_last_month
		,lag(a.arpu_over_3m_pop_inc,1) over (partition by null order by a.rep_month ) as arpu_over_3m_pop_inc_last_month
		,lag(a.arpu_over_6m_pop_inc,1) over (partition by null order by a.rep_month ) as arpu_over_6m_pop_inc_last_month
		,lag(a.arpu_over_12m_pop_inc,1) over (partition by null order by a.rep_month ) as arpu_over_12m_pop_inc_last_month
	from ${table-prefix}${cms-schema}.${table-prefix}rev_impact_tmp_step_2 a 
) b
;

create or replace temp table ${table-prefix}${cms-schema}.${table-prefix}rev_impact_tmp_step_4 as
select 
	b.*
	,round((b.mom_arpu_over_3m_pop_exc - b.mom_arpu_over_3m_ucg_exc)/1000,2) as gap_over_3m_exc
	,round((b.mom_arpu_over_6m_pop_exc - b.mom_arpu_over_6m_ucg_exc)/1000,2) as gap_over_6m_exc
	,round((b.mom_arpu_over_12m_pop_exc - b.mom_arpu_over_12m_ucg_exc)/1000,2) as gap_over_12m_exc
	,round((b.mom_arpu_over_3m_pop_inc - b.mom_arpu_over_3m_ucg_inc)/1000,2) as gap_over_3m_inc
	,round((b.mom_arpu_over_6m_pop_inc - b.mom_arpu_over_6m_ucg_inc)/1000,2) as gap_over_6m_inc
	,round((b.mom_arpu_over_12m_pop_inc - b.mom_arpu_over_12m_ucg_inc)/1000,2) as gap_over_12m_inc
from
(
	select 
		*
	from ${table-prefix}${cms-schema}.${table-prefix}rev_impact_tmp_step_3 a 
) b
;

create or replace temp table ${table-prefix}${cms-schema}.${table-prefix}rev_impact_tmp_step_5 as
select 
	b.*
	,round((b.gap_over_3m_exc*1000)/b.arpu_over_3m_pop_exc_last_month*100,2) as uplift_over_3m_exc
	,round((b.gap_over_6m_exc*1000)/b.arpu_over_6m_pop_exc_last_month*100,2) as uplift_over_6m_exc
	,round((b.gap_over_12m_exc*1000)/b.arpu_over_12m_pop_exc_last_month*100,2) as uplift_over_12m_exc
	,round((b.gap_over_3m_inc*1000)/b.arpu_over_3m_pop_inc_last_month*100,2) as uplift_over_3m_inc
	,round((b.gap_over_6m_inc*1000)/b.arpu_over_6m_pop_inc_last_month*100,2) as uplift_over_6m_inc
	,round((b.gap_over_12m_inc*1000)/b.arpu_over_12m_pop_inc_last_month*100,2) as uplift_over_12m_inc
from
(
	select 
		*
	from ${table-prefix}${cms-schema}.${table-prefix}rev_impact_tmp_step_4 a 
) b
;

create or replace temp table ${table-prefix}${cms-schema}.${table-prefix}rev_impact_tmp_step_6 as
select 
	b.*
	,round(b.uplift_over_3m_exc*(b.los_over_3m_revenue + b.los_over_6m_revenue + b.los_over_12m_revenue)*1000/100,2) as exclude_over_3m
	,round(b.uplift_over_6m_exc*(b.los_over_6m_revenue + b.los_over_12m_revenue)*1000/100,2) as exclude_over_6m
	,round(b.uplift_over_12m_exc*(b.los_over_12m_revenue)*1000/100,2) as exclude_over_12m
	,round(b.uplift_over_3m_inc*(b.los_over_3m_revenue + b.los_over_6m_revenue + b.los_over_12m_revenue)*1000/100,2) as include_over_3m
	,round(b.uplift_over_6m_inc*(b.los_over_6m_revenue + b.los_over_12m_revenue)*1000/100,2) as include_over_6m
	,round(b.uplift_over_12m_inc*(b.los_over_12m_revenue)*1000/100,2) as include_over_12m
from
(
	select 
		a.rep_month
		,a.ucg_los_over_0m_subs
		,a.ucg_los_over_3m_subs
		,a.ucg_los_over_6m_subs
		,a.ucg_los_over_12m_subs
		,a.pop_los_over_0m_subs
		,a.pop_los_over_3m_subs
		,a.pop_los_over_6m_subs
		,a.pop_los_over_12m_subs
		,a.ucg_los_over_0m_tot_rev
		,a.ucg_los_over_3m_tot_rev
		,a.ucg_los_over_6m_tot_rev
		,a.ucg_los_over_12m_tot_rev
		,a.pop_los_over_0m_tot_rev
		,a.pop_los_over_3m_tot_rev
		,a.pop_los_over_6m_tot_rev
		,a.pop_los_over_12m_tot_rev
		,a.arpu_over_3m_pop_exc   
		,a.arpu_over_3m_ucg_exc   
		,a.arpu_over_6m_pop_exc   
		,a.arpu_over_6m_ucg_exc   
		,a.arpu_over_12m_pop_exc
		,a.arpu_over_12m_ucg_exc
		,a.arpu_over_3m_pop_inc      
		,a.arpu_over_3m_ucg_inc      
		,a.arpu_over_6m_pop_inc      
		,a.arpu_over_6m_ucg_inc      
		,a.arpu_over_12m_pop_inc
		,a.arpu_over_12m_ucg_inc
		,a.mom_arpu_over_3m_ucg_exc    
		,a.mom_arpu_over_6m_ucg_exc    
		,a.mom_arpu_over_12m_ucg_exc
		,a.mom_arpu_over_3m_pop_exc    
		,a.mom_arpu_over_6m_pop_exc
		,a.mom_arpu_over_12m_pop_exc
		,a.mom_arpu_over_3m_ucg_inc
		,a.mom_arpu_over_6m_ucg_inc
		,a.mom_arpu_over_12m_ucg_inc
		,a.mom_arpu_over_3m_pop_inc    
		,a.mom_arpu_over_6m_pop_inc    
		,a.mom_arpu_over_12m_pop_inc
		,a.gap_over_3m_exc
		,a.uplift_over_3m_exc
		,a.gap_over_6m_exc
		,a.uplift_over_6m_exc
		,a.gap_over_12m_exc
		,a.uplift_over_12m_exc
		,a.gap_over_3m_inc
		,a.uplift_over_3m_inc
		,a.gap_over_6m_inc
		,a.uplift_over_6m_inc
		,a.gap_over_12m_inc
		,a.uplift_over_12m_inc    
		,a.los_over_3m_revenue
		,a.los_over_6m_revenue
		,a.los_over_12m_revenue
	from ${table-prefix}${cms-schema}.${table-prefix}rev_impact_tmp_step_5 a
) b
;

insert into ${table-prefix}${cms-schema}.${table-prefix}cms_ucg_rev_impact
(
	rep_month
	,ucg_los_over_0m_subs
	,ucg_los_over_3m_subs 
    ,ucg_los_over_6m_subs
    ,ucg_los_over_12m_subs
    ,pop_los_over_0m_subs
    ,pop_los_over_3m_subs
    ,pop_los_over_6m_subs
    ,pop_los_over_12m_subs
    ,ucg_los_over_0m_tot_rev
    ,ucg_los_over_3m_tot_rev
    ,ucg_los_over_6m_tot_rev
    ,ucg_los_over_12m_tot_rev
    ,pop_los_over_0m_tot_rev
    ,pop_los_over_3m_tot_rev
    ,pop_los_over_6m_tot_rev
    ,pop_los_over_12m_tot_rev
    ,arpu_exclude_churn_los_over_3m_ucg
    ,arpu_exclude_churn_los_over_3m_pop
    ,arpu_exclude_churn_los_over_6m_ucg
    ,arpu_exclude_churn_los_over_6m_pop
    ,arpu_exclude_churn_los_over_12m_ucg
    ,arpu_exclude_churn_los_over_12m_pop
    ,arpu_include_churn_los_over_3m_ucg
    ,arpu_include_churn_los_over_3m_pop
    ,arpu_include_churn_los_over_6m_ucg
    ,arpu_include_churn_los_over_6m_pop
    ,arpu_include_churn_los_over_12m_ucg
    ,arpu_include_churn_los_over_12m_pop
    ,vs_jan_exclude_churn_los_over_3m_ucg
    ,vs_jan_exclude_churn_los_over_3m_pop
    ,vs_jan_exclude_churn_los_over_6m_ucg
    ,vs_jan_exclude_churn_los_over_6m_pop
    ,vs_jan_exclude_churn_los_over_12m_ucg
    ,vs_jan_exclude_churn_los_over_12m_pop
    ,vs_jan_include_churn_los_over_3m_ucg
    ,vs_jan_include_churn_los_over_3m_pop
    ,vs_jan_include_churn_los_over_6m_ucg
    ,vs_jan_include_churn_los_over_6m_pop
    ,vs_jan_include_churn_los_over_12m_ucg
    ,vs_jan_include_churn_los_over_12m_pop
    ,vs_jan_exclude_churn_los_over_3m_gap
    ,vs_jan_exclude_churn_los_over_3m_uplift
    ,vs_jan_exclude_churn_los_over_6m_gap
    ,vs_jan_exclude_churn_los_over_6m_uplift
    ,vs_jan_exclude_churn_los_over_12m_gap
    ,vs_jan_exclude_churn_los_over_12m_uplift
    ,vs_jan_include_churn_los_over_3m_gap
    ,vs_jan_include_churn_los_over_3m_uplift
    ,vs_jan_include_churn_los_over_6m_gap
    ,vs_jan_include_churn_los_over_6m_uplift
    ,vs_jan_include_churn_los_over_12m_gap
    ,vs_jan_include_churn_los_over_12m_uplift
    ,revenue_over_3m
    ,revenue_over_6m
    ,revenue_over_12m
    ,exclude_over_3m
    ,exclude_over_6m
    ,exclude_over_12m
	,include_over_3m
	,include_over_6m
	,include_over_12m
	,file_date
)
select
	*
	,date('${run-date=yyyy-MM-dd}') as file_date
from ${table-prefix}${cms-schema}.${table-prefix}rev_impact_tmp_step_6 a
;

drop table ${table-prefix}${cms-schema}.${table-prefix}rev_impact_tmp_step_1;
drop table ${table-prefix}${cms-schema}.${table-prefix}rev_impact_tmp_step_2;
drop table ${table-prefix}${cms-schema}.${table-prefix}rev_impact_tmp_step_3;
drop table ${table-prefix}${cms-schema}.${table-prefix}rev_impact_tmp_step_4;
drop table ${table-prefix}${cms-schema}.${table-prefix}rev_impact_tmp_step_5;
drop table ${table-prefix}${cms-schema}.${table-prefix}rev_impact_tmp_step_6;
