create or replace table ${table-prefix}${cms-schema}.${table-prefix}cms_ucg_overall
(
	"rep_month" date,
	"total_subs_pop" bigint,
    "total_subs_ucg" bigint,
    "total_revenue_pop" double,
    "total_revenue_ucg" double,
    "arpu_pop_exc" integer,
    "arpu_ucg_exc" integer,
    "arpu_pop_inc" integer,
    "arpu_ucg_inc" integer,
    "mom_arpu_pop_exc" integer,
    "mom_arpu_ucg_exc" integer,
    "mom_gap_exc" double,
    "mom_uplift_exc" double,
    "mom_arpu_pop_inc" integer,
    "mom_arpu_ucg_inc" integer,
    "mom_gap_inc" double,
    "mom_uplift_inc" double,
    "file_date" DATE(dict) NOT NULL
)