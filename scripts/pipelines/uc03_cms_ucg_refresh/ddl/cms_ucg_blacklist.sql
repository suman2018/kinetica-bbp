create or replace table ${table-prefix}${cms_stg-schema}.${table-prefix}cms_ara_blacklist_input
(
	"msisdn" bigint(shard_key),
	"reason" varchar(32,dict)
)