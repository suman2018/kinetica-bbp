create or replace temp table ${table-prefix}${cms_stg-schema}.${table-prefix}cms_pre_post_input_temp
(
    "rep_month" date(dict),
	"msisdn" bigint(shard_key),
	"brand" varchar(16, dict),
	"area_lacci" varchar(8,dict),
    "region_lacci" varchar(32, dict),
    "region_hlr" varchar(16,dict),
    "city_hlr" varchar(16,dict),
    "los" int,
    "device_type" varchar(16,dict),
    "status" varchar(32, dict),
    "total_revenue" double,
    "rev_broadband" double
)