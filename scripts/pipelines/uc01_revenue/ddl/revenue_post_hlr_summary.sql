CREATE OR REPLACE TABLE ${table-prefix}${revenue-schema}.${table-prefix}revenue_post_hlr_summary(
   FILE_DATE             date             
  ,TRX_DATE              date             
  ,BRAND                 varchar(16, dict)           
  ,OFFER_ID              varchar(8, shard_key) 
  ,AREA_HLR              varchar(8, dict)
  ,REGION_HLR            varchar(16, dict)           
  ,CITY_HLR              varchar(16, dict)                  
  ,ACTIVATION_CHANNEL_ID varchar(8, dict)            
  ,NODE_TYPE             varchar(8, dict)            
  ,PRE_POST_FLAG         integer(dict)       
  ,L1_NAME               varchar(32, dict)           
  ,L2_NAME               varchar(32, dict)           
  ,L3_NAME               varchar(32, dict)           
  ,L4_NAME               varchar(64, shard_key)
  ,CUST_TYPE             varchar(16, dict)           
  ,CUST_SUBTYPE          varchar(8, dict)            
  ,TRX                   bigint                
  ,REV                   bigint                
  ,DUR                   bigint                
  ,VOL                   bigint                
);
