CREATE OR REPLACE TABLE ${table-prefix}${revenue-schema}.${table-prefix}revenue_summary(
   FILE_DATE             date             
  ,TRX_DATE              date             
  ,BRAND                 varchar(16, dict)           
  ,OFFER_ID              varchar(8, dict)            
  ,AREA_SALES            varchar(8, dict)            
  ,REGION_SALES          varchar(32, dict)           
  ,BRANCH                varchar(64, dict)           
  ,SUBBRANCH             varchar(32, dict)           
  ,CLUSTER               varchar(32, dict)           
  ,PROVINSI              varchar(32, dict)           
  ,KABUPATEN             varchar(32, dict)
  ,KECAMATAN             varchar(32, dict)           
  ,KELURAHAN             varchar(32, shard_key)           
  ,ACTIVATION_CHANNEL_ID varchar(8, dict)            
  ,NODE_TYPE             varchar(8, dict)            
  ,PRE_POST_FLAG         integer(dict)               
  ,L1_NAME               varchar(32, dict)           
  ,L2_NAME               varchar(32, dict)           
  ,L3_NAME               varchar(32, dict)           
  ,L4_NAME               varchar(64, dict)
  ,CUST_TYPE             varchar(16, dict)           
  ,CUST_SUBTYPE          varchar(8, dict)            
  ,VOL                   bigint                
  ,DUR                   bigint                
  ,REV                   bigint                
  ,TRX                   bigint(dict)              
);