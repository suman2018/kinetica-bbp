/* append current batch summary to summary table */
INSERT INTO ${table-prefix}${revenue-schema}.${table-prefix}revenue_summary (
	SELECT
		TIMESTAMP('${run-date=yyyy-MM-dd}') AS FILE_DATE,
		TRX_DATE,
		BRAND,
		OFFER_ID,
		AREA_SALES,
		REGION_SALES,
		BRANCH,
		SUBBRANCH,
		CLUSTER,
		PROVINSI,
		KABUPATEN,
		KECAMATAN,
		KELURAHAN,
		ACTIVATION_CHANNEL_ID,
		NODE_TYPE,
		PRE_POST_FLAG,
		L1_NAME,
		L2_NAME,
		L3_NAME,
		L4_NAME,
		CUST_TYPE,
		CUST_SUBTYPE,
		SUM(VOL) AS VOL,
		SUM(DUR) AS DUR,
		SUM(REV) AS REV,
		SUM(TRX) AS TRX
	FROM ${table-prefix}${revenue-schema}.${table-prefix}chg_rcg_destination_v
	GROUP BY
		TRX_DATE,
		BRAND,
		OFFER_ID,
		AREA_SALES,
		REGION_SALES,
		BRANCH,
		SUBBRANCH,
		CLUSTER,
		PROVINSI,
		KABUPATEN,
		KECAMATAN,
		KELURAHAN,
		ACTIVATION_CHANNEL_ID,
		NODE_TYPE,
		PRE_POST_FLAG,
		L1_NAME,
		L2_NAME,
		L3_NAME,
		L4_NAME,
		CUST_TYPE,
		CUST_SUBTYPE	
	UNION ALL
	SELECT
		TIMESTAMP('${run-date=yyyy-MM-dd}') AS FILE_DATE,
		TRX_DATE,
		BRAND,
		PRICE_PLAN,
		AREA_SALES,
		REGION_SALES,
		BRANCH,
		SUBBRANCH,
		CLUSTER,
		PROVINSI,
		KABUPATEN,
		KECAMATAN,
		KELURAHAN,
		'-99' AS ACTIVATION_CHANNEL_ID,
		NODE_TYPE,
		CASE WHEN UPPER(BRAND) = 'KARTUHALO' THEN 2 ELSE 1 END AS PRE_POST_FLAG,
		L1_NAME,
		L2_NAME,
		L3_NAME,
		L4_NAME,
		CUST_TYPE,
		CUST_SUBTYPE,
		SUM(VOL) AS VOL,
		0 AS DUR,
		0 AS REV,
		SUM(TRX) AS TRX
	FROM ${table-prefix}${revenue-schema}.${table-prefix}upcc_destination_v
	GROUP BY
		TRX_DATE,
		BRAND,
		PRICE_PLAN,
		AREA_SALES,
		REGION_SALES,
		BRANCH,
		SUBBRANCH,
		CLUSTER,
		PROVINSI,
		KABUPATEN,
		KECAMATAN,
		KELURAHAN,
		NODE_TYPE,
		L1_NAME,
		L2_NAME,
		L3_NAME,
		L4_NAME,
		CUST_TYPE,
		CUST_SUBTYPE
);


CREATE OR REPLACE REPLICATED TABLE ${table-prefix}${revenue-schema}.${table-prefix}upcc_destination_post AS (
	SELECT 
		TRX_DATE,
		BRAND,
		PRICE_PLAN,
		AREA_SALES,
		NODE_TYPE,
		L1_NAME,
		L2_NAME,
		L3_NAME,
		L4_NAME,
		CUST_TYPE,
		CUST_SUBTYPE,
		MSISDN_HLR_REF,
		SUM(TRX) AS TRX,
		SUM(VOL) AS VOL
	FROM ${table-prefix}${revenue-schema}.${table-prefix}upcc_destination_v a
	WHERE 
		UPPER(BRAND) = 'KARTUHALO'
	GROUP BY 
		TRX_DATE,
		BRAND,
		PRICE_PLAN,
		AREA_SALES,
		NODE_TYPE,
		L1_NAME,
		L2_NAME,
		L3_NAME,
		L4_NAME,
		CUST_TYPE,
		CUST_SUBTYPE,
		MSISDN_HLR_REF
);

--
--

CREATE OR REPLACE VIEW ${table-prefix}${revenue-schema}.${table-prefix}pre_hlr_summary_staging_v AS (
	SELECT 
		TIMESTAMP('${run-date=yyyy-MM-dd}') AS FILE_DATE,
		TRX_DATE,
		CASE WHEN BRAND = 'kartuHALO' THEN 'simPATI' ELSE BRAND END AS BRAND,
		OFFER_ID,
		AREA_HLR,  
		REGION_HLR,
		CITY_HLR,
		ACTIVATION_CHANNEL_ID,
		NODE_TYPE, 
		PRE_POST_FLAG,
		L1_NAME,
		L2_NAME,
		L3_NAME,
		L4_NAME,
		CUST_TYPE,
		'UNKNOWN' as CUST_SUBTYPE,
		SUM(TRX) TRX,
		SUM(REV) REV,
		SUM(DUR) DUR,
		SUM(VOL) VOL
                --,KI_SHARD_KEY(OFFER_ID, L4_NAME)
	FROM ${table-prefix}${revenue-schema}.${table-prefix}chg_rcg_destination_v
	WHERE PRE_POST_FLAG = '1'
	GROUP BY 
		TRX_DATE,
		--CASE WHEN BRAND = 'kartuHALO' THEN 'simPATI' ELSE BRAND END,
                BRAND,
		OFFER_ID,
		AREA_HLR,
		REGION_HLR,
		CITY_HLR,
		ACTIVATION_CHANNEL_ID,
		NODE_TYPE,
		PRE_POST_FLAG,
		L1_NAME,
		L2_NAME,
		L3_NAME,
		L4_NAME,
		CUST_TYPE
	UNION ALL
	SELECT 
		TIMESTAMP('${run-date=yyyy-MM-dd}') AS FILE_DATE,
		TRX_DATE,
		CASE WHEN BRAND = 'kartuHALO' THEN 'simPATI' ELSE BRAND END AS BRAND,
		PRICE_PLAN AS OFFER_ID,
		CASE
		WHEN COALESCE(b.REGIONAL, c.REGIONAL) IN ('SUMBAGUT', 'SUMBAGTENG', 'SUMBAGSEL') THEN 'AREA 1'
		WHEN COALESCE(b.REGIONAL, c.REGIONAL) IN ('JABOTABEK', 'JABAR') THEN 'AREA 2'
		WHEN COALESCE(b.REGIONAL, c.REGIONAL) IN ('JATENG', 'JATIM', 'BALINUSRA') THEN 'AREA 3'
		WHEN COALESCE(b.REGIONAL, c.REGIONAL) IN ('KALIMANTAN', 'SULAWESI', 'PUMA') THEN 'AREA 4'
		END AS AREA_HLR, 
		COALESCE(b.REGIONAL, c.REGIONAL) AS REGION_HLR,
		COALESCE(b.CITY, c.CITY) AS CITY_HLR,
		'-99' AS ACTIVATION_CHANNEL_ID,
		NODE_TYPE,
		CASE WHEN UPPER(BRAND) = 'KARTUHALO' THEN 2 ELSE 1 END AS PRE_POST_FLAG,
		L1_NAME,
		L2_NAME,
		L3_NAME,
		L4_NAME,
		CUST_TYPE,
		CUST_SUBTYPE,
		SUM(TRX) AS TRX,
		0 REV,
		0 DUR,
		SUM(VOL) AS VOL
                --,KI_SHARD_KEY(PRICE_PLAN, L4_NAME)
	FROM ${table-prefix}${revenue-schema}.${table-prefix}upcc_destination_v a
	LEFT JOIN ${table-prefix}${reference-schema}.${table-prefix}hlr_ref_${run-date=yyyyMMdd} b
		ON a.MSISDN_HLR_REF = b.PREFIX
	LEFT JOIN ${table-prefix}${reference-schema}.${table-prefix}hlr_ref_${run-date=yyyyMMdd} c
		ON (SUBSTRING(a.MSISDN_HLR_REF, 1, 6) = c.PREFIX)
	WHERE 
		upper(BRAND) <> 'KARTUHALO'
	GROUP BY 
		TRX_DATE,
		--CASE WHEN BRAND = 'kartuHALO' THEN 'simPATI' ELSE BRAND END,
                BRAND,
		PRICE_PLAN,
		COALESCE(b.REGIONAL, c.REGIONAL),
		COALESCE(b.CITY, c.CITY),
		NODE_TYPE,
                --CASE WHEN UPPER(BRAND) = 'KARTUHALO' THEN 2 ELSE 1 END,
                PRE_POST_FLAG,
		L1_NAME,
		L2_NAME,
		L3_NAME,
		L4_NAME,
		CUST_TYPE,
		CUST_SUBTYPE
);

INSERT INTO ${table-prefix}${revenue-schema}.${table-prefix}revenue_pre_hlr_summary (SELECT * FROM ${table-prefix}${revenue-schema}.${table-prefix}pre_hlr_summary_staging_v);


CREATE OR REPLACE VIEW ${table-prefix}${revenue-schema}.${table-prefix}post_hlr_summary_staging_v AS (
	SELECT 
		TIMESTAMP('${run-date=yyyy-MM-dd}') AS FILE_DATE,
		TRX_DATE,
		BRAND,
		OFFER_ID,
		AREA_HLR,
		REGION_HLR,
		CITY_HLR,
		ACTIVATION_CHANNEL_ID,
		NODE_TYPE, 
		PRE_POST_FLAG,
		L1_NAME,
		L2_NAME,
		L3_NAME,
		L4_NAME,
		CUST_TYPE,
		'UNKNOWN' as CUST_SUBTYPE,
		SUM(TRX) TRX,
		SUM(REV) REV,
		SUM(DUR) DUR,
		SUM(VOL) VOL
	FROM ${table-prefix}${revenue-schema}.${table-prefix}chg_rcg_destination_v
	WHERE PRE_POST_FLAG = '2' AND UPPER(BRAND) = 'KARTUHALO'
	GROUP BY 
		TRX_DATE,
		BRAND,
		OFFER_ID,
		REGION_HLR,
		CITY_HLR,
		AREA_HLR,
		ACTIVATION_CHANNEL_ID,
		NODE_TYPE,
		PRE_POST_FLAG,
		L1_NAME,
		L2_NAME,
		L3_NAME,
		L4_NAME,
		CUST_TYPE
	UNION ALL
	SELECT 
		TIMESTAMP('${run-date=yyyy-MM-dd}') AS FILE_DATE,
		TRX_DATE,
		BRAND,
		PRICE_PLAN AS OFFER_ID,
		CASE
		WHEN COALESCE(b.REGIONAL, c.REGIONAL) IN ('SUMBAGUT', 'SUMBAGTENG', 'SUMBAGSEL') THEN 'AREA 1'
		WHEN COALESCE(b.REGIONAL, c.REGIONAL) IN ('JABOTABEK', 'JABAR') THEN 'AREA 2'
		WHEN COALESCE(b.REGIONAL, c.REGIONAL) IN ('JATENG', 'JATIM', 'BALINUSRA') THEN 'AREA 3'
		WHEN COALESCE(b.REGIONAL, c.REGIONAL) IN ('KALIMANTAN', 'SULAWESI', 'PUMA') THEN 'AREA 4'
		END AS AREA_HLR,
		COALESCE(b.REGIONAL, c.REGIONAL) AS REGION_HLR,
		COALESCE(b.CITY, c.CITY) AS CITY_HLR,
		'-99' AS ACTIVATION_CHANNEL_ID,
		NODE_TYPE,
		CASE WHEN UPPER(BRAND) = 'KARTUHALO' THEN 2 ELSE 1 END AS PRE_POST_FLAG,
		L1_NAME,
		L2_NAME,
		L3_NAME,
		L4_NAME,
		CUST_TYPE,
		CUST_SUBTYPE,
		SUM(TRX) AS TRX,
		0 REV,
		0 DUR,
		SUM(VOL) AS VOL
	FROM ${table-prefix}${revenue-schema}.${table-prefix}upcc_destination_post a
	LEFT JOIN ${table-prefix}${reference-schema}.${table-prefix}hlr_ref_${run-date=yyyyMMdd} b
		ON a.MSISDN_HLR_REF = b.PREFIX
	LEFT JOIN ${table-prefix}${reference-schema}.${table-prefix}hlr_ref_${run-date=yyyyMMdd} c
		ON (SUBSTRING(a.MSISDN_HLR_REF, 1, 6) = c.PREFIX)
	WHERE 
		upper(BRAND) = 'KARTUHALO'
	GROUP BY 
		TRX_DATE,
		BRAND,
		PRICE_PLAN,
		COALESCE(b.REGIONAL, c.REGIONAL),
		COALESCE(b.CITY, c.CITY),
		AREA_SALES,
		NODE_TYPE,
		L1_NAME,
		L2_NAME,
		L3_NAME,
		L4_NAME,
		CUST_TYPE,
		CUST_SUBTYPE
);

INSERT INTO ${table-prefix}${revenue-schema}.${table-prefix}revenue_post_hlr_summary (SELECT * FROM ${table-prefix}${revenue-schema}.${table-prefix}post_hlr_summary_staging_v);


/* append to high-level tables */
INSERT INTO ${table-prefix}${uc1_history-schema}.${table-prefix}revenue_pos_cust_type_osdss (
SELECT
	TRX_DATE, CUST_TYPE, CUST_SUBTYPE, L1_NAME, L2_NAME, L3_NAME, AREA_HLR, CITY_HLR, 
	sum(REV), sum(TRX), sum(DUR) + sum(VOL), TIMESTAMP('${run-date=yyyy-MM-dd}') AS FILE_DATE
FROM 
	${table-prefix}${revenue-schema}.${table-prefix}post_hlr_summary_staging_v
GROUP BY
	TRX_DATE, CUST_TYPE, CUST_SUBTYPE, L1_NAME, L2_NAME, L3_NAME, AREA_HLR, CITY_HLR
);

INSERT INTO ${table-prefix}${uc1_history-schema}.${table-prefix}revenue_postpaid_hlr (
	SELECT
		TRX_DATE, AREA_HLR, REGION_HLR, CITY_HLR, BRAND, L1_NAME, L2_NAME, L3_NAME, 
		sum(TRX), sum(DUR) + sum(VOL), sum(REV), TIMESTAMP('${run-date=yyyy-MM-dd}') AS FILE_DATE
	FROM 
		${table-prefix}${revenue-schema}.${table-prefix}post_hlr_summary_staging_v
	WHERE PRE_POST_FLAG = '2'
	GROUP BY
		TRX_DATE, AREA_HLR, REGION_HLR, CITY_HLR, BRAND, L1_NAME, L2_NAME, L3_NAME
);


INSERT INTO ${table-prefix}${uc1_history-schema}.${table-prefix}revenue_cluster_priceplan (
	SELECT
		revenue_date, area, region, cluster, product_name, priceplan, L1, L2, L3, node, 
		sum(total_transaction) AS total_transaction, sum(total_duration) AS total_duration, sum(total_revenue) AS total_revenue, TIMESTAMP('${run-date=yyyy-MM-dd}') AS file_date
	FROM
		(
			SELECT
				rs.trx_date AS revenue_date, 
				rs.area_sales AS area, 
				rs.region_sales AS region,
				rs.cluster AS cluster,
				CASE WHEN rs.brand = 'kartuHALO' THEN 'HYBRID' ELSE rs.brand END AS product_name,
				od.offer AS priceplan,
				rs.l1_name AS L1,
				rs.l2_name AS L2,
				rs.l3_name AS L3,
				rs.node_type AS node,
				rs.trx AS total_transaction,
				rs.dur + rs.vol AS total_duration,
				rs.rev AS total_revenue
			FROM
				${table-prefix}${revenue-schema}.${table-prefix}revenue_summary rs
				LEFT JOIN
				${table-prefix}${reference-schema}.${table-prefix}offer_dim_ref_${run-date=yyyyMMdd} od
				ON rs.offer_id = od.offer_id
				WHERE rs.file_date = DATE('${run-date=yyyy-MM-dd}')
				AND rs.pre_post_flag=1
				AND rs.brand <> 'UNKNOWN'
		)
	GROUP BY
		revenue_date, area, region, cluster, product_name, priceplan, L1, L2, L3, node
);

INSERT INTO ${table-prefix}${uc1_history-schema}.${table-prefix}revenue_prepaid_hlr
SELECT
    TRX_DATE
    ,AREA_HLR
    ,REGION_HLR
    ,CITY_HLR
    ,BRAND
    ,L1_NAME
    ,L2_NAME
    ,L3_NAME
    ,SUM(TRX)
    ,SUM(DUR)+SUM(VOL)
    ,SUM(REV)
    ,FILE_DATE
FROM ${table-prefix}${revenue-schema}.${table-prefix}revenue_pre_hlr_summary
WHERE FILE_DATE = DATE('${run-date=yyyy-MM-dd}')
GROUP BY     FILE_DATE
    ,TRX_DATE
    ,AREA_HLR
    ,REGION_HLR
    ,CITY_HLR
    ,BRAND
    ,L1_NAME
    ,L2_NAME
    ,L3_NAME
    ,FILE_DATE;

DROP VIEW ${table-prefix}${revenue-schema}.${table-prefix}chg_rcg_destination_v;
DROP VIEW ${table-prefix}${revenue-schema}.${table-prefix}upcc_destination_v;
DROP TABLE ${table-prefix}${revenue-schema}.${table-prefix}upcc_destination_post;

DROP TABLE IF EXISTS ${table-prefix}chg_rcg_staging_${run-date=yyyyMMdd};
ALTER TABLE ${table-prefix}${revenue-schema}.${table-prefix}chg_rcg_staging RENAME TO ${table-prefix}chg_rcg_staging_${run-date=yyyyMMdd};
-- move this housekeeping into hvc rolling pipeline 
-- DROP TABLE IF EXISTS ${table-prefix}chg_rcg_staging_${retention-date=yyyyMMdd};

DROP TABLE IF EXISTS ${table-prefix}upcc_staging_${run-date=yyyyMMdd};
ALTER TABLE ${table-prefix}${revenue-schema}.${table-prefix}upcc_staging RENAME TO ${table-prefix}upcc_staging_${run-date=yyyyMMdd};
DROP TABLE IF EXISTS ${table-prefix}upcc_staging_${retention-date=yyyyMMdd};
    
-- DROP TABLE ${table-prefix}${revenue-schema}.${table-prefix}upcc_destination_post;
-- DROP VIEW ${table-prefix}${revenue-schema}.${table-prefix}chg_rcg_destination_v;
-- DROP VIEW ${table-prefix}${revenue-schema}.${table-prefix}upcc_destination_v;
