DELETE FROM ${table-prefix}${revenue-schema}.${table-prefix}revenue_post_hlr_summary WHERE file_date = DATE('${run-date=yyyy-MM-dd}');
DELETE FROM ${table-prefix}${revenue-schema}.${table-prefix}revenue_pre_hlr_summary WHERE file_date = DATE('${run-date=yyyy-MM-dd}');
DELETE FROM ${table-prefix}${revenue-schema}.${table-prefix}revenue_summary WHERE file_date = DATE('${run-date=yyyy-MM-dd}');

/* reprocessing for high-level (history) tables */
DELETE FROM ${table-prefix}${uc1_history-schema}.${table-prefix}revenue_pos_cust_type_osdss WHERE file_date = DATE('${run-date=yyyy-MM-dd}');
DELETE FROM ${table-prefix}${uc1_history-schema}.${table-prefix}revenue_postpaid_hlr WHERE file_date = DATE('${run-date=yyyy-MM-dd}');
DELETE FROM ${table-prefix}${uc1_history-schema}.${table-prefix}revenue_prepaid_hlr WHERE file_date = DATE('${run-date=yyyy-MM-dd}');
DELETE FROM ${table-prefix}${uc1_history-schema}.${table-prefix}revenue_cluster_priceplan WHERE file_date = DATE('${run-date=yyyy-MM-dd}');
