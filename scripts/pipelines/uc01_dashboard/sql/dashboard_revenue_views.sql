CREATE OR REPLACE MATERIALIZED VIEW ${table-prefix}${revenue-schema}.${table-prefix}db_revenue_summary_v  REFRESH EVERY 24 HOUR AS ( 
select 
	a.FILE_DATE AS FILE_DATE,
	a.TRX_DATE AS TRX_DATE,
	a.BRAND AS BRAND,
	a.OFFER_ID AS OFFER_ID,
	a.AREA_SALES AS AREA_SALES,
	a.REGION_SALES AS REGION_SALES,
	a.BRANCH AS BRANCH,
	a.SUBBRANCH AS SUBBRANCH,
	a.CLUSTER AS CLUSTER,
	a.PROVINSI AS PROVINSI,
	a.KABUPATEN AS KABUPATEN,
	a.KECAMATAN AS KECAMATAN,
	a.KELURAHAN AS KELURAHAN,
	a.ACTIVATION_CHANNEL_ID ACTIVATION_CHANNEL_ID,
	b.act_chnnl_nme AS ACT_CHNNL_NME,
	a.NODE_TYPE AS NODE_TYPE,
	a.PRE_POST_FLAG AS PRE_POST_FLAG,
	a.L1_NAME AS L1_NAME,
	a.L2_NAME AS L2_NAME,
	a.L3_NAME AS L3_NAME,
	a.L4_NAME AS L4_NAME,
	a.CUST_TYPE AS CUST_TYPE,
	a.CUST_SUBTYPE AS CUST_SUBTYPE,
	a.VOL AS VOL,
	a.DUR AS DUR,
	a.REV AS REV,
	a.TRX AS TRX
from 
	${table-prefix}${revenue-schema}.${table-prefix}revenue_summary a 
left join
	${table-prefix}${reference-schema}.${table-prefix}channel_ref b
on
	a.ACTIVATION_CHANNEL_ID=b.act_chnnl_id
);

CREATE OR REPLACE MATERIALIZED VIEW ${table-prefix}${revenue-schema}.${table-prefix}db_revenue_pre_hlr_summary_v  REFRESH EVERY 24 HOUR AS ( 
select 
	a.FILE_DATE AS FILE_DATE,
	a.TRX_DATE AS TRX_DATE,
	a.BRAND AS BRAND,
	a.OFFER_ID AS OFFER_ID,
	a.AREA_HLR AS AREA_HLR,
	a.REGION_HLR AS REGION_HLR,
	a.CITY_HLR AS CITY_HLR,
	a.ACTIVATION_CHANNEL_ID ACTIVATION_CHANNEL_ID,
	b.act_chnnl_nme AS ACT_CHNNL_NME,
	a.NODE_TYPE AS NODE_TYPE,
	a.PRE_POST_FLAG AS PRE_POST_FLAG,
	a.L1_NAME AS L1_NAME,
	a.L2_NAME AS L2_NAME,
	a.L3_NAME AS L3_NAME,
	a.L4_NAME AS L4_NAME,
	a.CUST_TYPE AS CUST_TYPE,
	a.CUST_SUBTYPE AS CUST_SUBTYPE,
	a.TRX AS TRX,
	a.REV AS REV,
	a.DUR AS DUR,
	a.VOL AS VOL
FROM 
	${table-prefix}${revenue-schema}.${table-prefix}revenue_pre_hlr_summary a 
left join
	${table-prefix}${reference-schema}.${table-prefix}channel_ref b
on
	a.ACTIVATION_CHANNEL_ID=b.act_chnnl_id
);

CREATE OR REPLACE MATERIALIZED VIEW ${table-prefix}${revenue-schema}.${table-prefix}db_revenue_post_hlr_summary_v  REFRESH EVERY 24 HOUR AS ( 
select 
	a.FILE_DATE AS FILE_DATE,
	a.TRX_DATE AS TRX_DATE,
	a.BRAND AS BRAND,
	a.OFFER_ID AS OFFER_ID,
	a.AREA_HLR AS AREA_HLR,
	a.REGION_HLR AS REGION_HLR,
	a.CITY_HLR AS CITY_HLR,
	a.ACTIVATION_CHANNEL_ID ACTIVATION_CHANNEL_ID,
	b.act_chnnl_nme AS ACT_CHNNL_NME,
	a.NODE_TYPE AS NODE_TYPE,
	a.PRE_POST_FLAG AS PRE_POST_FLAG,
	a.L1_NAME AS L1_NAME,
	a.L2_NAME AS L2_NAME,
	a.L3_NAME AS L3_NAME,
	a.L4_NAME AS L4_NAME,
	a.CUST_TYPE AS CUST_TYPE,
	a.CUST_SUBTYPE AS CUST_SUBTYPE,
	a.TRX AS TRX,
	a.REV AS REV,
	a.DUR AS DUR,
	a.VOL AS VOL
from 
	${table-prefix}${revenue-schema}.${table-prefix}revenue_post_hlr_summary a 
left join
	${table-prefix}${reference-schema}.${table-prefix}channel_ref b
on 
	a.ACTIVATION_CHANNEL_ID=b.act_chnnl_id
);

