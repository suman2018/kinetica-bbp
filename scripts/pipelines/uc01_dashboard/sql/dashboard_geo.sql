CREATE OR REPLACE MATERIALIZED VIEW ${table-prefix}${dashboard-schema}.${table-prefix}db_mkios_geo  REFRESH EVERY 3 HOUR AS ( 
select
	mkios.*, lac.lngtd, lac.lttde, lac.VNDR AS vendor, lac.RGN_NTWRK AS REGION, lac.KABUPATEN AS KABU, lac.KECAMATAN AS KECA, lac.KELURAHAN AS KELU, lac.REGIONAL_CHNNL AS REG_CHANNEL, lac.BRNCH AS BRANCH, lac.SB_BRNCH AS SUBBRANCH, lac.CLSTR_SLS AS CLUSTER, lac.AREA_NME AS AREA  
from
	${table-prefix}${recharge-schema}.${table-prefix}mkios_staging_${run-date=yyyyMMdd} mkios
join 
	${table-prefix}${reference-schema}.${table-prefix}laccima_ref_${run-date=yyyyMMdd} lac
on
	lac.lacci_id = mkios.subs_lacci_id
);

CREATE OR REPLACE MATERIALIZED VIEW ${table-prefix}${dashboard-schema}.${table-prefix}db_recharge_cluster_bnumber_agg  REFRESH EVERY 3 HOUR AS (
	select 
		max(area) as area, max(region) as region, cluster_name as cluster, 
		sum(voucher_denomination) as sum_voucher,
		sum(total_transaction) as sum_transaction,
		sum(total_recharge) as sum_recharge
	from 
		${table-prefix}${uc1_history-schema}.${table-prefix}recharge_cluster_bnumber
	group by
		cluster_name
);


CREATE OR REPLACE MATERIALIZED VIEW ${table-prefix}${dashboard-schema}.${table-prefix}db_recharge_cluster_bnumber_geo  REFRESH EVERY 3 HOUR AS (
select
	r.cluster, r.sum_voucher, r.sum_transaction, r.sum_recharge, 
	case
		when (r.sum_recharge < 800000000000) then 'low'
		when (r.sum_recharge < 1155000000000) then 'medium'
		when (r.sum_recharge >= 1155000000000) then 'high'
	end as recharge_cat,
	c.subbranch, c.branch, c.region, c.area, c.wkt
from 
	${table-prefix}${dashboard-schema}.${table-prefix}db_recharge_cluster_bnumber_agg r
join 
	cluster c
on
	r.cluster = c.cluster
);