/* Summary */
CREATE OR REPLACE MATERIALIZED VIEW ${table-prefix}${dashboard-schema}.${table-prefix}tmp_db_summary_input REFRESH EVERY 3 HOUR AS (
SELECT 
	'Revenue' as domain,
	CASE length(char4(month(revenue_date))) WHEN 1 THEN (char16((char4(year(revenue_date))||'-0'||char4(month(revenue_date))))) ELSE (char16((char4(year(revenue_date))||'-'||char4(month(revenue_date))))) END AS date_month,
	revenue_date as date, area, region, total_revenue as total
FROM 
	${table-prefix}${uc1_history-schema}.${table-prefix}revenue_cluster_priceplan
UNION ALL
SELECT 
	'Recharge' as domain,
	CASE length(char4(month(recharge_date))) WHEN 1 THEN (char16((char4(year(recharge_date))||'-0'||char4(month(recharge_date))))) ELSE (char16((char4(year(recharge_date))||'-'||char4(month(recharge_date))))) END AS date_month,
	recharge_date as date, area, region, total_recharge as total
FROM 
	${table-prefix}${uc1_history-schema}.${table-prefix}recharge_cluster_bnumber
UNION ALL
SELECT 
	'Sales' as domain,
	CASE length(char4(month(SCN_DATE))) WHEN 1 THEN (char16((char4(year(SCN_DATE))||'-0'||char4(month(SCN_DATE))))) ELSE (char16((char4(year(SCN_DATE))||'-'||char4(month(SCN_DATE))))) END AS date_month,
	SCN_DATE as date, area, region_name as region, SALES as total
FROM 
	${table-prefix}${uc1_history-schema}.${table-prefix}scn_cluster_osdss
UNION ALL
SELECT 
	'Churn' as domain,
	CASE length(char4(month(SCN_DATE))) WHEN 1 THEN (char16((char4(year(SCN_DATE))||'-0'||char4(month(SCN_DATE))))) ELSE (char16((char4(year(SCN_DATE))||'-'||char4(month(SCN_DATE))))) END AS date_month,
	SCN_DATE as date, area, region_name as region, CHURN as total
FROM 
	${table-prefix}${uc1_history-schema}.${table-prefix}scn_cluster_osdss
UNION ALL
SELECT 
	'Netadd' as domain,
	CASE length(char4(month(SCN_DATE))) WHEN 1 THEN (char16((char4(year(SCN_DATE))||'-0'||char4(month(SCN_DATE))))) ELSE (char16((char4(year(SCN_DATE))||'-'||char4(month(SCN_DATE))))) END AS date_month,
	SCN_DATE as date, area, region_name as region, NETADD as total
FROM 
	${table-prefix}${uc1_history-schema}.${table-prefix}scn_cluster_osdss);

CREATE OR REPLACE REPLICATED TABLE ${table-prefix}${dashboard-schema}.${table-prefix}region_id_lookup
(
    "region_id" int NOT NULL,
    "region" VARCHAR(32, dict) NOT NULL
);
INSERT INTO ${table-prefix}${dashboard-schema}.${table-prefix}region_id_lookup VALUES (1, 'SUMBAGUT');
INSERT INTO ${table-prefix}${dashboard-schema}.${table-prefix}region_id_lookup VALUES (2, 'SUMBAGTENG'); 
INSERT INTO ${table-prefix}${dashboard-schema}.${table-prefix}region_id_lookup VALUES (3, 'SUMBAGSEL');
INSERT INTO ${table-prefix}${dashboard-schema}.${table-prefix}region_id_lookup VALUES (4, 'WESTERN JABOTABEK'); 
INSERT INTO ${table-prefix}${dashboard-schema}.${table-prefix}region_id_lookup VALUES (5, 'CENTRAL JABOTABEK');
INSERT INTO ${table-prefix}${dashboard-schema}.${table-prefix}region_id_lookup VALUES (6, 'EASTERN JABOTABEK');
INSERT INTO ${table-prefix}${dashboard-schema}.${table-prefix}region_id_lookup VALUES (7, 'SAD REGIONAL');
INSERT INTO ${table-prefix}${dashboard-schema}.${table-prefix}region_id_lookup VALUES (8, 'JABAR');
INSERT INTO ${table-prefix}${dashboard-schema}.${table-prefix}region_id_lookup VALUES (9, 'JATENG-DIY'); 
INSERT INTO ${table-prefix}${dashboard-schema}.${table-prefix}region_id_lookup VALUES (10, 'JATIM');
INSERT INTO ${table-prefix}${dashboard-schema}.${table-prefix}region_id_lookup VALUES (11, 'BALI NUSRA');
INSERT INTO ${table-prefix}${dashboard-schema}.${table-prefix}region_id_lookup VALUES (12, 'KALIMANTAN');
INSERT INTO ${table-prefix}${dashboard-schema}.${table-prefix}region_id_lookup VALUES (13, 'SULAWESI');
INSERT INTO ${table-prefix}${dashboard-schema}.${table-prefix}region_id_lookup VALUES (14, 'PUMA');
INSERT INTO ${table-prefix}${dashboard-schema}.${table-prefix}region_id_lookup VALUES (15, 'OTHER');

CREATE OR REPLACE MATERIALIZED VIEW ${table-prefix}${dashboard-schema}.${table-prefix}db_summary REFRESH EVERY 3 HOUR  AS (
	SELECT 
		s.domain, s.date_month, s.date, s.area, s.region, l.region_id, sum(s.total) as total
	FROM 
		${table-prefix}${dashboard-schema}.${table-prefix}tmp_db_summary_input s
	INNER JOIN ${table-prefix}${dashboard-schema}.${table-prefix}region_id_lookup l
	ON s.region = l.region
	GROUP BY
		domain, date_month, date, area, l.region_id, s.region
);


/* L1, L2, L3, Revenue */
CREATE OR REPLACE MATERIALIZED VIEW ${table-prefix}${dashboard-schema}.${table-prefix}db_revenue_cluster_priceplan REFRESH EVERY 3 HOUR AS
SELECT 
	CASE length(char4(month(revenue_date))) WHEN 1 THEN (char16((char4(year(revenue_date))||'-0'||char4(month(revenue_date))))) ELSE (char16((char4(year(revenue_date))||'-'||char4(month(revenue_date))))) END AS date_month,
	*
FROM 
	${table-prefix}${uc1_history-schema}.${table-prefix}revenue_cluster_priceplan;

alter schema ${table-prefix}${dashboard-schema} set protected true;
