create or replace materialized view ${table-prefix}${dashboard-schema}.${table-prefix}db_scn_cluster_osdss REFRESH EVERY 3 HOUR AS 
(
select
SCN_DATE
,BRAND_NAME
,area
,region_name
,CLUSTER_NAME
,priceplan
,SALES
,CHURN
,NETADD
,file_date
from ${table-prefix}${uc1_history-schema}.${table-prefix}scn_cluster_osdss
);

create or replace materialized view ${table-prefix}${dashboard-schema}.${table-prefix}db_subscriber_cluster_osdss REFRESH EVERY 3 HOUR AS 
(
select
SCNDATE
,BRANDNAME
,AREA
,REGIONNAME
,CLUSTERNAME
,PPNAME
,SUBSCRIBER
,file_date
from ${table-prefix}${uc1_history-schema}.${table-prefix}subscriber_cluster_osdss
);