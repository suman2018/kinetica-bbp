CREATE OR REPLACE MATERIALIZED VIEW ${table-prefix}${dashboard-schema}.${table-prefix}db_recharge_cluster_bnumber  REFRESH EVERY 3 HOUR AS (
SELECT
	CASE length(char4(month(recharge_date))) WHEN 1 THEN (char16((char4(year(recharge_date))||'-0'||char4(month(recharge_date))))) ELSE (char16((char4(year(recharge_date))||'-'||char4(month(recharge_date))))) END AS year_month,
	DATE('2000-' || '01-' || CHAR4(CASE length(char4(day(recharge_date))) WHEN 1 THEN ('0'||char4(day(recharge_date))) ELSE (char4(day(recharge_date))) END)) AS day_of_month, 
	recharge_date as date, area, region, total_recharge as total
FROM 
	${table-prefix}${uc1_history-schema}.${table-prefix}recharge_cluster_bnumber)
	;

CREATE OR REPLACE MATERIALIZED VIEW ${table-prefix}${dashboard-schema}.${table-prefix}db_recharge_cluster_anumber  REFRESH EVERY 3 HOUR AS (
SELECT
	CASE length(char4(month(recharge_date))) WHEN 1 THEN (char16((char4(year(recharge_date))||'-0'||char4(month(recharge_date))))) ELSE (char16((char4(year(recharge_date))||'-'||char4(month(recharge_date))))) END AS year_month,
	DATE('2000-' || '01-' || CHAR4(CASE length(char4(day(recharge_date))) WHEN 1 THEN ('0'||char4(day(recharge_date))) ELSE (char4(day(recharge_date))) END)) AS day_of_month, 
	recharge_date as date, area, region, cluster_name as cluster, product, recharge_channel as channel, trans_type, trans_channel, total_recharge as total
FROM 
	${table-prefix}${uc1_history-schema}.${table-prefix}recharge_cluster_anumber)
	;
