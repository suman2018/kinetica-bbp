--From detail
select SUM(unique_taker) as unique_taker
from
(
select periode, 
count(distinct campaign_id) cid, sum(target) target, sum(eligible) eligible, sum(taker) taker
from 
(
select last_day(a.trx_date) as periode, campaign_id,
	count(distinct a.msisdn) as target,
	count(distinct case when a.iscontrolgroup = 'false' and a.eligible > 0 then a.msisdn end) as eligible,
	sum
		(
	    	case
	    		when a.iscontrolgroup = 'false' and a.campaign_group_id = 2 and a.taker > 0 then a.metric_2
	    		when a.iscontrolgroup = 'false' and a.campaign_group_id <> 2 and a.taker > 0 then a.taker
	    		else 0 
	    	end
	    ) as taker,
	sum(case when a.iscontrolgroup = 'false' and taker > 0 then a.metric_1 else 0 end) as revenue
from ${table-prefix}${cms-schema}.${table-prefix}cms_campaign_tracking_detail a 
where campaign_group_id not in (11,12)
and 
(
	trx_date between date(last_day(date('${run-date=yyyy-MM-dd}') - interval 2 month) + interval 1 day) and last_day(date('${run-date=yyyy-MM-dd}') - interval 1 month)
)
group by 1,2
) x  group by 1
) a 
left join 
(
select last_day(a.trx_date) as periode, 
count(distinct case when a.iscontrolgroup = 'false' and a.taker > 0 then a.msisdn end) as unique_taker
from ${table-prefix}${cms-schema}.${table-prefix}cms_campaign_tracking_detail a 
where campaign_group_id not in (11,12)
and 
(
	trx_date between date(last_day(date('${run-date=yyyy-MM-dd}') - interval 2 month) + interval 1 day) and last_day(date('${run-date=yyyy-MM-dd}') - interval 1 month)
)
group by 1
) b 
on a.periode = b.periode;

--from sum table
select
SUM(unique_taker) as unique_taker
from ${table-prefix}${cms_out-schema}.${table-prefix}cms_performance_summary_v a 
where periode = last_day(date('${run-date=yyyy-MM-dd}') - interval 1 month)
and eom = 1 
and brand = 'Total'
and campaign_category = 'Overall';