select
    count(distinct a.campaign_id) as cnt_cmpid
from
(
    select 
        ci.campid       as campaign_id
        ,min(datetime(ci.starton    ))  as start_period
        ,min(datetime(ci.endon      ))  as end_period
        ,min(ci.name       )  as campaign_name
        ,min(ci.longdesc   )  as segment
    from ${table-prefix}${cms_stg-schema}.${table-prefix}cms_definition_identification_input ci
    where upper(substring(ci.campid, 11, 1)) in ('A', 'B', 'C', 'D', 'E', 'F')
    	and abs(datediff(date(datetime(ci.starton)), date(datetime(ci.endon)))) <= 30
	group by 1
) a
left join
(
	select 
	    campid as cmp_id,
        sender,
        wording
    from ${table-prefix}${cms_stg-schema}.${table-prefix}cms_definition_contact_sender_input
) c on a.campaign_id = c.cmp_id
left join
(
	select
		 prop.campid
		,prop.owner
		,prop.productgroup
		,obj.campaign_group_id as campaign_group_id
		,obj.campaign_objective_id as campaign_objective_id
		,prop.typ
	from ${table-prefix}${cms_stg-schema}.${table-prefix}cms_definition_property_input prop
	join ${table-prefix}${cms-schema}.${table-prefix}cms_campaign_objective obj
	on upper(prop.objective) = upper(obj.campaign_objective_desc)
) e on a.campaign_id = e.campid
;

select count(DISTINCT campaign_id) as cnt_cmpid
from ${table-prefix}${cms-schema}.${table-prefix}cms_campaign_definition;