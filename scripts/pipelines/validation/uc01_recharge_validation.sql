SELECT SUM (RECH) FROM
(
SELECT SUM(RECH) AS RECH FROM ${table-prefix}${recharge-schema}.${table-prefix}MKIOS_STAGING_${run-date=yyyyMMdd}
UNION ALL
SELECT SUM(RECH) AS RECH FROM ${table-prefix}${recharge-schema}.${table-prefix}URP_STAGING_${run-date=yyyyMMdd}
);

SELECT SUM(SUM_RECH) FROM ${table-prefix}${recharge-schema}.${table-prefix}RECHARGE_SUMMARY WHERE FILE_DATE = '${run-date=yyyy-MM-dd}';