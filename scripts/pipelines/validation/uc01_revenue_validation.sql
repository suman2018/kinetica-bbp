SELECT SUM (TRX) FROM
(
SELECT SUM(TRX) AS TRX FROM ${table-prefix}${revenue-schema}.${table-prefix}CHG_RCG_STAGING_${run-date=yyyyMMdd}
UNION ALL
SELECT SUM(TRX) AS TRX FROM ${table-prefix}${revenue-schema}.${table-prefix}UPCC_STAGING_${run-date=yyyyMMdd}
);

SELECT SUM(TRX) FROM ${table-prefix}${revenue-schema}.${table-prefix}REVENUE_SUMMARY WHERE FILE_DATE = '${run-date=yyyy-MM-dd}';