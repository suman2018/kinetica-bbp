SELECT COUNT(DISTINCT MSISDN) FROM ${table-prefix}${scn-schema}.${table-prefix}CB_POST_STAGING_${run-date=yyyyMMdd};
SELECT SUM(CNT) FROM ${table-prefix}${scn-schema}.${table-prefix}CB_POST_SUMMARY WHERE FILE_DATE = '${run-date=yyyy-MM-dd}';

SELECT COUNT(DISTINCT MSISDN) FROM ${table-prefix}${scn-schema}.${table-prefix}CB_PRE_STAGING_${run-date=yyyyMMdd};
SELECT SUM(CNT) FROM ${table-prefix}${scn-schema}.${table-prefix}CB_PRE_SUMMARY WHERE FILE_DATE = '${run-date=yyyy-MM-dd}';

SELECT COUNT(DISTINCT MSISDN) FROM ${table-prefix}${scn-schema}.${table-prefix}CHURN_POST_STAGING_${run-date=yyyyMMdd};
SELECT SUM(CNT) FROM ${table-prefix}${scn-schema}.${table-prefix}CHURN_POST_SUMMARY WHERE FILE_DATE = '${run-date=yyyy-MM-dd}';

SELECT COUNT(DISTINCT MSISDN) FROM ${table-prefix}${scn-schema}.${table-prefix}CHURN_PRE_STAGING_${run-date=yyyyMMdd};
SELECT SUM(CNT) FROM ${table-prefix}${scn-schema}.${table-prefix}CHURN_PRE_SUMMARY WHERE FILE_DATE = '${run-date=yyyy-MM-dd}';

SELECT COUNT(DISTINCT MSISDN) FROM ${table-prefix}${scn-schema}.${table-prefix}SALES_POST_STAGING_${run-date=yyyyMMdd};
SELECT SUM(CNT) FROM ${table-prefix}${scn-schema}.${table-prefix}SALES_POST_SUMMARY WHERE FILE_DATE = '${run-date=yyyy-MM-dd}';

SELECT COUNT(DISTINCT MSISDN) FROM ${table-prefix}${scn-schema}.${table-prefix}SALES_PRE_STAGING_${run-date=yyyyMMdd};
SELECT SUM(CNT) FROM ${table-prefix}${scn-schema}.${table-prefix}SALES_PRE_SUMMARY WHERE FILE_DATE = '${run-date=yyyy-MM-dd}';