--------------------------------------------------------------------------------------------------
-- Staging Table
select 
    COUNT(DISTINCT a.MSISDN)
from 
    (select 
        msisdn 
     from 
        ${table-prefix}${hvc-schema}.${table-prefix}hvc_summary_cb 
     where 
        cut_off_report = '${run-date=yyyy-MM-dd}' and brnd_nme = 'kartuHALO'
    ) a , 
    (select 
        msisdn, inv_month,  max(bill_cycle) bill_cycle, sum(tot_bill_amount) as tot_bill_amount 
     from ${table-prefix}${hvc-schema}.${table-prefix}hvc_postpaid_cb_rev_stg 
     group by 1, 2
    ) b 
where  
    a.msisdn = b.msisdn
and  
    inv_month = 'M'
;
-- Summary Table
select 
    COUNT(DISTINCT a.MSISDN)
from 
    (select 
        msisdn 
     from 
        ${table-prefix}${hvc-schema}.${table-prefix}hvc_summary_cb 
     where 
        cut_off_report = '${run-date=yyyy-MM-dd}' and brnd_nme = 'kartuHALO'
    ) a , 
    (select 
        msisdn, inv_month,  max(bill_cycle) bill_cycle, sum(tot_bill_amount) as tot_bill_amount 
     from ${table-prefix}${hvc-schema}.${table-prefix}hvc_summary_postpaid_cb_rev 
     group by 1, 2
    ) b 
where  a.msisdn = b.msisdn 
and 
    inv_month = 'M'
;
--------------------------------------------------------------------------------------------------
-- Staging Table
select 
    count(distinct a.msisdn) c_msisdn 
from 
    (select 
        msisdn 
     from 
        ${table-prefix}${hvc-schema}.${table-prefix}hvc_summary_cb 
     where 
        cut_off_report = '${run-date=yyyy-MM-dd}' and brnd_nme = 'kartuHALO'
    ) a , 
    (select 
        msisdn, inv_month,  max(bill_cycle) bill_cycle, sum(tot_bill_amount) as tot_bill_amount 
     from ${table-prefix}${hvc-schema}.${table-prefix}hvc_postpaid_cb_rev_stg 
     group by 1, 2
    ) b 
where  
    a.msisdn = b.msisdn
and  
    inv_month = 'M'
;
-- Summary Table
select 
    count(distinct a.msisdn) c_msisdn 
from 
    (select 
        msisdn 
     from 
        ${table-prefix}${hvc-schema}.${table-prefix}hvc_summary_cb 
     where 
        cut_off_report = '${run-date=yyyy-MM-dd}' and brnd_nme = 'kartuHALO'
    ) a , 
    (select 
        msisdn, inv_month,  max(bill_cycle) bill_cycle, sum(tot_bill_amount) as tot_bill_amount 
     from ${table-prefix}${hvc-schema}.${table-prefix}hvc_summary_postpaid_cb_rev 
     group by 1, 2
    ) b 
where  a.msisdn = b.msisdn 
and 
    inv_month = 'M'
;

--------------------------------------------------------------------------------------------------
-- Staging Table
select 
    sum(tot_bill_amount) as tot_bill_amount 
from 
    (select 
        msisdn 
     from 
        ${table-prefix}${hvc-schema}.${table-prefix}hvc_summary_cb 
     where 
        cut_off_report = '${run-date=yyyy-MM-dd}' and brnd_nme = 'kartuHALO'
    ) a , 
    (select 
        msisdn, inv_month,  max(bill_cycle) bill_cycle, sum(tot_bill_amount) as tot_bill_amount 
     from ${table-prefix}${hvc-schema}.${table-prefix}hvc_postpaid_cb_rev_stg 
     group by 1, 2
    ) b 
where  
    a.msisdn = b.msisdn
and  
    inv_month = 'M'
;
-- Summary Table
select 
    sum(tot_bill_amount) as tot_bill_amount
from 
    (select 
        msisdn 
     from 
        ${table-prefix}${hvc-schema}.${table-prefix}hvc_summary_cb 
     where 
        cut_off_report = '${run-date=yyyy-MM-dd}' and brnd_nme = 'kartuHALO'
    ) a , 
    (select 
        msisdn, inv_month,  max(bill_cycle) bill_cycle, sum(tot_bill_amount) as tot_bill_amount 
     from ${table-prefix}${hvc-schema}.${table-prefix}hvc_summary_postpaid_cb_rev 
     group by 1, 2
    ) b 
where  a.msisdn = b.msisdn 
and 
    inv_month = 'M'
;