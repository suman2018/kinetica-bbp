SELECT COUNT(DISTINCT MSISDN) C_MSISDN FROM
(
SELECT MSISDN FROM ${table-prefix}${recharge-schema}.${table-prefix}MKIOS_STAGING_${run-date=yyyyMMdd}
UNION ALL
SELECT MSISDN FROM ${table-prefix}${recharge-schema}.${table-prefix}URP_STAGING_${run-date=yyyyMMdd}
);
SELECT COUNT(DISTINCT MSISDN) C_MSISDN FROM ${table-prefix}${hvc-schema}.${table-prefix}HVC_RECH_ROLLING_30D WHERE CUT_OFF_DT = '${run-date=yyyy-MM-dd}';

SELECT SUM(RCHRG_AMT) RCHRG_AMT FROM
(
SELECT RECH AS RCHRG_AMT FROM ${table-prefix}${recharge-schema}.${table-prefix}MKIOS_STAGING_${run-date=yyyyMMdd}
UNION ALL
SELECT RECH AS RCHRG_AMT FROM ${table-prefix}${recharge-schema}.${table-prefix}URP_STAGING_${run-date=yyyyMMdd}
);
SELECT SUM(RCHRG_AMT) RCHRG_AMT FROM ${table-prefix}${hvc-schema}.${table-prefix}HVC_RECH_ROLLING_30D WHERE CUT_OFF_DT = '${run-date=yyyy-MM-dd}';

SELECT SUM(RCHRG_TRX) RCHRG_TRX FROM
(
SELECT TRX_RECH AS RCHRG_TRX FROM ${table-prefix}${recharge-schema}.${table-prefix}MKIOS_STAGING_${run-date=yyyyMMdd}
UNION ALL
SELECT TRX_RECH AS RCHRG_TRX FROM ${table-prefix}${recharge-schema}.${table-prefix}URP_STAGING_${run-date=yyyyMMdd}
);
SELECT SUM(RCHRG_TRX) RCHRG_TRX FROM ${table-prefix}${hvc-schema}.${table-prefix}HVC_RECH_ROLLING_30D WHERE CUT_OFF_DT = '${run-date=yyyy-MM-dd}';