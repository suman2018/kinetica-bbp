SELECT COUNT(MSISDN) AS MSISDN FROM ${table-prefix}${niknokk-schema}.${table-prefix}NIKNOKK_CB_PREPAID_POSTPAID_${run-date=yyyyMM} WHERE BRAND <> 'kartuHALO';
SELECT COUNT(MSISDN) AS MSISDN FROM ${table-prefix}${niknokk-schema}.${table-prefix}NIKNOKK_PRE_REG_${run-date=yyyyMM};

SELECT
	COUNT(a.msisdn) AS MSISDN
FROM  
(
	SELECT '${run-date=yyyyMM}' month, msisdn,brand,'' imei, area_sales as area_lacci, 
	region_lacci,nik_id, reg_stts  
	FROM ${table-prefix}${niknokk-schema}.${table-prefix}niknokk_multidim_${run-date=yyyyMM}
	WHERE date(trx_date) = '${run-date=yyyy-MM-dd}' 
	GROUP BY 2,3,5,6,7,8
)  a  LEFT JOIN 
(
 	SELECT msisdn, simple_wording 
 	FROM ${table-prefix}${niknokk-schema}.${table-prefix}niknokk_preblk_ccm_cln_baru
 	WHERE  
		simple_wording = 'Success'		
		AND date(trx_timestamp) <= '${run-date=yyyy-MM-dd}' 
 	GROUP BY 1,2
) b ON a.msisdn=b.msisdn INNER JOIN
(
	 SELECT msisdn, area, brand
    	 FROM ${table-prefix}${niknokk-schema}.${table-prefix}niknokk_sales_pre
    	 WHERE date(datex) >= concat(SUBSTRING('${run-date=yyyy-MM-dd}',1,7),'-01') 
    	 and date(datex) <= '${run-date=yyyy-MM-dd}'
    	 GROUP BY 1,2,3
) c on a.msisdn = c.msisdn  
WHERE a.brand <> 'kartuHALO';
SELECT COUNT(MSISDN) AS MSISDN FROM ${table-prefix}${niknokk-schema}.${table-prefix}NIKNOKK_PRE_REG_${run-date=yyyyMM};

SELECT COUNT(DISTINCT MSISDN) FROM
(
SELECT MSISDN FROM ${table-prefix}${niknokk-schema}.${table-prefix}NIKNOKK_CHURN_PRE WHERE DATE(DATEX) >= CONCAT(substring('${run-date=yyyy-MM-dd}',1,7),'-01')
        AND DATE(datex) <= '${run-date=yyyy-MM-dd}'
EXCEPT
SELECT MSISDN FROM ${table-prefix}${niknokk-schema}.${table-prefix}niknokk_multidim_${run-date=yyyyMM} WHERE DATE(TRX_DATE) = '${run-date=yyyy-MM-dd}'
);
SELECT COUNT(MSISDN) FROM ${table-prefix}${niknokk-schema}.${table-prefix}NIKNOKK_08_CHURN_SEGMENT WHERE CHURN_DATE = '${run-date=yyyy-MM-dd}';