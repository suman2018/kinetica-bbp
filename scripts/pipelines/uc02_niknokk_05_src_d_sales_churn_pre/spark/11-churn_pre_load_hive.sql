-- Author: Sadar Baskoro <ssetihajid@kinetica.com>
-- Last Update: 10 Jan 2019
-- Objective: load daily churn pre

select
trim(deactivation_date) as deactivation_date
,trim(subs_id) as subs_id
,trim(msisdn) as msisdn
,trim(paychannel) as paychannel
,trim(prefix) as prefix
,trim(area_hlr) as area_hlr
,trim(region_hlr) as region_hlr
,trim(city_hlr) as city_hlr
,trim(price_plan_id) as price_plan_id
,trim(offer) as offer
,trim(brand) as brand
,trim(cust_type_desc) as cust_type_desc
,trim(do_date) as do_date
,trim(dealer_code) as dealer_code
,trim(item_code) as item_code
,trim(dist_type) as dist_type
,trim(last_usage_date) as last_usage_date
,trim(lacci_id) as lacci_id
,trim(lac) as lac
,trim(ci) as ci
,trim(cgi_prepaid) as cgi_prepaid
,trim(node) as node
,trim(area_name) as area_name
,trim(regional_channel) as regional_channel
,trim(branch) as branch
,trim(sub_branch) as sub_branch
,trim(cluster_sales) as cluster_sales
,trim(kabupaten) as kabupaten
,trim(lctn_closing_flag) as lctn_closing_flag
,trim(datex) as datex
from gpu.v_gpu_churn_pre_daily
where datex = '${run-date=yyyyMMdd}'
