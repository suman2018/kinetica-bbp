-- Author: Sadar Baskoro <ssetihajid@kinetica.com>
-- Last Update: 10 Jan 2019
-- Objective: load churn pre data in daily basis


create or replace table ${table-prefix}${niknokk-schema}.${table-prefix}niknokk_churn_pre_stg as
select 
datetime(deactivation_date) as deactivation_date
,cast(subs_id as integer) as subs_id
,msisdn
,paychannel      
,cast(prefix as integer) as prefix       
,area_hlr     
,region_hlr     
,city_hlr       
,cast(price_plan_id as integer) as price_plan_id               
,offer                  
,brand                  
,cust_type_desc         
,do_date                      
,dealer_code             
,item_code              
,dist_type             
,datetime(last_usage_date)  as last_usage_date           
,cast(lacci_id as integer)  as lacci_id                     
,cast(lac as integer)       as lac 
,cast(ci as integer)        as ci       
,cgi_prepaid     
,node            
,area_name as area            
,regional_channel as region      
,branch          
,sub_branch as subbranch       
,cluster_sales as cluster         
,kabupaten       
,lctn_closing_flag 
,date(substring(datex,1,4)||'-'||substring(datex,5,2)||'-'||substring(datex,7,2)) as datex
from ${table-prefix}${niknokk-schema}.${table-prefix}niknokk_churn_pre_spark;

