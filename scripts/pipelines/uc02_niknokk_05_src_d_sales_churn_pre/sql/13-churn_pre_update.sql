-- Author: Sadar Baskoro <ssetihajid@kinetica.com>
-- Last Update: 10 Jan 2019
-- Objective: load daily churn pre


delete from ${table-prefix}${niknokk-schema}.${table-prefix}niknokk_churn_pre
where date(datex) = '${run-date=yyyy-MM-dd}';


insert into ${table-prefix}${niknokk-schema}.${table-prefix}niknokk_churn_pre
select deactivation_date, subs_id, msisdn, paychannel, prefix, area_hlr, region_hlr, city_hlr, price_plan_id, offer, brand, cust_type_desc,
do_date, dealer_code, item_code, dist_type, last_usage_date, lacci_id, lac, ci, cgi_prepaid, node, area, region, branch, subbranch, 
cluster, kabupaten, lctn_closing_flag, datex  
from ${table-prefix}${niknokk-schema}.${table-prefix}niknokk_churn_pre_stg;


drop table if exists ${table-prefix}${niknokk-schema}.${table-prefix}niknokk_churn_pre_stg;


delete from ${table-prefix}${niknokk-schema}.${table-prefix}niknokk_churn_pre
where datetime(datex) <= datetime(last_day(date(date('${run-date=yyyy-MM-dd}') - interval '2' month)));
