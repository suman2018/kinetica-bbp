-- Author: Sadar Baskoro <ssetihajid@kinetica.com>
-- Last Update: 10 Jan 2019


create or replace table ${table-prefix}${niknokk-schema}.${table-prefix}niknokk_churn_pre_spark
(
   deactivation_date   varchar(32)             
  ,subs_id           varchar(16)               
  ,msisdn            varchar(16, shard_key)
  ,paychannel        varchar(8, dict)      
  ,prefix            varchar(16, dict)               
  ,area_hlr          varchar(16, dict)     
  ,region_hlr        varchar(32, dict)     
  ,city_hlr          varchar(32, dict)     
  ,price_plan_id     varchar(32, dict)       
  ,offer             varchar(64, dict)     
  ,brand             varchar(16, dict)     
  ,cust_type_desc    varchar(32, dict)     
  ,do_date           varchar(16, dict)           
  ,dealer_code       varchar(8, dict)      
  ,item_code         varchar(16, dict)     
  ,dist_type         varchar(16, dict)     
  ,last_usage_date   varchar(32, dict)             
  ,lacci_id          varchar(32, dict)     
  ,lac               varchar(32, dict)     
  ,ci                varchar(32, dict)     
  ,cgi_prepaid       varchar(32, dict)     
  ,node              varchar(32, dict)     
  ,area_name         	 varchar(16, dict)     
  ,regional_channel            varchar(32, dict)     
  ,branch            varchar(64, dict)     
  ,sub_branch         varchar(64, dict)     
  ,cluster_sales           varchar(32, dict)     
  ,kabupaten         varchar(32, dict)     
  ,lctn_closing_flag varchar(8, dict) 
  ,datex			 varchar(32, dict)
) ;
