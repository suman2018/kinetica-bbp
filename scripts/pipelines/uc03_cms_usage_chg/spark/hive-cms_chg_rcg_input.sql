select
 CAST( msisdn                    AS BIGINT)
,trx_date
,content_id  
,CAST( pricing_item_id           AS INT)
,vas_code
,activation_channel_id
,CAST( rev                       AS INT)
,CAST( dur                       AS INT)
,CAST( trx_c as int)
FROM gpu.gpu_chg_daily
where datex = '${run-date=yyyyMMdd}'
