delete from ${table-prefix}${cms-schema}.${table-prefix}cms_chg_detail
where file_date = date('${run-date=yyyy-MM-dd}')
;

--/* KI_HINT_DICT_PROJECTION */
--create or replace temp table ${table-prefix}${cms-schema}.${table-prefix}${pipeline-name}_tmp
--as
--select 
--	msisdn
--	,sum(rev) as rev
--	,trx_date
--	,date('${run-date=yyyy-MM-dd}') as file_date
--    ,'${job-id}' as job_id
--    ,'${pipeline-name}' as pipeline_name
--    ,'${pipeline-step}' as pipeline_step
--from ${table-prefix}${cms_stg-schema}.${table-prefix}cms_chg_rcg_input
--group by
--	msisdn
--	,trx_date
--;
--
--create or replace table ${table-prefix}${cms-schema}.${table-prefix}cms_chg_detail
--(
--    trx_date date(dict) NOT NULL, --needed for arpu
--    msisdn bigint(shard_key) NOT NULL, --needed for arpu
--    content_id varchar(64, dict),
--    pricing_item_id integer(dict),
--    vas_code varchar(64,dict),
--    activation_channel_id varchar(8, dict),
--    rev bigint(dict), --needed for arpu
--    dur bigint(dict),
--    trx_c integer(dict),
--	file_date DATE(dict) NOT NULL,
--    orch_job_id varchar(8, dict) NOT NULL,
--    pipeline_name varchar(64, dict) NOT NULL,
--    pipeline_step varchar(64, dict) NOT NULL
--)



--alter table ${table-prefix}${cms-schema}.${table-prefix}${pipeline-name}_tmp modify file_date date(dict) not null;
--alter table ${table-prefix}${cms-schema}.${table-prefix}${pipeline-name}_tmp modify trx_date date(dict) not null;
--alter table ${table-prefix}${cms-schema}.${table-prefix}${pipeline-name}_tmp modify rev bigint(dict) not null;
--alter table ${table-prefix}${cms-schema}.${table-prefix}${pipeline-name}_tmp modify job_id varchar(8, dict) not null;
--alter table ${table-prefix}${cms-schema}.${table-prefix}${pipeline-name}_tmp modify pipeline_name varchar(64, dict) not null;
--alter table ${table-prefix}${cms-schema}.${table-prefix}${pipeline-name}_tmp modify pipeline_step varchar(64, dict) not null;

insert into ${table-prefix}${cms-schema}.${table-prefix}cms_chg_detail
select 
    msisdn
    ,trx_date
    ,content_id
    ,pricing_item_id
    ,vas_code
    ,activation_channel_id
    ,rev
    ,dur
    ,trx_c
	,date('${run-date=yyyy-MM-dd}') as file_date
	,'${job-id}' as job_id
	,'${pipeline-name}' as pipeline_name
	,'${pipeline-step}' as pipeline_step
from ${table-prefix}${cms_stg-schema}.${table-prefix}cms_chg_rcg_input
;

delete from ${table-prefix}${cms-schema}.${table-prefix}cms_chg_detail
where file_date < date('${run-date=yyyy-MM-dd}') - interval '93' day
;

