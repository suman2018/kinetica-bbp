create or replace table ${table-prefix}${cms_stg-schema}.${table-prefix}cms_chg_rcg_input
(
    "msisdn" bigint(shard_key) NOT NULL,
    "trx_date" date(dict) NOT NULL,
    "content_id" varchar(64, dict),
    "pricing_item_id" integer(dict),
    "vas_code" varchar(64,dict),
    "activation_channel_id" varchar(8, dict),
    "rev" integer(dict),
    "dur" integer(dict),
    "trx_c" integer(dict)
);