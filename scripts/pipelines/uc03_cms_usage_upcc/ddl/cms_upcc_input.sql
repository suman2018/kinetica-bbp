create or replace table ${table-prefix}${cms_stg-schema}.${table-prefix}cms_upcc_input
(
    "trx_date" date(dict),
    "msisdn" bigint(shard_key),
    "profile_name" varchar(32, dict),
    "bucket_usage" integer(dict)
)