CREATE OR REPLACE REPLICATED TABLE ${table-prefix}${cms_stg-schema}.${table-prefix}cms_bucket_usage_ref_input
(
    "contentid" VARCHAR(64, dict) NOT NULL,
    "bonusid" VARCHAR(32, dict) NOT NULL,
    "source" VARCHAR(4, dict) NOT NULL,
    "validity" INTEGER(dict) NOT NULL
)