delete from ${table-prefix}${cms-schema}.${table-prefix}cms_campaign_tracking_detail
where file_date = date('${run-date=yyyy-MM-dd}')
and pipeline_name = '${pipeline-name}'
and pipeline_step = '${pipeline-step}';


insert into ${table-prefix}${cms-schema}.${table-prefix}cms_campaign_tracking_detail
(
	 category
	,campaign_type
	,msisdn
	,campaign_id
	,campaign_objective_id
	,campaign_name
	,start_period
	,end_period
	,iscontrolgroup
	,campaign_group_id
	,segment
	,los
	,arpu
	,business_group
	,initiated_group
	--,area
	,region_hlr
	,cluster
	,branch
	,trx_date
	,content_1
	,metric_1
	,metric_2
	,metric_3
	,file_date
	,orch_job_id
	,pipeline_name
	,pipeline_step
)
select
	 dtl.category
	,dtl.campaign_type
	,dtl.msisdn
	,dtl.campaign_id
	,dtl.campaign_objective_id
	,dtl.campaign_name
	,dtl.start_period
	,dtl.end_period
	,dtl.iscontrolgroup
	,dtl.campaign_group_id
	,dtl.segment
	,dtl.los
	,dtl.arpu
	,dtl.business_group
	,dtl.initiated_group
	--,dtl.area
	,dtl.region_hlr
	,dtl.cluster
	,dtl.branch
	,upcc.trx_date
	,cast(upcc.profile_name as varchar) as content_1
	,1024*upcc.bucket_usage as data_usage
	,0 as voice_usage
	,0 as sms_usage
	,date('${run-date=yyyy-MM-dd}') as file_date
	,'${job-id}' as job_id
	,'${pipeline-name}' as pipeline_name
	,'${pipeline-step}' as pipeline_step
from ${table-prefix}${cms-schema}.${table-prefix}cms_campaign_tracking_detail dtl
join ${table-prefix}${cms_stg-schema}.${table-prefix}cms_bucket_usage_ref_input rf
	on dtl.content_1 = rf.contentid
	and rf.source = 'UPCC'
join ${table-prefix}${cms_stg-schema}.${table-prefix}cms_upcc_input upcc
	on rf.bonusid = upcc.profile_name
	and dtl.msisdn = upcc.msisdn
where dtl.file_date = date('${run-date=yyyy-MM-dd}')
	and dtl.pipeline_name = '${pipeline-name}'
	and dtl.metric_1 is not null
;