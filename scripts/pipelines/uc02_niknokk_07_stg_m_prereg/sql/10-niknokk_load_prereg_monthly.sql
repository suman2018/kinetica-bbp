-- Author: Sadar Baskoro <ssetihajid@kinetica.com>
-- Last Update: 23 Dec 2018
-- Objective: update pre reg table for full month data


drop table if exists ${table-prefix}${niknokk-schema}.${table-prefix}niknokk_pre_reg_${run-date=yyyyMM};

create or replace table ${table-prefix}${niknokk-schema}.${table-prefix}niknokk_pre_reg_${run-date=yyyyMM} as
SELECT
    a.month,
    a.msisdn,
    a.brand,
    a.imei,
    a.area_lacci,
    a.region_lacci,
    c.nik_id,
    c.reg_stts,
    b.simple_wording
FROM ${table-prefix}${niknokk-schema}.${table-prefix}niknokk_cb_prepaid_postpaid_${run-date=yyyyMM} a  
LEFT JOIN 
(
	SELECT msisdn, simple_wording
	FROM ${table-prefix}${niknokk-schema}.${table-prefix}niknokk_preblk_ccm_cln_baru
	WHERE 
		simple_wording = 'Success' 
		AND DATE(trx_timestamp) <= '${run-date=yyyy-MM-dd}'
	GROUP BY 1,2
) b 
ON a.msisdn=b.msisdn
LEFT JOIN (
	SELECT brand, msisdn, nik_id, reg_stts
	FROM ${table-prefix}${niknokk-schema}.${table-prefix}niknokk_multidim_${run-date=yyyyMM}
	WHERE date(trx_date) = '${run-date=yyyy-MM-dd}'
	GROUP BY 1,2,3,4
) c 
ON a.msisdn=c.msisdn
WHERE a.brand <> 'kartuHALO';

alter table ${table-prefix}${niknokk-schema}.${table-prefix}niknokk_pre_reg_${run-date=yyyyMM} modify brand varchar(16,dict);
alter table ${table-prefix}${niknokk-schema}.${table-prefix}niknokk_pre_reg_${run-date=yyyyMM} modify area_lacci varchar(8,dict); 
alter table ${table-prefix}${niknokk-schema}.${table-prefix}niknokk_pre_reg_${run-date=yyyyMM} modify region_lacci varchar(32,dict);
alter table ${table-prefix}${niknokk-schema}.${table-prefix}niknokk_pre_reg_${run-date=yyyyMM} modify simple_wording varchar(8,dict);
