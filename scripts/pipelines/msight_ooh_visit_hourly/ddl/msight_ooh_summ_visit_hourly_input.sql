create or replace table ${table-prefix}${msight_ooh-schema}.${table-prefix}ooh_summ_visit_hourly_input
(
    "months" varchar(8, dict),
    "location_code" varchar(32, dict),
    "id_dtl"        varchar(4, dict),
    "id_bearing"    varchar(4, dict), 
    "id_distance_ac" varchar(1, dict),
    "id_distance_cb" varchar(1, dict),
    "trxdate"        varchar(16, dict),
    "hour"           integer(shard_key, dict),
    "traffic"        long(dict),
    "unique_traffic" long(dict)
)

