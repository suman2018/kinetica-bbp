/* KI_HINT_DICT_PROJECTION */
create or replace table ${table-prefix}${msight_ooh-schema}.${table-prefix}ooh_summ_visit_hourly_stg as
select
months,
location_code,
cast(id_dtl as decimal(2,1)) as id_dtl,
cast(id_bearing as integer) as id_bearing,
cast(id_distance_ac as integer) as id_distance_ac,
cast(id_distance_cb as integer) as id_distance_cb,
trxdate,
hour,
cast(traffic as integer) as traffic ,
cast(unique_traffic as integer) unique_traffic , 
ki_shard_key (hour)
FROM ${table-prefix}${msight_ooh-schema}.${table-prefix}ooh_summ_visit_hourly_input;

alter table ${table-prefix}${msight_ooh-schema}.${table-prefix}ooh_summ_visit_hourly_stg modify column months varchar (8, dict);
alter table ${table-prefix}${msight_ooh-schema}.${table-prefix}ooh_summ_visit_hourly_stg modify column location_code varchar (32, dict);
alter table ${table-prefix}${msight_ooh-schema}.${table-prefix}ooh_summ_visit_hourly_stg modify column trxdate varchar (16, dict);
alter table ${table-prefix}${msight_ooh-schema}.${table-prefix}ooh_summ_visit_hourly_stg modify column traffic integer (dict);
alter table ${table-prefix}${msight_ooh-schema}.${table-prefix}ooh_summ_visit_hourly_stg modify column unique_traffic integer (dict);

delete from ${table-prefix}${msight_ooh-schema}.${table-prefix}ooh_summ_visit_hourly where months = '${run-date=yyyyMM}';

insert into ${table-prefix}${msight_ooh-schema}.${table-prefix}ooh_summ_visit_hourly
select * from ${table-prefix}${msight_ooh-schema}.${table-prefix}ooh_summ_visit_hourly_stg;

DROP TABLE IF EXISTS ${table-prefix}${msight_ooh-schema}.${table-prefix}ooh_summ_visit_hourly_stg;
DROP TABLE IF EXISTS ${table-prefix}${msight_ooh-schema}.${table-prefix}ooh_summ_visit_hourly_input;




