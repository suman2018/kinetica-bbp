select
months,
location_code, 
cast(id_dtl as string) as id_dtl,
cast(id_bearing as string) as id_bearing, 
cast(id_distance_ac as string) as id_distance_ac,
cast(id_distance_cb as string) id_distance_cb,
trxdate,
hour,
traffic,
unique_traffic
from skp.ooh_summ_visit_hourly_${run-date=yyyyMM}

