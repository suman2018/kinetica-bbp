-- author: sadar baskoro <ssetihajid@kinetica.com>
-- last update: 10 Jan 2019
-- objective: create initial tables required



create or replace table ${table-prefix}${niknokk-schema}.${table-prefix}niknokk_preblk_ccm_cln_baru(
  trx_date					timestamp 
  ,subs_id					varchar(32)
  ,msisdn                   varchar(16, shard_key) 
  ,channel		            varchar(16, dict) 
  ,nik				        varchar(32)
 ,no_kk					varchar(64)
  ,trx_timestamp			timestamp
  ,insert_timestamp			timestamp
  ,update_timestamp			timestamp
  ,simple_wording			varchar(64, dict)
  ,code_status				integer
  ,id_status        		varchar(1)
  ,adn						varchar(16, dict)
  ,reg_type             	varchar(16, dict)
);



create or replace table ${table-prefix}${niknokk-schema}.${table-prefix}niknokk_churn_pre(
   deactivation_date timestamp  
  ,subs_id           integer    
  ,msisdn            varchar(16, shard_key)     
  ,paychannel        varchar(8, dict) 
  ,prefix            integer    
  ,area_hlr          varchar(16, dict)
  ,region_hlr        varchar(32, dict)
  ,city_hlr          varchar(32, dict)
  ,price_plan_id     integer    
  ,offer             varchar(64, dict)
  ,brand             varchar(16, dict)
  ,cust_type_desc    varchar(32, dict)
  ,do_date           varchar(16, dict)
  ,dealer_code       varchar(8, dict) 
  ,item_code         varchar(16, dict)
  ,dist_type         varchar(16, dict)
  ,last_usage_date   timestamp  
  ,lacci_id          integer    
  ,lac               integer    
  ,ci                integer
  ,cgi_prepaid       varchar(32, dict)     
  ,node              varchar(32, dict)
  ,area				 varchar(16, dict)
  ,region			 varchar(32, dict)
  ,branch			 varchar(64, dict)	
  ,subbranch		 varchar(64, dict)	
  ,cluster			 varchar(32, dict)
  ,kabupaten  		 varchar(32, dict)
  ,lctn_closing_flag varchar(8, dict) 
  ,datex			 date(dict)
) ;



create or replace table ${table-prefix}${niknokk-schema}.${table-prefix}niknokk_sales_pre(
   activation_date   timestamp  
  ,subs_id           varchar(16)    
  ,msisdn            varchar(16, shard_key)     
  ,paychannel        varchar(8, dict) 
  ,prefix            integer    
  ,area_hlr          varchar(16, dict)
  ,region_hlr        varchar(32, dict)
  ,city_hlr          varchar(32, dict)
  ,price_plan_id     integer    
  ,offer             varchar(64, dict)
  ,brand             varchar(16, dict)
  ,cust_type_desc    varchar(32, dict)
  ,do_date           varchar(16, dict)
  ,dealer_code       varchar(8, dict) 
  ,item_code         varchar(16, dict)
  ,dist_type         varchar(16, dict)
  ,first_usage_date  timestamp  
  ,lacci_id          integer    
  ,lac               integer    
  ,ci                integer    
  ,cgi_prepaid       varchar(32, dict)
  ,node              varchar(32, dict)
  ,area              varchar(16, dict) 
  ,region            varchar(32, dict)
  ,branch            varchar(64, dict)
  ,subbranch         varchar(64, dict)
  ,cluster           varchar(32, dict)
  ,kabupaten         varchar(32, dict)
  ,lctn_closing_flag varchar(8, dict) 
  ,datex			 date(dict)
) ;


create or replace table ${table-prefix}${niknokk-schema}.${table-prefix}niknokk_01_msisdn_prepaid_reg (
   mth              varchar(8, shard_key, dict)
  ,area_lacci         varchar(16, shard_key, dict)
  ,f_segment          varchar(16, shard_key, dict)
  ,subs	  			  int
);

create or replace table ${table-prefix}${niknokk-schema}.${table-prefix}niknokk_02_nik_prepaid_reg (
   mth              varchar(8, shard_key, dict)
  ,area_lacci         varchar(16, shard_key, dict)
  ,f_segment          varchar(16, shard_key, dict)
  ,subs	  			  int
);

create or replace table ${table-prefix}${niknokk-schema}.${table-prefix}niknokk_03_churn_msisdn_nik (
   mth              varchar(8, shard_key, dict)
  ,subs	  			  int
  ,nik	  			  int
);


create or replace table ${table-prefix}${niknokk-schema}.${table-prefix}niknokk_04_waterfall_churn (
   mth              varchar(8, shard_key, dict)
  ,subs	  			  int
  ,nik	  			  int
);


create or replace table ${table-prefix}${niknokk-schema}.${table-prefix}niknokk_05_segment_nik_churn (
   mth              varchar(8, shard_key, dict)
  ,c_msisdn	  		  int
  ,nik	  			  int
);



create or replace table ${table-prefix}${niknokk-schema}.${table-prefix}niknokk_06_segment_nik_all (
   mth              varchar(8, shard_key, dict)
  ,c_msisdn	  		  int
  ,nik	  			  int
);


create or replace table ${table-prefix}${niknokk-schema}.${table-prefix}niknokk_07_profile_churn_nik_regchnnl (
   mth              	varchar(8, shard_key, dict)
  ,reg_chnl	  			varchar(64, shard_key, dict)
  ,nik	  			  	int
);

create or replace table ${table-prefix}${niknokk-schema}.${table-prefix}niknokk_07_profile_churn_nik_age (
   mth                varchar(8, shard_key, dict)
  ,age	  			  int
  ,nik	  			  int
);


create or replace table ${table-prefix}${niknokk-schema}.${table-prefix}niknokk_08_churn_segment (
   churn_date           varchar(10, shard_key, dict)
  ,brand				varchar(32, shard_key, dict)
  ,registration_status	varchar(8, shard_key, dict)
  ,los_churn			varchar(8, shard_key, dict)
  ,subs					int
  ,rev	  			  	float
);


create or replace table ${table-prefix}${niknokk-schema}.${table-prefix}niknokk_cohort_family_acq (
   dates						varchar(64, shard_key, dict)
   ,flag_nik_null				varchar(1, dict)
   ,area_administrative 		varchar(16, dict)
   ,region_administrative		varchar(32, dict)
   ,provinsi_administrative		varchar(32, dict)
   ,kabupaten_administrative	varchar(64, dict)
   ,kecamatan_administrative	varchar(32, shard_key, dict)
   ,segment_nik_msisdn			varchar(32, dict)
   ,segment_nik_msisdn_now		varchar(32, dict)
   ,sum_nokk					int
   ,sum_nik						int
   ,sum_msisdn					int
   ,sum_rev						float
   ,sum_nokk_now				int	
   ,sum_nik_now					int
   ,sum_msisdn_now				int
   ,sum_rev_now					float
);


create or replace table ${table-prefix}${niknokk-schema}.${table-prefix}niknokk_summary1_family_acq (
	dt_dt						timestamp(shard_key)
   ,flag_nik_null				varchar(1, dict)
   ,area_administrative 		varchar(16, dict)
   ,region_administrative		varchar(32, dict)
   ,provinsi_administrative		varchar(32, dict)
   ,kabupaten_administrative	varchar(64, dict)
   ,kecamatan_administrative	varchar(32, shard_key, dict)
   ,segment_nik_msisdn			varchar(32, dict)
   ,sum_nokk					int
   ,sum_nik						int
   ,sum_msisdn					int
   ,rev							float
);


create or replace table ${table-prefix}${niknokk-schema}.${table-prefix}niknokk_summary2_family_acq (
	dt_dt						timestamp(shard_key)
   ,flag_nik_null				varchar(1, dict)
   ,area_lacci 					varchar(16, dict)
   ,region_lacci				varchar(32, dict)
   ,cluster_lacci				varchar(64, dict)
   ,kabupaten_lacci				varchar(64, dict)
   ,kecamatan_lacci				varchar(64, shard_key, dict)
   ,segment_nik_msisdn			varchar(32, dict)
   ,sum_nokk					int
   ,sum_nik						int
   ,sum_msisdn					int
   ,rev							float
);

