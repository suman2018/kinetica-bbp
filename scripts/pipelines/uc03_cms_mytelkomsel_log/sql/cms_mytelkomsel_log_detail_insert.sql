delete from ${table-prefix}${cms-schema}.${table-prefix}cms_campaign_tracking_detail
where file_date = date('${run-date=yyyy-MM-dd}')
and pipeline_name = '${pipeline-name}'
and pipeline_step = '${pipeline-step}';

--mytsel_log

/* KI_HINT_DICT_PROJECTION */
create or replace temp table ${table-prefix}${cms-schema}.${table-prefix}${pipeline-name}_tmp
as
select 
	cview.category
	,cview.campaign_type
	,cview.msisdn
	,cview.campaign_id
	,cview.campaign_objective_id
	,cview.campaign_name
	,cview.start_period
	,cview.end_period
	,cview.iscontrolgroup
	,cview.campaign_group_id
	,cview.segment
	,cview.los
	,cview.arpu
	,cview.business_group
	,cview.initiated_group
	,cview.region_hlr
	,cview.cluster
	,cview.kabupaten
	,coalesce(uxp.trx_date, date('${run-date=yyyy-MM-dd}')) as trx_date
	,0 as is_btl
	,cview.target
	,cview.eligible
	,case when cview.eligible > 0 and uxp.served_mobile_number is not null then 1 else 0 end as taker
	,date('${run-date=yyyy-MM-dd}') as file_date
	,'${job-id}' as job_id
	,'${pipeline-name}' as pipeline_name
	,'${pipeline-step}' as pipeline_step
from 
(
	select distinct mltdim.*
	from ${table-prefix}${cms-schema}.${table-prefix}cms_campaign_trackingstream_multidim_v mltdim
	join
	(
		select distinct trx_date
		from ${table-prefix}${cms_stg-schema}.${table-prefix}cms_uxpusertrack_input
	) trx_date
	on trx_date.trx_date between date(mltdim.start_period) and date(mltdim.end_period)
	where campaign_group_id = 4
		and upper(campaign_name) like '%MYTSEL_LOG%'
) cview
left join ${table-prefix}${cms_stg-schema}.${table-prefix}cms_uxpusertrack_input uxp --check if there are duplicates
	on uxp.served_mobile_number = cview.msisdn
;

insert into ${table-prefix}${cms-schema}.${table-prefix}cms_campaign_tracking_detail
(
	 category
	,campaign_type
	,msisdn
	,campaign_id
	,campaign_objective_id
	,campaign_name
	,start_period
	,end_period
	,iscontrolgroup
	,campaign_group_id
	,segment
	,los
	,arpu
	,business_group
	,initiated_group
	,region_hlr
	,cluster
	,branch
	,trx_date
	,is_btl
	,target
	,eligible
	,taker
	,file_date
	,orch_job_id
	,pipeline_name
	,pipeline_step
)
select * from ${table-prefix}${cms-schema}.${table-prefix}${pipeline-name}_tmp
;

drop table ${table-prefix}${cms-schema}.${table-prefix}${pipeline-name}_tmp
;