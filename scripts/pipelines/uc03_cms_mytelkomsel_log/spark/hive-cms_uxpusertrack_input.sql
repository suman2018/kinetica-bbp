select 
 trx_timestamp, cast(trx_date as string), track_id, 
 cast(served_mobile_number as bigint), channel,
 useraction, useractionparam, sub_custtype, sub_postcusttype, sub_postsubcusttype,
 sub_brand, sub_priceplan, sub_region, sub_location, sub_kecamatan, 
 cast(sub_lac as int), 
 cast(sub_ci as int), 
 sub_lte, status, response_code, response_description, context, 
 cast(file_id as int)
from 
 gpu.fact_uxpusertrack
where 
 datex = '${run-date=yyyyMMdd}'
