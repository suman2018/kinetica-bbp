DELETE FROM ${table-prefix}${recharge-schema}.${table-prefix}recharge_summary WHERE file_date = DATE '${run-date=yyyy-MM-dd}';

/* reprocessing for high-level (history) tables */
DELETE FROM ${table-prefix}${uc1_history-schema}.${table-prefix}recharge_cluster_anumber WHERE file_date = DATE '${run-date=yyyy-MM-dd}';
DELETE FROM ${table-prefix}${uc1_history-schema}.${table-prefix}recharge_cluster_bnumber WHERE file_date = DATE '${run-date=yyyy-MM-dd}';