CREATE OR REPLACE VIEW ${table-prefix}${recharge-schema}.${table-prefix}recharge_summary_staging_v AS (
	SELECT 
		file_date, trx_date, brand, price_plan, bnum_area_sales, bnum_region_sales, bnum_branch, bnum_subbranch, bnum_cluster, 
		bnum_kabupaten, bnum_kecamatan, bnum_node_type, cust_type, rech_type, rs_node_type, rs_area_sales, 
		rs_region_sales, rs_cluster, rs_branch, rs_subbranch, rs_kabupaten, rs_kecamatan, channel_group, channel_category, 
		channel, merchant, dealer_code, dealer_area_sales, dealer_region_sales, dealer_branch, dealer_subbranch, 
		dealer_cluster, lacci_border_flag, access_type, stock, denom, 
		sum(trx_rech) as sum_trx_rech, sum(rech) as sum_rech
	FROM (
		SELECT
			file_date, trx_date, brand, price_plan, bnum_area_sales, bnum_region_sales, bnum_branch, bnum_subbranch, bnum_cluster, 
			bnum_kabupaten, bnum_kecamatan, bnum_node_type, cust_type, rech_type, denom, rs_node_type, rs_area_sales, 
			rs_region_sales, rs_cluster, rs_branch, rs_subbranch, rs_kabupaten, rs_kecamatan, channel_group, channel_category, 
			channel, merchant, dealer_code, dealer_area_sales, dealer_region_sales, dealer_branch, dealer_subbranch, 
			dealer_cluster, lacci_border_flag, access_type, stock, trx_rech, rech
		FROM
			${table-prefix}${recharge-schema}.${table-prefix}mkios_destination_v
		UNION ALL
		SELECT
			file_date, trx_date, brand, price_plan, bnum_area_sales, bnum_region_sales, bnum_branch, bnum_subbranch, bnum_cluster, 
			bnum_kabupaten, bnum_kecamatan, bnum_node_type, cust_type, rech_type, denom, rs_node_type, rs_area_sales, 
			rs_region_sales, rs_cluster, rs_branch, rs_subbranch, rs_kabupaten, rs_kecamatan, channel_group, channel_category, 
			channel, merchant, dealer_code, dealer_area_sales, dealer_region_sales, dealer_branch, dealer_subbranch, 
			dealer_cluster, lacci_border_flag, cast(null as int) AS access_type, stock, trx_rech, rech
		FROM 
			${table-prefix}${recharge-schema}.${table-prefix}urp_destination_v
		) t
	GROUP BY 
		bnum_cluster, channel, file_date, trx_date, brand, price_plan, bnum_area_sales, bnum_region_sales, bnum_branch, bnum_subbranch, 
		bnum_kabupaten, bnum_kecamatan, bnum_node_type, cust_type, rech_type, rs_node_type, rs_area_sales, 
		rs_region_sales, rs_cluster, rs_branch, rs_subbranch, rs_kabupaten, rs_kecamatan, channel_group, channel_category, 
		merchant, dealer_code, dealer_area_sales, dealer_region_sales, dealer_branch, dealer_subbranch, 
		dealer_cluster, lacci_border_flag, access_type, stock, denom
);

/* append current batch summary to summary table */
INSERT INTO ${table-prefix}${recharge-schema}.${table-prefix}recharge_summary (SELECT * FROM ${table-prefix}${recharge-schema}.${table-prefix}recharge_summary_staging_v);


/* append to high-level tables */
INSERT INTO ${table-prefix}${uc1_history-schema}.${table-prefix}recharge_cluster_anumber (
SELECT
	TRX_DATE, RS_AREA_SALES, RS_REGION_SALES, RS_CLUSTER, BRAND, CHANNEL_GROUP, CHANNEL_CATEGORY, CHANNEL,
	denom, sum(trx_rech), sum(rech) as rech, TIMESTAMP(DATE '${run-date=yyyy-MM-dd}') AS FILE_DATE
FROM 
	${table-prefix}${recharge-schema}.${table-prefix}mkios_destination_v
GROUP BY
	TRX_DATE, BRAND, CHANNEL, RS_AREA_SALES, RS_REGION_SALES, RS_CLUSTER, CHANNEL_GROUP, CHANNEL_CATEGORY, denom
);

INSERT INTO ${table-prefix}${uc1_history-schema}.${table-prefix}recharge_cluster_bnumber (
SELECT
	TRX_DATE, BNUM_AREA_SALES, BNUM_REGION_SALES, BNUM_CLUSTER, BRAND, CHANNEL_GROUP, CHANNEL_CATEGORY, CHANNEL,
	denom, sum(sum_trx_rech), sum(sum_rech), TIMESTAMP(DATE '${run-date=yyyy-MM-dd}') AS FILE_DATE
FROM 
	${table-prefix}${recharge-schema}.${table-prefix}recharge_summary_staging_v
GROUP BY
	TRX_DATE, BRAND, CHANNEL, BNUM_AREA_SALES, BNUM_REGION_SALES, BNUM_CLUSTER, CHANNEL_GROUP, CHANNEL_CATEGORY, denom
);


DROP TABLE IF EXISTS ${table-prefix}mkios_staging_${run-date=yyyyMMdd};
ALTER TABLE ${table-prefix}${recharge-schema}.${table-prefix}mkios_staging RENAME TO ${table-prefix}mkios_staging_${run-date=yyyyMMdd};
-- move this housekeeping into hvc rolling 
-- DROP TABLE IF EXISTS ${table-prefix}mkios_staging_${retention-date=yyyyMMdd};

DROP TABLE IF EXISTS ${table-prefix}urp_staging_${run-date=yyyyMMdd};
ALTER TABLE ${table-prefix}${recharge-schema}.${table-prefix}urp_staging RENAME TO ${table-prefix}urp_staging_${run-date=yyyyMMdd};
-- move this housekeeping into hvc rolling 
-- DROP TABLE IF EXISTS ${table-prefix}urp_staging_${retention-date=yyyyMMdd};
