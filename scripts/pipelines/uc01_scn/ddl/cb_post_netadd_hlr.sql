CREATE OR REPLACE TABLE ${table-prefix}${scn-schema}.${table-prefix}cb_post_netadd_hlr(
   BRAND         varchar(16, dict)    
  ,PRICE_PLAN_ID integer        
  ,OFFER         varchar(64, dict)    
  ,AREA_HLR      varchar(16, dict)    
  ,REGION_HLR    varchar(32, dict)    
  ,CITY_HLR      varchar(32, dict)    
  ,TRX_DATE      timestamp      
  ,FILE_DATE     timestamp      
  ,CURRENT_CNT   bigint
  ,PREVIOUS_CNT  bigint         
  ,DELTA         bigint 
);