create or replace table ${table-prefix}${scn-schema}.${table-prefix}churn_pre_summary(
   FILE_DATE         timestamp      
  ,DEACTIVATION_DATE timestamp      
  ,PAYCHANNEL        varchar(8, dict)     
  ,PREFIX            integer        
  ,AREA_HLR          varchar(16, dict)    
  ,REGION_HLR        varchar(32, dict)    
  ,CITY_HLR          varchar(32, dict)    
  ,PRICE_PLAN_ID     integer        
  ,OFFER             varchar(64, dict)    
  ,BRAND             varchar(16, dict)    
  ,CUST_TYPE_DESC    varchar(32, dict)    
  ,DO_DATE           varchar(16, dict)    
  ,DEALER_CODE       varchar(8, dict)     
  ,ITEM_CODE         varchar(16, dict)    
  ,DIST_TYPE         varchar(16, dict)    
  ,LAST_USAGE_DATE   timestamp      
  ,LAC               integer(shard_key)        
  ,CI                integer(shard_key)  
  ,NODE              varchar(32, dict)
  ,AREA                   varchar(8, dict)     
  ,REGION                 varchar(32, dict)    
  ,BRANCH                 varchar(64, dict)    
  ,SUBBRANCH              varchar(64, dict)    
  ,CLUSTER                varchar(32, dict)    
  ,KABUPATEN              varchar(32, dict)    
  ,KECAMATAN              varchar(32, dict)    
  ,KELURAHAN              varchar(32, dict)
  ,CNT               bigint
);