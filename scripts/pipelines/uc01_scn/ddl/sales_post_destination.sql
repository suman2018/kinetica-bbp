create or replace table ${table-prefix}${scn-schema}.${table-prefix}sales_post_destination(
   ACTIVATION_DATE       timestamp        
  ,SUBS_ID               integer          
  ,MSISDN                bigint           
  ,ACCOUNT_ID            integer          
  ,PREFIX                integer          
  ,AREA_HLR              varchar(16, dict)
  ,REGION_HLR            varchar(32, dict)
  ,CITY_HLR              varchar(32, dict)
  ,PRICE_PLAN_ID         integer          
  ,OFFER                 varchar(64, dict)
  ,BRAND                 varchar(16, dict)
  ,BILL_CYCLE            integer          
  ,CUST_TYPE             varchar(1, dict) 
  ,CUST_TYPE_DESC        varchar(32, dict)
  ,CUST_SUBTYPE          varchar(1, dict) 
  ,CUST_SUBTYPE_DESC     varchar(32, dict)
  ,PRE_TO_POST_FLAG      varchar(8, dict) 
  ,CUST_TYPE_CHANGE_FLAG varchar(8, dict) 
  ,PREV_CUST_TYPE        varchar(8, dict) 
  ,PREV_CUST_SUBTYPE     varchar(8, dict) 
  ,OWNERSHIP_CHANGE_FLAG varchar(8, dict) 
  ,FIRST_USAGE_DATE      timestamp        
  ,LACCI_ID              integer          
  ,LAC                   integer(shard_key)
  ,CI                    integer(shard_key)
  ,CGI_POSTPAID          varchar(32, dict)
  ,NODE                  varchar(32, dict) 
  ,AREA              varchar(8, dict)
  ,REGION            varchar(32, dict)
  ,BRANCH            varchar(64, dict)
  ,SUBBRANCH         varchar(64, dict)
  ,CLUSTER           varchar(32, dict)
  ,KABUPATEN         varchar(32, dict)
  ,LCTN_CLOSING_FLAG     varchar(8, dict)
  ,FILE_DATE             timestamp 
);