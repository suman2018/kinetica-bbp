CREATE OR REPLACE TABLE ${table-prefix}${scn-schema}.${table-prefix}cb_pre_netadd_vlr(
   AREA_HLR      varchar(16, dict)    
  ,REGION_HLR    varchar(32, dict)
  ,BRAND         varchar(16, dict)     
  ,PRICE_PLAN_ID integer        
  ,OFFER         varchar(64, dict)        
  ,TRX_DATE      timestamp        
  ,FILE_DATE     timestamp      
  ,CURRENT_CNT   bigint
  ,PREVIOUS_CNT  bigint         
  ,DELTA         bigint
);