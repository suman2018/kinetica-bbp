DELETE FROM ${table-prefix}${scn-schema}.${table-prefix}cb_pre_destination WHERE file_date = TIMESTAMP ('${run-date=yyyy-MM-dd}');
DELETE FROM ${table-prefix}${scn-schema}.${table-prefix}cb_post_destination WHERE file_date = TIMESTAMP ('${run-date=yyyy-MM-dd}');
DELETE FROM ${table-prefix}${scn-schema}.${table-prefix}sales_pre_destination WHERE file_date = TIMESTAMP ('${run-date=yyyy-MM-dd}');
DELETE FROM ${table-prefix}${scn-schema}.${table-prefix}sales_post_destination WHERE file_date = TIMESTAMP ('${run-date=yyyy-MM-dd}');
DELETE FROM ${table-prefix}${scn-schema}.${table-prefix}churn_pre_destination WHERE file_date = TIMESTAMP ('${run-date=yyyy-MM-dd}');
DELETE FROM ${table-prefix}${scn-schema}.${table-prefix}churn_post_destination WHERE file_date = TIMESTAMP ('${run-date=yyyy-MM-dd}');

DELETE FROM ${table-prefix}${scn-schema}.${table-prefix}cb_pre_netadd_vlr WHERE file_date = TIMESTAMP ('${run-date=yyyy-MM-dd}');
DELETE FROM ${table-prefix}${scn-schema}.${table-prefix}cb_post_netadd_vlr WHERE file_date = TIMESTAMP ('${run-date=yyyy-MM-dd}');
DELETE FROM ${table-prefix}${scn-schema}.${table-prefix}cb_post_netadd_hlr WHERE file_date = TIMESTAMP ('${run-date=yyyy-MM-dd}');

DELETE FROM ${table-prefix}${scn-schema}.${table-prefix}cb_pre_summary WHERE file_date = TIMESTAMP ('${run-date=yyyy-MM-dd}');
DELETE FROM ${table-prefix}${scn-schema}.${table-prefix}cb_post_summary WHERE file_date = TIMESTAMP ('${run-date=yyyy-MM-dd}');
DELETE FROM ${table-prefix}${scn-schema}.${table-prefix}sales_pre_summary WHERE file_date = TIMESTAMP ('${run-date=yyyy-MM-dd}');
DELETE FROM ${table-prefix}${scn-schema}.${table-prefix}sales_post_summary WHERE file_date = TIMESTAMP ('${run-date=yyyy-MM-dd}');
DELETE FROM ${table-prefix}${scn-schema}.${table-prefix}churn_pre_summary WHERE file_date = TIMESTAMP ('${run-date=yyyy-MM-dd}');
DELETE FROM ${table-prefix}${scn-schema}.${table-prefix}churn_post_summary WHERE file_date = TIMESTAMP ('${run-date=yyyy-MM-dd}');


/* reprocessing for high-level (history) tables */
DELETE FROM ${table-prefix}${uc1_history-schema}.${table-prefix}scn_olap_hlr WHERE file_date = DATE ('${run-date=yyyy-MM-dd}');
DELETE FROM ${table-prefix}${uc1_history-schema}.${table-prefix}scn_cluster_osdss WHERE file_date = DATE ('${run-date=yyyy-MM-dd}');
DELETE FROM ${table-prefix}${uc1_history-schema}.${table-prefix}subscriber_cluster_osdss WHERE file_date = DATE ('${run-date=yyyy-MM-dd}');
DELETE FROM ${table-prefix}${uc1_history-schema}.${table-prefix}revenue_scn_enterprise_pre WHERE file_date = DATE ('${run-date=yyyy-MM-dd}');
