CREATE OR REPLACE TABLE ${table-prefix}${scn-schema}.${table-prefix}cb_pre_tmpDestination AS (
	SELECT
		TRX_DATE, SUBS_ID, MSISDN, PAYCHANNEL, SUBS_STATUS, ACTIVATION_DATE, PREFIX, AREA_HLR, REGION_HLR, CITY_HLR, PRICE_PLAN_ID, OFFER, BRAND, CUST_TYPE_DESC,
 		CUST_SUBTYPE_DESC, HYBRID_FLAG, DO_DATE, DEALER_CODE, ITEM_CODE, DIST_TYPE, LACCI_ID, LAC, CI, CGI_PRE, NODE, AREA, REGION, BRANCH, SUBBRANCH, CLUSTER,
 		KABUPATEN, LCTN_CLOSING_FLAG, FILE_DATE
	FROM
		${table-prefix}${scn-schema}.${table-prefix}cb_pre_destination
	WHERE FILE_DATE = TIMESTAMP( DATE('${run-date=yyyy-MM-dd}') - INTERVAL '1' DAY)
	UNION ALL
	SELECT
		TRX_DATE, SUBS_ID, MSISDN, PAYCHANNEL, SUBS_STATUS, ACTIVATION_DATE, PREFIX, AREA_HLR, REGION_HLR, CITY_HLR, PRICE_PLAN_ID, OFFER,         
        CASE WHEN BRAND = 'Kartu AS' THEN 'KartuAS'
        WHEN BRAND IN ('KartuHALO', 'kartuHALO') THEN 'simPATI'
        ELSE BRAND END AS BRAND,
        CUST_TYPE_DESC,
 		CUST_SUBTYPE_DESC, HYBRID_FLAG, DO_DATE, DEALER_CODE, ITEM_CODE, DIST_TYPE, LACCI_ID, LAC, CI, CGI_PRE, NODE, AREA, REGION, BRANCH, SUBBRANCH, CLUSTER,
 		KABUPATEN, LCTN_CLOSING_FLAG, TIMESTAMP('${run-date=yyyy-MM-dd}') AS FILE_DATE
	FROM
		${table-prefix}${scn-schema}.${table-prefix}cb_pre_staging
);
DROP TABLE ${table-prefix}${scn-schema}.${table-prefix}cb_pre_destination;
ALTER TABLE ${table-prefix}${scn-schema}.${table-prefix}cb_pre_tmpDestination RENAME TO ${table-prefix}cb_pre_destination;

ALTER TABLE ${table-prefix}${scn-schema}.${table-prefix}cb_pre_destination MODIFY PAYCHANNEL        VARCHAR(8, dict) ;
ALTER TABLE ${table-prefix}${scn-schema}.${table-prefix}cb_pre_destination MODIFY SUBS_STATUS       VARCHAR(8, dict) ;        
ALTER TABLE ${table-prefix}${scn-schema}.${table-prefix}cb_pre_destination MODIFY AREA_HLR          VARCHAR(16, dict);
ALTER TABLE ${table-prefix}${scn-schema}.${table-prefix}cb_pre_destination MODIFY REGION_HLR        VARCHAR(32, dict);
ALTER TABLE ${table-prefix}${scn-schema}.${table-prefix}cb_pre_destination MODIFY CITY_HLR          VARCHAR(32, dict);    
ALTER TABLE ${table-prefix}${scn-schema}.${table-prefix}cb_pre_destination MODIFY OFFER             VARCHAR(64, dict);
ALTER TABLE ${table-prefix}${scn-schema}.${table-prefix}cb_pre_destination MODIFY BRAND             VARCHAR(16, dict);
ALTER TABLE ${table-prefix}${scn-schema}.${table-prefix}cb_pre_destination MODIFY CUST_TYPE_DESC    VARCHAR(32, dict);
ALTER TABLE ${table-prefix}${scn-schema}.${table-prefix}cb_pre_destination MODIFY CUST_SUBTYPE_DESC VARCHAR(32, dict);
ALTER TABLE ${table-prefix}${scn-schema}.${table-prefix}cb_pre_destination MODIFY HYBRID_FLAG       VARCHAR(8, dict) ;
ALTER TABLE ${table-prefix}${scn-schema}.${table-prefix}cb_pre_destination MODIFY DO_DATE           VARCHAR(16, dict);
ALTER TABLE ${table-prefix}${scn-schema}.${table-prefix}cb_pre_destination MODIFY DEALER_CODE       VARCHAR(8, dict) ;
ALTER TABLE ${table-prefix}${scn-schema}.${table-prefix}cb_pre_destination MODIFY ITEM_CODE         VARCHAR(16, dict);
ALTER TABLE ${table-prefix}${scn-schema}.${table-prefix}cb_pre_destination MODIFY DIST_TYPE         VARCHAR(16, dict);        
ALTER TABLE ${table-prefix}${scn-schema}.${table-prefix}cb_pre_destination MODIFY NODE              VARCHAR(32, dict); 
ALTER TABLE ${table-prefix}${scn-schema}.${table-prefix}cb_pre_destination MODIFY AREA              VARCHAR(8, dict) ;
ALTER TABLE ${table-prefix}${scn-schema}.${table-prefix}cb_pre_destination MODIFY REGION            VARCHAR(32, dict);
ALTER TABLE ${table-prefix}${scn-schema}.${table-prefix}cb_pre_destination MODIFY BRANCH            VARCHAR(64, dict);
ALTER TABLE ${table-prefix}${scn-schema}.${table-prefix}cb_pre_destination MODIFY SUBBRANCH         VARCHAR(64, dict);
ALTER TABLE ${table-prefix}${scn-schema}.${table-prefix}cb_pre_destination MODIFY CLUSTER           VARCHAR(32, dict);
ALTER TABLE ${table-prefix}${scn-schema}.${table-prefix}cb_pre_destination MODIFY KABUPATEN         VARCHAR(32, dict);
ALTER TABLE ${table-prefix}${scn-schema}.${table-prefix}cb_pre_destination MODIFY LCTN_CLOSING_FLAG VARCHAR(8, dict) ;
ALTER TABLE ${table-prefix}${scn-schema}.${table-prefix}cb_pre_destination MODIFY PREFIX            INTEGER(dict);
ALTER TABLE ${table-prefix}${scn-schema}.${table-prefix}cb_pre_destination MODIFY PRICE_PLAN_ID     INTEGER(dict);


CREATE OR REPLACE TABLE ${table-prefix}${scn-schema}.${table-prefix}cb_post_tmpDestination AS (
	SELECT TRX_DATE, SUBS_ID ,MSISDN ,ACCOUNT_ID,SUBS_STATUS ,ACTIVATION_DATE ,ORIGIN_ACTIVATION_DATE ,PREFIX ,AREA_HLR ,REGION_HLR ,CITY_HLR ,PRICE_PLAN_ID ,
    	OFFER ,BRAND ,BILL_CYCLE ,CUST_TYPE ,CUST_TYPE_DESC ,CUST_SUBTYPE ,CUST_SUBTYPE_DESC ,HYBRID_FLAG ,PRE_TO_POST_FLAG, LACCI_ID, LAC ,CI ,CGI_POST ,NODE ,AREA ,
    	REGION ,BRANCH ,SUBBRANCH ,CLUSTER ,KABUPATEN ,LCTN_CLOSING_FLAG, FILE_DATE 
	FROM ${table-prefix}${scn-schema}.${table-prefix}cb_post_destination
	UNION ALL
	SELECT TRX_DATE ,SUBS_ID ,MSISDN ,ACCOUNT_ID,SUBS_STATUS ,ACTIVATION_DATE ,ORIGIN_ACTIVATION_DATE ,PREFIX ,AREA_HLR ,REGION_HLR ,CITY_HLR ,PRICE_PLAN_ID ,
    	OFFER ,
        CASE WHEN BRAND = 'KartuHALO' THEN 'kartuHALO'
        ELSE BRAND END AS BRAND,
        BILL_CYCLE ,CUST_TYPE ,CUST_TYPE_DESC ,CUST_SUBTYPE ,CUST_SUBTYPE_DESC ,HYBRID_FLAG ,PRE_TO_POST_FLAG, LACCI_ID, LAC ,CI ,CGI_POST ,NODE ,AREA ,
    	REGION ,BRANCH ,SUBBRANCH ,CLUSTER ,KABUPATEN ,LCTN_CLOSING_FLAG, TIMESTAMP('${run-date=yyyy-MM-dd}') AS FILE_DATE
    FROM ${table-prefix}${scn-schema}.${table-prefix}cb_post_staging
);
DROP TABLE ${table-prefix}${scn-schema}.${table-prefix}cb_post_destination;
ALTER TABLE ${table-prefix}${scn-schema}.${table-prefix}cb_post_tmpDestination RENAME TO ${table-prefix}cb_post_destination;
DELETE FROM ${table-prefix}${scn-schema}.${table-prefix}cb_post_destination WHERE FILE_DATE = TIMESTAMP(DATE('${run-date=yyyy-MM-dd}') - INTERVAL '2' DAY);

ALTER TABLE ${table-prefix}${scn-schema}.${table-prefix}cb_post_destination MODIFY ACCOUNT_ID        INTEGER(dict);
ALTER TABLE ${table-prefix}${scn-schema}.${table-prefix}cb_post_destination MODIFY AREA              VARCHAR(8,dict);
ALTER TABLE ${table-prefix}${scn-schema}.${table-prefix}cb_post_destination MODIFY AREA_HLR          VARCHAR(16,dict);
ALTER TABLE ${table-prefix}${scn-schema}.${table-prefix}cb_post_destination MODIFY BILL_CYCLE        VARCHAR(8,dict);
ALTER TABLE ${table-prefix}${scn-schema}.${table-prefix}cb_post_destination MODIFY BRANCH            VARCHAR(64,dict);
ALTER TABLE ${table-prefix}${scn-schema}.${table-prefix}cb_post_destination MODIFY BRAND             VARCHAR(16,dict);
ALTER TABLE ${table-prefix}${scn-schema}.${table-prefix}cb_post_destination MODIFY CITY_HLR          VARCHAR(32,dict);
ALTER TABLE ${table-prefix}${scn-schema}.${table-prefix}cb_post_destination MODIFY CLUSTER           VARCHAR(32,dict);
ALTER TABLE ${table-prefix}${scn-schema}.${table-prefix}cb_post_destination MODIFY CUST_SUBTYPE      VARCHAR(1,dict);
ALTER TABLE ${table-prefix}${scn-schema}.${table-prefix}cb_post_destination MODIFY CUST_SUBTYPE_DESC VARCHAR(32,dict);
ALTER TABLE ${table-prefix}${scn-schema}.${table-prefix}cb_post_destination MODIFY CUST_TYPE         VARCHAR(1,dict);
ALTER TABLE ${table-prefix}${scn-schema}.${table-prefix}cb_post_destination MODIFY CUST_TYPE_DESC    VARCHAR(32,dict);
ALTER TABLE ${table-prefix}${scn-schema}.${table-prefix}cb_post_destination MODIFY HYBRID_FLAG       VARCHAR(8,dict);
ALTER TABLE ${table-prefix}${scn-schema}.${table-prefix}cb_post_destination MODIFY KABUPATEN         VARCHAR(32,dict);
ALTER TABLE ${table-prefix}${scn-schema}.${table-prefix}cb_post_destination MODIFY LCTN_CLOSING_FLAG VARCHAR(8,dict);
ALTER TABLE ${table-prefix}${scn-schema}.${table-prefix}cb_post_destination MODIFY NODE              VARCHAR(32,dict);
ALTER TABLE ${table-prefix}${scn-schema}.${table-prefix}cb_post_destination MODIFY OFFER             VARCHAR(64,dict);
ALTER TABLE ${table-prefix}${scn-schema}.${table-prefix}cb_post_destination MODIFY PRE_TO_POST_FLAG  VARCHAR(8,dict);
ALTER TABLE ${table-prefix}${scn-schema}.${table-prefix}cb_post_destination MODIFY PREFIX            INTEGER(dict);
ALTER TABLE ${table-prefix}${scn-schema}.${table-prefix}cb_post_destination MODIFY PRICE_PLAN_ID     INTEGER(dict);
ALTER TABLE ${table-prefix}${scn-schema}.${table-prefix}cb_post_destination MODIFY REGION            VARCHAR(32,dict);
ALTER TABLE ${table-prefix}${scn-schema}.${table-prefix}cb_post_destination MODIFY REGION_HLR        VARCHAR(32,dict);
ALTER TABLE ${table-prefix}${scn-schema}.${table-prefix}cb_post_destination MODIFY SUBBRANCH         VARCHAR(64,dict);
ALTER TABLE ${table-prefix}${scn-schema}.${table-prefix}cb_post_destination MODIFY SUBS_STATUS       VARCHAR(8,dict);

CREATE OR REPLACE TABLE ${table-prefix}${scn-schema}.${table-prefix}sales_pre_tmpDestination AS (
	SELECT ACTIVATION_DATE, SUBS_ID, MSISDN, PAYCHANNEL, PREFIX, AREA_HLR, REGION_HLR, CITY_HLR, PRICE_PLAN_ID, OFFER, BRAND, CUST_TYPE_DESC, DO_DATE,
		DEALER_CODE, ITEM_CODE, DIST_TYPE, FIRST_USAGE_DATE, LACCI_ID, LAC, CI, CGI_PREPAID, NODE, AREA, REGION, BRANCH, SUBBRANCH, CLUSTER, KABUPATEN, LCTN_CLOSING_FLAG,
		FILE_DATE
	FROM ${table-prefix}${scn-schema}.${table-prefix}sales_pre_destination
	UNION ALL
	SELECT ACTIVATION_DATE, SUBS_ID, MSISDN, PAYCHANNEL, PREFIX, AREA_HLR, REGION_HLR, CITY_HLR, PRICE_PLAN_ID, OFFER,
        CASE WHEN BRAND = 'Kartu AS' THEN 'KartuAS'
        WHEN BRAND IN ('KartuHALO', 'kartuHALO') THEN 'simPATI'
        ELSE BRAND END AS BRAND,
        CUST_TYPE_DESC, DO_DATE, 
		DEALER_CODE, ITEM_CODE, DIST_TYPE, FIRST_USAGE_DATE, LACCI_ID, LAC, CI, CGI_PREPAID, NODE, AREA, REGION, BRANCH, SUBBRANCH, CLUSTER, KABUPATEN, LCTN_CLOSING_FLAG,
		TIMESTAMP('${run-date=yyyy-MM-dd}') AS FILE_DATE
	FROM ${table-prefix}${scn-schema}.${table-prefix}sales_pre_staging
);
DROP TABLE ${table-prefix}${scn-schema}.${table-prefix}sales_pre_destination;
ALTER TABLE ${table-prefix}${scn-schema}.${table-prefix}sales_pre_tmpDestination RENAME TO ${table-prefix}sales_pre_destination;
DELETE FROM ${table-prefix}${scn-schema}.${table-prefix}sales_pre_destination WHERE FILE_DATE = TIMESTAMP(DATE('${run-date=yyyy-MM-dd}') - INTERVAL '2' DAY);

CREATE OR REPLACE TABLE ${table-prefix}${scn-schema}.${table-prefix}sales_post_tmpDestination AS (
	SELECT ACTIVATION_DATE, SUBS_ID, MSISDN, ACCOUNT_ID, PREFIX, AREA_HLR, REGION_HLR, CITY_HLR, PRICE_PLAN_ID, OFFER, BRAND, BILL_CYCLE, CUST_TYPE, 
		CUST_TYPE_DESC, CUST_SUBTYPE, CUST_SUBTYPE_DESC, PRE_TO_POST_FLAG, CUST_TYPE_CHANGE_FLAG, PREV_CUST_TYPE, PREV_CUST_SUBTYPE, OWNERSHIP_CHANGE_FLAG, FIRST_USAGE_DATE, 
		LACCI_ID, LAC, CI, CGI_POSTPAID, NODE, AREA, REGION, BRANCH, SUBBRANCH, CLUSTER, KABUPATEN, LCTN_CLOSING_FLAG, FILE_DATE
	FROM ${table-prefix}${scn-schema}.${table-prefix}sales_post_destination
	UNION ALL
	SELECT ACTIVATION_DATE, SUBS_ID, MSISDN, ACCOUNT_ID, PREFIX, AREA_HLR, REGION_HLR, CITY_HLR, PRICE_PLAN_ID, OFFER, 
        CASE WHEN BRAND = 'KartuHALO' THEN 'kartuHALO'
        ELSE BRAND END AS BRAND,
        BILL_CYCLE, CUST_TYPE, 
		CUST_TYPE_DESC, CUST_SUBTYPE, CUST_SUBTYPE_DESC, PRE_TO_POST_FLAG, CUST_TYPE_CHANGE_FLAG, PREV_CUST_TYPE, PREV_CUST_SUBTYPE, OWNERSHIP_CHANGE_FLAG, FIRST_USAGE_DATE, 
		LACCI_ID, LAC, CI, CGI_POSTPAID, NODE, AREA, REGION, BRANCH, SUBBRANCH, CLUSTER, KABUPATEN, LCTN_CLOSING_FLAG, TIMESTAMP('${run-date=yyyy-MM-dd}') AS FILE_DATE
	FROM ${table-prefix}${scn-schema}.${table-prefix}sales_post_staging
);
DROP TABLE ${table-prefix}${scn-schema}.${table-prefix}sales_post_destination;
ALTER TABLE ${table-prefix}${scn-schema}.${table-prefix}sales_post_tmpDestination RENAME TO ${table-prefix}sales_post_destination;
DELETE FROM ${table-prefix}${scn-schema}.${table-prefix}sales_post_destination WHERE FILE_DATE = TIMESTAMP(DATE('${run-date=yyyy-MM-dd}') - INTERVAL '2' DAY);

CREATE OR REPLACE TABLE ${table-prefix}${scn-schema}.${table-prefix}churn_pre_tmpDestination AS (
	SELECT DEACTIVATION_DATE, SUBS_ID, MSISDN, PAYCHANNEL, PREFIX, AREA_HLR, REGION_HLR, CITY_HLR, PRICE_PLAN_ID, OFFER, BRAND, CUST_TYPE_DESC, DO_DATE,
 		DEALER_CODE, ITEM_CODE, DIST_TYPE, LAST_USAGE_DATE, LACCI_ID, LAC, CI, CGI_PRE, NODE, AREA, REGION, BRANCH, SUBBRANCH, CLUSTER, KABUPATEN, LCTN_CLOSING_FLAG, 
 		FILE_DATE
 	FROM ${table-prefix}${scn-schema}.${table-prefix}churn_pre_destination
	UNION ALL
	SELECT DEACTIVATION_DATE , SUBS_ID, MSISDN, PAYCHANNEL, PREFIX, AREA_HLR, REGION_HLR, CITY_HLR, PRICE_PLAN_ID, OFFER, 
        CASE WHEN BRAND = 'Kartu AS' THEN 'KartuAS'
        WHEN BRAND IN ('KartuHALO', 'kartuHALO') THEN 'simPATI'
        ELSE BRAND END AS BRAND,
        CUST_TYPE_DESC, DO_DATE,
 		DEALER_CODE, ITEM_CODE, DIST_TYPE, LAST_USAGE_DATE, LACCI_ID, LAC, CI, CGI_PRE, NODE, AREA, REGION, BRANCH, SUBBRANCH, CLUSTER, KABUPATEN, LCTN_CLOSING_FLAG,
 		TIMESTAMP('${run-date=yyyy-MM-dd}') AS FILE_DATE
 	FROM ${table-prefix}${scn-schema}.${table-prefix}churn_pre_staging
);
DROP TABLE ${table-prefix}${scn-schema}.${table-prefix}churn_pre_destination;
ALTER TABLE ${table-prefix}${scn-schema}.${table-prefix}churn_pre_tmpDestination RENAME TO ${table-prefix}churn_pre_destination;
DELETE FROM ${table-prefix}${scn-schema}.${table-prefix}churn_pre_destination WHERE FILE_DATE = TIMESTAMP(DATE('${run-date=yyyy-MM-dd}') - INTERVAL '2' DAY);

CREATE OR REPLACE TABLE ${table-prefix}${scn-schema}.${table-prefix}churn_post_tmpDestination AS (
	SELECT DEACTIVATION_DATE, SUBS_ID, MSISDN, ACCOUNT_ID, PREFIX, AREA_HLR, REGION_HLR, CITY_HLR, PRICE_PLAN_ID, OFFER, BRAND, BILL_CYCLE, CUST_TYPE,
 		CUST_TYPE_DESC, CUST_SUBTYPE, CUST_SUBTYPE_DESC, CUST_TYPE_CHANGE_FLAG, NEXT_CUST_TYPE, NEXT_CUST_SUBTYPE, OWNERSHIP_CHANGE_FLAG, LAST_USAGE_DATE, LACCI_ID, LAC, CI,
 		CGI_POST, AREA, REGION, BRANCH, SUBBRANCH, CLUSTER, KABUPATEN, NODE, FILE_DATE
 	FROM ${table-prefix}${scn-schema}.${table-prefix}churn_post_destination
	UNION ALL
	SELECT DEACTIVATION_DATE , SUBS_ID, MSISDN, ACCOUNT_ID, PREFIX, AREA_HLR, REGION_HLR, CITY_HLR, PRICE_PLAN_ID, OFFER, 
        CASE WHEN BRAND = 'KartuHALO' THEN 'kartuHALO'
        ELSE BRAND END AS BRAND,
        BILL_CYCLE, CUST_TYPE,
 		CUST_TYPE_DESC, CUST_SUBTYPE, CUST_SUBTYPE_DESC, CUST_TYPE_CHANGE_FLAG, NEXT_CUST_TYPE, NEXT_CUST_SUBTYPE, OWNERSHIP_CHANGE_FLAG, LAST_USAGE_DATE, LACCI_ID, LAC, CI,
 		CGI_POST, AREA, REGION, BRANCH, SUBBRANCH, CLUSTER, KABUPATEN, NODE, TIMESTAMP('${run-date=yyyy-MM-dd}') AS FILE_DATE
 	FROM ${table-prefix}${scn-schema}.${table-prefix}churn_post_staging
);
DROP TABLE ${table-prefix}${scn-schema}.${table-prefix}churn_post_destination;
ALTER TABLE ${table-prefix}${scn-schema}.${table-prefix}churn_post_tmpDestination RENAME TO ${table-prefix}churn_post_destination;
DELETE FROM ${table-prefix}${scn-schema}.${table-prefix}churn_post_destination WHERE FILE_DATE = TIMESTAMP(DATE('${run-date=yyyy-MM-dd}') - INTERVAL '2' DAY);

ALTER TABLE ${table-prefix}${scn-schema}.${table-prefix}churn_pre_destination MODIFY AREA              VARCHAR(8,dict);
ALTER TABLE ${table-prefix}${scn-schema}.${table-prefix}churn_pre_destination MODIFY AREA_HLR          VARCHAR(16,dict);
ALTER TABLE ${table-prefix}${scn-schema}.${table-prefix}churn_pre_destination MODIFY BRANCH            VARCHAR(64,dict);
ALTER TABLE ${table-prefix}${scn-schema}.${table-prefix}churn_pre_destination MODIFY BRAND             VARCHAR(16,dict);
ALTER TABLE ${table-prefix}${scn-schema}.${table-prefix}churn_pre_destination MODIFY CITY_HLR          VARCHAR(32,dict);
ALTER TABLE ${table-prefix}${scn-schema}.${table-prefix}churn_pre_destination MODIFY CLUSTER           VARCHAR(32,dict);
ALTER TABLE ${table-prefix}${scn-schema}.${table-prefix}churn_pre_destination MODIFY CUST_TYPE_DESC    VARCHAR(32,dict);
ALTER TABLE ${table-prefix}${scn-schema}.${table-prefix}churn_pre_destination MODIFY DEALER_CODE       VARCHAR(8,dict);
ALTER TABLE ${table-prefix}${scn-schema}.${table-prefix}churn_pre_destination MODIFY DIST_TYPE         VARCHAR(16,dict);
ALTER TABLE ${table-prefix}${scn-schema}.${table-prefix}churn_pre_destination MODIFY DO_DATE           VARCHAR(16,dict);
ALTER TABLE ${table-prefix}${scn-schema}.${table-prefix}churn_pre_destination MODIFY ITEM_CODE         VARCHAR(16,dict);
ALTER TABLE ${table-prefix}${scn-schema}.${table-prefix}churn_pre_destination MODIFY KABUPATEN         VARCHAR(32,dict);
ALTER TABLE ${table-prefix}${scn-schema}.${table-prefix}churn_pre_destination MODIFY LCTN_CLOSING_FLAG VARCHAR(8,dict);
ALTER TABLE ${table-prefix}${scn-schema}.${table-prefix}churn_pre_destination MODIFY NODE              VARCHAR(32,dict);
ALTER TABLE ${table-prefix}${scn-schema}.${table-prefix}churn_pre_destination MODIFY OFFER             VARCHAR(64,dict);
ALTER TABLE ${table-prefix}${scn-schema}.${table-prefix}churn_pre_destination MODIFY PAYCHANNEL        VARCHAR(8,dict);
ALTER TABLE ${table-prefix}${scn-schema}.${table-prefix}churn_pre_destination MODIFY PREFIX            INTEGER(dict);
ALTER TABLE ${table-prefix}${scn-schema}.${table-prefix}churn_pre_destination MODIFY PRICE_PLAN_ID     INTEGER(dict);
ALTER TABLE ${table-prefix}${scn-schema}.${table-prefix}churn_pre_destination MODIFY REGION            VARCHAR(32,dict);
ALTER TABLE ${table-prefix}${scn-schema}.${table-prefix}churn_pre_destination MODIFY REGION_HLR        VARCHAR(32,dict);
ALTER TABLE ${table-prefix}${scn-schema}.${table-prefix}churn_pre_destination MODIFY SUBBRANCH         VARCHAR(64,dict);

-- DROP TABLE cb_pre_staging;
-- DROP TABLE cb_post_staging;
-- DROP TABLE sales_pre_staging;
-- DROP TABLE sales_post_staging;
-- DROP TABLE churn_pre_staging;
-- DROP TABLE churn_post_staging;
