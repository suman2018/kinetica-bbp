delete from ${table-prefix}${cms-schema}.${table-prefix}cms_campaign_tracking_detail
where file_date = date('${run-date=yyyy-MM-dd}')
and pipeline_name = '${pipeline-name}'
and pipeline_step = '${pipeline-step}';

/* KI_HINT_DICT_PROJECTION */
create or replace temp table ${table-prefix}${cms-schema}.${table-prefix}${pipeline-name}_tmp
as
select 
	cview.category
	,cview.campaign_type
	,cview.msisdn
	,cview.campaign_id
	,cview.campaign_objective_id
	,cview.campaign_name
	,cview.start_period
	,cview.end_period
	,cview.iscontrolgroup
	,cview.campaign_group_id
	,cview.segment
	,cview.los
	,cview.arpu
	,cview.business_group
	,cview.initiated_group
	--,cview.area_sales
	,cview.region_hlr
	,cview.cluster
	,cview.kabupaten
	,date('${run-date=yyyy-MM-dd}') as trx_date
	--,case when cview.msisdn is null then 0 else 1 end as metric_1
	,0 as is_btl
	,cview.target
	,cview.eligible
	,case when cview.eligible > 0 and upper(cview.device_type) like '%SMARTPHONE%' then 1 else 0 end as taker
	,date('${run-date=yyyy-MM-dd}') as file_date
	,'${job-id}'
	,'${pipeline-name}'
	,'${pipeline-step}'
from ${table-prefix}${cms-schema}.${table-prefix}cms_campaign_trackingstream_multidim_v cview
where cview.campaign_group_id = 10
	and date('${run-date=yyyy-MM-dd}') between date(cview.start_period) and date(cview.end_period)
;
	
insert into ${table-prefix}${cms-schema}.${table-prefix}cms_campaign_tracking_detail
(
	 category
	,campaign_type
	,msisdn
	,campaign_id
	,campaign_objective_id
	,campaign_name
	,start_period
	,end_period
	,iscontrolgroup
	,campaign_group_id
	,segment
	,los
	,arpu
	,business_group
	,initiated_group
	--,area
	,region_hlr
	,cluster
	,branch
	,trx_date
	--,metric_1
	,is_btl
	,target
	,eligible
	,taker
	,file_date
	,orch_job_id
	,pipeline_name
	,pipeline_step
)
select * from ${table-prefix}${cms-schema}.${table-prefix}${pipeline-name}_tmp
;

drop table ${table-prefix}${cms-schema}.${table-prefix}${pipeline-name}_tmp
;