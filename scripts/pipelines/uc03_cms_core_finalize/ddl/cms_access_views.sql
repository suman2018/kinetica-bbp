create or replace materialized view ${table-prefix}${cms_out-schema}.${table-prefix}cms_campaign_tracking_detail_v
refresh every 24 hour
as
select
	*
from ${table-prefix}${cms-schema}.${table-prefix}cms_campaign_tracking_detail
;

create or replace materialized view ${table-prefix}${cms_out-schema}.${table-prefix}cms_campaign_definition_v
refresh every 24 hour
as
select
	*
from ${table-prefix}${cms-schema}.${table-prefix}cms_campaign_definition
;

