delete from ${table-prefix}${cms-schema}.${table-prefix}cms_campaign_summary
where periode = last_day(date('${run-date=yyyy-MM-dd}') - interval 1 month)
or periode = last_day(date('${run-date=yyyy-MM-dd}'))
;

--last month
insert into ${table-prefix}${cms-schema}.${table-prefix}cms_campaign_summary
select 
	last_day(date('${run-date=yyyy-MM-dd}') - interval 1 month) as periode
	,main.category
	,main.campaign_type
	,main.campaign_id
	,main.campaign_objective_id
	,main.campaign_objective_desc
	,main.campaign_name
	,main.start_period
	,main.end_period
	,main.campaign_group_id
	,main.segment
	,main.business_group
	,main.initiated_business
	,main.region_hlr
	,main.cluster
	,main.branch
	,min(unq.target_submitted) as target_submitted
	,min(unq.target_eligible) as target_eligible
	,min(unq.control_eligible) as control_eligible
	,sum(case when main.iscontrolgroup = 'false' then main.revenue end) as revenue
	,sum(case when main.iscontrolgroup = 'false' then main.taker end) as taker
	,min(unq.unique_taker) as unique_taker
	,sum(case when iscontrolgroup = 'true' then main.revenue end) as revenue_control
	,sum(case when main.iscontrolgroup = 'true' then main.taker end) as taker_control
	,min(unq.unique_taker_control) as unique_taker_control
	,min(coalesce(arpu.rev_before, 0))/min(unq.target_eligible) as arpu_before
	,min(coalesce(arpu.rev_after, 0))/min(unq.target_eligible) as arpu_after
	,min(coalesce(arpu.rev_control_before, 0))/min(unq.control_eligible) as arpu_control_before
	,min(coalesce(arpu.rev_control_after, 0))/min(unq.control_eligible) as arpu_control_after
	,date('${run-date=yyyy-MM-dd}') as file_date
    ,'${job-id}'
    ,'${pipeline-name}'
    ,'${pipeline-step}'
from 
(
	select *
	from ${table-prefix}${cms-schema}.${table-prefix}cms_campaign_performance_mm 
	where eom = 1
		and pipeline_step not in ('cms_usage_detail_insert', 'cms_campaign_digital_detail_insert')
		and (
			last_day(date(start_period)) = last_day(date('${run-date=yyyy-MM-dd}') - interval 1 month)
			or last_day(date(end_period)) = last_day(date('${run-date=yyyy-MM-dd}') - interval 1 month)
		)
) main 
join 
(
	select 
		campaign_id
		,count(distinct msisdn) as target_submitted
		,count(distinct case when eligible > 0 and iscontrolgroup = 'false' then msisdn end) as target_eligible
		,count(distinct case when eligible > 0 and iscontrolgroup = 'true' then msisdn end) as control_eligible
		,count(distinct case when taker > 0 and iscontrolgroup = 'false' then msisdn end) as unique_taker
		,count(distinct case when taker > 0 and iscontrolgroup = 'true' then msisdn end) as unique_taker_control
	from ${table-prefix}${cms-schema}.${table-prefix}cms_campaign_performance_mm
	where  eom = 1
		and pipeline_step not in ('cms_usage_detail_insert', 'cms_campaign_digital_detail_insert')
		and (
			last_day(date(start_period)) = last_day(date('${run-date=yyyy-MM-dd}') - interval 1 month)
			or last_day(date(end_period)) = last_day(date('${run-date=yyyy-MM-dd}') - interval 1 month)
		)
	group by campaign_id
) unq
on main.campaign_id = unq.campaign_id
left join
(
	select 
		perf.campaign_id
		,sum(case when datediff(perf.start_period,chg.trx_date) >  0 and iscontrolgroup = 'false' then chg.rev end) as rev_before
		,sum(case when datediff(chg.trx_date,perf.start_period) >= 0 and iscontrolgroup = 'false' then chg.rev end) as rev_after
		,sum(case when datediff(perf.start_period,chg.trx_date) >  0 and iscontrolgroup = 'true' then chg.rev end)  as rev_control_before
		,sum(case when datediff(chg.trx_date,perf.start_period) >= 0 and iscontrolgroup = 'true' then chg.rev end)  as rev_control_after
	from
	(
	    select distinct campaign_id, msisdn, start_period, end_period, periode, iscontrolgroup, case when diff < 30 then diff else 30 end as offset  
	    from 
	    (
	    	select campaign_id, msisdn, start_period, end_period, periode, iscontrolgroup, datediff(date('${run-date=yyyy-MM-dd}'), date(start_period)) as diff
	    	from ${table-prefix}${cms-schema}.${table-prefix}cms_campaign_performance_mm
	    	where periode = last_day(date('${run-date=yyyy-MM-dd}') - interval 1 month)
	    		and eligible > 0
	    		and eom = 1
	    		and (last_day(date(start_period)) = periode
				or last_day(date(end_period)) = periode)
	    )
	) perf
	left join ${table-prefix}${cms-schema}.${table-prefix}cms_chg_detail chg
	on chg.msisdn = perf.msisdn
	    and chg.trx_date between date(start_period) - interval perf.offset+1 day and date(start_period) + interval perf.offset day
	group by 1
) arpu
on main.campaign_id = arpu.campaign_id
group by
	main.category
	,main.campaign_type
	,main.campaign_id
	,main.campaign_objective_id
	,main.campaign_objective_desc
	,main.campaign_name
	,main.start_period
	,main.end_period
	,main.campaign_group_id
	,main.segment
	,main.business_group
	,main.initiated_business
	,main.region_hlr
	,main.cluster
	,main.branch
;

	
--current month
insert into ${table-prefix}${cms-schema}.${table-prefix}cms_campaign_summary
select 
	last_day(date('${run-date=yyyy-MM-dd}')) as periode
	,main.category
	,main.campaign_type
	,main.campaign_id
	,main.campaign_objective_id
	,main.campaign_objective_desc
	,main.campaign_name
	,main.start_period
	,main.end_period
	,main.campaign_group_id
	,main.segment
	,main.business_group
	,main.initiated_business
	,main.region_hlr
	,main.cluster
	,main.branch
	,min(unq.target_submitted) as target_submitted
	,min(unq.target_eligible) as target_eligible
	,min(unq.control_eligible) as control_eligible
	,sum(case when main.iscontrolgroup = 'false' then main.revenue end) as revenue
	,sum(case when main.iscontrolgroup = 'false' then main.taker end) as taker
	,min(unq.unique_taker) as unique_taker
	,sum(case when iscontrolgroup = 'true' then main.revenue end) as revenue_control
	,sum(case when main.iscontrolgroup = 'true' then main.taker end) as taker_control
	,min(unq.unique_taker_control) as unique_taker_control
	,min(coalesce(arpu.rev_before, 0))/min(unq.target_eligible) as arpu_before
	,min(coalesce(arpu.rev_after, 0))/min(unq.target_eligible) as arpu_after
	,min(coalesce(arpu.rev_control_before, 0))/min(unq.control_eligible) as arpu_control_before
	,min(coalesce(arpu.rev_control_after, 0))/min(unq.control_eligible) as arpu_control_after
	,date('${run-date=yyyy-MM-dd}') as file_date
    ,'${job-id}'
    ,'${pipeline-name}'
    ,'${pipeline-step}'
from 
(
	select *
	from ${table-prefix}${cms-schema}.${table-prefix}cms_campaign_performance_mm 
	where eom = 0
		and (
			last_day(date(start_period)) = last_day(date('${run-date=yyyy-MM-dd}'))
			or last_day(date(end_period)) = last_day(date('${run-date=yyyy-MM-dd}'))
		)
) main 
join 
(
	select 
		campaign_id
		,count(distinct msisdn) as target_submitted
		,count(distinct case when eligible > 0 and iscontrolgroup = 'false' then msisdn end) as target_eligible
		,count(distinct case when eligible > 0 and iscontrolgroup = 'true' then msisdn end) as control_eligible
		,count(distinct case when taker > 0 and iscontrolgroup = 'false' then msisdn end) as unique_taker
		,count(distinct case when taker > 0 and iscontrolgroup = 'true' then msisdn end) as unique_taker_control
	from ${table-prefix}${cms-schema}.${table-prefix}cms_campaign_performance_mm
	where eom = 0
		and (
			last_day(date(start_period)) = last_day(date('${run-date=yyyy-MM-dd}'))
			or last_day(date(end_period)) = last_day(date('${run-date=yyyy-MM-dd}'))
		)
	group by campaign_id
) unq
on main.campaign_id = unq.campaign_id
left join
(
	select 
		perf.campaign_id
		,sum(case when datediff(perf.start_period,chg.trx_date) >  0 and iscontrolgroup = 'false' then chg.rev end) as rev_before
		,sum(case when datediff(chg.trx_date,perf.start_period) >= 0 and iscontrolgroup = 'false' then chg.rev end) as rev_after
		,sum(case when datediff(perf.start_period,chg.trx_date) >  0 and iscontrolgroup = 'true' then chg.rev end)  as rev_control_before
		,sum(case when datediff(chg.trx_date,perf.start_period) >= 0 and iscontrolgroup = 'true' then chg.rev end)  as rev_control_after
	from
	(
	    select distinct campaign_id, msisdn, start_period, end_period, periode, iscontrolgroup, case when diff < 30 then diff else 30 end as offset  
	    from 
	    (
	    	select campaign_id, msisdn, start_period, end_period, periode, iscontrolgroup, datediff(date('${run-date=yyyy-MM-dd}'), date(start_period)) as diff
	    	from ${table-prefix}${cms-schema}.${table-prefix}cms_campaign_performance_mm
	    	where periode = last_day(date('${run-date=yyyy-MM-dd}'))
	    		and eligible > 0
	    		and eom = 0
	    		and (last_day(date(start_period)) = periode
				or last_day(date(end_period)) = periode)
	    )
	) perf
	left join ${table-prefix}${cms-schema}.${table-prefix}cms_chg_detail chg
	on chg.msisdn = perf.msisdn
	    and chg.trx_date between date(start_period) - interval perf.offset+1 day and date(start_period) + interval perf.offset day
	group by 1
) arpu
on main.campaign_id = arpu.campaign_id
group by
	main.category
	,main.campaign_type
	,main.campaign_id
	,main.campaign_objective_id
	,main.campaign_objective_desc
	,main.campaign_name
	,main.start_period
	,main.end_period
	,main.campaign_group_id
	,main.segment
	,main.business_group
	,main.initiated_business
	,main.region_hlr
	,main.cluster
	,main.branch
;