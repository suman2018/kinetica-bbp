alter table ${table-prefix}${cms-schema}.${table-prefix}cms_campaign_performance_dd
rename to ${table-prefix}cms_campaign_performance_dd_old
;

create or replace table ${table-prefix}${cms-schema}.${table-prefix}cms_campaign_performance_dd
(
    "category" VARCHAR(16, dict),
    "campaign_type" VARCHAR(32, dict),
    "campaign_name" VARCHAR(128, dict),
    "campaign_group_id" INTEGER(dict),
    "campaign_id" VARCHAR(64, dict, shard_key),
    "campaign_objective_id" INTEGER(dict),
    "campaign_objective_desc" VARCHAR(64, dict),
    "business_group" VARCHAR(8, dict),
    "initiated_business" VARCHAR(32, dict),
    "start_period" TIMESTAMP,
    "end_period" TIMESTAMP,
    "segment_name" VARCHAR(256, dict),
    "trx_date" DATE(dict) NOT NULL,
    "is_btl" INTEGER,
	"target_submitted" INTEGER,
    "target_eligible" INTEGER,
    "control_eligible" INTEGER,
    "revenue" BIGINT,
    "taker" INTEGER,
    "revenue_control" BIGINT,
    "taker_control" INTEGER,
	"file_date" DATE(dict) NOT NULL,
    "orch_job_id" VARCHAR(8, dict) NOT NULL,
    "pipeline_name" VARCHAR(64, dict) NOT NULL,
    "pipeline_step" VARCHAR(64, dict) NOT NULL
)
;

insert into ${table-prefix}${cms-schema}.${table-prefix}cms_campaign_performance_dd
select
	 category
	,campaign_type
	,campaign_name
	,campaign_group_id
	,campaign_id
	,campaign_objective_id
	,campaign_objective_desc
	,business_group
	,initiated_business
	,start_period
	,end_period
	,segment_name
	,trx_date
	,is_btl
	,target_submitted
	,target_eligible
	,control_eligible
	,revenue
	,taker
	,revenue_control
	,taker_control
	,file_date
	,orch_job_id
	,pipeline_name
	,pipeline_step
from ${table-prefix}${cms-schema}.${table-prefix}cms_campaign_performance_dd_old
where trx_date not in 
(
	select trx_date 
	from ${table-prefix}${cms-schema}.${table-prefix}cms_campaign_tracking_detail
	where file_date = date('${run-date=yyyy-MM-dd}')
)
;


drop table if exists ${table-prefix}${cms-schema}.${table-prefix}cms_campaign_performance_dd_old;