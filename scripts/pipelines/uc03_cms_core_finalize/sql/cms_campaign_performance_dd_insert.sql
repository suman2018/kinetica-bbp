insert into ${table-prefix}${cms-schema}.${table-prefix}cms_campaign_performance_dd
(
         category
        ,campaign_type
        ,campaign_name
        ,campaign_group_id
        ,campaign_id
        ,campaign_objective_id
        ,campaign_objective_desc
        ,business_group
        ,initiated_business
        ,start_period
        ,end_period
        ,segment_name
        ,trx_date
        ,is_btl
        ,target_submitted
        ,target_eligible
        ,control_eligible
        ,revenue
        ,taker
		,revenue_control
		,taker_control
        ,file_date
        ,orch_job_id
        ,pipeline_name
        ,pipeline_step
)
select
         dtl.category
        ,dtl.campaign_type
        ,dtl.campaign_name
        ,dtl.campaign_group_id
        ,dtl.campaign_id
        ,dtl.campaign_objective_id
        ,obj.campaign_objective_desc
        ,dtl.business_group
        ,dtl.initiated_group
        ,dtl.start_period
        ,dtl.end_period
        ,dtl.segment
        ,dtl.trx_date
        ,dtl.is_btl
		,count(distinct msisdn) as target_submitted
		,count(distinct case when eligible > 0 and iscontrolgroup = 'false' then msisdn end) as target_eligible
		,count(distinct case when eligible > 0 and iscontrolgroup = 'true' then msisdn end) as control_eligible
		,sum(case when taker > 0 and iscontrolgroup = 'false' then metric_1 end) as revenue
		,sum(
			case 
				when campaign_group_id in (2) and iscontrolgroup = 'false' and taker > 0 then metric_2
				when iscontrolgroup = 'false' then taker 
			end
		) as taker
		,sum(case when taker > 0 and iscontrolgroup = 'true' then metric_1 end) as revenue_control
		,sum(
			case 
				when campaign_group_id in (2) and iscontrolgroup = 'true' and taker > 0 then metric_2
				when iscontrolgroup = 'true' then taker 
			end
		) as taker_control
        ,date('${run-date=yyyy-MM-dd}') as file_date
	    ,'${job-id}'
	    ,'${pipeline-name}'
	    ,'${pipeline-step}'
from 
(
	select *
	from ${table-prefix}${cms-schema}.${table-prefix}cms_campaign_tracking_detail 
	where pipeline_step not in ('cms_usage_detail_insert', 'cms_campaign_digital_detail_insert')
		and trx_date in 
		(
			select trx_date 
			from ${table-prefix}${cms-schema}.${table-prefix}cms_campaign_tracking_detail
			where file_date = date('${run-date=yyyy-MM-dd}')
		)
) dtl
left join 
(
	select distinct
		campaign_objective_id
		,campaign_objective_desc
	from ${table-prefix}${cms-schema}.${table-prefix}cms_campaign_objective
) obj
on dtl.campaign_objective_id = obj.campaign_objective_id
group by
         dtl.category
        ,dtl.campaign_type
        ,dtl.campaign_name
        ,dtl.campaign_group_id
        ,dtl.campaign_id
        ,dtl.campaign_objective_id
        ,obj.campaign_objective_desc
        ,dtl.business_group
        ,dtl.initiated_group
        ,dtl.start_period
        ,dtl.end_period
        ,dtl.segment
        ,dtl.trx_date
        ,dtl.is_btl

