delete from ${table-prefix}${cms-schema}.${table-prefix}cms_campaign_performance_mm
where periode = last_day(date('${run-date=yyyy-MM-dd}') - interval 1 month)
	or eom = 0
;

insert into ${table-prefix}${cms-schema}.${table-prefix}cms_campaign_performance_mm
select
	dtl.periode
	,dtl.eom
	,dtl.category
	,dtl.campaign_type
	,dtl.msisdn
	,dtl.campaign_id
	,dtl.campaign_objective_id
	,obj.campaign_objective_desc
	,dtl.campaign_name
	,dtl.start_period
	,dtl.end_period
	,dtl.iscontrolgroup
	,dtl.campaign_group_id
	,dtl.segment
	,dtl.business_group
	,dtl.initiated_group
	,dtl.region_hlr
	,dtl.cluster
	,dtl.branch
	,dtl.is_btl
	,dtl.target
	,dtl.eligible
    ,dtl.taker
    ,dtl.revenue
	,dtl.arpu
	,date('${run-date=yyyy-MM-dd}') as file_date
    ,'${job-id}'
    ,'${pipeline-name}'
    ,'${pipeline-step}'
from 
(
	select 	
		last_day(trx_date) as periode
		,0 as eom
		,category
		,campaign_type
		,msisdn
		,campaign_id
		,campaign_objective_id
		,campaign_name
		,start_period
		,end_period
		,iscontrolgroup
		,campaign_group_id
		,segment
		,business_group
		,initiated_group
		,region_hlr
		,cluster
		,branch
		,is_btl
		,sum(target) as target
		,sum(eligible) as eligible
	    ,sum
		(
	    	case
	    		when campaign_group_id in (2) and taker > 0 then metric_2
	    		else taker 
	    	end
	    ) as taker
	    ,sum(case when taker > 0 then metric_1 end) as revenue
		,sum(arpu) as arpu
	from ${table-prefix}${cms-schema}.${table-prefix}cms_campaign_tracking_detail
	where pipeline_step not in ('cms_usage_detail_insert', 'cms_campaign_digital_detail_insert')
		and (
			   trx_date between date(last_day(date('${run-date=yyyy-MM-dd}') - interval 3 month) + interval 1 day) and date(date('${run-date=yyyy-MM-dd}') - interval 2 month)
			or trx_date between date(last_day(date('${run-date=yyyy-MM-dd}') - interval 2 month) + interval 1 day) and date(date('${run-date=yyyy-MM-dd}') - interval 1 month)
			or trx_date between date(last_day(date('${run-date=yyyy-MM-dd}') - interval 1 month) + interval 1 day) and date(date('${run-date=yyyy-MM-dd}'))
		)
	group by
		last_day(trx_date)
		,category
		,campaign_type
		,msisdn
		,campaign_id
		,campaign_objective_id
		,campaign_name
		,start_period
		,end_period
		,iscontrolgroup
		,campaign_group_id
		,segment
		,business_group
		,initiated_group
		,region_hlr
		,cluster
		,branch
		,is_btl
) dtl
left join 
(
	select distinct
		campaign_objective_id
		,campaign_objective_desc
	from ${table-prefix}${cms-schema}.${table-prefix}cms_campaign_objective
) obj
on dtl.campaign_objective_id = obj.campaign_objective_id
;

insert into ${table-prefix}${cms-schema}.${table-prefix}cms_campaign_performance_mm
select
	dtl.periode
	,dtl.eom
	,dtl.category
	,dtl.campaign_type
	,dtl.msisdn
	,dtl.campaign_id
	,dtl.campaign_objective_id
	,obj.campaign_objective_desc
	,dtl.campaign_name
	,dtl.start_period
	,dtl.end_period
	,dtl.iscontrolgroup
	,dtl.campaign_group_id
	,dtl.segment
	,dtl.business_group
	,dtl.initiated_group
	,dtl.region_hlr
	,dtl.cluster
	,dtl.branch
	,dtl.is_btl
	,dtl.target
	,dtl.eligible
    ,dtl.taker
    ,dtl.revenue
	,dtl.arpu
	,date('${run-date=yyyy-MM-dd}') as file_date
    ,'${job-id}'
    ,'${pipeline-name}'
    ,'${pipeline-step}'
from
(
	select
		last_day(trx_date) as periode
		,1 as eom
		,category
		,campaign_type
		,msisdn
		,campaign_id
		,campaign_objective_id
		,campaign_name
		,start_period
		,end_period
		,iscontrolgroup
		,campaign_group_id
		,segment
		,business_group
		,initiated_group
		,region_hlr
		,cluster
		,branch
		,is_btl
		,sum(target) as target
		,sum(eligible) as eligible
	    ,sum
		(
	    	case
	    		when campaign_group_id in (2) and taker > 0 then metric_2
	    		else taker 
	    	end
	    ) as taker
	    ,sum(case when taker > 0 then metric_1 end) as revenue
		,sum(arpu) as arpu
	from ${table-prefix}${cms-schema}.${table-prefix}cms_campaign_tracking_detail
	where pipeline_step not in ('cms_usage_detail_insert', 'cms_campaign_digital_detail_insert')
		and (
	   		trx_date between date(last_day(date('${run-date=yyyy-MM-dd}') - interval 2 month) + interval 1 day) and last_day(date('${run-date=yyyy-MM-dd}') - interval 1 month)
	   	)
	group by
		last_day(trx_date)
		,category
		,campaign_type
		,msisdn
		,campaign_id
		,campaign_objective_id
		,campaign_name
		,start_period
		,end_period
		,iscontrolgroup
		,campaign_group_id
		,segment
		,business_group
		,initiated_group
		,region_hlr
		,cluster
		,branch
		,is_btl  
) dtl
left join 
(
	select distinct
		campaign_objective_id
		,campaign_objective_desc
	from ${table-prefix}${cms-schema}.${table-prefix}cms_campaign_objective
) obj
on dtl.campaign_objective_id = obj.campaign_objective_id
;
