create or replace table ${table-prefix}${msight_ooh-schema}.${table-prefix}ooh_summ_visit_freq_stg as
SELECT
months,
location_code,
cast(id_dtl as decimal(2,1)) as id_dtl,
cast(id_bearing as integer) as id_bearing,
cast(id_distance_ac as integer) as id_distance_ac,
cast(id_distance_cb as integer) as id_distance_cb,
CAST (c_1 as integer) as c_1,
CAST (c_2 as integer) as c_2,
CAST (c_3 as integer) as c_3,
CAST (c_4 as integer) as c_4,
CAST (c_5 as integer) as c_5,
CAST (c_6 as integer) as c_6,
CAST (c_7 as integer) as c_7,
CAST (c_8 as integer) as c_8,
CAST (c_9 as integer) as c_9,
CAST (c_10 as integer) as c_10,
CAST (c_11 as integer) as c_11,
CAST (c_12 as integer) as c_12,
CAST (c_13 as integer) as c_13,
CAST (c_14 as integer) as c_14,
CAST (c_15 as integer) as c_15,
CAST (c_16 as integer) as c_16,
CAST (c_17 as integer) as c_17,
CAST (c_18 as integer) as c_18,
CAST (c_19 as integer) as c_19,
CAST (c_20 as integer) as c_20,
CAST (c_21 as integer) as c_21,
CAST (c_22 as integer) as c_22,
CAST (c_23 as integer) as c_23,
CAST (c_24 as integer) as c_24,
CAST (c_25 as integer) as c_25,
CAST (c_26 as integer) as c_26,
CAST (c_27 as integer) as c_27,
CAST (c_28 as integer) as c_28,
CAST (c_29 as integer) as c_29,
CAST (c_30 as integer) as c_30,
ki_shard_key (id_bearing, id_dtl, id_distance_ac, id_distance_cb) 
FROM ${table-prefix}${msight_ooh-schema}.${table-prefix}ooh_summ_visit_freq_input;

alter table ${table-prefix}${msight_ooh-schema}.${table-prefix}ooh_summ_visit_freq_stg modify column months varchar(8, dict) ;
alter table ${table-prefix}${msight_ooh-schema}.${table-prefix}ooh_summ_visit_freq_stg modify column location_code varchar(32, dict) ;

alter table ${table-prefix}${msight_ooh-schema}.${table-prefix}ooh_summ_visit_freq_stg modify column c_1 integer (dict);
alter table ${table-prefix}${msight_ooh-schema}.${table-prefix}ooh_summ_visit_freq_stg modify column c_2 integer (dict);
alter table ${table-prefix}${msight_ooh-schema}.${table-prefix}ooh_summ_visit_freq_stg modify column c_3 integer (dict);
alter table ${table-prefix}${msight_ooh-schema}.${table-prefix}ooh_summ_visit_freq_stg modify column c_4 integer (dict);
alter table ${table-prefix}${msight_ooh-schema}.${table-prefix}ooh_summ_visit_freq_stg modify column c_5 integer (dict);
alter table ${table-prefix}${msight_ooh-schema}.${table-prefix}ooh_summ_visit_freq_stg modify column c_6 integer (dict);
alter table ${table-prefix}${msight_ooh-schema}.${table-prefix}ooh_summ_visit_freq_stg modify column c_7 integer (dict);
alter table ${table-prefix}${msight_ooh-schema}.${table-prefix}ooh_summ_visit_freq_stg modify column c_8 integer (dict);
alter table ${table-prefix}${msight_ooh-schema}.${table-prefix}ooh_summ_visit_freq_stg modify column c_9 integer (dict);
alter table ${table-prefix}${msight_ooh-schema}.${table-prefix}ooh_summ_visit_freq_stg modify column c_10 integer (dict);
alter table ${table-prefix}${msight_ooh-schema}.${table-prefix}ooh_summ_visit_freq_stg modify column c_11 integer (dict);
alter table ${table-prefix}${msight_ooh-schema}.${table-prefix}ooh_summ_visit_freq_stg modify column c_12 integer (dict);
alter table ${table-prefix}${msight_ooh-schema}.${table-prefix}ooh_summ_visit_freq_stg modify column c_13 integer (dict);
alter table ${table-prefix}${msight_ooh-schema}.${table-prefix}ooh_summ_visit_freq_stg modify column c_14 integer (dict);
alter table ${table-prefix}${msight_ooh-schema}.${table-prefix}ooh_summ_visit_freq_stg modify column c_15 integer (dict);
alter table ${table-prefix}${msight_ooh-schema}.${table-prefix}ooh_summ_visit_freq_stg modify column c_16 integer (dict);
alter table ${table-prefix}${msight_ooh-schema}.${table-prefix}ooh_summ_visit_freq_stg modify column c_17 integer (dict);
alter table ${table-prefix}${msight_ooh-schema}.${table-prefix}ooh_summ_visit_freq_stg modify column c_18 integer (dict);
alter table ${table-prefix}${msight_ooh-schema}.${table-prefix}ooh_summ_visit_freq_stg modify column c_19 integer (dict);
alter table ${table-prefix}${msight_ooh-schema}.${table-prefix}ooh_summ_visit_freq_stg modify column c_20 integer (dict);
alter table ${table-prefix}${msight_ooh-schema}.${table-prefix}ooh_summ_visit_freq_stg modify column c_21 integer (dict);
alter table ${table-prefix}${msight_ooh-schema}.${table-prefix}ooh_summ_visit_freq_stg modify column c_22 integer (dict);
alter table ${table-prefix}${msight_ooh-schema}.${table-prefix}ooh_summ_visit_freq_stg modify column c_23 integer (dict);
alter table ${table-prefix}${msight_ooh-schema}.${table-prefix}ooh_summ_visit_freq_stg modify column c_24 integer (dict);
alter table ${table-prefix}${msight_ooh-schema}.${table-prefix}ooh_summ_visit_freq_stg modify column c_25 integer (dict);
alter table ${table-prefix}${msight_ooh-schema}.${table-prefix}ooh_summ_visit_freq_stg modify column c_26 integer (dict);
alter table ${table-prefix}${msight_ooh-schema}.${table-prefix}ooh_summ_visit_freq_stg modify column c_27 integer (dict);
alter table ${table-prefix}${msight_ooh-schema}.${table-prefix}ooh_summ_visit_freq_stg modify column c_28 integer (dict);
alter table ${table-prefix}${msight_ooh-schema}.${table-prefix}ooh_summ_visit_freq_stg modify column c_29 integer (dict);
alter table ${table-prefix}${msight_ooh-schema}.${table-prefix}ooh_summ_visit_freq_stg modify column c_30 integer (dict);


delete from ${table-prefix}${msight_ooh-schema}.${table-prefix}ooh_summ_visit_freq where months = '${run-date=yyyyMM}';

insert into ${table-prefix}${msight_ooh-schema}.${table-prefix}ooh_summ_visit_freq
select * from ${table-prefix}${msight_ooh-schema}.${table-prefix}ooh_summ_visit_freq_stg;

DROP TABLE IF EXISTS ${table-prefix}${msight_ooh-schema}.${table-prefix}ooh_summ_visit_freq_stg;
DROP TABLE IF EXISTS ${table-prefix}${msight_ooh-schema}.${table-prefix}ooh_summ_visit_freq_input;



