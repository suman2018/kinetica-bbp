create or replace table ${table-prefix}${msight_ooh-schema}.${table-prefix}ooh_summ_profile_stg as
select
dates,
location_code,
cast(id_dtl as decimal(2,1)) as id_dtl,
cast(id_bearing as integer) as id_bearing,
cast(id_distance_ac as integer) as id_distance_ac,
cast(id_distance_cb as integer) as id_distance_cb,
CAST (age_lt_15 as integer) as age_lt_15,
CAST (age16_20 as integer) as age16_20,
CAST (age21_25 as integer) as age21_25,
CAST (age26_30 as integer) as age26_30,
CAST (age31_35 as integer) as age31_35,
CAST (age36_40 as integer) as age36_40,
CAST (age41_45 as integer) as age41_45,
CAST (age46_50 as integer) as age46_50,
CAST (age51_55 as integer) as age51_55,
CAST (age_gt_56 as integer) as age_gt_56,
CAST (male as integer) as male,
CAST (female as integer) as female,
CAST (education_primary as integer) as education_primary,
CAST (education_secondary as integer) as education_secondary,
CAST (ses_a1 as integer) as ses_a1,
CAST (ses_a2 as integer) as ses_a2,
CAST (ses_b2 as integer) as ses_b2,
CAST (ses_c1 as integer) as ses_c1,
CAST (ses_c2 as integer) as ses_c2,
CAST (ses_d as integer) as ses_d,
CAST (ses_e as integer) as ses_e,
CAST (entrepreneur as integer) as entrepreneur,
CAST (housewife as integer) as housewife,
CAST (private as integer) as private,
CAST (retired as integer) as retired,
CAST (student as integer) as student,
CAST (data_2gb as integer) as data_2gb,
CAST (data_2_5gb as integer) as data_2_5gb,
CAST (dat_5_8gb as integer) as dat_5_8gb,
CAST (data_more_8gb as integer) as data_more_8gb,
CAST (arpu_very_low as integer) as arpu_very_low,
CAST (arpu_low as integer) as arpu_low,
CAST (arpu_medium as integer) as arpu_medium,
CAST (arpu_high as integer) as arpu_high,
CAST (arpu_very_high as integer) as arpu_very_high,
CAST (arpu_top_usage as integer) as arpu_top_usage,
CAST (os_android as integer) as os_android,
CAST (os_ios as integer) as os_ios,
CAST (os_blackberry as integer) as os_blackberry,
CAST (os_other as integer) as os_other,
CAST (type_smartphone_android as integer) as type_smartphone_android,
CAST (type_smartphone_iphone as integer) as type_smartphone_iphone,
CAST (type_basic_phone as integer) as type_basic_phone,
CAST (type_other as integer) as type_other,
CAST (married as integer) as married,
CAST (single as integer) as single,
CAST (new_beauty as integer) as new_beauty,
CAST (new_culinary as integer) as new_culinary,
CAST (new_entertainment as integer) as new_entertainment,
CAST (new_fashion as integer) as new_fashion,
CAST (new_football as integer) as new_football,
CAST (new_other_sports as integer) as new_other_sports,
CAST (new_outdoor_activity as integer) as new_outdoor_activity,
CAST (new_technology as integer) as new_technology,
CAST (new_travel as integer) as new_travel,
CAST (new_education as integer) as new_education,
CAST (new_health as integer) as new_health,
CAST (new_lifestyle as integer) as new_lifestyle,
CAST (new_music as integer) as new_music,
CAST (new_social_media as integer) as new_social_media,
CAST (new_shopping as integer) as new_shopping,
CAST (application_store as integer) as application_store,
CAST (automotive as integer) as automotive,
CAST (baby_care as integer) as baby_care,
CAST (banking as integer) as banking,
CAST (beauty as integer) as beauty,
CAST (chatting as integer) as chatting,
CAST (ecommerce as integer) as ecommerce,
CAST (education as integer) as education,
CAST (payment_and_finance as integer) as payment_and_finance,
CAST (culinary as integer) as culinary,
CAST (games as integer) as games,
CAST (government as integer) as government,
CAST (property as integer) as property,
CAST (photography as integer) as photography,
CAST (job_search as integer) as job_search,
CAST (music as integer) as music,
CAST (news as integer) as news,
CAST (productivity as integer) as productivity,
CAST (social_media as integer) as social_media,
CAST (sports as integer) as sports,
CAST (technology as integer) as technology,
CAST (transportation as integer) as transportation,
CAST (travel as integer) as travel,
CAST (video as integer) as video,
CAST (total as integer) as total,
ki_shard_key (id_bearing, id_dtl, id_distance_ac, id_distance_cb) 
FROM ${table-prefix}${msight_ooh-schema}.${table-prefix}ooh_summ_profile_input;


alter table ${table-prefix}${msight_ooh-schema}.${table-prefix}ooh_summ_profile_stg modify column dates varchar (16, dict);
alter table ${table-prefix}${msight_ooh-schema}.${table-prefix}ooh_summ_profile_stg modify column location_code varchar(32, dict);
alter table ${table-prefix}${msight_ooh-schema}.${table-prefix}ooh_summ_profile_stg modify column age_lt_15 integer (dict);
alter table ${table-prefix}${msight_ooh-schema}.${table-prefix}ooh_summ_profile_stg modify column age16_20 integer (dict);
alter table ${table-prefix}${msight_ooh-schema}.${table-prefix}ooh_summ_profile_stg modify column age21_25 integer (dict);
alter table ${table-prefix}${msight_ooh-schema}.${table-prefix}ooh_summ_profile_stg modify column age26_30 integer (dict);
alter table ${table-prefix}${msight_ooh-schema}.${table-prefix}ooh_summ_profile_stg modify column age31_35 integer (dict);
alter table ${table-prefix}${msight_ooh-schema}.${table-prefix}ooh_summ_profile_stg modify column age36_40 integer (dict);
alter table ${table-prefix}${msight_ooh-schema}.${table-prefix}ooh_summ_profile_stg modify column age41_45 integer (dict);
alter table ${table-prefix}${msight_ooh-schema}.${table-prefix}ooh_summ_profile_stg modify column age46_50 integer (dict);
alter table ${table-prefix}${msight_ooh-schema}.${table-prefix}ooh_summ_profile_stg modify column age51_55 integer (dict);
alter table ${table-prefix}${msight_ooh-schema}.${table-prefix}ooh_summ_profile_stg modify column age_gt_56 integer (dict);
alter table ${table-prefix}${msight_ooh-schema}.${table-prefix}ooh_summ_profile_stg modify column male integer (dict);
alter table ${table-prefix}${msight_ooh-schema}.${table-prefix}ooh_summ_profile_stg modify column female integer (dict);
alter table ${table-prefix}${msight_ooh-schema}.${table-prefix}ooh_summ_profile_stg modify column education_primary integer (dict);
alter table ${table-prefix}${msight_ooh-schema}.${table-prefix}ooh_summ_profile_stg modify column education_secondary integer (dict);
alter table ${table-prefix}${msight_ooh-schema}.${table-prefix}ooh_summ_profile_stg modify column ses_a1 integer (dict);
alter table ${table-prefix}${msight_ooh-schema}.${table-prefix}ooh_summ_profile_stg modify column ses_a2 integer (dict);
alter table ${table-prefix}${msight_ooh-schema}.${table-prefix}ooh_summ_profile_stg modify column ses_b2 integer (dict);
alter table ${table-prefix}${msight_ooh-schema}.${table-prefix}ooh_summ_profile_stg modify column ses_c1 integer (dict);
alter table ${table-prefix}${msight_ooh-schema}.${table-prefix}ooh_summ_profile_stg modify column ses_c2 integer (dict);
alter table ${table-prefix}${msight_ooh-schema}.${table-prefix}ooh_summ_profile_stg modify column ses_d integer (dict);
alter table ${table-prefix}${msight_ooh-schema}.${table-prefix}ooh_summ_profile_stg modify column ses_e integer (dict);
alter table ${table-prefix}${msight_ooh-schema}.${table-prefix}ooh_summ_profile_stg modify column entrepreneur integer (dict);
alter table ${table-prefix}${msight_ooh-schema}.${table-prefix}ooh_summ_profile_stg modify column housewife integer (dict);
alter table ${table-prefix}${msight_ooh-schema}.${table-prefix}ooh_summ_profile_stg modify column private integer (dict);
alter table ${table-prefix}${msight_ooh-schema}.${table-prefix}ooh_summ_profile_stg modify column retired integer (dict);
alter table ${table-prefix}${msight_ooh-schema}.${table-prefix}ooh_summ_profile_stg modify column student integer (dict);
alter table ${table-prefix}${msight_ooh-schema}.${table-prefix}ooh_summ_profile_stg modify column data_2gb integer (dict);
alter table ${table-prefix}${msight_ooh-schema}.${table-prefix}ooh_summ_profile_stg modify column data_2_5gb integer (dict);
alter table ${table-prefix}${msight_ooh-schema}.${table-prefix}ooh_summ_profile_stg modify column dat_5_8gb integer (dict);
alter table ${table-prefix}${msight_ooh-schema}.${table-prefix}ooh_summ_profile_stg modify column data_more_8gb integer (dict);
alter table ${table-prefix}${msight_ooh-schema}.${table-prefix}ooh_summ_profile_stg modify column arpu_very_low integer (dict);
alter table ${table-prefix}${msight_ooh-schema}.${table-prefix}ooh_summ_profile_stg modify column arpu_low integer (dict);
alter table ${table-prefix}${msight_ooh-schema}.${table-prefix}ooh_summ_profile_stg modify column arpu_medium integer (dict);
alter table ${table-prefix}${msight_ooh-schema}.${table-prefix}ooh_summ_profile_stg modify column arpu_high integer (dict);
alter table ${table-prefix}${msight_ooh-schema}.${table-prefix}ooh_summ_profile_stg modify column arpu_very_high integer (dict);
alter table ${table-prefix}${msight_ooh-schema}.${table-prefix}ooh_summ_profile_stg modify column arpu_top_usage integer (dict);
alter table ${table-prefix}${msight_ooh-schema}.${table-prefix}ooh_summ_profile_stg modify column os_android integer (dict);
alter table ${table-prefix}${msight_ooh-schema}.${table-prefix}ooh_summ_profile_stg modify column os_ios integer (dict);
alter table ${table-prefix}${msight_ooh-schema}.${table-prefix}ooh_summ_profile_stg modify column os_blackberry integer (dict);
alter table ${table-prefix}${msight_ooh-schema}.${table-prefix}ooh_summ_profile_stg modify column os_other integer (dict);
alter table ${table-prefix}${msight_ooh-schema}.${table-prefix}ooh_summ_profile_stg modify column type_smartphone_android integer (dict);
alter table ${table-prefix}${msight_ooh-schema}.${table-prefix}ooh_summ_profile_stg modify column type_smartphone_iphone integer (dict);
alter table ${table-prefix}${msight_ooh-schema}.${table-prefix}ooh_summ_profile_stg modify column type_basic_phone integer (dict);
alter table ${table-prefix}${msight_ooh-schema}.${table-prefix}ooh_summ_profile_stg modify column type_other integer (dict);
alter table ${table-prefix}${msight_ooh-schema}.${table-prefix}ooh_summ_profile_stg modify column married integer (dict);
alter table ${table-prefix}${msight_ooh-schema}.${table-prefix}ooh_summ_profile_stg modify column single integer (dict);
alter table ${table-prefix}${msight_ooh-schema}.${table-prefix}ooh_summ_profile_stg modify column new_beauty integer (dict);
alter table ${table-prefix}${msight_ooh-schema}.${table-prefix}ooh_summ_profile_stg modify column new_culinary integer (dict);
alter table ${table-prefix}${msight_ooh-schema}.${table-prefix}ooh_summ_profile_stg modify column new_entertainment integer (dict);
alter table ${table-prefix}${msight_ooh-schema}.${table-prefix}ooh_summ_profile_stg modify column new_fashion integer (dict);
alter table ${table-prefix}${msight_ooh-schema}.${table-prefix}ooh_summ_profile_stg modify column new_football integer (dict);
alter table ${table-prefix}${msight_ooh-schema}.${table-prefix}ooh_summ_profile_stg modify column new_other_sports integer (dict);
alter table ${table-prefix}${msight_ooh-schema}.${table-prefix}ooh_summ_profile_stg modify column new_outdoor_activity integer (dict);
alter table ${table-prefix}${msight_ooh-schema}.${table-prefix}ooh_summ_profile_stg modify column new_technology integer (dict);
alter table ${table-prefix}${msight_ooh-schema}.${table-prefix}ooh_summ_profile_stg modify column new_travel integer (dict);
alter table ${table-prefix}${msight_ooh-schema}.${table-prefix}ooh_summ_profile_stg modify column new_education integer (dict);
alter table ${table-prefix}${msight_ooh-schema}.${table-prefix}ooh_summ_profile_stg modify column new_health integer (dict);
alter table ${table-prefix}${msight_ooh-schema}.${table-prefix}ooh_summ_profile_stg modify column new_lifestyle integer (dict);
alter table ${table-prefix}${msight_ooh-schema}.${table-prefix}ooh_summ_profile_stg modify column new_music integer (dict);
alter table ${table-prefix}${msight_ooh-schema}.${table-prefix}ooh_summ_profile_stg modify column new_social_media integer (dict);
alter table ${table-prefix}${msight_ooh-schema}.${table-prefix}ooh_summ_profile_stg modify column new_shopping integer (dict);
alter table ${table-prefix}${msight_ooh-schema}.${table-prefix}ooh_summ_profile_stg modify column application_store integer (dict);
alter table ${table-prefix}${msight_ooh-schema}.${table-prefix}ooh_summ_profile_stg modify column automotive integer (dict);
alter table ${table-prefix}${msight_ooh-schema}.${table-prefix}ooh_summ_profile_stg modify column baby_care integer (dict);
alter table ${table-prefix}${msight_ooh-schema}.${table-prefix}ooh_summ_profile_stg modify column banking integer (dict);
alter table ${table-prefix}${msight_ooh-schema}.${table-prefix}ooh_summ_profile_stg modify column beauty integer (dict);
alter table ${table-prefix}${msight_ooh-schema}.${table-prefix}ooh_summ_profile_stg modify column chatting integer (dict);
alter table ${table-prefix}${msight_ooh-schema}.${table-prefix}ooh_summ_profile_stg modify column ecommerce integer (dict);
alter table ${table-prefix}${msight_ooh-schema}.${table-prefix}ooh_summ_profile_stg modify column education integer (dict);
alter table ${table-prefix}${msight_ooh-schema}.${table-prefix}ooh_summ_profile_stg modify column payment_and_finance integer (dict);
alter table ${table-prefix}${msight_ooh-schema}.${table-prefix}ooh_summ_profile_stg modify column culinary integer (dict);
alter table ${table-prefix}${msight_ooh-schema}.${table-prefix}ooh_summ_profile_stg modify column games integer (dict);
alter table ${table-prefix}${msight_ooh-schema}.${table-prefix}ooh_summ_profile_stg modify column government integer (dict);
alter table ${table-prefix}${msight_ooh-schema}.${table-prefix}ooh_summ_profile_stg modify column property integer (dict);
alter table ${table-prefix}${msight_ooh-schema}.${table-prefix}ooh_summ_profile_stg modify column photography integer (dict);
alter table ${table-prefix}${msight_ooh-schema}.${table-prefix}ooh_summ_profile_stg modify column job_search integer (dict);
alter table ${table-prefix}${msight_ooh-schema}.${table-prefix}ooh_summ_profile_stg modify column music integer (dict);
alter table ${table-prefix}${msight_ooh-schema}.${table-prefix}ooh_summ_profile_stg modify column news integer (dict);
alter table ${table-prefix}${msight_ooh-schema}.${table-prefix}ooh_summ_profile_stg modify column productivity integer (dict);
alter table ${table-prefix}${msight_ooh-schema}.${table-prefix}ooh_summ_profile_stg modify column social_media integer (dict);
alter table ${table-prefix}${msight_ooh-schema}.${table-prefix}ooh_summ_profile_stg modify column sports integer (dict);
alter table ${table-prefix}${msight_ooh-schema}.${table-prefix}ooh_summ_profile_stg modify column technology integer (dict);
alter table ${table-prefix}${msight_ooh-schema}.${table-prefix}ooh_summ_profile_stg modify column transportation integer (dict);
alter table ${table-prefix}${msight_ooh-schema}.${table-prefix}ooh_summ_profile_stg modify column travel integer (dict);
alter table ${table-prefix}${msight_ooh-schema}.${table-prefix}ooh_summ_profile_stg modify column video integer (dict);
alter table ${table-prefix}${msight_ooh-schema}.${table-prefix}ooh_summ_profile_stg modify column total integer (dict);

delete from ${table-prefix}${msight_ooh-schema}.${table-prefix}ooh_summ_profile where months = '${run-date=yyyyMM}';

insert into ${table-prefix}${msight_ooh-schema}.${table-prefix}ooh_summ_profile
select 
 '${run-date=yyyyMM}' as months
,a.* from ${table-prefix}${msight_ooh-schema}.${table-prefix}ooh_summ_profile_stg a;


DROP TABLE IF EXISTS ${table-prefix}${msight_ooh-schema}.${table-prefix}ooh_summ_profile_stg;
DROP TABLE IF EXISTS ${table-prefix}${msight_ooh-schema}.${table-prefix}ooh_summ_profile_input;



