ALTER TABLE ${table-prefix}${uc1_history-schema}.${table-prefix}recharge_cluster_anumber     RENAME TO ${table-prefix}recharge_cluster_anumber_tmp      ;
ALTER TABLE ${table-prefix}${uc1_history-schema}.${table-prefix}recharge_cluster_bnumber     RENAME TO ${table-prefix}recharge_cluster_bnumber_tmp      ;
ALTER TABLE ${table-prefix}${uc1_history-schema}.${table-prefix}revenue_pos_cust_type_osdss  RENAME TO ${table-prefix}revenue_pos_cust_type_osdss_tmp   ;
ALTER TABLE ${table-prefix}${uc1_history-schema}.${table-prefix}revenue_postpaid_hlr         RENAME TO ${table-prefix}revenue_postpaid_hlr_tmp          ;
ALTER TABLE ${table-prefix}${uc1_history-schema}.${table-prefix}revenue_prepaid_hlr          RENAME TO ${table-prefix}revenue_prepaid_hlr_tmp           ;
ALTER TABLE ${table-prefix}${uc1_history-schema}.${table-prefix}scn_cluster_osdss            RENAME TO ${table-prefix}scn_cluster_osdss_tmp             ;
ALTER TABLE ${table-prefix}${uc1_history-schema}.${table-prefix}scn_olap_hlr                 RENAME TO ${table-prefix}scn_olap_hlr_tmp                  ;
ALTER TABLE ${table-prefix}${uc1_history-schema}.${table-prefix}subscriber_cluster_osdss     RENAME TO ${table-prefix}subscriber_cluster_osdss_tmp      ;
ALTER TABLE ${table-prefix}${uc1_history-schema}.${table-prefix}revenue_scn_enterprise_pre   RENAME TO ${table-prefix}revenue_scn_enterprise_pre_tmp    ;
ALTER TABLE ${table-prefix}${uc1_history-schema}.${table-prefix}revenue_cluster_priceplan    RENAME TO ${table-prefix}revenue_cluster_priceplan_tmp     ;

/* KI_HINT_DICT_PROJECTION */
CREATE TABLE ${table-prefix}${uc1_history-schema}.${table-prefix}recharge_cluster_anumber     AS SELECT *, DATE('2000-01-01') AS file_date FROM ${table-prefix}${uc1_history-schema}.${table-prefix}recharge_cluster_anumber_tmp    ORDER BY recharge_date  ;
/* KI_HINT_DICT_PROJECTION */
CREATE TABLE ${table-prefix}${uc1_history-schema}.${table-prefix}recharge_cluster_bnumber     AS SELECT *, DATE('2000-01-01') AS file_date FROM ${table-prefix}${uc1_history-schema}.${table-prefix}recharge_cluster_bnumber_tmp    ORDER BY recharge_date  ;
/* KI_HINT_DICT_PROJECTION */
CREATE TABLE ${table-prefix}${uc1_history-schema}.${table-prefix}revenue_pos_cust_type_osdss  AS SELECT *, DATE('2000-01-01') AS file_date FROM ${table-prefix}${uc1_history-schema}.${table-prefix}revenue_pos_cust_type_osdss_tmp ORDER BY RevenueDate    ;
/* KI_HINT_DICT_PROJECTION */
CREATE TABLE ${table-prefix}${uc1_history-schema}.${table-prefix}revenue_postpaid_hlr         AS SELECT *, DATE('2000-01-01') AS file_date FROM ${table-prefix}${uc1_history-schema}.${table-prefix}revenue_postpaid_hlr_tmp        ORDER BY revenue_date   ;
/* KI_HINT_DICT_PROJECTION */
CREATE TABLE ${table-prefix}${uc1_history-schema}.${table-prefix}revenue_prepaid_hlr          AS SELECT *, DATE('2000-01-01') AS file_date FROM ${table-prefix}${uc1_history-schema}.${table-prefix}revenue_prepaid_hlr_tmp         ORDER BY revenue_date   ;
/* KI_HINT_DICT_PROJECTION */
CREATE TABLE ${table-prefix}${uc1_history-schema}.${table-prefix}scn_cluster_osdss            AS SELECT *, DATE('2000-01-01') AS file_date FROM ${table-prefix}${uc1_history-schema}.${table-prefix}scn_cluster_osdss_tmp           ORDER BY SCN_DATE       ;
/* KI_HINT_DICT_PROJECTION */
CREATE TABLE ${table-prefix}${uc1_history-schema}.${table-prefix}scn_olap_hlr                 AS SELECT *, DATE('2000-01-01') AS file_date FROM ${table-prefix}${uc1_history-schema}.${table-prefix}scn_olap_hlr_tmp                ORDER BY SCN_DATE       ;
/* KI_HINT_DICT_PROJECTION */
CREATE TABLE ${table-prefix}${uc1_history-schema}.${table-prefix}subscriber_cluster_osdss     AS SELECT *, DATE('2000-01-01') AS file_date FROM ${table-prefix}${uc1_history-schema}.${table-prefix}subscriber_cluster_osdss_tmp    ORDER BY SCNDATE        ;
/* KI_HINT_DICT_PROJECTION */
CREATE TABLE ${table-prefix}${uc1_history-schema}.${table-prefix}revenue_scn_enterprise_pre   AS SELECT *, DATE('2000-01-01') AS file_date FROM ${table-prefix}${uc1_history-schema}.${table-prefix}revenue_scn_enterprise_pre_tmp  ORDER BY Revenuedate    ;
/* KI_HINT_DICT_PROJECTION */
CREATE TABLE ${table-prefix}${uc1_history-schema}.${table-prefix}revenue_cluster_priceplan    AS SELECT *, DATE('2000-01-01') AS file_date FROM ${table-prefix}${uc1_history-schema}.${table-prefix}revenue_cluster_priceplan_tmp   ORDER BY revenue_date   ;


DROP TABLE ${table-prefix}${uc1_history-schema}.${table-prefix}recharge_cluster_anumber_tmp      ;
DROP TABLE ${table-prefix}${uc1_history-schema}.${table-prefix}recharge_cluster_bnumber_tmp      ;
DROP TABLE ${table-prefix}${uc1_history-schema}.${table-prefix}revenue_pos_cust_type_osdss_tmp   ;
DROP TABLE ${table-prefix}${uc1_history-schema}.${table-prefix}revenue_postpaid_hlr_tmp          ;
DROP TABLE ${table-prefix}${uc1_history-schema}.${table-prefix}revenue_prepaid_hlr_tmp           ;
DROP TABLE ${table-prefix}${uc1_history-schema}.${table-prefix}scn_cluster_osdss_tmp             ;
DROP TABLE ${table-prefix}${uc1_history-schema}.${table-prefix}scn_olap_hlr_tmp                  ;
DROP TABLE ${table-prefix}${uc1_history-schema}.${table-prefix}subscriber_cluster_osdss_tmp      ;
DROP TABLE ${table-prefix}${uc1_history-schema}.${table-prefix}revenue_scn_enterprise_pre_tmp    ;
DROP TABLE ${table-prefix}${uc1_history-schema}.${table-prefix}revenue_cluster_priceplan_tmp     ;


ALTER TABLE ${table-prefix}${uc1_history-schema}.${table-prefix}recharge_cluster_anumber MODIFY area varchar(8,dict);
ALTER TABLE ${table-prefix}${uc1_history-schema}.${table-prefix}recharge_cluster_anumber MODIFY cluster_name varchar(32,dict);
ALTER TABLE ${table-prefix}${uc1_history-schema}.${table-prefix}recharge_cluster_anumber MODIFY recharge_channel varchar(32,dict);
ALTER TABLE ${table-prefix}${uc1_history-schema}.${table-prefix}recharge_cluster_anumber MODIFY region varchar(32,dict);
ALTER TABLE ${table-prefix}${uc1_history-schema}.${table-prefix}recharge_cluster_anumber MODIFY trans_type varchar(32,dict);
ALTER TABLE ${table-prefix}${uc1_history-schema}.${table-prefix}recharge_cluster_anumber MODIFY voucher_denomination integer(dict);
ALTER TABLE ${table-prefix}${uc1_history-schema}.${table-prefix}recharge_cluster_bnumber MODIFY area varchar(8,dict);
ALTER TABLE ${table-prefix}${uc1_history-schema}.${table-prefix}recharge_cluster_bnumber MODIFY cluster_name varchar(32,dict);
ALTER TABLE ${table-prefix}${uc1_history-schema}.${table-prefix}recharge_cluster_bnumber MODIFY recharge_channel varchar(32,dict);
ALTER TABLE ${table-prefix}${uc1_history-schema}.${table-prefix}recharge_cluster_bnumber MODIFY region varchar(32,dict);
ALTER TABLE ${table-prefix}${uc1_history-schema}.${table-prefix}recharge_cluster_bnumber MODIFY trans_type varchar(32,dict);
ALTER TABLE ${table-prefix}${uc1_history-schema}.${table-prefix}recharge_cluster_bnumber MODIFY voucher_denomination integer(dict);
ALTER TABLE ${table-prefix}${uc1_history-schema}.${table-prefix}revenue_cluster_priceplan MODIFY area varchar(8,dict);
ALTER TABLE ${table-prefix}${uc1_history-schema}.${table-prefix}revenue_cluster_priceplan MODIFY L1 varchar(32,dict);
ALTER TABLE ${table-prefix}${uc1_history-schema}.${table-prefix}revenue_cluster_priceplan MODIFY L2 varchar(32,dict);
ALTER TABLE ${table-prefix}${uc1_history-schema}.${table-prefix}revenue_cluster_priceplan MODIFY L3 varchar(32,dict);
ALTER TABLE ${table-prefix}${uc1_history-schema}.${table-prefix}revenue_cluster_priceplan MODIFY node varchar(16,dict);
ALTER TABLE ${table-prefix}${uc1_history-schema}.${table-prefix}revenue_cluster_priceplan MODIFY priceplan varchar(64,dict);
ALTER TABLE ${table-prefix}${uc1_history-schema}.${table-prefix}revenue_cluster_priceplan MODIFY product_name varchar(16,dict);
ALTER TABLE ${table-prefix}${uc1_history-schema}.${table-prefix}revenue_cluster_priceplan MODIFY region varchar(32,dict);
ALTER TABLE ${table-prefix}${uc1_history-schema}.${table-prefix}revenue_pos_cust_type_osdss MODIFY AREA varchar(8,dict);
ALTER TABLE ${table-prefix}${uc1_history-schema}.${table-prefix}revenue_pos_cust_type_osdss MODIFY CustomerType varchar(16,dict);
ALTER TABLE ${table-prefix}${uc1_history-schema}.${table-prefix}revenue_pos_cust_type_osdss MODIFY L1 varchar(32,dict);
ALTER TABLE ${table-prefix}${uc1_history-schema}.${table-prefix}revenue_pos_cust_type_osdss MODIFY L2 varchar(32,dict);
ALTER TABLE ${table-prefix}${uc1_history-schema}.${table-prefix}revenue_pos_cust_type_osdss MODIFY L3 varchar(32,dict);
ALTER TABLE ${table-prefix}${uc1_history-schema}.${table-prefix}revenue_pos_cust_type_osdss MODIFY SubCustomerType varchar(32,dict);
ALTER TABLE ${table-prefix}${uc1_history-schema}.${table-prefix}revenue_postpaid_hlr MODIFY area varchar(8,dict);
ALTER TABLE ${table-prefix}${uc1_history-schema}.${table-prefix}revenue_postpaid_hlr MODIFY L1 varchar(32,dict);
ALTER TABLE ${table-prefix}${uc1_history-schema}.${table-prefix}revenue_postpaid_hlr MODIFY L2 varchar(32,dict);
ALTER TABLE ${table-prefix}${uc1_history-schema}.${table-prefix}revenue_postpaid_hlr MODIFY product_detail_name varchar(32,dict);
ALTER TABLE ${table-prefix}${uc1_history-schema}.${table-prefix}revenue_postpaid_hlr MODIFY PROVIDER_ID varchar(16,dict);
ALTER TABLE ${table-prefix}${uc1_history-schema}.${table-prefix}revenue_postpaid_hlr MODIFY region varchar(16,dict);
ALTER TABLE ${table-prefix}${uc1_history-schema}.${table-prefix}revenue_prepaid_hlr MODIFY area varchar(8,dict);
ALTER TABLE ${table-prefix}${uc1_history-schema}.${table-prefix}revenue_prepaid_hlr MODIFY L1 varchar(32,dict);
ALTER TABLE ${table-prefix}${uc1_history-schema}.${table-prefix}revenue_prepaid_hlr MODIFY L2 varchar(32,dict);
ALTER TABLE ${table-prefix}${uc1_history-schema}.${table-prefix}revenue_prepaid_hlr MODIFY product_detail_name varchar(32,dict);
ALTER TABLE ${table-prefix}${uc1_history-schema}.${table-prefix}revenue_prepaid_hlr MODIFY PROVIDER_ID varchar(16,dict);
ALTER TABLE ${table-prefix}${uc1_history-schema}.${table-prefix}revenue_prepaid_hlr MODIFY region varchar(16,dict);
ALTER TABLE ${table-prefix}${uc1_history-schema}.${table-prefix}scn_cluster_osdss MODIFY area varchar(8,dict);
ALTER TABLE ${table-prefix}${uc1_history-schema}.${table-prefix}scn_cluster_osdss MODIFY BRAND_NAME varchar(16,dict);
ALTER TABLE ${table-prefix}${uc1_history-schema}.${table-prefix}scn_cluster_osdss MODIFY CHURN integer(dict);
ALTER TABLE ${table-prefix}${uc1_history-schema}.${table-prefix}scn_cluster_osdss MODIFY NETADD integer(dict);
ALTER TABLE ${table-prefix}${uc1_history-schema}.${table-prefix}scn_cluster_osdss MODIFY priceplan varchar(64,dict);
ALTER TABLE ${table-prefix}${uc1_history-schema}.${table-prefix}scn_cluster_osdss MODIFY region_name varchar(32,dict);
ALTER TABLE ${table-prefix}${uc1_history-schema}.${table-prefix}scn_cluster_osdss MODIFY SALES integer(dict);
ALTER TABLE ${table-prefix}${uc1_history-schema}.${table-prefix}subscriber_cluster_osdss MODIFY AREA varchar(8,dict);
ALTER TABLE ${table-prefix}${uc1_history-schema}.${table-prefix}subscriber_cluster_osdss MODIFY BRANDNAME varchar(16,dict);
ALTER TABLE ${table-prefix}${uc1_history-schema}.${table-prefix}subscriber_cluster_osdss MODIFY PPNAME varchar(64,dict);
ALTER TABLE ${table-prefix}${uc1_history-schema}.${table-prefix}subscriber_cluster_osdss MODIFY REGIONNAME varchar(32,dict);