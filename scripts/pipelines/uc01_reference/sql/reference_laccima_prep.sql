DROP TABLE IF EXISTS ${table-prefix}${reference-schema}.${table-prefix}laccima_ref_${run-date=yyyyMMdd};     

ALTER TABLE ${table-prefix}${reference-schema}.${table-prefix}laccima_ref RENAME      TO ${table-prefix}laccima_ref_${run-date=yyyyMMdd};

----- table housekeeping will be execute on hvc_rolling