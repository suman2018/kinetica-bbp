DROP TABLE IF EXISTS ${table-prefix}${reference-schema}.${table-prefix}product_line_ref_${run-date=yyyyMMdd};

ALTER TABLE ${table-prefix}${reference-schema}.${table-prefix}product_line_ref RENAME TO ${table-prefix}product_line_ref_${run-date=yyyyMMdd};

DROP TABLE IF EXISTS ${table-prefix}product_line_ref_${retention-date=yyyyMMdd};