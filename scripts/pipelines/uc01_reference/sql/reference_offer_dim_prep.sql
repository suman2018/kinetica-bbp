DROP TABLE IF EXISTS ${table-prefix}${reference-schema}.${table-prefix}offer_dim_ref_${run-date=yyyyMMdd};   

ALTER TABLE ${table-prefix}${reference-schema}.${table-prefix}offer_dim_ref RENAME    TO ${table-prefix}offer_dim_ref_${run-date=yyyyMMdd};

DROP TABLE IF EXISTS ${table-prefix}offer_dim_ref_${retention-date=yyyyMMdd};
