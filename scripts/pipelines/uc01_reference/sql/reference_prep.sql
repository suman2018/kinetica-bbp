DROP TABLE IF EXISTS ${table-prefix}${reference-schema}.${table-prefix}hlr_ref_${run-date=yyyyMMdd};         
DROP TABLE IF EXISTS ${table-prefix}${reference-schema}.${table-prefix}laccima_ref_${run-date=yyyyMMdd};     
DROP TABLE IF EXISTS ${table-prefix}${reference-schema}.${table-prefix}offer_dim_ref_${run-date=yyyyMMdd};   
DROP TABLE IF EXISTS ${table-prefix}${reference-schema}.${table-prefix}product_line_ref_${run-date=yyyyMMdd};

ALTER TABLE ${table-prefix}${reference-schema}.${table-prefix}hlr_ref RENAME          TO ${table-prefix}hlr_ref_${run-date=yyyyMMdd};
ALTER TABLE ${table-prefix}${reference-schema}.${table-prefix}laccima_ref RENAME      TO ${table-prefix}laccima_ref_${run-date=yyyyMMdd};
ALTER TABLE ${table-prefix}${reference-schema}.${table-prefix}offer_dim_ref RENAME    TO ${table-prefix}offer_dim_ref_${run-date=yyyyMMdd};
ALTER TABLE ${table-prefix}${reference-schema}.${table-prefix}product_line_ref RENAME TO ${table-prefix}product_line_ref_${run-date=yyyyMMdd};