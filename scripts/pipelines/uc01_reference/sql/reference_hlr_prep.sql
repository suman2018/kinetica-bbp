DROP TABLE IF EXISTS ${table-prefix}${reference-schema}.${table-prefix}hlr_ref_${run-date=yyyyMMdd};         

ALTER TABLE ${table-prefix}${reference-schema}.${table-prefix}hlr_ref RENAME          TO ${table-prefix}hlr_ref_${run-date=yyyyMMdd};

DROP TABLE IF EXISTS ${table-prefix}hlr_ref_${retention-date=yyyyMMdd};