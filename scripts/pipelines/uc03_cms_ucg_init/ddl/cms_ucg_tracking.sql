create or replace table ${table-prefix}${cms-schema}.${table-prefix}cms_ucg_tracking
(
	"rep_month" date(dict),
	"msisdn" bigint(shard_key),
	"brand" varchar(16, dict),
	"area_lacci" varchar(8,dict),
    "region_lacci" varchar(32, dict),
    "region_hlr" varchar(32,dict),
    "city_hlr" varchar(16,dict),
    "los" int,
    "device_type" varchar(16,dict),
    "total_revenue_base" double,
    "rev_broadband" double,
    "total_revenue" double,
    "is_ucg" int,
    "file_date" date(dict) not null,
    "los_segment" varchar(32, dict),
    "rev_data_segment" varchar(32, dict),
    "smartphone_user" varchar(32, dict),
    "arpu_group" varchar(32, dict)
)