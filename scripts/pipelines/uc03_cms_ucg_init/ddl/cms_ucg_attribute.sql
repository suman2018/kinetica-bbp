create or replace table ${table-prefix}${cms-schema}.${table-prefix}ucg_attribute
(
	"rep_month" date,
	"is_ucg" int,
	"brand" varchar(16, dict),
	"region" varchar(32, dict),
	"area"	varchar(5,dict),
	"los_segment" varchar(32, dict),
	"arpu_group" varchar(32, dict),
	"rev_data_segment" varchar(32, dict),
	"smartphone_user" varchar(32, dict),
	"msisdn" bigint(shard_key),
	"total_revenue" bigint,
	"file_date" DATE(dict) NOT NULL
)