-- Author: Sadar Baskoro <ssetihajid@kinetica.com>
-- Last Update: 20 Feb 2019
-- Objective: get Multidim from UC02 HVC

create or replace table ${table-prefix}${niknokk-schema}.${table-prefix}niknokk_multidim_${run-date=yyyyMM} as 
select 
	trx_date
	,msisdn_char as msisdn
	,nik_id
	,ss_nmbr
	,brand
	,reg_stts
    ,reg_chnl
	,area_sales
	,region_lacci
	,region_sales
	,cluster
	,kabupaten
	,kecamatan
	,reg_dt
	,nik_dob
	,activation_date
	,rev_mtd
	,KI_SHARD_KEY(msisdn)
from ${table-prefix}${hvc-schema}.${table-prefix}hvc_multidim_${run-date=yyyyMMdd};


alter table ${table-prefix}${niknokk-schema}.${table-prefix}niknokk_multidim_${run-date=yyyyMM} modify brand varchar(32,dict);
alter table ${table-prefix}${niknokk-schema}.${table-prefix}niknokk_multidim_${run-date=yyyyMM} modify area_sales varchar(16,dict);
alter table ${table-prefix}${niknokk-schema}.${table-prefix}niknokk_multidim_${run-date=yyyyMM} modify region_lacci varchar(64,dict);
alter table ${table-prefix}${niknokk-schema}.${table-prefix}niknokk_multidim_${run-date=yyyyMM} modify region_sales varchar(64,dict);
alter table ${table-prefix}${niknokk-schema}.${table-prefix}niknokk_multidim_${run-date=yyyyMM} modify cluster varchar(64,dict);
alter table ${table-prefix}${niknokk-schema}.${table-prefix}niknokk_multidim_${run-date=yyyyMM} modify kabupaten varchar(64,dict);
alter table ${table-prefix}${niknokk-schema}.${table-prefix}niknokk_multidim_${run-date=yyyyMM} modify kecamatan varchar(64,dict);
alter table ${table-prefix}${niknokk-schema}.${table-prefix}niknokk_multidim_${run-date=yyyyMM} modify reg_chnl varchar(64,dict);
