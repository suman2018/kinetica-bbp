delete from ${table-prefix}${reference-schema}.${table-prefix}channel_ref;

insert into ${table-prefix}${reference-schema}.${table-prefix}channel_ref
select * from ${table-prefix}${reference-schema}.${table-prefix}channel_ref_stg;

drop table IF EXISTS ${table-prefix}${reference-schema}.${table-prefix}channel_ref_stg;

