delete from ${table-prefix}${hvc-schema}.${table-prefix}hvc_cb_baseline;

insert into ${table-prefix}${hvc-schema}.${table-prefix}hvc_cb_baseline
select * from ${table-prefix}${hvc-schema}.${table-prefix}hvc_cb_baseline_stg;

drop table IF EXISTS ${table-prefix}${hvc-schema}.${table-prefix}hvc_cb_baseline_stg;

