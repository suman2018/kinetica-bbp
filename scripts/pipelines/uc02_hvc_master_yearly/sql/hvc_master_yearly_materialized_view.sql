CREATE OR REPLACE MATERIALIZED VIEW
${table-prefix}${hvc-schema}_out.${table-prefix}hvc_cb_baseline_v
REFRESH EVERY 1 DAY AS (
SELECT *
FROM ${table-prefix}${hvc-schema}.${table-prefix}hvc_cb_baseline
);

alter schema ${table-prefix}${hvc-schema}_out set protected true;