#!/bin/bash

source ~/.bashrc
source ~/.bash_profile

## control file that inform ingestion success
uc_ctl_file=(\
"uc01_dashboard_????????" \
"uc02_hvc_rolling_new_????????" \
"uc02_niknokk_ingestion_daily_????????" \
)

## backup file path, must be ordered as uc_ctl_file
uc_backup_path=(\
"/data01/hnat_gpu/landing_uc1_dev/report_gpu/BACKUP" \
"/data01/hnat_gpu/landing_UC02/BACKUP/HVC" \
"/data01/hnat_gpu/landing_UC02/BACKUP/NIKNOK" \
)

## source file path, must be ordered as uc_ctl_file
uc_file_path=(\
"/data01/hnat_gpu/landing_uc1_dev/report_gpu/OUT" \
"/data01/hnat_gpu/landing_UC02/HVC" \
"/data01/hnat_gpu/landing_UC02/NIKNOK" \
)

## check ingestion complete this days before
days_before_compress=6
days_retention=60
logs_path="/home/hnat_gpu/orch_prod/logs"

for ((i = 0; i < ${#uc_ctl_file[@]}; i++))
do 
     echo ${uc_ctl_file[i]}
     echo ${uc_backup_path[i]}
     echo ${uc_file_path[i]}

     list_files=()

     ctl_file=${uc_ctl_file[i]}
     cmd="find /home/hnat_gpu/orch_prod/logs -maxdepth 1 -type f -iname ${uc_ctl_file[i]} -mtime ${days_before_compress} -printf \"%f\\n\""
     list_files=`eval $cmd`

     echo $cmd
     echo $list_files
 
     ##echo $list_files 

     for pfile in ${list_files[@]}
     do
         echo $pfile
         tglFile=${pfile: -8}
         echo $tglFile

         cmd="date -d'${tglFile} + 1 day' +%Y%m%d"
         next_date=`eval $cmd`
        
         if [ ${next_date: -2} == '01' ]
         then 
               echo 'Awal bulan : '$tglFile
               continue
         fi 

         ### compress source file of some days before completed process
         ### move the compress into BACKUP folder
         cmd="mkdir -p ${uc_backup_path[i]}/$tglFile"
         echo $cmd
         eval `$cmd`

         cmd="gzip ${uc_file_path[i]}/$tglFile/*.flg"
         echo $cmd
         eval `$cmd`

         cmd="gzip ${uc_file_path[i]}/$tglFile/*.txt"
         echo $cmd
         eval `$cmd`

         cmd="mv ${uc_file_path[i]}/$tglFile/*.gz ${uc_backup_path[i]}/$tglFile/"
         echo $cmd
         eval `$cmd`

         touch_ctl=${uc_ctl_file[i]/"????????"/""}
         cmd="touch $logs_path/compress_$touch_ctl$tglFile"
         echo $cmd
         eval `$cmd`

         ### housekeeping compressed file before days_retention 
         cmd="date -d'${tglFile} - ${days_retention} day' +%Y%m%d"
         compress_retention_date=`eval $cmd`
         echo retention=${compress_retention_date}

         cmd="rm -f ${uc_backup_path[i]}/${compress_retention_date}/*.*"
         echo ${cmd}
         eval `$cmd`

         cmd="rmdir --ignore-fail-on-non-empty ${uc_backup_path[i]}/${compress_retention_date}"
         echo ${cmd}
         eval `$cmd`

         cmd="rmdir --ignore-fail-on-non-empty ${uc_file_path[i]}/${compress_retention_date}"
         echo ${cmd}
         eval `$cmd`

      done

done

