#!/usr/bin/env bash

runDate=`date +%Y-%h-%d" "%H:%M:%S`
runTime=`date +%s`
jobDate=`date +"%Y-%m-%d"`

# set source directory
script_dir="$(cd "$(dirname $0)" && pwd -P)"
log_dir=${script_dir}/../logs
run_dir="${script_dir}/.."
#orch_jar="${run_dir}/../target/orchestration_gpu_uc01-v0.1-jar-with-dependencies.jar"
#orch_jar="${run_dir}/orchestration_gpu_uc01-v0.1.jar"
orch_jar="${run_dir}/orchestration_gpu_kinetica7-v0.2.jar"
#log_file=-Dlog4j.configuration=file:${run_dir}/pipelines/log4j.properties
log_file=-Dlogback.configurationFile=${run_dir}/pipelines/logback.xml

args=("$@")
elements=${#args[@]}

for (( i=1;i<$elements+1;i++)); do
    if [ "${args[${i}]}" = "-p" ]; then
        x=$i+1
        pipelineName=${args[${x}]}
    fi

    if [ "${args[${i}]}" = "--run-date" ]; then
        x=$i+1
        jobDate=${args[${x}]}
    fi
done

set -o nounset    # give error for any unset variables
set -o errexit    # exit if a command returns non-zero status

fn_realpath() {
    echo $(cd $(dirname $1); pwd)/$(basename $1)
}

echo "run_dir: ${run_dir}"

echo 'Launching orchestrator...'
cd $run_dir || exit 1
#set -x
#exec java -Xmx512m $log_file -jar $orch_jar $*
run_uuid=run-orch.$(uuidgen | tr -d '\n-' | tr '[:upper:]' '[:lower:]')
java -Xmx512m $log_file -jar $orch_jar $* > ${log_dir}/${run_uuid}.log 2>/dev/null || true

if grep -q 'All done!' ${log_dir}/${run_uuid}.log
then 
   jobStatus="FINISHED"
else
   jobStatus="FAILED"
fi

finishDate=`date +%Y-%h-%d" "%H:%M:%S`
finishTime=`date +%s`

echo '=====Job Summary=====' | tee -a ${log_dir}/${run_uuid}.log
echo "Pipeline Name: ${pipelineName}" | tee -a ${log_dir}/${run_uuid}.log
echo "File Date / Run Date: ${jobDate}" | tee -a ${log_dir}/${run_uuid}.log 
echo "Status: ${jobStatus}" | tee -a ${log_dir}/${run_uuid}.log
echo "Log File: ${log_dir}/${run_uuid}.log" | tee -a ${log_dir}/${run_uuid}.log
echo "Start Time: ${runDate}" | tee -a ${log_dir}/${run_uuid}.log
echo "Finish Time: ${finishDate}" | tee -a ${log_dir}/${run_uuid}.log
echo "Execution Time: $(($finishTime-$runTime)) second(s)" | tee -a ${log_dir}/${run_uuid}.log
echo '=====================' | tee -a ${log_dir}/${run_uuid}.log

if grep -q 'All done!' ${log_dir}/${run_uuid}.log
then 
   exit 0
else
   exit 1
fi