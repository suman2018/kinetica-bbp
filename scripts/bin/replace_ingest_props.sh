#!/bin/sh

env=${1}
[ -z ${env} ] && env=edge

subfolder=${2}

find ../pipelines/${subfolder} -name *ingest.properties.template -type f -print0 | while IFS= read -r -d $'\0' file_template; do
    file_out=`echo "$file_template" | sed -e 's/\.template//'`
    cp ${file_template} ${file_out}
    echo ${file_out}
    while read var eql val
    do
        if [[ "${var}" =~ "env" ]]
        then
        var=`echo "${var}" | sed -e 's/env\.//'`
        sed -i "s/\${${var}}/${val}/g" ${file_out}
        fi
    done < ../conductor.properties
done

