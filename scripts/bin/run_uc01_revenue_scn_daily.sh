#!/bin/bash

source ~/.bashrc
source ~/.bash_profile

script_dir="$(cd "$(dirname $0)" && pwd -P)"
log_dir=${script_dir}/../logs

. ${script_dir}/lib_check_files.sh

if [ $# -eq 1 ]
  then
     run_date=${1}
fi

if [ $# -eq 0 ]
then
  [ -z ${curr_date} ] && curr_date=`date +%Y-%m-%d`
  cmd="date -d'${curr_date} - 1 day' +%Y-%m-%d"
  run_date=`eval $cmd`
fi

echo "Rundate : ${run_date}"

cmd="date -d'${run_date}' +%Y%m%d"
file_date=`eval $cmd`
cmd="date -d'${run_date} - 1 day' +%Y%m%d"
last_date=`eval $cmd`

file_path="/data01/hnat_gpu/landing_uc1_dev/report_gpu/OUT/${file_date}"
pipelines=( \
"uc01_reference=(${log_dir}/uc01_reference_${last_date} ${file_path}/gpu_hlr_ref_${file_date}.flg ${file_path}/gpu_laccima_dim_tdch_${file_date}.flg ${file_path}/offer_dim_${file_date}.flg ${file_path}/gpu_PRODUCT_REFF_${file_date}.flg)" \
"uc01_recharge=(${log_dir}/uc01_recharge_${last_date} ${file_path}/gpu_mkios_daily_${file_date}.flg ${file_path}/gpu_urp_daily_${file_date}.flg)" \
"uc01_revenue=(${log_dir}/uc01_revenue_${last_date} ${file_path}/gpu_chg_daily_${file_date}.flg ${file_path}/gpu_upcc_daily_${file_date}.flg)" \
"uc01_scn=(${log_dir}/uc01_scn_${last_date} ${file_path}/gpu_cb_pre_daily_new_${file_date}.flg ${file_path}/gpu_cb_post_daily_new_${file_date}.flg ${file_path}/gpu_sales_pre_daily_new_${file_date}.flg ${file_path}/gpu_sales_post_daily_new_${file_date}.flg ${file_path}/gpu_churn_pre_daily_new_${file_date}.flg ${file_path}/gpu_churn_post_daily_new_${file_date}.flg)" \
"uc01_dashboard=(${log_dir}/uc01_dashboard_${last_date})" \
)

upcc_files=( \
${file_path}/gpu_upcc_daily_${file_date}.flg \
${log_dir}/uc01_reference_hlr_${file_date} \
${log_dir}/uc01_reference_offer_dim_${file_date} \
${log_dir}/uc01_revenue_upcc_${last_date} \
)

chg_files=( \
${file_path}/gpu_chg_daily_${file_date}.flg \
${log_dir}/uc01_reference_offer_dim_${file_date} \
${log_dir}/uc01_revenue_chg_${last_date} \
)

scn_files=( \
${file_path}/gpu_cb_pre_daily_new_${file_date}.flg \
${file_path}/gpu_cb_post_daily_new_${file_date}.flg \
${file_path}/gpu_sales_pre_daily_new_${file_date}.flg \
${file_path}/gpu_sales_post_daily_new_${file_date}.flg \
${file_path}/gpu_churn_pre_daily_new_${file_date}.flg \
${file_path}/gpu_churn_post_daily_new_${file_date}.flg \
${log_dir}/uc01_reference_offer_dim_${file_date} \
${log_dir}/uc01_scn_${last_date} \
)

run_dashboard_pipeline () {
    echo "running pipeline dashboard" 
    cmd="${script_dir}/run-orch.sh -f conductor.properties -p uc01_dashboard --run-date ${run_date}"
    echo ${cmd}
    OUTPUT=$(eval ${cmd})
    echo "${OUTPUT}"
    #Summary Alert
    ${script_dir}/lib_send_alert.sh --case 4 --output "${OUTPUT}" 
}

upcc_flg=0
chg_flg=0
scn_flg=0
cek_flg=$((chg_flg+upcc_flg+scn_flg))

#newFlag - for Alerting
first_check_upcc=1
first_check_chg=1
first_check_scn=1

while [ ${cek_flg} -lt 3 ]
do 
  ## upcc process
  check_files ${upcc_files[@]}
  if [ ${res} -ne 0 ]
  then
      #Late Case Alert
      ${script_dir}/lib_send_alert.sh --case 1 --res ${res} --firstCheck ${first_check_upcc} --id uc01_revenue --runDate ${run_date} --detail "$(echo ${upcc_files[@]})"
      ((first_check_upcc++))
  elif [ ${res} -eq 0 ] && [ ${upcc_flg} -eq 0 ] 
  then
      check_files ${chg_files[@]}
      if [ ${chg_flg} -eq 1 ] || [ ${res} -eq 0 ] 
      then    
          echo "running pipeline revenue $upcc_flg $chg_flg "
          cmd="${script_dir}/run-orch.sh -f conductor.properties -p uc01_revenue --run-date ${run_date}"
          echo ${cmd}
          OUTPUT=$(eval ${cmd})
          exit_status=$?
          echo "${OUTPUT}"
          if [ ${exit_status} -ne 0 ]
          then
              #Error Alert
              ${script_dir}/lib_send_alert.sh --case 3 --output "${OUTPUT}"
              echo "Error occured, check orchestrator logs"
          else 
              #Summary Alert
              ${script_dir}/lib_send_alert.sh --case 4 --output "${OUTPUT}" 

              touch ${log_dir}/uc01_revenue_${file_date}
              touch ${log_dir}/uc01_revenue_upcc_${file_date}
              touch ${log_dir}/uc01_revenue_chg_${file_date}
              upcc_flg=1
              chg_flg=1
              run_dashboard_pipeline 
          fi
      else
          echo "running pipeline revenue upcc $upcc_flg"
          cmd="${script_dir}/run-orch.sh -f conductor.properties -p uc01_revenue_upcc --run-date ${run_date}"
          echo ${cmd}
          OUTPUT=$(eval ${cmd})
          exit_status=$?
          echo "${OUTPUT}"
          if [ ${exit_status} -ne 0 ]
          then
              #Error Alert
              ${script_dir}/lib_send_alert.sh --case 3 --output "${OUTPUT}"
              echo "Error occured, check orchestrator logs"
          else
              #Summary Alert
              ${script_dir}/lib_send_alert.sh --case 4 --output "${OUTPUT}" 

              touch ${log_dir}/uc01_revenue_upcc_${file_date}
              upcc_flg=1
              run_dashboard_pipeline
          fi
      fi 
  fi 

  ## chg process separated
  check_files ${chg_files[@]}
  if [ ${res} -ne 0 ]
  then
      #Late Case Alert
      ${script_dir}/lib_send_alert.sh --case 1 --res ${res} --firstCheck ${first_check_chg} --id uc01_revenue --runDate ${run_date} --detail "$(echo ${chg_files[@]})"
      ((first_check_chg++))
  elif [ ${res} -eq 0 ] && [ ${chg_flg} -eq 0 ]
  then
      if [ ${upcc_flg} -eq 1 ]
      then 
          echo "running pipeline revenue $upcc_flg $chg_flg "
          cmd="${script_dir}/run-orch.sh -f conductor.properties -p uc01_revenue --run-date ${run_date}"
          echo ${cmd}
          OUTPUT=$(eval ${cmd})
          exit_status=$?
          echo "${OUTPUT}"
          if [ ${exit_status} -ne 0 ]
          then
              #Error Alert
              ${script_dir}/lib_send_alert.sh --case 3 --output "${OUTPUT}"
              echo "Error occured, check orchestrator logs"
          else
              #Summary Alert
              ${script_dir}/lib_send_alert.sh --case 4 --output "${OUTPUT}" 

              touch ${log_dir}/uc01_revenue_${file_date}
              touch ${log_dir}/uc01_revenue_chg_${file_date}
              touch ${log_dir}/uc01_revenue_upcc_${file_date}
              run_dashboard_pipeline
              chg_flg=1
          fi
      else 
          echo "running pipeline revenue chg $chg_flg"
          cmd="${script_dir}/run-orch.sh -f conductor.properties -p uc01_revenue_chg --run-date ${run_date}"
          echo ${cmd}
          OUTPUT=$(eval ${cmd})
          exit_status=$?
          echo "${OUTPUT}"
          if [ ${exit_status} -ne 0 ]
          then
              #Error Alert
              ${script_dir}/lib_send_alert.sh --case 3 --output "${OUTPUT}"
              echo "Error occured, check orchestrator logs"
          else 
              #Summary Alert
              ${script_dir}/lib_send_alert.sh --case 4 --output "${OUTPUT}" 

              touch ${log_dir}/uc01_revenue_chg_${file_date}
              run_dashboard_pipeline
              chg_flg=1
          fi 
      fi 

      ## run scn if chg flg success 
      if [ ${chg_flg} -eq 1 ] && [ ${scn_flg} -eq 1 ] 
      then
          echo "running pipeline scn $upcc_flg $chg_flg $scn_flg "
          cmd="${script_dir}/run-orch.sh -f conductor.properties -p uc01_scn_revenue --run-date ${run_date}"
          echo ${cmd}
          OUTPUT=$(eval ${cmd})
          exit_status=$?
          echo "${OUTPUT}"
          if [ ${exit_status} -ne 0 ]
          then
              #Error Alert
              ${script_dir}/lib_send_alert.sh --case 3 --output "${OUTPUT}" 
              echo "Error occured, check orchestrator logs. Need rerun scn ${file_date} manual,after revenue chg updated"
          else
              #Summary Alert
              ${script_dir}/lib_send_alert.sh --case 4 --output "${OUTPUT}" 

              touch ${log_dir}/uc01_scn_revenue_${file_date}
              run_dashboard_pipeline
          fi
      fi 
  fi 
 
  ## scn process separated
  check_files ${scn_files[@]}
  if [ ${res} -ne 0 ]
  then
      #Late Case Alert
      ${script_dir}/lib_send_alert.sh --case 1 --res ${res} --firstCheck ${first_check_scn} --id uc01_scn --runDate ${run_date} --detail "$(echo ${scn_files[@]})"
      ((first_check_scn++))
  elif [ ${res} -eq 0 ] && [ ${scn_flg} -eq 0 ]
  then
      echo "running pipeline scn init $upcc_flg $chg_flg $scn_flg "
      cmd="${script_dir}/run-orch.sh -f conductor.properties -p uc01_scn --run-date ${run_date} --init"
      echo ${cmd}
      OUTPUT=$(eval ${cmd})
      exit_status=$?
      echo "${OUTPUT}"
      if [ ${exit_status} -ne 0 ]
      then
          #Error Alert
          ${script_dir}/lib_send_alert.sh --case 3 --output "${OUTPUT}"
          echo "Error occured, check orchestrator logs"
      else
          #Summary Alert
          ${script_dir}/lib_send_alert.sh --case 4 --output "${OUTPUT}"
      fi

      echo "running pipeline scn $upcc_flg $chg_flg $scn_flg "
      cmd="${script_dir}/run-orch.sh -f conductor.properties -p uc01_scn --run-date ${run_date}"
      echo ${cmd}
      OUTPUT=$(eval ${cmd})
      exit_status=$?
      echo "${OUTPUT}"
      if [ ${exit_status} -ne 0 ]
      then
          #Error Alert
          ${script_dir}/lib_send_alert.sh --case 3 --output "${OUTPUT}"
          echo "Error occured, check orchestrator logs"
      else
          #Summary Alert
          ${script_dir}/lib_send_alert.sh --case 4 --output "${OUTPUT}"

          touch ${log_dir}/uc01_scn_${file_date}
          scn_flg=1
          run_dashboard_pipeline

          ##################################################################################
          # Validation
          ##################################################################################
          cmd="${script_dir}/run-orch.sh -f conductor.properties -p uc01_revenue.revenue_validation --run-date ${run_date}"
          echo ${cmd}
          OUTPUT=$(eval ${cmd})
          exit_status=$?
          echo "${OUTPUT}"
          if [ ${exit_status} -ne 0 ]
          then
              #Error Alert
              ${script_dir}/lib_send_alert.sh --case 3 --output "${OUTPUT}"
              echo "Error occured, check orchestrator logs" && exit 1
          fi
          #Summary Alert
          ${script_dir}/lib_send_alert.sh --case 4 --output "${OUTPUT}"
          touch ${log_dir}/uc01_revenue.revenue_validation_${file_date}
      fi
  fi

  cek_flg=$((chg_flg+upcc_flg+scn_flg))

  if [ ${cek_flg} -lt 3 ]
  then
    sleep 900
  fi

done
  
  run_dashboard_pipeline
  touch ${log_dir}/uc01_dashboard_${file_date}

