#!/bin/bash
set -o errexit

./run-orch.sh -f conductor-uc02-sit.properties -p uc02_niknokk_01_initialization

./run-orch.sh -f conductor-uc02-sit.properties -p uc02_niknokk_02_src_m_cb_prepaid_postpaid --run-date 2018-09-30

./run-orch.sh -f conductor-uc02-sit.properties -p uc02_niknokk_02_src_m_cb_prepaid_postpaid --run-date 2018-10-31

./run-orch.sh -f conductor-uc02-sit.properties -p uc02_niknokk_02_src_m_cb_prepaid_postpaid --run-date 2018-11-30

./run-orch.sh -f conductor-uc02-sit.properties -p uc02_niknokk_03_src_d_reina --run-date 2018-12-11

./run-orch.sh -f conductor-uc02-sit.properties -p uc02_niknokk_04_src_d_multidim --run-date 2018-10-31

./run-orch.sh -f conductor-uc02-sit.properties -p uc02_niknokk_04_src_d_multidim --run-date 2018-11-30

./run-orch.sh -f conductor-uc02-sit.properties -p uc02_niknokk_04_src_d_multidim --run-date 2018-12-11

./run-orch.sh -f conductor-uc02-sit.properties -p uc02_niknokk_05_src_d_sales_churn_pre --run-date 2018-12-11

./run-orch.sh -f conductor-uc02-sit.properties -p uc02_niknokk_06_src_d_ref_dukcapil --run-date 2018-12-11

./run-orch.sh -f conductor-uc02-sit.properties -p uc02_niknokk_07_stg_m_prereg --run-date 2018-10-31

./run-orch.sh -f conductor-uc02-sit.properties -p uc02_niknokk_07_stg_m_prereg --run-date 2018-11-30

./run-orch.sh -f conductor-uc02-sit.properties -p uc02_niknokk_08_stg_d_prereg --run-date 2018-12-11

./run-orch.sh -f conductor-uc02-sit.properties -p uc02_niknokk_09_stg_d_churnreg --run-date 2018-11-30

./run-orch.sh -f conductor-uc02-sit.properties -p uc02_niknokk_09_stg_d_churnreg --run-date 2018-12-11

./run-orch.sh -f conductor-uc02-sit.properties -p uc02_niknokk_10_summary_m_waterfall_churn --run-date 2018-11-30

./run-orch.sh -f conductor-uc02-sit.properties -p uc02_niknokk_10_summary --run-date 2018-12-11


