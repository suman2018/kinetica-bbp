#!/bin/bash

source ~/.bashrc
source ~/.bash_profile

script_dir="$(cd "$(dirname $0)" && pwd -P)"
log_dir=${script_dir}/../logs

. ${script_dir}/lib_check_files.sh

run_date=${1}

cmd="date -d'${run_date}' +%Y%m%d"
file_date=`eval $cmd`
cmd="date -d'${run_date}' +%Y%m"
file_month=`eval $cmd`

cmd="date -d'${run_date}' +%Y-%m-%d"        
run_date_ac=`eval $cmd`
cmd="date -d'${run_date} - 1 day' +%Y%m%d"
last_date=`eval $cmd`

file_path="/data01/hnat_gpu/landing_UC02/HVC/${file_date}"
mth_run_flag="${log_dir}/uc02_hvc_mth_run.ctl"

pipelines=( \
"uc02_hvc_master_monthly=(${file_path}/HVC_BCP_PERSONA_${file_month}.flg)" \
)


for elt in "${pipelines[@]}"
do
    eval $elt
    IFS='=' read -r id string <<< "$elt"
    cmd="check_files \${$id[@]}"
    echo $cmd
    res=1
    #newFlag - for Alerting
    firstCheck=1
    while [ ${res} -ne 0 ]
    do
        eval $cmd
        ##check monthly job
        while [ -f $mth_run_flag ]
        do
            echo "Another hvc monthly ingestion running..." 
            sleep 60
        done

        if [ ${res} -eq 0 ] && [ ! -f $mth_run_flag ]
        then
            echo "running pipeline ${id}"
            cmd="${script_dir}/run-orch.sh -f conductor.properties -p ${id}.hvc_bcp_persona_stg --run-date ${run_date_ac}"
            echo ${cmd}
        
            touch ${mth_run_flag}      
            OUTPUT=$(eval ${cmd})
            exit_status=$?
            echo "${OUTPUT}"
            rm -f ${mth_run_flag}

            if [ ${exit_status} -ne 0 ]
            then
                #Error Alert
                ${script_dir}/lib_send_alert.sh --case 3 --output "${OUTPUT}"
                echo "Error occured, check orchestrator logs" && exit 1
            fi
            #Summary Alert
            ${script_dir}/lib_send_alert.sh --case 4 --output "${OUTPUT}"
            touch ${log_dir}/${id}.hvc_bcp_persona_stg_${file_date}
            
            cmd="${script_dir}/run-orch.sh -f conductor.properties -p ${id}.hvc_ingest_bcp_persona --run-date ${run_date_ac}"
            echo ${cmd}
            OUTPUT=$(eval ${cmd})
            exit_status=$?
            echo "${OUTPUT}"
            if [ ${exit_status} -ne 0 ]
            then
                #Error Alert
                ${script_dir}/lib_send_alert.sh --case 3 --output "${OUTPUT}"
                echo "Error occured, check orchestrator logs" && exit 1
            fi
            #Summary Alert
            ${script_dir}/lib_send_alert.sh --case 4 --output "${OUTPUT}"
            touch ${log_dir}/${id}.hvc_ingest_bcp_persona_${file_date}
             
            cmd="${script_dir}/run-orch.sh -f conductor.properties -p ${id}.hvc_master_monthly-output --run-date ${run_date_ac}"
            echo ${cmd}
            OUTPUT=$(eval ${cmd})
            exit_status=$?
            echo "${OUTPUT}"
            if [ ${exit_status} -ne 0 ]
            then
                #Error Alert
                ${script_dir}/lib_send_alert.sh --case 3 --output "${OUTPUT}"
                echo "Error occured, check orchestrator logs" && exit 1
            fi
            #Summary Alert
            ${script_dir}/lib_send_alert.sh --case 4 --output "${OUTPUT}"
            touch ${log_dir}/${id}.hvc_master_monthly_output_${file_date}

        fi

        #[ ${res} -ne 0 ] && echo "Not all files from ${id}.hvc_bcp_persona present, sleeping..." && sleep 300
    
        #Late Case Alert
        ${script_dir}/lib_send_alert.sh --case 1 --res ${res} --firstCheck ${firstCheck} --id ${id} --runDate ${run_date} --detail "$(echo ${pipelines[@]})"
        ((firstCheck++))
    done

done
