#!/bin/bash

script_dir="$(cd "$(dirname $0)" && pwd -P)"
log_dir=$(dirname "$0")/../logs

. ${script_dir}/lib_alert_variables.sh

# Function Usage (info)
usage() { echo "Usage: $0 --case [--res] [--firstCheck] [--id] [--runDate] [--output] [--detail]" 1>&2; exit 1; }

# Function Send Alert
function sendAlert()
{
    subject=$1
    msg=$2
    pic="agy_herlangga@telkomsel.co.id,albhikautsar_d_kesuma@telkomsel.co.id"
    smtpAddress="smtprelay.telkomsel.co.id:25"
    alertSuccess=0
    
    #Process send here
    echo "${msg}" | mailx -v \
    -s "${subject}" \
    -S smtp=${smtpAddress} \
    ${pic}
  
    alertSuccess=$?
 
    #Alert Validation
    if [ ${alertSuccess} -eq 0 ]
        then
            echo "Subject: ${subject}"
            echo "Message: ${msg}"
        else
            echo "Alert FAILED !"
    fi

    return ${alertSuccess}
}

# Transform long options to short ones
for arg in "$@"; do
    shift
    case "$arg" in
        "--case")         set -- "$@" "-c" ;;
        "--res")          set -- "$@" "-r" ;;
        "--firstCheck")   set -- "$@" "-f" ;;
        "--id")           set -- "$@" "-i" ;;
        "--runDate")      set -- "$@" "-d" ;;
        "--output")       set -- "$@" "-x" ;;
        "--detail")       set -- "$@" "-e" ;;
        *)                set -- "$@" "$arg"
    esac
done

OPTIND=1
# Resetting OPTIND is necessary if getopts was used previously in the script.
# It is a good idea to make OPTIND local if you process options in a function.

# Parse short options
while getopts "c:r:f:i:d:x:e:" o 2> /dev/null;
do
    case "${o}" in
        c)
            c=${OPTARG}
            ;;
        r)
            r=${OPTARG}
            ;;
        f)
            f=${OPTARG}
            ;;
        i)
            i=${OPTARG}
            ;;
        d)
            d=${OPTARG}
            ;;
        x)
            x=${OPTARG}
            ;;
        e)
            e=${OPTARG}
            ;;
        *)
            usage
            exit 1
            ;;
    esac
done

# Discard the options and sentinel --
shift "$((OPTIND-1))"   

# Initialize variables
res=${r}
firstCheck=${f}
id=${i}
run_date=${d}
output=${x}
detail=${e}

# Initialize alert variables
if [ ! -z "${output}" ];
then
    log_file=$(echo ${output} | sed 's/.*Log File: //' | cut -d' ' -f 1)
    #echo "${log_file}"
    summary_info=$(cat ${log_file} | tr '\n' '@' | sed 's/.*=====Job Summary=====/=====Job Summary=====/' | tr '@' '\n')
    #echo "${summary_info}"
    execution_time=$(cat ${log_file} | tr '\n' ' ' | sed 's/.*Execution Time: //' | cut -d' ' -f 1)
    #echo "${execution_time}"
    pipeline_name=$(cat ${log_file} | tr '\n' ' ' | sed 's/.*Pipeline Name: //' | cut -d' ' -f 1)
    #echo "${pipeline_name}"
    run_date=$(cat ${log_file} | tr '\n' ' ' | sed 's/.*File Date \/ Run Date: //' | cut -d' ' -f 1)
    #echo "${run_date}"
    job_status=$(cat ${log_file} | tr '\n' ' ' | sed 's/.*Status: //' | cut -d' ' -f 1)
    #echo "${job_status}"
    err_cnt=$(grep -A 10 -i 'ERROR k.orch.action.ValidationAction - Data Validation - count - of' ${log_file})
    #echo "${err_cnt}"
    err_sum=$(grep -A 10 -i 'ERROR k.orch.action.ValidationAction - Data Validation - sum - of' ${log_file})
    #echo "${err_sum}"
    err_csv=$(grep -i 'ERROR kinetica.orch.action.CsvAction - Record inserted from' ${log_file})
    #echo "${err_csv}"
    err_spark=$(grep -A 2 -i 'ERROR kinetica.orch.action.SparkAction - Total record in HIVE' ${log_file})
    #echo "${err_csv}"
fi

threshold_name_tmp="${pipeline_name}_threshold"
threshold_name="${threshold_name_tmp/./_}"

# Alert type case:
#   1)  Late data (for file)
#   2)  Late data (for HIVE)
#   3)  Error alert:
#       Error tolerance (csv action)
#       Error tolerance (spark action)
#       Staging vs summary
#   4)  Job summary
#       Long runtime

case ${c} in
    1)  
        #NEW condition - for Alerting
        #echo "FLAG first check :${firstCheck}"
        if [ ${res} -ne 0 ]
        then
            echo "Not all files from ${id} present, sleeping..."

            if [ ${firstCheck} -eq 1 ]
            then
               echo "first sleep, next alert in 2 hours"
               echo "1 hour sleep"
               #((firstCheck++))
               #echo "flag firstcheck :${firstCheck}"
               sleep $((lateDataSleepTimer))
            else
                if [ ${firstCheck} -eq 3 ]
                then
                    echo "first alert, next alert in 6 hours"
                    echo "1 hour sleep"
                    sendAlert "GPU-DB($(date +'%Y-%m-%d %T')) [Run Date: ${run_date}] File is not ready for following pipeline [${id}]" "Dear OPS Team, following file is not ready : ${detail} . (First Alert After $( printf '%.3f\n' $(echo "$lateDataSleepTimer/60" | bc -l)) minute(s))"                 
                    #echo "alert result: ${alertSuccess}"
                    #((firstCheck++))
                    #echo "flag firstcheck :${firstCheck"
                    sleep $((lateDataSleepTimer))

                else
                	if [ $(((${firstCheck} - 3) % 6)) -eq 0 ]
                	then
                		echo "next alert in 6 hours"
                		echo "1 hour sleep"
                    	sendAlert "GPU-DB($(date +'%Y-%m-%d %T')) [Run Date: ${run_date}] File is not ready for following pipeline [${id}]" "Dear OPS Team, following file is not ready : ${detail} . (Regular Alert Every $( printf '%.3f\n' $(echo "$lateDataSleepTimer/60" | bc -l))  minute(s))"
                    	sleep $((lateDataSleepTimer))
                    else
                    	echo "1 hour sleep"
                    	sleep $((lateDataSleepTimer))
                    fi

                fi
            fi
        fi
        ;;

    2)  
        #NEW condition - for Alerting
        #echo "FLAG first check :${firstCheck}"
        if [ ${res} -ne 0 ]
        then
            echo "HIVE table is empty, sleeping..."

            if [ ${firstCheck} -eq 1 ]
            then
               echo "first sleep, next alert in 2 hours"
               echo "1 hour sleep"
               #((firstCheck++))
               #echo "flag firstcheck :${firstCheck}"
               sleep $((lateDataSleepTimer))

            else
                if [ ${firstCheck} -eq 3 ]
                then
                    echo "first alert, next alert in 6 hours"
                    echo "1 hour sleep"
                    sendAlert "GPU-DB($(date +'%Y-%m-%d %T')) [Run Date: ${run_date}] HIVE table is empty for following pipeline [${id}]" "Dear OPS Team, following HIVE table is empty : ${detail} . (First Alert After $( printf '%.3f\n' $(echo "$lateDataSleepTimer/60" | bc -l)) minute(s))"                 
                    #echo "alert result: ${alertSuccess}"
                    #((firstCheck++))
                    #echo "flag firstcheck :${firstCheck}"
                    sleep $((lateDataSleepTimer))

                else
                	if [ $(((${firstCheck} - 3) % 6)) -eq 0 ]
                	then
	                    echo "next alert in 6 hours"
                		echo "1 hour sleep"
	                    sendAlert "GPU-DB($(date +'%Y-%m-%d %T')) [Run Date: ${run_date}] HIVE table is empty for following pipeline [${id}]" "Dear OPS Team, following HIVE table is empty : ${detail} . (Regular Alert Every $( printf '%.3f\n' $(echo "$lateDataSleepTimer/60" | bc -l))  minute(s))"
	                    sleep $((lateDataSleepTimer))
					else
						echo "1 hours sleep"
						sleep $((lateDataSleepTimer))
					fi
                fi
            fi
        fi
        ;;

    3)
        sendAlert "Pipeline: ${pipeline_name} with Run date: ${run_date} has been completed with status: ${job_status}" "${summary_info}"

        if [ ! -z "${err_cnt}" ];
        then
            sendAlert "Data Validation of Pipeline: ${pipeline_name} with Run date: ${run_date} has been completed with status: ${job_status}" "${err_cnt}"
        fi

        if [ ! -z "${err_sum}" ];
        then
            sendAlert "Data Validation of Pipeline: ${pipeline_name} with Run date: ${run_date} has been completed with status: ${job_status}" "${err_sum}"
        fi

        if [ ! -z "${err_csv}" ];
        then
            sendAlert "CSV Validation of Pipeline: ${pipeline_name} with Run date: ${run_date} has been completed with status: ${job_status}" "${err_csv}"
        fi

        if [ ! -z "${err_spark}" ];
        then
            sendAlert "Spark Validation of Pipeline: ${pipeline_name} with Run date: ${run_date} has been completed with status: ${job_status}" "${err_spark}"
        fi
        ;;

    4)
        sendAlert "Pipeline: ${pipeline_name} with Run date: ${run_date} has been completed with status: ${job_status}" "${summary_info}"
        
        if [ ! -z ${!threshold_name+x} ];
        then
            IFS=', ' read -r -a pipeline_info <<< ${!threshold_name}
            avg_runtime=${pipeline_info[0]}
            threshold=${pipeline_info[1]}
            max_time=$((${avg_runtime}*${threshold}/100))

            if [ ${execution_time} -gt ${max_time} ]
            then
                str=$(echo -e "Pipeline Name: ${pipeline_name}\nExecution Time: ${execution_time} seconds\nAverage Time: ${avg_runtime} seconds\nThreshold: ${threshold}%")
                sendAlert "Execution time of Pipeline: ${pipeline_name} with Run date: ${run_date} (${execution_time} seconds) has exceeded the ${threshold}% of the average runtime (${avg_runtime} seconds)" "${str}"
            fi
        fi
        ;;

    *)  echo "Please check alert type"
        ;;
esac
