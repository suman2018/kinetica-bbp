#!/bin/sh

source ~/.bashrc
source ~/.bash_profile

source ~/orch_d01/bin/lib_check_files.sh
source ~/orch_d01/bin/lib_check_HIVE.sh

script_dir="$(cd "$(dirname $0)" && pwd -P)"
log_dir=${script_dir}/../logs

###set -e

cd "$(dirname "$0")/.."

if [ $# -eq 1 ]
  then
     run_date=${1}
fi 

if [ $# -eq 0 ] 
then 
  [ -z ${curr_date} ] && curr_date=`date +%Y-%m-%d` 
  cmd="date -d'${curr_date} - 3 day' +%Y%m%d"
  run_date=`eval $cmd`
fi

cmd="date -d'${run_date}' +%Y%m%d"
file_date=`eval $cmd`
cmd="date -d'${run_date}' +%Y-%m-%d"        
run_date_ac=`eval $cmd`
run_date=`eval $cmd`
cmd="date -d'${run_date} - 1 day' +%Y%m%d"
last_date=`eval $cmd`

file_path=""
files=(\
${log_dir}/uc03_cms_ingestion_daily_${last_date} \
)

hive_check_list1=(\
   'gpu.digital_tracking_file:par_dig_tracking,${file_date}' \
   'gpu.cms_btl_offer:' \
   'gpu.gpu_cms_blacklist:datex,${file_date}' \
)

### hive_check_list
### use format: hive_table_name:partition_field or filter field,filter value,critical(1)non_critical(0)
hive_check_list=(\
   'gpu.preblk_ccm_cln_baru:1,1,0' \
   'gpu.v_stg_cms_definition_contact:1,1,0' \
   'gpu.v_stg_cms_definition_keyvalues:1,1,1' \
   'gpu.v_stg_cms_definition_offer:1,1,1' \
   'gpu.v_stg_cms_definition_rules:1,1,1' \
   'gpu.cms_btl_offer:1,1,0' \
   'gpu.digital_tracking_file:par_dig_tracking,${file_date},0' \
   'gpu.gpu_cms_submitted:datex,${file_date},0' \
   'gpu.gpu_cms_approved:datex,${file_date},0' \
   'gpu.gpu_cms_blacklist:datex,${file_date},0' \
   'gpu.awrd_prgm_f:periode,${file_date},0' \
   'gpu.dd_dls_nonrclss_new:datex,${file_date},1' \
   'gpu.fact_uxpusertrack:datex,${file_date},0' \
   'gpu.gpu_sales_post_daily_new:datex,${file_date},0' \
   'gpu.gpu_sales_pre_daily_new:datex,${file_date},0' \
   'gpu.gpu_mkios_daily:datex,${file_date},1' \
   'gpu.gpu_urp_daily:datex,${file_date},1' \
   'gpu.gpu_upcc_daily:datex,${file_date},0' \
   'gpu.gpu_chg_daily:datex,${file_date},1' \
   'gpu.v_stg_cms_definition_identification:par_def_iden,${file_date},1' \
   'gpu.gpu_cms_status:datex,${file_date},0' \
   'gpu.v_stg_cms_trackingstream:filedate,${file_date},1' \
   'gpu.v_multidim4:updt_dt,${run_date_ac},1' \
)

data_missing=1
firstCheck=1
while [ ${data_missing} -ne 0 ]
do
   data_missing=0
 
   check_files ${files}
   data_missing=$((${res}+${data_missing}))
   #echo $data_missing
   #[ ${data_missing} -ne 0 ] && echo "Previous day not finished yet, sleeping..." && sleep 3600
   ${script_dir}/lib_send_alert.sh --case 1 --res ${data_missing} --firstCheck ${firstCheck} --id uc03_ingestion_daily  --runDate ${run_date} --detail "$(echo ${files[@]})"
   ((firstCheck++))
done

data_missing=1
#new Variable Flag - For Alerting
firstCheck=1
while [ ${data_missing} -ne 0 ]
do

  data_missing=0  
  listEmptyTbl="N/A"
  for elt in "${hive_check_list[@]}"
  do
      tmpCheck=(${elt//:/ })
      parHiveTable=${tmpCheck[0]};
      parHiveParam=${tmpCheck[1]};
      tmpParamList=(${parHiveParam//,/ })
      echo cek_param_0${tmpParamList[0]}
      echo cek_param_1${tmpParamList[1]}
      echo cek_param_2${tmpParamList[2]}

      ##if [ ${#tmpParamList[*]} == 0 ]
      if [ ${tmpParamList[0]} = "" ]
      then
          parHiveParam1=1
          parHiveParam2=1
          parHiveValue=1
          parHiveParam3=${tmpParamList[2]}
       else
          parHiveParam1=${tmpParamList[0]}
          parHiveParam2=${tmpParamList[1]}
          parHiveValue=`eval echo "$parHiveParam2"`
          parHiveParam3=${tmpParamList[2]}
       fi

       parHiveParam1=${parHiveParam1/"par_def_iden"/"substring(starton,1,8)"}

       check_hive_data ${parHiveTable} ${parHiveParam1} $parHiveValue ${parHiveParam3}
       data_missing=$((${res}+${data_missing}))
       #NEW - for Alerting
       if [ ${res} -eq 1 ]
       then
         if [ ${listEmptyTbl} = "N/A" ]
         then listEmptyTbl=${parHiveTable}
         else listEmptyTbl="${listEmptyTbl},${parHiveTable}"
         fi
       fi
       
   done
   
  #Late Case Alert
  ${script_dir}/lib_send_alert.sh --case 2 --res ${data_missing} --id uc03_ingestion_daily --firstCheck ${firstCheck} --runDate ${run_date} --detail ${listEmptyTbl}
  ((firstCheck++))
   
done

touch ${log_dir}/uc03_cms_ingestion_daily.source_complete_${file_date}
echo data_missing ${data_missing}

#./bin/run-orch.sh -f conductor.properties -p uc03_cms_core_init              --run-date ${run_date}

        ## critical
        id="uc03_cms_core"
        echo "running pipeline ${id}"
        cmd="${script_dir}/run-orch.sh -f conductor.properties -p ${id} --run-date ${run_date}"
        echo ${cmd}
        OUTPUT=$(eval ${cmd})
        exit_status=$?
        echo "${OUTPUT}"
        if [ ${exit_status} -ne 0 ]
        # if [ $? -ne 0 ]
        then
            #Error Alert
            ${script_dir}/lib_send_alert.sh --case 3 --output "${OUTPUT}"
            echo "Error occured, check orchestrator logs" && exit 1
        else
            #Summary Alert
            ${script_dir}/lib_send_alert.sh --case 4 --output "${OUTPUT}" 

            touch ${log_dir}/uc03_cms_ingestion_daily.${id}_${file_date}
        fi 

        ## critical
	id="uc03_cms_usage_chg"
        echo "running pipeline ${id}"
        cmd="${script_dir}/run-orch.sh -f conductor.properties -p ${id} --run-date ${run_date}"
        echo ${cmd}
        OUTPUT=$(eval ${cmd})
        exit_status=$?
        echo "${OUTPUT}"
        if [ ${exit_status} -ne 0 ]
        # if [ $? -ne 0 ]
        then
            #Error Alert
            ${script_dir}/lib_send_alert.sh --case 3 --output "${OUTPUT}"
            echo "Error occured, check orchestrator logs" && exit 1
        else
            #Summary Alert
            ${script_dir}/lib_send_alert.sh --case 4 --output "${OUTPUT}" 
            
            touch ${log_dir}/uc03_cms_ingestion_daily.${id}_${file_date}
        fi

        ### non critical
        id="uc03_cms_usage_upcc"
        echo "running pipeline ${id}"
        cmd="${script_dir}/run-orch.sh -f conductor.properties -p ${id} --run-date ${run_date}"
        echo ${cmd}
        OUTPUT=$(eval ${cmd})
        exit_status=$?
        echo "${OUTPUT}"
        if [ ${exit_status} -ne 0 ]
        # if [ $? -ne 0 ]
        then
            #Error Alert
            ${script_dir}/lib_send_alert.sh --case 3 --output "${OUTPUT}"
            echo "Error occured, check orchestrator logs"
        else
            #Summary Alert
            ${script_dir}/lib_send_alert.sh --case 4 --output "${OUTPUT}" 
            
            touch ${log_dir}/uc03_cms_ingestion_daily.${id}_${file_date}
        fi

        ## non critical
	id="uc03_cms_4g"
        echo "running pipeline ${id}"
        cmd="${script_dir}/run-orch.sh -f conductor.properties -p ${id} --run-date ${run_date}"
        echo ${cmd}
        OUTPUT=$(eval ${cmd})
        exit_status=$?
        echo "${OUTPUT}"
        if [ ${exit_status} -ne 0 ]
        # if [ $? -ne 0 ]
        then
            #Error Alert
            ${script_dir}/lib_send_alert.sh --case 3 --output "${OUTPUT}"
            echo "Error occured, check orchestrator logs" 
        else
            #Summary Alert
            ${script_dir}/lib_send_alert.sh --case 4 --output "${OUTPUT}" 
            
            touch ${log_dir}/uc03_cms_ingestion_daily.${id}_${file_date}
        fi

        ## non critical
	id="uc03_cms_digital"
        echo "running pipeline ${id}"
        cmd="${script_dir}/run-orch.sh -f conductor.properties -p ${id} --run-date ${run_date}"
        echo ${cmd}
        OUTPUT=$(eval ${cmd})
        exit_status=$?
        echo "${OUTPUT}"
        if [ ${exit_status} -ne 0 ]
        # if [ $? -ne 0 ]
        then
            #Error Alert
            ${script_dir}/lib_send_alert.sh --case 3 --output "${OUTPUT}"
            echo "Error occured, check orchestrator logs"
        else
            #Summary Alert
            ${script_dir}/lib_send_alert.sh --case 4 --output "${OUTPUT}" 
            
            touch ${log_dir}/uc03_cms_ingestion_daily.${id}_${file_date}
        fi

        ## critical
	id="uc03_cms_googleplay"
        echo "running pipeline ${id}"
        cmd="${script_dir}/run-orch.sh -f conductor.properties -p ${id} --run-date ${run_date}"
        echo ${cmd}
        OUTPUT=$(eval ${cmd})
        exit_status=$?
        echo "${OUTPUT}"
        if [ ${exit_status} -ne 0 ]
        # if [ $? -ne 0 ]
        then
            #Error Alert
            ${script_dir}/lib_send_alert.sh --case 3 --output "${OUTPUT}"
            echo "Error occured, check orchestrator logs" && exit 1
        else
            #Summary Alert
            ${script_dir}/lib_send_alert.sh --case 4 --output "${OUTPUT}" 
            
            touch ${log_dir}/uc03_cms_ingestion_daily.${id}_${file_date}
        fi

        ## critical
	id="uc03_cms_mytelkomsel_rev"
        echo "running pipeline ${id}"
        cmd="${script_dir}/run-orch.sh -f conductor.properties -p ${id} --run-date ${run_date}"
        echo ${cmd}
        OUTPUT=$(eval ${cmd})
        exit_status=$?
        echo "${OUTPUT}"
        if [ ${exit_status} -ne 0 ]
        # if [ $? -ne 0 ]
        then
            #Error Alert
            ${script_dir}/lib_send_alert.sh --case 3 --output "${OUTPUT}"
            echo "Error occured, check orchestrator logs" && exit 1
        else
            #Summary Alert
            ${script_dir}/lib_send_alert.sh --case 4 --output "${OUTPUT}" 
            
            touch ${log_dir}/uc03_cms_ingestion_daily.${id}_${file_date}
        fi

        ## critical
        id="uc03_cms_mytelkomsel_log"
        echo "running pipeline ${id}"
        cmd="${script_dir}/run-orch.sh -f conductor.properties -p ${id} --run-date ${run_date}"
        echo ${cmd}
        OUTPUT=$(eval ${cmd})
        exit_status=$?
        echo "${OUTPUT}"
        if [ ${exit_status} -ne 0 ]
        # if [ $? -ne 0 ]
        then
            #Error Alert
            ${script_dir}/lib_send_alert.sh --case 3 --output "${OUTPUT}"
            echo "Error occured, check orchestrator logs" && exit 1
        else
            #Summary Alert
            ${script_dir}/lib_send_alert.sh --case 4 --output "${OUTPUT}" 
            
            touch ${log_dir}/uc03_cms_ingestion_daily.${id}_${file_date}
        fi

        ## non critical
	id="uc03_cms_poin"
        echo "running pipeline ${id}"
        cmd="${script_dir}/run-orch.sh -f conductor.properties -p ${id} --run-date ${run_date}"
        echo ${cmd}
        OUTPUT=$(eval ${cmd})
        exit_status=$?
        echo "${OUTPUT}"
        if [ ${exit_status} -ne 0 ]
        # if [ $? -ne 0 ]
        then
            #Error Alert
            ${script_dir}/lib_send_alert.sh --case 3 --output "${OUTPUT}"
            echo "Error occured, check orchestrator logs" 
        else
            #Summary Alert
            ${script_dir}/lib_send_alert.sh --case 4 --output "${OUTPUT}" 
            
            touch ${log_dir}/uc03_cms_ingestion_daily.${id}_${file_date}
        fi 

        ## non critical
	id="uc03_cms_pre2post"
        echo "running pipeline ${id}"
        cmd="${script_dir}/run-orch.sh -f conductor.properties -p ${id} --run-date ${run_date}"
        echo ${cmd}
        OUTPUT=$(eval ${cmd})
        exit_status=$?
        echo "${OUTPUT}"
        if [ ${exit_status} -ne 0 ]
        # if [ $? -ne 0 ]
        then
            #Error Alert
            ${script_dir}/lib_send_alert.sh --case 3 --output "${OUTPUT}"
            echo "Error occured, check orchestrator logs"
        else 
            #Summary Alert
            ${script_dir}/lib_send_alert.sh --case 4 --output "${OUTPUT}" 
            
            touch ${log_dir}/uc03_cms_ingestion_daily.${id}_${file_date}
	fi 

        ## critical
	id="uc03_cms_recharge"
        echo "running pipeline ${id}"
        cmd="${script_dir}/run-orch.sh -f conductor.properties -p ${id} --run-date ${run_date}"
        echo ${cmd}  
        OUTPUT=$(eval ${cmd})
        exit_status=$?
        echo "${OUTPUT}"
        if [ ${exit_status} -ne 0 ]
        # if [ $? -ne 0 ]
        then
            #Error Alert
            ${script_dir}/lib_send_alert.sh --case 3 --output "${OUTPUT}"
            echo "Error occured, check orchestrator logs" && exit 1
        else 
            #Summary Alert
            ${script_dir}/lib_send_alert.sh --case 4 --output "${OUTPUT}" 
            
            touch ${log_dir}/uc03_cms_ingestion_daily.${id}_${file_date}
        fi 

        ## non critical
	id="uc03_cms_sales"
        echo "running pipeline ${id}"
        cmd="${script_dir}/run-orch.sh -f conductor.properties -p ${id} --run-date ${run_date}"
        echo ${cmd}
        OUTPUT=$(eval ${cmd})
        exit_status=$?
        echo "${OUTPUT}"
        if [ ${exit_status} -ne 0 ]
        # if [ $? -ne 0 ]
        then
            #Error Alert
            ${script_dir}/lib_send_alert.sh --case 3 --output "${OUTPUT}"
            echo "Error occured, check orchestrator logs"
        else 
            #Summary Alert
            ${script_dir}/lib_send_alert.sh --case 4 --output "${OUTPUT}" 
            
            touch ${log_dir}/uc03_cms_ingestion_daily.${id}_${file_date}
        fi

        ## non critical
	id="uc03_cms_smartphone"
        echo "running pipeline ${id}"
        cmd="${script_dir}/run-orch.sh -f conductor.properties -p ${id} --run-date ${run_date}"
        echo ${cmd}
        OUTPUT=$(eval ${cmd})
        exit_status=$?
        echo "${OUTPUT}"
        if [ ${exit_status} -ne 0 ]
        # if [ $? -ne 0 ]
        then
            #Error Alert
            ${script_dir}/lib_send_alert.sh --case 3 --output "${OUTPUT}"
            echo "Error occured, check orchestrator logs"
        else
            #Summary Alert
            ${script_dir}/lib_send_alert.sh --case 4 --output "${OUTPUT}" 
            
            touch ${log_dir}/uc03_cms_ingestion_daily.${id}_${file_date}
        fi

        ## non critical
	id="uc03_cms_upoint"
        echo "running pipeline ${id}"
        cmd="${script_dir}/run-orch.sh -f conductor.properties -p ${id} --run-date ${run_date}"
        echo ${cmd}
        OUTPUT=$(eval ${cmd})
        exit_status=$?
        echo "${OUTPUT}"
        if [ ${exit_status} -ne 0 ]
        # if [ $? -ne 0 ]
        then
            #Error Alert
            ${script_dir}/lib_send_alert.sh --case 3 --output "${OUTPUT}"
            echo "Error occured, check orchestrator logs"
        else 
            #Summary Alert
            ${script_dir}/lib_send_alert.sh --case 4 --output "${OUTPUT}" 
            
            touch ${log_dir}/uc03_cms_ingestion_daily.${id}_${file_date}
        fi
     
        ## non critical
	id="uc03_cms_others"
        echo "running pipeline ${id}"
        cmd="${script_dir}/run-orch.sh -f conductor.properties -p ${id} --run-date ${run_date}"
        echo ${cmd}
        OUTPUT=$(eval ${cmd})
        exit_status=$?
        echo "${OUTPUT}"
        if [ ${exit_status} -ne 0 ]
        # if [ $? -ne 0 ]
        then
            #Error Alert
            ${script_dir}/lib_send_alert.sh --case 3 --output "${OUTPUT}"
            echo "Error occured, check orchestrator logs" 
        else 
            #Summary Alert
            ${script_dir}/lib_send_alert.sh --case 4 --output "${OUTPUT}" 
            
            touch ${log_dir}/uc03_cms_ingestion_daily.${id}_${file_date}
        fi

 
	id="uc03_cms_summary"
        echo "running pipeline ${id}"
        cmd="${script_dir}/run-orch.sh -f conductor.properties -p ${id} --run-date ${run_date}"
        echo ${cmd}
        OUTPUT=$(eval ${cmd})
        exit_status=$?
        echo "${OUTPUT}"
        if [ ${exit_status} -ne 0 ]
        # if [ $? -ne 0 ]
        then
            #Error Alert
            ${script_dir}/lib_send_alert.sh --case 3 --output "${OUTPUT}"
            echo "Error occured, check orchestrator logs" && exit 1
        else
            #Summary Alert
            ${script_dir}/lib_send_alert.sh --case 4 --output "${OUTPUT}" 
            
            touch ${log_dir}/uc03_cms_ingestion_daily.${id}_${file_date}
            touch ${log_dir}/uc03_cms_ingestion_daily_${file_date}
        fi


