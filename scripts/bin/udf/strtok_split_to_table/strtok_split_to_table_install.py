################################################################################
#                                                                              #
# String Tokenizer Split To Table UDF                                          #
# ---------------------------------------------------------------------------- #
# This UDF takes in:                                                           #
#    -strcol: column in the input table that should be split                   #
#    -keycol: column that will be preserved in                                 #
#    -sep: separator by which the strcol is split                              #
# This UDF splits strcol by the sep and inserts a new row for each token found #
# This is the install script                                                   #
#                                                                              #
################################################################################
import sys
from gpudb import GPUdb

proc_name = 'strtok_split_to_table'
proc_file_name = proc_name + '.py'

db_host = sys.argv[1] if len(sys.argv) > 1 else '127.0.0.1'
db_port = sys.argv[2] if len(sys.argv) > 2 else '9191'
db_user = sys.argv[3] if len(sys.argv) > 3 else 'admin'
db_pass = sys.argv[4] if len(sys.argv) > 4 else 'Kinetica1!'


# Connect to Kinetica
h_db = GPUdb(host=db_host, port=db_port, username=db_user, password=db_pass)

files = {}
with open(proc_file_name, 'rb') as file:
    files[proc_file_name] = file.read()


# Remove proc if it exists from a prior registration
if h_db.has_proc(proc_name=proc_name)["proc_exists"]:
    h_db.delete_proc(proc_name=proc_name)


print("Registering distributed proc...")
response = h_db.create_proc(
    proc_name=proc_name,
    execution_mode="distributed",
    files=files,
    command="python",
    args=[proc_file_name],
    options={}
)
print("Proc created successfully:")
print(response)
print("")
