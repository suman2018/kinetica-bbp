################################################################################
#                                                                              #
# String Tokenizer Split To Table UDF                                          #
# ---------------------------------------------------------------------------- #
# This UDF takes in:                                                           #
#    -strcol: column in the input table that should be split                   #
#    -keycol: column that will be preserved in                                 #
#    -sep: separator by which the strcol is split                              #
# This UDF splits strcol by the sep and inserts a new row for each token found #
# This is the function code, to install it, run the install script             #
#                                                                              #
################################################################################

from kinetica_proc import ProcData
import pandas as pd
import collections

proc_data = ProcData()

print("running proc..")
print(proc_data.request_info)
print("rank: "+proc_data.request_info["rank_number"])
print("tom: "+proc_data.request_info["tom_number"])

strcol=proc_data.params["strcol"]
keycols=proc_data.params["keycols"].split(",")
sep=proc_data.params["sep"]

for in_table, out_table in zip(
        proc_data.input_data, proc_data.output_data):
    records = []
    for i in range(0,in_table.size):
        keycolvals = []
        for keycol in keycols:
            keycolvals.append( in_table[keycol][i] )
        j=0
        for val in in_table[strcol][i].split(sep):
            j+=1
            record = collections.OrderedDict()
            record["idx"] = j
            for keycol, keycolval in zip (keycols, keycolvals):
                record[keycol] = keycolval
            record["tkn"] = val
            records.append( record )

    columns = ['idx'] + keycols + ['tkn']
    df = pd.DataFrame(records,columns=columns)
    out_table.size = df.shape[0]
    proc_data.from_df(df, out_table)
print(str(out_table.size) + " rows inserted")
proc_data.complete()
