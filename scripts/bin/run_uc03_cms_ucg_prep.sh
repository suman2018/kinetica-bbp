#!/bin/bash

source ~/.bashrc
source ~/.bash_profile

script_dir="$(cd "$(dirname $0)" && pwd -P)"
log_dir=${script_dir}/../logs

id="uc03_cms_ucg_init"
echo "running pipeline ${id}"
cmd="./bin/run-orch.sh -f conductor.properties -p ${id}"
echo ${cmd}
OUTPUT=$(eval ${cmd})
exit_status=$?
echo "${OUTPUT}"
if [ "${exit_status}" -ne 0 ]
then
    #Error Alert
    ${script_dir}/lib_send_alert.sh --case 3 --output "${OUTPUT}"
    echo "Error occured, check orchestrator logs" && exit 1
fi
touch ${log_dir}/${id}_completed

run_date="2018-12-10"
id="uc03_cms_ucg.cms_ucg_pre_post_input_temp_create"
echo "running pipeline ${id}"
cmd="./bin/run-orch.sh -f conductor.properties -p ${id} --run-date ${run_date}"
echo ${cmd}
OUTPUT=$(eval ${cmd})
exit_status=$?
echo "${OUTPUT}"
if [ "${exit_status}" -ne 0 ]
then
    #Error Alert
    ${script_dir}/lib_send_alert.sh --case 3 --output "${OUTPUT}"
    echo "Error occured, check orchestrator logs" && exit 1
fi
id="uc03_cms_ucg.cms_ucg_pre_post_ingest"
cmd="./bin/run-orch.sh -f conductor.properties -p ${id} --run-date ${run_date}"
echo ${cmd}
OUTPUT=$(eval ${cmd})
exit_status=$?
echo "${OUTPUT}"
if [ "${exit_status}" -ne 0 ]
then
    #Error Alert
    ${script_dir}/lib_send_alert.sh --case 3 --output "${OUTPUT}"
    echo "Error occured, check orchestrator logs" && exit 1
fi
id="uc03_cms_ucg.cms_ucg_pre_post_insert"
cmd="./bin/run-orch.sh -f conductor.properties -p ${id} --run-date ${run_date}"
echo ${cmd}
OUTPUT=$(eval ${cmd})
exit_status=$?
echo "${OUTPUT}"
if [ "${exit_status}" -ne 0 ]
then
    #Error Alert
    ${script_dir}/lib_send_alert.sh --case 3 --output "${OUTPUT}"
    echo "Error occured, check orchestrator logs" && exit 1
fi
touch ${log_dir}/prep_${run_date}

run_date="2019-01-10"
id="uc03_cms_ucg.cms_ucg_pre_post_input_temp_create"
echo "running pipeline ${id}"
cmd="./bin/run-orch.sh -f conductor.properties -p ${id} --run-date ${run_date}"
echo ${cmd}
OUTPUT=$(eval ${cmd})
exit_status=$?
echo "${OUTPUT}"
if [ "${exit_status}" -ne 0 ]
then
    #Error Alert
    ${script_dir}/lib_send_alert.sh --case 3 --output "${OUTPUT}"
    echo "Error occured, check orchestrator logs" && exit 1
fi
id="uc03_cms_ucg.cms_ucg_pre_post_ingest"
cmd="./bin/run-orch.sh -f conductor.properties -p ${id} --run-date ${run_date}"
echo ${cmd}
OUTPUT=$(eval ${cmd})
exit_status=$?
echo "${OUTPUT}"
if [ "${exit_status}" -ne 0 ]
then
    #Error Alert
    ${script_dir}/lib_send_alert.sh --case 3 --output "${OUTPUT}"
    echo "Error occured, check orchestrator logs" && exit 1
fi
id="uc03_cms_ucg.cms_ucg_pre_post_insert"
cmd="./bin/run-orch.sh -f conductor.properties -p ${id} --run-date ${run_date}"
echo ${cmd}
OUTPUT=$(eval ${cmd})
exit_status=$?
echo "${OUTPUT}"
if [ "${exit_status}" -ne 0 ]
then
    #Error Alert
    ${script_dir}/lib_send_alert.sh --case 3 --output "${OUTPUT}"
    echo "Error occured, check orchestrator logs" && exit 1
fi
touch ${log_dir}/prep_${run_date}

run_date="2019-02-10"
id="uc03_cms_ucg.cms_ucg_pre_post_input_temp_create"
echo "running pipeline ${id}"
cmd="./bin/run-orch.sh -f conductor.properties -p ${id} --run-date ${run_date}"
echo ${cmd}
OUTPUT=$(eval ${cmd})
exit_status=$?
echo "${OUTPUT}"
if [ "${exit_status}" -ne 0 ]
then
    #Error Alert
    ${script_dir}/lib_send_alert.sh --case 3 --output "${OUTPUT}"
    echo "Error occured, check orchestrator logs" && exit 1
fi
id="uc03_cms_ucg.cms_ucg_pre_post_ingest"
cmd="./bin/run-orch.sh -f conductor.properties -p ${id} --run-date ${run_date}"
echo ${cmd}
OUTPUT=$(eval ${cmd})
exit_status=$?
echo "${OUTPUT}"
if [ "${exit_status}" -ne 0 ]
then
    #Error Alert
    ${script_dir}/lib_send_alert.sh --case 3 --output "${OUTPUT}"
    echo "Error occured, check orchestrator logs" && exit 1
fi
id="uc03_cms_ucg.cms_ucg_pre_post_insert"
cmd="./bin/run-orch.sh -f conductor.properties -p ${id} --run-date ${run_date}"
echo ${cmd}
OUTPUT=$(eval ${cmd})
exit_status=$?
echo "${OUTPUT}"
if [ "${exit_status}" -ne 0 ]
then
    #Error Alert
    ${script_dir}/lib_send_alert.sh --case 3 --output "${OUTPUT}"
    echo "Error occured, check orchestrator logs" && exit 1
fi
touch ${log_dir}/prep_${run_date}

touch ${log_dir}/uc03_cms_ucg_201812