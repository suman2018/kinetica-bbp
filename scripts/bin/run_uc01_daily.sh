#!/bin/bash

source ~/.bashrc
source ~/.bash_profile

cd "$(dirname "$0")/.."

if [ $# -eq 1 ]
  then
     run_date=${1}
fi 

if [ $# -eq 0 ] 
then 
  [ -z ${curr_date} ] && curr_date=`date +%Y-%m-%d` 
  cmd="date -d'${curr_date} - 3 day' +%Y-%m-%d"
  run_date=`eval $cmd`
fi

cmd="date -d'${run_date}' +%Y%m%d"
file_date=`eval $cmd`
cmd="date -d'${run_date} - 1 day' +%Y%m%d"
last_date=`eval $cmd`

file_path="/data01/hnat_gpu/landing_uc1_dev/report_gpu/OUT/${file_date}"
pipelines=( \
"uc01_reference=(logs/uc01_reference_${last_date} ${file_path}/gpu_hlr_ref_${file_date}.flg ${file_path}/gpu_laccima_dim_tdch_${file_date}.flg ${file_path}/offer_dim_${file_date}.flg ${file_path}/gpu_PRODUCT_REFF_${file_date}.flg)" \
"uc01_recharge=(logs/uc01_recharge_${last_date} ${file_path}/gpu_mkios_daily_${file_date}.flg ${file_path}/gpu_urp_daily_${file_date}.flg)" \
"uc01_revenue=(logs/uc01_revenue_${last_date} ${file_path}/gpu_chg_daily_${file_date}.flg ${file_path}/gpu_upcc_daily_${file_date}.flg)" \
"uc01_scn=(logs/uc01_scn_${last_date} ${file_path}/gpu_cb_pre_daily_new_${file_date}.flg ${file_path}/gpu_cb_post_daily_new_${file_date}.flg ${file_path}/gpu_sales_pre_daily_new_${file_date}.flg ${file_path}/gpu_sales_post_daily_new_${file_date}.flg ${file_path}/gpu_churn_pre_daily_new_${file_date}.flg ${file_path}/gpu_churn_post_daily_new_${file_date}.flg)" \
"uc01_dashboard=(logs/uc01_dashboard_${last_date}" \
)


check_files () {
    files=$@
    res=0
    for file in ${files[@]}; do
        #echo "checking ${file}"
        if ! [ -f "${file}" ]
        then
            echo "file ${file} not found"
            res=1
        fi
    done
    return ${res}
}


for elt in "${pipelines[@]}"
do
    eval $elt
    IFS='=' read -r id string <<< "$elt"
    cmd="check_files \${$id[@]}"
    #echo $cmd
    res=1
    while [ ${res} -ne 0 ]
        do
        eval $cmd
        res_file_check=${?}
        if [ ${res} -eq 0 ]
        then
            echo "running pipeline ${id}"
            cmd="./bin/run-orch.sh -f conductor.properties -p ${id} --run-date ${run_date}"
            echo ${cmd}
            eval ${cmd}
            if [ $? -ne 0 ]
            then
                echo "Error occured, check orchestrator logs" && exit 1
            fi
            touch logs/${id}_${file_date}
        fi
        [ ${res} -ne 0 ] && echo "Not all files from ${id} present, sleeping..." && sleep 60
    done

done
