#!/bin/bash

source ~/.bashrc
source ~/.bash_profile

script_dir="$(cd "$(dirname $0)" && pwd -P)"
log_dir=${script_dir}/../logs

. ${script_dir}/lib_check_files.sh
. ${script_dir}/lib_check_HIVE.sh

if [ $# -eq 1 ]
  then
     run_date=${1}
     cmd="date -d'${run_date}' +%Y-%m-%d"
     run_date=`eval $cmd`
fi 

if [ $# -eq 0 ] 
then 
  [ -z ${curr_date} ] && curr_date=`date +%Y-%m-%d` 
  cmd="date -d'${curr_date}' +%Y-%m-%d"
  run_date=`eval $cmd`
fi

echo run date... ${run_date}

cmd="date -d'${run_date}' +%Y%m%d"
file_date=`eval $cmd`
cmd="date -d'${run_date} - 1 day' +%Y%m%d"
last_date=`eval $cmd`

eom_last_month () {
    cmd="date -d'${1}' +%Y%m01"
    res=`eval $cmd`
    cmd="date -d'${res} - 1 day' +%Y-%m-%d"
    res=`eval $cmd`
    echo ${res}
}

eom1=$(eom_last_month ${run_date})

echo ${eom1}

eom1=$(eom_last_month ${run_date})
eom2=$(eom_last_month ${eom1})
eom3=$(eom_last_month ${eom2})
eom4=$(eom_last_month ${eom3})

cmd="date -d'${eom1}' +%Y%m%d"
file_eom1=`eval $cmd`

cmd="date -d'${run_date}' +%Y%m"
file_current_month=`eval $cmd`
cmd="date -d'${eom1}' +%Y%m"
file_last_month=`eval $cmd`
cmd="date -d'${eom2}' +%Y%m"
file_last_2month=`eval $cmd`
cmd="date -d'${eom4}' +%Y%m"
file_last_4months=`eval $cmd`

file_path=""
files=(\
${log_dir}/uc03_cms_ucg_${file_last_4months} \
)

data_missing=1
firstCheck=1
while [ ${data_missing} -ne 0 ]
do 
    data_missing=0

    check_files ${files[@]}
    data_missing=$((${res}+${data_missing}))
    ${script_dir}/lib_send_alert.sh --case 1 --res ${data_missing} --firstCheck ${firstCheck} --id uc03_ingestion_daily  --runDate ${run_date} --detail "$(echo ${files[@]})"
    ((firstCheck++))
done

data_missing=1
firstCheck=1
while [ ${data_missing} -ne 0 ]
do
    str_table_missing="N/A"
    data_missing=0
    
    check_hive_data gpu.gpu_mck_ucg_list description REFRESH_${file_last_2month} 1
    data_missing=$((${res}+${data_missing}))
    
    if [ ${res} -gt 0 ]
    then 
    	str_table_missing="gpu.gpu_mck_ucg_list"
    fi
    
    check_hive_data gpu.gpu_ara_cms_blacklist 1 1 1
    data_missing=$((${res}+${data_missing}))
    
    if [ ${res} -gt 0 ]
    then
    	if [ ${str_table_missing} = "N/A" ]
        then	
            str_table_missing="gpu.gpu_ara_cms_blacklist"
        else
            str_table_missing="${str_table_missing},gpu.gpu_ara_cms_blacklist"
        fi
    fi
    
    check_hive_data gpu.cb_pre_post_uc2 datex ${file_last_month} 1
    data_missing=$((${res}+${data_missing}))
    
    if [ ${res} -gt 0 ]
    then
        if [ ${str_table_missing} = "N/A" ]
        then	
            str_table_missing="gpu.cb_pre_post_uc2"
        else
            str_table_missing="${str_table_missing},gpu.cb_pre_post_uc2"
        fi
    fi

    # [ ${data_missing} -ne 0 ] && echo "Not all data present, sleeping..." && sleep 600
    ${script_dir}/lib_send_alert.sh --case 2 --res ${data_missing} --firstCheck ${firstCheck} --id run_uc03_cms_ucg_refresh --runDate ${run_date} --detail ${str_table_missing}
    ((firstCheck++))
done

touch ${log_dir}/uc03_cms_ucg_refresh.source_complete_${file_last_month}

id="uc03_cms_ucg_refresh"
echo "running pipeline ${id}"
cmd="${script_dir}/run-orch.sh -f conductor.properties -p ${id} --run-date ${eom1}"
echo ${cmd}
OUTPUT=$(eval ${cmd})
exit_status=$?
echo "${OUTPUT}"
if [ ${exit_status} -ne 0 ]
then
    #Error Alert
    ${script_dir}/lib_send_alert.sh --case 3 --output "${OUTPUT}"
    echo "Error occured, check orchestrator logs" && exit 1
fi
#Summary Alert
${script_dir}/lib_send_alert.sh --case 4 --output "${OUTPUT}"
touch ${log_dir}/uc03_cms_ucg_${file_last_month}

