#!/bin/bash

source ~/.bashrc
source ~/.bash_profile

set -e

cd "$(dirname "$0")/.."

./bin/run_uc02_niknokk_ingestion_daily.sh 2019-02-01
./bin/run_uc02_niknokk_ingestion_daily.sh 2019-02-02
./bin/run_uc02_niknokk_ingestion_daily.sh 2019-02-03
./bin/run_uc02_niknokk_ingestion_daily.sh 2019-02-04
./bin/run_uc02_niknokk_ingestion_daily.sh 2019-02-05
./bin/run_uc02_niknokk_ingestion_daily.sh 2019-02-06
./bin/run_uc02_niknokk_ingestion_daily.sh 2019-02-07
./bin/run_uc02_niknokk_ingestion_daily.sh 2019-02-08
./bin/run_uc02_niknokk_ingestion_daily.sh 2019-02-09
./bin/run_uc02_niknokk_ingestion_daily.sh 2019-02-10
./bin/run_uc02_niknokk_ingestion_daily.sh 2019-02-11
./bin/run_uc02_niknokk_ingestion_daily.sh 2019-02-12
./bin/run_uc02_niknokk_ingestion_daily.sh 2019-02-13
./bin/run_uc02_niknokk_ingestion_daily.sh 2019-02-14
./bin/run_uc02_niknokk_ingestion_daily.sh 2019-02-15
./bin/run_uc02_niknokk_ingestion_daily.sh 2019-02-16
./bin/run_uc02_niknokk_ingestion_daily.sh 2019-02-17
./bin/run_uc02_niknokk_ingestion_daily.sh 2019-02-18
./bin/run_uc02_niknokk_ingestion_daily.sh 2019-02-19
./bin/run_uc02_niknokk_ingestion_daily.sh 2019-02-20


