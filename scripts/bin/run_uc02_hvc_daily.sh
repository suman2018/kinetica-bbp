#!/bin/bash

source ~/.bashrc
source ~/.bash_profile

cd "$(dirname "$0")/.."

if [ $# -eq 1 ]
  then
     run_date=${1}
fi 

if [ $# -eq 0 ] 
then 
  [ -z ${curr_date} ] && curr_date=`date +%Y-%m-%d` 
  cmd="date -d'${curr_date} - 3 day' +%Y%m%d"
  run_date=`eval $cmd`
fi

cmd="date -d'${run_date}' +%Y%m%d"
file_date=`eval $cmd`
cmd="date -d'${run_date}' +%Y-%m-%d"        
run_date_ac=`eval $cmd`
cmd="date -d'${run_date} - 1 day' +%Y%m%d"
last_date=`eval $cmd`

file_path="/data01/hnat_gpu/landing_UC02/HVC/${file_date}"

files=( \
${file_path}/HVC_DASHBOARD_CB_${file_date}.flg \
${file_path}/HVC_RGB_DLY_${file_date}.flg \
${file_path}/HVC_DASHBOARD_RECH_${file_date}.flg \
${file_path}/HVC_PURCHASE_CHANNEL_${file_date}.flg \
${file_path}/HVC_TRAFFIC_${file_date}.flg \
${file_path}/HVC_CASE_MNGMNT_${file_date}.flg \
logs/uc01_reference_${file_date} \
logs/uc02_hvc_master_${last_date} \
)
 
pipelines=( \
"uc02_hvc_master=(logs/uc02_hvc_master_${last_date})" \
)

check_files () {
    ##files=$@
    res=0
    for file in ${files[@]}; do
        echo "checking ${file}"
        if ! [ -f "${file}" ]
        then
            echo "file ${file} not found"
            res=1
        fi
    done
    return ${res}
}


for elt in "${pipelines[@]}"
do
    eval $elt
    IFS='=' read -r id string <<< "$elt"
    cmd="check_files \${$id[@]}"
    #echo $cmd
    res=1
    while [ ${res} -ne 0 ]
        do
        eval $cmd
        res_file_check=${?}
        if [ ${res} -eq 0 ]
        then
            echo "running pipeline ${id}"
            cmd="./bin/run-orch.sh -f conductor.properties -p ${id} --run-date ${run_date_ac}"
            echo ${cmd}
            eval ${cmd}
            if [ $? -ne 0 ]
            then
                echo "Error occured, check orchestrator logs" && exit 1
            fi
            touch logs/${id}_${file_date}
        fi
        [ ${res} -ne 0 ] && echo "Not all files from ${id} present, sleeping..." && sleep 300
    done

done
