#!/bin/sh

source ~/.bashrc
source ~/.bash_profile

set -e

cd "$(dirname "$0")/.."

run_date=$1



#./bin/run-orch.sh -f conductor.properties -p uc03_cms_core_init              --run-date ${run_date}

./bin/run-orch.sh -f conductor.properties --resume uc03_cms_core.cms_trackingstream_input_ingest             --run-date ${run_date}
./bin/run-orch.sh -f conductor.properties -p uc03_cms_usage_chg                                              --run-date ${run_date}
./bin/run-orch.sh -f conductor.properties -p uc03_cms_4g                                                     --run-date ${run_date}
#./bin/run-orch.sh -f conductor.properties -p uc03_cms_digital                                                --run-date ${run_date}
./bin/run-orch.sh -f conductor.properties -p uc03_cms_googleplay                                             --run-date ${run_date}
./bin/run-orch.sh -f conductor.properties -p uc03_cms_mytelkomsel_rev                                        --run-date ${run_date}
./bin/run-orch.sh -f conductor.properties -p uc03_cms_mytelkomsel_log                                        --run-date ${run_date}
./bin/run-orch.sh -f conductor.properties -p uc03_cms_poin                                                   --run-date ${run_date}
./bin/run-orch.sh -f conductor.properties -p uc03_cms_pre2post                                               --run-date ${run_date}
./bin/run-orch.sh -f conductor.properties -p uc03_cms_recharge                                               --run-date ${run_date}
./bin/run-orch.sh -f conductor.properties -p uc03_cms_sales                                                  --run-date ${run_date}
./bin/run-orch.sh -f conductor.properties -p uc03_cms_smartphone                                             --run-date ${run_date}
./bin/run-orch.sh -f conductor.properties -p uc03_cms_upoint                                                 --run-date ${run_date}
./bin/run-orch.sh -f conductor.properties -p uc03_cms_others                                                 --run-date ${run_date}
./bin/run-orch.sh -f conductor.properties --resume uc03_cms_core_finalize.cms_campaign_performance_dd_move   --run-date ${run_date}
./bin/run-orch.sh -f conductor.properties --resume uc03_cms_core_finalize.cms_campaign_performance_dd_insert --run-date ${run_date}
#./bin/run-orch.sh -f conductor.properties -p uc03_cms_core_finalize                                          --run-date ${run_date}
