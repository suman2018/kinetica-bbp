CREATE OR REPLACE TABLE "utils"."utils_profiler_results"
(
    "tbl" VARCHAR(256, shard_key) NOT NULL,
    "col" VARCHAR(256, shard_key) NOT NULL,
    "dtype" VARCHAR(32) NOT NULL,
    "minval" VARCHAR(256),
    "maxval" VARCHAR(256),
    "minlength" INTEGER,
    "maxlength" INTEGER,
    "cnt" BIGINT NOT NULL,
    "cnt_distinct" BIGINT NOT NULL,
    "cnt_nulls" BIGINT,
    "run_ts" TIMESTAMP
);

create or replace view utils.utils_dict_encoding 
refresh on change as
select char128('alter table '||char64(tbl)||' modify '||char32(col))||char64(' '||SPLIT(char32(dtype), ')', 1)||',dict);'),t.* from utils.utils_profiler_results t
where 1=1
and (dtype like 'varchar%')
and dtype not like '%dict%'
and dtype not like '%shard%'
union all
select char128('alter table '||char64(tbl)||' modify '||char32(col))||char64(' '||char32(dtype)||'(dict);'),t.* from utils.utils_profiler_results t
where 1=1
and (dtype in ('integer','bigint'))
and dtype not like '%dict%'
and dtype not like '%shard%'
order by tbl,cnt_distinct 
;