#!/bin/bash
BINPATH=/opt/gpudb/bin
HOSTINFO="-h 127.0.0.1 -user admin -pwd Kinetica1!"
DDLDIR="./"
usage()
{
    echo "usage: profiler.sh [[-t table ] | [-c collection] | [-h]]"
	echo "The script will profile all columns in the desired collection or table populate utils.utils_profiler_results table with the results"
	echo "Run with no arguments to profile all tables in the system"
}

while [ "$1" != "" ]; do
    case $1 in
        -t | --table )          shift
                                TBL="-table ${1}"
                                ;;
        -c | --collection )     shift
                                COL="-schema ${1}"
                                ;;
        -h | --help )           usage
                                exit
                                ;;
        * )                     usage
                                exit 1
    esac
    shift
done

echo "Running kiddl"
${BINPATH}/kiddl ${HOSTINFO} ${TBL} ${COL} > ${DDLDIR}/ddl.out
[ ${?} -eq 0 ] || exit 8
ISINDDL=0
TABLE=""
COLUMN=""
echo > script.sql

while read LINE
do
    [[ ${LINE} =~ ') ;' ]] && ISINDDL=0
    DTYPE=`echo ${LINE} | sed 's/[^ ]*  *\(.*\)/\1/g'`
    if [ ${ISINDDL} -eq 1 ]
    then
        COLUMN=`echo ${LINE} | sed 's/,*\([^ ]*\).*/\1/'`
        if [[ "$DTYPE" =~ "string" || "$DTYPE" =~ "wkt" ]]; then
            echo "INSERT INTO utils.utils_profiler_results SELECT '${TABLE}' AS tbl, '${COLUMN}' as col, '${DTYPE}' AS dtype, \
null AS minval, \
null AS maxval, \
null AS minlength, \
null AS maxlength, \
0 as cnt, \
0 as cnt_distinct, \
null AS cnt_nulls, \
CURRENT_TIMESTAMP AS run_ts FROM ${TABLE} LIMIT 1;"  >> script.sql
        elif [[ "$DTYPE" =~ "timestamp" || "$DTYPE" =~ "date" ]]; then
            echo "INSERT INTO utils.utils_profiler_results SELECT '${TABLE}' AS tbl, '${COLUMN}' as col, '${DTYPE}' AS dtype, \
cast(MIN(${COLUMN}) AS VARCHAR) AS minval, \
cast(MAX(${COLUMN}) AS VARCHAR) AS maxval, \
NULL AS minlength, \
NULL AS maxlength, \
COUNT(*) as cnt, \
COUNT(DISTINCT ${COLUMN}) as cnt_distinct, \
SUM(CASE WHEN ${COLUMN} IS NULL THEN 1 ELSE 0 END) AS cnt_nulls, \
CURRENT_TIMESTAMP AS run_ts FROM ${TABLE} LIMIT 1;"  >> script.sql        
        else
            echo "INSERT INTO utils.utils_profiler_results SELECT '${TABLE}' AS tbl, '${COLUMN}' as col, '${DTYPE}' AS dtype, \
cast(MIN(${COLUMN}) AS VARCHAR) AS minval, \
cast(MAX(${COLUMN}) AS VARCHAR) AS maxval, \
MIN(LENGTH(${COLUMN})) AS minlength, \
MAX(LENGTH(${COLUMN})) AS maxlength, \
COUNT(*) as cnt, \
COUNT(DISTINCT ${COLUMN}) as cnt_distinct, \
SUM(CASE WHEN ${COLUMN} IS NULL THEN 1 ELSE 0 END) AS cnt_nulls, \
CURRENT_TIMESTAMP AS run_ts FROM ${TABLE} LIMIT 1;"  >> script.sql        

        fi
    fi
    
    [[ ${LINE} =~ 'create or replace' ]] && ISINDDL=1 && TABLE=`echo ${LINE} | sed 's/.*[ ]\([^(]*\)[(]/\1/g'`
    
    
    
done < ${DDLDIR}/ddl.out


${BINPATH}/kisql ${HOSTINFO} -f script.sql -echoSql true
