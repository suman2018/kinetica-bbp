#!/bin/bash

source ~/.bashrc
source ~/.bash_profile

set -e

cd "$(dirname "$0")/.."


./bin/run-orch.sh -f conductor.properties -p uc02_hvc_rolling.multidim-load --run-date 2018-10-31
./bin/run-orch.sh -f conductor.properties -p uc02_hvc_rolling.multidim-stg --run-date 2018-10-31
./bin/run-orch.sh -f conductor.properties -p uc02_hvc_rolling.multidim-load --run-date 2018-11-30
./bin/run-orch.sh -f conductor.properties -p uc02_hvc_rolling.multidim-stg --run-date 2018-11-30
./bin/run-orch.sh -f conductor.properties -p uc02_hvc_rolling.multidim-load --run-date 2018-12-31
./bin/run-orch.sh -f conductor.properties -p uc02_hvc_rolling.multidim-stg --run-date 2018-12-31
./bin/run-orch.sh -f conductor.properties -p uc02_hvc_rolling.multidim-load --run-date 2019-01-31
./bin/run-orch.sh -f conductor.properties -p uc02_hvc_rolling.multidim-stg --run-date 2019-01-31

./bin/run-orch.sh -f conductor.properties -p uc02_hvc_rolling.multidim-load --run-date 2019-02-01
./bin/run-orch.sh -f conductor.properties -p uc02_hvc_rolling.multidim-stg --run-date 2019-02-01
./bin/run-orch.sh -f conductor.properties -p uc02_hvc_rolling.multidim-load --run-date 2019-02-02
./bin/run-orch.sh -f conductor.properties -p uc02_hvc_rolling.multidim-stg --run-date 2019-02-02
./bin/run-orch.sh -f conductor.properties -p uc02_hvc_rolling.multidim-load --run-date 2019-02-03
./bin/run-orch.sh -f conductor.properties -p uc02_hvc_rolling.multidim-stg --run-date 2019-02-03
./bin/run-orch.sh -f conductor.properties -p uc02_hvc_rolling.multidim-load --run-date 2019-02-04
./bin/run-orch.sh -f conductor.properties -p uc02_hvc_rolling.multidim-stg --run-date 2019-02-04
./bin/run-orch.sh -f conductor.properties -p uc02_hvc_rolling.multidim-load --run-date 2019-02-05
./bin/run-orch.sh -f conductor.properties -p uc02_hvc_rolling.multidim-stg --run-date 2019-02-05
./bin/run-orch.sh -f conductor.properties -p uc02_hvc_rolling.multidim-load --run-date 2019-02-06
./bin/run-orch.sh -f conductor.properties -p uc02_hvc_rolling.multidim-stg --run-date 2019-02-06
./bin/run-orch.sh -f conductor.properties -p uc02_hvc_rolling.multidim-load --run-date 2019-02-07
./bin/run-orch.sh -f conductor.properties -p uc02_hvc_rolling.multidim-stg --run-date 2019-02-07
./bin/run-orch.sh -f conductor.properties -p uc02_hvc_rolling.multidim-load --run-date 2019-02-08
./bin/run-orch.sh -f conductor.properties -p uc02_hvc_rolling.multidim-stg --run-date 2019-02-08
./bin/run-orch.sh -f conductor.properties -p uc02_hvc_rolling.multidim-load --run-date 2019-02-09
./bin/run-orch.sh -f conductor.properties -p uc02_hvc_rolling.multidim-stg --run-date 2019-02-09
./bin/run-orch.sh -f conductor.properties -p uc02_hvc_rolling.multidim-load --run-date 2019-02-10
./bin/run-orch.sh -f conductor.properties -p uc02_hvc_rolling.multidim-stg --run-date 2019-02-10
./bin/run-orch.sh -f conductor.properties -p uc02_hvc_rolling.multidim-load --run-date 2019-02-11
./bin/run-orch.sh -f conductor.properties -p uc02_hvc_rolling.multidim-stg --run-date 2019-02-11
./bin/run-orch.sh -f conductor.properties -p uc02_hvc_rolling.multidim-load --run-date 2019-02-12
./bin/run-orch.sh -f conductor.properties -p uc02_hvc_rolling.multidim-stg --run-date 2019-02-12
./bin/run-orch.sh -f conductor.properties -p uc02_hvc_rolling.multidim-load --run-date 2019-02-13
./bin/run-orch.sh -f conductor.properties -p uc02_hvc_rolling.multidim-stg --run-date 2019-02-13
./bin/run-orch.sh -f conductor.properties -p uc02_hvc_rolling.multidim-load --run-date 2019-02-14
./bin/run-orch.sh -f conductor.properties -p uc02_hvc_rolling.multidim-stg --run-date 2019-02-14
./bin/run-orch.sh -f conductor.properties -p uc02_hvc_rolling.multidim-load --run-date 2019-02-15
./bin/run-orch.sh -f conductor.properties -p uc02_hvc_rolling.multidim-stg --run-date 2019-02-15
./bin/run-orch.sh -f conductor.properties -p uc02_hvc_rolling.multidim-load --run-date 2019-02-16
./bin/run-orch.sh -f conductor.properties -p uc02_hvc_rolling.multidim-stg --run-date 2019-02-16
./bin/run-orch.sh -f conductor.properties -p uc02_hvc_rolling.multidim-load --run-date 2019-02-17
./bin/run-orch.sh -f conductor.properties -p uc02_hvc_rolling.multidim-stg --run-date 2019-02-17
./bin/run-orch.sh -f conductor.properties -p uc02_hvc_rolling.multidim-load --run-date 2019-02-18
./bin/run-orch.sh -f conductor.properties -p uc02_hvc_rolling.multidim-stg --run-date 2019-02-18
./bin/run-orch.sh -f conductor.properties -p uc02_hvc_rolling.multidim-load --run-date 2019-02-19
./bin/run-orch.sh -f conductor.properties -p uc02_hvc_rolling.multidim-stg --run-date 2019-02-19
./bin/run-orch.sh -f conductor.properties -p uc02_hvc_rolling.multidim-load --run-date 2019-02-20
./bin/run-orch.sh -f conductor.properties -p uc02_hvc_rolling.multidim-stg --run-date 2019-02-20
./bin/run-orch.sh -f conductor.properties -p uc02_hvc_rolling.multidim-load --run-date 2019-02-21
./bin/run-orch.sh -f conductor.properties -p uc02_hvc_rolling.multidim-stg --run-date 2019-02-21
./bin/run-orch.sh -f conductor.properties -p uc02_hvc_rolling.multidim-load --run-date 2019-02-22
./bin/run-orch.sh -f conductor.properties -p uc02_hvc_rolling.multidim-stg --run-date 2019-02-22
./bin/run-orch.sh -f conductor.properties -p uc02_hvc_rolling.multidim-load --run-date 2019-02-23
./bin/run-orch.sh -f conductor.properties -p uc02_hvc_rolling.multidim-stg --run-date 2019-02-23
./bin/run-orch.sh -f conductor.properties -p uc02_hvc_rolling.multidim-load --run-date 2019-02-24
./bin/run-orch.sh -f conductor.properties -p uc02_hvc_rolling.multidim-stg --run-date 2019-02-24