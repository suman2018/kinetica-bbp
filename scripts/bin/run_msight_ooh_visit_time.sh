#!/bin/bash

source ~/.bashrc
source ~/.bash_profile

cd "$(dirname "$0")/.."

eom_last_month () {
    cmd="date -d'${1}' +%Y%m01"
    res=`eval $cmd`
    cmd="date -d'${res} - 1 day' +%Y-%m-%d"
    res=`eval $cmd`
    echo ${res}
}

if [ $# -eq 1 ]
  then
     run_date=${1}
     cmd="date -d'${run_date}' +%Y-%m-01"
     run_date=`eval $cmd`
     eom1=${run_date}
fi 

if [ $# -eq 0 ] 
then 
  [ -z ${curr_date} ] && curr_date=`date +%Y-%m-%d` 
  cmd="date -d'${curr_date}' +%Y-%m-%d"
  run_date=`eval $cmd`
  eom1=$(eom_last_month ${run_date})
fi

echo run date... ${run_date}
echo ${eom1}

eom2=$(eom_last_month ${eom1})

cmd="date -d'${eom1}' +%Y%m%d"
file_eom1=`eval $cmd`

cmd="date -d'${run_date}' +%Y%m"
file_current_month=`eval $cmd`
cmd="date -d'${eom1}' +%Y%m"
file_last_month=`eval $cmd`
cmd="date -d'${eom2}' +%Y%m"
file_last_2months=`eval $cmd`

file_path=""
files=(\
logs/msight_ooh_visit_time_${file_last_2months} \
)

check_files () {
    fls=$@
    res=0
    for file in ${fls[@]}; do
        echo "checking file ${file}"
        if [ ! -f "${file}" ]
        then
            echo "file ${file} not found"
            res=$((res+1))
        fi
    done
    return 0
}

check_hive_data () {
    tbl=${1}
    datecol=${2}
    dateval=${3}
    res=0
    ##hql="select count(*) as table_row_count from ${tbl} where ${datecol} = '${dateval}'"
    hql="select 1 as table_row_count from ${tbl} where ${datecol} = '${dateval}' limit 1 "
    #hql="select 1 as table_row_count"
    cmd="beeline -u \"jdbc:hive2://virapapp1.telkomsel.co.id:10000/default;principal=hive/virapapp1@TELKOMSEL.CO.ID?mapred.job.queue.name=root.hue_gpu\" -e \"${hql}\""
    echo "Running hql: ${hql}"
    eval ${cmd} > "logs/msight_${tbl}_${dateval}" 
    
    cmd="grep -2 table_row_count logs/msight_${tbl}_${dateval} | tail -1 | cut -d \" \" -f2"
    cnt=`eval $cmd`
    echo "Rows found: ${cnt}"
    [ "${cnt}" != "1" ] && res=1
    ##[ ${cnt} -eq 0 ] && res=1
    return 0
}

data_missing=1
while [ ${data_missing} -ne 0 ]
do 
    data_missing=0

    check_files ${files[@]}
    data_missing=$((${res}+${data_missing}))
    #echo $data_missing
    [ ${data_missing} -ne 0 ] && echo "There is dependency process is not finished, please double check..." && exit 1 
done

data_missing=1
while [ ${data_missing} -ne 0 ]
do
    data_missing=0
    check_hive_data skp.ooh_summ_visit_time_${file_last_month} 1 1 
    data_missing=$((${res}+${data_missing}))
    
    [ ${data_missing} -ne 0 ] && echo "Error: Data source is not ready, please rerun after the source is ready." && exit 1
done
   
touch logs/msight_ooh_visit_time.source_complete_${file_last_month}

  id="msight_ooh_visit_time"
  echo "running pipeline ${id}"
  cmd="./bin/run-orch.sh -f conductor.properties -p ${id} --run-date ${eom1}"
  echo ${cmd}
  eval ${cmd}
        if [ $? -ne 0 ]
        then
            echo "Error occured, check orchestrator logs" && exit 1
        fi  
        
        touch logs/${id}_${file_last_month}

