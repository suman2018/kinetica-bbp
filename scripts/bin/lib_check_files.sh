#!/bin/bash
function check_files(){
  files=$@
  res=0
  for file in ${files[@]}; do
  #echo "checking ${file}"
    if ! [ -f "${file}" ]
      then
        echo "file ${file} not found"
        res=1
    fi
  done
  return ${res}
}
