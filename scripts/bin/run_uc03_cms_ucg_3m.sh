#!/bin/bash

source ~/.bashrc
source ~/.bash_profile

cd "$(dirname "$0")/.."

if [ $# -eq 1 ]
  then
     run_date=${1}
     cmd="date -d'${run_date}' +%Y-%m-%d"
     run_date=`eval $cmd`
fi 

if [ $# -eq 0 ] 
then 
  [ -z ${curr_date} ] && curr_date=`date +%Y-%m-%d` 
  cmd="date -d'${curr_date}' +%Y-%m-%d"
  run_date=`eval $cmd`
fi

echo run date... ${run_date}

cmd="date -d'${run_date}' +%Y%m%d"
file_date=`eval $cmd`
cmd="date -d'${run_date} - 1 day' +%Y%m%d"
last_date=`eval $cmd`

eom_last_month () {
    cmd="date -d'${1}' +%Y%m01"
    res=`eval $cmd`
    cmd="date -d'${res} - 1 day' +%Y-%m-%d"
    res=`eval $cmd`
    echo ${res}
}

eom1=$(eom_last_month ${run_date})

echo ${eom1}

check_files () {
    fls=$@
    res=0
    for file in ${fls[@]}; do
        echo "checking file ${file}"
        if [ ! -f "${file}" ]
        then
            echo "file ${file} not found"
            res=$((res+1))
        fi
    done
    return 0
}

check_hive_data () {
    tbl=${1}
    datecol=${2}
    dateval=${3}
    res=0
    ##hql="select 1 as table_row_count from ${tbl} where ${datecol} = '${dateval}' limit 1 "

    datecol="last_day(from_unixtime(unix_timestamp(substring(description,9,6), 'yyyyMM')))" 
    hql="select 1 as table_row_count from ${tbl} where ${datecol} < '${dateval}' limit 1 "

    cmd="beeline -u \"jdbc:hive2://virapapp1.telkomsel.co.id:10000/default;principal=hive/virapapp1@TELKOMSEL.CO.ID?mapred.job.queue.name=root.hue_gpu\" -e \"${hql}\""
    echo "Running hql: ${hql}"
    eval ${cmd} > "logs/uc03_cms_ucg_3m_${tbl}_${dateval}" 
    
    cmd="grep -2 table_row_count logs/uc03_cms_ucg_3m_${tbl}_${dateval} | tail -1 | cut -d \" \" -f2"
    cnt=`eval $cmd`
    echo "Rows found: ${cnt}"
    [ "${cnt}" != "1" ] && res=1
    ##[ ${cnt} -eq 0 ] && res=1
    return 0
}

eom1=$(eom_last_month ${run_date})
eom2=$(eom_last_month ${eom1})

cmd="date -d'${eom1}' +%Y%m%d"
file_eom1=`eval $cmd`

cmd="date -d'${run_date}' +%Y%m"
file_current_month=`eval $cmd`
cmd="date -d'${eom1}' +%Y%m"
file_last_month=`eval $cmd`
cmd="date -d'${eom2}' +%Y%m"
file_last_2months=`eval $cmd`

file_path=""
files=(\
logs/uc03_cms_ucg_3m_${file_last_2months} \
)

data_missing=1
while [ ${data_missing} -ne 0 ]
do 
    data_missing=0

    check_files ${files[@]}
    data_missing=$((${res}+${data_missing}))
    #echo $data_missing
    [ ${data_missing} -ne 0 ] && echo "There is dependency process is not finished..,sleeping" && sleep 600
done

data_missing=1
while [ ${data_missing} -ne 0 ]
do
    data_missing=0
    check_hive_data gpu.gpu_mck_ucg_list 1 ${eom1}
    data_missing=$((${res}+${data_missing}))
        
    [ ${data_missing} -ne 0 ] && echo "Not all data present, sleeping..." && sleep 600
done
    touch logs/uc03_cms_ucg_3m.source_complete_${file_last_month}

	id="uc03_cms_ucg_3m"
        echo "running pipeline ${id}"
        cmd="./bin/run-orch.sh -f conductor.properties -p ${id} --run-date ${eom1}"
        echo ${cmd}
        eval ${cmd}
        if [ $? -ne 0 ]
        then
            echo "Error occured, check orchestrator logs" && exit 1
        fi
        touch logs/${id}_${file_last_month}

