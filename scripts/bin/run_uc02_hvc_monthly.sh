#!/bin/bash

source ~/.bashrc
source ~/.bash_profile

script_dir="$(cd "$(dirname $0)" && pwd -P)"

run_date=${1}
[ -z ${run_date} ] && run_date=`date +%Y-%m-%d`
cmd="date -d'${run_date}' +%Y%m%d"
file_date=`eval $cmd`
cmd="date -d'${run_date} - 1 day' +%Y%m%d"
last_date=`eval $cmd`

eom_last_month () {
    cmd="date -d'${1}' +%Y%m01"
    res=`eval $cmd`
    cmd="date -d'${res} - 1 day' +%Y-%m-%d"
    res=`eval $cmd`
    echo ${res}
}

eom1=$(eom_last_month ${run_date})

mth_run_flag="${log_dir}/uc02_hvc_mth_run.ctl"
rm -f ${mth_run_flag}

sh ${script_dir}/run_uc02_hvc_mth_bcp_persona.sh ${eom1} &
sleep 5m 
sh ${script_dir}/run_uc02_hvc_mth_baseline.sh ${eom1} &
sleep 5m
sh ${script_dir}/run_uc02_hvc_mth_postpaid_cb_rev.sh $eom1 &
sleep 5m
sh ${script_dir}/run_uc02_hvc_mth_itdaml_demography.sh $eom1 &
sleep 5m
sh ${script_dir}/run_uc02_hvc_mth_work_staypoint.sh $eom1 &
sleep 5m
sh ${script_dir}/run_uc02_hvc_mth_home_staypoint.sh $eom1 &

## data source not available since March 2019
##sleep 5m
##sh bin/run_uc02_hvc_mth_community_cb_dou.sh $eom1 &




