#!/bin/bash

source ~/.bashrc
source ~/.bash_profile

script_dir="$(cd "$(dirname $0)" && pwd -P)"
log_dir=${script_dir}/../logs

. ${script_dir}/lib_check_files.sh

if [ $# -eq 1 ]
  then
     run_date=${1}
fi 

if [ $# -eq 0 ] 
then 
  [ -z ${curr_date} ] && curr_date=`date +%Y-%m-%d` 
  cmd="date -d'${curr_date}' +%Y%m%d"
  run_date=`eval $cmd`
fi

cmd="date -d'${run_date}' +%Y%m%d"
file_date=`eval $cmd`
cmd="date -d'${run_date}' +%Y-%m-%d"        
run_date_ac=`eval $cmd`
cmd="date -d'${run_date} - 1 day' +%Y%m%d"
last_date=`eval $cmd`


file_path="/data01/hnat_gpu/landing_UC02/HVC/adhoc_ref"

pipelines=( \
"ref_channel_ref=(${file_path}/channel_ref*.flg )" \
"hvc_cb_baseline=(${file_path}/HVC_CB_BASELINE*.flg )" \
)

for elt in "${pipelines[@]}"
do
    eval $elt
    IFS='=' read -r id string <<< "$elt"
    cmd="check_files \${$id[@]}"
    res=1
    eval $cmd
        if [ ${res} -eq 0 ]
        then
            echo "running pipeline ${id}"
            cmd="${script_dir}/run-orch.sh -f conductor.properties -p uc02_hvc_master_yearly.${id} --run-date ${run_date_ac}"
            echo ${cmd}
            OUTPUT=$(eval ${cmd})
            exit_status=$?
            echo "${OUTPUT}"        
            if [ ${exit_status} -ne 0 ]
            then
                #Error Alert
                ${script_dir}/lib_send_alert.sh --case 3 --output "${OUTPUT}"
                echo "Error occured, check orchestrator logs" && exit 1
            else
            #Summary Alert
            ${script_dir}/lib_send_alert.sh --case 4 --output "${OUTPUT}"
            touch ${log_dir}/${id}_${file_date}
            fi

            ## Jira-633 
            echo "running pipeline ${id}_sql"
            cmd="${script_dir}/run-orch.sh -f conductor.properties -p uc02_hvc_master_yearly.${id}_sql --run-date ${run_date_ac}"
            echo ${cmd}
            OUTPUT=$(eval ${cmd})
            exit_status=$?
            echo "${OUTPUT}"        
            if [ ${exit_status} -ne 0 ]
            then
                #Error Alert
                ${script_dir}/lib_send_alert.sh --case 3 --output "${OUTPUT}"
                echo "Error occured, check orchestrator logs" && exit 1
            else
            ${script_dir}/lib_send_alert.sh --case 4 --output "${OUTPUT}"
            touch ${log_dir}/${id}_sql_${file_date}
            fi
            ### Jira-633

            if [ $id == "ref_channel_ref" ]   
            then 
                cmd="mv ${file_path}/channel_ref*.* ${file_path}/backup/"
            	eval ${cmd}
            fi       
            if [ $id == "hvc_cb_baseline" ]
            then
                cmd="mv ${file_path}/HVC_CB_BASELINE*.* ${file_path}/backup/"
            	eval ${cmd}
            fi 
        else
        	#Summary Alert
            ${script_dir}/lib_send_alert.sh --case 4 --output "${OUTPUT}"
        fi

        #[ ${res} -ne 0 ] && echo "Not all files from ${id} present"

        #Late Case Alert, No Need for Late Case Alert bc the File Source is not mandatory
        ###${script_dir}/lib_send_alert.sh --case 1 --res ${res} --firstCheck ${firstCheck} --id ${id} --runDate ${run_date} --detail "$(echo ${pipelines[@]})"
        ###((firstCheck++))
    
done
