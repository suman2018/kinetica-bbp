#!/bin/bash

source ~/.bashrc
source ~/.bash_profile

set -e

cd "$(dirname "$0")/.."

run_date=${1}
[ -z ${run_date} ] && run_date=`date +%Y-%m-%d`
cmd="date -d'${run_date}' +%Y%m%d"
file_date=`eval $cmd`
cmd="date -d'${run_date} - 1 day' +%Y%m%d"
last_date=`eval $cmd`

eom_last_month () {
    cmd="date -d'${1}' +%Y%m01"
    res=`eval $cmd`
    cmd="date -d'${res} - 1 day' +%Y-%m-%d"
    res=`eval $cmd`
    echo ${res}
}

check_files () {
    fls=$@
    res=0
    for file in ${fls[@]}; do
        echo "checking file ${file}"
        if [ ! -f "${file}" ]
        then
            echo "file ${file} not found"
            res=$((res+1))
        fi
    done
    return 0
}

eom1=$(eom_last_month ${run_date})
eom2=$(eom_last_month ${eom1})
eom3=$(eom_last_month ${eom2})
eom4=$(eom_last_month ${eom3})

cmd="date -d'${eom1}' +%Y%m%d"
file_eom1=`eval $cmd`
cmd="date -d'${eom2}' +%Y%m%d"
file_eom2=`eval $cmd`
cmd="date -d'${eom3}' +%Y%m%d"
file_eom3=`eval $cmd`
cmd="date -d'${eom4}' +%Y%m%d"
file_eom4=`eval $cmd`


file_path=""
files=(\
${file_path}/data01/hnat_gpu/landing_UC02/NIKNOK/${file_eom1}/sandbox_cb_prepaid_postpaid_$((${file_eom1}/100)).txt \
${file_path}/data01/hnat_gpu/landing_UC02/NIKNOK/${file_eom2}/sandbox_cb_prepaid_postpaid_$((${file_eom2}/100)).txt \
${file_path}/data01/hnat_gpu/landing_UC02/NIKNOK/${file_eom3}/sandbox_cb_prepaid_postpaid_$((${file_eom3}/100)).txt \
${file_path}/data01/hnat_gpu/landing_UC02/NIKNOK/${file_eom4}/sandbox_cb_prepaid_postpaid_$((${file_eom4}/100)).txt \
)

data_missing=1
while [ ${data_missing} -ne 0 ]
do
    
    data_missing=0
        
    check_files ${files[@]}
    data_missing=$((${res}+${data_missing}))
    #echo $data_missing
    [ ${data_missing} -ne 0 ] && echo "Not all data present, sleeping..." && sleep 60
done

./bin/run-orch.sh -f conductor.properties -p uc02_niknokk_01_initialization
./bin/run-orch.sh -f conductor.properties -p uc02_niknokk_02_src_m_cb_prepaid_postpaid --run-date ${eom4}
./bin/run-orch.sh -f conductor.properties -p uc02_niknokk_02_src_m_cb_prepaid_postpaid --run-date ${eom3}
./bin/run-orch.sh -f conductor.properties -p uc02_niknokk_02_src_m_cb_prepaid_postpaid --run-date ${eom2}
./bin/run-orch.sh -f conductor.properties -p uc02_niknokk_02_src_m_cb_prepaid_postpaid --run-date ${eom1}

exit

cmd="date -d'${run_date}' +%Y%m%d"
file_date=`eval $cmd`
cmd="date -d'${run_date} - 1 month' +%Y%m"
eom1=`eval $cmd`"01"


