#!/bin/bash

script_dir="$(cd "$(dirname $0)" && pwd -P)"
log_dir=${script_dir}/../logs

function check_hive_data () {
    tbl=${1}
    datecol=${2}
    dateval=${3}
    criticalval=${4}
    res=0

    if [ ${tbl} == "gpu.digital_tracking_file" ]
    then
         datecol="substring(from_unixtime(Cast(created_at as int)),1,10)"
    fi

    hql="select 1 as table_row_count from ${tbl} where ${datecol} = '${dateval}' limit 1 "

    #hql="select 1 as table_row_count"
    cmd="beeline -u \"jdbc:hive2://virapapp1.telkomsel.co.id:10000/default;principal=hive/virapapp1@TELKOMSEL.CO.ID?mapred.job.queue.name=root.hue_gpu\" -e \"${hql}\""
    echo "Running hql: ${hql}"
    echo ${cmd}
    eval ${cmd} > "${log_dir}/uc03_cms_${tbl}_${dateval}"

    cmd="grep -2 table_row_count ${log_dir}/uc03_cms_${tbl}_${dateval} | tail -1 | cut -d \" \" -f2"
    cnt=`eval $cmd`
    echo "Rows found: ${cnt}"
    [ "${cnt}" != "1" ] && [ "${criticalval}" == "1" ] && res=1
    ##[ ${cnt} -eq 0 ] && res=1
    echo Result:${tbl}_${datecol}_${dateval}_${criticalval}_${res}
    return 0
}