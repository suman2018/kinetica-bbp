#!/bin/bash

source ~/.bashrc
source ~/.bash_profile

cd "$(dirname "$0")/.."

## path of logs file 
uc_backup_path="/data01/hnat_gpu/backup_log"

## housekeeping logs for x days before
days_before=33
logs_path="/home/hnat_gpu/orch_prod/logs"

     cmd="find /home/hnat_gpu/orch_prod/logs -maxdepth 1 -type f -iname '*.log' -mtime ${days_before} -printf \"%f\\n\""
     list_files=`eval $cmd`

     echo $cmd

     cmd="date -d'${curr_date} - ${days_before} day' +%Y%m%d"
     run_date=`eval $cmd`
     echo ${run_date}

     cmd="mkdir -p ${uc_backup_path}/${run_date}"
     echo $cmd
     eval `$cmd`
 
     for pfile in ${list_files[@]}
     do 
         cmd="mv -f  ${logs_path}/${pfile} ${uc_backup_path}/$run_date/"
         echo $cmd
         eval `$cmd`

      done

         cmd="touch $logs_path/gpu_housekeeping_log_$run_date"
         echo $cmd
         eval `$cmd`
