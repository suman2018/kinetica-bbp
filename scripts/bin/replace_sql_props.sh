#!/bin/sh

find ../pipelines/${1} -name *.sql -type f -print0 | while IFS= read -r -d $'\0' file_template; do
    file_out=${file_template}.jkr
    cp ${file_template} ${file_out}
    echo ${file_out}
    while read var eql val
    do
        if [[ "${var}" =~ "env" ]]
        then
        var=`echo "${var}" | sed -e 's/env\.//'`
        sed -i "s/\${${var}}/${val}/g" ${file_out}
        fi
    done < ../conductor-uc02-dev.properties
    sed -i "s/\${run-date=yyyy-MM-dd}/2018-12-06/g" ${file_out}
    sed -i "s/\${run-date=yyyyMMdd}/20181206/g" ${file_out}
    sed -i "s/\${job-id}/DUMMY123/g" ${file_out}
done

