#!/bin/bash

source ~/.bashrc
source ~/.bash_profile

#set -e

script_dir="$(cd "$(dirname $0)" && pwd -P)"
log_dir=${script_dir}/../logs

. ${script_dir}/lib_check_files.sh
. ${script_dir}/lib_check_HIVE.sh

if [ $# -eq 1 ]
  then
     run_date=${1}
fi 

if [ $# -eq 0 ] 
then 
  [ -z ${curr_date} ] && curr_date=`date +%Y-%m-%d` 
  cmd="date -d'${curr_date} - 3 day' +%Y-%m-%d"
  run_date=`eval $cmd`
fi

cmd="date -d'${run_date}' +%Y%m%d"
file_date=`eval $cmd`
cmd="date -d'${run_date} - 1 day' +%Y%m%d"
last_date=`eval $cmd`

eom_last_month () {
    cmd="date -d'${1}' +%Y%m01"
    res=`eval $cmd`
    cmd="date -d'${res} - 1 day' +%Y-%m-%d"
    res=`eval $cmd`
    echo ${res}
}

eom1=$(eom_last_month ${run_date})
eom2=$(eom_last_month ${eom1})
eom3=$(eom_last_month ${eom2})
eom4=$(eom_last_month ${eom3})

cmd="date -d'${eom1}' +%Y%m%d"
file_eom1=`eval $cmd`
cmd="date -d'${eom2}' +%Y%m%d"
file_eom2=`eval $cmd`
cmd="date -d'${eom3}' +%Y%m%d"
file_eom3=`eval $cmd`
cmd="date -d'${eom4}' +%Y%m%d"
file_eom4=`eval $cmd`

file_path=""
#files=(\
#${log_dir}/uc02_niknokk_ingestion_daily_${last_date} \
#${file_path}/data01/hnat_gpu/landing_UC02/NIKNOK/ref_dukcapil_nik_map.flg \
#)

files="${log_dir}/uc02_niknokk_ingestion_daily_${last_date} ${file_path}/data01/hnat_gpu/landing_UC02/NIKNOK/ref_dukcapil_nik_map.flg"

data_missing=1
firstCheck=1
while [ ${data_missing} -ne 0 ]
do 
    data_missing=0

    check_files ${files}
    data_missing=$((${res}+${data_missing}))
    #echo $data_missing
    # [ ${data_missing} -ne 0 ] && echo "There is dependency process is not finished..,sleeping" && sleep 600
    ${script_dir}/lib_send_alert.sh --case 1 --res ${data_missing} --firstCheck ${firstCheck} --id uc02_niknokk_ingestion_daily --runDate ${run_date} --detail "$(echo ${files[@]})"
    ((firstCheck++))
done

data_missing=1
firstCheck=1
while [ ${data_missing} -ne 0 ]
do
    data_missing=0
    str_table_missing="0"
    
    check_hive_data gpu.v_gpu_sales_pre_daily datex ${file_date} 1
    data_missing=$((${res}+${data_missing}))
    
    if [ ${res} -gt 0 ]
    then 
    	str_table_missing="gpu.v_gpu_sales_pre_daily"
    fi
    
    check_hive_data gpu.v_gpu_churn_pre_daily datex ${file_date} 1
    data_missing=$((${res}+${data_missing}))
    
    if [ ${res} -gt 0 ]
    then	
    	str_table_missing="${str_table_missing} gpu.v_gpu_churn_pre_daily"
    fi
    # [ ${data_missing} -ne 0 ] && echo "Not all data present, sleeping..." && sleep 600
    ${script_dir}/lib_send_alert.sh --case 2 --res ${data_missing} --firstCheck ${firstCheck} --id uc02_niknokk_ingestion_daily --runDate ${run_date} --detail ${str_table_missing}
    ((firstCheck++))
done

############################################################
cmd="${script_dir}/run-orch.sh -f conductor.properties -p uc02_niknokk_03_src_d_reina --run-date ${run_date}"
echo ${cmd}
OUTPUT=$(eval ${cmd})
exit_status=$?
echo "${OUTPUT}"
if [ ${exit_status} -ne 0 ]
then
    #Error Alert
    ${script_dir}/lib_send_alert.sh --case 3 --output "${OUTPUT}"
    echo "Error occured, check orchestrator logs" && exit 1
fi
#Summary Alert
${script_dir}/lib_send_alert.sh --case 4 --output "${OUTPUT}"

############################################################
cmd="${script_dir}/run-orch.sh -f conductor.properties -p uc02_niknokk_05_src_d_sales_churn_pre --run-date ${run_date}"
echo ${cmd}
OUTPUT=$(eval ${cmd})
exit_status=$?
echo "${OUTPUT}"
if [ ${exit_status} -ne 0 ]
then
    #Error Alert
    ${script_dir}/lib_send_alert.sh --case 3 --output "${OUTPUT}"
    echo "Error occured, check orchestrator logs" && exit 1
fi
#Summary Alert
${script_dir}/lib_send_alert.sh --case 4 --output "${OUTPUT}"

############################################################
cmd="${script_dir}/run-orch.sh -f conductor.properties -p uc02_niknokk_06_src_d_ref_dukcapil --run-date ${run_date}"
echo ${cmd}
OUTPUT=$(eval ${cmd})
exit_status=$?
echo "${OUTPUT}"
if [ ${exit_status} -ne 0 ]
then
    #Error Alert
    ${script_dir}/lib_send_alert.sh --case 3 --output "${OUTPUT}"
    echo "Error occured, check orchestrator logs" && exit 1
fi
#Summary Alert
${script_dir}/lib_send_alert.sh --case 4 --output "${OUTPUT}"

touch ${log_dir}/uc02_niknokk_ingestion_daily_${file_date}
