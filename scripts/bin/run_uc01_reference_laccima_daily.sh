#!/bin/bash

source ~/.bashrc
source ~/.bash_profile

script_dir="$(cd "$(dirname $0)" && pwd -P)"
log_dir=${script_dir}/../logs

. ${script_dir}/lib_check_files.sh

if [ $# -eq 1 ]
  then
     run_date=${1}
fi

if [ $# -eq 0 ]
then
  [ -z ${curr_date} ] && curr_date=`date +%Y-%m-%d`
  cmd="date -d'${curr_date} - 1 day' +%Y-%m-%d"
  run_date=`eval $cmd`
fi

echo "Rundate : ${run_date}"

cmd="date -d'${run_date}' +%Y%m%d"
file_date=`eval $cmd`
cmd="date -d'${run_date} - 1 day' +%Y%m%d"
last_date=`eval $cmd`

file_path="/data01/hnat_gpu/landing_uc1_dev/report_gpu/OUT/${file_date}"
pipelines=( \
"uc01_reference_laccima=(${log_dir}/uc01_reference_laccima_${last_date} ${file_path}/gpu_laccima_dim_tdch_${file_date}.flg)" \
)

for elt in "${pipelines[@]}"
do
    eval $elt
    IFS='=' read -r id string <<< "$elt"
    cmd="check_files \${$id[@]}"
    #echo $cmd
    res=1
    #newFlag - for Alerting
    firstCheck=1
    while [ ${res} -ne 0 ]
        do
        eval $cmd
        if [ ${res} -eq 0 ]
        then
            echo "running pipeline ${id}"
            cmd="${script_dir}/run-orch.sh -f conductor.properties -p ${id} --run-date ${run_date}"
            echo ${cmd}
            OUTPUT=$(eval ${cmd})
            exit_status=$?
            echo "${OUTPUT}"
            if [ ${exit_status} -ne 0 ]
            then
                #Error Alert
                ${script_dir}/lib_send_alert.sh --case 3 --output "${OUTPUT}"
                echo "Error occured, check orchestrator logs" && exit 1
            fi
            #Summary Alert
            ${script_dir}/lib_send_alert.sh --case 4 --output "${OUTPUT}"

            touch ${log_dir}/${id}_${file_date}
        fi
        #[ ${res} -ne 0 ] && echo "Not all files from ${id} present, sleeping..." && sleep 900
        
        #Late Case Alert
        ${script_dir}/lib_send_alert.sh --case 1 --res ${res} --firstCheck ${firstCheck} --id ${id} --runDate ${run_date} --detail "$(echo ${pipelines[@]})"
        ((firstCheck++))
    done

done
