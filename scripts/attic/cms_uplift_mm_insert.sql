delete from ${table-prefix}${cms-schema}.${table-prefix}cms_uplift_mm
where file_date = date('${run-date=yyyy-MM-dd}')
;

insert into ${table-prefix}${cms-schema}.${table-prefix}cms_uplift_mm
select
	last_day(date('${run-date=yyyy-MM-dd}')) as periode
	,category
	,initiated_group as initiated_business
	,campaign_objective_id
	,cluster
	,region_hlr
	,sum(revenue_this) as revenue_this
	,sum(revenue_last) as revenue_last
	,sum(revenue_control_this) as revenue_control_this
	,sum(revenue_control_last) as revenue_control_last
	,date('${run-date=yyyy-MM-dd}') as file_date
	,'${job-id}' as job_id
	,'${pipeline-name}' as pipeline_name
	,'${pipeline-step}' as pipeline_step
from 
(
	select
		dtl.msisdn
		,min(case
			when upper(substring(dtl.campaign_id, 11, 1)) = 'A' then 'Simpati'
			when upper(substring(dtl.campaign_id, 11, 1)) = 'B' then 'KartuAs'
			when upper(substring(dtl.campaign_id, 11, 1)) = 'C' then 'AllPrepaid'
			when upper(substring(dtl.campaign_id, 11, 1)) = 'D' then 'KartuHALO'
			when upper(substring(dtl.campaign_id, 11, 1)) = 'E' then 'AllBrands'
			when upper(substring(dtl.campaign_id, 11, 1)) = 'F' then 'Loop'
		end) as brand
		,min(dtl.category) as category
		,min(dtl.initiated_group) as initiated_group
		,min(dtl.campaign_objective_id) as campaign_objective_id
		,min(dtl.cluster) as cluster
		,min(dtl.region_hlr) as region_hlr
		,min(case when ucg.msisdn is null and last_day(dtl.trx_date) = cb.periode then cast(cb.total_revenue as bigint) else 0 end) as revenue_this
		,min(case when ucg.msisdn is null and last_day(dtl.trx_date) = cb.periode then 0 else cast(cb.total_revenue as bigint) end) as revenue_last
		,min(case when ucg.msisdn is not null and last_day(dtl.trx_date) = cb.periode then cast(cb.total_revenue as bigint) else 0 end) as revenue_control_this
		,min(case when ucg.msisdn is not null and last_day(dtl.trx_date) = cb.periode then 0 else cast(cb.total_revenue as bigint) end) as revenue_control_last
	from d02_cms.d02_cms_campaign_tracking_detail dtl
	join d02_cms_stg.d02_cms_pre_post_input cb
	on dtl.msisdn = cb.msisdn
		and 
		(
			last_day(dtl.trx_date) = cb.periode or last_day(last_day(dtl.trx_date) - interval 1 month) = cb.periode
		)
	left join d02_cms_stg.d02_cms_ucg_list_input ucg
	on dtl.msisdn = ucg.msisdn
		and ucg.periode = cb.periode
	where last_day(dtl.trx_date) = date('${run-date=yyyy-MM-dd}')
	group by dtl.msisdn
) t
group by category
	,initiated_group
	,campaign_objective_id
	,cluster
	,region_hlr
;