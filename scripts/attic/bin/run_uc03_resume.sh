#!/bin/sh

source ~/.bashrc
source ~/.bash_profile

cd "$(dirname "$0")/.."

pipeline=${1}
run_date=${2}
		
echo "resuming pipeline ${pipeline} step"
cmd="./bin/run-orch.sh -f conductor.properties --resume ${pipeline} --run-date ${run_date}"
echo ${cmd}
eval ${cmd}
if [ $? -ne 0 ]
then
	echo "Error occured, check orchestrator logs" && exit 1
else
	touch logs/uc03_cms_ingestion_daily.${pipeline}_${run_date}
fi