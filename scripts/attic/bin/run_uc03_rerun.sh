#!/bin/sh

source ~/.bashrc
source ~/.bash_profile

cd "$(dirname "$0")/.."

pipeline=${1}
run_date=${2}
		
echo "running pipeline ${pipeline}"
cmd="./bin/run-orch.sh -f conductor.properties -p ${pipeline} --run-date ${run_date}"
echo ${cmd}
eval ${cmd}
if [ $? -ne 0 ]
then
	echo "Error occured, check orchestrator logs" && exit 1
else
	touch logs/uc03_cms_ingestion_daily.${pipeline}_${run_date}
fi