#!/bin/sh

source ~/.bashrc
source ~/.bash_profile

cd "$(dirname "$0")/.."

./home/hnat_gpu/orch_tucg/bin/run_uc03_cms_ucg_prep.sh
./home/hnat_gpu/orch_tucg/bin/run_uc03_cms_ucg_refresh.sh 2019-04-10
./home/hnat_gpu/orch_tucg/bin/run_uc03_cms_ucg.sh 2019-05-10