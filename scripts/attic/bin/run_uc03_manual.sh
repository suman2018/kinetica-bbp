#!/bin/sh

source ~/.bashrc
source ~/.bash_profile

cd "$(dirname "$0")/.."

pipeline=${2}

if [ $# -eq 3 ]
then
	file_date=${3}
	if [ ${1} -eq "--rerun"]
	then
		if [ ${2} -eq "uc03_cms_core" ]
		then
			./bin/run_uc03_rerun.sh uc03_cms_core ${file_date}
			./bin/run_uc03_rerun.sh uc03_cms_usage_chg ${file_date}
			./bin/run_uc03_rerun.sh uc03_cms_usage_upcc ${file_date}
			./bin/run_uc03_rerun.sh uc03_cms_4g ${file_date}
			./bin/run_uc03_rerun.sh uc03_cms_digital ${file_date}
			./bin/run_uc03_rerun.sh uc03_cms_googleplay ${file_date}
			./bin/run_uc03_rerun.sh uc03_cms_mytelkomsel_rev ${file_date}
			./bin/run_uc03_rerun.sh uc03_cms_mytelkomsel_log ${file_date}
			./bin/run_uc03_rerun.sh uc03_cms_pre2post ${file_date}
			./bin/run_uc03_rerun.sh uc03_cms_recharge ${file_date}
			./bin/run_uc03_rerun.sh uc03_cms_sales ${file_date}
			./bin/run_uc03_rerun.sh uc03_cms_smartphone ${file_date}
			./bin/run_uc03_rerun.sh uc03_cms_upoint ${file_date}
			./bin/run_uc03_rerun.sh uc03_cms_others ${file_date}
			./bin/run_uc03_rerun.sh uc03_cms_core_finalize ${file_date}
			touch logs/uc03_cms_ingestion_daily_${file_date}
		elif [ ${2} -eq "uc03_cms_usage_chg" ]
		then
			./bin/run_uc03_rerun.sh uc03_cms_usage_chg ${file_date}
			./bin/run_uc03_rerun.sh uc03_cms_usage_upcc ${file_date}
			./bin/run_uc03_rerun.sh uc03_cms_4g ${file_date}
			./bin/run_uc03_rerun.sh uc03_cms_digital ${file_date}
			./bin/run_uc03_rerun.sh uc03_cms_googleplay ${file_date}
			./bin/run_uc03_rerun.sh uc03_cms_mytelkomsel_rev ${file_date}
			./bin/run_uc03_rerun.sh uc03_cms_mytelkomsel_log ${file_date}
			./bin/run_uc03_rerun.sh uc03_cms_pre2post ${file_date}
			./bin/run_uc03_rerun.sh uc03_cms_recharge ${file_date}
			./bin/run_uc03_rerun.sh uc03_cms_sales ${file_date}
			./bin/run_uc03_rerun.sh uc03_cms_smartphone ${file_date}
			./bin/run_uc03_rerun.sh uc03_cms_upoint ${file_date}
			./bin/run_uc03_rerun.sh uc03_cms_others ${file_date}
			./bin/run_uc03_rerun.sh uc03_cms_core_finalize ${file_date}
			touch logs/uc03_cms_ingestion_daily_${file_date}
		elif [ ${2} -eq "uc03_cms_usage_upcc" ]
		then
			
			./bin/run_uc03_rerun.sh uc03_cms_usage_upcc ${file_date}
			./bin/run_uc03_rerun.sh uc03_cms_4g ${file_date}
			./bin/run_uc03_rerun.sh uc03_cms_digital ${file_date}
			./bin/run_uc03_rerun.sh uc03_cms_googleplay ${file_date}
			./bin/run_uc03_rerun.sh uc03_cms_mytelkomsel_rev ${file_date}
			./bin/run_uc03_rerun.sh uc03_cms_mytelkomsel_log ${file_date}
			./bin/run_uc03_rerun.sh uc03_cms_pre2post ${file_date}
			./bin/run_uc03_rerun.sh uc03_cms_recharge ${file_date}
			./bin/run_uc03_rerun.sh uc03_cms_sales ${file_date}
			./bin/run_uc03_rerun.sh uc03_cms_smartphone ${file_date}
			./bin/run_uc03_rerun.sh uc03_cms_upoint ${file_date}
			./bin/run_uc03_rerun.sh uc03_cms_others ${file_date}
			./bin/run_uc03_rerun.sh uc03_cms_core_finalize ${file_date}
			touch logs/uc03_cms_ingestion_daily_${file_date}
		elif [ ${2} -eq "uc03_cms_4g" ]
		then
			./bin/run_uc03_rerun.sh uc03_cms_4g ${file_date}
			./bin/run_uc03_rerun.sh uc03_cms_digital ${file_date}
			./bin/run_uc03_rerun.sh uc03_cms_googleplay ${file_date}
			./bin/run_uc03_rerun.sh uc03_cms_mytelkomsel_rev ${file_date}
			./bin/run_uc03_rerun.sh uc03_cms_mytelkomsel_log ${file_date}
			./bin/run_uc03_rerun.sh uc03_cms_pre2post ${file_date}
			./bin/run_uc03_rerun.sh uc03_cms_recharge ${file_date}
			./bin/run_uc03_rerun.sh uc03_cms_sales ${file_date}
			./bin/run_uc03_rerun.sh uc03_cms_smartphone ${file_date}
			./bin/run_uc03_rerun.sh uc03_cms_upoint ${file_date}
			./bin/run_uc03_rerun.sh uc03_cms_others ${file_date}
			./bin/run_uc03_rerun.sh uc03_cms_core_finalize ${file_date}
			touch logs/uc03_cms_ingestion_daily_${file_date}
		elif [ ${2} -eq "uc03_cms_digital" ]
		then
			./bin/run_uc03_rerun.sh uc03_cms_digital ${file_date}
			./bin/run_uc03_rerun.sh uc03_cms_googleplay ${file_date}
			./bin/run_uc03_rerun.sh uc03_cms_mytelkomsel_rev ${file_date}
			./bin/run_uc03_rerun.sh uc03_cms_mytelkomsel_log ${file_date}
			./bin/run_uc03_rerun.sh uc03_cms_pre2post ${file_date}
			./bin/run_uc03_rerun.sh uc03_cms_recharge ${file_date}
			./bin/run_uc03_rerun.sh uc03_cms_sales ${file_date}
			./bin/run_uc03_rerun.sh uc03_cms_smartphone ${file_date}
			./bin/run_uc03_rerun.sh uc03_cms_upoint ${file_date}
			./bin/run_uc03_rerun.sh uc03_cms_others ${file_date}
			./bin/run_uc03_rerun.sh uc03_cms_core_finalize ${file_date}
			touch logs/uc03_cms_ingestion_daily_${file_date}
		elif [ ${2} -eq "uc03_cms_googleplay" ]
		then
			./bin/run_uc03_rerun.sh uc03_cms_googleplay ${file_date}
			./bin/run_uc03_rerun.sh uc03_cms_mytelkomsel_rev ${file_date}
			./bin/run_uc03_rerun.sh uc03_cms_mytelkomsel_log ${file_date}
			./bin/run_uc03_rerun.sh uc03_cms_pre2post ${file_date}
			./bin/run_uc03_rerun.sh uc03_cms_recharge ${file_date}
			./bin/run_uc03_rerun.sh uc03_cms_sales ${file_date}
			./bin/run_uc03_rerun.sh uc03_cms_smartphone ${file_date}
			./bin/run_uc03_rerun.sh uc03_cms_upoint ${file_date}
			./bin/run_uc03_rerun.sh uc03_cms_others ${file_date}
			./bin/run_uc03_rerun.sh uc03_cms_core_finalize ${file_date}
			touch logs/uc03_cms_ingestion_daily_${file_date}
		elif [ ${2} -eq "uc03_cms_mytelkomsel_rev" ]
		then
			./bin/run_uc03_rerun.sh uc03_cms_mytelkomsel_rev ${file_date}
			./bin/run_uc03_rerun.sh uc03_cms_mytelkomsel_log ${file_date}
			./bin/run_uc03_rerun.sh uc03_cms_pre2post ${file_date}
			./bin/run_uc03_rerun.sh uc03_cms_recharge ${file_date}
			./bin/run_uc03_rerun.sh uc03_cms_sales ${file_date}
			./bin/run_uc03_rerun.sh uc03_cms_smartphone ${file_date}
			./bin/run_uc03_rerun.sh uc03_cms_upoint ${file_date}
			./bin/run_uc03_rerun.sh uc03_cms_others ${file_date}
			./bin/run_uc03_rerun.sh uc03_cms_core_finalize ${file_date}
			touch logs/uc03_cms_ingestion_daily_${file_date}
		elif [ ${2} -eq "uc03_cms_mytelkomsel_log" ]
		then
			./bin/run_uc03_rerun.sh uc03_cms_mytelkomsel_log ${file_date}
			./bin/run_uc03_rerun.sh uc03_cms_pre2post ${file_date}
			./bin/run_uc03_rerun.sh uc03_cms_recharge ${file_date}
			./bin/run_uc03_rerun.sh uc03_cms_sales ${file_date}
			./bin/run_uc03_rerun.sh uc03_cms_smartphone ${file_date}
			./bin/run_uc03_rerun.sh uc03_cms_upoint ${file_date}
			./bin/run_uc03_rerun.sh uc03_cms_others ${file_date}
			./bin/run_uc03_rerun.sh uc03_cms_core_finalize ${file_date}
			touch logs/uc03_cms_ingestion_daily_${file_date}
		elif [ ${2} -eq "uc03_cms_pre2post" ]
		then
			./bin/run_uc03_rerun.sh uc03_cms_pre2post ${file_date}
			./bin/run_uc03_rerun.sh uc03_cms_recharge ${file_date}
			./bin/run_uc03_rerun.sh uc03_cms_sales ${file_date}
			./bin/run_uc03_rerun.sh uc03_cms_smartphone ${file_date}
			./bin/run_uc03_rerun.sh uc03_cms_upoint ${file_date}
			./bin/run_uc03_rerun.sh uc03_cms_others ${file_date}
			./bin/run_uc03_rerun.sh uc03_cms_core_finalize ${file_date}
			touch logs/uc03_cms_ingestion_daily_${file_date}
		elif [ ${2} -eq "uc03_cms_recharge" ]
		then
			./bin/run_uc03_rerun.sh uc03_cms_recharge ${file_date}
			./bin/run_uc03_rerun.sh uc03_cms_sales ${file_date}
			./bin/run_uc03_rerun.sh uc03_cms_smartphone ${file_date}
			./bin/run_uc03_rerun.sh uc03_cms_upoint ${file_date}
			./bin/run_uc03_rerun.sh uc03_cms_others ${file_date}
			./bin/run_uc03_rerun.sh uc03_cms_core_finalize ${file_date}
			touch logs/uc03_cms_ingestion_daily_${file_date}
		elif [ ${2} -eq "uc03_cms_sales" ]
		then
			./bin/run_uc03_rerun.sh uc03_cms_sales ${file_date}
			./bin/run_uc03_rerun.sh uc03_cms_smartphone ${file_date}
			./bin/run_uc03_rerun.sh uc03_cms_upoint ${file_date}
			./bin/run_uc03_rerun.sh uc03_cms_others ${file_date}
			./bin/run_uc03_rerun.sh uc03_cms_core_finalize ${file_date}
			touch logs/uc03_cms_ingestion_daily_${file_date}
		elif [ ${2} -eq "uc03_cms_smartphone" ]
		then
			./bin/run_uc03_rerun.sh uc03_cms_smartphone ${file_date}
			./bin/run_uc03_rerun.sh uc03_cms_upoint ${file_date}
			./bin/run_uc03_rerun.sh uc03_cms_others ${file_date}
			./bin/run_uc03_rerun.sh uc03_cms_core_finalize ${file_date}
			touch logs/uc03_cms_ingestion_daily_${file_date}
		elif [ ${2} -eq "uc03_cms_upoint" ]
		then
			./bin/run_uc03_rerun.sh uc03_cms_upoint ${file_date}
			./bin/run_uc03_rerun.sh uc03_cms_others ${file_date}
			./bin/run_uc03_rerun.sh uc03_cms_core_finalize ${file_date}
			touch logs/uc03_cms_ingestion_daily_${file_date}
		elif [ ${2} -eq "uc03_cms_others" ]
		then
			./bin/run_uc03_rerun.sh uc03_cms_others ${file_date}
			./bin/run_uc03_rerun.sh uc03_cms_core_finalize ${file_date}
			touch logs/uc03_cms_ingestion_daily_${file_date}
		elif [ ${2} -eq "uc03_cms_core_finalize" ]
		then
			./bin/run_uc03_rerun.sh uc03_cms_core_finalize ${file_date}
			touch logs/uc03_cms_ingestion_daily_${file_date}
		fi	
	elif [ ${1} -eq "--resume"]
	then
		if [ ${2} -eq "uc03_cms_core*" ]
		then
			./bin/run_uc03_resume.sh ${2} ${file_date}
			./bin/run_uc03_resume.sh uc03_cms_usage_chg ${file_date}
			./bin/run_uc03_resume.sh uc03_cms_usage_upcc ${file_date}
			./bin/run_uc03_resume.sh uc03_cms_4g ${file_date}
			./bin/run_uc03_resume.sh uc03_cms_digital ${file_date}
			./bin/run_uc03_resume.sh uc03_cms_googleplay ${file_date}
			./bin/run_uc03_resume.sh uc03_cms_mytelkomsel_rev ${file_date}
			./bin/run_uc03_resume.sh uc03_cms_mytelkomsel_log ${file_date}
			./bin/run_uc03_resume.sh uc03_cms_pre2post ${file_date}
			./bin/run_uc03_resume.sh uc03_cms_recharge ${file_date}
			./bin/run_uc03_resume.sh uc03_cms_sales ${file_date}
			./bin/run_uc03_resume.sh uc03_cms_smartphone ${file_date}
			./bin/run_uc03_resume.sh uc03_cms_upoint ${file_date}
			./bin/run_uc03_resume.sh uc03_cms_others ${file_date}
			./bin/run_uc03_resume.sh uc03_cms_core_finalize ${file_date}
			touch logs/uc03_cms_ingestion_daily_${file_date}
		elif [ ${2} -eq "uc03_cms_usage_chg*" ]
		then
			./bin/run_uc03_resume.sh ${2} ${file_date}
			./bin/run_uc03_resume.sh uc03_cms_usage_upcc ${file_date}
			./bin/run_uc03_resume.sh uc03_cms_4g ${file_date}
			./bin/run_uc03_resume.sh uc03_cms_digital ${file_date}
			./bin/run_uc03_resume.sh uc03_cms_googleplay ${file_date}
			./bin/run_uc03_resume.sh uc03_cms_mytelkomsel_rev ${file_date}
			./bin/run_uc03_resume.sh uc03_cms_mytelkomsel_log ${file_date}
			./bin/run_uc03_resume.sh uc03_cms_pre2post ${file_date}
			./bin/run_uc03_resume.sh uc03_cms_recharge ${file_date}
			./bin/run_uc03_resume.sh uc03_cms_sales ${file_date}
			./bin/run_uc03_resume.sh uc03_cms_smartphone ${file_date}
			./bin/run_uc03_resume.sh uc03_cms_upoint ${file_date}
			./bin/run_uc03_resume.sh uc03_cms_others ${file_date}
			./bin/run_uc03_resume.sh uc03_cms_core_finalize ${file_date}
			touch logs/uc03_cms_ingestion_daily_${file_date}
		elif [ ${2} -eq "uc03_cms_usage_upcc*" ]
		then
			
			./bin/run_uc03_resume.sh ${2} ${file_date}
			./bin/run_uc03_resume.sh uc03_cms_4g ${file_date}
			./bin/run_uc03_resume.sh uc03_cms_digital ${file_date}
			./bin/run_uc03_resume.sh uc03_cms_googleplay ${file_date}
			./bin/run_uc03_resume.sh uc03_cms_mytelkomsel_rev ${file_date}
			./bin/run_uc03_resume.sh uc03_cms_mytelkomsel_log ${file_date}
			./bin/run_uc03_resume.sh uc03_cms_pre2post ${file_date}
			./bin/run_uc03_resume.sh uc03_cms_recharge ${file_date}
			./bin/run_uc03_resume.sh uc03_cms_sales ${file_date}
			./bin/run_uc03_resume.sh uc03_cms_smartphone ${file_date}
			./bin/run_uc03_resume.sh uc03_cms_upoint ${file_date}
			./bin/run_uc03_resume.sh uc03_cms_others ${file_date}
			./bin/run_uc03_resume.sh uc03_cms_core_finalize ${file_date}
			touch logs/uc03_cms_ingestion_daily_${file_date}
		elif [ ${2} -eq "uc03_cms_4g*" ]
		then
			./bin/run_uc03_resume.sh ${2} ${file_date}
			./bin/run_uc03_resume.sh uc03_cms_digital ${file_date}
			./bin/run_uc03_resume.sh uc03_cms_googleplay ${file_date}
			./bin/run_uc03_resume.sh uc03_cms_mytelkomsel_rev ${file_date}
			./bin/run_uc03_resume.sh uc03_cms_mytelkomsel_log ${file_date}
			./bin/run_uc03_resume.sh uc03_cms_pre2post ${file_date}
			./bin/run_uc03_resume.sh uc03_cms_recharge ${file_date}
			./bin/run_uc03_resume.sh uc03_cms_sales ${file_date}
			./bin/run_uc03_resume.sh uc03_cms_smartphone ${file_date}
			./bin/run_uc03_resume.sh uc03_cms_upoint ${file_date}
			./bin/run_uc03_resume.sh uc03_cms_others ${file_date}
			./bin/run_uc03_resume.sh uc03_cms_core_finalize ${file_date}
			touch logs/uc03_cms_ingestion_daily_${file_date}
		elif [ ${2} -eq "uc03_cms_digital*" ]
		then
			./bin/run_uc03_resume.sh ${2} ${file_date}
			./bin/run_uc03_resume.sh uc03_cms_googleplay ${file_date}
			./bin/run_uc03_resume.sh uc03_cms_mytelkomsel_rev ${file_date}
			./bin/run_uc03_resume.sh uc03_cms_mytelkomsel_log ${file_date}
			./bin/run_uc03_resume.sh uc03_cms_pre2post ${file_date}
			./bin/run_uc03_resume.sh uc03_cms_recharge ${file_date}
			./bin/run_uc03_resume.sh uc03_cms_sales ${file_date}
			./bin/run_uc03_resume.sh uc03_cms_smartphone ${file_date}
			./bin/run_uc03_resume.sh uc03_cms_upoint ${file_date}
			./bin/run_uc03_resume.sh uc03_cms_others ${file_date}
			./bin/run_uc03_resume.sh uc03_cms_core_finalize ${file_date}
			touch logs/uc03_cms_ingestion_daily_${file_date}
		elif [ ${2} -eq "uc03_cms_googleplay*" ]
		then
			./bin/run_uc03_resume.sh ${2} ${file_date}
			./bin/run_uc03_resume.sh uc03_cms_mytelkomsel_rev ${file_date}
			./bin/run_uc03_resume.sh uc03_cms_mytelkomsel_log ${file_date}
			./bin/run_uc03_resume.sh uc03_cms_pre2post ${file_date}
			./bin/run_uc03_resume.sh uc03_cms_recharge ${file_date}
			./bin/run_uc03_resume.sh uc03_cms_sales ${file_date}
			./bin/run_uc03_resume.sh uc03_cms_smartphone ${file_date}
			./bin/run_uc03_resume.sh uc03_cms_upoint ${file_date}
			./bin/run_uc03_resume.sh uc03_cms_others ${file_date}
			./bin/run_uc03_resume.sh uc03_cms_core_finalize ${file_date}
			touch logs/uc03_cms_ingestion_daily_${file_date}
		elif [ ${2} -eq "uc03_cms_mytelkomsel_rev" ]
		then
			./bin/run_uc03_resume.sh ${2} ${file_date}
			./bin/run_uc03_resume.sh uc03_cms_mytelkomsel_log ${file_date}
			./bin/run_uc03_resume.sh uc03_cms_pre2post ${file_date}
			./bin/run_uc03_resume.sh uc03_cms_recharge ${file_date}
			./bin/run_uc03_resume.sh uc03_cms_sales ${file_date}
			./bin/run_uc03_resume.sh uc03_cms_smartphone ${file_date}
			./bin/run_uc03_resume.sh uc03_cms_upoint ${file_date}
			./bin/run_uc03_resume.sh uc03_cms_others ${file_date}
			./bin/run_uc03_resume.sh uc03_cms_core_finalize ${file_date}
			touch logs/uc03_cms_ingestion_daily_${file_date}
		elif [ ${2} -eq "uc03_cms_mytelkomsel_log*" ]
		then
			./bin/run_uc03_resume.sh ${2} ${file_date}
			./bin/run_uc03_resume.sh uc03_cms_pre2post ${file_date}
			./bin/run_uc03_resume.sh uc03_cms_recharge ${file_date}
			./bin/run_uc03_resume.sh uc03_cms_sales ${file_date}
			./bin/run_uc03_resume.sh uc03_cms_smartphone ${file_date}
			./bin/run_uc03_resume.sh uc03_cms_upoint ${file_date}
			./bin/run_uc03_resume.sh uc03_cms_others ${file_date}
			./bin/run_uc03_resume.sh uc03_cms_core_finalize ${file_date}
			touch logs/uc03_cms_ingestion_daily_${file_date}
		elif [ ${2} -eq "uc03_cms_pre2post*" ]
		then
			./bin/run_uc03_resume.sh ${2} ${file_date}
			./bin/run_uc03_resume.sh uc03_cms_recharge ${file_date}
			./bin/run_uc03_resume.sh uc03_cms_sales ${file_date}
			./bin/run_uc03_resume.sh uc03_cms_smartphone ${file_date}
			./bin/run_uc03_resume.sh uc03_cms_upoint ${file_date}
			./bin/run_uc03_resume.sh uc03_cms_others ${file_date}
			./bin/run_uc03_resume.sh uc03_cms_core_finalize ${file_date}
			touch logs/uc03_cms_ingestion_daily_${file_date}
		elif [ ${2} -eq "uc03_cms_recharge*" ]
		then
			./bin/run_uc03_resume.sh ${2} ${file_date}
			./bin/run_uc03_resume.sh uc03_cms_sales ${file_date}
			./bin/run_uc03_resume.sh uc03_cms_smartphone ${file_date}
			./bin/run_uc03_resume.sh uc03_cms_upoint ${file_date}
			./bin/run_uc03_resume.sh uc03_cms_others ${file_date}
			./bin/run_uc03_resume.sh uc03_cms_core_finalize ${file_date}
			touch logs/uc03_cms_ingestion_daily_${file_date}
		elif [ ${2} -eq "uc03_cms_sales*" ]
		then
			./bin/run_uc03_resume.sh ${2} ${file_date}
			./bin/run_uc03_resume.sh uc03_cms_smartphone ${file_date}
			./bin/run_uc03_resume.sh uc03_cms_upoint ${file_date}
			./bin/run_uc03_resume.sh uc03_cms_others ${file_date}
			./bin/run_uc03_resume.sh uc03_cms_core_finalize ${file_date}
			touch logs/uc03_cms_ingestion_daily_${file_date}
		elif [ ${2} -eq "uc03_cms_smartphone*" ]
		then
			./bin/run_uc03_resume.sh ${2} ${file_date}
			./bin/run_uc03_resume.sh uc03_cms_upoint ${file_date}
			./bin/run_uc03_resume.sh uc03_cms_others ${file_date}
			./bin/run_uc03_resume.sh uc03_cms_core_finalize ${file_date}
			touch logs/uc03_cms_ingestion_daily_${file_date}
		elif [ ${2} -eq "uc03_cms_upoint*" ]
		then
			./bin/run_uc03_resume.sh ${2} ${file_date}
			./bin/run_uc03_resume.sh uc03_cms_others ${file_date}
			./bin/run_uc03_resume.sh uc03_cms_core_finalize ${file_date}
			touch logs/uc03_cms_ingestion_daily_${file_date}
		elif [ ${2} -eq "uc03_cms_others*" ]
		then
			./bin/run_uc03_resume.sh ${2} ${file_date}
			./bin/run_uc03_resume.sh uc03_cms_core_finalize ${file_date}
			touch logs/uc03_cms_ingestion_daily_${file_date}
		elif [ ${2} -eq "uc03_cms_core_finalize*" ]
		then
			./bin/run_uc03_resume.sh ${2} ${file_date}
			touch logs/uc03_cms_ingestion_daily_${file_date}
		fi
	elif [ ${1} -eq "--help" || $1 -eq "-h"]
	then
		echo "usage: run_uc03_manual.sh
		  	  -h,--help      					    	Display parameters.\n
		      --resume                            		Resume pipeline.\n
		      --rerun							 		Rerun pipeline."	
	else
		echo "Unable to recognize ${1}, try the following parameters instead:\n
			  -h,--help                                 Display parameters.\n
			     --resume                               Resume pipeline.\n
			     --rerun							    Rerun pipeline."
	fi
elif [ $# -eq 2 ]
then
	run_date=`date +%Y-%m-%d`
	if [ ${1} -eq "--rerun"]
	then
		if [ ${2} -eq "uc03_cms_core" ]
		then
			./bin/run_uc03_rerun.sh uc03_cms_core ${file_date}
			./bin/run_uc03_rerun.sh uc03_cms_usage_chg ${file_date}
			./bin/run_uc03_rerun.sh uc03_cms_usage_upcc ${file_date}
			./bin/run_uc03_rerun.sh uc03_cms_4g ${file_date}
			./bin/run_uc03_rerun.sh uc03_cms_digital ${file_date}
			./bin/run_uc03_rerun.sh uc03_cms_googleplay ${file_date}
			./bin/run_uc03_rerun.sh uc03_cms_mytelkomsel_rev ${file_date}
			./bin/run_uc03_rerun.sh uc03_cms_mytelkomsel_log ${file_date}
			./bin/run_uc03_rerun.sh uc03_cms_pre2post ${file_date}
			./bin/run_uc03_rerun.sh uc03_cms_recharge ${file_date}
			./bin/run_uc03_rerun.sh uc03_cms_sales ${file_date}
			./bin/run_uc03_rerun.sh uc03_cms_smartphone ${file_date}
			./bin/run_uc03_rerun.sh uc03_cms_upoint ${file_date}
			./bin/run_uc03_rerun.sh uc03_cms_others ${file_date}
			./bin/run_uc03_rerun.sh uc03_cms_core_finalize ${file_date}
			touch logs/uc03_cms_ingestion_daily_${file_date}
		elif [ ${2} -eq "uc03_cms_usage_chg" ]
		then
			./bin/run_uc03_rerun.sh uc03_cms_usage_chg ${file_date}
			./bin/run_uc03_rerun.sh uc03_cms_usage_upcc ${file_date}
			./bin/run_uc03_rerun.sh uc03_cms_4g ${file_date}
			./bin/run_uc03_rerun.sh uc03_cms_digital ${file_date}
			./bin/run_uc03_rerun.sh uc03_cms_googleplay ${file_date}
			./bin/run_uc03_rerun.sh uc03_cms_mytelkomsel_rev ${file_date}
			./bin/run_uc03_rerun.sh uc03_cms_mytelkomsel_log ${file_date}
			./bin/run_uc03_rerun.sh uc03_cms_pre2post ${file_date}
			./bin/run_uc03_rerun.sh uc03_cms_recharge ${file_date}
			./bin/run_uc03_rerun.sh uc03_cms_sales ${file_date}
			./bin/run_uc03_rerun.sh uc03_cms_smartphone ${file_date}
			./bin/run_uc03_rerun.sh uc03_cms_upoint ${file_date}
			./bin/run_uc03_rerun.sh uc03_cms_others ${file_date}
			./bin/run_uc03_rerun.sh uc03_cms_core_finalize ${file_date}
			touch logs/uc03_cms_ingestion_daily_${file_date}
		elif [ ${2} -eq "uc03_cms_usage_upcc" ]
		then
			
			./bin/run_uc03_rerun.sh uc03_cms_usage_upcc ${file_date}
			./bin/run_uc03_rerun.sh uc03_cms_4g ${file_date}
			./bin/run_uc03_rerun.sh uc03_cms_digital ${file_date}
			./bin/run_uc03_rerun.sh uc03_cms_googleplay ${file_date}
			./bin/run_uc03_rerun.sh uc03_cms_mytelkomsel_rev ${file_date}
			./bin/run_uc03_rerun.sh uc03_cms_mytelkomsel_log ${file_date}
			./bin/run_uc03_rerun.sh uc03_cms_pre2post ${file_date}
			./bin/run_uc03_rerun.sh uc03_cms_recharge ${file_date}
			./bin/run_uc03_rerun.sh uc03_cms_sales ${file_date}
			./bin/run_uc03_rerun.sh uc03_cms_smartphone ${file_date}
			./bin/run_uc03_rerun.sh uc03_cms_upoint ${file_date}
			./bin/run_uc03_rerun.sh uc03_cms_others ${file_date}
			./bin/run_uc03_rerun.sh uc03_cms_core_finalize ${file_date}
			touch logs/uc03_cms_ingestion_daily_${file_date}
		elif [ ${2} -eq "uc03_cms_4g" ]
		then
			./bin/run_uc03_rerun.sh uc03_cms_4g ${file_date}
			./bin/run_uc03_rerun.sh uc03_cms_digital ${file_date}
			./bin/run_uc03_rerun.sh uc03_cms_googleplay ${file_date}
			./bin/run_uc03_rerun.sh uc03_cms_mytelkomsel_rev ${file_date}
			./bin/run_uc03_rerun.sh uc03_cms_mytelkomsel_log ${file_date}
			./bin/run_uc03_rerun.sh uc03_cms_pre2post ${file_date}
			./bin/run_uc03_rerun.sh uc03_cms_recharge ${file_date}
			./bin/run_uc03_rerun.sh uc03_cms_sales ${file_date}
			./bin/run_uc03_rerun.sh uc03_cms_smartphone ${file_date}
			./bin/run_uc03_rerun.sh uc03_cms_upoint ${file_date}
			./bin/run_uc03_rerun.sh uc03_cms_others ${file_date}
			./bin/run_uc03_rerun.sh uc03_cms_core_finalize ${file_date}
			touch logs/uc03_cms_ingestion_daily_${file_date}
		elif [ ${2} -eq "uc03_cms_digital" ]
		then
			./bin/run_uc03_rerun.sh uc03_cms_digital ${file_date}
			./bin/run_uc03_rerun.sh uc03_cms_googleplay ${file_date}
			./bin/run_uc03_rerun.sh uc03_cms_mytelkomsel_rev ${file_date}
			./bin/run_uc03_rerun.sh uc03_cms_mytelkomsel_log ${file_date}
			./bin/run_uc03_rerun.sh uc03_cms_pre2post ${file_date}
			./bin/run_uc03_rerun.sh uc03_cms_recharge ${file_date}
			./bin/run_uc03_rerun.sh uc03_cms_sales ${file_date}
			./bin/run_uc03_rerun.sh uc03_cms_smartphone ${file_date}
			./bin/run_uc03_rerun.sh uc03_cms_upoint ${file_date}
			./bin/run_uc03_rerun.sh uc03_cms_others ${file_date}
			./bin/run_uc03_rerun.sh uc03_cms_core_finalize ${file_date}
			touch logs/uc03_cms_ingestion_daily_${file_date}
		elif [ ${2} -eq "uc03_cms_googleplay" ]
		then
			./bin/run_uc03_rerun.sh uc03_cms_googleplay ${file_date}
			./bin/run_uc03_rerun.sh uc03_cms_mytelkomsel_rev ${file_date}
			./bin/run_uc03_rerun.sh uc03_cms_mytelkomsel_log ${file_date}
			./bin/run_uc03_rerun.sh uc03_cms_pre2post ${file_date}
			./bin/run_uc03_rerun.sh uc03_cms_recharge ${file_date}
			./bin/run_uc03_rerun.sh uc03_cms_sales ${file_date}
			./bin/run_uc03_rerun.sh uc03_cms_smartphone ${file_date}
			./bin/run_uc03_rerun.sh uc03_cms_upoint ${file_date}
			./bin/run_uc03_rerun.sh uc03_cms_others ${file_date}
			./bin/run_uc03_rerun.sh uc03_cms_core_finalize ${file_date}
			touch logs/uc03_cms_ingestion_daily_${file_date}
		elif [ ${2} -eq "uc03_cms_mytelkomsel_rev" ]
		then
			./bin/run_uc03_rerun.sh uc03_cms_mytelkomsel_rev ${file_date}
			./bin/run_uc03_rerun.sh uc03_cms_mytelkomsel_log ${file_date}
			./bin/run_uc03_rerun.sh uc03_cms_pre2post ${file_date}
			./bin/run_uc03_rerun.sh uc03_cms_recharge ${file_date}
			./bin/run_uc03_rerun.sh uc03_cms_sales ${file_date}
			./bin/run_uc03_rerun.sh uc03_cms_smartphone ${file_date}
			./bin/run_uc03_rerun.sh uc03_cms_upoint ${file_date}
			./bin/run_uc03_rerun.sh uc03_cms_others ${file_date}
			./bin/run_uc03_rerun.sh uc03_cms_core_finalize ${file_date}
			touch logs/uc03_cms_ingestion_daily_${file_date}
		elif [ ${2} -eq "uc03_cms_mytelkomsel_log" ]
		then
			./bin/run_uc03_rerun.sh uc03_cms_mytelkomsel_log ${file_date}
			./bin/run_uc03_rerun.sh uc03_cms_pre2post ${file_date}
			./bin/run_uc03_rerun.sh uc03_cms_recharge ${file_date}
			./bin/run_uc03_rerun.sh uc03_cms_sales ${file_date}
			./bin/run_uc03_rerun.sh uc03_cms_smartphone ${file_date}
			./bin/run_uc03_rerun.sh uc03_cms_upoint ${file_date}
			./bin/run_uc03_rerun.sh uc03_cms_others ${file_date}
			./bin/run_uc03_rerun.sh uc03_cms_core_finalize ${file_date}
			touch logs/uc03_cms_ingestion_daily_${file_date}
		elif [ ${2} -eq "uc03_cms_pre2post" ]
		then
			./bin/run_uc03_rerun.sh uc03_cms_pre2post ${file_date}
			./bin/run_uc03_rerun.sh uc03_cms_recharge ${file_date}
			./bin/run_uc03_rerun.sh uc03_cms_sales ${file_date}
			./bin/run_uc03_rerun.sh uc03_cms_smartphone ${file_date}
			./bin/run_uc03_rerun.sh uc03_cms_upoint ${file_date}
			./bin/run_uc03_rerun.sh uc03_cms_others ${file_date}
			./bin/run_uc03_rerun.sh uc03_cms_core_finalize ${file_date}
			touch logs/uc03_cms_ingestion_daily_${file_date}
		elif [ ${2} -eq "uc03_cms_recharge" ]
		then
			./bin/run_uc03_rerun.sh uc03_cms_recharge ${file_date}
			./bin/run_uc03_rerun.sh uc03_cms_sales ${file_date}
			./bin/run_uc03_rerun.sh uc03_cms_smartphone ${file_date}
			./bin/run_uc03_rerun.sh uc03_cms_upoint ${file_date}
			./bin/run_uc03_rerun.sh uc03_cms_others ${file_date}
			./bin/run_uc03_rerun.sh uc03_cms_core_finalize ${file_date}
			touch logs/uc03_cms_ingestion_daily_${file_date}
		elif [ ${2} -eq "uc03_cms_sales" ]
		then
			./bin/run_uc03_rerun.sh uc03_cms_sales ${file_date}
			./bin/run_uc03_rerun.sh uc03_cms_smartphone ${file_date}
			./bin/run_uc03_rerun.sh uc03_cms_upoint ${file_date}
			./bin/run_uc03_rerun.sh uc03_cms_others ${file_date}
			./bin/run_uc03_rerun.sh uc03_cms_core_finalize ${file_date}
			touch logs/uc03_cms_ingestion_daily_${file_date}
		elif [ ${2} -eq "uc03_cms_smartphone" ]
		then
			./bin/run_uc03_rerun.sh uc03_cms_smartphone ${file_date}
			./bin/run_uc03_rerun.sh uc03_cms_upoint ${file_date}
			./bin/run_uc03_rerun.sh uc03_cms_others ${file_date}
			./bin/run_uc03_rerun.sh uc03_cms_core_finalize ${file_date}
			touch logs/uc03_cms_ingestion_daily_${file_date}
		elif [ ${2} -eq "uc03_cms_upoint" ]
		then
			./bin/run_uc03_rerun.sh uc03_cms_upoint ${file_date}
			./bin/run_uc03_rerun.sh uc03_cms_others ${file_date}
			./bin/run_uc03_rerun.sh uc03_cms_core_finalize ${file_date}
			touch logs/uc03_cms_ingestion_daily_${file_date}
		elif [ ${2} -eq "uc03_cms_others" ]
		then
			./bin/run_uc03_rerun.sh uc03_cms_others ${file_date}
			./bin/run_uc03_rerun.sh uc03_cms_core_finalize ${file_date}
			touch logs/uc03_cms_ingestion_daily_${file_date}
		elif [ ${2} -eq "uc03_cms_core_finalize" ]
		then
			./bin/run_uc03_rerun.sh uc03_cms_core_finalize ${file_date}
			touch logs/uc03_cms_ingestion_daily_${file_date}
		fi	
	elif [ ${1} -eq "--resume"]
	then
		if [ ${2} -eq "uc03_cms_core*" ]
		then
			./bin/run_uc03_resume.sh ${2} ${file_date}
			./bin/run_uc03_resume.sh uc03_cms_usage_chg ${file_date}
			./bin/run_uc03_resume.sh uc03_cms_usage_upcc ${file_date}
			./bin/run_uc03_resume.sh uc03_cms_4g ${file_date}
			./bin/run_uc03_resume.sh uc03_cms_digital ${file_date}
			./bin/run_uc03_resume.sh uc03_cms_googleplay ${file_date}
			./bin/run_uc03_resume.sh uc03_cms_mytelkomsel_rev ${file_date}
			./bin/run_uc03_resume.sh uc03_cms_mytelkomsel_log ${file_date}
			./bin/run_uc03_resume.sh uc03_cms_pre2post ${file_date}
			./bin/run_uc03_resume.sh uc03_cms_recharge ${file_date}
			./bin/run_uc03_resume.sh uc03_cms_sales ${file_date}
			./bin/run_uc03_resume.sh uc03_cms_smartphone ${file_date}
			./bin/run_uc03_resume.sh uc03_cms_upoint ${file_date}
			./bin/run_uc03_resume.sh uc03_cms_others ${file_date}
			./bin/run_uc03_resume.sh uc03_cms_core_finalize ${file_date}
			touch logs/uc03_cms_ingestion_daily_${file_date}
		elif [ ${2} -eq "uc03_cms_usage_chg*" ]
		then
			./bin/run_uc03_resume.sh ${2} ${file_date}
			./bin/run_uc03_resume.sh uc03_cms_usage_upcc ${file_date}
			./bin/run_uc03_resume.sh uc03_cms_4g ${file_date}
			./bin/run_uc03_resume.sh uc03_cms_digital ${file_date}
			./bin/run_uc03_resume.sh uc03_cms_googleplay ${file_date}
			./bin/run_uc03_resume.sh uc03_cms_mytelkomsel_rev ${file_date}
			./bin/run_uc03_resume.sh uc03_cms_mytelkomsel_log ${file_date}
			./bin/run_uc03_resume.sh uc03_cms_pre2post ${file_date}
			./bin/run_uc03_resume.sh uc03_cms_recharge ${file_date}
			./bin/run_uc03_resume.sh uc03_cms_sales ${file_date}
			./bin/run_uc03_resume.sh uc03_cms_smartphone ${file_date}
			./bin/run_uc03_resume.sh uc03_cms_upoint ${file_date}
			./bin/run_uc03_resume.sh uc03_cms_others ${file_date}
			./bin/run_uc03_resume.sh uc03_cms_core_finalize ${file_date}
			touch logs/uc03_cms_ingestion_daily_${file_date}
		elif [ ${2} -eq "uc03_cms_usage_upcc*" ]
		then
			
			./bin/run_uc03_resume.sh ${2} ${file_date}
			./bin/run_uc03_resume.sh uc03_cms_4g ${file_date}
			./bin/run_uc03_resume.sh uc03_cms_digital ${file_date}
			./bin/run_uc03_resume.sh uc03_cms_googleplay ${file_date}
			./bin/run_uc03_resume.sh uc03_cms_mytelkomsel_rev ${file_date}
			./bin/run_uc03_resume.sh uc03_cms_mytelkomsel_log ${file_date}
			./bin/run_uc03_resume.sh uc03_cms_pre2post ${file_date}
			./bin/run_uc03_resume.sh uc03_cms_recharge ${file_date}
			./bin/run_uc03_resume.sh uc03_cms_sales ${file_date}
			./bin/run_uc03_resume.sh uc03_cms_smartphone ${file_date}
			./bin/run_uc03_resume.sh uc03_cms_upoint ${file_date}
			./bin/run_uc03_resume.sh uc03_cms_others ${file_date}
			./bin/run_uc03_resume.sh uc03_cms_core_finalize ${file_date}
			touch logs/uc03_cms_ingestion_daily_${file_date}
		elif [ ${2} -eq "uc03_cms_4g*" ]
		then
			./bin/run_uc03_resume.sh ${2} ${file_date}
			./bin/run_uc03_resume.sh uc03_cms_digital ${file_date}
			./bin/run_uc03_resume.sh uc03_cms_googleplay ${file_date}
			./bin/run_uc03_resume.sh uc03_cms_mytelkomsel_rev ${file_date}
			./bin/run_uc03_resume.sh uc03_cms_mytelkomsel_log ${file_date}
			./bin/run_uc03_resume.sh uc03_cms_pre2post ${file_date}
			./bin/run_uc03_resume.sh uc03_cms_recharge ${file_date}
			./bin/run_uc03_resume.sh uc03_cms_sales ${file_date}
			./bin/run_uc03_resume.sh uc03_cms_smartphone ${file_date}
			./bin/run_uc03_resume.sh uc03_cms_upoint ${file_date}
			./bin/run_uc03_resume.sh uc03_cms_others ${file_date}
			./bin/run_uc03_resume.sh uc03_cms_core_finalize ${file_date}
			touch logs/uc03_cms_ingestion_daily_${file_date}
		elif [ ${2} -eq "uc03_cms_digital*" ]
		then
			./bin/run_uc03_resume.sh ${2} ${file_date}
			./bin/run_uc03_resume.sh uc03_cms_googleplay ${file_date}
			./bin/run_uc03_resume.sh uc03_cms_mytelkomsel_rev ${file_date}
			./bin/run_uc03_resume.sh uc03_cms_mytelkomsel_log ${file_date}
			./bin/run_uc03_resume.sh uc03_cms_pre2post ${file_date}
			./bin/run_uc03_resume.sh uc03_cms_recharge ${file_date}
			./bin/run_uc03_resume.sh uc03_cms_sales ${file_date}
			./bin/run_uc03_resume.sh uc03_cms_smartphone ${file_date}
			./bin/run_uc03_resume.sh uc03_cms_upoint ${file_date}
			./bin/run_uc03_resume.sh uc03_cms_others ${file_date}
			./bin/run_uc03_resume.sh uc03_cms_core_finalize ${file_date}
			touch logs/uc03_cms_ingestion_daily_${file_date}
		elif [ ${2} -eq "uc03_cms_googleplay*" ]
		then
			./bin/run_uc03_resume.sh ${2} ${file_date}
			./bin/run_uc03_resume.sh uc03_cms_mytelkomsel_rev ${file_date}
			./bin/run_uc03_resume.sh uc03_cms_mytelkomsel_log ${file_date}
			./bin/run_uc03_resume.sh uc03_cms_pre2post ${file_date}
			./bin/run_uc03_resume.sh uc03_cms_recharge ${file_date}
			./bin/run_uc03_resume.sh uc03_cms_sales ${file_date}
			./bin/run_uc03_resume.sh uc03_cms_smartphone ${file_date}
			./bin/run_uc03_resume.sh uc03_cms_upoint ${file_date}
			./bin/run_uc03_resume.sh uc03_cms_others ${file_date}
			./bin/run_uc03_resume.sh uc03_cms_core_finalize ${file_date}
			touch logs/uc03_cms_ingestion_daily_${file_date}
		elif [ ${2} -eq "uc03_cms_mytelkomsel_rev" ]
		then
			./bin/run_uc03_resume.sh ${2} ${file_date}
			./bin/run_uc03_resume.sh uc03_cms_mytelkomsel_log ${file_date}
			./bin/run_uc03_resume.sh uc03_cms_pre2post ${file_date}
			./bin/run_uc03_resume.sh uc03_cms_recharge ${file_date}
			./bin/run_uc03_resume.sh uc03_cms_sales ${file_date}
			./bin/run_uc03_resume.sh uc03_cms_smartphone ${file_date}
			./bin/run_uc03_resume.sh uc03_cms_upoint ${file_date}
			./bin/run_uc03_resume.sh uc03_cms_others ${file_date}
			./bin/run_uc03_resume.sh uc03_cms_core_finalize ${file_date}
			touch logs/uc03_cms_ingestion_daily_${file_date}
		elif [ ${2} -eq "uc03_cms_mytelkomsel_log*" ]
		then
			./bin/run_uc03_resume.sh ${2} ${file_date}
			./bin/run_uc03_resume.sh uc03_cms_pre2post ${file_date}
			./bin/run_uc03_resume.sh uc03_cms_recharge ${file_date}
			./bin/run_uc03_resume.sh uc03_cms_sales ${file_date}
			./bin/run_uc03_resume.sh uc03_cms_smartphone ${file_date}
			./bin/run_uc03_resume.sh uc03_cms_upoint ${file_date}
			./bin/run_uc03_resume.sh uc03_cms_others ${file_date}
			./bin/run_uc03_resume.sh uc03_cms_core_finalize ${file_date}
			touch logs/uc03_cms_ingestion_daily_${file_date}
		elif [ ${2} -eq "uc03_cms_pre2post*" ]
		then
			./bin/run_uc03_resume.sh ${2} ${file_date}
			./bin/run_uc03_resume.sh uc03_cms_recharge ${file_date}
			./bin/run_uc03_resume.sh uc03_cms_sales ${file_date}
			./bin/run_uc03_resume.sh uc03_cms_smartphone ${file_date}
			./bin/run_uc03_resume.sh uc03_cms_upoint ${file_date}
			./bin/run_uc03_resume.sh uc03_cms_others ${file_date}
			./bin/run_uc03_resume.sh uc03_cms_core_finalize ${file_date}
			touch logs/uc03_cms_ingestion_daily_${file_date}
		elif [ ${2} -eq "uc03_cms_recharge*" ]
		then
			./bin/run_uc03_resume.sh ${2} ${file_date}
			./bin/run_uc03_resume.sh uc03_cms_sales ${file_date}
			./bin/run_uc03_resume.sh uc03_cms_smartphone ${file_date}
			./bin/run_uc03_resume.sh uc03_cms_upoint ${file_date}
			./bin/run_uc03_resume.sh uc03_cms_others ${file_date}
			./bin/run_uc03_resume.sh uc03_cms_core_finalize ${file_date}
			touch logs/uc03_cms_ingestion_daily_${file_date}
		elif [ ${2} -eq "uc03_cms_sales*" ]
		then
			./bin/run_uc03_resume.sh ${2} ${file_date}
			./bin/run_uc03_resume.sh uc03_cms_smartphone ${file_date}
			./bin/run_uc03_resume.sh uc03_cms_upoint ${file_date}
			./bin/run_uc03_resume.sh uc03_cms_others ${file_date}
			./bin/run_uc03_resume.sh uc03_cms_core_finalize ${file_date}
			touch logs/uc03_cms_ingestion_daily_${file_date}
		elif [ ${2} -eq "uc03_cms_smartphone*" ]
		then
			./bin/run_uc03_resume.sh ${2} ${file_date}
			./bin/run_uc03_resume.sh uc03_cms_upoint ${file_date}
			./bin/run_uc03_resume.sh uc03_cms_others ${file_date}
			./bin/run_uc03_resume.sh uc03_cms_core_finalize ${file_date}
			touch logs/uc03_cms_ingestion_daily_${file_date}
		elif [ ${2} -eq "uc03_cms_upoint*" ]
		then
			./bin/run_uc03_resume.sh ${2} ${file_date}
			./bin/run_uc03_resume.sh uc03_cms_others ${file_date}
			./bin/run_uc03_resume.sh uc03_cms_core_finalize ${file_date}
			touch logs/uc03_cms_ingestion_daily_${file_date}
		elif [ ${2} -eq "uc03_cms_others*" ]
		then
			./bin/run_uc03_resume.sh ${2} ${file_date}
			./bin/run_uc03_resume.sh uc03_cms_core_finalize ${file_date}
			touch logs/uc03_cms_ingestion_daily_${file_date}
		elif [ ${2} -eq "uc03_cms_core_finalize*" ]
		then
			./bin/run_uc03_resume.sh ${2} ${file_date}
			touch logs/uc03_cms_ingestion_daily_${file_date}
		fi
	elif [ ${1} -eq "--help" || $1 -eq "-h"]
	then
		echo "usage: run_uc03_manual.sh
		  	  -h,--help      					    	Display parameters.\n
		      --resume                            		Resume pipeline.\n
		      --rerun							 		Rerun pipeline."	
	else
		echo "Unable to recognize ${1}, try the following parameters instead:\n
			  -h,--help                                 Display parameters.\n
			     --resume                               Resume pipeline.\n
			     --rerun							    Rerun pipeline."
	fi
fi	