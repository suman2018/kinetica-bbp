#!/bin/bash

source ~/.bashrc
source ~/.bash_profile
. lib_alert_variables.sh
. lib_send_alert.sh
. lib_check_files.sh

cd "$(dirname "$0")/.."

if [ $# -eq 1 ]
  then
     run_date=${1}
fi

if [ $# -eq 0 ]
then
  [ -z ${curr_date} ] && curr_date=`date +%Y-%m-%d`
  cmd="date -d'${curr_date} - 3 day' +%Y-%m-%d"
  run_date=`eval $cmd`
fi

echo "Rundate : ${run_date}"

cmd="date -d'${run_date}' +%Y%m%d"
file_date=`eval $cmd`
cmd="date -d'${run_date} - 1 day' +%Y%m%d"
last_date=`eval $cmd`


file_path="/data01/hnat_gpu/landing_uc1_dev/report_gpu/OUT/${file_date}"
pipelines=( \
"uc01_reference=(logs/uc01_reference_${last_date} ${file_path}/gpu_hlr_ref_${file_date}.flg ${file_path}/gpu_laccima_dim_tdch_${file_date}.flg ${file_path}/offer_dim_${file_date}.flg ${file_path}/gpu_PRODUCT_REFF_${file_date}.flg)" \
)

for elt in "${pipelines[@]}"
do
    eval $elt
    IFS='=' read -r id string <<< "$elt"
    cmd="check_files \${$id[@]}"
    #echo $cmd
    res=1
    #newFlag - for Alerting
    firstCheck=1
    while [ ${res} -ne 0 ]
        do
        eval $cmd
        res_file_check=${?}
        if [ ${res} -eq 0 ]
        then
            echo "running pipeline ${id}"
            cmd="./bin/run-orch.sh -f conductor.properties -p ${id} --run-date ${run_date}"
            echo ${cmd}
            ########comment for testing purpose######## eval ${cmd}##to be implemented##OUTPUT=$(eval ${cmd})
            ##to be implemented##exit_status=$?
            ##to be implemented##echo "${OUTPUT}"
            ##to be implemented##log_output=$(echo ${OUTPUT} | sed 's/.*Log File: //' | cut -d' ' -f 1)
            ##to be implemented##if [ ${exit_status} -ne 0 ]
            if [ $? -ne 0 ]
            then
                #Error Alert
                ##to be implemented##${script_dir}/lib_send_alert.sh --case 3 --logFile ${log_output} 
                echo "Error occured, check orchestrator logs" && exit 1
            fi
            touch logs/${id}_${file_date}
            touch logs/${id}_hlr_${file_date}
            touch logs/${id}_laccima_${file_date}
            touch logs/${id}_offer_dim_${file_date}
            touch logs/${id}_product_line_${file_date}
        fi
        #[ ${res} -ne 0 ] && echo "Not all files from ${id} present, sleeping..." && sleep 900
        
        #NEW condition - for Alerting
        #echo "FLAG first check :${firstCheck}"
        if [ ${res} -ne 0 ]
        then
          echo "Not all files from ${id} present, sleeping..."

          if [ ${firstCheck} -eq 1 ]
          then
             echo "first sleep 2 hour"
             ((firstCheck++))
             #echo "flag firstcheck :${firstCheck}"
             sleep $((lateDataFirstSleepTimer))

          else
                if [ ${firstCheck} -eq 2 ]
                then
                  #echo "after 2 hour send alert"
                  sendAlert "GPU-DB($(date +'%Y-%m-%d %T'))  [${id}] [${run_date}] File is not ready for following feeds" "Dear OPS Team, GPU-DB($(date +'%Y-%m-%d %T')) [${id}] [File Date: ${run_date}] File is not ready for following feeds. (First Alert After $( printf '%.3f\n' $(echo "$lateDataFirstSleepTimer/60" | bc -l)) minute(s) )"                 
                  #echo "alert result: ${alertSuccess}"
                  ((firstCheck++))
                  #echo "flag firstcheck :${firstCheck"
                  sleep $((lateDataSleepTimerDefault))

                else
                  sendAlert "GPU-DB($(date +'%Y-%m-%d %T')) [${id}] [${run_date}] File is not ready for following feeds" "Dear OPS Team, GPU-DB($(date +'%Y-%m-%d %T')) [${id}] [File Date: ${run_date}] File is not ready for following feeds. (Regular Alert Every $( printf '%.3f\n' $(echo "$lateDataSleepTimerDefault/60" | bc -l))  minute(s) )"
                  sleep $((lateDataSleepTimerDefault))

                fi
          fi
        fi
    done

done
