CREATE OR REPLACE TABLE ${table-prefix}${cms_stg-schema}.${table-prefix}cms_campaign_status_input
(
    "cmpgn_id" VARCHAR(64, dict, shard_key),
    "cmpgn_nme" VARCHAR(64, dict),
    "cmpgn_stg" VARCHAR(8, dict),
    "cmpgn_sttus" VARCHAR(8, dict),
    "cmpgn_shrt_dscrptn" VARCHAR(128, dict),
    "cmpgn_strt_on" VARCHAR(32, dict),
    "cmpgn_end_on" VARCHAR(32, dict),
    "prop_typ" VARCHAR(32, dict),
    "prop_ownr" VARCHAR(8, dict),
    "char_busnss_ownr" VARCHAR(64, dict)
)