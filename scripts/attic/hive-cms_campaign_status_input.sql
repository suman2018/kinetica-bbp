select distinct 
cmpgn_id
,cmpgn_nme
,cmpgn_stg
,cmpgn_sttus
,cmpgn_shrt_dscrptn
,cast(from_unixtime(unix_timestamp(cmpgn_strt_on, 'dd-MMM-yy HH.mm.ss.ssssss')) as string) as cmpgn_strt_on
,cast(from_unixtime(unix_timestamp(cmpgn_end_on, 'dd-MMM-yy HH.mm.ss.ssssss')) as string) as cmpgn_end_on
,prop_typ
,prop_ownr
,char_busnss_ownr
from gpu.gpu_cms_status
where datex = '${run-date=yyyyMMdd}'
