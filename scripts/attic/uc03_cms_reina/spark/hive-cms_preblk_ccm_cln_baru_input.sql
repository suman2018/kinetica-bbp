select
bigint(msisdn) as msisdn, 
dt_dt_reg as dt_dt_reg, 
int(status_reg_ori) as status_reg_ori,
simple_wording as simple_wording,
id_status as id_status,
code_status as code_status,
nik as nik,
nokk as nokk,
flag_nik as flag_nik,
flag_nokk as flag_nokk
from gpu.preblk_ccm_cln_baru
