create or replace table ${table-prefix}${cms_stg-schema}.${table-prefix}cms_preblk_ccm_cln_baru_input 
(
    "msisdn" BIGINT(shard_key),
    "dt_dt_reg" VARCHAR(32),
    "status_reg_ori" INT(dict),
    "simple_wording" VARCHAR(64, dict),
    "id_status" VARCHAR(1, dict),
    "code_status" VARCHAR(1, dict),
    "nik" VARCHAR(32),
    "nokk" VARCHAR(64),
    "flag_nik" VARCHAR(64, dict),
    "flag_nokk" VARCHAR(16, dict)
)