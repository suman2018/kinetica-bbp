DELETE FROM SCN.cb_pre_destination WHERE file_date = DATE(CURRENT_TIMESTAMP());
DELETE FROM SCN.cb_post_destination WHERE file_date = DATE(CURRENT_TIMESTAMP());
DELETE FROM SCN.sales_pre_destination WHERE file_date = DATE(CURRENT_TIMESTAMP());
DELETE FROM SCN.sales_post_destination WHERE file_date = DATE(CURRENT_TIMESTAMP());
DELETE FROM SCN.churn_pre_destination WHERE file_date = DATE(CURRENT_TIMESTAMP());
DELETE FROM SCN.churn_post_destination WHERE file_date = DATE(CURRENT_TIMESTAMP());

DELETE FROM SCN.cb_pre_netadd_vlr WHERE file_date = DATE(CURRENT_TIMESTAMP());
DELETE FROM SCN.cb_post_netadd_vlr WHERE file_date = DATE(CURRENT_TIMESTAMP());
DELETE FROM SCN.cb_post_netadd_hlr WHERE file_date = DATE(CURRENT_TIMESTAMP());

DELETE FROM SCN.cb_pre_summary WHERE file_date = DATE(CURRENT_TIMESTAMP());
DELETE FROM SCN.cb_post_summary WHERE file_date = DATE(CURRENT_TIMESTAMP());
DELETE FROM SCN.sales_pre_summary WHERE file_date = DATE(CURRENT_TIMESTAMP());
DELETE FROM SCN.sales_post_summary WHERE file_date = DATE(CURRENT_TIMESTAMP());
DELETE FROM SCN.churn_pre_summary WHERE file_date = DATE(CURRENT_TIMESTAMP());
DELETE FROM SCN.churn_post_summary WHERE file_date = DATE(CURRENT_TIMESTAMP());


/* reprocessing for high-level (history) tables */
DELETE FROM UC1_HISTORY.scn_olap_hlr WHERE file_date = DATE(CURRENT_TIMESTAMP());
DELETE FROM UC1_HISTORY.scn_cluster_osdss WHERE file_date = DATE(CURRENT_TIMESTAMP());
DELETE FROM UC1_HISTORY.subscriber_cluster_osdss WHERE file_date = DATE(CURRENT_TIMESTAMP());
DELETE FROM UC1_HISTORY.revenue_scn_enterprise_pre WHERE file_date = DATE(CURRENT_TIMESTAMP());
