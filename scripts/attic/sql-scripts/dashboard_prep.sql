/* Summary */
	
CREATE OR REPLACE VIEW DASHBOARD.tmp_db_summary_input AS (
SELECT 
	'Revenue' as domain,
	CASE length(char4(month(revenue_date))) WHEN 1 THEN (char16((char4(year(revenue_date))||'-0'||char4(month(revenue_date))))) ELSE (char16((char4(year(revenue_date))||'-'||char4(month(revenue_date))))) END AS date_month,
	revenue_date as date, area, region, total_revenue as total
FROM 
	revenue_cluster_priceplan
UNION ALL
SELECT 
	'Recharge' as domain,
	CASE length(char4(month(recharge_date))) WHEN 1 THEN (char16((char4(year(recharge_date))||'-0'||char4(month(recharge_date))))) ELSE (char16((char4(year(recharge_date))||'-'||char4(month(recharge_date))))) END AS date_month,
	recharge_date as date, area, region, total_recharge as total
FROM 
	recharge_cluster_bnumber
UNION ALL
SELECT 
	'Sales' as domain,
	CASE length(char4(month(SCN_DATE))) WHEN 1 THEN (char16((char4(year(SCN_DATE))||'-0'||char4(month(SCN_DATE))))) ELSE (char16((char4(year(SCN_DATE))||'-'||char4(month(SCN_DATE))))) END AS date_month,
	SCN_DATE as date, area, region_name as region, SALES as total
FROM 
	scn_cluster_osdss
UNION ALL
SELECT 
	'Churn' as domain,
	CASE length(char4(month(SCN_DATE))) WHEN 1 THEN (char16((char4(year(SCN_DATE))||'-0'||char4(month(SCN_DATE))))) ELSE (char16((char4(year(SCN_DATE))||'-'||char4(month(SCN_DATE))))) END AS date_month,
	SCN_DATE as date, area, region_name as region, CHURN as total
FROM 
	scn_cluster_osdss
UNION ALL
SELECT 
	'Netadd' as domain,
	CASE length(char4(month(SCN_DATE))) WHEN 1 THEN (char16((char4(year(SCN_DATE))||'-0'||char4(month(SCN_DATE))))) ELSE (char16((char4(year(SCN_DATE))||'-'||char4(month(SCN_DATE))))) END AS date_month,
	SCN_DATE as date, area, region_name as region, NETADD as total
FROM 
	scn_cluster_osdss);

CREATE OR REPLACE MATERIALIZED VIEW DASHBOARD.db_summary AS (
	SELECT 
		domain, date_month, date, area, region, sum(total) as total
	FROM 
		DASHBOARD.tmp_db_summary_input
	GROUP BY
		domain, date_month, date, area, region
);

/* Revenue */
CREATE OR REPLACE MATERIALIZED VIEW DASHBOARD.db_revenue_cluster_priceplan AS
SELECT 
	CASE length(char4(month(revenue_date))) WHEN 1 THEN (char16((char4(year(revenue_date))||'-0'||char4(month(revenue_date))))) ELSE (char16((char4(year(revenue_date))||'-'||char4(month(revenue_date))))) END AS date_month,
	*
FROM 
	revenue_cluster_priceplan;
	
	
CREATE OR REPLACE TABLE DASHBOARD.db_revenue_post_summary AS (
	SELECT 
		revenue_post.*, lacci.*
	FROM
		revenue_post_summary revenue_post
	JOIN
		laccima_ref lacci
		ON
			revenue_post.lacci_id = lacci.lacci_id
);


/* Recharge */
CREATE OR REPLACE MATERIALIZED VIEW DASHBOARD.db_recharge_cluster_bnumber AS
SELECT
	CASE length(char4(month(recharge_date))) WHEN 1 THEN (char16((char4(year(recharge_date))||'-0'||char4(month(recharge_date))))) ELSE (char16((char4(year(recharge_date))||'-'||char4(month(recharge_date))))) END AS year_month,
	DATE('2000-' || '01-' || CASE length(char4(day(recharge_date))) WHEN 1 THEN ('0'||char4(day(recharge_date))) ELSE (char4(day(recharge_date))) END) AS day_of_month, 
	recharge_date as date, area, region, total_recharge as total
FROM 
	recharge_cluster_bnumber;

CREATE OR REPLACE MATERIALIZED VIEW DASHBOARD.db_recharge_cluster_anumber AS
SELECT
	CASE length(char4(month(recharge_date))) WHEN 1 THEN (char16((char4(year(recharge_date))||'-0'||char4(month(recharge_date))))) ELSE (char16((char4(year(recharge_date))||'-'||char4(month(recharge_date))))) END AS year_month,
	DATE('2000-' || '01-' || CASE length(char4(day(recharge_date))) WHEN 1 THEN ('0'||char4(day(recharge_date))) ELSE (char4(day(recharge_date))) END) AS day_of_month, 
	recharge_date as date, area, region, cluster_name as cluster, product, recharge_channel as channel, trans_type, trans_channel, total_recharge as total
FROM 
	recharge_cluster_anumber;
	

/* geo spatial*/
create or replace table DASHBOARD.mkios_geo as ( 
select
	mkios.*, lac.lngtd, lac.lttde 
from
	mkios_destination_v mkios
join 
	laccima_ref lac
on
	lac.lacci_id = mkios.subs_lacci_id
);

create or replace table DASHBOARD.recharge_cluster_bnumber_agg as(
select 
	max(area) as area, max(region) as region, cluster_name as cluster, 
	sum(voucher_denomination) as sum_voucher,
	sum(total_transaction) as sum_transaction,
	sum(total_recharge) as sum_recharge
from 
	recharge_cluster_bnumber
group by
	cluster_name
);

create or replace table DASHBOARD.db_recharge_cluster_bnumber_geo as (
select
	r.cluster, r.sum_voucher, r.sum_transaction, r.sum_recharge, 
	case
		when (r.sum_recharge < 800000000000) then 'low'
		when (r.sum_recharge < 1155000000000) then 'medium'
		when (r.sum_recharge >= 1155000000000) then 'high'
	end as recharge_cat,
	c.subbranch, c.branch, c.region, c.area, c.wkt
from 
	recharge_cluster_bnumber_agg r
join 
	cluster c
on
	r.cluster = c.cluster
);