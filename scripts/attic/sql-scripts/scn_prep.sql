/*
 assumes staging tables to be present (from kineticaloader): cb_pre_staging, cb_post_staging, sales_pre_staging, sales_post_staging, churn_pre_staging, churn_post_staging 
 assumes destination tables to be present (from revenue_init.sql): cb_pre_destination, cb_post_destination, sales_pre_destination, sales_post_destination, churn_pre_destination, churn_post_destination
 for all scn staging tables:
	copy batch (t) from staging table to destination table
		set fileDate to current date (t)
	truncate destination table (t-2)
	delete staging table
For performance reasons do the above steps without insert into statements, but via temporary tables and unions.
*/

CREATE OR REPLACE TABLE SCN.cb_pre_tmpDestination AS (
	SELECT
		LACCI_ID, CURRENT_DATE AS FILE_DATE, TRX_DATE, PAYCHANNEL, SUBS_STATUS, ACTIVATION_DATE,PREFIX, AREA_HLR, REGION_HLR, CITY_HLR, PRICE_PLAN_ID, 
		OFFER, BRAND, CUST_TYPE_DESC, CUST_SUBTYPE_DESC, HYBRID_FLAG, DO_DATE, DEALER_CODE, ITEM_CODE, 
		DIST_TYPE, LAC, CI, NODE, LCTN_CLOSING_FLAG, ki_shard_key(LACCI_ID),
		count(*) AS CNT
	FROM
		SCN.cb_pre_staging
	WHERE LACCI_ID <> -99
	GROUP BY
		LACCI_ID, TRX_DATE, PAYCHANNEL, SUBS_STATUS, ACTIVATION_DATE, PREFIX, AREA_HLR, REGION_HLR, CITY_HLR, PRICE_PLAN_ID, 
		OFFER, BRAND, CUST_TYPE_DESC, CUST_SUBTYPE_DESC, HYBRID_FLAG, DO_DATE, DEALER_CODE, ITEM_CODE, 
		DIST_TYPE, LAC, CI, NODE, LCTN_CLOSING_FLAG 
	UNION ALL
	SELECT
		LACCI_ID, FILE_DATE, TRX_DATE, PAYCHANNEL, SUBS_STATUS, ACTIVATION_DATE,PREFIX, AREA_HLR, REGION_HLR, CITY_HLR, PRICE_PLAN_ID, 
		OFFER, BRAND, CUST_TYPE_DESC, CUST_SUBTYPE_DESC, HYBRID_FLAG, DO_DATE, DEALER_CODE, ITEM_CODE, 
		DIST_TYPE, LAC, CI, NODE, LCTN_CLOSING_FLAG, ki_shard_key(LACCI_ID), CNT
	FROM
		SCN.cb_pre_destination
	WHERE FILE_DATE = CURRENT_DATE - INTERVAL 1 DAY
);
DROP TABLE SCN.cb_pre_destination;
ALTER TABLE SCN.cb_pre_tmpDestination RENAME TO cb_pre_destination;


CREATE OR REPLACE TABLE SCN.cb_post_tmpDestination AS (
	SELECT * FROM SCN.cb_post_destination
	UNION ALL
	SELECT *, CURRENT_DATE() FROM SCN.cb_post_staging
);
DROP TABLE SCN.cb_post_destination;
ALTER TABLE SCN.cb_post_tmpDestination RENAME TO cb_post_destination;
DELETE FROM SCN.cb_post_destination WHERE file_date = DATE(CURRENT_TIMESTAMP() - INTERVAL 2 DAY);

CREATE OR REPLACE TABLE SCN.sales_pre_tmpDestination AS (
	SELECT * FROM SCN.sales_pre_destination
	UNION ALL
	SELECT *, CURRENT_DATE() FROM SCN.sales_pre_staging
);
DROP TABLE SCN.sales_pre_destination;
ALTER TABLE SCN.sales_pre_tmpDestination RENAME TO sales_pre_destination;
DELETE FROM SCN.sales_pre_destination WHERE file_date = DATE(CURRENT_TIMESTAMP() - INTERVAL 2 DAY);

CREATE OR REPLACE TABLE SCN.sales_post_tmpDestination AS (
	SELECT * FROM SCN.sales_post_destination
	UNION ALL
	SELECT *, CURRENT_DATE() FROM SCN.sales_post_staging
);
DROP TABLE SCN.sales_post_destination;
ALTER TABLE SCN.sales_post_tmpDestination RENAME TO sales_post_destination;
DELETE FROM SCN.sales_post_destination WHERE file_date = DATE(CURRENT_TIMESTAMP() - INTERVAL 2 DAY);

CREATE OR REPLACE TABLE SCN.churn_pre_tmpDestination AS (
	SELECT * FROM SCN.churn_pre_destination
	UNION ALL
	SELECT *, CURRENT_DATE() FROM SCN.churn_pre_staging
);
DROP TABLE SCN.churn_pre_destination;
ALTER TABLE SCN.churn_pre_tmpDestination RENAME TO churn_pre_destination;
DELETE FROM SCN.churn_pre_destination WHERE file_date = DATE(CURRENT_TIMESTAMP() - INTERVAL 2 DAY);

CREATE OR REPLACE TABLE SCN.churn_post_tmpDestination AS (
	SELECT * FROM SCN.churn_post_destination
	UNION ALL
	SELECT *, CURRENT_DATE() FROM SCN.churn_post_staging
);
DROP TABLE SCN.churn_post_destination;
ALTER TABLE SCN.churn_post_tmpDestination RENAME TO churn_post_destination;
DELETE FROM SCN.churn_post_destination WHERE file_date = DATE(CURRENT_TIMESTAMP() - INTERVAL 2 DAY);

-- DROP TABLE cb_pre_staging;
-- DROP TABLE cb_post_staging;
-- DROP TABLE sales_pre_staging;
-- DROP TABLE sales_post_staging;
-- DROP TABLE churn_pre_staging;
-- DROP TABLE churn_post_staging;
