/* setup all destination and summary tables for scn pipeline */

-- cb_pre_destination
-- cb_post_destination
-- sales_pre_destination
-- sales_post_destination
-- churn_pre_destination
-- churn_post_destination

-- cb_pre_netadd_vlr  	 
-- cb_post_netadd_vlr 
-- cb_post_netadd_hlr
-- cb_pre_summary
-- cb_post_summary
-- sales_pre_summary
-- sales_post_summary
-- churn_pre_summary
-- churn_post_summary


create or replace table SCN.cb_pre_destination(
   TRX_DATE          timestamp        
  ,SUBS_ID           integer          
  ,MSISDN            bigint           
  ,PAYCHANNEL        varchar(8, dict) 
  ,SUBS_STATUS       varchar(8, dict) 
  ,ACTIVATION_DATE   timestamp        
  ,PREFIX            integer          
  ,AREA_HLR          varchar(16, dict)
  ,REGION_HLR        varchar(32, dict)
  ,CITY_HLR          varchar(32, dict)
  ,PRICE_PLAN_ID     integer          
  ,OFFER             varchar(64, dict)
  ,BRAND             varchar(16, dict)
  ,CUST_TYPE_DESC    varchar(32, dict)
  ,CUST_SUBTYPE_DESC varchar(32, dict)
  ,HYBRID_FLAG       varchar(8, dict) 
  ,DO_DATE           varchar(16, dict)
  ,DEALER_CODE       varchar(8, dict) 
  ,ITEM_CODE         varchar(16, dict)
  ,DIST_TYPE         varchar(16, dict)
  ,LACCI_ID          integer          
  ,LAC               integer          
  ,CI                varchar(16, dict)
  ,CGI_PRE           bigint           
  ,NODE              varchar(8, dict) 
  ,LCTN_CLOSING_FLAG varchar(8, dict) 
  ,FILE_DATE         timestamp
  ,CNT BIGINT NOT NULL
);

create or replace table SCN.cb_post_destination(
   TRX_DATE               timestamp        
  ,SUBS_ID                integer          
  ,MSISDN                 bigint           
  ,ACCOUNT_ID             integer          
  ,SUBS_STATUS            varchar(8, dict) 
  ,ACTIVATION_DATE        timestamp        
  ,ORIGIN_ACTIVATION_DATE timestamp        
  ,PREFIX                 integer          
  ,AREA_HLR               varchar(16, dict)
  ,REGION_HLR             varchar(32, dict)
  ,CITY_HLR               varchar(32, dict)
  ,PRICE_PLAN_ID          integer          
  ,OFFER                  varchar(64, dict)
  ,BRAND                  varchar(16, dict)
  ,BILL_CYCLE             varchar(8, dict) 
  ,CUST_TYPE              varchar(1, dict) 
  ,CUST_TYPE_DESC         varchar(32, dict)
  ,CUST_SUBTYPE           varchar(1, dict) 
  ,CUST_SUBTYPE_DESC      varchar(32, dict)
  ,HYBRID_FLAG            varchar(8, dict) 
  ,PRE_TO_POST_FLAG       varchar(8, dict) 
  ,LACCI_ID               integer          
  ,LAC                    integer          
  ,CI                     integer          
  ,CGI_POST               varchar(32, dict)
  ,NODE                   varchar(8, dict) 
  ,LCTN_CLOSING_FLAG      varchar(8, dict)
  ,FILE_DATE              timestamp 
);

create or replace table SCN.sales_pre_destination(
   ACTIVATION_DATE   timestamp        
  ,SUBS_ID           integer          
  ,MSISDN            bigint           
  ,PAYCHANNEL        varchar(8, dict) 
  ,PREFIX            integer          
  ,AREA_HLR          varchar(16, dict)
  ,REGION_HLR        varchar(32, dict)
  ,CITY_HLR          varchar(32, dict)
  ,PRICE_PLAN_ID     integer          
  ,OFFER             varchar(64, dict)
  ,BRAND             varchar(16, dict)
  ,CUST_TYPE_DESC    varchar(32, dict)
  ,DO_DATE           varchar(16, dict)
  ,DEALER_CODE       varchar(8, dict) 
  ,ITEM_CODE         varchar(16, dict)
  ,DIST_TYPE         varchar(16, dict)
  ,FIRST_USAGE_DATE  timestamp        
  ,LACCI_ID          integer          
  ,LAC               integer          
  ,CI                integer          
  ,CGI_PREPAID       varchar(32, dict)
  ,NODE              varchar(8, dict) 
  ,LCTN_CLOSING_FLAG varchar(8, dict)
  ,FILE_DATE         timestamp 
);

create or replace table SCN.sales_post_destination(
   ACTIVATION_DATE       timestamp        
  ,SUBS_ID               integer          
  ,MSISDN                bigint           
  ,ACCOUNT_ID            integer          
  ,PREFIX                integer          
  ,AREA_HLR              varchar(16, dict)
  ,REGION_HLR            varchar(32, dict)
  ,CITY_HLR              varchar(32, dict)
  ,PRICE_PLAN_ID         integer          
  ,OFFER                 varchar(64, dict)
  ,BRAND                 varchar(16, dict)
  ,BILL_CYCLE            integer          
  ,CUST_TYPE             varchar(1, dict) 
  ,CUST_TYPE_DESC        varchar(32, dict)
  ,CUST_SUBTYPE          varchar(1, dict) 
  ,CUST_SUBTYPE_DESC     varchar(32, dict)
  ,PRE_TO_POST_FLAG      varchar(8, dict) 
  ,CUST_TYPE_CHANGE_FLAG varchar(8, dict) 
  ,PREV_CUST_TYPE        varchar(8, dict) 
  ,PREV_CUST_SUBTYPE     varchar(8, dict) 
  ,OWNERSHIP_CHANGE_FLAG varchar(8, dict) 
  ,FIRST_USAGE_DATE      timestamp        
  ,LACCI_ID              integer          
  ,LAC                   integer          
  ,CI                    integer          
  ,CGI_POSTPAID          varchar(32, dict)
  ,NODE                  varchar(8, dict) 
  ,LCTN_CLOSING_FLAG     varchar(8, dict)
  ,FILE_DATE             timestamp 
);

create or replace table SCN.churn_pre_destination(
   DEACTIVATION_DATE timestamp        
  ,SUBS_ID           integer          
  ,MSISDN            bigint           
  ,PAYCHANNEL        varchar(8, dict) 
  ,PREFIX            integer          
  ,AREA_HLR          varchar(16, dict)
  ,REGION_HLR        varchar(32, dict)
  ,CITY_HLR          varchar(32, dict)
  ,PRICE_PLAN_ID     integer          
  ,OFFER             varchar(64, dict)
  ,BRAND             varchar(16, dict)
  ,CUST_TYPE_DESC    varchar(32, dict)
  ,DO_DATE           varchar(16, dict)
  ,DEALER_CODE       varchar(8, dict) 
  ,ITEM_CODE         varchar(16, dict)
  ,DIST_TYPE         varchar(16, dict)
  ,LAST_USAGE_DATE   timestamp        
  ,LACCI_ID          integer          
  ,LAC               integer          
  ,CI                varchar(16, dict)
  ,CGI_PRE           bigint           
  ,NODE              varchar(8, dict) 
  ,LCTN_CLOSING_FLAG varchar(8, dict)
  ,FILE_DATE         timestamp 
);

create or replace table SCN.churn_post_destination(
   DEACTIVATION_DATE     timestamp        
  ,SUBS_ID               integer          
  ,MSISDN                bigint           
  ,ACCOUNT_ID            integer          
  ,PREFIX                integer          
  ,AREA_HLR              varchar(16, dict)
  ,REGION_HLR            varchar(32, dict)
  ,CITY_HLR              varchar(32, dict)
  ,PRICE_PLAN_ID         integer          
  ,OFFER                 varchar(64, dict)
  ,BRAND                 varchar(16, dict)
  ,BILL_CYCLE            integer          
  ,CUST_TYPE             varchar(1, dict) 
  ,CUST_TYPE_DESC        varchar(32, dict)
  ,CUST_SUBTYPE          varchar(1, dict) 
  ,CUST_SUBTYPE_DESC     varchar(32, dict)
  ,CUST_TYPE_CHANGE_FLAG varchar(8, dict) 
  ,NEXT_CUST_TYPE        varchar(8, dict) 
  ,NEXT_CUST_SUBTYPE     varchar(8, dict) 
  ,OWNERSHIP_CHANGE_FLAG varchar(8, dict) 
  ,LAST_USAGE_DATE       timestamp        
  ,LACCI_ID              integer          
  ,LAC                   integer          
  ,CI                    integer          
  ,CGI_POST              varchar(32, dict)
  ,NODE                  varchar(8, dict) 
  ,FILE_DATE             timestamp
);

CREATE OR REPLACE TABLE SCN.cb_pre_netadd_vlr(
   AREA_HLR      varchar(16, dict)    
  ,REGION_HLR    varchar(32, dict)
  ,BRAND         varchar(16, dict)     
  ,PRICE_PLAN_ID integer        
  ,OFFER         varchar(64, dict)        
  ,TRX_DATE      timestamp        
  ,FILE_DATE     timestamp      
  ,CURRENT_CNT   bigint
  ,PREVIOUS_CNT  bigint         
  ,DELTA         bigint
);

CREATE OR REPLACE TABLE SCN.cb_post_netadd_vlr(
   AREA_HLR      varchar(16, dict)    
  ,REGION_HLR    varchar(32, dict)
  ,BRAND         varchar(16, dict)    
  ,PRICE_PLAN_ID integer        
  ,OFFER         varchar(64, dict)    
  ,TRX_DATE      timestamp      
  ,FILE_DATE     timestamp      
  ,CURRENT_CNT   bigint
  ,PREVIOUS_CNT  bigint         
  ,DELTA         bigint   
);

CREATE OR REPLACE TABLE SCN.cb_post_netadd_hlr(
   BRAND         varchar(16, dict)    
  ,PRICE_PLAN_ID integer        
  ,OFFER         varchar(64, dict)    
  ,AREA_HLR      varchar(16, dict)    
  ,REGION_HLR    varchar(32, dict)    
  ,CITY_HLR      varchar(32, dict)    
  ,TRX_DATE      timestamp      
  ,FILE_DATE     timestamp      
  ,CURRENT_CNT   bigint
  ,PREVIOUS_CNT  bigint         
  ,DELTA         bigint 
);

create or replace table SCN.cb_pre_summary(
   FILE_DATE         timestamp      
  ,TRX_DATE          timestamp      
  ,PAYCHANNEL        varchar(8, dict)     
  ,SUBS_STATUS       varchar(8, dict)     
  ,ACTIVATION_DATE   timestamp      
  ,PREFIX            integer        
  ,AREA_HLR          varchar(16, dict)    
  ,REGION_HLR        varchar(32, dict)    
  ,CITY_HLR          varchar(32, dict)    
  ,PRICE_PLAN_ID     integer        
  ,OFFER             varchar(64, dict)    
  ,BRAND             varchar(16, dict)    
  ,CUST_TYPE_DESC    varchar(32, dict)    
  ,CUST_SUBTYPE_DESC varchar(32, dict)    
  ,HYBRID_FLAG       varchar(8, dict)     
  ,DO_DATE           varchar(16, dict)    
  ,DEALER_CODE       varchar(8, dict)     
  ,ITEM_CODE         varchar(16, dict)    
  ,DIST_TYPE         varchar(16, dict)    
  ,LAC               integer        
  ,CI                varchar(16, dict)    
  ,NODE              varchar(8, dict)     
  ,LCTN_CLOSING_FLAG varchar(8, dict)
  ,AREA                   varchar(8, dict)     
  ,REGION                 varchar(32, dict)    
  ,BRANCH                 varchar(64, dict)    
  ,SUBBRANCH              varchar(32, dict)    
  ,CLUSTER                varchar(32, dict)    
  ,KABUPATEN              varchar(32, dict)    
  ,KECAMATAN              varchar(32, dict)    
  ,KELURAHAN              varchar(32, dict)
  ,CNT               bigint
);

create or replace table SCN.cb_post_summary(
   FILE_DATE              timestamp      
  ,TRX_DATE               timestamp      
  ,SUBS_STATUS            varchar(8, dict)     
  ,ACTIVATION_DATE        timestamp      
  ,ORIGIN_ACTIVATION_DATE timestamp      
  ,PREFIX                 integer        
  ,AREA_HLR               varchar(16, dict)    
  ,REGION_HLR             varchar(32, dict)    
  ,CITY_HLR               varchar(32, dict)    
  ,PRICE_PLAN_ID          integer        
  ,OFFER                  varchar(64, dict)    
  ,BRAND                  varchar(16, dict)    
  ,BILL_CYCLE             varchar(8, dict)     
  ,CUST_TYPE              varchar(1, dict)     
  ,CUST_TYPE_DESC         varchar(32, dict)    
  ,CUST_SUBTYPE           varchar(1, dict)     
  ,CUST_SUBTYPE_DESC      varchar(32, dict)    
  ,HYBRID_FLAG            varchar(8, dict)     
  ,PRE_TO_POST_FLAG       varchar(8, dict)     
  ,LAC                    integer        
  ,CI                     integer        
  ,NODE                   varchar(8, dict)     
  ,LCTN_CLOSING_FLAG      varchar(8, dict)
  ,AREA                   varchar(8, dict)     
  ,REGION                 varchar(32, dict)    
  ,BRANCH                 varchar(64, dict)    
  ,SUBBRANCH              varchar(32, dict)    
  ,CLUSTER                varchar(32, dict)    
  ,KABUPATEN              varchar(32, dict)    
  ,KECAMATAN              varchar(32, dict)    
  ,KELURAHAN              varchar(32, dict)
  ,CNT                    bigint
);

create or replace table SCN.sales_pre_summary(
   FILE_DATE         timestamp      
  ,ACTIVATION_DATE   timestamp      
  ,PAYCHANNEL        varchar(8, dict)     
  ,PREFIX            integer        
  ,AREA_HLR          varchar(16, dict)    
  ,REGION_HLR        varchar(32, dict)    
  ,CITY_HLR          varchar(32, dict)    
  ,PRICE_PLAN_ID     integer        
  ,OFFER             varchar(64, dict)    
  ,BRAND             varchar(16, dict)    
  ,CUST_TYPE_DESC    varchar(32, dict)    
  ,DO_DATE           varchar(16, dict)    
  ,DEALER_CODE       varchar(8, dict)     
  ,ITEM_CODE         varchar(16, dict)    
  ,DIST_TYPE         varchar(16, dict)    
  ,FIRST_USAGE_DATE  timestamp      
  ,LAC               integer        
  ,CI                integer        
  ,NODE              varchar(8, dict)     
  ,LCTN_CLOSING_FLAG varchar(8, dict)
  ,AREA                   varchar(8, dict)     
  ,REGION                 varchar(32, dict)    
  ,BRANCH                 varchar(64, dict)    
  ,SUBBRANCH              varchar(32, dict)    
  ,CLUSTER                varchar(32, dict)    
  ,KABUPATEN              varchar(32, dict)    
  ,KECAMATAN              varchar(32, dict)    
  ,KELURAHAN              varchar(32, dict)
  ,CNT               bigint
);

create or replace table SCN.sales_post_summary(
   FILE_DATE             timestamp      
  ,ACTIVATION_DATE       timestamp      
  ,PREFIX                integer        
  ,AREA_HLR              varchar(16, dict)    
  ,REGION_HLR            varchar(32, dict)    
  ,CITY_HLR              varchar(32, dict)    
  ,PRICE_PLAN_ID         integer        
  ,OFFER                 varchar(64, dict)    
  ,BRAND                 varchar(16, dict)    
  ,BILL_CYCLE            integer        
  ,CUST_TYPE             varchar(1, dict)     
  ,CUST_TYPE_DESC        varchar(32, dict)    
  ,CUST_SUBTYPE          varchar(1, dict)     
  ,CUST_SUBTYPE_DESC     varchar(32, dict)    
  ,PRE_TO_POST_FLAG      varchar(8, dict)     
  ,CUST_TYPE_CHANGE_FLAG varchar(8, dict)     
  ,PREV_CUST_TYPE        varchar(8, dict)     
  ,PREV_CUST_SUBTYPE     varchar(8, dict)     
  ,OWNERSHIP_CHANGE_FLAG varchar(8, dict)     
  ,FIRST_USAGE_DATE      timestamp      
  ,LAC                   integer        
  ,CI                    integer        
  ,NODE                  varchar(8, dict)     
  ,LCTN_CLOSING_FLAG     varchar(8, dict)
  ,AREA                   varchar(8, dict)     
  ,REGION                 varchar(32, dict)    
  ,BRANCH                 varchar(64, dict)    
  ,SUBBRANCH              varchar(32, dict)    
  ,CLUSTER                varchar(32, dict)    
  ,KABUPATEN              varchar(32, dict)    
  ,KECAMATAN              varchar(32, dict)    
  ,KELURAHAN              varchar(32, dict)
  ,CNT                   bigint
);

create or replace table SCN.churn_pre_summary(
   FILE_DATE         timestamp      
  ,DEACTIVATION_DATE timestamp      
  ,PAYCHANNEL        varchar(8, dict)     
  ,PREFIX            integer        
  ,AREA_HLR          varchar(16, dict)    
  ,REGION_HLR        varchar(32, dict)    
  ,CITY_HLR          varchar(32, dict)    
  ,PRICE_PLAN_ID     integer        
  ,OFFER             varchar(64, dict)    
  ,BRAND             varchar(16, dict)    
  ,CUST_TYPE_DESC    varchar(32, dict)    
  ,DO_DATE           varchar(16, dict)    
  ,DEALER_CODE       varchar(8, dict)     
  ,ITEM_CODE         varchar(16, dict)    
  ,DIST_TYPE         varchar(16, dict)    
  ,LAST_USAGE_DATE   timestamp      
  ,LAC               integer        
  ,CI                varchar(16, dict)    
  ,NODE              varchar(8, dict)
  ,AREA                   varchar(8, dict)     
  ,REGION                 varchar(32, dict)    
  ,BRANCH                 varchar(64, dict)    
  ,SUBBRANCH              varchar(32, dict)    
  ,CLUSTER                varchar(32, dict)    
  ,KABUPATEN              varchar(32, dict)    
  ,KECAMATAN              varchar(32, dict)    
  ,KELURAHAN              varchar(32, dict)
  ,CNT               bigint
);

create or replace table SCN.churn_post_summary(
   FILE_DATE             timestamp      
  ,DEACTIVATION_DATE     timestamp      
  ,PREFIX                integer        
  ,AREA_HLR              varchar(16, dict)    
  ,REGION_HLR            varchar(32, dict)    
  ,CITY_HLR              varchar(32, dict)    
  ,PRICE_PLAN_ID         integer        
  ,OFFER                 varchar(64, dict)    
  ,BRAND                 varchar(16, dict)    
  ,BILL_CYCLE            integer        
  ,CUST_TYPE             varchar(1, dict)     
  ,CUST_TYPE_DESC        varchar(32, dict)    
  ,CUST_SUBTYPE          varchar(1, dict)     
  ,CUST_SUBTYPE_DESC     varchar(32, dict)    
  ,CUST_TYPE_CHANGE_FLAG varchar(8, dict)     
  ,NEXT_CUST_TYPE        varchar(8, dict)     
  ,NEXT_CUST_SUBTYPE     varchar(8, dict)     
  ,OWNERSHIP_CHANGE_FLAG varchar(8, dict)     
  ,LAST_USAGE_DATE       timestamp      
  ,LAC                   integer        
  ,CI                    integer        
  ,NODE                  varchar(8, dict)
  ,AREA                   varchar(8, dict)     
  ,REGION                 varchar(32, dict)    
  ,BRANCH                 varchar(64, dict)    
  ,SUBBRANCH              varchar(32, dict)    
  ,CLUSTER                varchar(32, dict)    
  ,KABUPATEN              varchar(32, dict)    
  ,KECAMATAN              varchar(32, dict)    
  ,KELURAHAN              varchar(32, dict)
  ,CNT                   bigint
);

/* add default file_date for history table reprocessing */
ALTER TABLE scn_olap_hlr ADD file_date timestamp;
UPDATE scn_olap_hlr SET file_date = DATE('2000-01-01') WHERE file_date IS NULL;
ALTER TABLE scn_cluster_osdss ADD file_date timestamp;
UPDATE scn_cluster_osdss SET file_date = DATE('2000-01-01') WHERE file_date IS NULL;
ALTER TABLE subscriber_cluster_osdss ADD file_date timestamp;
UPDATE subscriber_cluster_osdss SET file_date = DATE('2000-01-01') WHERE file_date IS NULL;
ALTER TABLE revenue_scn_enterprise_pre ADD file_date timestamp;
UPDATE revenue_scn_enterprise_pre SET file_date = DATE('2000-01-01') WHERE file_date IS NULL;

