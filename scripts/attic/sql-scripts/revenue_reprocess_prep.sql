DELETE FROM REVENUE.revenue_post_hlr_summary WHERE file_date = DATE(CURRENT_TIMESTAMP());
DELETE FROM REVENUE.revenue_pre_hlr_summary WHERE file_date = DATE(CURRENT_TIMESTAMP());
DELETE FROM REVENUE.revenue_pre_summary WHERE file_date = DATE(CURRENT_TIMESTAMP());

/* reprocessing for high-level (history) tables */
DELETE FROM UC1_HISTORY.revenue_pos_cust_type_osdss WHERE file_date = DATE(CURRENT_TIMESTAMP());
DELETE FROM UC1_HISTORY.revenue_postpaid_hlr WHERE file_date = DATE(CURRENT_TIMESTAMP());
DELETE FROM UC1_HISTORY.revenue_cluster_priceplan WHERE file_date = DATE(CURRENT_TIMESTAMP());