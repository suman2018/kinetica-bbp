DELETE FROM RECHARGE.recharge_summary WHERE file_date = DATE(CURRENT_TIMESTAMP());

/* reprocessing for high-level (history) tables */
DELETE FROM UC1_HISTORY.recharge_cluster_anumber WHERE file_date = DATE(CURRENT_TIMESTAMP());
DELETE FROM UC1_HISTORY.recharge_cluster_bnumber WHERE file_date = DATE(CURRENT_TIMESTAMP());