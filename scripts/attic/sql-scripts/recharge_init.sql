/* setup destination and summary tables for recharge pipeline */

-- recharge_summary
create or replace table RECHARGE.recharge_summary(
   BNUM_CLUSTER        varchar(32, shard_key, dict)
  ,CHANNEL             varchar(64, shard_key, dict)
  ,FILE_DATE           timestamp             
  ,TRX_DATE            timestamp             
  ,BRAND               varchar(16, dict)
  ,PRICE_PLAN          varchar(32, dict)           
  ,BNUM_AREA_SALES     varchar(8, dict)           
  ,BNUM_REGION_SALES   varchar(32, dict)           
  ,BNUM_BRANCH         varchar(64, dict)           
  ,BNUM_SUBBRANCH      varchar(32, dict)                      
  ,BNUM_KABUPATEN      varchar(32, dict)           
  ,BNUM_KECAMATAN      varchar(32, dict)           
  ,BNUM_NODE_TYPE      varchar(32, dict)           
  ,CUST_TYPE           varchar(32, dict)           
  ,RECH_TYPE           varchar(8, dict)                       
  ,RS_NODE_TYPE        varchar(32, dict)           
  ,RS_AREA_SALES       varchar(8, dict)            
  ,RS_REGION_SALES     varchar(32, dict)           
  ,RS_CLUSTER          varchar(32, dict)
  ,RS_BRANCH           varchar(64, dict)           
  ,RS_SUBBRANCH        varchar(32, dict)           
  ,RS_KABUPATEN        varchar(32, dict)           
  ,RS_KECAMATAN        varchar(32, dict)           
  ,CHANNEL_GROUP       varchar(32, dict)           
  ,CHANNEL_CATEGORY    varchar(32, dict)                      
  ,MERCHANT            varchar(32, dict)           
  ,DEALER_CODE         varchar(32, dict)           
  ,DEALER_AREA_SALES   varchar(32, dict)           
  ,DEALER_REGION_SALES varchar(32, dict)           
  ,DEALER_BRANCH       varchar(32, dict)           
  ,DEALER_SUBBRANCH    varchar(32, dict)           
  ,DEALER_CLUSTER      varchar(32, dict)           
  ,LACCI_BORDER_FLAG   varchar(32, dict)           
  ,ACCESS_TYPE         integer               
  ,STOCK               varchar(32, dict)
  ,DENOM               double                
  ,SUM_TRX_RECH        bigint                
  ,SUM_RECH            bigint     
);


/* add default file_date for history table reprocessing */
ALTER TABLE recharge_cluster_anumber ADD file_date timestamp;
UPDATE recharge_cluster_anumber SET file_date = DATE('2000-01-01') WHERE file_date IS NULL;
ALTER TABLE recharge_cluster_bnumber ADD file_date timestamp;
UPDATE recharge_cluster_bnumber SET file_date = DATE('2000-01-01') WHERE file_date IS NULL;
