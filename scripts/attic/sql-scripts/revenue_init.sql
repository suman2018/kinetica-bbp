/* setup destination and summary tables for revenue pipeline */

-- revenue_summary
-- revenue_post_summary

CREATE OR REPLACE TABLE REVENUE.revenue_pre_summary(
   FILE_DATE             date             
  ,TRX_DATE              date             
  ,BRAND                 varchar(16, dict)           
  ,OFFER_ID              varchar(8, dict)            
  ,AREA_SALES            varchar(8, dict)            
  ,REGION_SALES          varchar(32, dict)           
  ,BRANCH                varchar(64, dict)           
  ,SUBBRANCH             varchar(32, dict)           
  ,CLUSTER               varchar(32, dict)           
  ,PROVINSI              varchar(32, dict)           
  ,KABUPATEN             varchar(32, dict)
  ,KECAMATAN             varchar(32, dict)           
  ,KELURAHAN             varchar(32, shard_key)           
  ,ACTIVATION_CHANNEL_ID varchar(8, dict)            
  ,NODE_TYPE             varchar(8, dict)            
  ,PRE_POST_FLAG         integer               
  ,L1_NAME               varchar(32, dict)           
  ,L2_NAME               varchar(32, dict)           
  ,L3_NAME               varchar(32, dict)           
  ,L4_NAME               varchar(64, dict)
  ,CUST_TYPE             varchar(16, dict)           
  ,CUST_SUBTYPE          varchar(8, dict)            
  ,VOL                   bigint                
  ,DUR                   bigint                
  ,REV                   bigint                
  ,TRX                   bigint                
);

CREATE OR REPLACE TABLE REVENUE.revenue_pre_hlr_summary(
   FILE_DATE             date             
  ,TRX_DATE              date             
  ,BRAND                 varchar(16, dict)           
  ,OFFER_ID              varchar(8, shard_key) 
  ,REGION_HLR            varchar(16, dict)           
  ,CITY_HLR              varchar(16, dict)           
  ,ACTIVATION_CHANNEL_ID varchar(8, dict)            
  ,NODE_TYPE             varchar(8, dict)            
  ,PRE_POST_FLAG         integer               
  ,L1_NAME               varchar(32, dict)           
  ,L2_NAME               varchar(32, dict)           
  ,L3_NAME               varchar(32, dict)           
  ,L4_NAME               varchar(64, shard_key)
  ,CUST_TYPE             varchar(16, dict)           
  ,CUST_SUBTYPE          varchar(8, dict)            
  ,TRX                   bigint                
  ,REV                   bigint                
  ,DUR                   bigint                
  ,VOL                   bigint                
);

CREATE OR REPLACE TABLE REVENUE.revenue_post_hlr_summary(
   FILE_DATE             date             
  ,TRX_DATE              date             
  ,BRAND                 varchar(16, dict)           
  ,OFFER_ID              varchar(8, shard_key) 
  ,REGION_HLR            varchar(16, dict)           
  ,CITY_HLR              varchar(16, dict)           
  ,AREA_SALES            varchar(8, dict)            
  ,ACTIVATION_CHANNEL_ID varchar(8, dict)            
  ,NODE_TYPE             varchar(8, dict)            
  ,PRE_POST_FLAG         integer               
  ,L1_NAME               varchar(32, dict)           
  ,L2_NAME               varchar(32, dict)           
  ,L3_NAME               varchar(32, dict)           
  ,L4_NAME               varchar(64, shard_key)
  ,CUST_TYPE             varchar(16, dict)           
  ,CUST_SUBTYPE          varchar(8, dict)            
  ,TRX                   bigint                
  ,REV                   bigint                
  ,DUR                   bigint                
  ,VOL                   bigint                
);


/* add default file_date for history table reprocessing */
ALTER TABLE revenue_pos_cust_type_osdss ADD file_date timestamp;
UPDATE revenue_pos_cust_type_osdss SET file_date = DATE('2000-01-01') WHERE file_date IS NULL;
ALTER TABLE revenue_postpaid_hlr ADD file_date timestamp;
UPDATE revenue_postpaid_hlr SET file_date = DATE('2000-01-01') WHERE file_date IS NULL;
ALTER TABLE revenue_cluster_priceplan ADD file_date timestamp;
UPDATE revenue_cluster_priceplan SET file_date = DATE('2000-01-01') WHERE file_date IS NULL;
