delete from ${table-prefix}${cms-schema}.${table-prefix}cms_campaign_performance
where file_date = date('${run-date=yyyy-MM-dd}')
and pipeline_name = '${pipeline-name}'
and pipeline_step = '${pipeline-step}';

insert into ${table-prefix}${cms-schema}.${table-prefix}cms_campaign_performance
(
	category
	,campaign_type
	,campaign_name
	,campaign_group
	,campaign_id
	,campaign_objective_id
	,business_group
	,initiated_business
	,start_period
	,end_period
	,segment_name
	,wording
	,target
	,control
	,revenue
	,taker
	,unique_taker
	,taker_rate
	,revenue_control
	,taker_control
	,unique_taker_control
	,taker_rate_control
	,arpu_target_before
	,arpu_target_after
	,arpu_control_before
	,arpu_control_after
	,arpu_uplift_campaign
	,arpu_uplift_weight
	,file_date
	,orch_job_id
	,pipeline_name
	,pipeline_step
)
select
	cmp.category
	,cmp.campaign_type
	,cmp.campaign_name
	,cmp.campaign_group_id
	,cmp.campaign_id
	,cmp.campaign_objective_id
	,cmp.business_group
	,cmp.initiated_group
	,cmp.start_period
	,cmp.end_period
	,cmp.segment
	,wrd.wording
	,cmp.target
	,cmp.control
	,cmp.revenue
	,cmp.taker
	,cmp.unique_taker
	,cast(cmp.unique_taker as decimal(4,2))/target as taker_rate
	,cmp.revenue_control
	,cmp.taker_control
	,cmp.unique_taker_control
	,cast(cmp.taker_control as decimal(4,2))/control as taker_rate_control
	,cmp.arpu_target_before
	,cmp.arpu_target_after
	,cmp.arpu_control_before
	,cmp.arpu_control_after
	,cmp.arpu_uplift_campaign
	,cmp.arpu_uplift_weight
	,date('${run-date=yyyy-MM-dd}') as file_date
	,'${job-id}' as job_id
	,'${pipeline-name}' as pipeline_name
	,'${pipeline-step}' as pipeline_step
from
(
	select
		category
		,campaign_type
		,campaign_name
		,campaign_group_id
		,campaign_id
		,campaign_objective_id
		,business_group
		,initiated_group
		,start_period
		,end_period
		,segment
		,count(distinct msisdn) as target -- #eligible count(distinct msisdn)
		,count(distinct case when iscontrolgroup = 'true' then msisdn else null end) as control -- #control count (distinct case whene iscontrolgroup = 'true' then msisdn else null end)
		,sum(metric_1) as revenue
		,count(case when metric_1 is not null then msisdn else null end) as taker --#taker count(case whene metric_1 is not null then msisdn else null end)
		,count(distinct case when metric_1 is not null then msisdn else null end) as unique_taker --#disticnt count(distinct case whene metric_1 is not null then msisdn else null end)
		,sum(case when iscontrolgroup = 'true' then metric_1 else null end) as revenue_control --case when contrl then sum
		,count(case when iscontrolgroup = 'true' and metric_1 is not null then msisdn else null end) as taker_control --case when contrl then  #taker
		,count(distinct case when iscontrolgroup = 'true' and metric_1 is not null then msisdn else null end) as unique_taker_control --case when contrl then #distinct taker
		,null as arpu_target_before
		,null as arpu_target_after
		,null as arpu_control_before
		,null as arpu_control_after
		,null as arpu_uplift_campaign
		,null as arpu_uplift_weight
		,ki_shard_key(campaign_id)
	from ${table-prefix}${cms-schema}.${table-prefix}cms_campaign_tracking_detail
	where file_date = date('${run-date=yyyy-MM-dd}')
		and pipeline_name = '${pipeline-name}'
		and pipeline_step = 'cms_campaign_revenue_detail_insert'
	group by 
		category
		,campaign_type
		,campaign_name
		,campaign_group_id
		,campaign_id
		,campaign_objective_id
		,business_group
		,initiated_group
		,start_period
		,end_period
		,segment
) cmp
left join (select campid, wording from ${table-prefix}${cms_stg-schema}.${table-prefix}cms_definition_contact_sender_input) wrd
on cmp.campaign_id = wrd.campid
;

refresh materialized view ${table-prefix}${cms_out-schema}.${table-prefix}cms_campaign_performance_v
;