#!/bin/bash

# UC1: all tables
./capacity_planner.sh -i "uc1_all_tables.csv" -d "./dummy" -p 'Kinetica1!' -f "uc1_all_capacity.txt"
# create a comma delimtted file for use in excel later.

# UC2: Nik-Nok
#./capacity_planner.sh -i "uc2_niknok_tables.csv" -d "../../ddl/uc2" -p 'Kinetica1!' -f "uc2_niknok_capacity.txt"

# UC2: HVC
#./capacity_planner.sh -i "uc2_hvc_tables.csv" -d "../../ddl/uc2" -p 'Kinetica1!' -f "uc2_hvc_capacity.txt"

# create a comma delimtted file for use in excel later.
tr -s ' ' < "uc1_all_capacity.txt" | tr ' ' ',' > "all_capacity.csv"
#tr -s ' ' < "uc2_niknok_capacity.txt" | tr ' ' ',' >> "all_capacity.csv"
#tr -s ' ' < "uc2_hvc_capacity.txt" | tr ' ' ',' >> "all_capacity.csv"
