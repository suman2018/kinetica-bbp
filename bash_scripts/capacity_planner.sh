#!/bin/bash

fire_ddl()
{
    if [ "$RECREATEDDL" != "true" ]; then
        return 0
    fi

   ddl_file=$(find $DDLDIR -iname $table_name.sql -print)
   cmd="$kisql -h $host -p $kisqlport -user $user -pwd $PASSWD -db $schema -file $ddl_file"
   if [ "$DEBUG" == "true" ]; then
       echo "fire_ddl(): $cmd"
   fi
   eval $cmd
}

get_row_size()
{
   cmd="$kiddl -h $host -p $kiddlport -user $user -pwd $PASSWD -table $table_name"
   if [ "$DEBUG" == "true" ]; then
       echo -e "kiddl command: $cmd"
   fi
   eval $cmd > /tmp/dummy.txt
   str=$(grep -rn "memory record byte len" /tmp/dummy.txt)
   row_size=$(echo $str | awk '{print $NF}')

   if [ "$unit" == "day" ]; then
       total_size_bytes=$(( $row_size * $rows * $days_window ))
   elif [ "$unit" == "month" ]; then
       total_size_bytes=$(( $row_size * ($rows/30) * $days_window ))
   else
       total_size_bytes=0
   fi
   gb=$(( 1024 * 1024 * 1024 ))
   total_gb=$(echo $total_size_bytes/$gb |bc -l)

   printf "%-40s %-9s %-9s %-9s %-9s %-5s %-5s %-15s %-15s\n" $table_name $schema $table_type $row_size $rows $unit $days_window $total_size_bytes $total_gb >> $OUTPUTFILE
}

########## main ################

DEBUG="false"
RECREATEDDL="false"
kiddl="/opt/gpudb/kitools/kiddl/kiddl"
kisql="/opt/gpudb/kitools/kisql/kisql"
host="10.54.168.118"
user="admin"
kiddlport="9191"
kisqlport="9292"


while getopts i:d:b:p:f:r: option
do
    case "${option}"
    in
    i) TBLFILE=${OPTARG};;
    d) DDLDIR=${OPTARG};;
    b) DEBUG=${OPTARG};;
    p) PASSWD=${OPTARG};;
    f) OUTPUTFILE=${OPTARG};;
    r) RECREATEDDL=${OPTARG};;
esac
done

echo -e "Table file: ${TBLFILE}"
echo -e "DDL directory: ${DDLDIR}\n"

printf "%-40s %-9s %-9s %-9s %-9s %-5s %-5s %-15s %-15s\n" "table_name" "schema" "type" "rowsize" "rows" "unit" "days" "bytes" "GB" > $OUTPUTFILE
echo "--------------------------------------------------------------------------------------------------------------" >> $OUTPUTFILE

i=1
while read -r line; do
    test $i -eq 1 && ((i=i+1)) && continue

    IFS=',' read -ra tokens <<< "$line"    # split the line into tokens.
    cnt=${#tokens[@]}                      # get the number of tokens.

    table_name=${tokens[0]}                # some var assignments.
    schema=${tokens[1]}
    table_type=${tokens[2]}
    rows=${tokens[3]}
    unit=${tokens[4]}
    days_window=${tokens[5]}

    if [ "$DEBUG" == "true" ]; then
        echo "table name = $table_name, schema = $schema, table type = $table_stype, rows = $rows, unit = $unit, days window = $days_window"
    fi

    fire_ddl
    get_row_size

done < $TBLFILE
