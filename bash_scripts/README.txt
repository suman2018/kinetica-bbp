Here is exampe of how to execute.

# UC1: all tables
./capacity_planner.sh -i "uc1_all_tables.csv" -d "./dummy" -p 'Kinetica1!' -f "uc1_all_capacity.txt"

# UC2: Nik-Nok
./capacity_planner.sh -i "uc2_niknok_tables.csv" -d "../../ddl/uc2" -p 'Kinetica1!' -f "uc2_niknok_capacity.txt" -b "true"; cat uc2_niknok_capacity.txt

# UC2: HVC
./capacity_planner.sh -i "uc2_hvc_tables.csv" -d "../../ddl/uc2" -p 'Kinetica1!' -f "uc2_hvc_capacity.txt" -b "true" -r "true"; cat uc2_hvc_capacity.txt
